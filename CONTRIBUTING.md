Si vous voulez contribuer au projet, veuillez vous référer à la page aux instructions de la page d'accueil.

Vous pouvez aider au projet et devenir facilement un contributeur. Vous pouvez tout d'abord rejoindre la communauté discord "La Fonderie" qui suppose d'un salon dédié à PF2.

Contribuer est à la portée de chacun.

En allant sur la page du dictionnaire, vous trouverez ce qui est traduit et ce qui ne l'est pas.
Pour traduire quelque chose qui n'est pas encore traduit, il suffit de cliquer dessus.
S'ouvre alors un éditeur sur lequel vous trouverez un fichier comprenant plusieurs des champs suivants : 

* **Name:** C'est le nom en anglais du fichier et le nom affiché en anglais de la capacité, du don, du sort,... Vous n'y touchez pas.
* **Nom:** Ici vous devrez inscrire le nom que vous aurez traduit s'il ne l'est pas déjà ou copié s'il existe déjà en vf
* **PrereqEN:** Ce champ n'existe que pour les dons et correspond aux prérequis quand ils existent. Vous n'y touchez pas
* **PrereqFR:** Ici vous devrez remplir en  indiquant les prérequis traduit. S'il y en a plusieurs, vous devre les séparer avec une barre verticale qui s'obtient en appuyant sur AltGr+6.
* **État:** S'il est inscrit aucune, c'est que le fichier n'a jamais été traduit. Si vous faites la traduction, vous remplacez aucune par libre ; si vous réemployez la traduction officielle effectuée par BBE en français, vous remplacez par officielle. S'il est écrit changé, le fichier a déjà été complété et cela signifie que le fichier anglophone a connu en principe des modifications par rapport au moment où il a été traduit. Cela peut être des changements de structure, de balises, l'insertion de formules de calculs. Dans ce cas, en dessous, figure un autre champ. Il faut alors repérer les modifications existantes entre la vf et la vo. Il faut être certain de toutes les repérer avant de modifier quoi que ce soit.
* **État d'origine:** Ce champ n'existe que si le fichier a déjà été traduit. 

* ------ **Description (en)** ------
suivie d'un texte en anglais, comprenant une mise en forme avec des balises de code.

* ------ **Description (en)** ------
S'il n'existe aucune traduction, ce champ est vide et vous devrez le remplir.

** soit vous disposez de la traduction officielle. Alors vous la copier et replacez les balises existantes en anglais.

** Soit vous ne disposez pas de la traduction officielle. Alors vous traduisez en vous aidant au besoin du Dictionnaire, notamment pour les mots qui font l'objet d'une balise dans la traduction anglaise.

Vous pouvez facilement voir comment on fait en éditant un fichier déjà traduit.

Au besoin, vous pouvez vous faire aider par différents membres de la communauté francophone sur le salon PF2 du discord La Fonderie ou sur le salon dédié au jeu Pathfinder francophone.

* **Quelques conventions et observations** :

** Une majuscule est utilisée sur le premier mot du nom de la capacité que vous traduisez et sur les noms propres. Ainsi : _Blocage au bouclier_

** La première lettre des mots qui correspondent à un terme technique du jeu sont fréquemment en majuscule. Ainsi : _vous faites une Frappe à une créature Observée._

** Il existe des balises pour créer des liens que vous repérerez facilement car elles prennent la forme  suivante @Compendium[pf2e.feats-srd.muMOxZyduEFv8UT6]{Nom en français}. Quand vous les croisez, vous remplacez ce qui est entre les {nom affiché}. Vous pouvez alors sans souci mettre au féminin, au pluriel, conjuguer, le lien se faisant par renvoi à l'ID du compendium indiquez entre les [crochets]

** il existe des balises qui permettent en jeu de lancer les dés. Elles ont une structure très particulière que vous ne touchez pas sauf si vous êtes capable de coder.

Fait le 26 décembre 2020
