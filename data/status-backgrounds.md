# État de la traduction (backgrounds)

 * **changé**: 186
 * **aucune**: 19


Dernière mise à jour: 2021-02-07 20:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[1VdLr4Qm8fv1m4tM.htm](backgrounds/1VdLr4Qm8fv1m4tM.htm)|Godless Graycloak|
|[3hKnDmRXtF3LGmjy.htm](backgrounds/3hKnDmRXtF3LGmjy.htm)|Blow-In (Thievery)|
|[3M2FRDlunjFshzbq.htm](backgrounds/3M2FRDlunjFshzbq.htm)|Fogfen Tale-Teller|
|[5qUQOpdlNsJjpFVX.htm](backgrounds/5qUQOpdlNsJjpFVX.htm)|Ruin Delver|
|[ap25MWBuFGwwhYIG.htm](backgrounds/ap25MWBuFGwwhYIG.htm)|Aspiring Free-Captain|
|[aWAfj7bhTZM2oK81.htm](backgrounds/aWAfj7bhTZM2oK81.htm)|Hookclaw Digger|
|[BZhPPw9VD9U2ur6B.htm](backgrounds/BZhPPw9VD9U2ur6B.htm)|Witchlight Follower|
|[faHydq29Q4RP5CAK.htm](backgrounds/faHydq29Q4RP5CAK.htm)|Martial Disciple (Athletics)|
|[H3E69w8Xg0T7rAqD.htm](backgrounds/H3E69w8Xg0T7rAqD.htm)|Shoanti Name-Bearer|
|[HNp0uNsIx3BNBcr5.htm](backgrounds/HNp0uNsIx3BNBcr5.htm)|Animal Wrangler (Athletics)|
|[iWWg16f3re1YChiD.htm](backgrounds/iWWg16f3re1YChiD.htm)|Oenopion-Ooze Tender|
|[lRVYgV9zL6O6O3U4.htm](backgrounds/lRVYgV9zL6O6O3U4.htm)|Martial Disciple (Acrobatics)|
|[MiRWGXZnEdurMvVf.htm](backgrounds/MiRWGXZnEdurMvVf.htm)|Eldritch Anatomist|
|[n2JN5Kiu7tOCAHPr.htm](backgrounds/n2JN5Kiu7tOCAHPr.htm)|Market Runner|
|[sR3S7Xn15drU6rOF.htm](backgrounds/sR3S7Xn15drU6rOF.htm)|Starwatcher|
|[TPZ0ev0Tl5sveZuM.htm](backgrounds/TPZ0ev0Tl5sveZuM.htm)|Animal Wrangler (Nature)|
|[tsMvqxJhl6xgcDaU.htm](backgrounds/tsMvqxJhl6xgcDaU.htm)|Blow-In (Deception)|
|[UgityMZaujmYUpil.htm](backgrounds/UgityMZaujmYUpil.htm)|Out-Of-Towner|
|[xvz5F7iYBWEIjz0r.htm](backgrounds/xvz5F7iYBWEIjz0r.htm)|Bibliophile|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0c9Np7Yq5JSxZ6Tb.htm](backgrounds/0c9Np7Yq5JSxZ6Tb.htm)|Alkenstar Tinker|Bricoleur d'Alkenstar|changé|
|[0z0PSizHviOehdJF.htm](backgrounds/0z0PSizHviOehdJF.htm)|Haunting Vision|Hanté par une vision|changé|
|[0ZfBP7Tp2P3WN7Dp.htm](backgrounds/0ZfBP7Tp2P3WN7Dp.htm)|Amnesiac|Amnésique|changé|
|[2bzqI0D4J3LUi8nq.htm](backgrounds/2bzqI0D4J3LUi8nq.htm)|Laborer|Manoeuvre|changé|
|[2qH61dLeaqgNOdOp.htm](backgrounds/2qH61dLeaqgNOdOp.htm)|Desert Tracker|Pisteur du désert|changé|
|[3frMfODIYFeqTl2k.htm](backgrounds/3frMfODIYFeqTl2k.htm)|Fortune Teller|Voyant|changé|
|[3gN09dOT2hMwGcK2.htm](backgrounds/3gN09dOT2hMwGcK2.htm)|Cook|Cuisinier|changé|
|[3kXTGUvodNMnJTxb.htm](backgrounds/3kXTGUvodNMnJTxb.htm)|Aiudara Seeker|Chercheur d'aiudara|changé|
|[3wLnNwWnZ2dHIbV4.htm](backgrounds/3wLnNwWnZ2dHIbV4.htm)|Diobel Pearl Diver|Pêcheur de perles de Diobel|changé|
|[3WPo7m6rJQh9L7MN.htm](backgrounds/3WPo7m6rJQh9L7MN.htm)|Emissary|Émissaire|changé|
|[4a2sVO0o2mMTydN8.htm](backgrounds/4a2sVO0o2mMTydN8.htm)|Local Scion|Enfant du pays|changé|
|[4aVFnYyRajog0mNl.htm](backgrounds/4aVFnYyRajog0mNl.htm)|Mantis Scion|Fils de la Mante|changé|
|[4fBIXtSVSRYn2ZGi.htm](backgrounds/4fBIXtSVSRYn2ZGi.htm)|Goblinblood Orphan|Orphelin des guerres du Sang gobelin|changé|
|[4naQmCXBl0007c2W.htm](backgrounds/4naQmCXBl0007c2W.htm)|Touched by Dahak|Touché par Dahak|changé|
|[4SSKsyD72AYYpzgm.htm](backgrounds/4SSKsyD72AYYpzgm.htm)|Hermit|Ermite|changé|
|[4yN5miHoMvKwZIsa.htm](backgrounds/4yN5miHoMvKwZIsa.htm)|Press-Ganged|Forçat|changé|
|[5RGLAPi5sLykRcmm.htm](backgrounds/5RGLAPi5sLykRcmm.htm)|Animal Whisperer|Dresseur|changé|
|[5Z3cLEpsx9nHVwhM.htm](backgrounds/5Z3cLEpsx9nHVwhM.htm)|Hunter|Chasseur|changé|
|[6abqATPjYoF946LD.htm](backgrounds/6abqATPjYoF946LD.htm)|Bandit|Bandit|changé|
|[6c0rsuiiAaVqGTu7.htm](backgrounds/6c0rsuiiAaVqGTu7.htm)|Rivethun Adherent|Adhérent du Rivethun|changé|
|[6irgRkKZ8tRZzLvs.htm](backgrounds/6irgRkKZ8tRZzLvs.htm)|Artisan|Artisan|changé|
|[6K6jJkjZ2MJYqQ6h.htm](backgrounds/6K6jJkjZ2MJYqQ6h.htm)|Bellflower Agent|Agent de la Campanule|changé|
|[6UmhTxOQeqFnppxx.htm](backgrounds/6UmhTxOQeqFnppxx.htm)|Guard|Garde|changé|
|[6vsoyCZKqxG0lVe8.htm](backgrounds/6vsoyCZKqxG0lVe8.htm)|Inlander|Habitant de l'intérieur des terres|changé|
|[76RK9WizWYdyhMy5.htm](backgrounds/76RK9WizWYdyhMy5.htm)|Mammoth Speaker|Porte parole mammouth|changé|
|[7AfixHrjbXgDPPkp.htm](backgrounds/7AfixHrjbXgDPPkp.htm)|Translator|Traducteur|changé|
|[7IrOApgShgnmp1A5.htm](backgrounds/7IrOApgShgnmp1A5.htm)|Rigger|Gréeur|changé|
|[7K6ZSWOoihZKSdyd.htm](backgrounds/7K6ZSWOoihZKSdyd.htm)|Ruby Phoenix Enthusiast|Enthousiaste du Phénix de rubis|changé|
|[84uVpQFCqn0Atfpo.htm](backgrounds/84uVpQFCqn0Atfpo.htm)|Legendary Parents|Parents légendaires|changé|
|[86TbxxwfpWjScwSQ.htm](backgrounds/86TbxxwfpWjScwSQ.htm)|Undercover Lotus Guard|Garde Lotus infiltré|changé|
|[88WyCqU5x1eJ0MK2.htm](backgrounds/88WyCqU5x1eJ0MK2.htm)|Gladiator|Gladiateur|changé|
|[89LEOv97ZwsjnhNx.htm](backgrounds/89LEOv97ZwsjnhNx.htm)|Gambler|Parieur|changé|
|[8q4PhvpmIxZD7rsV.htm](backgrounds/8q4PhvpmIxZD7rsV.htm)|Rostland Partisan|Partisan du Rost|changé|
|[8UEKgUkagUDixkL2.htm](backgrounds/8UEKgUkagUDixkL2.htm)|Issian Partisan|Partisan Issien|changé|
|[8UXahQfkP9GZ1TNW.htm](backgrounds/8UXahQfkP9GZ1TNW.htm)|Nomad|Nomade|changé|
|[93icIDHD4IrqI2oV.htm](backgrounds/93icIDHD4IrqI2oV.htm)|Sodden Scavenger|récupérateur détrempé|changé|
|[9LnXsMRwYcxi7nDO.htm](backgrounds/9LnXsMRwYcxi7nDO.htm)|Bekyar Restorer|Restaurateur Bekyar|changé|
|[9lVw1JGl5ser6626.htm](backgrounds/9lVw1JGl5ser6626.htm)|Criminal|Criminel|changé|
|[9pK15dQJVypSCjzO.htm](backgrounds/9pK15dQJVypSCjzO.htm)|Blessed|Béni|changé|
|[9uTdwJaj27F18ZDX.htm](backgrounds/9uTdwJaj27F18ZDX.htm)|Razmiran Faithful|Croyant en Razmir|changé|
|[a45LqkSRX07ljKdW.htm](backgrounds/a45LqkSRX07ljKdW.htm)|Merabite Prodigy|Prodige Merabite|changé|
|[a5dCSuAwGE2hqQjj.htm](backgrounds/a5dCSuAwGE2hqQjj.htm)|Freed Slave of Absalom|Esclave libéré d'Absalom|changé|
|[a8BmnIIUR7AYog5B.htm](backgrounds/a8BmnIIUR7AYog5B.htm)|Barkeep|Tavernier|changé|
|[a9Q4iIiAZryVWN27.htm](backgrounds/a9Q4iIiAZryVWN27.htm)|Bounty Hunter|Chasseur de primes|changé|
|[AfBCrHsw1xbRFejN.htm](backgrounds/AfBCrHsw1xbRFejN.htm)|Sleepless Suns Star|Étoile des soleils sans repos|changé|
|[aisuJF1A98bHfkLH.htm](backgrounds/aisuJF1A98bHfkLH.htm)|Whispering Way Scion|Enfant de la Voie du Murmure|changé|
|[AJ41zFEYwlOUghXp.htm](backgrounds/AJ41zFEYwlOUghXp.htm)|Osirionologist|Osirionologiste|changé|
|[ajcpRVb5EG00l7Y4.htm](backgrounds/ajcpRVb5EG00l7Y4.htm)|Cursed Family|Famille maudite|changé|
|[apXTV7jJx6yJpj8D.htm](backgrounds/apXTV7jJx6yJpj8D.htm)|Prisoner|Prisonnier|changé|
|[B8kEwzPUMIjhofUm.htm](backgrounds/B8kEwzPUMIjhofUm.htm)|Sarkorian Survivor|Survivant du sarkoris|changé|
|[b9EPEY09dYOVzdue.htm](backgrounds/b9EPEY09dYOVzdue.htm)|Shadow War Survivor|Survivant de la Guerre de l'ombre|changé|
|[BBeJA7n0xpSsBCGq.htm](backgrounds/BBeJA7n0xpSsBCGq.htm)|Lesser Scion|Benjamin|changé|
|[bCJ9p3P5uJDAtaUI.htm](backgrounds/bCJ9p3P5uJDAtaUI.htm)|Miner|Mineur|changé|
|[bDyb0k0rTfDTyhd8.htm](backgrounds/bDyb0k0rTfDTyhd8.htm)|Geb Crusader|Croisé du Geb|changé|
|[bh6O2Ad5mkYwRngM.htm](backgrounds/bh6O2Ad5mkYwRngM.htm)|Hermean Expatriate|Expatrié d'Herméa|changé|
|[CAjQrHZZbALE7Qjy.htm](backgrounds/CAjQrHZZbALE7Qjy.htm)|Acolyte|Acolyte|changé|
|[cFdndc4pMWhnRUOY.htm](backgrounds/cFdndc4pMWhnRUOY.htm)|Teacher|Enseignant|changé|
|[CKU1sbFofcwZUJMx.htm](backgrounds/CKU1sbFofcwZUJMx.htm)|Ex-Con Token Guard|Garde des Pièces ancien taulard|changé|
|[dAvFZ5QmbAHgXcNp.htm](backgrounds/dAvFZ5QmbAHgXcNp.htm)|Outrider|Éclaireur|changé|
|[DBxOUwM7qhGH8MrF.htm](backgrounds/DBxOUwM7qhGH8MrF.htm)|Courier|Garçon de course|changé|
|[dd6DbCsT67rl8va3.htm](backgrounds/dd6DbCsT67rl8va3.htm)|Magaambya Academic|Étudiant de Magaambya|changé|
|[DHrzVqB8f1ed3zTk.htm](backgrounds/DHrzVqB8f1ed3zTk.htm)|Clown|Clown|changé|
|[dVRDDjT4FOu6uLDR.htm](backgrounds/dVRDDjT4FOu6uLDR.htm)|Detective|Détective|changé|
|[DVtZab19D1vD3a0n.htm](backgrounds/DVtZab19D1vD3a0n.htm)|Post Guard of All Trades|Garde du poste touche-à-tout|changé|
|[E2ij2Cg8oMC0W0NS.htm](backgrounds/E2ij2Cg8oMC0W0NS.htm)|Nirmathi Guerrilla|Escarmoucheur Nirmathi|changé|
|[EJRWGsPWzAhixuvQ.htm](backgrounds/EJRWGsPWzAhixuvQ.htm)|Belkzen Slayer|Tueur du Belkzen|changé|
|[eYY3bX7xSH7aicqT.htm](backgrounds/eYY3bX7xSH7aicqT.htm)|Atteran Rancher|Rancher Atteran|changé|
|[ffcNsTUBsxFwbNgJ.htm](backgrounds/ffcNsTUBsxFwbNgJ.htm)|Lastwall Survivor|Survivant de Dernier-Rempart|changé|
|[FKHut73XDUGTnKkP.htm](backgrounds/FKHut73XDUGTnKkP.htm)|Field Medic|Médecin militaire|changé|
|[fML6YrXYDqQy0g7L.htm](backgrounds/fML6YrXYDqQy0g7L.htm)|Iolite Trainee Hobgoblin|Recrue iolite hobgobeline|changé|
|[fuTLDmihr9Z9e5wa.htm](backgrounds/fuTLDmihr9Z9e5wa.htm)|Cultist|Cultiste|changé|
|[gfklP8ub45R4wXKe.htm](backgrounds/gfklP8ub45R4wXKe.htm)|Aspiring River Monarch|Aspirant Monarque du Fleuve|changé|
|[GNidqGnSABx1rQUQ.htm](backgrounds/GNidqGnSABx1rQUQ.htm)|Missionary|Missionnaire|changé|
|[GPI5kNu0xfom9kKa.htm](backgrounds/GPI5kNu0xfom9kKa.htm)|Dragon Scholar|Spécialiste des dragons|changé|
|[h98cEl4DY75IL6KJ.htm](backgrounds/h98cEl4DY75IL6KJ.htm)|Teamster|Conducteur d'attelages|changé|
|[HdnmIaLadhRfZq8X.htm](backgrounds/HdnmIaLadhRfZq8X.htm)|Insurgent|Insurgé|changé|
|[HDquvQywAZimmcFF.htm](backgrounds/HDquvQywAZimmcFF.htm)|Refugee (Fall of Plaguestone)|Réfugié (la Chute de Plaguestone)|changé|
|[HEd2Lxgvl080nRxx.htm](backgrounds/HEd2Lxgvl080nRxx.htm)|Shadow Lodge Defector|Défecteur de la Loge de l'ombre|changé|
|[HKRcQO8Xj7xzBxAw.htm](backgrounds/HKRcQO8Xj7xzBxAw.htm)|Faction Opportunist|Opportuniste des Factions|changé|
|[hPx0xiv00GQqPWUH.htm](backgrounds/hPx0xiv00GQqPWUH.htm)|Kalistrade Follower|Suivant de Kalistrade|changé|
|[HZ3oBBdEnsH3fWrm.htm](backgrounds/HZ3oBBdEnsH3fWrm.htm)|Shory Seeker|Chercheur Rivain|changé|
|[I0vuIFypx8ADSJQC.htm](backgrounds/I0vuIFypx8ADSJQC.htm)|Black Market Smuggler|Contrebandier du Marché noir|changé|
|[i28Z9JXhEvoc7BX5.htm](backgrounds/i28Z9JXhEvoc7BX5.htm)|Refugee (APG)|Réfugié (MJA)|changé|
|[i4hN6OYv8qmi3GLW.htm](backgrounds/i4hN6OYv8qmi3GLW.htm)|Entertainer|Bateleur|changé|
|[i5G6E5dkGWiq838C.htm](backgrounds/i5G6E5dkGWiq838C.htm)|Scholar of the Ancients|Étudiant de l'Antiquité|changé|
|[i6y4DiKvqitdE0PW.htm](backgrounds/i6y4DiKvqitdE0PW.htm)|Quick|Rapide|changé|
|[i79pgNIAtJfkkOiw.htm](backgrounds/i79pgNIAtJfkkOiw.htm)|Tinker|Bricoleur|changé|
|[iaM6TjvijLCgiHeD.htm](backgrounds/iaM6TjvijLCgiHeD.htm)|Returning Descendant|Descendant de retour|changé|
|[IFHYbU6Nu8BiTsRa.htm](backgrounds/IFHYbU6Nu8BiTsRa.htm)|Acrobat|Acrobate|changé|
|[IfpYRxN8qyV4ym0o.htm](backgrounds/IfpYRxN8qyV4ym0o.htm)|Purveyor of the Bizzare|Fournisseur de Bizarreries|changé|
|[IoBhge83aYpq0pPV.htm](backgrounds/IoBhge83aYpq0pPV.htm)|Archaeologist|Archéologue|changé|
|[IObZEUz8wneEMgR3.htm](backgrounds/IObZEUz8wneEMgR3.htm)|Deckhand|Homme de pont|changé|
|[irDibuV3Wi7T43sL.htm](backgrounds/irDibuV3Wi7T43sL.htm)|Child of the Puddles|Marmot des flaques|changé|
|[ixluAGUDZciLEHtb.htm](backgrounds/ixluAGUDZciLEHtb.htm)|Tax Collector|Percepteur|changé|
|[IXxdCzBS0xP20ckw.htm](backgrounds/IXxdCzBS0xP20ckw.htm)|Ward|Pupille|changé|
|[j2G71vQahw1DiWpO.htm](backgrounds/j2G71vQahw1DiWpO.htm)|Cursed|Maudit|changé|
|[j9v38iHA0sVy59SR.htm](backgrounds/j9v38iHA0sVy59SR.htm)|Pathfinder Hopeful|Aspirant Éclaireur|changé|
|[JauSkDtMV6dhDZS8.htm](backgrounds/JauSkDtMV6dhDZS8.htm)|Political Scion|Rejeton de politicien|changé|
|[JfGVkZkaoz2lnmov.htm](backgrounds/JfGVkZkaoz2lnmov.htm)|Bright Lion|Lion brillant|changé|
|[khGFmnQMBYmz2ONR.htm](backgrounds/khGFmnQMBYmz2ONR.htm)|Sarkorian Reclaimer|Réclamateur du Sarkoris|changé|
|[kLjqmylGOXeQ5o5Y.htm](backgrounds/kLjqmylGOXeQ5o5Y.htm)|Bonuwat Wavetouched|Bonuwat Touché par les vagues|changé|
|[KMv7ollLVaZ81XDV.htm](backgrounds/KMv7ollLVaZ81XDV.htm)|Merchant|Marchand|changé|
|[lav3yRNPc7lQ7e9k.htm](backgrounds/lav3yRNPc7lQ7e9k.htm)|Senghor Sailor|Marin de Senghor|changé|
|[lCR8gyEZbwqh3RWi.htm](backgrounds/lCR8gyEZbwqh3RWi.htm)|Harbor Guard Moonlighter|Noctambule de la Garde du port|changé|
|[LJmBnA2IYDBqQgRx.htm](backgrounds/LJmBnA2IYDBqQgRx.htm)|Tapestry Refugee|Réfugié de la Tapisserie|changé|
|[locc0cjOmOQHe3j7.htm](backgrounds/locc0cjOmOQHe3j7.htm)|Savior of Air|Sauveur de l'air|changé|
|[lqjmBmGHYRaSiglZ.htm](backgrounds/lqjmBmGHYRaSiglZ.htm)|Molthuni Mercenary|Mercenaire du Molthune|changé|
|[LwAu4r3uocYfpKA8.htm](backgrounds/LwAu4r3uocYfpKA8.htm)|Trailblazer|Pionnier|changé|
|[m1vRLRHTpCrgk89G.htm](backgrounds/m1vRLRHTpCrgk89G.htm)|Thrune Loyalist|Loyaliste Thrune|changé|
|[moVRsnpjB5THCwxE.htm](backgrounds/moVRsnpjB5THCwxE.htm)|Street Urchin|Enfant des rues|changé|
|[mrkgVjiEdlPjLUsN.htm](backgrounds/mrkgVjiEdlPjLUsN.htm)|Vidrian Reformer|Réformateur vidrien|changé|
|[MslumKt6iwJ85GKZ.htm](backgrounds/MslumKt6iwJ85GKZ.htm)|Thuvian Unifier|Unificateur de Thuvie|changé|
|[mxJRdRSMsyZfBf5c.htm](backgrounds/mxJRdRSMsyZfBf5c.htm)|Hermean Heritor|Héritier d'Herméa|changé|
|[nhQKn1tVV6PKCurq.htm](backgrounds/nhQKn1tVV6PKCurq.htm)|Butcher|"Boucher"|changé|
|[nnBAO4NwdINCGQFK.htm](backgrounds/nnBAO4NwdINCGQFK.htm)|Learned Guard Prodigy|Prodige de la Garde Instruite|changé|
|[ns9BOvCyjdapYhI0.htm](backgrounds/ns9BOvCyjdapYhI0.htm)|Root Worker|Chasseur de racines|changé|
|[NXYce9NAHls2fcIf.htm](backgrounds/NXYce9NAHls2fcIf.htm)|Pathfinder Recruiter|Recruteur des Éclaireurs|changé|
|[NywLl1XMQmzA6rP7.htm](backgrounds/NywLl1XMQmzA6rP7.htm)|Demon Slayer|Tueur de démons|changé|
|[NZY0r4Csjul6eVPp.htm](backgrounds/NZY0r4Csjul6eVPp.htm)|Finadar Leshy|Léchi de Finadar|changé|
|[o1lhSKOpKPamTITI.htm](backgrounds/o1lhSKOpKPamTITI.htm)|Bookkeeper|Comptable|changé|
|[o7RbsQbv5iLRvd8j.htm](backgrounds/o7RbsQbv5iLRvd8j.htm)|Scout|Éclaireur|changé|
|[oEm937kNrP5sXxFD.htm](backgrounds/oEm937kNrP5sXxFD.htm)|Emancipated|Émancipé|changé|
|[OhP7cqvNouFgHIdJ.htm](backgrounds/OhP7cqvNouFgHIdJ.htm)|Sewer Dragon|Dragon des égoûts|changé|
|[p27PSjFtHAWikKaw.htm](backgrounds/p27PSjFtHAWikKaw.htm)|Early Explorer|Explorateur précoce|changé|
|[P65AGDPkhD2B4JtG.htm](backgrounds/P65AGDPkhD2B4JtG.htm)|Perfection Seeker|À la recherche de la Perfection|changé|
|[pBX18FI1grWwkWjk.htm](backgrounds/pBX18FI1grWwkWjk.htm)|Kyonin Emissary|Émissaire du Kyonin|changé|
|[pGOlKz4Krnh7MyUM.htm](backgrounds/pGOlKz4Krnh7MyUM.htm)|Haunted|Hanté|changé|
|[PhqUBXLLkVXb6oUE.htm](backgrounds/PhqUBXLLkVXb6oUE.htm)|Taldan Schemer|Intrigant taldorien|changé|
|[Phvnfdmz4bB7jrI3.htm](backgrounds/Phvnfdmz4bB7jrI3.htm)|Barrister|Avocat|changé|
|[PO94ilqe62V6jtBE.htm](backgrounds/PO94ilqe62V6jtBE.htm)|Spell Seeker|Chercheur de sorts|changé|
|[ppBGlWl0UkBKkJgE.htm](backgrounds/ppBGlWl0UkBKkJgE.htm)|Feybound|Engagé avec les fées|changé|
|[Q2brdDtEoI3cmpuD.htm](backgrounds/Q2brdDtEoI3cmpuD.htm)|Feral Child|Enfant sauvage|changé|
|[qB7g1OiZ8v8zgvkL.htm](backgrounds/qB7g1OiZ8v8zgvkL.htm)|Wonder Taster|Goûteur de merveilles|changé|
|[qbvzNG8hMjb8f66D.htm](backgrounds/qbvzNG8hMjb8f66D.htm)|Squire|Écuyer|changé|
|[qY4IUwVWIKPSFskP.htm](backgrounds/qY4IUwVWIKPSFskP.htm)|Aerialist|Trapéziste|changé|
|[r0kYIbN06Cv8eNG3.htm](backgrounds/r0kYIbN06Cv8eNG3.htm)|Warrior|Homme d'armes|changé|
|[R1v4gUu8oRMoOASM.htm](backgrounds/R1v4gUu8oRMoOASM.htm)|Wildwood Local|Habitué des bois|changé|
|[r9fzNQEz33HyKTxm.htm](backgrounds/r9fzNQEz33HyKTxm.htm)|Pilgrim|Pélerin|changé|
|[RC4l6WsxPn89a1f8.htm](backgrounds/RC4l6WsxPn89a1f8.htm)|Raised by Belief|Élevé dans la croyance|changé|
|[RgFOKlEmMIw2eZpo.htm](backgrounds/RgFOKlEmMIw2eZpo.htm)|Harrow-Led|Déterminé par le Tourment|changé|
|[rzyRtasSTfHS3e0y.htm](backgrounds/rzyRtasSTfHS3e0y.htm)|Returned|Ressuscité|changé|
|[SJ3nNOI5A8A4hK0Q.htm](backgrounds/SJ3nNOI5A8A4hK0Q.htm)|Winter's Child|Enfant de l'Hiver|changé|
|[SOmJyAtPOokesZoe.htm](backgrounds/SOmJyAtPOokesZoe.htm)|Farmhand|Ouvrier agricole|changé|
|[su8y75pGMVTUsNHK.htm](backgrounds/su8y75pGMVTUsNHK.htm)|Thassilonian Traveler|Voyageur Thassilonien|changé|
|[t0t1ck8iKpaI4o5W.htm](backgrounds/t0t1ck8iKpaI4o5W.htm)|Charlatan|Charlatan|changé|
|[tA0nggrWfBEhvsKA.htm](backgrounds/tA0nggrWfBEhvsKA.htm)|Chelish Rebel|Rebelle chélaxien|changé|
|[TC7jpN5EA4UBIYep.htm](backgrounds/TC7jpN5EA4UBIYep.htm)|Truth Seeker|En quête de vérité|changé|
|[tcsSxwkl4wCsfO3k.htm](backgrounds/tcsSxwkl4wCsfO3k.htm)|Trade Consortium Underling|Subalterne de consortium marchand|changé|
|[TPoP1mKpqUOpRQ5Y.htm](backgrounds/TPoP1mKpqUOpRQ5Y.htm)|Reputation Seeker|En quête de renommée|changé|
|[tQ9t7uIssRCR2y3W.htm](backgrounds/tQ9t7uIssRCR2y3W.htm)|Final Blade Survivor|Survivant de la Lame finale|changé|
|[Ty8FRM0k262xuHfF.htm](backgrounds/Ty8FRM0k262xuHfF.htm)|Undersea Enthusiast|Enthousiaste aquatique|changé|
|[uC6D2nmDTATxXrV6.htm](backgrounds/uC6D2nmDTATxXrV6.htm)|Royalty|De sang royal|changé|
|[UdOUj7i8XGTI72Zc.htm](backgrounds/UdOUj7i8XGTI72Zc.htm)|Servant|Serviteur|changé|
|[uJcFanGjVranEarv.htm](backgrounds/uJcFanGjVranEarv.htm)|Lumber Consortium Laborer|Ouvrier du Consortium du Bois|changé|
|[uNhdcyhiog7YvXPT.htm](backgrounds/uNhdcyhiog7YvXPT.htm)|Varisian Wanderer|Vagabond Varisien|changé|
|[uNvD4XK1kdvGjQVo.htm](backgrounds/uNvD4XK1kdvGjQVo.htm)|Child of Westcrown|Enfant de Couronne d'Ouest|changé|
|[UURvnfwXypRYYXBI.htm](backgrounds/UURvnfwXypRYYXBI.htm)|Storm Survivor|Rescapé d'une tempête|changé|
|[UyddtAwqDGjQ1SZK.htm](backgrounds/UyddtAwqDGjQ1SZK.htm)|Scholar of the Sky Key|Étudiant de la Clé du Ciel|changé|
|[V1RAIckpUJd2OzXi.htm](backgrounds/V1RAIckpUJd2OzXi.htm)|Mana Wastes Refugee|Réfugié de la Désolation de Mana|changé|
|[V31KRG7aA7xS0m8L.htm](backgrounds/V31KRG7aA7xS0m8L.htm)|Shadow Haunted|Ombre hantée|changé|
|[V3nYEAhyA54RtYky.htm](backgrounds/V3nYEAhyA54RtYky.htm)|Ulfen Raider|Raider ulfe|changé|
|[vBPu7RwNXGDQ1ThL.htm](backgrounds/vBPu7RwNXGDQ1ThL.htm)|Dreamer of the Verdant Moon|Rêveur de la lune verdoyante|changé|
|[vE6nb2OSIXqprDXk.htm](backgrounds/vE6nb2OSIXqprDXk.htm)|Sally Guard Neophyte|Néophyte de la Garde des Percées|changé|
|[vgin9ff2sUBMpuaI.htm](backgrounds/vgin9ff2sUBMpuaI.htm)|Former Aspis Agent|Ancien Agent de l'Aspis|changé|
|[vHeP960qjhfob4Je.htm](backgrounds/vHeP960qjhfob4Je.htm)|Scavenger|Récupérateur|changé|
|[vjhB0ZTV9OZgSuSz.htm](backgrounds/vjhB0ZTV9OZgSuSz.htm)|Thassilonian Delver|Fouilleur du thassilon|changé|
|[vNWSzv36L1GBPPoc.htm](backgrounds/vNWSzv36L1GBPPoc.htm)|Hellknight Historian|Historien des chevaliers infernaux|changé|
|[WRVEUkemqj2uNHwl.htm](backgrounds/WRVEUkemqj2uNHwl.htm)|Scholar|Érudit|changé|
|[wU1qd8tZNcYn43y2.htm](backgrounds/wU1qd8tZNcYn43y2.htm)|Lost and Alone|Perdu et seul|changé|
|[wudDO9OEsRjJsqhU.htm](backgrounds/wudDO9OEsRjJsqhU.htm)|Barber|Barbier|changé|
|[WueM94C9JXk10jPd.htm](backgrounds/WueM94C9JXk10jPd.htm)|Onyx Trader|Marchand d'Onyx|changé|
|[x2y25cE98Eq4qxbu.htm](backgrounds/x2y25cE98Eq4qxbu.htm)|Ustalavic Academic|Étudiant ustalavien|changé|
|[xCCvT9tprRQVFVDq.htm](backgrounds/xCCvT9tprRQVFVDq.htm)|Grizzled Muckrucker|Fangeux grisonnant|changé|
|[XHY0xrSSbX0cTJKK.htm](backgrounds/XHY0xrSSbX0cTJKK.htm)|Droskari Disciple|Disciple de Droskar|changé|
|[y0WZS51fSi6dILHq.htm](backgrounds/y0WZS51fSi6dILHq.htm)|Secular Medic|Médecin séculier|changé|
|[Y35nOXZRryiyHjlk.htm](backgrounds/Y35nOXZRryiyHjlk.htm)|Nexian Mystic|Mystique Nexien|changé|
|[Y50ssWBBKSRVBpSa.htm](backgrounds/Y50ssWBBKSRVBpSa.htm)|Mystic Seer|Voyant mystique|changé|
|[y9OyNsxGfmjqdcP0.htm](backgrounds/y9OyNsxGfmjqdcP0.htm)|Artist|Artiste|changé|
|[yAtyaKbcHZWCJlf5.htm](backgrounds/yAtyaKbcHZWCJlf5.htm)|Witch Wary|Gare aux sorcières|changé|
|[YJpEdmSOjlA2QZeu.htm](backgrounds/YJpEdmSOjlA2QZeu.htm)|Circus Born|Circassien|changé|
|[yK40c3082U30BUX5.htm](backgrounds/yK40c3082U30BUX5.htm)|Grand Council Bureaucrat|Bureaucrate du Grand Conseil|changé|
|[Yu7Cl0Lk94LdPRi6.htm](backgrounds/Yu7Cl0Lk94LdPRi6.htm)|Noble|Noble|changé|
|[YyzIzLxn2UCFubj4.htm](backgrounds/YyzIzLxn2UCFubj4.htm)|Menagerie Dung Sweeper|Balayeur de fumier de la Ménagerie|changé|
|[z4cCsOT36MB7xldR.htm](backgrounds/z4cCsOT36MB7xldR.htm)|Barker|Aboyeur|changé|
|[ZdhPKEY9FfaOS8Wy.htm](backgrounds/ZdhPKEY9FfaOS8Wy.htm)|Herbalist|Herboriste|changé|
|[Zmwyhsxe4i6rZN75.htm](backgrounds/Zmwyhsxe4i6rZN75.htm)|Sailor|Marin|changé|
