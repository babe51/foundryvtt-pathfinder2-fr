# État de la traduction (spell-effects)

 * **libre**: 33
 * **changé**: 5
 * **aucune**: 142


Dernière mise à jour: 2021-02-07 20:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[00-b2kWJuCPj1rDMdwz.htm](spell-effects/00-b2kWJuCPj1rDMdwz.htm)|Stance: Wolf Stance|
|[00-BCyGDKcplkJiSAKJ.htm](spell-effects/00-BCyGDKcplkJiSAKJ.htm)|Stance: Stumbling Stance|
|[00-CgxYa0lrLUjS2ZhI.htm](spell-effects/00-CgxYa0lrLUjS2ZhI.htm)|Stance: Cobra Stance|
|[00-eeAlh6edygcZIz9c.htm](spell-effects/00-eeAlh6edygcZIz9c.htm)|Stance: Wild Winds Stance|
|[00-gYpy9XBPScIlY93p.htm](spell-effects/00-gYpy9XBPScIlY93p.htm)|Stance: Mountain Stance|
|[00-Im5JBInybWFbHEYS.htm](spell-effects/00-Im5JBInybWFbHEYS.htm)|Stance: Rain of Embers Stance|
|[00-nwkYZs6YwXYAJ4ps.htm](spell-effects/00-nwkYZs6YwXYAJ4ps.htm)|Stance: Crane Stance|
|[00-pf9yvKNg6jZLrE30.htm](spell-effects/00-pf9yvKNg6jZLrE30.htm)|Stance: Tiger Stance|
|[00-PMHwCrnh9W4sMu5b.htm](spell-effects/00-PMHwCrnh9W4sMu5b.htm)|Stance: Tangled Forest Stance|
|[00-qUowHpn79Dpt1hVn.htm](spell-effects/00-qUowHpn79Dpt1hVn.htm)|Stance: Dragon Stance|
|[00-RozqjLocahvQWERr.htm](spell-effects/00-RozqjLocahvQWERr.htm)|Stance: Gorilla Stance|
|[00-tPKXLtDJ3bzJcXlv.htm](spell-effects/00-tPKXLtDJ3bzJcXlv.htm)|Stance: Ironblood Stance|
|[00-uFYvW3kFP9iyNfVX.htm](spell-effects/00-uFYvW3kFP9iyNfVX.htm)|Stance: Clinging Shadows Stance|
|[01-16tOZk4qy329s2aK.htm](spell-effects/01-16tOZk4qy329s2aK.htm)|Effect: Shielding Salve|
|[01-3gGBZHcUFsHLJeQH.htm](spell-effects/01-3gGBZHcUFsHLJeQH.htm)|Effect: Elemental Blood Magic (Self)|
|[01-5Gof60StUppR2Xn9.htm](spell-effects/01-5Gof60StUppR2Xn9.htm)|Effect: Skeptic's Elixir (Lesser)|
|[01-5IGz4iheaiUWm5KR.htm](spell-effects/01-5IGz4iheaiUWm5KR.htm)|Effect: Eye of the Arclords|
|[01-6fb15XuSV4TNuVAT.htm](spell-effects/01-6fb15XuSV4TNuVAT.htm)|Effect: Hag Blood Magic|
|[01-7BFd8A9HFrmg6vwL.htm](spell-effects/01-7BFd8A9HFrmg6vwL.htm)|Effect: Psychopomp Blood Magic (Self)|
|[01-9AUcoY48H5LrVZiF.htm](spell-effects/01-9AUcoY48H5LrVZiF.htm)|Effect: Genie Blood Magic (Self)|
|[01-aKRo5TIhUtu0kyEr.htm](spell-effects/01-aKRo5TIhUtu0kyEr.htm)|Effect: Demonic Blood Magic (Self)|
|[01-dWbg2gACxMkSnZag.htm](spell-effects/01-dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Ward|
|[01-FNTTeJHiK6iOjrSq.htm](spell-effects/01-FNTTeJHiK6iOjrSq.htm)|Effect: Draconic Blood Magic|
|[01-GnWkI3T3LYRlm3X8.htm](spell-effects/01-GnWkI3T3LYRlm3X8.htm)|Spell Effect: Magic Weapon|
|[01-KVbS7AbhQdeuA0J6.htm](spell-effects/01-KVbS7AbhQdeuA0J6.htm)|Effect: Genie Blood Magic (Target)|
|[01-Lb4q2bBAgxamtix5.htm](spell-effects/01-Lb4q2bBAgxamtix5.htm)|Effect: Treat Wounds Immunity|
|[01-n1vhmOd7aNiuR3nk.htm](spell-effects/01-n1vhmOd7aNiuR3nk.htm)|Effect: Diabolic Blood Magic (Self)|
|[01-Nv70aqcQgCBpDYp8.htm](spell-effects/01-Nv70aqcQgCBpDYp8.htm)|Effect: Shadow Blood Magic (Perception)|
|[01-OqH6IaeOwRWkGPrk.htm](spell-effects/01-OqH6IaeOwRWkGPrk.htm)|Effect: Shadow Blood Magic (Stealth)|
|[01-rJpkKaPRGaH0pLse.htm](spell-effects/01-rJpkKaPRGaH0pLse.htm)|Effect: Fey Blood Magic|
|[01-ruRAfGJnik7lRavk.htm](spell-effects/01-ruRAfGJnik7lRavk.htm)|Effect: Nymph Blood Magic (Target)|
|[01-s1tulrmW6teTFjVd.htm](spell-effects/01-s1tulrmW6teTFjVd.htm)|Effect: Angelic Blood Magic|
|[01-SVGW8CLKwixFlnTv.htm](spell-effects/01-SVGW8CLKwixFlnTv.htm)|Effect: Nymph Blood Magic (Self)|
|[01-UQ7vZgmfK0VSFS8A.htm](spell-effects/01-UQ7vZgmfK0VSFS8A.htm)|Effect: Aberrant Blood Magic|
|[01-vguxP8ukwVTWWWaA.htm](spell-effects/01-vguxP8ukwVTWWWaA.htm)|Effect: Imperial Blood Magic|
|[01-yfbP64r4a9e5oyli.htm](spell-effects/01-yfbP64r4a9e5oyli.htm)|Effect: Demonic Blood Magic (Target)|
|[01-z3uyCMBddrPK5umr.htm](spell-effects/01-z3uyCMBddrPK5umr.htm)|Effect: Rage|
|[02-7UL8belWmo7U5YGM.htm](spell-effects/02-7UL8belWmo7U5YGM.htm)|Effect: Darkvision Elixir (Lesser)|
|[02-LXf1Cqi1zyo4DaLv.htm](spell-effects/02-LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|
|[02-sPCWrhUHqlbGhYSD.htm](spell-effects/02-sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|
|[03-BV8RPntjc9FUzD3g.htm](spell-effects/03-BV8RPntjc9FUzD3g.htm)|Effect: Drakeheart Mutagen (Moderate)|
|[03-fUrZ4xcMJz0CfTyG.htm](spell-effects/03-fUrZ4xcMJz0CfTyG.htm)|Effect: Juggernaut Mutagen (Moderate)|
|[03-l9HRQggofFGIxEse.htm](spell-effects/03-l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|
|[03-lLP56tbG689TNKW5.htm](spell-effects/03-lLP56tbG689TNKW5.htm)|Effect: Bracelets of Dashing|
|[03-lO95TwgihBdTilAB.htm](spell-effects/03-lO95TwgihBdTilAB.htm)|Effect: Thurible of Revelation|
|[03-OAN5Fj21PJPhIqRU.htm](spell-effects/03-OAN5Fj21PJPhIqRU.htm)|Effect: Vermin Repellent Agent (Lesser)|
|[03-PeiuJ951kkBPTCSM.htm](spell-effects/03-PeiuJ951kkBPTCSM.htm)|Effect: Bracers of Missile Deflection|
|[03-v5Ht1V4MZvRKRBjL.htm](spell-effects/03-v5Ht1V4MZvRKRBjL.htm)|Effect: Silvertongue Mutagen (Moderate)|
|[03-VPtsrpbP0AE642al.htm](spell-effects/03-VPtsrpbP0AE642al.htm)|Effect: Quicksilver Mutagen (Moderate)|
|[03-xFQRiVU6h8EA6Lw9.htm](spell-effects/03-xFQRiVU6h8EA6Lw9.htm)|Effect: Bestial Mutagen (Moderate)|
|[03-yrbz0rZzp8aZEqbv.htm](spell-effects/03-yrbz0rZzp8aZEqbv.htm)|Effect: Serene Mutagen (Moderate)|
|[03-ztxW3lBPRcesF7wK.htm](spell-effects/03-ztxW3lBPRcesF7wK.htm)|Effect: Cognitive Mutagen (Moderate)|
|[04-41WThj17MZBXTO2X.htm](spell-effects/04-41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|
|[04-4tepFOJLhZSelPoa.htm](spell-effects/04-4tepFOJLhZSelPoa.htm)|Effect: Dragon Turtle Scale|
|[04-AMhUb42NAJ1aisZp.htm](spell-effects/04-AMhUb42NAJ1aisZp.htm)|Effect: Stone Fist Elixir|
|[04-bcxVvIbuZWOvsKcA.htm](spell-effects/04-bcxVvIbuZWOvsKcA.htm)|Effect: Darkvision Elixir (Moderate)|
|[04-xVAdPzFaSvJXPMKv.htm](spell-effects/04-xVAdPzFaSvJXPMKv.htm)|Effect: Applereed Mutagen (Lesser)|
|[05-5uK3fmGlfJrbWQz4.htm](spell-effects/05-5uK3fmGlfJrbWQz4.htm)|Effect: Stalwart's ring|
|[05-88kqcDmsoAEddzUt.htm](spell-effects/05-88kqcDmsoAEddzUt.htm)|Effect: Boots of Elvenkind|
|[05-EpB7yJPEuG6ez4z3.htm](spell-effects/05-EpB7yJPEuG6ez4z3.htm)|Effect: Elixir of Life (Lesser)|
|[05-j9zVZwRBVAcnpEkE.htm](spell-effects/05-j9zVZwRBVAcnpEkE.htm)|Effect: Cheetah's Elixir (Moderate)|
|[05-MCny5ohCGf09a7Wl.htm](spell-effects/05-MCny5ohCGf09a7Wl.htm)|Effect: Salve of Slipperiness|
|[05-pAMyEbJzWBoYoGhs.htm](spell-effects/05-pAMyEbJzWBoYoGhs.htm)|Effect: Diplomat's Badge|
|[05-PEPOd38VfVzQMKG5.htm](spell-effects/05-PEPOd38VfVzQMKG5.htm)|Effect: Stone Body Mutagen (Lesser)|
|[05-RRusoN3HEGnDO1Dg.htm](spell-effects/05-RRusoN3HEGnDO1Dg.htm)|Effect: Sea Touch Elixir (Greater)|
|[05-thOpQunbQr77XWdF.htm](spell-effects/05-thOpQunbQr77XWdF.htm)|Effect: Sea Touch Elixir (Lesser)|
|[05-Wa4317cqU4lJ8vAQ.htm](spell-effects/05-Wa4317cqU4lJ8vAQ.htm)|Effect: Eagle Eye Elixir (Moderate)|
|[05-XrlChFETfe8avLsX.htm](spell-effects/05-XrlChFETfe8avLsX.htm)|Effect: Sixfingers Elixir (Lesser)|
|[06-7MgpgF8tOXOiDEwv.htm](spell-effects/06-7MgpgF8tOXOiDEwv.htm)|Effect: Vaultbreaker's Harness|
|[06-lBMhT2W2raYMa8JS.htm](spell-effects/06-lBMhT2W2raYMa8JS.htm)|Effect: Spellguard Shield|
|[06-mG6S6zm6hxaF7Tla.htm](spell-effects/06-mG6S6zm6hxaF7Tla.htm)|Effect: Skeptic's Elixir (Moderate)|
|[06-Uadsb25G18pKdZ2e.htm](spell-effects/06-Uadsb25G18pKdZ2e.htm)|Effect: Clandestine Cloak|
|[06-W3xQBLj5hLOtb6Tj.htm](spell-effects/06-W3xQBLj5hLOtb6Tj.htm)|Effect: Potion of Swimming (Moderate)|
|[06-wFF0SZs1Hcf87Kk1.htm](spell-effects/06-wFF0SZs1Hcf87Kk1.htm)|Effect: Bloodhound Mask (Moderate)|
|[07-eeGWTG9ZAha4IIOY.htm](spell-effects/07-eeGWTG9ZAha4IIOY.htm)|Effect: Cloak of Elvenkind|
|[07-W3BCLbX6j1IqL0uB.htm](spell-effects/07-W3BCLbX6j1IqL0uB.htm)|Effect: Slippers of Spider Climbing|
|[07-zlSNbMDIlTOpcO8R.htm](spell-effects/07-zlSNbMDIlTOpcO8R.htm)|Effect: Skinstitch Salve|
|[07-zqKzWGLODgIvtiKf.htm](spell-effects/07-zqKzWGLODgIvtiKf.htm)|Effect: Spellguard Blade|
|[08-3O5lvuX4VHqtpCkU.htm](spell-effects/08-3O5lvuX4VHqtpCkU.htm)|Effect: Lover's Gloves|
|[08-7vCenP9j6FuHRv5C.htm](spell-effects/08-7vCenP9j6FuHRv5C.htm)|Effect: Darkvision Elixir (Greater)|
|[08-Hnt3Trd7TiFICB06.htm](spell-effects/08-Hnt3Trd7TiFICB06.htm)|Effect: Vermin Repellent Agent (Moderate)|
|[08-Mf9EBLhYmZerf0nS.htm](spell-effects/08-Mf9EBLhYmZerf0nS.htm)|Effect: Potion of Flying (Standard)|
|[08-zd85Ny1RS46OL0TD.htm](spell-effects/08-zd85Ny1RS46OL0TD.htm)|Effect: Shrinking Potion (Greater)|
|[09-9PASRixhNM0ogqmG.htm](spell-effects/09-9PASRixhNM0ogqmG.htm)|Effect: Triton's Conch|
|[09-cg5qyeMJUh6b4fta.htm](spell-effects/09-cg5qyeMJUh6b4fta.htm)|Effect: Belt of 5 Kings (Wearer)|
|[09-E4B02mJmNexQLa8F.htm](spell-effects/09-E4B02mJmNexQLa8F.htm)|Effect: Inspiring Spotlight|
|[09-fbSFwwp60AuDDKpK.htm](spell-effects/09-fbSFwwp60AuDDKpK.htm)|Effect: Belt of the Five Kings (Allies)|
|[09-i0tm2ZHekp7rGGR3.htm](spell-effects/09-i0tm2ZHekp7rGGR3.htm)|Effect: Stole of Civility|
|[09-PeuUz7JaabCgl6Yh.htm](spell-effects/09-PeuUz7JaabCgl6Yh.htm)|Effect: Cheetah's Elixir (Greater)|
|[09-Yxssrnh9UZJAM0V7.htm](spell-effects/09-Yxssrnh9UZJAM0V7.htm)|Effect: Elixir of Life (Moderate)|
|[10-1S51uIRb9bnZtpFU.htm](spell-effects/10-1S51uIRb9bnZtpFU.htm)|Effect: Winged Boots|
|[10-G0lG7IIZnCZtYi6v.htm](spell-effects/10-G0lG7IIZnCZtYi6v.htm)|Effect: Breastplate of Command|
|[10-ioGzmVSmMGXWWBYb.htm](spell-effects/10-ioGzmVSmMGXWWBYb.htm)|Effect: Cloak of the Bat|
|[10-kgotU0sFmtAHYySB.htm](spell-effects/10-kgotU0sFmtAHYySB.htm)|Effect: Eagle Eye Elixir (Greater)|
|[10-P7Y7pO2ulZ5wBgxU.htm](spell-effects/10-P7Y7pO2ulZ5wBgxU.htm)|Effect: Barding of the Zephyr|
|[10-UlalLihKzDxcOdXL.htm](spell-effects/10-UlalLihKzDxcOdXL.htm)|Effect: Thurible of Revelation (Moderate)|
|[10-uXEp1rPU5fY4OiBf.htm](spell-effects/10-uXEp1rPU5fY4OiBf.htm)|Effect: Clandestine Cloak (Greater)|
|[10-wTZnKkT0K4Tdy8mD.htm](spell-effects/10-wTZnKkT0K4Tdy8mD.htm)|Effect: Bravo's Brew (Moderate)|
|[11-2Bds6d4UGQZqYSZM.htm](spell-effects/11-2Bds6d4UGQZqYSZM.htm)|Effect: Quicksilver Mutagen (Greater)|
|[11-jw6Tr9FbErjLAFLQ.htm](spell-effects/11-jw6Tr9FbErjLAFLQ.htm)|Effect: Serene Mutagen (Greater)|
|[11-kwD0wuW5Ndkc9YXB.htm](spell-effects/11-kwD0wuW5Ndkc9YXB.htm)|Effect: Bestial Mutagen (Greater)|
|[11-MI5OCkF9IXmD2lPF.htm](spell-effects/11-MI5OCkF9IXmD2lPF.htm)|Effect: Bloodhound Mask (Greater)|
|[11-ModBoFdCi7YQU4gP.htm](spell-effects/11-ModBoFdCi7YQU4gP.htm)|Effect: Potion of Swimming (Greater)|
|[11-oAewXfq9c0ecaSfw.htm](spell-effects/11-oAewXfq9c0ecaSfw.htm)|Effect: Silvertongue Mutagen (Greater)|
|[11-qit1mLbJUyRTYcPU.htm](spell-effects/11-qit1mLbJUyRTYcPU.htm)|Effect: Cognitive Mutagen (Greater)|
|[11-qwoLV4awdezlEJ60.htm](spell-effects/11-qwoLV4awdezlEJ60.htm)|Effect: Drakeheart Mutagen (Greater)|
|[11-qzRcSQ0HTTp58hV2.htm](spell-effects/11-qzRcSQ0HTTp58hV2.htm)|Effect: Sixfingers Elixir (Moderate)|
|[11-TsWUTODTVi487SEz.htm](spell-effects/11-TsWUTODTVi487SEz.htm)|Effect: Skeptic's Elixir (Greater)|
|[11-xLilBqqf34ZJYO9i.htm](spell-effects/11-xLilBqqf34ZJYO9i.htm)|Effect: Juggernaut Mutagen (Greater)|
|[12-1xHHvQlW4pRR89qj.htm](spell-effects/12-1xHHvQlW4pRR89qj.htm)|Effect: Stone Body Mutagen (Moderate)|
|[12-9keegq0GdS1eSrNr.htm](spell-effects/12-9keegq0GdS1eSrNr.htm)|Effect: Sea Touch Elixir (Moderate)|
|[12-fYjvLx9DHIdCHdDx.htm](spell-effects/12-fYjvLx9DHIdCHdDx.htm)|Effect: Applereed Mutagen (Moderate)|
|[12-p2aGtovaY1feytws.htm](spell-effects/12-p2aGtovaY1feytws.htm)|Effect: Aeon Stone (Black Pearl)|
|[12-viCX9fZzTWGuoO85.htm](spell-effects/12-viCX9fZzTWGuoO85.htm)|Effect: Cloak of Elvenkind (Greater)|
|[12-VrYfR2WuyA15zFhq.htm](spell-effects/12-VrYfR2WuyA15zFhq.htm)|Effect: Vermin Repellent Agent (Greater)|
|[13-gDefAEEMXVVZgqXH.htm](spell-effects/13-gDefAEEMXVVZgqXH.htm)|Effect: Celestial Armor|
|[13-Z9oPh462q82IYIZ6.htm](spell-effects/13-Z9oPh462q82IYIZ6.htm)|Effect: Elixir of Life (Greater)|
|[14-EpNflrkmWzQ0lEb4.htm](spell-effects/14-EpNflrkmWzQ0lEb4.htm)|Effect: Glaive of the Artist|
|[15-b9DTIJyBT8kvIBpj.htm](spell-effects/15-b9DTIJyBT8kvIBpj.htm)|Effect: Stone Body Mutagen (Greater)|
|[15-PpLxndUSgzgs6dd0.htm](spell-effects/15-PpLxndUSgzgs6dd0.htm)|Effect: Elixir of Life (Major)|
|[15-q1EhQ716bPSgJVnC.htm](spell-effects/15-q1EhQ716bPSgJVnC.htm)|Effect: Bravo's Brew (Greater)|
|[15-Zdh2uO1vVYJmaqld.htm](spell-effects/15-Zdh2uO1vVYJmaqld.htm)|Effect: Potion of Flying (Greater)|
|[16-e6dXfbKzv5sNr1zh.htm](spell-effects/16-e6dXfbKzv5sNr1zh.htm)|Effect: Vermin Repellent Agent (Major)|
|[16-etJW0w4CiSFgMrWP.htm](spell-effects/16-etJW0w4CiSFgMrWP.htm)|Effect: Aeon Stone (Orange Prism) (Nature)|
|[16-FbFl95WRpzrrijh3.htm](spell-effects/16-FbFl95WRpzrrijh3.htm)|Effect: Aeon Stone (Orange Prism) (Religion)|
|[16-vOU4Yv2MyAfYBbmF.htm](spell-effects/16-vOU4Yv2MyAfYBbmF.htm)|Effect: Aeon Stone (Orange Prism) (Occultism)|
|[16-wyLEew86nhNUXASu.htm](spell-effects/16-wyLEew86nhNUXASu.htm)|Effect: Eagle Eye Elixir (Major)|
|[16-yvabfuAO74pvH8hh.htm](spell-effects/16-yvabfuAO74pvH8hh.htm)|Effect: Aeon Stone (Orange Prism) (Arcana)|
|[17-1ouUo8lLK6H79Rqh.htm](spell-effects/17-1ouUo8lLK6H79Rqh.htm)|Effect: Bestial Mutagen (Major)|
|[17-2PNo8u4wxSbz5WEs.htm](spell-effects/17-2PNo8u4wxSbz5WEs.htm)|Effect: Juggernaut Mutagen (Major)|
|[17-988f6NpOo4YzFzIr.htm](spell-effects/17-988f6NpOo4YzFzIr.htm)|Effect: Quicksilver Mutagen (Major)|
|[17-9FfFhu2kl2wMTsiI.htm](spell-effects/17-9FfFhu2kl2wMTsiI.htm)|Effect: Silvertongue Mutagen (Major)|
|[17-iK6JeCsZwm5Vakks.htm](spell-effects/17-iK6JeCsZwm5Vakks.htm)|Effect: Anklets of Alacrity|
|[17-RT1BxXrbbGgk40Ti.htm](spell-effects/17-RT1BxXrbbGgk40Ti.htm)|Effect: Cognitive Mutagen (Major)|
|[17-t7VUJHSUT6bkVUjg.htm](spell-effects/17-t7VUJHSUT6bkVUjg.htm)|Effect: Serene Mutagen (Major)|
|[17-TkRuKKYyPHTGPfgf.htm](spell-effects/17-TkRuKKYyPHTGPfgf.htm)|Effect: Sixfingers Elixir (Greater)|
|[17-vFOr2JAJxiVvvn2E.htm](spell-effects/17-vFOr2JAJxiVvvn2E.htm)|Effect: Drakeheart Mutagen (Major)|
|[18-kwOtHtmlH69ctK0O.htm](spell-effects/18-kwOtHtmlH69ctK0O.htm)|Effect: Sun Orchid Poultice|
|[18-QapoFh0tbUgMwSIB.htm](spell-effects/18-QapoFh0tbUgMwSIB.htm)|Effect: Thurible of Revelation (Greater)|
|[18-tTBJ33UGtzXjWOJp.htm](spell-effects/18-tTBJ33UGtzXjWOJp.htm)|Effect: Applereed Mutagen (Greater)|
|[19-9MeHc072G4L8AJkp.htm](spell-effects/19-9MeHc072G4L8AJkp.htm)|Effect: Elixir of Life (True)|
|[20-ah41XCrV4LFsVyzl.htm](spell-effects/20-ah41XCrV4LFsVyzl.htm)|Effect: Shield of the Unified Legion|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00-7dLsA9PAb5ij7Bc6.htm](spell-effects/00-7dLsA9PAb5ij7Bc6.htm)|Effect: Dueling Cape|Effet : Cape de duel|libre|
|[00-8ersuvNJXX00XaIQ.htm](spell-effects/00-8ersuvNJXX00XaIQ.htm)|Effect: Euryale (Curse) Card|Effet : Carte Euryale (malédiction)|changé|
|[00-eSIYyxi6uTKiP6W5.htm](spell-effects/00-eSIYyxi6uTKiP6W5.htm)|Effect: Improvised Weapon|Effet : arme improvisée|libre|
|[00-NE7Fm5YnUhD4ySX3.htm](spell-effects/00-NE7Fm5YnUhD4ySX3.htm)|Effect: Earplugs|Bouchons d'oreilles|libre|
|[00-Zb8RYgmzCI6fQE0o.htm](spell-effects/00-Zb8RYgmzCI6fQE0o.htm)|Effect: Throne Card|Effet: Carte Trône|changé|
|[01-1l139A2Qik4lBHKO.htm](spell-effects/01-1l139A2Qik4lBHKO.htm)|Effect: Juggernaut Mutagen (Lesser)|Effet : Mutagène de Juggernaut (Inférieur)|libre|
|[01-1nCwQErK6hpkNvfw.htm](spell-effects/01-1nCwQErK6hpkNvfw.htm)|Effect: Dueling Parry|Effet : Parade en duel|libre|
|[01-3qHKBDF7lrHw8jFK.htm](spell-effects/01-3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|Effet : Assistance divine|libre|
|[01-5xgapIXn5DwbXHKh.htm](spell-effects/01-5xgapIXn5DwbXHKh.htm)|Effect: Serene Mutagen (Lesser)|Effet : Mutagène de sérénité (Inférieur)|libre|
|[01-6A8jsLR7upLGuRiv.htm](spell-effects/01-6A8jsLR7upLGuRiv.htm)|Effect: Lastwall Soup|Effet : Soupe de Dernier-Rempart|libre|
|[01-6PNLBIdlqqWNCFMy.htm](spell-effects/01-6PNLBIdlqqWNCFMy.htm)|Effect: Quicksilver Mutagen (Lesser)|Effet : Mutagène vif-argent|changé|
|[01-7z1iY4AaNEAIKuAU.htm](spell-effects/01-7z1iY4AaNEAIKuAU.htm)|Effect: Antidote (Lesser)|Effet : Antidote (inférieur)|libre|
|[01-a3hqeAXpmMZgMWuB.htm](spell-effects/01-a3hqeAXpmMZgMWuB.htm)|Effect: Darkvision Elixir|Effet : Élixir de vision dans le noir|libre|
|[01-beReeFroAx24hj83.htm](spell-effects/01-beReeFroAx24hj83.htm)|Spell Effect: Inspire Courage|Effet : Inspiration Vaillante|libre|
|[01-dpIrjd1UPY7EnWUD.htm](spell-effects/01-dpIrjd1UPY7EnWUD.htm)|Effect: Silvertongue Mutagen (Lesser)|Effet : Mutagène de langue dorée (Inférieur)|libre|
|[01-fIpzDpuwLdIS4tW5.htm](spell-effects/01-fIpzDpuwLdIS4tW5.htm)|Effect: Bestial Mutagen (Lesser)|Effet : Mutagène bestial (Inférieur)|changé|
|[01-GBBjw61g4ekJymT0.htm](spell-effects/01-GBBjw61g4ekJymT0.htm)|Effect: Drakeheart Mutagen (Lesser)|Effet : Mutagène cœur de drake (Inférieur)|libre|
|[01-jaBMZKdoywOTrQvP.htm](spell-effects/01-jaBMZKdoywOTrQvP.htm)|Effect: Cognitive Mutagen (Lesser)|Effet : Mutagène cognitif (Inférieur)|changé|
|[01-Jemq5UknGdMO7b73.htm](spell-effects/01-Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|Effet : Bouclier|libre|
|[01-kkDbalYEavzRpYlp.htm](spell-effects/01-kkDbalYEavzRpYlp.htm)|Effect: Antiplague (Lesser)|Effet : Antimaladie (Inférieur)|libre|
|[01-lNWACCNe9RYgaFxb.htm](spell-effects/01-lNWACCNe9RYgaFxb.htm)|Effect: Cheetah's Elixir (Lesser)|Effet : Élixir du Guépard (Inférieur)|libre|
|[01-lPRuIRbu0rHBkoKY.htm](spell-effects/01-lPRuIRbu0rHBkoKY.htm)|Effect: Elixir of Life (Minor)|Effet : Élixir de vie (Mineur)|libre|
|[01-qkwb5DD3zmKwvbk0.htm](spell-effects/01-qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mage Armor|Effet : Armure du mage|libre|
|[01-uBJsxCzNhje8m8jj.htm](spell-effects/01-uBJsxCzNhje8m8jj.htm)|Effect: Panache|Effet : Panache|libre|
|[01-VCypzSu659eC6jNi.htm](spell-effects/01-VCypzSu659eC6jNi.htm)|Effect: Eagle Eye Elixir (Lesser)|Effet : Élixir d'œil du faucon (Inférieur)|libre|
|[02-2C1HuKDQDGFZuv7l.htm](spell-effects/02-2C1HuKDQDGFZuv7l.htm)|Effect: Boulderhead Bock|Effet : Bock têtederoc|libre|
|[02-Cxa7MdgMCUoMqbKm.htm](spell-effects/02-Cxa7MdgMCUoMqbKm.htm)|Effect: Bronze Bull Pendant|Effet : Pendentif de taureau de bronze|libre|
|[02-eh7EqmDBDW30ShCu.htm](spell-effects/02-eh7EqmDBDW30ShCu.htm)|Effect: Bravo's Brew (Lesser)|Effet : Breuvage de bravoure (Inférieur)|libre|
|[02-iEkH8BKLMUa2wxLX.htm](spell-effects/02-iEkH8BKLMUa2wxLX.htm)|Effect: Glamorous Buckler|Effet : Targe étincelante|libre|
|[02-S4MZzALqFoXJsr6o.htm](spell-effects/02-S4MZzALqFoXJsr6o.htm)|Effect: Bloodhound Mask (Lesser)|Effet : Masque du limier (Inférieur)|libre|
|[03-bP40jr6wE6MCsRvY.htm](spell-effects/03-bP40jr6wE6MCsRvY.htm)|Effect: Golden Legion Epaulet|Effet : Épaulette de la légion dorée|libre|
|[06-HeRHBo2NaKy5IxhU.htm](spell-effects/06-HeRHBo2NaKy5IxhU.htm)|Effect: Antiplague (Moderate)|Effet : Antimaladie (Modéré)|libre|
|[06-mi4Md1fB2XThCand.htm](spell-effects/06-mi4Md1fB2XThCand.htm)|Effect: Antidote (Moderate)|Effet : Antidote (Modéré)|libre|
|[10-QuZ5frBMJF3gi7RY.htm](spell-effects/10-QuZ5frBMJF3gi7RY.htm)|Effect: Antidote (Greater)|Effet : Antidote (Supérieur)|libre|
|[10-R106i7WCXvHLGMTu.htm](spell-effects/10-R106i7WCXvHLGMTu.htm)|Effect: Antiplague (Greater)|Effet : Antimaladie (Supérieur)|libre|
|[14-1mKjaWC65KWPuFR4.htm](spell-effects/14-1mKjaWC65KWPuFR4.htm)|Effect: Antidote (Major)|Effet : Antidote (Majeur)|libre|
|[14-qVKrrKpTghgMIuGH.htm](spell-effects/14-qVKrrKpTghgMIuGH.htm)|Effect: Antiplague (Major)|Effet : Antimaladie (Majeur)|libre|
|[Gqy7K6FnbLtwGpud.htm](spell-effects/Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|Effet de sort : Bénédiction|libre|
