# Bibliothèque



Dernière mise à jour: 2021-02-07 20:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!

 * [Actions](#actions)
 * [Aptitudes des familiers](#aptitudes-des-familiers)
 * [Aptitudes du bestiaire](#aptitudes-du-bestiaire)
 * [Archétypes](#archétypes)
 * [Ascendances](#ascendances)
 * [Ascendances (aptitudes)](#ascendances-(aptitudes))
 * [Backgrounds](#backgrounds)
 * [Bestiaire](#bestiaire)
 * [Bestiaire 2](#bestiaire-2)
 * [Bénédictions et malédictions](#bénédictions-et-malédictions)
 * [Capacités de classe](#capacités-de-classe)
 * [Classes](#classes)
 * [Conditions](#conditions)
 * [Dangers](#dangers)
 * [Dons](#dons)
 * [Effets des sorts](#effets-des-sorts)
 * [Guide du MJ](#guide-du-mj)
 * [Macros PF2e](#macros-pf2e)
 * [Sorts](#sorts)
 * [Équipement](#équipement)


## Actions

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Absorb into the Aegis](actions/S9PZFOVe7zhORkUc.htm)|Absorber dans l'égide|`@Compendium[pf2e.actionspf2e.S9PZFOVe7zhORkUc]`|
|[Administer First Aid](actions/MHLuKy4nQO2Z4Am1.htm)|Prodiguer les premiers soins|`@Compendium[pf2e.actionspf2e.MHLuKy4nQO2Z4Am1]`|
|[Affix a Talisman](actions/UAaQk93a30nx0nYY.htm)|Fixer un talisman|`@Compendium[pf2e.actionspf2e.UAaQk93a30nx0nYY]`|
|[Aid](actions/HCl3pzVefiv9ZKQW.htm)|Aider|`@Compendium[pf2e.actionspf2e.HCl3pzVefiv9ZKQW]`|
|[Arrest a Fall](actions/qm7xptMSozAinnPS.htm)|Arrêter une chute|`@Compendium[pf2e.actionspf2e.qm7xptMSozAinnPS]`|
|[Avert Gaze](actions/UWdRX1VelipCzrCg.htm)|Détourner le regard|`@Compendium[pf2e.actionspf2e.UWdRX1VelipCzrCg]`|
|[Avoid Notice](actions/IE2nThCmoyhQA0Jn.htm)|Échapper aux regards|`@Compendium[pf2e.actionspf2e.IE2nThCmoyhQA0Jn]`|
|[Balance](actions/M76ycLAqHoAgbcej.htm)|Garder l'équilibre|`@Compendium[pf2e.actionspf2e.M76ycLAqHoAgbcej]`|
|[Basic Finisher](actions/dCuvfq3r2K9wXY9g.htm)|Coup final de base|`@Compendium[pf2e.actionspf2e.dCuvfq3r2K9wXY9g]`|
|[Boarding Assault](actions/vZltxwRNvF5khf9a.htm)|Assaut d'abordage|`@Compendium[pf2e.actionspf2e.vZltxwRNvF5khf9a]`|
|[Borrow an Arcane Spell](actions/OizxuPb44g3eHPFh.htm)|Emprunter un sort arcanique|`@Compendium[pf2e.actionspf2e.OizxuPb44g3eHPFh]`|
|[Burrow](actions/H6v1VgowHaKHnVlG.htm)|Creuser|`@Compendium[pf2e.actionspf2e.H6v1VgowHaKHnVlG]`|
|[Call Companion](actions/8kGpUUUSX1sB2OqQ.htm)|Appeler un compagnon|`@Compendium[pf2e.actionspf2e.8kGpUUUSX1sB2OqQ]`|
|[Call Upon the Brightness](actions/mk6rzaAzsBBRGJnh.htm)|Faire appel à la lumière|`@Compendium[pf2e.actionspf2e.mk6rzaAzsBBRGJnh]`|
|[Call on Ancient Blood](actions/8w6esW689NNbbq3i.htm)|Appel du sang ancien|`@Compendium[pf2e.actionspf2e.8w6esW689NNbbq3i]`|
|[Cast a Spell](actions/aBQ8ajvEBByv45yz.htm)|Lancer un sort|`@Compendium[pf2e.actionspf2e.aBQ8ajvEBByv45yz]`|
|[Climb](actions/pprgrYQ1QnIDGZiy.htm)|Escalader|`@Compendium[pf2e.actionspf2e.pprgrYQ1QnIDGZiy]`|
|[Clue In](actions/25WDi1cVUrW92sUj.htm)|Partager les indices|`@Compendium[pf2e.actionspf2e.25WDi1cVUrW92sUj]`|
|[Coerce](actions/tHCqgwjtQtzNqVvd.htm)|Contraindre|`@Compendium[pf2e.actionspf2e.tHCqgwjtQtzNqVvd]`|
|[Command an Animal](actions/q9nbyIF0PEBqMtYe.htm)|Diriger un animal|`@Compendium[pf2e.actionspf2e.q9nbyIF0PEBqMtYe]`|
|[Conceal an Object](actions/qVNVSmsgpKFGk9hV.htm)|Dissimuler un objet|`@Compendium[pf2e.actionspf2e.qVNVSmsgpKFGk9hV]`|
|[Confident Finisher](actions/K878asDgf1EF0B9S.htm)|Coup final assuré|`@Compendium[pf2e.actionspf2e.K878asDgf1EF0B9S]`|
|[Cover Tracks](actions/SB7cMECVtE06kByk.htm)|Dissimuler des traces|`@Compendium[pf2e.actionspf2e.SB7cMECVtE06kByk]`|
|[Craft](actions/rmwa3OyhTZ2i2AHl.htm)|Fabriquer|`@Compendium[pf2e.actionspf2e.rmwa3OyhTZ2i2AHl]`|
|[Crawl](actions/Tj055UcNm6UEgtCg.htm)|Ramper|`@Compendium[pf2e.actionspf2e.Tj055UcNm6UEgtCg]`|
|[Create Forgery](actions/ftG89SjTSa9DYDOD.htm)|Contrefaire|`@Compendium[pf2e.actionspf2e.ftG89SjTSa9DYDOD]`|
|[Create a Diversion](actions/GkmbTGfg8KcgynOA.htm)|Faire diversion|`@Compendium[pf2e.actionspf2e.GkmbTGfg8KcgynOA]`|
|[Debilitating Strike](actions/bp0Up04x3dzGK5bB.htm)|Frappe incapacitante|`@Compendium[pf2e.actionspf2e.bp0Up04x3dzGK5bB]`|
|[Decipher Writing](actions/d9gbpiQjChYDYA2L.htm)|Déchiffrer un texte|`@Compendium[pf2e.actionspf2e.d9gbpiQjChYDYA2L]`|
|[Defend](actions/cYtYKa1gDEl7y2N0.htm)|Défendre|`@Compendium[pf2e.actionspf2e.cYtYKa1gDEl7y2N0]`|
|[Delay](actions/A72nHGUtNXgY5Ey9.htm)|Retarder|`@Compendium[pf2e.actionspf2e.A72nHGUtNXgY5Ey9]`|
|[Demoralize](actions/2u915NdUyQan6uKF.htm)|Démoraliser|`@Compendium[pf2e.actionspf2e.2u915NdUyQan6uKF]`|
|[Destructive Vengeance](actions/r5Uth6yvCoE4tr9z.htm)|Vengeance destructrice|`@Compendium[pf2e.actionspf2e.r5Uth6yvCoE4tr9z]`|
|[Detect Magic](actions/Yb0C1uLzeHrVLl7a.htm)|Détection de la magie|`@Compendium[pf2e.actionspf2e.Yb0C1uLzeHrVLl7a]`|
|[Devise a Stratagem](actions/m0f2B7G9eaaTmhFL.htm)|Concevoir un stratagème|`@Compendium[pf2e.actionspf2e.m0f2B7G9eaaTmhFL]`|
|[Disable Device](actions/cYdz2grcOcRt4jk6.htm)|Désamorcer un dispositif|`@Compendium[pf2e.actionspf2e.cYdz2grcOcRt4jk6]`|
|[Disarm](actions/Dt6B1slsBy8ipJu9.htm)|Désarmer|`@Compendium[pf2e.actionspf2e.Dt6B1slsBy8ipJu9]`|
|[Dismiss](actions/AjLSHZSWQ90exdLo.htm)|Révoquer|`@Compendium[pf2e.actionspf2e.AjLSHZSWQ90exdLo]`|
|[Drain Bonded Item](actions/v82XtjAVN4ffgVVR.htm)|Drain d'objet lié|`@Compendium[pf2e.actionspf2e.v82XtjAVN4ffgVVR]`|
|[Drop Prone](actions/HYNhdaPtF1QmQbR3.htm)|Se jeter à terre|`@Compendium[pf2e.actionspf2e.HYNhdaPtF1QmQbR3]`|
|[Earn Income](actions/QyzlsLrqM0EEwd7j.htm)|Gagner de l'argent|`@Compendium[pf2e.actionspf2e.QyzlsLrqM0EEwd7j]`|
|[Eldritch Shot](actions/DS9sDOWkXrz2xmHi.htm)|Tir mystique|`@Compendium[pf2e.actionspf2e.DS9sDOWkXrz2xmHi]`|
|[Encouraging Words](actions/e2ePMDa7ixbLRryj.htm)|Paroles encourageantes|`@Compendium[pf2e.actionspf2e.e2ePMDa7ixbLRryj]`|
|[Energy Emanation](actions/7qjfYsLNTr17Aftf.htm)|Émanation énergétique|`@Compendium[pf2e.actionspf2e.7qjfYsLNTr17Aftf]`|
|[Escape](actions/SkZAQRkLLkmBQNB9.htm)|S'échapper|`@Compendium[pf2e.actionspf2e.SkZAQRkLLkmBQNB9]`|
|[Exhale Poison](actions/GAoUxmcKx1vW3vEB.htm)|Expirer le poison|`@Compendium[pf2e.actionspf2e.GAoUxmcKx1vW3vEB]`|
|[Expeditious Inspection](actions/jftNJjBNxp2cleoi.htm)|Inspection expéditive|`@Compendium[pf2e.actionspf2e.jftNJjBNxp2cleoi]`|
|[Feint](actions/QNAVeNKtHA0EUw4X.htm)|Feinter|`@Compendium[pf2e.actionspf2e.QNAVeNKtHA0EUw4X]`|
|[Fey's Fortune](actions/TMBXArwICQRJdwT6.htm)|Chance féérique|`@Compendium[pf2e.actionspf2e.TMBXArwICQRJdwT6]`|
|[Final Surge](actions/EHa0owz6mccnmSBf.htm)|Accélération finale|`@Compendium[pf2e.actionspf2e.EHa0owz6mccnmSBf]`|
|[Fire in the Hole](actions/UEkGL7uAGYDPFNfK.htm)|Mise à feu !|`@Compendium[pf2e.actionspf2e.UEkGL7uAGYDPFNfK]`|
|[Flurry of Blows](actions/nbfNETdpee8CVM17.htm)|Déluge de coups|`@Compendium[pf2e.actionspf2e.nbfNETdpee8CVM17]`|
|[Fly](actions/cS9nfDRGD83bNU1p.htm)|Voler|`@Compendium[pf2e.actionspf2e.cS9nfDRGD83bNU1p]`|
|[Follow the Expert](actions/tfa4Sh7wcxCEqL29.htm)|Suivre l'expert|`@Compendium[pf2e.actionspf2e.tfa4Sh7wcxCEqL29]`|
|[Force Open](actions/SjmKHgI7a5Z9JzBx.htm)|Ouvrir de force|`@Compendium[pf2e.actionspf2e.SjmKHgI7a5Z9JzBx]`|
|[Foresight](actions/VOEWhPQfN3lvHivK.htm)|Prémonition|`@Compendium[pf2e.actionspf2e.VOEWhPQfN3lvHivK]`|
|[Gather Information](actions/plBGdZhqq5JBl1D8.htm)|Recueillir des informations|`@Compendium[pf2e.actionspf2e.plBGdZhqq5JBl1D8]`|
|[Glimpse of Redemption](actions/tuZnRWHixLArvaIf.htm)|Lueur de rédemption|`@Compendium[pf2e.actionspf2e.tuZnRWHixLArvaIf]`|
|[Grab an Edge](actions/3yoajuKjwHZ9ApUY.htm)|Se raccrocher in extremis|`@Compendium[pf2e.actionspf2e.3yoajuKjwHZ9ApUY]`|
|[Grapple](actions/PMbdMWc2QroouFGD.htm)|Saisir|`@Compendium[pf2e.actionspf2e.PMbdMWc2QroouFGD]`|
|[Hide](actions/XMcnh4cSI32tljXa.htm)|Se cacher|`@Compendium[pf2e.actionspf2e.XMcnh4cSI32tljXa]`|
|[High Jump](actions/2HJ4yuEFY1Cast4h.htm)|Sauter en hauteur|`@Compendium[pf2e.actionspf2e.2HJ4yuEFY1Cast4h]`|
|[Hunt Prey](actions/JYi4MnsdFu618hPm.htm)|Chasser une proie|`@Compendium[pf2e.actionspf2e.JYi4MnsdFu618hPm]`|
|[Hustle](actions/JuqmIAnkL9hVGai8.htm)|S'empresser|`@Compendium[pf2e.actionspf2e.JuqmIAnkL9hVGai8]`|
|[Identify Alchemy](actions/Q4kdWVOf2ztIBFg1.htm)|Identifier l'alchimie|`@Compendium[pf2e.actionspf2e.Q4kdWVOf2ztIBFg1]`|
|[Identify Magic](actions/eReSHVEPCsdkSL4G.htm)|Identifier la magie|`@Compendium[pf2e.actionspf2e.eReSHVEPCsdkSL4G]`|
|[Impersonate](actions/AJstokjdG6iDjVjE.htm)|Se déguiser|`@Compendium[pf2e.actionspf2e.AJstokjdG6iDjVjE]`|
|[Instinctive Obfuscation](actions/rSpCV0leurp2Bg2d.htm)|Obscurcissement instinctif|`@Compendium[pf2e.actionspf2e.rSpCV0leurp2Bg2d]`|
|[Interact](actions/pvQ5rY2zrtPI614F.htm)|Interagir|`@Compendium[pf2e.actionspf2e.pvQ5rY2zrtPI614F]`|
|[Invest an Item](actions/lvqPQDdWT2DDO0k2.htm)|Investir un objet|`@Compendium[pf2e.actionspf2e.lvqPQDdWT2DDO0k2]`|
|[Investigate](actions/EwgTZBWsc8qKaViP.htm)|Enquêter|`@Compendium[pf2e.actionspf2e.EwgTZBWsc8qKaViP]`|
|[Iron Command](actions/M8RCbthRhB4bxO9t.htm)|Ordre de fer|`@Compendium[pf2e.actionspf2e.M8RCbthRhB4bxO9t]`|
|[Jinx](actions/hFRHPBj6wjAayNtW.htm)|Maudire|`@Compendium[pf2e.actionspf2e.hFRHPBj6wjAayNtW]`|
|[Leap](actions/d5I6018Mci2SWokk.htm)|Bondir|`@Compendium[pf2e.actionspf2e.d5I6018Mci2SWokk]`|
|[Learn a Spell](actions/Q5iIYCFdqJFM31GW.htm)|Apprendre un sort|`@Compendium[pf2e.actionspf2e.Q5iIYCFdqJFM31GW]`|
|[Liberating Step](actions/IX1VlVCL5sFTptEE.htm)|Pas libérateur|`@Compendium[pf2e.actionspf2e.IX1VlVCL5sFTptEE]`|
|[Lie](actions/ewwCglB7XOPLUz72.htm)|Mentir|`@Compendium[pf2e.actionspf2e.ewwCglB7XOPLUz72]`|
|[Long Jump](actions/JUvAvruz7yRQXfz2.htm)|Sauter en longueur|`@Compendium[pf2e.actionspf2e.JUvAvruz7yRQXfz2]`|
|[Long Term Rest](actions/DXIZ4DHGxhZiWNWb.htm)|Repos long|`@Compendium[pf2e.actionspf2e.DXIZ4DHGxhZiWNWb]`|
|[Make an Impression](actions/OX4fy22hQgUHDr0q.htm)|Faire bonne impression|`@Compendium[pf2e.actionspf2e.OX4fy22hQgUHDr0q]`|
|[Maneuver in Flight](actions/Qf1ylAbdVi1rkc8M.htm)|Manœuvrer en vol|`@Compendium[pf2e.actionspf2e.Qf1ylAbdVi1rkc8M]`|
|[Mark for Death](actions/orjJjLdm4XNAcFi8.htm)|Marquer pour mort|`@Compendium[pf2e.actionspf2e.orjJjLdm4XNAcFi8]`|
|[Master Strike](actions/Rlp7ND33yYfxiEWi.htm)|Frappe de maître|`@Compendium[pf2e.actionspf2e.Rlp7ND33yYfxiEWi]`|
|[Mesmerizing Performance](actions/CrUPaPlsxy2bswaT.htm)|Performance hypnotisante|`@Compendium[pf2e.actionspf2e.CrUPaPlsxy2bswaT]`|
|[Mighty Rage](actions/74iat04PtfG8gn2Q.htm)|Rage formidable|`@Compendium[pf2e.actionspf2e.74iat04PtfG8gn2Q]`|
|[Mount](actions/PM5jvValFkbFH3TV.htm)|Se mettre en selle|`@Compendium[pf2e.actionspf2e.PM5jvValFkbFH3TV]`|
|[Mutagenic Flashback](actions/00LNVSCbwJ8pszwE.htm)|Flashback du mutagène|`@Compendium[pf2e.actionspf2e.00LNVSCbwJ8pszwE]`|
|[Opportune Riposte](actions/EfjoIuDmtUn4yiow.htm)|Riposte opportune|`@Compendium[pf2e.actionspf2e.EfjoIuDmtUn4yiow]`|
|[Palm an Object](actions/ijZ0DDFpMkWqaShd.htm)|Escamoter un objet|`@Compendium[pf2e.actionspf2e.ijZ0DDFpMkWqaShd]`|
|[Perform](actions/EEDElIyin4z60PXx.htm)|Se produire|`@Compendium[pf2e.actionspf2e.EEDElIyin4z60PXx]`|
|[Pick a Lock](actions/2EE4aF4SZpYf0R6H.htm)|Crocheter une serrure|`@Compendium[pf2e.actionspf2e.2EE4aF4SZpYf0R6H]`|
|[Point Out](actions/sn2hIy1iIJX9Vpgj.htm)|Signaler|`@Compendium[pf2e.actionspf2e.sn2hIy1iIJX9Vpgj]`|
|[Pointed Question](actions/xccOiNL2W1EtfUYl.htm)|Question imparable|`@Compendium[pf2e.actionspf2e.xccOiNL2W1EtfUYl]`|
|[Pursue a Lead](actions/xTK2zsWFyxSJvYbX.htm)|Suivre une piste|`@Compendium[pf2e.actionspf2e.xTK2zsWFyxSJvYbX]`|
|[Quick Alchemy](actions/yzNJgwzV9XqEhKc6.htm)|Alchimie rapide|`@Compendium[pf2e.actionspf2e.yzNJgwzV9XqEhKc6]`|
|[Quick Tincture](actions/QHFMeJGzFWj2QczA.htm)|Préparation rapide|`@Compendium[pf2e.actionspf2e.QHFMeJGzFWj2QczA]`|
|[Rage](actions/Ah5g9pDwWF9b9VW9.htm)|Rage|`@Compendium[pf2e.actionspf2e.Ah5g9pDwWF9b9VW9]`|
|[Raise a Shield](actions/xjGwis0uaC2305pm.htm)|Lever un bouclier|`@Compendium[pf2e.actionspf2e.xjGwis0uaC2305pm]`|
|[Rally](actions/xJEkXFJgEfEida27.htm)|Rallier|`@Compendium[pf2e.actionspf2e.xJEkXFJgEfEida27]`|
|[Rampaging Ferocity](actions/FkfWKq9jhhPzKAbb.htm)|Férocité déchaînée|`@Compendium[pf2e.actionspf2e.FkfWKq9jhhPzKAbb]`|
|[Ready](actions/dLgAMt3TbkmLkUqE.htm)|Préparer|`@Compendium[pf2e.actionspf2e.dLgAMt3TbkmLkUqE]`|
|[Recall Knowledge (Arcana)](actions/KygTSeDvsFoSO6HW.htm)|Se souvenir (Arcanes)|`@Compendium[pf2e.actionspf2e.KygTSeDvsFoSO6HW]`|
|[Recall Knowledge (Crafting)](actions/B0Eu3EfwIa9kyDEA.htm)|Se souvenir (Artisanat)|`@Compendium[pf2e.actionspf2e.B0Eu3EfwIa9kyDEA]`|
|[Recall Knowledge (Lore)](actions/1OagaWtBpVXExToo.htm)|Se souvenir (Connaissance)|`@Compendium[pf2e.actionspf2e.1OagaWtBpVXExToo]`|
|[Recall Knowledge (Medicine)](actions/SeUolRoPzorFUAaI.htm)|Se souvenir (Médecine)|`@Compendium[pf2e.actionspf2e.SeUolRoPzorFUAaI]`|
|[Recall Knowledge (Nature)](actions/eT1jXYvz2YH70Ovp.htm)|Se souvenir (Nature)|`@Compendium[pf2e.actionspf2e.eT1jXYvz2YH70Ovp]`|
|[Recall Knowledge (Occultism)](actions/B2BpIZFHoF9Kjzpx.htm)|Se souvenir (Occultisme)|`@Compendium[pf2e.actionspf2e.B2BpIZFHoF9Kjzpx]`|
|[Recall Knowledge (Religion)](actions/LZgjpWd0pL3vK9Q1.htm)|Se souvenir (Religion)|`@Compendium[pf2e.actionspf2e.LZgjpWd0pL3vK9Q1]`|
|[Recall Knowledge (Society)](actions/KUfLlXDWTcAWhl8l.htm)|Se souvenir (Société)|`@Compendium[pf2e.actionspf2e.KUfLlXDWTcAWhl8l]`|
|[Refocus](actions/OSefkMgojBLqmRDh.htm)|Refocaliser|`@Compendium[pf2e.actionspf2e.OSefkMgojBLqmRDh]`|
|[Release](actions/wQYmDStjdjn0I26t.htm)|Relâcher|`@Compendium[pf2e.actionspf2e.wQYmDStjdjn0I26t]`|
|[Repair](actions/bT3skovyLUtP22ME.htm)|Réparer|`@Compendium[pf2e.actionspf2e.bT3skovyLUtP22ME]`|
|[Repeat a Spell](actions/OQaFzDtVEOMWizJJ.htm)|Répéter un sort|`@Compendium[pf2e.actionspf2e.OQaFzDtVEOMWizJJ]`|
|[Request](actions/DCb62iCBrJXy0Ik6.htm)|Solliciter|`@Compendium[pf2e.actionspf2e.DCb62iCBrJXy0Ik6]`|
|[Resist Elf Magic](actions/On5CQjX4euWqToly.htm)|Résister à la magie elfique|`@Compendium[pf2e.actionspf2e.On5CQjX4euWqToly]`|
|[Retraining](actions/24PSsn1SKpUwYA1X.htm)|Réapprentissage|`@Compendium[pf2e.actionspf2e.24PSsn1SKpUwYA1X]`|
|[Retributive Strike](actions/EAP98XaChJEbgKcK.htm)|Frappe punitive|`@Compendium[pf2e.actionspf2e.EAP98XaChJEbgKcK]`|
|[Scout](actions/kV3XM0YJeS2KCSOb.htm)|Reconnaître|`@Compendium[pf2e.actionspf2e.kV3XM0YJeS2KCSOb]`|
|[Search](actions/TiNDYUGlMmxzxBYU.htm)|Fouiller|`@Compendium[pf2e.actionspf2e.TiNDYUGlMmxzxBYU]`|
|[Seek](actions/BlAOM2X92SI6HMtJ.htm)|Chercher|`@Compendium[pf2e.actionspf2e.BlAOM2X92SI6HMtJ]`|
|[Selfish Shield](actions/enQieRrITuEQZxx2.htm)|Bouclier égoïste|`@Compendium[pf2e.actionspf2e.enQieRrITuEQZxx2]`|
|[Sense Direction](actions/fJImDBQfqfjKJOhk.htm)|S'orienter|`@Compendium[pf2e.actionspf2e.fJImDBQfqfjKJOhk]`|
|[Sense Motive](actions/1xRFPTFtWtGJ9ELw.htm)|Deviner les intentions|`@Compendium[pf2e.actionspf2e.1xRFPTFtWtGJ9ELw]`|
|[Shove](actions/7blmbDrQFNfdT731.htm)|Pousser|`@Compendium[pf2e.actionspf2e.7blmbDrQFNfdT731]`|
|[Sneak](actions/VMozDqMMuK5kpoX4.htm)|Être furtif|`@Compendium[pf2e.actionspf2e.VMozDqMMuK5kpoX4]`|
|[Soaring Flight](actions/mVscmsZWWcVACdU5.htm)|Voler haut|`@Compendium[pf2e.actionspf2e.mVscmsZWWcVACdU5]`|
|[Squeeze](actions/kMcV8e5EZUxa6evt.htm)|Se faufiler|`@Compendium[pf2e.actionspf2e.kMcV8e5EZUxa6evt]`|
|[Stand](actions/OdIUybJ3ddfL7wzj.htm)|Se relever|`@Compendium[pf2e.actionspf2e.OdIUybJ3ddfL7wzj]`|
|[Steal](actions/RDXXE7wMrSPCLv5k.htm)|Voler|`@Compendium[pf2e.actionspf2e.RDXXE7wMrSPCLv5k]`|
|[Steel your Resolve](actions/D2PNfIw7U6Ks0VY4.htm)|Puiser dans votre résolution|`@Compendium[pf2e.actionspf2e.D2PNfIw7U6Ks0VY4]`|
|[Step](actions/UHpkTuCtyaPqiCAB.htm)|Faire un pas|`@Compendium[pf2e.actionspf2e.UHpkTuCtyaPqiCAB]`|
|[Stride](actions/Bcxarzksqt9ezrs6.htm)|Marcher rapidement|`@Compendium[pf2e.actionspf2e.Bcxarzksqt9ezrs6]`|
|[Strike](actions/VjxZFuUXrCU94MWR.htm)|Frapper|`@Compendium[pf2e.actionspf2e.VjxZFuUXrCU94MWR]`|
|[Subsist](actions/49y9Ec4bDii8pcD3.htm)|Subsister|`@Compendium[pf2e.actionspf2e.49y9Ec4bDii8pcD3]`|
|[Sustain a Spell](actions/3f5DMFu8fPiqHpRg.htm)|Maintenir un sort|`@Compendium[pf2e.actionspf2e.3f5DMFu8fPiqHpRg]`|
|[Sustain an Activation](actions/XSGlLjI80LDCimAP.htm)|Maintenir une activation|`@Compendium[pf2e.actionspf2e.XSGlLjI80LDCimAP]`|
|[Swim](actions/c8TGiZ48ygoSPofx.htm)|Nager|`@Compendium[pf2e.actionspf2e.c8TGiZ48ygoSPofx]`|
|[Tail Toxin](actions/ev8OHpBO3xq3Zt08.htm)|Queue à toxine|`@Compendium[pf2e.actionspf2e.ev8OHpBO3xq3Zt08]`|
|[Take Cover](actions/ust1jJSCZQUhBZIz.htm)|Mise à l'abri|`@Compendium[pf2e.actionspf2e.ust1jJSCZQUhBZIz]`|
|[Take a Breather](actions/qc0VsZ0UesnurUUB.htm)|Reprendre son souffle|`@Compendium[pf2e.actionspf2e.qc0VsZ0UesnurUUB]`|
|[Threatening Approach](actions/W4M8L9WepGLamlHs.htm)|Approche menaçante|`@Compendium[pf2e.actionspf2e.W4M8L9WepGLamlHs]`|
|[Track](actions/EA5vuSgJfiHH7plD.htm)|Pister|`@Compendium[pf2e.actionspf2e.EA5vuSgJfiHH7plD]`|
|[Treat Disease](actions/TC7OcDa7JlWbqMaN.htm)|Soigner une maladie|`@Compendium[pf2e.actionspf2e.TC7OcDa7JlWbqMaN]`|
|[Treat Poison](actions/KjoCEEmPGTeFE4hh.htm)|Soigner un empoisonnement|`@Compendium[pf2e.actionspf2e.KjoCEEmPGTeFE4hh]`|
|[Treat Wounds](actions/1kGNdIIhuglAjIp9.htm)|Soigner les blessures|`@Compendium[pf2e.actionspf2e.1kGNdIIhuglAjIp9]`|
|[Trip](actions/ge56Lu1xXVFYUnLP.htm)|Croc-en-jambe|`@Compendium[pf2e.actionspf2e.ge56Lu1xXVFYUnLP]`|
|[Tumble Through](actions/21WIfSu7Xd7uKqV8.htm)|Déplacement acrobatique|`@Compendium[pf2e.actionspf2e.21WIfSu7Xd7uKqV8]`|
|[Upstage](actions/F0JgJR2rXKOg9k1z.htm)|Surpasser|`@Compendium[pf2e.actionspf2e.F0JgJR2rXKOg9k1z]`|
|[Warding Sign](actions/7pdG8l9POMK76Lf2.htm)|Signe de protection|`@Compendium[pf2e.actionspf2e.7pdG8l9POMK76Lf2]`|


## Aptitudes des familiers

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Accompanist](familiar-abilities/01-92lgSEPFIDLvKOCF.htm)|Accompagnateur|`@Compendium[pf2e.familiar-abilities.92lgSEPFIDLvKOCF]`|
|[Amphibious](familiar-abilities/01-A0C86V3MUECpX5jd.htm)|Amphibie|`@Compendium[pf2e.familiar-abilities.A0C86V3MUECpX5jd]`|
|[Burrower](familiar-abilities/01-uy15sDBuYNK48N3v.htm)|Fouisseur|`@Compendium[pf2e.familiar-abilities.uy15sDBuYNK48N3v]`|
|[Climber](familiar-abilities/01-dWTfO5WbLkD5mw2H.htm)|Grimpeur|`@Compendium[pf2e.familiar-abilities.dWTfO5WbLkD5mw2H]`|
|[Damage Avoidance: Fortitude](familiar-abilities/01-mK3mAUWiRLZZYNdz.htm)|Évitement des dégâts : Vigueur|`@Compendium[pf2e.familiar-abilities.mK3mAUWiRLZZYNdz]`|
|[Damage Avoidance: Reflex](familiar-abilities/01-K5OLRDsGCfPZ6mO6.htm)|Évitement des dégâts : Réflexes|`@Compendium[pf2e.familiar-abilities.K5OLRDsGCfPZ6mO6]`|
|[Damage Avoidance: Will](familiar-abilities/01-5gwqSpkRqWzrbDDU.htm)|Évitement des dégâts : Volonté|`@Compendium[pf2e.familiar-abilities.5gwqSpkRqWzrbDDU]`|
|[Darkvision](familiar-abilities/01-0Xrkk46IM43iI1Fv.htm)|Vision dans le noir|`@Compendium[pf2e.familiar-abilities.0Xrkk46IM43iI1Fv]`|
|[Fast Movement: Climb](familiar-abilities/01-SxWYVgqNMsq0OijU.htm)|Déplacement rapide : Escalade|`@Compendium[pf2e.familiar-abilities.SxWYVgqNMsq0OijU]`|
|[Fast Movement: Fly](familiar-abilities/01-VHQUZcjUxfC3GcJ9.htm)|Déplacement rapide : Vol|`@Compendium[pf2e.familiar-abilities.VHQUZcjUxfC3GcJ9]`|
|[Fast Movement: Land](familiar-abilities/01-BXssJhTJjKrfojwG.htm)|Déplacement rapide : Au sol|`@Compendium[pf2e.familiar-abilities.BXssJhTJjKrfojwG]`|
|[Fast Movement: Swim](familiar-abilities/01-vpw2ReYdcyQBpdqn.htm)|Déplacement rapide : Nage|`@Compendium[pf2e.familiar-abilities.vpw2ReYdcyQBpdqn]`|
|[Flier](familiar-abilities/01-ZHSzNt3NxkXbj1mI.htm)|Volant|`@Compendium[pf2e.familiar-abilities.ZHSzNt3NxkXbj1mI]`|
|[Focused Rejuvenation](familiar-abilities/01-O5TIjqXAuta8iVSz.htm)|Récupération concentrée|`@Compendium[pf2e.familiar-abilities.O5TIjqXAuta8iVSz]`|
|[Independent](familiar-abilities/01-2fiQzoEKu6YUnrU9.htm)|Indépendant|`@Compendium[pf2e.familiar-abilities.2fiQzoEKu6YUnrU9]`|
|[Kinspeech](familiar-abilities/01-qTxH8mSOvc4PMzrP.htm)|Parole des semblables|`@Compendium[pf2e.familiar-abilities.qTxH8mSOvc4PMzrP]`|
|[Lab Assistant](familiar-abilities/01-7QosmRHlyLLhU1hX.htm)|Assistant de laboratoire|`@Compendium[pf2e.familiar-abilities.7QosmRHlyLLhU1hX]`|
|[Manual Dexterity](familiar-abilities/01-cT5octWchU4gjrhP.htm)|Dextérité manuelle|`@Compendium[pf2e.familiar-abilities.cT5octWchU4gjrhP]`|
|[Master's Form](familiar-abilities/01-JRP2bdkdCdj2JDrq.htm)|Forme du maître|`@Compendium[pf2e.familiar-abilities.JRP2bdkdCdj2JDrq]`|
|[Partner in Crime](familiar-abilities/01-v7zE3tKQb9G6PaYU.htm)|Associés dans le crime|`@Compendium[pf2e.familiar-abilities.v7zE3tKQb9G6PaYU]`|
|[Plant Form](familiar-abilities/01-57b8u8s3fV0UCrgJ.htm)|Forme de plante|`@Compendium[pf2e.familiar-abilities.57b8u8s3fV0UCrgJ]`|
|[Poison Reservoir](familiar-abilities/01-8Z1UkLEWkFWIjOF8.htm)|Réservoir de poison|`@Compendium[pf2e.familiar-abilities.8Z1UkLEWkFWIjOF8]`|
|[Resistance](familiar-abilities/01-FcQQLMAJMgOLjnSv.htm)|Résistance|`@Compendium[pf2e.familiar-abilities.FcQQLMAJMgOLjnSv]`|
|[Scent](familiar-abilities/01-FlRUb8U13Crj3NaA.htm)|Odorat|`@Compendium[pf2e.familiar-abilities.FlRUb8U13Crj3NaA]`|
|[Skilled](familiar-abilities/01-jevzf9JbJJibpqaI.htm)|Compétent|`@Compendium[pf2e.familiar-abilities.jevzf9JbJJibpqaI]`|
|[Speech](familiar-abilities/01-zyMRLQnFCQVpltiR.htm)|Parole|`@Compendium[pf2e.familiar-abilities.zyMRLQnFCQVpltiR]`|
|[Spellcasting](familiar-abilities/01-asOhEdyF8CWFbR96.htm)|Incantateur|`@Compendium[pf2e.familiar-abilities.asOhEdyF8CWFbR96]`|
|[Toolbearer](familiar-abilities/01-uUrsZ4WvhjKjFjnt.htm)|Porte outils|`@Compendium[pf2e.familiar-abilities.uUrsZ4WvhjKjFjnt]`|
|[Touch Telepathy](familiar-abilities/01-XCqYnlVbLGqEGPeX.htm)|Télépathie de contact|`@Compendium[pf2e.familiar-abilities.XCqYnlVbLGqEGPeX]`|
|[Tough](familiar-abilities/01-Le8UWr5BU8rV3iBf.htm)|Robuste|`@Compendium[pf2e.familiar-abilities.Le8UWr5BU8rV3iBf]`|
|[Valet](familiar-abilities/01-9PsptrEoCC4QdM23.htm)|Valet|`@Compendium[pf2e.familiar-abilities.9PsptrEoCC4QdM23]`|


## Aptitudes du bestiaire

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[All-Around Vision](bestiary-ability-glossary-srd/9qV49KjZujZnSp6w.htm)|Vision à 360°|`@Compendium[pf2e.bestiary-ability-glossary-srd.9qV49KjZujZnSp6w]`|
|[Aquatic Ambush](bestiary-ability-glossary-srd/HBrBrUzjfvj2gDXB.htm)|Embuscade aquatique|`@Compendium[pf2e.bestiary-ability-glossary-srd.HBrBrUzjfvj2gDXB]`|
|[At-Will Spells](bestiary-ability-glossary-srd/2YRDYVnC1eljaXKK.htm)|Sorts à volonté|`@Compendium[pf2e.bestiary-ability-glossary-srd.2YRDYVnC1eljaXKK]`|
|[Attack of Opportunity](bestiary-ability-glossary-srd/hFtNbo1LKYCoDy2O.htm)|Attaque d'opportunité|`@Compendium[pf2e.bestiary-ability-glossary-srd.hFtNbo1LKYCoDy2O]`|
|[Aura](bestiary-ability-glossary-srd/v61oEQaDdcRpaZ9X.htm)|Aura|`@Compendium[pf2e.bestiary-ability-glossary-srd.v61oEQaDdcRpaZ9X]`|
|[Buck](bestiary-ability-glossary-srd/nZMQh4AaBr291TUf.htm)|Désarçonner|`@Compendium[pf2e.bestiary-ability-glossary-srd.nZMQh4AaBr291TUf]`|
|[Catch Rock](bestiary-ability-glossary-srd/uG0Z8PsyZtsYuvGR.htm)|Interception de rochers|`@Compendium[pf2e.bestiary-ability-glossary-srd.uG0Z8PsyZtsYuvGR]`|
|[Change Shape](bestiary-ability-glossary-srd/eQM5hQ1W3d1uen97.htm)|Changement de forme|`@Compendium[pf2e.bestiary-ability-glossary-srd.eQM5hQ1W3d1uen97]`|
|[Constant Spells](bestiary-ability-glossary-srd/kakyXBG5WA8c6Zfd.htm)|Sorts constants|`@Compendium[pf2e.bestiary-ability-glossary-srd.kakyXBG5WA8c6Zfd]`|
|[Constrict](bestiary-ability-glossary-srd/g26YiEIfSHCpLocV.htm)|Constriction|`@Compendium[pf2e.bestiary-ability-glossary-srd.g26YiEIfSHCpLocV]`|
|[Coven](bestiary-ability-glossary-srd/52CdldlWMiVTZk1F.htm)|Cercle|`@Compendium[pf2e.bestiary-ability-glossary-srd.52CdldlWMiVTZk1F]`|
|[Darkvision](bestiary-ability-glossary-srd/qCCLZhnp2HhP3Ex6.htm)|Vision dans le noir|`@Compendium[pf2e.bestiary-ability-glossary-srd.qCCLZhnp2HhP3Ex6]`|
|[Disease](bestiary-ability-glossary-srd/Z025dWgZtawbuRLI.htm)|Maladie|`@Compendium[pf2e.bestiary-ability-glossary-srd.Z025dWgZtawbuRLI]`|
|[Engulf](bestiary-ability-glossary-srd/zU3Ovaet4xQ5Gmvy.htm)|Engloutissement|`@Compendium[pf2e.bestiary-ability-glossary-srd.zU3Ovaet4xQ5Gmvy]`|
|[Fast Healing](bestiary-ability-glossary-srd/fJSNOw4zHGbIm4bZ.htm)|Guérison accélérée|`@Compendium[pf2e.bestiary-ability-glossary-srd.fJSNOw4zHGbIm4bZ]`|
|[Ferocity](bestiary-ability-glossary-srd/N1kstYbHScxgUQtN.htm)|Férocité|`@Compendium[pf2e.bestiary-ability-glossary-srd.N1kstYbHScxgUQtN]`|
|[Frightful Presence](bestiary-ability-glossary-srd/etMnv73EIdEZrYYu.htm)|Présence terrifiante|`@Compendium[pf2e.bestiary-ability-glossary-srd.etMnv73EIdEZrYYu]`|
|[Grab](bestiary-ability-glossary-srd/Tkd8sH4pwFIPzqTr.htm)|Empoignade/agrippement|`@Compendium[pf2e.bestiary-ability-glossary-srd.Tkd8sH4pwFIPzqTr]`|
|[Greater Constrict](bestiary-ability-glossary-srd/OmV6E3aELvq9CkK1.htm)|Constriction supérieure|`@Compendium[pf2e.bestiary-ability-glossary-srd.OmV6E3aELvq9CkK1]`|
|[Improved Knockdown](bestiary-ability-glossary-srd/LbnsT8yXchHMQT3t.htm)|Renversement amélioré|`@Compendium[pf2e.bestiary-ability-glossary-srd.LbnsT8yXchHMQT3t]`|
|[Improved Push](bestiary-ability-glossary-srd/6l7e7CHQLESlI57U.htm)|Bousculade améliorée|`@Compendium[pf2e.bestiary-ability-glossary-srd.6l7e7CHQLESlI57U]`|
|[Inproved Grab](bestiary-ability-glossary-srd/i18TlebMzwONyPhI.htm)|Empoignade/agrippement amélioré|`@Compendium[pf2e.bestiary-ability-glossary-srd.i18TlebMzwONyPhI]`|
|[Knockdown](bestiary-ability-glossary-srd/BCLvAx4Pz4MLa2pu.htm)|Renversement|`@Compendium[pf2e.bestiary-ability-glossary-srd.BCLvAx4Pz4MLa2pu]`|
|[Lifesense](bestiary-ability-glossary-srd/sebk9XseMCRkDqRg.htm)|Sens de la vie|`@Compendium[pf2e.bestiary-ability-glossary-srd.sebk9XseMCRkDqRg]`|
|[Light Blindness](bestiary-ability-glossary-srd/I0HYG0ctCLP5JRsW.htm)|Aveuglé par la lumière|`@Compendium[pf2e.bestiary-ability-glossary-srd.I0HYG0ctCLP5JRsW]`|
|[Low-Light Vision](bestiary-ability-glossary-srd/fFu2sZz4KB01fVRc.htm)|Vision nocturne|`@Compendium[pf2e.bestiary-ability-glossary-srd.fFu2sZz4KB01fVRc]`|
|[Negative Healing](bestiary-ability-glossary-srd/TTCw5NusiSSkJU1x.htm)|Soins négatifs|`@Compendium[pf2e.bestiary-ability-glossary-srd.TTCw5NusiSSkJU1x]`|
|[Poison](bestiary-ability-glossary-srd/kFv54DisTfCMTBEY.htm)|Poison|`@Compendium[pf2e.bestiary-ability-glossary-srd.kFv54DisTfCMTBEY]`|
|[Push](bestiary-ability-glossary-srd/t6cx9FOODmeZQNYl.htm)|Bousculade|`@Compendium[pf2e.bestiary-ability-glossary-srd.t6cx9FOODmeZQNYl]`|
|[Regeneration](bestiary-ability-glossary-srd/lR9R5Vld8Eu2Dha5.htm)|Régénération|`@Compendium[pf2e.bestiary-ability-glossary-srd.lR9R5Vld8Eu2Dha5]`|
|[Rend](bestiary-ability-glossary-srd/3JOi2cMcGhT3eze1.htm)|Éventration|`@Compendium[pf2e.bestiary-ability-glossary-srd.3JOi2cMcGhT3eze1]`|
|[Retributive Strike](bestiary-ability-glossary-srd/IQtb58p4EaeUzTN1.htm)|Frappe punitive|`@Compendium[pf2e.bestiary-ability-glossary-srd.IQtb58p4EaeUzTN1]`|
|[Scent](bestiary-ability-glossary-srd/rqfnQ5VHT5hxm25r.htm)|Odorat|`@Compendium[pf2e.bestiary-ability-glossary-srd.rqfnQ5VHT5hxm25r]`|
|[Shield Block](bestiary-ability-glossary-srd/m4HQ2o5aPxjXf2kY.htm)|Blocage au bouclier|`@Compendium[pf2e.bestiary-ability-glossary-srd.m4HQ2o5aPxjXf2kY]`|
|[Sneak Attack](bestiary-ability-glossary-srd/AWvNPE4U0kEJSL1T.htm)|Attaque sournoise|`@Compendium[pf2e.bestiary-ability-glossary-srd.AWvNPE4U0kEJSL1T]`|
|[Swallow Whole](bestiary-ability-glossary-srd/uJSseLa57HZYSMUu.htm)|Gober|`@Compendium[pf2e.bestiary-ability-glossary-srd.uJSseLa57HZYSMUu]`|
|[Swarm Mind](bestiary-ability-glossary-srd/baA0nSMhQyFyJIia.htm)|Esprit de la nuée|`@Compendium[pf2e.bestiary-ability-glossary-srd.baA0nSMhQyFyJIia]`|
|[Telepathy](bestiary-ability-glossary-srd/kdhbPaBMK1d1fpbA.htm)|Télépathie|`@Compendium[pf2e.bestiary-ability-glossary-srd.kdhbPaBMK1d1fpbA]`|
|[Throw Rock](bestiary-ability-glossary-srd/a0tx6exmB9v9CUsj.htm)|Projection de rochers|`@Compendium[pf2e.bestiary-ability-glossary-srd.a0tx6exmB9v9CUsj]`|
|[Trample](bestiary-ability-glossary-srd/UNah0bxXxkcZjxO3.htm)|Piétinement|`@Compendium[pf2e.bestiary-ability-glossary-srd.UNah0bxXxkcZjxO3]`|
|[Tremorsense](bestiary-ability-glossary-srd/j2wsK6IsW5yMW1jW.htm)|Perception des vibrations|`@Compendium[pf2e.bestiary-ability-glossary-srd.j2wsK6IsW5yMW1jW]`|
|[Wavesense](bestiary-ability-glossary-srd/VdSMQ6yRZ3YXNXHL.htm)|Perception des ondes|`@Compendium[pf2e.bestiary-ability-glossary-srd.VdSMQ6yRZ3YXNXHL]`|


## Archétypes

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Acrobat](archetypes/SbvhfCo5gFVt1DgD.htm)|Acrobate|`@Compendium[pf2e.archetypes.SbvhfCo5gFVt1DgD]`|
|[Aldori Duelist](archetypes/cnsq5cXgQu4FXnoZ.htm)|Duelliste Aldori|`@Compendium[pf2e.archetypes.cnsq5cXgQu4FXnoZ]`|
|[Animal Trainer](archetypes/kisnuKPMGheORROU.htm)|Dompteur|`@Compendium[pf2e.archetypes.kisnuKPMGheORROU]`|
|[Archaeologist](archetypes/myvFp45VnA2FPOvG.htm)|Archéologue|`@Compendium[pf2e.archetypes.myvFp45VnA2FPOvG]`|
|[Archer](archetypes/MJ2EBeAVkc61mznR.htm)|Archer|`@Compendium[pf2e.archetypes.MJ2EBeAVkc61mznR]`|
|[Assassin](archetypes/7BNWl18efHJ93hke.htm)|Assassin|`@Compendium[pf2e.archetypes.7BNWl18efHJ93hke]`|
|[Bastion](archetypes/UsT8XqN5LwfbBMzp.htm)|Bastion|`@Compendium[pf2e.archetypes.UsT8XqN5LwfbBMzp]`|
|[Beastmaster](archetypes/ZvIreFl9TXgrj52Q.htm)|Maître des bêtes|`@Compendium[pf2e.archetypes.ZvIreFl9TXgrj52Q]`|
|[Bellflower Tiller](archetypes/gw4J1pXYED71TPWx.htm)|Laboureur du Campanule|`@Compendium[pf2e.archetypes.gw4J1pXYED71TPWx]`|
|[Blessed One](archetypes/5zILvODoiQC34tWU.htm)|Élu divin|`@Compendium[pf2e.archetypes.5zILvODoiQC34tWU]`|
|[Bounty Hunter](archetypes/GCoO342NLbrmEer1.htm)|Chasseur de primes|`@Compendium[pf2e.archetypes.GCoO342NLbrmEer1]`|
|[Bright Lion](archetypes/bDP7kO6bnQh6no0C.htm)|Lion Radieux|`@Compendium[pf2e.archetypes.bDP7kO6bnQh6no0C]`|
|[Cavalier](archetypes/hLFBAs2NpyGjQiDA.htm)|Cavalier|`@Compendium[pf2e.archetypes.hLFBAs2NpyGjQiDA]`|
|[Celebrity](archetypes/ThxMkphglPJuweXB.htm)|Célébrité|`@Compendium[pf2e.archetypes.ThxMkphglPJuweXB]`|
|[Crystal Keeper](archetypes/xNzNpPjeNSrXE7WG.htm)|Gardien des cristaux|`@Compendium[pf2e.archetypes.xNzNpPjeNSrXE7WG]`|
|[Dandy](archetypes/5cbEwgPLlMWGGVxN.htm)|Dandy|`@Compendium[pf2e.archetypes.5cbEwgPLlMWGGVxN]`|
|[Dragon Disciple](archetypes/lsU4NQEei111OetM.htm)|Disciple draconique|`@Compendium[pf2e.archetypes.lsU4NQEei111OetM]`|
|[Dual-Weapon Warrior](archetypes/K1mmFJ3QSjNkHm2L.htm)|Combattant à deux armes|`@Compendium[pf2e.archetypes.K1mmFJ3QSjNkHm2L]`|
|[Duelist](archetypes/DYJfhMcLlP05oMDD.htm)|Duelliste|`@Compendium[pf2e.archetypes.DYJfhMcLlP05oMDD]`|
|[Edgewatch Detective](archetypes/Q3VydQ6b5Mxm1c41.htm)|Officier d'Edgewatch|`@Compendium[pf2e.archetypes.Q3VydQ6b5Mxm1c41]`|
|[Eldritch Archer](archetypes/z5yvyY2HfIRgwGnO.htm)|Archer mystique|`@Compendium[pf2e.archetypes.z5yvyY2HfIRgwGnO]`|
|[Familiar Master](archetypes/Bl0KM29OOlwBegCi.htm)|Maître familier|`@Compendium[pf2e.archetypes.Bl0KM29OOlwBegCi]`|
|[Firebrand Braggart](archetypes/YBvXmJ5e9MThdws0.htm)|Agitateur vantard|`@Compendium[pf2e.archetypes.YBvXmJ5e9MThdws0]`|
|[Ghost Hunter](archetypes/2TRqXfTtfMhTSIDY.htm)|Chasseur de Fantômes|`@Compendium[pf2e.archetypes.2TRqXfTtfMhTSIDY]`|
|[Gladiator](archetypes/IU0WKnP9qciXztOC.htm)|Gladiateur|`@Compendium[pf2e.archetypes.IU0WKnP9qciXztOC]`|
|[Golem Grafter](archetypes/XATPzPTZ9Je89erT.htm)|Greffeur de golem|`@Compendium[pf2e.archetypes.XATPzPTZ9Je89erT]`|
|[Halcyon Speaker](archetypes/Qsmzm6w1ZNpHX7GD.htm)|Orateur paisible|`@Compendium[pf2e.archetypes.Qsmzm6w1ZNpHX7GD]`|
|[Hellknight](archetypes/2Wol7CpOBJjVT3aP.htm)|Chevalier infernal|`@Compendium[pf2e.archetypes.2Wol7CpOBJjVT3aP]`|
|[Hellknight Armiger](archetypes/LYYDozZfXok19VYW.htm)|Écuyer des chevaliers infernaux|`@Compendium[pf2e.archetypes.LYYDozZfXok19VYW]`|
|[Hellknight Signifer](archetypes/GwItivMACzBmW82g.htm)|Signifer|`@Compendium[pf2e.archetypes.GwItivMACzBmW82g]`|
|[Herbalist](archetypes/cEiMI4QGqjv69pJ6.htm)|Herboriste|`@Compendium[pf2e.archetypes.cEiMI4QGqjv69pJ6]`|
|[Horizon Walker](archetypes/RvjlaAPa2Wkdhrms.htm)|Arpenteur d'horizon|`@Compendium[pf2e.archetypes.RvjlaAPa2Wkdhrms]`|
|[Investigator](archetypes/Buptz08MArCtY41w.htm)|Enquêteur|`@Compendium[pf2e.archetypes.Buptz08MArCtY41w]`|
|[Jalmeri Heavenseeker](archetypes/s7kWIaeCdj3IFte9.htm)|Chercheur de paradis du Jalmeray|`@Compendium[pf2e.archetypes.s7kWIaeCdj3IFte9]`|
|[Juggler](archetypes/8vuL2hCPrcG2duLt.htm)|Jongleur|`@Compendium[pf2e.archetypes.8vuL2hCPrcG2duLt]`|
|[Knight Reclaimant](archetypes/WrQ2Qpv958C6cfD0.htm)|Chevalier récupérateur|`@Compendium[pf2e.archetypes.WrQ2Qpv958C6cfD0]`|
|[Knight Vigilant](archetypes/l4sKdl0ub0UGBwsb.htm)|Chevalier vigilant|`@Compendium[pf2e.archetypes.l4sKdl0ub0UGBwsb]`|
|[Lastwall Sentry](archetypes/Utxq3fo8AoPyfh4K.htm)|Sentinelle du Dernier-Rempart|`@Compendium[pf2e.archetypes.Utxq3fo8AoPyfh4K]`|
|[Linguist](archetypes/TdoPhja8JA9C2Itm.htm)|Linguiste|`@Compendium[pf2e.archetypes.TdoPhja8JA9C2Itm]`|
|[Lion Blade](archetypes/678FjGfCmxuNbNhe.htm)|Lame du Lion|`@Compendium[pf2e.archetypes.678FjGfCmxuNbNhe]`|
|[Living Monolith](archetypes/4xzvjFVytFA0LIYj.htm)|Monolithe vivant|`@Compendium[pf2e.archetypes.4xzvjFVytFA0LIYj]`|
|[Loremaster](archetypes/DMsE43xgf1gHPl8o.htm)|Maître savant|`@Compendium[pf2e.archetypes.DMsE43xgf1gHPl8o]`|
|[Magaambyan Attendant](archetypes/yvb2pY3Qb7Jl35hk.htm)|Gardien du Magaambya|`@Compendium[pf2e.archetypes.yvb2pY3Qb7Jl35hk]`|
|[Magic Warrior](archetypes/OmEG0nhQgeKnAUsn.htm)|Guerrier magique|`@Compendium[pf2e.archetypes.OmEG0nhQgeKnAUsn]`|
|[Marshal](archetypes/zv31F34JgUesREuz.htm)|Marshal|`@Compendium[pf2e.archetypes.zv31F34JgUesREuz]`|
|[Martial Artist](archetypes/Ld3wizivrctmoHhy.htm)|Artiste martial|`@Compendium[pf2e.archetypes.Ld3wizivrctmoHhy]`|
|[Mauler](archetypes/UOhVrWvuaAVtQHEo.htm)|Cogneur|`@Compendium[pf2e.archetypes.UOhVrWvuaAVtQHEo]`|
|[Medic](archetypes/CA22ZhzFPjahrO4W.htm)|Médecin|`@Compendium[pf2e.archetypes.CA22ZhzFPjahrO4W]`|
|[Oozemorph](archetypes/GADKhvTRWCqlI9vy.htm)|Vasemorphe|`@Compendium[pf2e.archetypes.GADKhvTRWCqlI9vy]`|
|[Oracle](archetypes/akmaoP0StWvIv3jb.htm)|Oracle|`@Compendium[pf2e.archetypes.akmaoP0StWvIv3jb]`|
|[Pathfinder Agent](archetypes/o3C85NHW08Y2inAN.htm)|Agent des Éclaireurs|`@Compendium[pf2e.archetypes.o3C85NHW08Y2inAN]`|
|[Pirate](archetypes/PlPc4w1XG9VOpDgn.htm)|Pirate|`@Compendium[pf2e.archetypes.PlPc4w1XG9VOpDgn]`|
|[Poisoner](archetypes/fjyB6HdS95xHEQn0.htm)|Empoisonneur|`@Compendium[pf2e.archetypes.fjyB6HdS95xHEQn0]`|
|[Provocator](archetypes/YJQJxskQf8VqDt8G.htm)|Provocator|`@Compendium[pf2e.archetypes.YJQJxskQf8VqDt8G]`|
|[Red Mantis Assassin](archetypes/DeixUOfwL7Bruad5.htm)|Assassin des Mantes rouges|`@Compendium[pf2e.archetypes.DeixUOfwL7Bruad5]`|
|[Ritualist](archetypes/IoDeWTPMoa7LNLWD.htm)|Ritualiste|`@Compendium[pf2e.archetypes.IoDeWTPMoa7LNLWD]`|
|[Runescarred](archetypes/RyjrUkqSIg52Mjl8.htm)|Scarifié des runes|`@Compendium[pf2e.archetypes.RyjrUkqSIg52Mjl8]`|
|[Scout](archetypes/pHTnfVHOzsgfZ9E2.htm)|Éclaireur|`@Compendium[pf2e.archetypes.pHTnfVHOzsgfZ9E2]`|
|[Scroll Trickster](archetypes/xFjXKdIkA574QRkv.htm)|Usurpateur de parchemins|`@Compendium[pf2e.archetypes.xFjXKdIkA574QRkv]`|
|[Scrollmaster](archetypes/2eAiIynDWTMKTGDU.htm)|Maître des parchemins|`@Compendium[pf2e.archetypes.2eAiIynDWTMKTGDU]`|
|[Scrounger](archetypes/eEyxTQ3oHfthdg0e.htm)|Bricoleur|`@Compendium[pf2e.archetypes.eEyxTQ3oHfthdg0e]`|
|[Sentinel](archetypes/e2KCqgD3zt8yvxGg.htm)|Sentinelle|`@Compendium[pf2e.archetypes.e2KCqgD3zt8yvxGg]`|
|[Shadowdancer](archetypes/70DYBk9gZCH9uSUs.htm)|Danseur de l'ombre|`@Compendium[pf2e.archetypes.70DYBk9gZCH9uSUs]`|
|[Snarecrafter](archetypes/uKoay1d62vRae3z1.htm)|Fabricant de pièges artisanaux|`@Compendium[pf2e.archetypes.uKoay1d62vRae3z1]`|
|[Spellmaster](archetypes/25NJcH5SkWh1Tol7.htm)|Maître des sorts|`@Compendium[pf2e.archetypes.25NJcH5SkWh1Tol7]`|
|[Staff Acrobat](archetypes/XZwGLLgC1sIKlR1c.htm)|Funambule|`@Compendium[pf2e.archetypes.XZwGLLgC1sIKlR1c]`|
|[Student of Perfection](archetypes/JiRqSMv1STETLwSP.htm)|Étudiant en Perfection|`@Compendium[pf2e.archetypes.JiRqSMv1STETLwSP]`|
|[Swashbuckler](archetypes/GQn5HfP1Jg5qO0Dz.htm)|Bretteur|`@Compendium[pf2e.archetypes.GQn5HfP1Jg5qO0Dz]`|
|[Swordmaster](archetypes/bi1SeBS7Af3ZisIA.htm)|Maître des épées|`@Compendium[pf2e.archetypes.bi1SeBS7Af3ZisIA]`|
|[Talisman Dabbler](archetypes/3jceS1GfWcSQ3tMY.htm)|Amateur de talismans|`@Compendium[pf2e.archetypes.3jceS1GfWcSQ3tMY]`|
|[Turpin Rowe Lumberjack](archetypes/VRUYc3QyRjjOjppc.htm)|Bûcheron de Turpin Rowe|`@Compendium[pf2e.archetypes.VRUYc3QyRjjOjppc]`|
|[Vigilante](archetypes/R0ORjHAeQ0auxl2j.htm)|Justicier|`@Compendium[pf2e.archetypes.R0ORjHAeQ0auxl2j]`|
|[Viking](archetypes/SJi2cicLd7R2FInl.htm)|Viking|`@Compendium[pf2e.archetypes.SJi2cicLd7R2FInl]`|
|[Weapon Improviser](archetypes/NGrKVIaZJx3r3In4.htm)|Improvisateur d'arme|`@Compendium[pf2e.archetypes.NGrKVIaZJx3r3In4]`|
|[Witch](archetypes/SAyPnVRucJa8k4MN.htm)|Sorcière|`@Compendium[pf2e.archetypes.SAyPnVRucJa8k4MN]`|
|[Zephyr Guard](archetypes/Lr108TWazbuxxUzP.htm)|Garde Zéphyr|`@Compendium[pf2e.archetypes.Lr108TWazbuxxUzP]`|


## Ascendances

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Catfolk](ancestries/972EkpJOPv9KkQIW.htm)|Homme-félin (félide)|`@Compendium[pf2e.ancestries.972EkpJOPv9KkQIW]`|
|[Dwarf](ancestries/BYj5ZvlXZdpaEgA6.htm)|Nain|`@Compendium[pf2e.ancestries.BYj5ZvlXZdpaEgA6]`|
|[Elf](ancestries/PgKmsA2aKdbLU6O0.htm)|Elfe|`@Compendium[pf2e.ancestries.PgKmsA2aKdbLU6O0]`|
|[Gnome](ancestries/CYlfsYLJcBOgqKtD.htm)|Gnome|`@Compendium[pf2e.ancestries.CYlfsYLJcBOgqKtD]`|
|[Goblin](ancestries/sQfjTMDaZbT9DThq.htm)|Gobelin|`@Compendium[pf2e.ancestries.sQfjTMDaZbT9DThq]`|
|[Halfling](ancestries/GgZAHbrjnzWOZy2v.htm)|Halfelin|`@Compendium[pf2e.ancestries.GgZAHbrjnzWOZy2v]`|
|[Hobgoblin](ancestries/piNLXUrm9iaGqD2i.htm)|Hobgobelin|`@Compendium[pf2e.ancestries.piNLXUrm9iaGqD2i]`|
|[Human](ancestries/IiG7DgeLWYrSNXuX.htm)|Humain|`@Compendium[pf2e.ancestries.IiG7DgeLWYrSNXuX]`|
|[Kobold](ancestries/7oQxL6wgsokD3QXG.htm)|Kobold|`@Compendium[pf2e.ancestries.7oQxL6wgsokD3QXG]`|
|[Leshy](ancestries/cdhgByGG1WtuaK73.htm)|Léchi|`@Compendium[pf2e.ancestries.cdhgByGG1WtuaK73]`|
|[Lizardfolk](ancestries/HWEgF7Gmoq55VhTL.htm)|Hommes-lézard|`@Compendium[pf2e.ancestries.HWEgF7Gmoq55VhTL]`|
|[Orc](ancestries/lSGWXjcbOa6O5fTx.htm)|Orc|`@Compendium[pf2e.ancestries.lSGWXjcbOa6O5fTx]`|
|[Ratfolk](ancestries/P6PcVnCkh4XMdefw.htm)|Homme-rat|`@Compendium[pf2e.ancestries.P6PcVnCkh4XMdefw]`|
|[Shoony](ancestries/q6rsqYARyOGXZA8F.htm)|Shoony|`@Compendium[pf2e.ancestries.q6rsqYARyOGXZA8F]`|
|[Tengu](ancestries/18xDKYPDBLEv2myX.htm)|Tengu|`@Compendium[pf2e.ancestries.18xDKYPDBLEv2myX]`|


## Ascendances (aptitudes)

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Aasimar](ancestryfeatures/feat-01-1oLMOmLpurfWTTff.htm)|Aasimar|`@Compendium[pf2e.ancestryfeatures.1oLMOmLpurfWTTff]`|
|[Ancient Elf](ancestryfeatures/feat-01-Nd9hdX8rdYyRozw8.htm)|Elfe ancien|`@Compendium[pf2e.ancestryfeatures.Nd9hdX8rdYyRozw8]`|
|[Ancient-Blooded Dwarf](ancestryfeatures/feat-01-yL6944LrPo2HNdEJ.htm)|Nain sang-ancien|`@Compendium[pf2e.ancestryfeatures.yL6944LrPo2HNdEJ]`|
|[Anvil Dwarf](ancestryfeatures/feat-01-kRDsVbhdBVeSlpBa.htm)|Nain de la forge|`@Compendium[pf2e.ancestryfeatures.kRDsVbhdBVeSlpBa]`|
|[Aquatic Adaptation](ancestryfeatures/feat-01-AzGJN1wwLFaLJIeo.htm)|Adaptation aquatique|`@Compendium[pf2e.ancestryfeatures.AzGJN1wwLFaLJIeo]`|
|[Arctic Elf](ancestryfeatures/feat-01-1dYDucCIaZpCJqBc.htm)|Elfe arctique|`@Compendium[pf2e.ancestryfeatures.1dYDucCIaZpCJqBc]`|
|[Badlands Orc](ancestryfeatures/feat-01-8Gsa8KFsHizEwSHU.htm)|Orque des badlands|`@Compendium[pf2e.ancestryfeatures.8Gsa8KFsHizEwSHU]`|
|[Bloodhound Shoony](ancestryfeatures/feat-01-z4cvqtpkkAYoFpHa.htm)|Shoony limier|`@Compendium[pf2e.ancestryfeatures.z4cvqtpkkAYoFpHa]`|
|[Blunt Snout](ancestryfeatures/feat-01-dtNsRAhCRfteA1ev.htm)|Museau court|`@Compendium[pf2e.ancestryfeatures.dtNsRAhCRfteA1ev]`|
|[Cavern Elf](ancestryfeatures/feat-01-NfIAGatB1KIzt8G7.htm)|Elfe des cavernes|`@Compendium[pf2e.ancestryfeatures.NfIAGatB1KIzt8G7]`|
|[Cavern Kobold](ancestryfeatures/feat-01-35k2aujXYvqUCSS1.htm)|Kobold cavernicole|`@Compendium[pf2e.ancestryfeatures.35k2aujXYvqUCSS1]`|
|[Chameleon Gnome](ancestryfeatures/feat-01-XeXWsvcWU3Zaj5WC.htm)|Gnome Caméléon|`@Compendium[pf2e.ancestryfeatures.XeXWsvcWU3Zaj5WC]`|
|[Changeling](ancestryfeatures/feat-01-EKY9v7SF1hVsUdbH.htm)|Changelin|`@Compendium[pf2e.ancestryfeatures.EKY9v7SF1hVsUdbH]`|
|[Charhide Goblin](ancestryfeatures/feat-01-32oX6hHUY6K8N70Q.htm)|Gobelin peaud'charbon|`@Compendium[pf2e.ancestryfeatures.32oX6hHUY6K8N70Q]`|
|[Clawed Catfolk](ancestryfeatures/feat-01-bLhIBwqdjTiVJ5qm.htm)|Homme-félin griffu|`@Compendium[pf2e.ancestryfeatures.bLhIBwqdjTiVJ5qm]`|
|[Claws](ancestryfeatures/feat-01-NfkxFWUeG6g41e8w.htm)|Griffes|`@Compendium[pf2e.ancestryfeatures.NfkxFWUeG6g41e8w]`|
|[Cliffscale Lizardfolk](ancestryfeatures/feat-01-OoUqJJB77VfWbWRM.htm)|Homme-lézard écaille de falaise|`@Compendium[pf2e.ancestryfeatures.OoUqJJB77VfWbWRM]`|
|[Darkvision](ancestryfeatures/feat-01-HHVQDp61ehcpdiU8.htm)|Vision dans le noir|`@Compendium[pf2e.ancestryfeatures.HHVQDp61ehcpdiU8]`|
|[Death Warden Dwarf](ancestryfeatures/feat-01-VTtXwBxrfRUXSL38.htm)|Nain gardemort|`@Compendium[pf2e.ancestryfeatures.VTtXwBxrfRUXSL38]`|
|[Deep Orc](ancestryfeatures/feat-01-wn4EbYk1QN3tyFhh.htm)|Orque des profondeurs|`@Compendium[pf2e.ancestryfeatures.wn4EbYk1QN3tyFhh]`|
|[Deep Rat](ancestryfeatures/feat-01-7p9HtLzWBHc18JDW.htm)|Rat des profondeurs|`@Compendium[pf2e.ancestryfeatures.7p9HtLzWBHc18JDW]`|
|[Desert Elf](ancestryfeatures/feat-01-kiKxnKd7Dfegk9dM.htm)|Elfe du désert|`@Compendium[pf2e.ancestryfeatures.kiKxnKd7Dfegk9dM]`|
|[Desert Rat](ancestryfeatures/feat-01-twayjFuXbsvyHUwy.htm)|Rat des sables|`@Compendium[pf2e.ancestryfeatures.twayjFuXbsvyHUwy]`|
|[Dhampir](ancestryfeatures/feat-01-UaD5VDoFlILEmbyz.htm)|Dhampir|`@Compendium[pf2e.ancestryfeatures.UaD5VDoFlILEmbyz]`|
|[Draconic Exemplar](ancestryfeatures/feat-01-egpiSWBrNBb1Fmig.htm)|Modèle draconique|`@Compendium[pf2e.ancestryfeatures.egpiSWBrNBb1Fmig]`|
|[Dragonscaled Kobold](ancestryfeatures/feat-01-nF5NUinK23fOLn5u.htm)|Kobold à écaille de dragon|`@Compendium[pf2e.ancestryfeatures.nF5NUinK23fOLn5u]`|
|[Duskwalker](ancestryfeatures/feat-01-ZZKZkeSP5TuT62IA.htm)|Crépusculaire|`@Compendium[pf2e.ancestryfeatures.ZZKZkeSP5TuT62IA]`|
|[Elemental Heart Dwarf](ancestryfeatures/feat-01-MeMAAtUlZmFgKSMF.htm)|Nain Cœurélémentaire|`@Compendium[pf2e.ancestryfeatures.MeMAAtUlZmFgKSMF]`|
|[Elfbane Hobgoblin](ancestryfeatures/feat-01-ikNJZRxUjcRLisO6.htm)|Hobgobelin fléau elfique|`@Compendium[pf2e.ancestryfeatures.ikNJZRxUjcRLisO6]`|
|[Fey-Touched Gnome](ancestryfeatures/feat-01-D3hTAqgwSank8OyO.htm)|Gnome béni des fées|`@Compendium[pf2e.ancestryfeatures.D3hTAqgwSank8OyO]`|
|[Fishseeker Shoony](ancestryfeatures/feat-01-isJhIPhT4MsjJvoq.htm)|Shoony Traquepoisson|`@Compendium[pf2e.ancestryfeatures.isJhIPhT4MsjJvoq]`|
|[Forge Dwarf](ancestryfeatures/feat-01-5CqsBKCZuGON53Hk.htm)|Nain des forges|`@Compendium[pf2e.ancestryfeatures.5CqsBKCZuGON53Hk]`|
|[Frilled Lizardfolk](ancestryfeatures/feat-01-6rIIsZg3tOyIU3g3.htm)|Homme-lézard à colerette|`@Compendium[pf2e.ancestryfeatures.6rIIsZg3tOyIU3g3]`|
|[Fungus Leshy](ancestryfeatures/feat-01-87h0jepQuzIbN7jN.htm)|Léchi fongique|`@Compendium[pf2e.ancestryfeatures.87h0jepQuzIbN7jN]`|
|[Gourd Leshy](ancestryfeatures/feat-01-wNnsjird4OQe0s6p.htm)|Léchi calebasse|`@Compendium[pf2e.ancestryfeatures.wNnsjird4OQe0s6p]`|
|[Gutsy Halfling](ancestryfeatures/feat-01-Eq42wZ5OTweJLnLU.htm)|Halfelin flegmatique|`@Compendium[pf2e.ancestryfeatures.Eq42wZ5OTweJLnLU]`|
|[Half-Elf](ancestryfeatures/feat-01-N36ZR4lh9eCazDaN.htm)|Demi-elfe|`@Compendium[pf2e.ancestryfeatures.N36ZR4lh9eCazDaN]`|
|[Half-Orc](ancestryfeatures/feat-01-tLd8Qg82AwEbbmgX.htm)|Demi-orc|`@Compendium[pf2e.ancestryfeatures.tLd8Qg82AwEbbmgX]`|
|[Hillock Halfling](ancestryfeatures/feat-01-Mj7uHxxVkRUlOFwJ.htm)|Halfelin des collines|`@Compendium[pf2e.ancestryfeatures.Mj7uHxxVkRUlOFwJ]`|
|[Hold-Scarred Orc](ancestryfeatures/feat-01-MTTU2t7x6TjvUDnE.htm)|Orque scarifié|`@Compendium[pf2e.ancestryfeatures.MTTU2t7x6TjvUDnE]`|
|[Hunting Catfolk](ancestryfeatures/feat-01-1lv7RMp7t5iqeUFT.htm)|Homme-félin chasseur|`@Compendium[pf2e.ancestryfeatures.1lv7RMp7t5iqeUFT]`|
|[Irongut Goblin](ancestryfeatures/feat-01-gyoN45SVfRZwHMkk.htm)|Gobelin Boyaud’fer|`@Compendium[pf2e.ancestryfeatures.gyoN45SVfRZwHMkk]`|
|[Jinxed Halfling](ancestryfeatures/feat-01-G9Gwfi8ZIva52uGp.htm)|Halfelin maudit|`@Compendium[pf2e.ancestryfeatures.G9Gwfi8ZIva52uGp]`|
|[Jinxed Tengu](ancestryfeatures/feat-01-g4FRxyuHndZu4KTo.htm)|Tengu maudit|`@Compendium[pf2e.ancestryfeatures.g4FRxyuHndZu4KTo]`|
|[Jungle Catfolk](ancestryfeatures/feat-01-HpqQ5VQ0w4HqYgVC.htm)|Homme-félin de la jungle|`@Compendium[pf2e.ancestryfeatures.HpqQ5VQ0w4HqYgVC]`|
|[Keen Eyes](ancestryfeatures/feat-00-y1EmCv2cEb5hXBwx.htm)|Yeux perçants|`@Compendium[pf2e.ancestryfeatures.y1EmCv2cEb5hXBwx]`|
|[Land on Your Feet](ancestryfeatures/feat-01-sL1hHxrHdMNIZVAd.htm)|Sur vos pattes|`@Compendium[pf2e.ancestryfeatures.sL1hHxrHdMNIZVAd]`|
|[Leaf Leshy](ancestryfeatures/feat-01-dQqurQys37aJYb26.htm)|Léchi feuillu|`@Compendium[pf2e.ancestryfeatures.dQqurQys37aJYb26]`|
|[Longsnout Rat](ancestryfeatures/feat-01-fROPRHGyUn4PgcER.htm)|Rat à long museau|`@Compendium[pf2e.ancestryfeatures.fROPRHGyUn4PgcER]`|
|[Low-Light Vision](ancestryfeatures/feat-01-DRtaqOHXTRtGRIUT.htm)|Vision nocturne|`@Compendium[pf2e.ancestryfeatures.DRtaqOHXTRtGRIUT]`|
|[Mountainkeeper Tengu](ancestryfeatures/feat-01-ptpK6H1rM4Bu3ry4.htm)|Tengu gardemontagne|`@Compendium[pf2e.ancestryfeatures.ptpK6H1rM4Bu3ry4]`|
|[Nine Lives Catfolk](ancestryfeatures/feat-01-hOPOyyt7qZXYYCOU.htm)|Homme-félin aux neuf vies|`@Compendium[pf2e.ancestryfeatures.hOPOyyt7qZXYYCOU]`|
|[Nomadic Halfling](ancestryfeatures/feat-01-idGDjqi1q3Ft8bAZ.htm)|Halfelin nomade|`@Compendium[pf2e.ancestryfeatures.idGDjqi1q3Ft8bAZ]`|
|[Oathkeeper Dwarf](ancestryfeatures/feat-01-85tRKGZUTFa6pKpG.htm)|Nain gardeserment|`@Compendium[pf2e.ancestryfeatures.85tRKGZUTFa6pKpG]`|
|[Observant Halfling](ancestryfeatures/feat-01-Lp7ywxabmm88Gei6.htm)|Halfelin observateur|`@Compendium[pf2e.ancestryfeatures.Lp7ywxabmm88Gei6]`|
|[Paddler Shoony](ancestryfeatures/feat-01-mnhpCk9dIwMuFegM.htm)|Shoony Pagayeur|`@Compendium[pf2e.ancestryfeatures.mnhpCk9dIwMuFegM]`|
|[Plant Nourishment](ancestryfeatures/feat-01-Sm3tKetM6kddTio3.htm)|Alimentation des plantes|`@Compendium[pf2e.ancestryfeatures.Sm3tKetM6kddTio3]`|
|[Rainfall Orc](ancestryfeatures/feat-01-dwKCwwtWetvPmJks.htm)|Orque des moussons|`@Compendium[pf2e.ancestryfeatures.dwKCwwtWetvPmJks]`|
|[Razortooth Goblin](ancestryfeatures/feat-01-a6F2WjYU8D0suT8T.htm)|Gobelin Dent’rasoir|`@Compendium[pf2e.ancestryfeatures.a6F2WjYU8D0suT8T]`|
|[Rock Dwarf](ancestryfeatures/feat-01-VSyOvtgJ1ZNpIVgC.htm)|Nain des roches|`@Compendium[pf2e.ancestryfeatures.VSyOvtgJ1ZNpIVgC]`|
|[Runtboss Hobgoblin](ancestryfeatures/feat-01-CzOHITB2ihLGqMuJ.htm)|Hobgobelin commandant avorton|`@Compendium[pf2e.ancestryfeatures.CzOHITB2ihLGqMuJ]`|
|[Sandstrider Lizardfolk](ancestryfeatures/feat-01-6xxXtgj3fcCi53lt.htm)|Homme-lézard arpenteur des sables|`@Compendium[pf2e.ancestryfeatures.6xxXtgj3fcCi53lt]`|
|[Seer Elf](ancestryfeatures/feat-01-jEtVesbqYcWGbBYk.htm)|Elfe visionnaire|`@Compendium[pf2e.ancestryfeatures.jEtVesbqYcWGbBYk]`|
|[Sensate Gnome](ancestryfeatures/feat-01-tXC5Gwn9D5x0ouJh.htm)|Gnome sensitif|`@Compendium[pf2e.ancestryfeatures.tXC5Gwn9D5x0ouJh]`|
|[Sewer Rat](ancestryfeatures/feat-01-eFsD7W6hnK33jlDQ.htm)|Rat d'égoûts|`@Compendium[pf2e.ancestryfeatures.eFsD7W6hnK33jlDQ]`|
|[Shadow Rat](ancestryfeatures/feat-01-tarfuEXmi0E0Enfy.htm)|Rat de l'ombre|`@Compendium[pf2e.ancestryfeatures.tarfuEXmi0E0Enfy]`|
|[Sharp Beak](ancestryfeatures/feat-00-qJD3PJdoSXFrZEwr.htm)|Bec aiguisé|`@Compendium[pf2e.ancestryfeatures.qJD3PJdoSXFrZEwr]`|
|[Skilled Heritage](ancestryfeatures/feat-01-zVf0Hlp5xG0Q7kmc.htm)|Héritage talentueux|`@Compendium[pf2e.ancestryfeatures.zVf0Hlp5xG0Q7kmc]`|
|[Skyborn Tengu](ancestryfeatures/feat-01-rKV11HWREwjjMIum.htm)|Tengu des cieux|`@Compendium[pf2e.ancestryfeatures.rKV11HWREwjjMIum]`|
|[Smokeworker Hobgoblin](ancestryfeatures/feat-01-n2eJEjA8pnOMiuCm.htm)|Hobgobelin travailleur de fumée|`@Compendium[pf2e.ancestryfeatures.n2eJEjA8pnOMiuCm]`|
|[Snow Goblin](ancestryfeatures/feat-01-gQyPU441J3rGt8mD.htm)|Gobelin des Neiges|`@Compendium[pf2e.ancestryfeatures.gQyPU441J3rGt8mD]`|
|[Spellscale Kobold](ancestryfeatures/feat-01-VRyX00OuPGsJSurM.htm)|Kobold écaille de sort|`@Compendium[pf2e.ancestryfeatures.VRyX00OuPGsJSurM]`|
|[Stormtossed Tengu](ancestryfeatures/feat-01-7vHLPleFpSqKAjWG.htm)|Tengu battu par les vents|`@Compendium[pf2e.ancestryfeatures.7vHLPleFpSqKAjWG]`|
|[Strong-Blooded Dwarf](ancestryfeatures/feat-01-UV2sABrTC5teOXTE.htm)|Nain sang-fort|`@Compendium[pf2e.ancestryfeatures.UV2sABrTC5teOXTE]`|
|[Strongjaw Kobold](ancestryfeatures/feat-01-WaCn0mcivFv1omNK.htm)|Kobold à forte mâchoire|`@Compendium[pf2e.ancestryfeatures.WaCn0mcivFv1omNK]`|
|[Tailed Goblin](ancestryfeatures/feat-01-8wGUh9RsMUamOKjh.htm)|Gobelin à queue|`@Compendium[pf2e.ancestryfeatures.8wGUh9RsMUamOKjh]`|
|[Taloned Tengu](ancestryfeatures/feat-01-qM566kCXljkOpocA.htm)|Tengu à ergots|`@Compendium[pf2e.ancestryfeatures.qM566kCXljkOpocA]`|
|[Thickcoat Shoony](ancestryfeatures/feat-01-MhXHEh7utEfxBwmc.htm)|Shoony à fourrure|`@Compendium[pf2e.ancestryfeatures.MhXHEh7utEfxBwmc]`|
|[Tiefling](ancestryfeatures/feat-01-wGMVflH4t1UXrNn5.htm)|Tieffelin|`@Compendium[pf2e.ancestryfeatures.wGMVflH4t1UXrNn5]`|
|[Treedweller Goblin](ancestryfeatures/feat-01-EEvA4uj8h3zDiAfP.htm)|Gobelin arboricole|`@Compendium[pf2e.ancestryfeatures.EEvA4uj8h3zDiAfP]`|
|[Twilight Halfling](ancestryfeatures/feat-01-cCy8vsZENlwiAyZ6.htm)|Halfelin du crépuscule|`@Compendium[pf2e.ancestryfeatures.cCy8vsZENlwiAyZ6]`|
|[Umbral Gnome](ancestryfeatures/feat-01-d0bNxgGqvaCkFlhN.htm)|Gnome ombral|`@Compendium[pf2e.ancestryfeatures.d0bNxgGqvaCkFlhN]`|
|[Unbreakable Goblin](ancestryfeatures/feat-01-ULj56ZoW7dWdnBvu.htm)|Gobelin Incassable|`@Compendium[pf2e.ancestryfeatures.ULj56ZoW7dWdnBvu]`|
|[Unseen Lizardfolk](ancestryfeatures/feat-01-VYfpTUuXJM3iBOz0.htm)|Homme-lézard mimétique|`@Compendium[pf2e.ancestryfeatures.VYfpTUuXJM3iBOz0]`|
|[Venomtail Kobold](ancestryfeatures/feat-01-m9rrlchS10xHFA2G.htm)|Kobold à queue venimeuse|`@Compendium[pf2e.ancestryfeatures.m9rrlchS10xHFA2G]`|
|[Versatile Heritage](ancestryfeatures/feat-01-hFBwsVcSnNCJoimo.htm)|Héritage polyvalent|`@Compendium[pf2e.ancestryfeatures.hFBwsVcSnNCJoimo]`|
|[Vine Leshy](ancestryfeatures/feat-01-evXJISqyhl3fHE9u.htm)|Léchi à liane|`@Compendium[pf2e.ancestryfeatures.evXJISqyhl3fHE9u]`|
|[Vivacious Gnome](ancestryfeatures/feat-01-ViKRoVgog172r163.htm)|Gnome vivace|`@Compendium[pf2e.ancestryfeatures.ViKRoVgog172r163]`|
|[Warmarch Hobgoblin](ancestryfeatures/feat-01-K124fCpU03SJvmeP.htm)|Hobgobelin Marchebataille|`@Compendium[pf2e.ancestryfeatures.K124fCpU03SJvmeP]`|
|[Warrenbred Hobgoblin](ancestryfeatures/feat-01-0vaeOoECfVD5EGbq.htm)|Hobgobelin Terrierné|`@Compendium[pf2e.ancestryfeatures.0vaeOoECfVD5EGbq]`|
|[Wellspring Gnome](ancestryfeatures/feat-01-etkuQkjkNLPLnjkA.htm)|Gnome source|`@Compendium[pf2e.ancestryfeatures.etkuQkjkNLPLnjkA]`|
|[Wetlander Lizardfolk](ancestryfeatures/feat-01-dJeiekfqGQ8dkwsO.htm)|Homme-lézard des terres humides|`@Compendium[pf2e.ancestryfeatures.dJeiekfqGQ8dkwsO]`|
|[Whisper Elf](ancestryfeatures/feat-01-1wVDYY9Wue0G5R9Q.htm)|Elfe des murmures|`@Compendium[pf2e.ancestryfeatures.1wVDYY9Wue0G5R9Q]`|
|[Wildwood Halfling](ancestryfeatures/feat-01-xtRIYizCjLg9qe1Z.htm)|Halfelin bois-sauvage|`@Compendium[pf2e.ancestryfeatures.xtRIYizCjLg9qe1Z]`|
|[Winter Catfolk](ancestryfeatures/feat-01-6dMd4JG0ndrObEUj.htm)|Homme-félin hivernal|`@Compendium[pf2e.ancestryfeatures.6dMd4JG0ndrObEUj]`|
|[Wintertouched Human](ancestryfeatures/feat-01-KO33MNyY9VqNQmbZ.htm)|Humain touché par l'hiver|`@Compendium[pf2e.ancestryfeatures.KO33MNyY9VqNQmbZ]`|
|[Woodland Elf](ancestryfeatures/feat-01-nW1gi13E62Feto2w.htm)|Elfe des bois|`@Compendium[pf2e.ancestryfeatures.nW1gi13E62Feto2w]`|


## Backgrounds

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Acolyte](backgrounds/CAjQrHZZbALE7Qjy.htm)|Acolyte|`@Compendium[pf2e.backgrounds.CAjQrHZZbALE7Qjy]`|
|[Acrobat](backgrounds/IFHYbU6Nu8BiTsRa.htm)|Acrobate|`@Compendium[pf2e.backgrounds.IFHYbU6Nu8BiTsRa]`|
|[Aerialist](backgrounds/qY4IUwVWIKPSFskP.htm)|Trapéziste|`@Compendium[pf2e.backgrounds.qY4IUwVWIKPSFskP]`|
|[Aiudara Seeker](backgrounds/3kXTGUvodNMnJTxb.htm)|Chercheur d'aiudara|`@Compendium[pf2e.backgrounds.3kXTGUvodNMnJTxb]`|
|[Alkenstar Tinker](backgrounds/0c9Np7Yq5JSxZ6Tb.htm)|Bricoleur d'Alkenstar|`@Compendium[pf2e.backgrounds.0c9Np7Yq5JSxZ6Tb]`|
|[Amnesiac](backgrounds/0ZfBP7Tp2P3WN7Dp.htm)|Amnésique|`@Compendium[pf2e.backgrounds.0ZfBP7Tp2P3WN7Dp]`|
|[Animal Whisperer](backgrounds/5RGLAPi5sLykRcmm.htm)|Dresseur|`@Compendium[pf2e.backgrounds.5RGLAPi5sLykRcmm]`|
|[Animal Wrangler (Athletics)](backgrounds/HNp0uNsIx3BNBcr5.htm)||`@Compendium[pf2e.backgrounds.HNp0uNsIx3BNBcr5]`|
|[Animal Wrangler (Nature)](backgrounds/TPZ0ev0Tl5sveZuM.htm)||`@Compendium[pf2e.backgrounds.TPZ0ev0Tl5sveZuM]`|
|[Archaeologist](backgrounds/IoBhge83aYpq0pPV.htm)|Archéologue|`@Compendium[pf2e.backgrounds.IoBhge83aYpq0pPV]`|
|[Artisan](backgrounds/6irgRkKZ8tRZzLvs.htm)|Artisan|`@Compendium[pf2e.backgrounds.6irgRkKZ8tRZzLvs]`|
|[Artist](backgrounds/y9OyNsxGfmjqdcP0.htm)|Artiste|`@Compendium[pf2e.backgrounds.y9OyNsxGfmjqdcP0]`|
|[Aspiring Free-Captain](backgrounds/ap25MWBuFGwwhYIG.htm)||`@Compendium[pf2e.backgrounds.ap25MWBuFGwwhYIG]`|
|[Aspiring River Monarch](backgrounds/gfklP8ub45R4wXKe.htm)|Aspirant Monarque du Fleuve|`@Compendium[pf2e.backgrounds.gfklP8ub45R4wXKe]`|
|[Atteran Rancher](backgrounds/eYY3bX7xSH7aicqT.htm)|Rancher Atteran|`@Compendium[pf2e.backgrounds.eYY3bX7xSH7aicqT]`|
|[Bandit](backgrounds/6abqATPjYoF946LD.htm)|Bandit|`@Compendium[pf2e.backgrounds.6abqATPjYoF946LD]`|
|[Barber](backgrounds/wudDO9OEsRjJsqhU.htm)|Barbier|`@Compendium[pf2e.backgrounds.wudDO9OEsRjJsqhU]`|
|[Barkeep](backgrounds/a8BmnIIUR7AYog5B.htm)|Tavernier|`@Compendium[pf2e.backgrounds.a8BmnIIUR7AYog5B]`|
|[Barker](backgrounds/z4cCsOT36MB7xldR.htm)|Aboyeur|`@Compendium[pf2e.backgrounds.z4cCsOT36MB7xldR]`|
|[Barrister](backgrounds/Phvnfdmz4bB7jrI3.htm)|Avocat|`@Compendium[pf2e.backgrounds.Phvnfdmz4bB7jrI3]`|
|[Bekyar Restorer](backgrounds/9LnXsMRwYcxi7nDO.htm)|Restaurateur Bekyar|`@Compendium[pf2e.backgrounds.9LnXsMRwYcxi7nDO]`|
|[Belkzen Slayer](backgrounds/EJRWGsPWzAhixuvQ.htm)|Tueur du Belkzen|`@Compendium[pf2e.backgrounds.EJRWGsPWzAhixuvQ]`|
|[Bellflower Agent](backgrounds/6K6jJkjZ2MJYqQ6h.htm)|Agent de la Campanule|`@Compendium[pf2e.backgrounds.6K6jJkjZ2MJYqQ6h]`|
|[Bibliophile](backgrounds/xvz5F7iYBWEIjz0r.htm)||`@Compendium[pf2e.backgrounds.xvz5F7iYBWEIjz0r]`|
|[Black Market Smuggler](backgrounds/I0vuIFypx8ADSJQC.htm)|Contrebandier du Marché noir|`@Compendium[pf2e.backgrounds.I0vuIFypx8ADSJQC]`|
|[Blessed](backgrounds/9pK15dQJVypSCjzO.htm)|Béni|`@Compendium[pf2e.backgrounds.9pK15dQJVypSCjzO]`|
|[Blow-In (Deception)](backgrounds/tsMvqxJhl6xgcDaU.htm)||`@Compendium[pf2e.backgrounds.tsMvqxJhl6xgcDaU]`|
|[Blow-In (Thievery)](backgrounds/3hKnDmRXtF3LGmjy.htm)||`@Compendium[pf2e.backgrounds.3hKnDmRXtF3LGmjy]`|
|[Bonuwat Wavetouched](backgrounds/kLjqmylGOXeQ5o5Y.htm)|Bonuwat Touché par les vagues|`@Compendium[pf2e.backgrounds.kLjqmylGOXeQ5o5Y]`|
|[Bookkeeper](backgrounds/o1lhSKOpKPamTITI.htm)|Comptable|`@Compendium[pf2e.backgrounds.o1lhSKOpKPamTITI]`|
|[Bounty Hunter](backgrounds/a9Q4iIiAZryVWN27.htm)|Chasseur de primes|`@Compendium[pf2e.backgrounds.a9Q4iIiAZryVWN27]`|
|[Bright Lion](backgrounds/JfGVkZkaoz2lnmov.htm)|Lion brillant|`@Compendium[pf2e.backgrounds.JfGVkZkaoz2lnmov]`|
|[Butcher](backgrounds/nhQKn1tVV6PKCurq.htm)|"Boucher"|`@Compendium[pf2e.backgrounds.nhQKn1tVV6PKCurq]`|
|[Charlatan](backgrounds/t0t1ck8iKpaI4o5W.htm)|Charlatan|`@Compendium[pf2e.backgrounds.t0t1ck8iKpaI4o5W]`|
|[Chelish Rebel](backgrounds/tA0nggrWfBEhvsKA.htm)|Rebelle chélaxien|`@Compendium[pf2e.backgrounds.tA0nggrWfBEhvsKA]`|
|[Child of Westcrown](backgrounds/uNvD4XK1kdvGjQVo.htm)|Enfant de Couronne d'Ouest|`@Compendium[pf2e.backgrounds.uNvD4XK1kdvGjQVo]`|
|[Child of the Puddles](backgrounds/irDibuV3Wi7T43sL.htm)|Marmot des flaques|`@Compendium[pf2e.backgrounds.irDibuV3Wi7T43sL]`|
|[Circus Born](backgrounds/YJpEdmSOjlA2QZeu.htm)|Circassien|`@Compendium[pf2e.backgrounds.YJpEdmSOjlA2QZeu]`|
|[Clown](backgrounds/DHrzVqB8f1ed3zTk.htm)|Clown|`@Compendium[pf2e.backgrounds.DHrzVqB8f1ed3zTk]`|
|[Cook](backgrounds/3gN09dOT2hMwGcK2.htm)|Cuisinier|`@Compendium[pf2e.backgrounds.3gN09dOT2hMwGcK2]`|
|[Courier](backgrounds/DBxOUwM7qhGH8MrF.htm)|Garçon de course|`@Compendium[pf2e.backgrounds.DBxOUwM7qhGH8MrF]`|
|[Criminal](backgrounds/9lVw1JGl5ser6626.htm)|Criminel|`@Compendium[pf2e.backgrounds.9lVw1JGl5ser6626]`|
|[Cultist](backgrounds/fuTLDmihr9Z9e5wa.htm)|Cultiste|`@Compendium[pf2e.backgrounds.fuTLDmihr9Z9e5wa]`|
|[Cursed](backgrounds/j2G71vQahw1DiWpO.htm)|Maudit|`@Compendium[pf2e.backgrounds.j2G71vQahw1DiWpO]`|
|[Cursed Family](backgrounds/ajcpRVb5EG00l7Y4.htm)|Famille maudite|`@Compendium[pf2e.backgrounds.ajcpRVb5EG00l7Y4]`|
|[Deckhand](backgrounds/IObZEUz8wneEMgR3.htm)|Homme de pont|`@Compendium[pf2e.backgrounds.IObZEUz8wneEMgR3]`|
|[Demon Slayer](backgrounds/NywLl1XMQmzA6rP7.htm)|Tueur de démons|`@Compendium[pf2e.backgrounds.NywLl1XMQmzA6rP7]`|
|[Desert Tracker](backgrounds/2qH61dLeaqgNOdOp.htm)|Pisteur du désert|`@Compendium[pf2e.backgrounds.2qH61dLeaqgNOdOp]`|
|[Detective](backgrounds/dVRDDjT4FOu6uLDR.htm)|Détective|`@Compendium[pf2e.backgrounds.dVRDDjT4FOu6uLDR]`|
|[Diobel Pearl Diver](backgrounds/3wLnNwWnZ2dHIbV4.htm)|Pêcheur de perles de Diobel|`@Compendium[pf2e.backgrounds.3wLnNwWnZ2dHIbV4]`|
|[Dragon Scholar](backgrounds/GPI5kNu0xfom9kKa.htm)|Spécialiste des dragons|`@Compendium[pf2e.backgrounds.GPI5kNu0xfom9kKa]`|
|[Dreamer of the Verdant Moon](backgrounds/vBPu7RwNXGDQ1ThL.htm)|Rêveur de la lune verdoyante|`@Compendium[pf2e.backgrounds.vBPu7RwNXGDQ1ThL]`|
|[Droskari Disciple](backgrounds/XHY0xrSSbX0cTJKK.htm)|Disciple de Droskar|`@Compendium[pf2e.backgrounds.XHY0xrSSbX0cTJKK]`|
|[Early Explorer](backgrounds/p27PSjFtHAWikKaw.htm)|Explorateur précoce|`@Compendium[pf2e.backgrounds.p27PSjFtHAWikKaw]`|
|[Eldritch Anatomist](backgrounds/MiRWGXZnEdurMvVf.htm)||`@Compendium[pf2e.backgrounds.MiRWGXZnEdurMvVf]`|
|[Emancipated](backgrounds/oEm937kNrP5sXxFD.htm)|Émancipé|`@Compendium[pf2e.backgrounds.oEm937kNrP5sXxFD]`|
|[Emissary](backgrounds/3WPo7m6rJQh9L7MN.htm)|Émissaire|`@Compendium[pf2e.backgrounds.3WPo7m6rJQh9L7MN]`|
|[Entertainer](backgrounds/i4hN6OYv8qmi3GLW.htm)|Bateleur|`@Compendium[pf2e.backgrounds.i4hN6OYv8qmi3GLW]`|
|[Ex-Con Token Guard](backgrounds/CKU1sbFofcwZUJMx.htm)|Garde des Pièces ancien taulard|`@Compendium[pf2e.backgrounds.CKU1sbFofcwZUJMx]`|
|[Faction Opportunist](backgrounds/HKRcQO8Xj7xzBxAw.htm)|Opportuniste des Factions|`@Compendium[pf2e.backgrounds.HKRcQO8Xj7xzBxAw]`|
|[Farmhand](backgrounds/SOmJyAtPOokesZoe.htm)|Ouvrier agricole|`@Compendium[pf2e.backgrounds.SOmJyAtPOokesZoe]`|
|[Feral Child](backgrounds/Q2brdDtEoI3cmpuD.htm)|Enfant sauvage|`@Compendium[pf2e.backgrounds.Q2brdDtEoI3cmpuD]`|
|[Feybound](backgrounds/ppBGlWl0UkBKkJgE.htm)|Engagé avec les fées|`@Compendium[pf2e.backgrounds.ppBGlWl0UkBKkJgE]`|
|[Field Medic](backgrounds/FKHut73XDUGTnKkP.htm)|Médecin militaire|`@Compendium[pf2e.backgrounds.FKHut73XDUGTnKkP]`|
|[Finadar Leshy](backgrounds/NZY0r4Csjul6eVPp.htm)|Léchi de Finadar|`@Compendium[pf2e.backgrounds.NZY0r4Csjul6eVPp]`|
|[Final Blade Survivor](backgrounds/tQ9t7uIssRCR2y3W.htm)|Survivant de la Lame finale|`@Compendium[pf2e.backgrounds.tQ9t7uIssRCR2y3W]`|
|[Fogfen Tale-Teller](backgrounds/3M2FRDlunjFshzbq.htm)||`@Compendium[pf2e.backgrounds.3M2FRDlunjFshzbq]`|
|[Former Aspis Agent](backgrounds/vgin9ff2sUBMpuaI.htm)|Ancien Agent de l'Aspis|`@Compendium[pf2e.backgrounds.vgin9ff2sUBMpuaI]`|
|[Fortune Teller](backgrounds/3frMfODIYFeqTl2k.htm)|Voyant|`@Compendium[pf2e.backgrounds.3frMfODIYFeqTl2k]`|
|[Freed Slave of Absalom](backgrounds/a5dCSuAwGE2hqQjj.htm)|Esclave libéré d'Absalom|`@Compendium[pf2e.backgrounds.a5dCSuAwGE2hqQjj]`|
|[Gambler](backgrounds/89LEOv97ZwsjnhNx.htm)|Parieur|`@Compendium[pf2e.backgrounds.89LEOv97ZwsjnhNx]`|
|[Geb Crusader](backgrounds/bDyb0k0rTfDTyhd8.htm)|Croisé du Geb|`@Compendium[pf2e.backgrounds.bDyb0k0rTfDTyhd8]`|
|[Gladiator](backgrounds/88WyCqU5x1eJ0MK2.htm)|Gladiateur|`@Compendium[pf2e.backgrounds.88WyCqU5x1eJ0MK2]`|
|[Goblinblood Orphan](backgrounds/4fBIXtSVSRYn2ZGi.htm)|Orphelin des guerres du Sang gobelin|`@Compendium[pf2e.backgrounds.4fBIXtSVSRYn2ZGi]`|
|[Godless Graycloak](backgrounds/1VdLr4Qm8fv1m4tM.htm)||`@Compendium[pf2e.backgrounds.1VdLr4Qm8fv1m4tM]`|
|[Grand Council Bureaucrat](backgrounds/yK40c3082U30BUX5.htm)|Bureaucrate du Grand Conseil|`@Compendium[pf2e.backgrounds.yK40c3082U30BUX5]`|
|[Grizzled Muckrucker](backgrounds/xCCvT9tprRQVFVDq.htm)|Fangeux grisonnant|`@Compendium[pf2e.backgrounds.xCCvT9tprRQVFVDq]`|
|[Guard](backgrounds/6UmhTxOQeqFnppxx.htm)|Garde|`@Compendium[pf2e.backgrounds.6UmhTxOQeqFnppxx]`|
|[Harbor Guard Moonlighter](backgrounds/lCR8gyEZbwqh3RWi.htm)|Noctambule de la Garde du port|`@Compendium[pf2e.backgrounds.lCR8gyEZbwqh3RWi]`|
|[Harrow-Led](backgrounds/RgFOKlEmMIw2eZpo.htm)|Déterminé par le Tourment|`@Compendium[pf2e.backgrounds.RgFOKlEmMIw2eZpo]`|
|[Haunted](backgrounds/pGOlKz4Krnh7MyUM.htm)|Hanté|`@Compendium[pf2e.backgrounds.pGOlKz4Krnh7MyUM]`|
|[Haunting Vision](backgrounds/0z0PSizHviOehdJF.htm)|Hanté par une vision|`@Compendium[pf2e.backgrounds.0z0PSizHviOehdJF]`|
|[Hellknight Historian](backgrounds/vNWSzv36L1GBPPoc.htm)|Historien des chevaliers infernaux|`@Compendium[pf2e.backgrounds.vNWSzv36L1GBPPoc]`|
|[Herbalist](backgrounds/ZdhPKEY9FfaOS8Wy.htm)|Herboriste|`@Compendium[pf2e.backgrounds.ZdhPKEY9FfaOS8Wy]`|
|[Hermean Expatriate](backgrounds/bh6O2Ad5mkYwRngM.htm)|Expatrié d'Herméa|`@Compendium[pf2e.backgrounds.bh6O2Ad5mkYwRngM]`|
|[Hermean Heritor](backgrounds/mxJRdRSMsyZfBf5c.htm)|Héritier d'Herméa|`@Compendium[pf2e.backgrounds.mxJRdRSMsyZfBf5c]`|
|[Hermit](backgrounds/4SSKsyD72AYYpzgm.htm)|Ermite|`@Compendium[pf2e.backgrounds.4SSKsyD72AYYpzgm]`|
|[Hookclaw Digger](backgrounds/aWAfj7bhTZM2oK81.htm)||`@Compendium[pf2e.backgrounds.aWAfj7bhTZM2oK81]`|
|[Hunter](backgrounds/5Z3cLEpsx9nHVwhM.htm)|Chasseur|`@Compendium[pf2e.backgrounds.5Z3cLEpsx9nHVwhM]`|
|[Inlander](backgrounds/6vsoyCZKqxG0lVe8.htm)|Habitant de l'intérieur des terres|`@Compendium[pf2e.backgrounds.6vsoyCZKqxG0lVe8]`|
|[Insurgent](backgrounds/HdnmIaLadhRfZq8X.htm)|Insurgé|`@Compendium[pf2e.backgrounds.HdnmIaLadhRfZq8X]`|
|[Iolite Trainee Hobgoblin](backgrounds/fML6YrXYDqQy0g7L.htm)|Recrue iolite hobgobeline|`@Compendium[pf2e.backgrounds.fML6YrXYDqQy0g7L]`|
|[Issian Partisan](backgrounds/8UEKgUkagUDixkL2.htm)|Partisan Issien|`@Compendium[pf2e.backgrounds.8UEKgUkagUDixkL2]`|
|[Kalistrade Follower](backgrounds/hPx0xiv00GQqPWUH.htm)|Suivant de Kalistrade|`@Compendium[pf2e.backgrounds.hPx0xiv00GQqPWUH]`|
|[Kyonin Emissary](backgrounds/pBX18FI1grWwkWjk.htm)|Émissaire du Kyonin|`@Compendium[pf2e.backgrounds.pBX18FI1grWwkWjk]`|
|[Laborer](backgrounds/2bzqI0D4J3LUi8nq.htm)|Manoeuvre|`@Compendium[pf2e.backgrounds.2bzqI0D4J3LUi8nq]`|
|[Lastwall Survivor](backgrounds/ffcNsTUBsxFwbNgJ.htm)|Survivant de Dernier-Rempart|`@Compendium[pf2e.backgrounds.ffcNsTUBsxFwbNgJ]`|
|[Learned Guard Prodigy](backgrounds/nnBAO4NwdINCGQFK.htm)|Prodige de la Garde Instruite|`@Compendium[pf2e.backgrounds.nnBAO4NwdINCGQFK]`|
|[Legendary Parents](backgrounds/84uVpQFCqn0Atfpo.htm)|Parents légendaires|`@Compendium[pf2e.backgrounds.84uVpQFCqn0Atfpo]`|
|[Lesser Scion](backgrounds/BBeJA7n0xpSsBCGq.htm)|Benjamin|`@Compendium[pf2e.backgrounds.BBeJA7n0xpSsBCGq]`|
|[Local Scion](backgrounds/4a2sVO0o2mMTydN8.htm)|Enfant du pays|`@Compendium[pf2e.backgrounds.4a2sVO0o2mMTydN8]`|
|[Lost and Alone](backgrounds/wU1qd8tZNcYn43y2.htm)|Perdu et seul|`@Compendium[pf2e.backgrounds.wU1qd8tZNcYn43y2]`|
|[Lumber Consortium Laborer](backgrounds/uJcFanGjVranEarv.htm)|Ouvrier du Consortium du Bois|`@Compendium[pf2e.backgrounds.uJcFanGjVranEarv]`|
|[Magaambya Academic](backgrounds/dd6DbCsT67rl8va3.htm)|Étudiant de Magaambya|`@Compendium[pf2e.backgrounds.dd6DbCsT67rl8va3]`|
|[Mammoth Speaker](backgrounds/76RK9WizWYdyhMy5.htm)|Porte parole mammouth|`@Compendium[pf2e.backgrounds.76RK9WizWYdyhMy5]`|
|[Mana Wastes Refugee](backgrounds/V1RAIckpUJd2OzXi.htm)|Réfugié de la Désolation de Mana|`@Compendium[pf2e.backgrounds.V1RAIckpUJd2OzXi]`|
|[Mantis Scion](backgrounds/4aVFnYyRajog0mNl.htm)|Fils de la Mante|`@Compendium[pf2e.backgrounds.4aVFnYyRajog0mNl]`|
|[Market Runner](backgrounds/n2JN5Kiu7tOCAHPr.htm)||`@Compendium[pf2e.backgrounds.n2JN5Kiu7tOCAHPr]`|
|[Martial Disciple (Acrobatics)](backgrounds/lRVYgV9zL6O6O3U4.htm)||`@Compendium[pf2e.backgrounds.lRVYgV9zL6O6O3U4]`|
|[Martial Disciple (Athletics)](backgrounds/faHydq29Q4RP5CAK.htm)||`@Compendium[pf2e.backgrounds.faHydq29Q4RP5CAK]`|
|[Menagerie Dung Sweeper](backgrounds/YyzIzLxn2UCFubj4.htm)|Balayeur de fumier de la Ménagerie|`@Compendium[pf2e.backgrounds.YyzIzLxn2UCFubj4]`|
|[Merabite Prodigy](backgrounds/a45LqkSRX07ljKdW.htm)|Prodige Merabite|`@Compendium[pf2e.backgrounds.a45LqkSRX07ljKdW]`|
|[Merchant](backgrounds/KMv7ollLVaZ81XDV.htm)|Marchand|`@Compendium[pf2e.backgrounds.KMv7ollLVaZ81XDV]`|
|[Miner](backgrounds/bCJ9p3P5uJDAtaUI.htm)|Mineur|`@Compendium[pf2e.backgrounds.bCJ9p3P5uJDAtaUI]`|
|[Missionary](backgrounds/GNidqGnSABx1rQUQ.htm)|Missionnaire|`@Compendium[pf2e.backgrounds.GNidqGnSABx1rQUQ]`|
|[Molthuni Mercenary](backgrounds/lqjmBmGHYRaSiglZ.htm)|Mercenaire du Molthune|`@Compendium[pf2e.backgrounds.lqjmBmGHYRaSiglZ]`|
|[Mystic Seer](backgrounds/Y50ssWBBKSRVBpSa.htm)|Voyant mystique|`@Compendium[pf2e.backgrounds.Y50ssWBBKSRVBpSa]`|
|[Nexian Mystic](backgrounds/Y35nOXZRryiyHjlk.htm)|Mystique Nexien|`@Compendium[pf2e.backgrounds.Y35nOXZRryiyHjlk]`|
|[Nirmathi Guerrilla](backgrounds/E2ij2Cg8oMC0W0NS.htm)|Escarmoucheur Nirmathi|`@Compendium[pf2e.backgrounds.E2ij2Cg8oMC0W0NS]`|
|[Noble](backgrounds/Yu7Cl0Lk94LdPRi6.htm)|Noble|`@Compendium[pf2e.backgrounds.Yu7Cl0Lk94LdPRi6]`|
|[Nomad](backgrounds/8UXahQfkP9GZ1TNW.htm)|Nomade|`@Compendium[pf2e.backgrounds.8UXahQfkP9GZ1TNW]`|
|[Oenopion-Ooze Tender](backgrounds/iWWg16f3re1YChiD.htm)||`@Compendium[pf2e.backgrounds.iWWg16f3re1YChiD]`|
|[Onyx Trader](backgrounds/WueM94C9JXk10jPd.htm)|Marchand d'Onyx|`@Compendium[pf2e.backgrounds.WueM94C9JXk10jPd]`|
|[Osirionologist](backgrounds/AJ41zFEYwlOUghXp.htm)|Osirionologiste|`@Compendium[pf2e.backgrounds.AJ41zFEYwlOUghXp]`|
|[Out-Of-Towner](backgrounds/UgityMZaujmYUpil.htm)||`@Compendium[pf2e.backgrounds.UgityMZaujmYUpil]`|
|[Outrider](backgrounds/dAvFZ5QmbAHgXcNp.htm)|Éclaireur|`@Compendium[pf2e.backgrounds.dAvFZ5QmbAHgXcNp]`|
|[Pathfinder Hopeful](backgrounds/j9v38iHA0sVy59SR.htm)|Aspirant Éclaireur|`@Compendium[pf2e.backgrounds.j9v38iHA0sVy59SR]`|
|[Pathfinder Recruiter](backgrounds/NXYce9NAHls2fcIf.htm)|Recruteur des Éclaireurs|`@Compendium[pf2e.backgrounds.NXYce9NAHls2fcIf]`|
|[Perfection Seeker](backgrounds/P65AGDPkhD2B4JtG.htm)|À la recherche de la Perfection|`@Compendium[pf2e.backgrounds.P65AGDPkhD2B4JtG]`|
|[Pilgrim](backgrounds/r9fzNQEz33HyKTxm.htm)|Pélerin|`@Compendium[pf2e.backgrounds.r9fzNQEz33HyKTxm]`|
|[Political Scion](backgrounds/JauSkDtMV6dhDZS8.htm)|Rejeton de politicien|`@Compendium[pf2e.backgrounds.JauSkDtMV6dhDZS8]`|
|[Post Guard of All Trades](backgrounds/DVtZab19D1vD3a0n.htm)|Garde du poste touche-à-tout|`@Compendium[pf2e.backgrounds.DVtZab19D1vD3a0n]`|
|[Press-Ganged](backgrounds/4yN5miHoMvKwZIsa.htm)|Forçat|`@Compendium[pf2e.backgrounds.4yN5miHoMvKwZIsa]`|
|[Prisoner](backgrounds/apXTV7jJx6yJpj8D.htm)|Prisonnier|`@Compendium[pf2e.backgrounds.apXTV7jJx6yJpj8D]`|
|[Purveyor of the Bizzare](backgrounds/IfpYRxN8qyV4ym0o.htm)|Fournisseur de Bizarreries|`@Compendium[pf2e.backgrounds.IfpYRxN8qyV4ym0o]`|
|[Quick](backgrounds/i6y4DiKvqitdE0PW.htm)|Rapide|`@Compendium[pf2e.backgrounds.i6y4DiKvqitdE0PW]`|
|[Raised by Belief](backgrounds/RC4l6WsxPn89a1f8.htm)|Élevé dans la croyance|`@Compendium[pf2e.backgrounds.RC4l6WsxPn89a1f8]`|
|[Razmiran Faithful](backgrounds/9uTdwJaj27F18ZDX.htm)|Croyant en Razmir|`@Compendium[pf2e.backgrounds.9uTdwJaj27F18ZDX]`|
|[Refugee (APG)](backgrounds/i28Z9JXhEvoc7BX5.htm)|Réfugié (MJA)|`@Compendium[pf2e.backgrounds.i28Z9JXhEvoc7BX5]`|
|[Refugee (Fall of Plaguestone)](backgrounds/HDquvQywAZimmcFF.htm)|Réfugié (la Chute de Plaguestone)|`@Compendium[pf2e.backgrounds.HDquvQywAZimmcFF]`|
|[Reputation Seeker](backgrounds/TPoP1mKpqUOpRQ5Y.htm)|En quête de renommée|`@Compendium[pf2e.backgrounds.TPoP1mKpqUOpRQ5Y]`|
|[Returned](backgrounds/rzyRtasSTfHS3e0y.htm)|Ressuscité|`@Compendium[pf2e.backgrounds.rzyRtasSTfHS3e0y]`|
|[Returning Descendant](backgrounds/iaM6TjvijLCgiHeD.htm)|Descendant de retour|`@Compendium[pf2e.backgrounds.iaM6TjvijLCgiHeD]`|
|[Rigger](backgrounds/7IrOApgShgnmp1A5.htm)|Gréeur|`@Compendium[pf2e.backgrounds.7IrOApgShgnmp1A5]`|
|[Rivethun Adherent](backgrounds/6c0rsuiiAaVqGTu7.htm)|Adhérent du Rivethun|`@Compendium[pf2e.backgrounds.6c0rsuiiAaVqGTu7]`|
|[Root Worker](backgrounds/ns9BOvCyjdapYhI0.htm)|Chasseur de racines|`@Compendium[pf2e.backgrounds.ns9BOvCyjdapYhI0]`|
|[Rostland Partisan](backgrounds/8q4PhvpmIxZD7rsV.htm)|Partisan du Rost|`@Compendium[pf2e.backgrounds.8q4PhvpmIxZD7rsV]`|
|[Royalty](backgrounds/uC6D2nmDTATxXrV6.htm)|De sang royal|`@Compendium[pf2e.backgrounds.uC6D2nmDTATxXrV6]`|
|[Ruby Phoenix Enthusiast](backgrounds/7K6ZSWOoihZKSdyd.htm)|Enthousiaste du Phénix de rubis|`@Compendium[pf2e.backgrounds.7K6ZSWOoihZKSdyd]`|
|[Ruin Delver](backgrounds/5qUQOpdlNsJjpFVX.htm)||`@Compendium[pf2e.backgrounds.5qUQOpdlNsJjpFVX]`|
|[Sailor](backgrounds/Zmwyhsxe4i6rZN75.htm)|Marin|`@Compendium[pf2e.backgrounds.Zmwyhsxe4i6rZN75]`|
|[Sally Guard Neophyte](backgrounds/vE6nb2OSIXqprDXk.htm)|Néophyte de la Garde des Percées|`@Compendium[pf2e.backgrounds.vE6nb2OSIXqprDXk]`|
|[Sarkorian Reclaimer](backgrounds/khGFmnQMBYmz2ONR.htm)|Réclamateur du Sarkoris|`@Compendium[pf2e.backgrounds.khGFmnQMBYmz2ONR]`|
|[Sarkorian Survivor](backgrounds/B8kEwzPUMIjhofUm.htm)|Survivant du sarkoris|`@Compendium[pf2e.backgrounds.B8kEwzPUMIjhofUm]`|
|[Savior of Air](backgrounds/locc0cjOmOQHe3j7.htm)|Sauveur de l'air|`@Compendium[pf2e.backgrounds.locc0cjOmOQHe3j7]`|
|[Scavenger](backgrounds/vHeP960qjhfob4Je.htm)|Récupérateur|`@Compendium[pf2e.backgrounds.vHeP960qjhfob4Je]`|
|[Scholar](backgrounds/WRVEUkemqj2uNHwl.htm)|Érudit|`@Compendium[pf2e.backgrounds.WRVEUkemqj2uNHwl]`|
|[Scholar of the Ancients](backgrounds/i5G6E5dkGWiq838C.htm)|Étudiant de l'Antiquité|`@Compendium[pf2e.backgrounds.i5G6E5dkGWiq838C]`|
|[Scholar of the Sky Key](backgrounds/UyddtAwqDGjQ1SZK.htm)|Étudiant de la Clé du Ciel|`@Compendium[pf2e.backgrounds.UyddtAwqDGjQ1SZK]`|
|[Scout](backgrounds/o7RbsQbv5iLRvd8j.htm)|Éclaireur|`@Compendium[pf2e.backgrounds.o7RbsQbv5iLRvd8j]`|
|[Secular Medic](backgrounds/y0WZS51fSi6dILHq.htm)|Médecin séculier|`@Compendium[pf2e.backgrounds.y0WZS51fSi6dILHq]`|
|[Senghor Sailor](backgrounds/lav3yRNPc7lQ7e9k.htm)|Marin de Senghor|`@Compendium[pf2e.backgrounds.lav3yRNPc7lQ7e9k]`|
|[Servant](backgrounds/UdOUj7i8XGTI72Zc.htm)|Serviteur|`@Compendium[pf2e.backgrounds.UdOUj7i8XGTI72Zc]`|
|[Sewer Dragon](backgrounds/OhP7cqvNouFgHIdJ.htm)|Dragon des égoûts|`@Compendium[pf2e.backgrounds.OhP7cqvNouFgHIdJ]`|
|[Shadow Haunted](backgrounds/V31KRG7aA7xS0m8L.htm)|Ombre hantée|`@Compendium[pf2e.backgrounds.V31KRG7aA7xS0m8L]`|
|[Shadow Lodge Defector](backgrounds/HEd2Lxgvl080nRxx.htm)|Défecteur de la Loge de l'ombre|`@Compendium[pf2e.backgrounds.HEd2Lxgvl080nRxx]`|
|[Shadow War Survivor](backgrounds/b9EPEY09dYOVzdue.htm)|Survivant de la Guerre de l'ombre|`@Compendium[pf2e.backgrounds.b9EPEY09dYOVzdue]`|
|[Shoanti Name-Bearer](backgrounds/H3E69w8Xg0T7rAqD.htm)||`@Compendium[pf2e.backgrounds.H3E69w8Xg0T7rAqD]`|
|[Shory Seeker](backgrounds/HZ3oBBdEnsH3fWrm.htm)|Chercheur Rivain|`@Compendium[pf2e.backgrounds.HZ3oBBdEnsH3fWrm]`|
|[Sleepless Suns Star](backgrounds/AfBCrHsw1xbRFejN.htm)|Étoile des soleils sans repos|`@Compendium[pf2e.backgrounds.AfBCrHsw1xbRFejN]`|
|[Sodden Scavenger](backgrounds/93icIDHD4IrqI2oV.htm)|récupérateur détrempé|`@Compendium[pf2e.backgrounds.93icIDHD4IrqI2oV]`|
|[Spell Seeker](backgrounds/PO94ilqe62V6jtBE.htm)|Chercheur de sorts|`@Compendium[pf2e.backgrounds.PO94ilqe62V6jtBE]`|
|[Squire](backgrounds/qbvzNG8hMjb8f66D.htm)|Écuyer|`@Compendium[pf2e.backgrounds.qbvzNG8hMjb8f66D]`|
|[Starwatcher](backgrounds/sR3S7Xn15drU6rOF.htm)||`@Compendium[pf2e.backgrounds.sR3S7Xn15drU6rOF]`|
|[Storm Survivor](backgrounds/UURvnfwXypRYYXBI.htm)|Rescapé d'une tempête|`@Compendium[pf2e.backgrounds.UURvnfwXypRYYXBI]`|
|[Street Urchin](backgrounds/moVRsnpjB5THCwxE.htm)|Enfant des rues|`@Compendium[pf2e.backgrounds.moVRsnpjB5THCwxE]`|
|[Taldan Schemer](backgrounds/PhqUBXLLkVXb6oUE.htm)|Intrigant taldorien|`@Compendium[pf2e.backgrounds.PhqUBXLLkVXb6oUE]`|
|[Tapestry Refugee](backgrounds/LJmBnA2IYDBqQgRx.htm)|Réfugié de la Tapisserie|`@Compendium[pf2e.backgrounds.LJmBnA2IYDBqQgRx]`|
|[Tax Collector](backgrounds/ixluAGUDZciLEHtb.htm)|Percepteur|`@Compendium[pf2e.backgrounds.ixluAGUDZciLEHtb]`|
|[Teacher](backgrounds/cFdndc4pMWhnRUOY.htm)|Enseignant|`@Compendium[pf2e.backgrounds.cFdndc4pMWhnRUOY]`|
|[Teamster](backgrounds/h98cEl4DY75IL6KJ.htm)|Conducteur d'attelages|`@Compendium[pf2e.backgrounds.h98cEl4DY75IL6KJ]`|
|[Thassilonian Delver](backgrounds/vjhB0ZTV9OZgSuSz.htm)|Fouilleur du thassilon|`@Compendium[pf2e.backgrounds.vjhB0ZTV9OZgSuSz]`|
|[Thassilonian Traveler](backgrounds/su8y75pGMVTUsNHK.htm)|Voyageur Thassilonien|`@Compendium[pf2e.backgrounds.su8y75pGMVTUsNHK]`|
|[Thrune Loyalist](backgrounds/m1vRLRHTpCrgk89G.htm)|Loyaliste Thrune|`@Compendium[pf2e.backgrounds.m1vRLRHTpCrgk89G]`|
|[Thuvian Unifier](backgrounds/MslumKt6iwJ85GKZ.htm)|Unificateur de Thuvie|`@Compendium[pf2e.backgrounds.MslumKt6iwJ85GKZ]`|
|[Tinker](backgrounds/i79pgNIAtJfkkOiw.htm)|Bricoleur|`@Compendium[pf2e.backgrounds.i79pgNIAtJfkkOiw]`|
|[Touched by Dahak](backgrounds/4naQmCXBl0007c2W.htm)|Touché par Dahak|`@Compendium[pf2e.backgrounds.4naQmCXBl0007c2W]`|
|[Trade Consortium Underling](backgrounds/tcsSxwkl4wCsfO3k.htm)|Subalterne de consortium marchand|`@Compendium[pf2e.backgrounds.tcsSxwkl4wCsfO3k]`|
|[Trailblazer](backgrounds/LwAu4r3uocYfpKA8.htm)|Pionnier|`@Compendium[pf2e.backgrounds.LwAu4r3uocYfpKA8]`|
|[Translator](backgrounds/7AfixHrjbXgDPPkp.htm)|Traducteur|`@Compendium[pf2e.backgrounds.7AfixHrjbXgDPPkp]`|
|[Truth Seeker](backgrounds/TC7jpN5EA4UBIYep.htm)|En quête de vérité|`@Compendium[pf2e.backgrounds.TC7jpN5EA4UBIYep]`|
|[Ulfen Raider](backgrounds/V3nYEAhyA54RtYky.htm)|Raider ulfe|`@Compendium[pf2e.backgrounds.V3nYEAhyA54RtYky]`|
|[Undercover Lotus Guard](backgrounds/86TbxxwfpWjScwSQ.htm)|Garde Lotus infiltré|`@Compendium[pf2e.backgrounds.86TbxxwfpWjScwSQ]`|
|[Undersea Enthusiast](backgrounds/Ty8FRM0k262xuHfF.htm)|Enthousiaste aquatique|`@Compendium[pf2e.backgrounds.Ty8FRM0k262xuHfF]`|
|[Ustalavic Academic](backgrounds/x2y25cE98Eq4qxbu.htm)|Étudiant ustalavien|`@Compendium[pf2e.backgrounds.x2y25cE98Eq4qxbu]`|
|[Varisian Wanderer](backgrounds/uNhdcyhiog7YvXPT.htm)|Vagabond Varisien|`@Compendium[pf2e.backgrounds.uNhdcyhiog7YvXPT]`|
|[Vidrian Reformer](backgrounds/mrkgVjiEdlPjLUsN.htm)|Réformateur vidrien|`@Compendium[pf2e.backgrounds.mrkgVjiEdlPjLUsN]`|
|[Ward](backgrounds/IXxdCzBS0xP20ckw.htm)|Pupille|`@Compendium[pf2e.backgrounds.IXxdCzBS0xP20ckw]`|
|[Warrior](backgrounds/r0kYIbN06Cv8eNG3.htm)|Homme d'armes|`@Compendium[pf2e.backgrounds.r0kYIbN06Cv8eNG3]`|
|[Whispering Way Scion](backgrounds/aisuJF1A98bHfkLH.htm)|Enfant de la Voie du Murmure|`@Compendium[pf2e.backgrounds.aisuJF1A98bHfkLH]`|
|[Wildwood Local](backgrounds/R1v4gUu8oRMoOASM.htm)|Habitué des bois|`@Compendium[pf2e.backgrounds.R1v4gUu8oRMoOASM]`|
|[Winter's Child](backgrounds/SJ3nNOI5A8A4hK0Q.htm)|Enfant de l'Hiver|`@Compendium[pf2e.backgrounds.SJ3nNOI5A8A4hK0Q]`|
|[Witch Wary](backgrounds/yAtyaKbcHZWCJlf5.htm)|Gare aux sorcières|`@Compendium[pf2e.backgrounds.yAtyaKbcHZWCJlf5]`|
|[Witchlight Follower](backgrounds/BZhPPw9VD9U2ur6B.htm)||`@Compendium[pf2e.backgrounds.BZhPPw9VD9U2ur6B]`|
|[Wonder Taster](backgrounds/qB7g1OiZ8v8zgvkL.htm)|Goûteur de merveilles|`@Compendium[pf2e.backgrounds.qB7g1OiZ8v8zgvkL]`|


## Bestiaire

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Aasimar Redeemer](pathfinder-bestiary/gDMPUL0UiOHrUUd3.htm)|Rédempteur aasimar|`@Compendium[pf2e.pathfinder-bestiary.gDMPUL0UiOHrUUd3]`|
|[Adamantine Golem](pathfinder-bestiary/keCgklXcy4HZgQIL.htm)|Golem d’adamantium|`@Compendium[pf2e.pathfinder-bestiary.keCgklXcy4HZgQIL]`|
|[Adult Black Dragon](pathfinder-bestiary/irrXrWxJ0LYSUCQB.htm)|Dragon noir adulte|`@Compendium[pf2e.pathfinder-bestiary.irrXrWxJ0LYSUCQB]`|
|[Adult Blue Dragon](pathfinder-bestiary/i3N3udPyTGVPLpoq.htm)|Dragon bleu adulte|`@Compendium[pf2e.pathfinder-bestiary.i3N3udPyTGVPLpoq]`|
|[Adult Brass Dragon](pathfinder-bestiary/JnOgG1xfWleFGNt9.htm)|Dragon d'airain adulte|`@Compendium[pf2e.pathfinder-bestiary.JnOgG1xfWleFGNt9]`|
|[Adult Bronze Dragon](pathfinder-bestiary/IMasNR02C74jy3cT.htm)|Dragon de bronze adulte|`@Compendium[pf2e.pathfinder-bestiary.IMasNR02C74jy3cT]`|
|[Adult Copper Dragon](pathfinder-bestiary/Fzn4jHusVeyytgfx.htm)|Dragon de cuivre adulte|`@Compendium[pf2e.pathfinder-bestiary.Fzn4jHusVeyytgfx]`|
|[Adult Gold Dragon](pathfinder-bestiary/aAiuQvMGPN9QXwKY.htm)|Dragon d'or adulte|`@Compendium[pf2e.pathfinder-bestiary.aAiuQvMGPN9QXwKY]`|
|[Adult Green Dragon](pathfinder-bestiary/zXi6UhZ0mXWpKk4A.htm)|Dragon vert adulte|`@Compendium[pf2e.pathfinder-bestiary.zXi6UhZ0mXWpKk4A]`|
|[Adult Red Dragon](pathfinder-bestiary/JhQGNMlKARMx1n2D.htm)|Dragon rouge adulte|`@Compendium[pf2e.pathfinder-bestiary.JhQGNMlKARMx1n2D]`|
|[Adult Silver Dragon](pathfinder-bestiary/v9eQFqibX6EYsmuX.htm)|Dragon d'argent adulte|`@Compendium[pf2e.pathfinder-bestiary.v9eQFqibX6EYsmuX]`|
|[Adult White Dragon](pathfinder-bestiary/024PqcF8yMRBrPuq.htm)|Dragon blanc adulte|`@Compendium[pf2e.pathfinder-bestiary.024PqcF8yMRBrPuq]`|
|[Air Mephit](pathfinder-bestiary/KDRlxdIUADWHI6Vr.htm)|Méphite d'air|`@Compendium[pf2e.pathfinder-bestiary.KDRlxdIUADWHI6Vr]`|
|[Alchemical Golem](pathfinder-bestiary/Tpuqwt6Af29EMtqX.htm)|Golem alchimique|`@Compendium[pf2e.pathfinder-bestiary.Tpuqwt6Af29EMtqX]`|
|[Alghollthu Master](pathfinder-bestiary/tuIqEvsct8EO33xs.htm)|Maître alghollthu|`@Compendium[pf2e.pathfinder-bestiary.tuIqEvsct8EO33xs]`|
|[Ancient Black Dragon](pathfinder-bestiary/Lvdykf6wfqzZBlZd.htm)|Dragon noir vénérable|`@Compendium[pf2e.pathfinder-bestiary.Lvdykf6wfqzZBlZd]`|
|[Ancient Blue Dragon](pathfinder-bestiary/uPxh3VpA80zZdWfx.htm)|Dragon bleu vénérable|`@Compendium[pf2e.pathfinder-bestiary.uPxh3VpA80zZdWfx]`|
|[Ancient Brass Dragon](pathfinder-bestiary/bSjF2lCgchtp2ocS.htm)|Dragon d'airain vénérable|`@Compendium[pf2e.pathfinder-bestiary.bSjF2lCgchtp2ocS]`|
|[Ancient Bronze Dragon](pathfinder-bestiary/BzBFDaNj51PP97RZ.htm)|Dragon de bronze vénérable|`@Compendium[pf2e.pathfinder-bestiary.BzBFDaNj51PP97RZ]`|
|[Ancient Copper Dragon](pathfinder-bestiary/B9KJUdZre51J3E3e.htm)|Dragon de cuivre vénérable|`@Compendium[pf2e.pathfinder-bestiary.B9KJUdZre51J3E3e]`|
|[Ancient Gold Dragon](pathfinder-bestiary/nWiibOE9wVSlTdkx.htm)|Dragon d'or vénérable|`@Compendium[pf2e.pathfinder-bestiary.nWiibOE9wVSlTdkx]`|
|[Ancient Green Dragon](pathfinder-bestiary/0FGz2eXm0SB04sJW.htm)|Dragon vert vénérable|`@Compendium[pf2e.pathfinder-bestiary.0FGz2eXm0SB04sJW]`|
|[Ancient Red Dragon](pathfinder-bestiary/9H11QHnoKz8hHKSM.htm)|Dragon rouge vénérable|`@Compendium[pf2e.pathfinder-bestiary.9H11QHnoKz8hHKSM]`|
|[Ancient Silver Dragon](pathfinder-bestiary/SWOZ0e2IQifxzlgL.htm)|Dragon d'argent vénérable|`@Compendium[pf2e.pathfinder-bestiary.SWOZ0e2IQifxzlgL]`|
|[Ancient White Dragon](pathfinder-bestiary/mXkMSlj3LldzKxB9.htm)|Dragon blanc vénérable|`@Compendium[pf2e.pathfinder-bestiary.mXkMSlj3LldzKxB9]`|
|[Animated Armor](pathfinder-bestiary/cZDiyluplFqRxmGy.htm)|Armure animée|`@Compendium[pf2e.pathfinder-bestiary.cZDiyluplFqRxmGy]`|
|[Animated Broom](pathfinder-bestiary/ETwmjdnmSkqGdD5r.htm)|Balai animé|`@Compendium[pf2e.pathfinder-bestiary.ETwmjdnmSkqGdD5r]`|
|[Animated Statue](pathfinder-bestiary/PiAGhPEzJMC2egQk.htm)|Statue animée|`@Compendium[pf2e.pathfinder-bestiary.PiAGhPEzJMC2egQk]`|
|[Ankhrav](pathfinder-bestiary/wMomrpcaC8QvIdlj.htm)|Ankhrav|`@Compendium[pf2e.pathfinder-bestiary.wMomrpcaC8QvIdlj]`|
|[Ankylosaurus](pathfinder-bestiary/6CQEelygt968CB7m.htm)|Ankylosaure|`@Compendium[pf2e.pathfinder-bestiary.6CQEelygt968CB7m]`|
|[Annis Hag](pathfinder-bestiary/t6Xh4fHdHi2GP87z.htm)|Guenaude annis|`@Compendium[pf2e.pathfinder-bestiary.t6Xh4fHdHi2GP87z]`|
|[Arbiter](pathfinder-bestiary/LVhVb7abhv4onzZZ.htm)|Arbitre|`@Compendium[pf2e.pathfinder-bestiary.LVhVb7abhv4onzZZ]`|
|[Arboreal Regent](pathfinder-bestiary/c6AE2Mh8BRtBgbtz.htm)|Régent arboréen|`@Compendium[pf2e.pathfinder-bestiary.c6AE2Mh8BRtBgbtz]`|
|[Arboreal Warden](pathfinder-bestiary/aNWiP985fISjClGo.htm)|Gardien arboréen|`@Compendium[pf2e.pathfinder-bestiary.aNWiP985fISjClGo]`|
|[Astradaemon](pathfinder-bestiary/fkBcMpr3Yxxfvz9v.htm)|Astradaémon|`@Compendium[pf2e.pathfinder-bestiary.fkBcMpr3Yxxfvz9v]`|
|[Astral Deva](pathfinder-bestiary/45neevf5aLl0YPyk.htm)|Deva astral|`@Compendium[pf2e.pathfinder-bestiary.45neevf5aLl0YPyk]`|
|[Awakened Tree](pathfinder-bestiary/oBMIc2S8ekmDgPpi.htm)|Arbre éveillé|`@Compendium[pf2e.pathfinder-bestiary.oBMIc2S8ekmDgPpi]`|
|[Axiomite](pathfinder-bestiary/9qjXP1Lho1UmAihJ.htm)|Axiomite|`@Compendium[pf2e.pathfinder-bestiary.9qjXP1Lho1UmAihJ]`|
|[Azure Worm](pathfinder-bestiary/iD3YlM0QzI2SrjD6.htm)|Ver azur|`@Compendium[pf2e.pathfinder-bestiary.iD3YlM0QzI2SrjD6]`|
|[Balisse](pathfinder-bestiary/kFQorgvvyozQVSKi.htm)|Balisse|`@Compendium[pf2e.pathfinder-bestiary.kFQorgvvyozQVSKi]`|
|[Ball Python](pathfinder-bestiary/sf42HB8VsWGlYixP.htm)|Python boule|`@Compendium[pf2e.pathfinder-bestiary.sf42HB8VsWGlYixP]`|
|[Balor](pathfinder-bestiary/9vNYtJZiseCEf4wt.htm)|Balor|`@Compendium[pf2e.pathfinder-bestiary.9vNYtJZiseCEf4wt]`|
|[Banshee](pathfinder-bestiary/M6RknN77XTo23v45.htm)|Banshie|`@Compendium[pf2e.pathfinder-bestiary.M6RknN77XTo23v45]`|
|[Baomal](pathfinder-bestiary/JiThbhDfjUoPaTP1.htm)|Baomal|`@Compendium[pf2e.pathfinder-bestiary.JiThbhDfjUoPaTP1]`|
|[Barbazu](pathfinder-bestiary/FIoRPHaHdYUPVKdT.htm)|Barbazu|`@Compendium[pf2e.pathfinder-bestiary.FIoRPHaHdYUPVKdT]`|
|[Barghest](pathfinder-bestiary/x4mlZseBP5bWcy4H.htm)|Barghest|`@Compendium[pf2e.pathfinder-bestiary.x4mlZseBP5bWcy4H]`|
|[Basilisk](pathfinder-bestiary/rPHxXClTnoPYHYuZ.htm)|Basilic|`@Compendium[pf2e.pathfinder-bestiary.rPHxXClTnoPYHYuZ]`|
|[Black Pudding](pathfinder-bestiary/saEPHCN5lDiylb5H.htm)|Pouding noir|`@Compendium[pf2e.pathfinder-bestiary.saEPHCN5lDiylb5H]`|
|[Bloodseeker](pathfinder-bestiary/HpD0BTfid3hnUEWj.htm)|Cherchesang|`@Compendium[pf2e.pathfinder-bestiary.HpD0BTfid3hnUEWj]`|
|[Boar](pathfinder-bestiary/4MwjCsa5O9aAjxSm.htm)|Sanglier|`@Compendium[pf2e.pathfinder-bestiary.4MwjCsa5O9aAjxSm]`|
|[Boggard Scout](pathfinder-bestiary/wifELOkkRO2634bc.htm)|Éclaireur bourbiérin|`@Compendium[pf2e.pathfinder-bestiary.wifELOkkRO2634bc]`|
|[Boggard Swampseer](pathfinder-bestiary/pG3UPgbAxNCXAyQE.htm)|Devin des marais bourbiérin|`@Compendium[pf2e.pathfinder-bestiary.pG3UPgbAxNCXAyQE]`|
|[Boggard Warrior](pathfinder-bestiary/JkBJ8B07ElXrfDaG.htm)|Guerrier bourbiérin|`@Compendium[pf2e.pathfinder-bestiary.JkBJ8B07ElXrfDaG]`|
|[Brain Collector](pathfinder-bestiary/Fa1S0A8fAx3SkO9h.htm)|Collecteur de cerveaux|`@Compendium[pf2e.pathfinder-bestiary.Fa1S0A8fAx3SkO9h]`|
|[Brine Shark](pathfinder-bestiary/9sa2KE4Fbh3OPH7M.htm)|Requin de saumure|`@Compendium[pf2e.pathfinder-bestiary.9sa2KE4Fbh3OPH7M]`|
|[Brontosaurus](pathfinder-bestiary/05E3kkjoLZVjFOeO.htm)|Brontosaure|`@Compendium[pf2e.pathfinder-bestiary.05E3kkjoLZVjFOeO]`|
|[Bugbear Thug](pathfinder-bestiary/4htFfofrXLkbWMRg.htm)|Brute gobelours|`@Compendium[pf2e.pathfinder-bestiary.4htFfofrXLkbWMRg]`|
|[Bugbear Tormentor](pathfinder-bestiary/7JvA7kTqCUwcJoNe.htm)|Tourmenteur gobelours|`@Compendium[pf2e.pathfinder-bestiary.7JvA7kTqCUwcJoNe]`|
|[Bulette](pathfinder-bestiary/CzxQpB3p0d9hwPeR.htm)|Bulette|`@Compendium[pf2e.pathfinder-bestiary.CzxQpB3p0d9hwPeR]`|
|[Bunyip](pathfinder-bestiary/S8iX8sPaYFFYDoUq.htm)|Bunyip|`@Compendium[pf2e.pathfinder-bestiary.S8iX8sPaYFFYDoUq]`|
|[Cacodaemon](pathfinder-bestiary/E0PIGtVfc5PFVT2C.htm)|Cacodaémon|`@Compendium[pf2e.pathfinder-bestiary.E0PIGtVfc5PFVT2C]`|
|[Caligni Creeper](pathfinder-bestiary/nFMZjWQL6pd9XdqR.htm)|Caligni rampant|`@Compendium[pf2e.pathfinder-bestiary.nFMZjWQL6pd9XdqR]`|
|[Caligni Dancer](pathfinder-bestiary/eQdLBzkluS1fvVC8.htm)|Danseur caligni|`@Compendium[pf2e.pathfinder-bestiary.eQdLBzkluS1fvVC8]`|
|[Caligni Stalker](pathfinder-bestiary/gWxpeqOQ54Jd4HTG.htm)|Traqueur caligni|`@Compendium[pf2e.pathfinder-bestiary.gWxpeqOQ54Jd4HTG]`|
|[Cassisian](pathfinder-bestiary/ZzMJ7Y4qxapAVvlF.htm)|Cassisien|`@Compendium[pf2e.pathfinder-bestiary.ZzMJ7Y4qxapAVvlF]`|
|[Catfolk Pouncer](pathfinder-bestiary/saKs2Qaor8QktboH.htm)|Félide musard|`@Compendium[pf2e.pathfinder-bestiary.saKs2Qaor8QktboH]`|
|[Cauthooj](pathfinder-bestiary/FHfrIJCdKTzy2rrR.htm)|Cauthooj|`@Compendium[pf2e.pathfinder-bestiary.FHfrIJCdKTzy2rrR]`|
|[Cave Bear](pathfinder-bestiary/b8NQkby4QV4uOqFT.htm)|Ours des cavernes|`@Compendium[pf2e.pathfinder-bestiary.b8NQkby4QV4uOqFT]`|
|[Centaur](pathfinder-bestiary/l05LjJTXvFS4tYTE.htm)|Centaure|`@Compendium[pf2e.pathfinder-bestiary.l05LjJTXvFS4tYTE]`|
|[Centipede Swarm](pathfinder-bestiary/ZPAM4OavHmdgmGnw.htm)|Nuée de mille-pattes|`@Compendium[pf2e.pathfinder-bestiary.ZPAM4OavHmdgmGnw]`|
|[Ceustodaemon](pathfinder-bestiary/NW1Ax1QGE9W4DmiN.htm)|Ceustodaémon|`@Compendium[pf2e.pathfinder-bestiary.NW1Ax1QGE9W4DmiN]`|
|[Changeling Exile](pathfinder-bestiary/bAjHCeyNcPRqOmLv.htm)|Exilée changeline|`@Compendium[pf2e.pathfinder-bestiary.bAjHCeyNcPRqOmLv]`|
|[Chimera](pathfinder-bestiary/ScNPruIwcIJNuSHb.htm)|Chimère|`@Compendium[pf2e.pathfinder-bestiary.ScNPruIwcIJNuSHb]`|
|[Choral](pathfinder-bestiary/zUvgAbgeQH5t6DWs.htm)|Chœur|`@Compendium[pf2e.pathfinder-bestiary.zUvgAbgeQH5t6DWs]`|
|[Chuul](pathfinder-bestiary/aD76W2uEQhFFUrom.htm)|Chuul|`@Compendium[pf2e.pathfinder-bestiary.aD76W2uEQhFFUrom]`|
|[Cinder Rat](pathfinder-bestiary/hiGwRWdxAsoCII4f.htm)|Rat des braises|`@Compendium[pf2e.pathfinder-bestiary.hiGwRWdxAsoCII4f]`|
|[Clay Golem](pathfinder-bestiary/skz8n8N9GEQY2c90.htm)|Golem d’argile|`@Compendium[pf2e.pathfinder-bestiary.skz8n8N9GEQY2c90]`|
|[Cloaker](pathfinder-bestiary/bkaDwBD3mIBgvULs.htm)|Manteleur|`@Compendium[pf2e.pathfinder-bestiary.bkaDwBD3mIBgvULs]`|
|[Cloud Giant](pathfinder-bestiary/l17XDoK0UIjXUvOv.htm)|Géant des nuages|`@Compendium[pf2e.pathfinder-bestiary.l17XDoK0UIjXUvOv]`|
|[Cockatrice](pathfinder-bestiary/fgsDAeZHVbHRhSE8.htm)|Cockatrice|`@Compendium[pf2e.pathfinder-bestiary.fgsDAeZHVbHRhSE8]`|
|[Crag Linnorm](pathfinder-bestiary/tTmml7T2Knz2NrLd.htm)|Linnorm des falaises|`@Compendium[pf2e.pathfinder-bestiary.tTmml7T2Knz2NrLd]`|
|[Crimson Worm](pathfinder-bestiary/8U8K0YEghIErml35.htm)|Ver écarlate|`@Compendium[pf2e.pathfinder-bestiary.8U8K0YEghIErml35]`|
|[Crocodile](pathfinder-bestiary/vJwnApm0HkadGR7w.htm)|Crocodile|`@Compendium[pf2e.pathfinder-bestiary.vJwnApm0HkadGR7w]`|
|[Cyclops](pathfinder-bestiary/Dwgl1DzJAYE3ienu.htm)|Cyclope|`@Compendium[pf2e.pathfinder-bestiary.Dwgl1DzJAYE3ienu]`|
|[Daeodon](pathfinder-bestiary/quXuocHuT2US7cWz.htm)|Daeodon|`@Compendium[pf2e.pathfinder-bestiary.quXuocHuT2US7cWz]`|
|[Dandasuka](pathfinder-bestiary/hNR4xZRsxUkGPI1v.htm)|Dandasuka|`@Compendium[pf2e.pathfinder-bestiary.hNR4xZRsxUkGPI1v]`|
|[Dark Naga](pathfinder-bestiary/0MOWKI97bgGDf5Xi.htm)|Naga ténébreux|`@Compendium[pf2e.pathfinder-bestiary.0MOWKI97bgGDf5Xi]`|
|[Deadly Mantis](pathfinder-bestiary/HeoH8hi5iieKPuJ2.htm)|Mante meurtrière|`@Compendium[pf2e.pathfinder-bestiary.HeoH8hi5iieKPuJ2]`|
|[Deep Gnome Rockwarden](pathfinder-bestiary/0hnnwyqLfYVIenzd.htm)|Gardien des roches gnome des profondeurs|`@Compendium[pf2e.pathfinder-bestiary.0hnnwyqLfYVIenzd]`|
|[Deep Gnome Scout](pathfinder-bestiary/9FZMzpAu4XhCI0IB.htm)|Éclaireur gnome des profondeurs|`@Compendium[pf2e.pathfinder-bestiary.9FZMzpAu4XhCI0IB]`|
|[Deep Gnome Warrior](pathfinder-bestiary/b6qiHvyx6ymROTBL.htm)|Guerrier gnome des profondeurs|`@Compendium[pf2e.pathfinder-bestiary.b6qiHvyx6ymROTBL]`|
|[Deinonychus](pathfinder-bestiary/2IrWQjtFvsen8ioo.htm)|Deinonychus|`@Compendium[pf2e.pathfinder-bestiary.2IrWQjtFvsen8ioo]`|
|[Deinosuchus](pathfinder-bestiary/ScOT6QOlXIsevhNq.htm)|Deinosuchus|`@Compendium[pf2e.pathfinder-bestiary.ScOT6QOlXIsevhNq]`|
|[Demilich](pathfinder-bestiary/S35DifoycBwkxaGq.htm)|Demi-liche|`@Compendium[pf2e.pathfinder-bestiary.S35DifoycBwkxaGq]`|
|[Dero Magister](pathfinder-bestiary/ePa0KmNPpR4zUPfX.htm)|Magister dero|`@Compendium[pf2e.pathfinder-bestiary.ePa0KmNPpR4zUPfX]`|
|[Dero Stalker](pathfinder-bestiary/bxAJWWKrEMjgNkUp.htm)|Traqueur dero|`@Compendium[pf2e.pathfinder-bestiary.bxAJWWKrEMjgNkUp]`|
|[Dero Strangler](pathfinder-bestiary/ZsduIlmluQe4ZxFy.htm)|Étrangleur dero|`@Compendium[pf2e.pathfinder-bestiary.ZsduIlmluQe4ZxFy]`|
|[Desert Drake](pathfinder-bestiary/Y3T7XfC2BeiNBmuS.htm)|Drake du désert|`@Compendium[pf2e.pathfinder-bestiary.Y3T7XfC2BeiNBmuS]`|
|[Dezullon](pathfinder-bestiary/U1L3MFKHe0sNvLoU.htm)|Dézullone|`@Compendium[pf2e.pathfinder-bestiary.U1L3MFKHe0sNvLoU]`|
|[Dhampir Wizard](pathfinder-bestiary/jMiiQDIDxW9ZMvCV.htm)|Magicien dhampir|`@Compendium[pf2e.pathfinder-bestiary.jMiiQDIDxW9ZMvCV]`|
|[Dire Wolf](pathfinder-bestiary/qHqhUWeNUZRET9xV.htm)|Loup sanguinaire|`@Compendium[pf2e.pathfinder-bestiary.qHqhUWeNUZRET9xV]`|
|[Djinni](pathfinder-bestiary/GyigGu36XLPV72nW.htm)|Djinn|`@Compendium[pf2e.pathfinder-bestiary.GyigGu36XLPV72nW]`|
|[Doppelganger](pathfinder-bestiary/AdQVjlOWB6rmBRVp.htm)|Doppelganger|`@Compendium[pf2e.pathfinder-bestiary.AdQVjlOWB6rmBRVp]`|
|[Dragon Turtle](pathfinder-bestiary/gX66KyBxUOvMv5Sf.htm)|Tortue dragon|`@Compendium[pf2e.pathfinder-bestiary.gX66KyBxUOvMv5Sf]`|
|[Drakauthix](pathfinder-bestiary/SUpy8sleRwi2Egsq.htm)|Drakauthix|`@Compendium[pf2e.pathfinder-bestiary.SUpy8sleRwi2Egsq]`|
|[Drider](pathfinder-bestiary/B7eLG2k7qUo8HU6O.htm)|Drider|`@Compendium[pf2e.pathfinder-bestiary.B7eLG2k7qUo8HU6O]`|
|[Drow Fighter](pathfinder-bestiary/dTz1SQvJIUsB9S2w.htm)|Guerrier drow|`@Compendium[pf2e.pathfinder-bestiary.dTz1SQvJIUsB9S2w]`|
|[Drow Priestess](pathfinder-bestiary/FaBHkmFGuEIqIYM1.htm)|Prêtresse drow|`@Compendium[pf2e.pathfinder-bestiary.FaBHkmFGuEIqIYM1]`|
|[Drow Rogue](pathfinder-bestiary/VBXjZ1jYxdKf64B7.htm)|Roublard drow|`@Compendium[pf2e.pathfinder-bestiary.VBXjZ1jYxdKf64B7]`|
|[Dryad](pathfinder-bestiary/iSwUKe7cEytclS7r.htm)|Dryade|`@Compendium[pf2e.pathfinder-bestiary.iSwUKe7cEytclS7r]`|
|[Dryad Queen](pathfinder-bestiary/io7johJlZinrSCiH.htm)|Dryade souveraine|`@Compendium[pf2e.pathfinder-bestiary.io7johJlZinrSCiH]`|
|[Duergar Bombardier](pathfinder-bestiary/qfuoFK2GXBJusQ33.htm)|Bombardier duergar|`@Compendium[pf2e.pathfinder-bestiary.qfuoFK2GXBJusQ33]`|
|[Duergar Sharpshooter](pathfinder-bestiary/i1HEQ6f15fMEcHQf.htm)|Tireur d’élite duergar|`@Compendium[pf2e.pathfinder-bestiary.i1HEQ6f15fMEcHQf]`|
|[Duergar Taskmaster](pathfinder-bestiary/dEecX0AEfl32KUVN.htm)|Maître de corvée duergar|`@Compendium[pf2e.pathfinder-bestiary.dEecX0AEfl32KUVN]`|
|[Dullahan](pathfinder-bestiary/0SJqmk4ItwL31Rg9.htm)|Dullahan|`@Compendium[pf2e.pathfinder-bestiary.0SJqmk4ItwL31Rg9]`|
|[Duskwalker Ghost Hunter](pathfinder-bestiary/VoLW6eUxMSsXvgVP.htm)|Chasseur de fantômes crépusculaire|`@Compendium[pf2e.pathfinder-bestiary.VoLW6eUxMSsXvgVP]`|
|[Eagle](pathfinder-bestiary/8r8Ar08ojdJuPeiH.htm)|Aigle|`@Compendium[pf2e.pathfinder-bestiary.8r8Ar08ojdJuPeiH]`|
|[Earth Mephit](pathfinder-bestiary/x23aXeWTo026pMui.htm)|Méphite de la terre|`@Compendium[pf2e.pathfinder-bestiary.x23aXeWTo026pMui]`|
|[Efreeti](pathfinder-bestiary/c0zqasudrwZU3fdw.htm)|Éfrit|`@Compendium[pf2e.pathfinder-bestiary.c0zqasudrwZU3fdw]`|
|[Elananx](pathfinder-bestiary/8uXLbKKzxN5O0ZhM.htm)|Elananxe|`@Compendium[pf2e.pathfinder-bestiary.8uXLbKKzxN5O0ZhM]`|
|[Electric Eel](pathfinder-bestiary/tr75FAbdOkrfQviy.htm)|Anguille électrique|`@Compendium[pf2e.pathfinder-bestiary.tr75FAbdOkrfQviy]`|
|[Elemental Avalanche](pathfinder-bestiary/XoXf5ExS95Vv6lNf.htm)|Avalanche élémentaire|`@Compendium[pf2e.pathfinder-bestiary.XoXf5ExS95Vv6lNf]`|
|[Elemental Hurricane](pathfinder-bestiary/FPKoiMXENk5FouXp.htm)|Ouragan élémenataire|`@Compendium[pf2e.pathfinder-bestiary.FPKoiMXENk5FouXp]`|
|[Elemental Inferno](pathfinder-bestiary/NbGLrlt7RYdFFBQ5.htm)|Brasier élémentaire|`@Compendium[pf2e.pathfinder-bestiary.NbGLrlt7RYdFFBQ5]`|
|[Elemental Tsunami](pathfinder-bestiary/vmN9SCUJxN1MIXwp.htm)|Tsunami élémentaire|`@Compendium[pf2e.pathfinder-bestiary.vmN9SCUJxN1MIXwp]`|
|[Elephant](pathfinder-bestiary/HpY0addhUqtHMgUN.htm)|Éléphant|`@Compendium[pf2e.pathfinder-bestiary.HpY0addhUqtHMgUN]`|
|[Erinys](pathfinder-bestiary/ETNlci6VnSthx5V6.htm)|Érinye|`@Compendium[pf2e.pathfinder-bestiary.ETNlci6VnSthx5V6]`|
|[Ether Spider](pathfinder-bestiary/csRH8Fx0r6iMWlFc.htm)|Araignée de l'éther|`@Compendium[pf2e.pathfinder-bestiary.csRH8Fx0r6iMWlFc]`|
|[Ettin](pathfinder-bestiary/skwkJmgz7mqIcStF.htm)|Ettin|`@Compendium[pf2e.pathfinder-bestiary.skwkJmgz7mqIcStF]`|
|[Faceless Stalker](pathfinder-bestiary/qr46S4VDnaUK0GcM.htm)|Traqueur sans visage|`@Compendium[pf2e.pathfinder-bestiary.qr46S4VDnaUK0GcM]`|
|[Faerie Dragon](pathfinder-bestiary/atrhmCtNKx1MR06I.htm)|Dragon féerique|`@Compendium[pf2e.pathfinder-bestiary.atrhmCtNKx1MR06I]`|
|[Fire Giant](pathfinder-bestiary/80TiZrVvIBW7E6L2.htm)|Géant du feu|`@Compendium[pf2e.pathfinder-bestiary.80TiZrVvIBW7E6L2]`|
|[Fire Mephit](pathfinder-bestiary/k9fM0vufbdsDPQul.htm)|Méphite du feu|`@Compendium[pf2e.pathfinder-bestiary.k9fM0vufbdsDPQul]`|
|[Firewyrm](pathfinder-bestiary/Q8FAcsuta4p6d8KS.htm)|Wyrm de feu|`@Compendium[pf2e.pathfinder-bestiary.Q8FAcsuta4p6d8KS]`|
|[Flame Drake](pathfinder-bestiary/O5YbsTSlX5VhciP4.htm)|Drake des flammes|`@Compendium[pf2e.pathfinder-bestiary.O5YbsTSlX5VhciP4]`|
|[Flash Beetle](pathfinder-bestiary/i6Rd1BE30hhyKxwo.htm)|Scarabée-flash|`@Compendium[pf2e.pathfinder-bestiary.i6Rd1BE30hhyKxwo]`|
|[Flesh Golem](pathfinder-bestiary/sNFBOw4z9KagWUHe.htm)|Golem de chair|`@Compendium[pf2e.pathfinder-bestiary.sNFBOw4z9KagWUHe]`|
|[Frost Drake](pathfinder-bestiary/pyFvLyQsyYjOz0xI.htm)|Drake du froid|`@Compendium[pf2e.pathfinder-bestiary.pyFvLyQsyYjOz0xI]`|
|[Frost Giant](pathfinder-bestiary/oSvWsqFnQLS5wlvg.htm)|Géant du givre|`@Compendium[pf2e.pathfinder-bestiary.oSvWsqFnQLS5wlvg]`|
|[Fungus Leshy](pathfinder-bestiary/oiXbo1VSfDrHpIQm.htm)|Léchi fongique|`@Compendium[pf2e.pathfinder-bestiary.oiXbo1VSfDrHpIQm]`|
|[Gancanagh](pathfinder-bestiary/0SAlss24nUMdX9r8.htm)|Gancanagh|`@Compendium[pf2e.pathfinder-bestiary.0SAlss24nUMdX9r8]`|
|[Gargoyle](pathfinder-bestiary/aeCoh4u6c5kt1iCs.htm)|Gargouille|`@Compendium[pf2e.pathfinder-bestiary.aeCoh4u6c5kt1iCs]`|
|[Gelatinous Cube](pathfinder-bestiary/TZcDdN5o7s4alZNE.htm)|Cube gélatineux|`@Compendium[pf2e.pathfinder-bestiary.TZcDdN5o7s4alZNE]`|
|[Gelugon](pathfinder-bestiary/kNmRn3WfiWLsuwoe.htm)|Gélugon|`@Compendium[pf2e.pathfinder-bestiary.kNmRn3WfiWLsuwoe]`|
|[Ghaele](pathfinder-bestiary/Iw2QccAkRc5Vnfzj.htm)|Ghaéle|`@Compendium[pf2e.pathfinder-bestiary.Iw2QccAkRc5Vnfzj]`|
|[Ghast](pathfinder-bestiary/VC7rtYR4hLxwg7WZ.htm)|Blême|`@Compendium[pf2e.pathfinder-bestiary.VC7rtYR4hLxwg7WZ]`|
|[Ghost Commoner](pathfinder-bestiary/2H2AEwQnfKJC0nrd.htm)|Roturier fantôme|`@Compendium[pf2e.pathfinder-bestiary.2H2AEwQnfKJC0nrd]`|
|[Ghost Mage](pathfinder-bestiary/ToSwRvspZ0IB7SHQ.htm)|Mage fantôme|`@Compendium[pf2e.pathfinder-bestiary.ToSwRvspZ0IB7SHQ]`|
|[Ghoul](pathfinder-bestiary/LHHgGSs0ELCR4CYK.htm)|Goule|`@Compendium[pf2e.pathfinder-bestiary.LHHgGSs0ELCR4CYK]`|
|[Giant Anaconda](pathfinder-bestiary/05wwpHHsBlxBbdkN.htm)|Anaconda géant|`@Compendium[pf2e.pathfinder-bestiary.05wwpHHsBlxBbdkN]`|
|[Giant Animated Statue](pathfinder-bestiary/wKVZdVVcXtvLxgsY.htm)|Statue animée géante|`@Compendium[pf2e.pathfinder-bestiary.wKVZdVVcXtvLxgsY]`|
|[Giant Bat](pathfinder-bestiary/C9s5tBxVValC2HTE.htm)|Chauve-souris géante|`@Compendium[pf2e.pathfinder-bestiary.C9s5tBxVValC2HTE]`|
|[Giant Centipede](pathfinder-bestiary/BKPRkJgq7ehsW7uX.htm)|Mille-pattes géant|`@Compendium[pf2e.pathfinder-bestiary.BKPRkJgq7ehsW7uX]`|
|[Giant Eagle](pathfinder-bestiary/tKaOsbg8cmIUSjSE.htm)|Aigle géant|`@Compendium[pf2e.pathfinder-bestiary.tKaOsbg8cmIUSjSE]`|
|[Giant Flytrap](pathfinder-bestiary/5pk6bfodgnllSIOy.htm)|Attrape-mouche géant|`@Compendium[pf2e.pathfinder-bestiary.5pk6bfodgnllSIOy]`|
|[Giant Frilled Lizard](pathfinder-bestiary/QRjjE4TJNfaDhhQC.htm)|Lézard à collerette géant|`@Compendium[pf2e.pathfinder-bestiary.QRjjE4TJNfaDhhQC]`|
|[Giant Gecko](pathfinder-bestiary/9hOuoOONmp6500GZ.htm)|Gecko géant|`@Compendium[pf2e.pathfinder-bestiary.9hOuoOONmp6500GZ]`|
|[Giant Mantis](pathfinder-bestiary/Z7xWkQKCHGyd02B1.htm)|Mante géante|`@Compendium[pf2e.pathfinder-bestiary.Z7xWkQKCHGyd02B1]`|
|[Giant Monitor Lizard](pathfinder-bestiary/SuWpn5yZdsHDHpL2.htm)|Gecko géant|`@Compendium[pf2e.pathfinder-bestiary.SuWpn5yZdsHDHpL2]`|
|[Giant Moray Eel](pathfinder-bestiary/3VsQFEdIN5e1uWle.htm)|Murène géante|`@Compendium[pf2e.pathfinder-bestiary.3VsQFEdIN5e1uWle]`|
|[Giant Octopus](pathfinder-bestiary/vxKqnzwcxNAgLp7C.htm)|Pieuvre géante|`@Compendium[pf2e.pathfinder-bestiary.vxKqnzwcxNAgLp7C]`|
|[Giant Rat](pathfinder-bestiary/AYwdybUfm4meGUTJ.htm)|Rat géant|`@Compendium[pf2e.pathfinder-bestiary.AYwdybUfm4meGUTJ]`|
|[Giant Scorpion](pathfinder-bestiary/zJZqpx6pPW7dxEUV.htm)|Scorpion géant|`@Compendium[pf2e.pathfinder-bestiary.zJZqpx6pPW7dxEUV]`|
|[Giant Stag Beetle](pathfinder-bestiary/bIw7czN0E3rENrVd.htm)|Lucane géant|`@Compendium[pf2e.pathfinder-bestiary.bIw7czN0E3rENrVd]`|
|[Giant Tarantula](pathfinder-bestiary/9X7hOvCKy1bqw0g6.htm)|Tarentule géante|`@Compendium[pf2e.pathfinder-bestiary.9X7hOvCKy1bqw0g6]`|
|[Giant Viper](pathfinder-bestiary/KsWAIXTTh3mfNWOY.htm)|Vipère géante|`@Compendium[pf2e.pathfinder-bestiary.KsWAIXTTh3mfNWOY]`|
|[Giant Wasp](pathfinder-bestiary/J0dSbywBRgD2kf19.htm)|Guêpe géante|`@Compendium[pf2e.pathfinder-bestiary.J0dSbywBRgD2kf19]`|
|[Gibbering Mouther](pathfinder-bestiary/dEAneTvoPuQXZoLR.htm)|Babélien|`@Compendium[pf2e.pathfinder-bestiary.dEAneTvoPuQXZoLR]`|
|[Gimmerling](pathfinder-bestiary/HyOf4CfAIhC3qWtz.htm)|Gimmerling|`@Compendium[pf2e.pathfinder-bestiary.HyOf4CfAIhC3qWtz]`|
|[Glabrezu](pathfinder-bestiary/BRo0snV2sH6TFuh6.htm)|Glabrezu|`@Compendium[pf2e.pathfinder-bestiary.BRo0snV2sH6TFuh6]`|
|[Gnoll Cultist](pathfinder-bestiary/hXpqjD3eBRxlemNs.htm)|Cultiste gnoll|`@Compendium[pf2e.pathfinder-bestiary.hXpqjD3eBRxlemNs]`|
|[Gnoll Hunter](pathfinder-bestiary/o3DRwRKeJrl83Wv9.htm)|Chasseur gnoll|`@Compendium[pf2e.pathfinder-bestiary.o3DRwRKeJrl83Wv9]`|
|[Gnoll Sergeant](pathfinder-bestiary/oMsOm06HhX1gG0Jz.htm)|Sergent gnoll|`@Compendium[pf2e.pathfinder-bestiary.oMsOm06HhX1gG0Jz]`|
|[Goblin Commando](pathfinder-bestiary/jGzVwekcRX5aQpbT.htm)|Commando gobelin|`@Compendium[pf2e.pathfinder-bestiary.jGzVwekcRX5aQpbT]`|
|[Goblin Dog](pathfinder-bestiary/0rfropeocJWXC6pg.htm)|Chien gobelin|`@Compendium[pf2e.pathfinder-bestiary.0rfropeocJWXC6pg]`|
|[Goblin Pyro](pathfinder-bestiary/z0l0lHc79NbMxiqZ.htm)|Pyro gobelin|`@Compendium[pf2e.pathfinder-bestiary.z0l0lHc79NbMxiqZ]`|
|[Goblin War Chanter](pathfinder-bestiary/4BBzo72pHOpecoIp.htm)|Chantre de guerre gobelin|`@Compendium[pf2e.pathfinder-bestiary.4BBzo72pHOpecoIp]`|
|[Goblin Warrior](pathfinder-bestiary/jVZRROs0GzDjVrgi.htm)|Guerrier gobelin|`@Compendium[pf2e.pathfinder-bestiary.jVZRROs0GzDjVrgi]`|
|[Gogiteth](pathfinder-bestiary/IXen98RbUlbxDWBD.htm)|Gogiteth|`@Compendium[pf2e.pathfinder-bestiary.IXen98RbUlbxDWBD]`|
|[Goliath Spider](pathfinder-bestiary/T5CUuPsMPb17d6Qy.htm)|Araignée goliath|`@Compendium[pf2e.pathfinder-bestiary.T5CUuPsMPb17d6Qy]`|
|[Gorilla](pathfinder-bestiary/XVX9Uhqb8shG5Pwm.htm)|Gorille|`@Compendium[pf2e.pathfinder-bestiary.XVX9Uhqb8shG5Pwm]`|
|[Gourd Leshy](pathfinder-bestiary/UYHtIbN0JVaIYcgs.htm)|Léchi calebasse|`@Compendium[pf2e.pathfinder-bestiary.UYHtIbN0JVaIYcgs]`|
|[Graveknight](pathfinder-bestiary/4p07SH4zdmVZ405I.htm)|Chevalier sépulcre|`@Compendium[pf2e.pathfinder-bestiary.4p07SH4zdmVZ405I]`|
|[Great Cyclops](pathfinder-bestiary/Zn0p5YjELMjEwkqx.htm)|Grands cyclops|`@Compendium[pf2e.pathfinder-bestiary.Zn0p5YjELMjEwkqx]`|
|[Great White Shark](pathfinder-bestiary/WNiNj0Brn2LCYmwd.htm)|Grand requin blanc|`@Compendium[pf2e.pathfinder-bestiary.WNiNj0Brn2LCYmwd]`|
|[Greater Barghest](pathfinder-bestiary/DlRe4c2XlBSpwmms.htm)|Barghest supérieur|`@Compendium[pf2e.pathfinder-bestiary.DlRe4c2XlBSpwmms]`|
|[Greater Nightmare](pathfinder-bestiary/QQQhNnCit9XLMMoN.htm)|Destrier noir supérieur|`@Compendium[pf2e.pathfinder-bestiary.QQQhNnCit9XLMMoN]`|
|[Greater Shadow](pathfinder-bestiary/IUzKFRX0uHl1yxkn.htm)|Ombre majeure|`@Compendium[pf2e.pathfinder-bestiary.IUzKFRX0uHl1yxkn]`|
|[Green Hag](pathfinder-bestiary/3nOBhH8j9I7ps6fC.htm)|Guenaude verte|`@Compendium[pf2e.pathfinder-bestiary.3nOBhH8j9I7ps6fC]`|
|[Griffon](pathfinder-bestiary/Oc5NXZmMkSDCRNlQ.htm)|Griffon|`@Compendium[pf2e.pathfinder-bestiary.Oc5NXZmMkSDCRNlQ]`|
|[Grig](pathfinder-bestiary/j1mm6bb3hZ56jSrK.htm)|Grig|`@Compendium[pf2e.pathfinder-bestiary.j1mm6bb3hZ56jSrK]`|
|[Grikkitog](pathfinder-bestiary/UPm2rwIevsX9Odbm.htm)|Grikkitog|`@Compendium[pf2e.pathfinder-bestiary.UPm2rwIevsX9Odbm]`|
|[Grim Reaper](pathfinder-bestiary/mGr4e6fH3w8ewcSX.htm)|Faucheuse|`@Compendium[pf2e.pathfinder-bestiary.mGr4e6fH3w8ewcSX]`|
|[Grizzly Bear](pathfinder-bestiary/CJP3GGBXuGgkaj6C.htm)|Grizzly|`@Compendium[pf2e.pathfinder-bestiary.CJP3GGBXuGgkaj6C]`|
|[Grothlut](pathfinder-bestiary/K9Hw43co8fhwmKkM.htm)|Grothlut|`@Compendium[pf2e.pathfinder-bestiary.K9Hw43co8fhwmKkM]`|
|[Guard Dog](pathfinder-bestiary/QRRX82FIjBKd8pzs.htm)|Chien de garde|`@Compendium[pf2e.pathfinder-bestiary.QRRX82FIjBKd8pzs]`|
|[Guardian Naga](pathfinder-bestiary/aq2H1lRALUNMEGRG.htm)|Naga gardien|`@Compendium[pf2e.pathfinder-bestiary.aq2H1lRALUNMEGRG]`|
|[Gug](pathfinder-bestiary/zq18QX6CBJNeUIgG.htm)|Gug|`@Compendium[pf2e.pathfinder-bestiary.zq18QX6CBJNeUIgG]`|
|[Guthallath](pathfinder-bestiary/m3x8q5rZ6zh9x82s.htm)|Guthallath|`@Compendium[pf2e.pathfinder-bestiary.m3x8q5rZ6zh9x82s]`|
|[Harpy](pathfinder-bestiary/AuCC04X2AO8oFN75.htm)|Harpie|`@Compendium[pf2e.pathfinder-bestiary.AuCC04X2AO8oFN75]`|
|[Hell Hound](pathfinder-bestiary/jeAGl6OAVrrIPgu3.htm)|Molosse infernal|`@Compendium[pf2e.pathfinder-bestiary.jeAGl6OAVrrIPgu3]`|
|[Hill Giant](pathfinder-bestiary/aqW5sp9HPgZgpXxs.htm)|Géant des collines|`@Compendium[pf2e.pathfinder-bestiary.aqW5sp9HPgZgpXxs]`|
|[Hive Mother](pathfinder-bestiary/pTkz08ak9YlKRsOY.htm)|Mère de la ruche|`@Compendium[pf2e.pathfinder-bestiary.pTkz08ak9YlKRsOY]`|
|[Hobgoblin Archer](pathfinder-bestiary/KclNszYZ7sjwE9nX.htm)|Arbalétrier hobgobelin|`@Compendium[pf2e.pathfinder-bestiary.KclNszYZ7sjwE9nX]`|
|[Hobgoblin General](pathfinder-bestiary/5hQk5NJk4L10txyW.htm)|Général hobgobelin|`@Compendium[pf2e.pathfinder-bestiary.5hQk5NJk4L10txyW]`|
|[Hobgoblin Soldier](pathfinder-bestiary/PiZkpRK23u89h82S.htm)|Soldat hobgobelin|`@Compendium[pf2e.pathfinder-bestiary.PiZkpRK23u89h82S]`|
|[Homunculus](pathfinder-bestiary/2GRPw4VK6zfCS2Qw.htm)|Homoncule|`@Compendium[pf2e.pathfinder-bestiary.2GRPw4VK6zfCS2Qw]`|
|[Horned Archon](pathfinder-bestiary/iII08V0HlvWGWSmu.htm)|Archon cornu|`@Compendium[pf2e.pathfinder-bestiary.iII08V0HlvWGWSmu]`|
|[Hunting Spider](pathfinder-bestiary/E4ctF7Fvi3cdkgQq.htm)|Araignée chasseuse|`@Compendium[pf2e.pathfinder-bestiary.E4ctF7Fvi3cdkgQq]`|
|[Hyaenodon](pathfinder-bestiary/VcUdFYNaxauNr5Hn.htm)|Hyaenodon|`@Compendium[pf2e.pathfinder-bestiary.VcUdFYNaxauNr5Hn]`|
|[Hydra](pathfinder-bestiary/RiKjpztTt7tZbOeo.htm)|Hydre|`@Compendium[pf2e.pathfinder-bestiary.RiKjpztTt7tZbOeo]`|
|[Hyena](pathfinder-bestiary/Zv6eaumsdz4HdxRV.htm)|Hyène|`@Compendium[pf2e.pathfinder-bestiary.Zv6eaumsdz4HdxRV]`|
|[Ice Linnorm](pathfinder-bestiary/6eabIbxzYepfZAHX.htm)|Linnorm des glaces|`@Compendium[pf2e.pathfinder-bestiary.6eabIbxzYepfZAHX]`|
|[Imp](pathfinder-bestiary/I4CpyMUsWfFYdpL5.htm)|Diablotin|`@Compendium[pf2e.pathfinder-bestiary.I4CpyMUsWfFYdpL5]`|
|[Invisible Stalker](pathfinder-bestiary/mqz4MfBwFxlBQeHs.htm)|Traqueur invisible|`@Compendium[pf2e.pathfinder-bestiary.mqz4MfBwFxlBQeHs]`|
|[Iron Golem](pathfinder-bestiary/t4bHhZBBDUyVzEmf.htm)|Golem de fer|`@Compendium[pf2e.pathfinder-bestiary.t4bHhZBBDUyVzEmf]`|
|[Janni](pathfinder-bestiary/tMxtnGthVV01wNQb.htm)|Jann|`@Compendium[pf2e.pathfinder-bestiary.tMxtnGthVV01wNQb]`|
|[Jinkin](pathfinder-bestiary/gfRXFd24U633OC9r.htm)|Jinkin|`@Compendium[pf2e.pathfinder-bestiary.gfRXFd24U633OC9r]`|
|[Jungle Drake](pathfinder-bestiary/vEFENJJixCdmBNl5.htm)|Drake de la jungle|`@Compendium[pf2e.pathfinder-bestiary.vEFENJJixCdmBNl5]`|
|[Keketar](pathfinder-bestiary/zBPGUUP788b0g1Ng.htm)|Ké’ktar|`@Compendium[pf2e.pathfinder-bestiary.zBPGUUP788b0g1Ng]`|
|[Kobold Dragon Mage](pathfinder-bestiary/qBu78r2alwJNs7ry.htm)|Mage draconique kobold|`@Compendium[pf2e.pathfinder-bestiary.qBu78r2alwJNs7ry]`|
|[Kobold Scout](pathfinder-bestiary/rPaHIh0ICnTLnRO6.htm)|Éclaireur kobold|`@Compendium[pf2e.pathfinder-bestiary.rPaHIh0ICnTLnRO6]`|
|[Kobold Warrior](pathfinder-bestiary/r9w1n85mp9Ip4QiS.htm)|Guerrier kobold|`@Compendium[pf2e.pathfinder-bestiary.r9w1n85mp9Ip4QiS]`|
|[Kolyarut](pathfinder-bestiary/PgBsMyjMITjQnCs8.htm)|Kolyarut|`@Compendium[pf2e.pathfinder-bestiary.PgBsMyjMITjQnCs8]`|
|[Kraken](pathfinder-bestiary/45Eo7MFWG3ShikvD.htm)|Kraken|`@Compendium[pf2e.pathfinder-bestiary.45Eo7MFWG3ShikvD]`|
|[Krooth](pathfinder-bestiary/WNqPRMjKW0oCHZ8X.htm)|Krooth|`@Compendium[pf2e.pathfinder-bestiary.WNqPRMjKW0oCHZ8X]`|
|[Lamia](pathfinder-bestiary/IQsTNM8aXcCUmFu0.htm)|Lamie|`@Compendium[pf2e.pathfinder-bestiary.IQsTNM8aXcCUmFu0]`|
|[Lamia Matriarch](pathfinder-bestiary/NymSyXbXqfkGLFWF.htm)|Matriarche lamie|`@Compendium[pf2e.pathfinder-bestiary.NymSyXbXqfkGLFWF]`|
|[Lantern Archon](pathfinder-bestiary/0HjVFx8qIKDCfblg.htm)|Archon lumineux|`@Compendium[pf2e.pathfinder-bestiary.0HjVFx8qIKDCfblg]`|
|[Leaf Leshy](pathfinder-bestiary/BeptBpCJ4Ny4biOH.htm)|Léchi de feuilles|`@Compendium[pf2e.pathfinder-bestiary.BeptBpCJ4Ny4biOH]`|
|[Legion Archon](pathfinder-bestiary/4B47SUq57pcl3U9u.htm)|Archon légionnaire|`@Compendium[pf2e.pathfinder-bestiary.4B47SUq57pcl3U9u]`|
|[Lemure](pathfinder-bestiary/zJro50sLFmOcDLdO.htm)|Lémure|`@Compendium[pf2e.pathfinder-bestiary.zJro50sLFmOcDLdO]`|
|[Leopard](pathfinder-bestiary/wqPYzMNgYvrO6oEP.htm)|Léopard|`@Compendium[pf2e.pathfinder-bestiary.wqPYzMNgYvrO6oEP]`|
|[Lesser Death](pathfinder-bestiary/pkNWilK2pHZ5TDsd.htm)|Mort inférieure|`@Compendium[pf2e.pathfinder-bestiary.pkNWilK2pHZ5TDsd]`|
|[Leukodaemon](pathfinder-bestiary/N62zM3aTelygWIt2.htm)|Leukodaemon|`@Compendium[pf2e.pathfinder-bestiary.N62zM3aTelygWIt2]`|
|[Lich](pathfinder-bestiary/EibxkD9y30YmPaLH.htm)|Liche|`@Compendium[pf2e.pathfinder-bestiary.EibxkD9y30YmPaLH]`|
|[Lillend](pathfinder-bestiary/d9W89Yv6zyvfxZuG.htm)|Lillende|`@Compendium[pf2e.pathfinder-bestiary.d9W89Yv6zyvfxZuG]`|
|[Lion](pathfinder-bestiary/l3Pe8FsFbLvft1Fq.htm)|Lion|`@Compendium[pf2e.pathfinder-bestiary.l3Pe8FsFbLvft1Fq]`|
|[Living Landslide](pathfinder-bestiary/xYlOudjXyTakF1m8.htm)|Glissement de terrain vivant|`@Compendium[pf2e.pathfinder-bestiary.xYlOudjXyTakF1m8]`|
|[Living Waterfall](pathfinder-bestiary/BLFEu9jCKPAMko01.htm)|Cascade vivante|`@Compendium[pf2e.pathfinder-bestiary.BLFEu9jCKPAMko01]`|
|[Living Whirlwind](pathfinder-bestiary/wjw8FQp4icafYash.htm)|Tourbillon vivant|`@Compendium[pf2e.pathfinder-bestiary.wjw8FQp4icafYash]`|
|[Living Wildfire](pathfinder-bestiary/zRNHsSxi1g3IFYFu.htm)|Incendie vivant|`@Compendium[pf2e.pathfinder-bestiary.zRNHsSxi1g3IFYFu]`|
|[Lizardfolk Defender](pathfinder-bestiary/EhB5Q98OO25DDOOl.htm)|Défenseur homme-lézard|`@Compendium[pf2e.pathfinder-bestiary.EhB5Q98OO25DDOOl]`|
|[Lizardfolk Scout](pathfinder-bestiary/1CzZINpYRcNBKDnO.htm)|Éclaireur homme-lézard|`@Compendium[pf2e.pathfinder-bestiary.1CzZINpYRcNBKDnO]`|
|[Lizardfolk Stargazer](pathfinder-bestiary/yzyaD2yGDrxmYh7P.htm)|Homme-lézard astrobservateur|`@Compendium[pf2e.pathfinder-bestiary.yzyaD2yGDrxmYh7P]`|
|[Lyrakien](pathfinder-bestiary/XUTUBrQixSs7VLov.htm)|Lyrakien|`@Compendium[pf2e.pathfinder-bestiary.XUTUBrQixSs7VLov]`|
|[Mammoth](pathfinder-bestiary/yIXRooXdsKtbcw2D.htm)|Mammouth|`@Compendium[pf2e.pathfinder-bestiary.yIXRooXdsKtbcw2D]`|
|[Manticore](pathfinder-bestiary/qpRJOzx3bJ7rolHp.htm)|Manticore|`@Compendium[pf2e.pathfinder-bestiary.qpRJOzx3bJ7rolHp]`|
|[Marid](pathfinder-bestiary/cuET2PHGcE7eL7DJ.htm)|Maride|`@Compendium[pf2e.pathfinder-bestiary.cuET2PHGcE7eL7DJ]`|
|[Marilith](pathfinder-bestiary/ImueS9YFhV6sxqBP.htm)|Marilith|`@Compendium[pf2e.pathfinder-bestiary.ImueS9YFhV6sxqBP]`|
|[Medusa](pathfinder-bestiary/kkFVngQUGTACeggf.htm)|Méduse|`@Compendium[pf2e.pathfinder-bestiary.kkFVngQUGTACeggf]`|
|[Megalodon](pathfinder-bestiary/vUKCuAgLQdz5akgp.htm)|Megalodon|`@Compendium[pf2e.pathfinder-bestiary.vUKCuAgLQdz5akgp]`|
|[Megaprimatus](pathfinder-bestiary/GssFAdolUA3ghg2e.htm)|Megaprimatus|`@Compendium[pf2e.pathfinder-bestiary.GssFAdolUA3ghg2e]`|
|[Merfolk Warrior](pathfinder-bestiary/O3J59mUJ6DHQZZ6F.htm)|Combattant homme-poisson|`@Compendium[pf2e.pathfinder-bestiary.O3J59mUJ6DHQZZ6F]`|
|[Merfolk Wavecaller](pathfinder-bestiary/uCw15c4AnIrOy5AV.htm)|Implorateur des vagues homme-poisson|`@Compendium[pf2e.pathfinder-bestiary.uCw15c4AnIrOy5AV]`|
|[Mimic](pathfinder-bestiary/Br1AtKUHe3nbzjnY.htm)|Mimique|`@Compendium[pf2e.pathfinder-bestiary.Br1AtKUHe3nbzjnY]`|
|[Minotaur](pathfinder-bestiary/E9rT02pPDLq7rARq.htm)|Minotaure|`@Compendium[pf2e.pathfinder-bestiary.E9rT02pPDLq7rARq]`|
|[Mitflit](pathfinder-bestiary/E0LCMHVp4sxAbQYa.htm)|Mitflit|`@Compendium[pf2e.pathfinder-bestiary.E0LCMHVp4sxAbQYa]`|
|[Morrigna](pathfinder-bestiary/DPEmRRXYevk3ADqW.htm)|Morrigna|`@Compendium[pf2e.pathfinder-bestiary.DPEmRRXYevk3ADqW]`|
|[Mu Spore](pathfinder-bestiary/VUJrPHKOjYkIQnWn.htm)|Spore de mu|`@Compendium[pf2e.pathfinder-bestiary.VUJrPHKOjYkIQnWn]`|
|[Mukradi](pathfinder-bestiary/9llfviiJg5bJlBth.htm)|Mukradi|`@Compendium[pf2e.pathfinder-bestiary.9llfviiJg5bJlBth]`|
|[Mummy Guardian](pathfinder-bestiary/E5RDV3n7GnjAspQ5.htm)|Momie gardienne|`@Compendium[pf2e.pathfinder-bestiary.E5RDV3n7GnjAspQ5]`|
|[Mummy Pharaoh](pathfinder-bestiary/kfeL172Ix3x1YRc9.htm)|Momie pharaon|`@Compendium[pf2e.pathfinder-bestiary.kfeL172Ix3x1YRc9]`|
|[Naiad](pathfinder-bestiary/RXEnAk6cbSnk3w7O.htm)|Naîade|`@Compendium[pf2e.pathfinder-bestiary.RXEnAk6cbSnk3w7O]`|
|[Naiad Queen](pathfinder-bestiary/zA1I5YXI9GCSaksP.htm)|Naïade souveraine|`@Compendium[pf2e.pathfinder-bestiary.zA1I5YXI9GCSaksP]`|
|[Naunet](pathfinder-bestiary/pL4sS2HZtGAryKnN.htm)|Naunet|`@Compendium[pf2e.pathfinder-bestiary.pL4sS2HZtGAryKnN]`|
|[Nessian Warhound](pathfinder-bestiary/B7b0alybm5U34nFV.htm)|Molosse nessien|`@Compendium[pf2e.pathfinder-bestiary.B7b0alybm5U34nFV]`|
|[Night Hag](pathfinder-bestiary/WQy7HBUcgDLsfVJd.htm)|Guenaude noire|`@Compendium[pf2e.pathfinder-bestiary.WQy7HBUcgDLsfVJd]`|
|[Nightmare](pathfinder-bestiary/waPgKbjhijeZ00Zm.htm)|Destrier noir|`@Compendium[pf2e.pathfinder-bestiary.waPgKbjhijeZ00Zm]`|
|[Nilith](pathfinder-bestiary/PfcS6WzhMGzds5Wf.htm)|Nilith|`@Compendium[pf2e.pathfinder-bestiary.PfcS6WzhMGzds5Wf]`|
|[Nosoi](pathfinder-bestiary/oGIWTW0WqQxYNJOD.htm)|Nosoi|`@Compendium[pf2e.pathfinder-bestiary.oGIWTW0WqQxYNJOD]`|
|[Ochre Jelly](pathfinder-bestiary/uQBOJfjtzEXvpLQz.htm)|Gelée ocre|`@Compendium[pf2e.pathfinder-bestiary.uQBOJfjtzEXvpLQz]`|
|[Ofalth](pathfinder-bestiary/jP8CO6z7bNIhOuqQ.htm)|Ofalth|`@Compendium[pf2e.pathfinder-bestiary.jP8CO6z7bNIhOuqQ]`|
|[Ogre Boss](pathfinder-bestiary/bIXfNKFWduf8MH0f.htm)|Boss ogre|`@Compendium[pf2e.pathfinder-bestiary.bIXfNKFWduf8MH0f]`|
|[Ogre Glutton](pathfinder-bestiary/oJaC1WbXQuQX2d2J.htm)|Ogre glouton|`@Compendium[pf2e.pathfinder-bestiary.oJaC1WbXQuQX2d2J]`|
|[Ogre Warrior](pathfinder-bestiary/gdXok08bITkhowDJ.htm)|Guerrier ogre|`@Compendium[pf2e.pathfinder-bestiary.gdXok08bITkhowDJ]`|
|[Orc Brute](pathfinder-bestiary/cDm6PzhO5nXlkGoi.htm)|Brute orque|`@Compendium[pf2e.pathfinder-bestiary.cDm6PzhO5nXlkGoi]`|
|[Orc Warchief](pathfinder-bestiary/XLqbEDjmGpIc4XoY.htm)|Chef de guerre orc|`@Compendium[pf2e.pathfinder-bestiary.XLqbEDjmGpIc4XoY]`|
|[Orc Warrior](pathfinder-bestiary/iA9lbwH0qROTjCva.htm)|Guerrier orc|`@Compendium[pf2e.pathfinder-bestiary.iA9lbwH0qROTjCva]`|
|[Otyugh](pathfinder-bestiary/kPBSSOuR8n8vZHXB.htm)|Otyugh|`@Compendium[pf2e.pathfinder-bestiary.kPBSSOuR8n8vZHXB]`|
|[Owlbear](pathfinder-bestiary/X03vq2RWi2jiA6Ri.htm)|Hibours|`@Compendium[pf2e.pathfinder-bestiary.X03vq2RWi2jiA6Ri]`|
|[Pegasus](pathfinder-bestiary/PCh2kxeSYWRit9TE.htm)|Pégase|`@Compendium[pf2e.pathfinder-bestiary.PCh2kxeSYWRit9TE]`|
|[Phistophilus](pathfinder-bestiary/B8QjalVNcWjuqgG7.htm)|Phistophilus|`@Compendium[pf2e.pathfinder-bestiary.B8QjalVNcWjuqgG7]`|
|[Phoenix](pathfinder-bestiary/gioxLqV8N4p9iIAh.htm)|Phénix|`@Compendium[pf2e.pathfinder-bestiary.gioxLqV8N4p9iIAh]`|
|[Pit Fiend](pathfinder-bestiary/QT2gA8WMaT2cuXr7.htm)|Diantrefosse|`@Compendium[pf2e.pathfinder-bestiary.QT2gA8WMaT2cuXr7]`|
|[Pixie](pathfinder-bestiary/wpmvdP5w936Kmq0e.htm)|Pixie|`@Compendium[pf2e.pathfinder-bestiary.wpmvdP5w936Kmq0e]`|
|[Plague Zombie](pathfinder-bestiary/KxN9aGFGPxl6oLGF.htm)|Zombie pestiféré|`@Compendium[pf2e.pathfinder-bestiary.KxN9aGFGPxl6oLGF]`|
|[Pleroma](pathfinder-bestiary/bjJUZKcA47Qp0ZwL.htm)|Pleroma|`@Compendium[pf2e.pathfinder-bestiary.bjJUZKcA47Qp0ZwL]`|
|[Poltergeist](pathfinder-bestiary/k3Lt3bWBadXvlIbG.htm)|Poltergeist|`@Compendium[pf2e.pathfinder-bestiary.k3Lt3bWBadXvlIbG]`|
|[Poracha](pathfinder-bestiary/r9WAwtCLxoJMjd8J.htm)|Poracha|`@Compendium[pf2e.pathfinder-bestiary.r9WAwtCLxoJMjd8J]`|
|[Pteranodon](pathfinder-bestiary/CSefkWGVmA5yGxNR.htm)|Ptéranodon|`@Compendium[pf2e.pathfinder-bestiary.CSefkWGVmA5yGxNR]`|
|[Pugwampi](pathfinder-bestiary/jnmUcTs4hn1c5bz9.htm)|Pugwampi|`@Compendium[pf2e.pathfinder-bestiary.jnmUcTs4hn1c5bz9]`|
|[Purple Worm](pathfinder-bestiary/4h9jhODg2NwiYsPg.htm)|Ver pourpre|`@Compendium[pf2e.pathfinder-bestiary.4h9jhODg2NwiYsPg]`|
|[Quasit](pathfinder-bestiary/9jF564DF6ylEovna.htm)|Quasit|`@Compendium[pf2e.pathfinder-bestiary.9jF564DF6ylEovna]`|
|[Quatoid](pathfinder-bestiary/Jgh0WGVetNXi5jlM.htm)|Quatoid|`@Compendium[pf2e.pathfinder-bestiary.Jgh0WGVetNXi5jlM]`|
|[Quelaunt](pathfinder-bestiary/x26dyZZZRvZpzK2X.htm)|Quelaunt|`@Compendium[pf2e.pathfinder-bestiary.x26dyZZZRvZpzK2X]`|
|[Quetzalcoatlus](pathfinder-bestiary/8JvzSTwQWtOsxRfL.htm)|Quetzalcoatlus|`@Compendium[pf2e.pathfinder-bestiary.8JvzSTwQWtOsxRfL]`|
|[Raja Rakshasa](pathfinder-bestiary/hOXpmWmXU8N3n4Bw.htm)|Rakshasa raja|`@Compendium[pf2e.pathfinder-bestiary.hOXpmWmXU8N3n4Bw]`|
|[Rat Swarm](pathfinder-bestiary/sPClc6y3dT3XZupv.htm)|Nuée de rats|`@Compendium[pf2e.pathfinder-bestiary.sPClc6y3dT3XZupv]`|
|[Ratfolk Grenadier](pathfinder-bestiary/XbClt5wkqECrQToJ.htm)|Grenadier homme-rat|`@Compendium[pf2e.pathfinder-bestiary.XbClt5wkqECrQToJ]`|
|[Redcap](pathfinder-bestiary/BAD7npndaooB3Pz1.htm)|Bonnet rouge|`@Compendium[pf2e.pathfinder-bestiary.BAD7npndaooB3Pz1]`|
|[Reefclaw](pathfinder-bestiary/URREWYZtc8QJ9ld6.htm)|Pince des récifs|`@Compendium[pf2e.pathfinder-bestiary.URREWYZtc8QJ9ld6]`|
|[Remorhaz](pathfinder-bestiary/Zbvh6ChdD0TWv257.htm)|Rémorhaz|`@Compendium[pf2e.pathfinder-bestiary.Zbvh6ChdD0TWv257]`|
|[Riding Dog](pathfinder-bestiary/5Azg87M6OnQ7Q4ZS.htm)|Chien de selle|`@Compendium[pf2e.pathfinder-bestiary.5Azg87M6OnQ7Q4ZS]`|
|[Riding Horse](pathfinder-bestiary/In2nNwo3JL1RXQhj.htm)|Cheval de selle|`@Compendium[pf2e.pathfinder-bestiary.In2nNwo3JL1RXQhj]`|
|[Riding Pony](pathfinder-bestiary/YUk9S6caKqheRsUQ.htm)|Poney de selle|`@Compendium[pf2e.pathfinder-bestiary.YUk9S6caKqheRsUQ]`|
|[River Drake](pathfinder-bestiary/DX1xNtucLTenn3P3.htm)|Drake des rivières|`@Compendium[pf2e.pathfinder-bestiary.DX1xNtucLTenn3P3]`|
|[Roc](pathfinder-bestiary/2HvXtedQziTTfI0S.htm)|Roc|`@Compendium[pf2e.pathfinder-bestiary.2HvXtedQziTTfI0S]`|
|[Roper](pathfinder-bestiary/ZXbr1ke1vF0ZFKY3.htm)|Enlaceur|`@Compendium[pf2e.pathfinder-bestiary.ZXbr1ke1vF0ZFKY3]`|
|[Rune Giant](pathfinder-bestiary/oMcHaTX5unOHC2Pm.htm)|Géant des runes|`@Compendium[pf2e.pathfinder-bestiary.oMcHaTX5unOHC2Pm]`|
|[Rust Monster](pathfinder-bestiary/mEmWRqTRxLUZQYSh.htm)|Oxydeur|`@Compendium[pf2e.pathfinder-bestiary.mEmWRqTRxLUZQYSh]`|
|[Salamander](pathfinder-bestiary/CHIh3vMssFixUlw8.htm)|Salamandre|`@Compendium[pf2e.pathfinder-bestiary.CHIh3vMssFixUlw8]`|
|[Satyr](pathfinder-bestiary/3kLXBdtKpUsU8ey5.htm)|Satyre|`@Compendium[pf2e.pathfinder-bestiary.3kLXBdtKpUsU8ey5]`|
|[Scorpion Swarm](pathfinder-bestiary/IpzDMSmJ42alvf9F.htm)|Nuée de scorpions|`@Compendium[pf2e.pathfinder-bestiary.IpzDMSmJ42alvf9F]`|
|[Sea Devil Baron](pathfinder-bestiary/HFbZ580RDOG6Rxz2.htm)|Baron diable des mers|`@Compendium[pf2e.pathfinder-bestiary.HFbZ580RDOG6Rxz2]`|
|[Sea Devil Brute](pathfinder-bestiary/UrviURGu5o9LkhxN.htm)|Brute diable des mers|`@Compendium[pf2e.pathfinder-bestiary.UrviURGu5o9LkhxN]`|
|[Sea Devil Scout](pathfinder-bestiary/PXru127aJljwKhSy.htm)|Éclaireur diable de la mer|`@Compendium[pf2e.pathfinder-bestiary.PXru127aJljwKhSy]`|
|[Sea Hag](pathfinder-bestiary/f3c1CS2W8Tft3hW7.htm)|Guenaude marine|`@Compendium[pf2e.pathfinder-bestiary.f3c1CS2W8Tft3hW7]`|
|[Sea Serpent](pathfinder-bestiary/CFHLgMj8zHLqcagc.htm)|Serpent de mer|`@Compendium[pf2e.pathfinder-bestiary.CFHLgMj8zHLqcagc]`|
|[Sewer Ooze](pathfinder-bestiary/Hkq9ZS2J2iKnT7vT.htm)|Vase des égouts|`@Compendium[pf2e.pathfinder-bestiary.Hkq9ZS2J2iKnT7vT]`|
|[Shadow](pathfinder-bestiary/XrmHgbKgcHDi4OnK.htm)|Ombre|`@Compendium[pf2e.pathfinder-bestiary.XrmHgbKgcHDi4OnK]`|
|[Shaitan](pathfinder-bestiary/xqnl2eFzy5H2NZoQ.htm)|Shaitan|`@Compendium[pf2e.pathfinder-bestiary.xqnl2eFzy5H2NZoQ]`|
|[Shambler](pathfinder-bestiary/6XlGTt3RveX49YbC.htm)|Grand tertre|`@Compendium[pf2e.pathfinder-bestiary.6XlGTt3RveX49YbC]`|
|[Shemhazian](pathfinder-bestiary/e8rmI5xt6IANatfX.htm)|Shemhazian|`@Compendium[pf2e.pathfinder-bestiary.e8rmI5xt6IANatfX]`|
|[Shield Archon](pathfinder-bestiary/qkWkshBHyTbLP07b.htm)|Archon bouclier|`@Compendium[pf2e.pathfinder-bestiary.qkWkshBHyTbLP07b]`|
|[Shining Child](pathfinder-bestiary/t4JYGYJqT1CaqKvh.htm)|Enfant lumineux|`@Compendium[pf2e.pathfinder-bestiary.t4JYGYJqT1CaqKvh]`|
|[Shoggoth](pathfinder-bestiary/Q3EaaLLx5kDXb5vQ.htm)|Shoggoth|`@Compendium[pf2e.pathfinder-bestiary.Q3EaaLLx5kDXb5vQ]`|
|[Shuln](pathfinder-bestiary/fxYMucI5b2IUoBpw.htm)|Shuln|`@Compendium[pf2e.pathfinder-bestiary.fxYMucI5b2IUoBpw]`|
|[Simurgh](pathfinder-bestiary/ASc5sT2hU84tp1fx.htm)|Simurgh|`@Compendium[pf2e.pathfinder-bestiary.ASc5sT2hU84tp1fx]`|
|[Sinspawn](pathfinder-bestiary/ymhcfbqpaiXrcCPW.htm)|Rejeton du péché|`@Compendium[pf2e.pathfinder-bestiary.ymhcfbqpaiXrcCPW]`|
|[Skeletal Champion](pathfinder-bestiary/RMSx2C27yty0MTva.htm)|Champion squelette|`@Compendium[pf2e.pathfinder-bestiary.RMSx2C27yty0MTva]`|
|[Skeletal Giant](pathfinder-bestiary/5MVBU86ZRw2ANMQn.htm)|Géant squelettique|`@Compendium[pf2e.pathfinder-bestiary.5MVBU86ZRw2ANMQn]`|
|[Skeletal Horse](pathfinder-bestiary/mblGfyIXWhiaNpFw.htm)|Cheval squelettique|`@Compendium[pf2e.pathfinder-bestiary.mblGfyIXWhiaNpFw]`|
|[Skeletal Hulk](pathfinder-bestiary/lo4fR4jDVzLdwwkH.htm)|Mastodonte squelettique|`@Compendium[pf2e.pathfinder-bestiary.lo4fR4jDVzLdwwkH]`|
|[Skeleton Guard](pathfinder-bestiary/5H8ZX7y5IkUBhvhF.htm)|Garde squelette|`@Compendium[pf2e.pathfinder-bestiary.5H8ZX7y5IkUBhvhF]`|
|[Skulltaker](pathfinder-bestiary/myEeYWWAgnkLwtIb.htm)|Volecrâne|`@Compendium[pf2e.pathfinder-bestiary.myEeYWWAgnkLwtIb]`|
|[Skum](pathfinder-bestiary/ZSQ4M8qeAv4dkLq1.htm)|Skum|`@Compendium[pf2e.pathfinder-bestiary.ZSQ4M8qeAv4dkLq1]`|
|[Slurk](pathfinder-bestiary/eHLDsL1LG3jQ1H6Y.htm)|Slurk|`@Compendium[pf2e.pathfinder-bestiary.eHLDsL1LG3jQ1H6Y]`|
|[Smilodon](pathfinder-bestiary/SP72xojHR0UGAWcs.htm)|Smilodon|`@Compendium[pf2e.pathfinder-bestiary.SP72xojHR0UGAWcs]`|
|[Snapping Flytrap](pathfinder-bestiary/Ey19J4nTn1dQvLtE.htm)|Attrape-mouches vorace|`@Compendium[pf2e.pathfinder-bestiary.Ey19J4nTn1dQvLtE]`|
|[Sod Hound](pathfinder-bestiary/NVWaLagWOu5tCCZu.htm)|Molosse de tourbe|`@Compendium[pf2e.pathfinder-bestiary.NVWaLagWOu5tCCZu]`|
|[Soulbound Doll](pathfinder-bestiary/j7GXfpAMO0LptnvM.htm)|Poupée des âmes|`@Compendium[pf2e.pathfinder-bestiary.j7GXfpAMO0LptnvM]`|
|[Sphinx](pathfinder-bestiary/Qa7HaKfKiosEPr94.htm)|Sphinx|`@Compendium[pf2e.pathfinder-bestiary.Qa7HaKfKiosEPr94]`|
|[Spider Swarm](pathfinder-bestiary/KpxhSWRIhG7ns5NA.htm)|Nuée d'araignées|`@Compendium[pf2e.pathfinder-bestiary.KpxhSWRIhG7ns5NA]`|
|[Sprite](pathfinder-bestiary/cDgOfBCrWcpYwRVS.htm)|Esprit follet|`@Compendium[pf2e.pathfinder-bestiary.cDgOfBCrWcpYwRVS]`|
|[Stegosaurus](pathfinder-bestiary/eP96NzLFSjua4NS5.htm)|Stégosaure|`@Compendium[pf2e.pathfinder-bestiary.eP96NzLFSjua4NS5]`|
|[Stone Giant](pathfinder-bestiary/WiOY3YbiKEJKIQQz.htm)|Géant de pierre|`@Compendium[pf2e.pathfinder-bestiary.WiOY3YbiKEJKIQQz]`|
|[Stone Golem](pathfinder-bestiary/Upf2fOZ6QgGG3seI.htm)|Golem de pierre|`@Compendium[pf2e.pathfinder-bestiary.Upf2fOZ6QgGG3seI]`|
|[Stone Mauler](pathfinder-bestiary/NeYU7wwCv0RUesZ1.htm)|Broyeur de pierre|`@Compendium[pf2e.pathfinder-bestiary.NeYU7wwCv0RUesZ1]`|
|[Storm Giant](pathfinder-bestiary/B0QyZEjAXUG5TsJU.htm)|Géant des tempêtes|`@Compendium[pf2e.pathfinder-bestiary.B0QyZEjAXUG5TsJU]`|
|[Storm Lord](pathfinder-bestiary/LUtSo30fQWj7mrDn.htm)|Seigneur des tempêtes|`@Compendium[pf2e.pathfinder-bestiary.LUtSo30fQWj7mrDn]`|
|[Succubus](pathfinder-bestiary/AiPXegCJ1leUslTm.htm)|Succube|`@Compendium[pf2e.pathfinder-bestiary.AiPXegCJ1leUslTm]`|
|[Tarn Linnorm](pathfinder-bestiary/2IrbfdtWyXiGOLBA.htm)|Linnorm des lacs|`@Compendium[pf2e.pathfinder-bestiary.2IrbfdtWyXiGOLBA]`|
|[Tengu Sneak](pathfinder-bestiary/70JDH25JLTC4t5Ko.htm)|Chipeur Tengu|`@Compendium[pf2e.pathfinder-bestiary.70JDH25JLTC4t5Ko]`|
|[Terotricus](pathfinder-bestiary/cZsaAKlEYWZUO1CV.htm)|Terotricus|`@Compendium[pf2e.pathfinder-bestiary.cZsaAKlEYWZUO1CV]`|
|[Tidal Master](pathfinder-bestiary/6UczvqBDlGNXcIlW.htm)|Maître des marées|`@Compendium[pf2e.pathfinder-bestiary.6UczvqBDlGNXcIlW]`|
|[Tiefling Adept](pathfinder-bestiary/tm3Tixb7IDoLdJ5k.htm)|Adepte tieffelin|`@Compendium[pf2e.pathfinder-bestiary.tm3Tixb7IDoLdJ5k]`|
|[Tiger](pathfinder-bestiary/ypLkUfuHHfNDsVUQ.htm)|Tigre|`@Compendium[pf2e.pathfinder-bestiary.ypLkUfuHHfNDsVUQ]`|
|[Tor Linnorm](pathfinder-bestiary/x6wfK4UCJ6wYok9t.htm)|Linnorm des roches|`@Compendium[pf2e.pathfinder-bestiary.x6wfK4UCJ6wYok9t]`|
|[Treerazer](pathfinder-bestiary/8BloAdRqlLpt5bNg.htm)|Fléau des arbres|`@Compendium[pf2e.pathfinder-bestiary.8BloAdRqlLpt5bNg]`|
|[Triceratops](pathfinder-bestiary/WAgQt9pkzgPOlcJI.htm)|Triceratops|`@Compendium[pf2e.pathfinder-bestiary.WAgQt9pkzgPOlcJI]`|
|[Troll](pathfinder-bestiary/gvCCATlH9mPGWbsp.htm)|Troll|`@Compendium[pf2e.pathfinder-bestiary.gvCCATlH9mPGWbsp]`|
|[Troll King](pathfinder-bestiary/FwH05kDUlC8CwWTU.htm)|Roi troll|`@Compendium[pf2e.pathfinder-bestiary.FwH05kDUlC8CwWTU]`|
|[Tyrannosaurus](pathfinder-bestiary/LDQpLwN40OGefZD0.htm)|Tyrannosaure|`@Compendium[pf2e.pathfinder-bestiary.LDQpLwN40OGefZD0]`|
|[Unicorn](pathfinder-bestiary/c3iA9lkU1QY4YCY6.htm)|Licorne|`@Compendium[pf2e.pathfinder-bestiary.c3iA9lkU1QY4YCY6]`|
|[Unseen Servant](pathfinder-bestiary/j7NNPfZwD19BwSEZ.htm)|Serviteur invisible|`@Compendium[pf2e.pathfinder-bestiary.j7NNPfZwD19BwSEZ]`|
|[Uthul](pathfinder-bestiary/n1Mv0Q1MirfjBmfI.htm)|Uthul|`@Compendium[pf2e.pathfinder-bestiary.n1Mv0Q1MirfjBmfI]`|
|[Vampire Bat Swarm](pathfinder-bestiary/VAWmwDA08ZLQd8lW.htm)|Nuée de chauves-souris vampires|`@Compendium[pf2e.pathfinder-bestiary.VAWmwDA08ZLQd8lW]`|
|[Vampire Count](pathfinder-bestiary/Cq8sRhVVF0hagBu6.htm)|Vampire, comte|`@Compendium[pf2e.pathfinder-bestiary.Cq8sRhVVF0hagBu6]`|
|[Vampire Mastermind](pathfinder-bestiary/yxCmLBpw6xqWFU3E.htm)|Vampire, cerveau|`@Compendium[pf2e.pathfinder-bestiary.yxCmLBpw6xqWFU3E]`|
|[Vampire Spawn Rogue](pathfinder-bestiary/H2ZxTHZOEigpH4LK.htm)|Rejeton du vampire roublard|`@Compendium[pf2e.pathfinder-bestiary.H2ZxTHZOEigpH4LK]`|
|[Veiled Master](pathfinder-bestiary/9AlfVoEMLwDODjxl.htm)|Maître voilé|`@Compendium[pf2e.pathfinder-bestiary.9AlfVoEMLwDODjxl]`|
|[Velociraptor](pathfinder-bestiary/S5z0mtoEhbz7BvE9.htm)|Velociraptor|`@Compendium[pf2e.pathfinder-bestiary.S5z0mtoEhbz7BvE9]`|
|[Viper](pathfinder-bestiary/lFlXmieuHTBIonhj.htm)|Vipère|`@Compendium[pf2e.pathfinder-bestiary.lFlXmieuHTBIonhj]`|
|[Voidworm](pathfinder-bestiary/8meqlz36gPHTTvNz.htm)|Ver du néant|`@Compendium[pf2e.pathfinder-bestiary.8meqlz36gPHTTvNz]`|
|[Vrock](pathfinder-bestiary/4cPw8hZwW6uvyzvh.htm)|Vrock|`@Compendium[pf2e.pathfinder-bestiary.4cPw8hZwW6uvyzvh]`|
|[War Horse](pathfinder-bestiary/Jy2va0NTTbaUH1zP.htm)|Cheval de bataille|`@Compendium[pf2e.pathfinder-bestiary.Jy2va0NTTbaUH1zP]`|
|[War Pony](pathfinder-bestiary/OPavstjKhgcp30fc.htm)|Poney de bataille|`@Compendium[pf2e.pathfinder-bestiary.OPavstjKhgcp30fc]`|
|[Warg](pathfinder-bestiary/Sft7n3LMmnTxhhYn.htm)|Warg|`@Compendium[pf2e.pathfinder-bestiary.Sft7n3LMmnTxhhYn]`|
|[Warsworn](pathfinder-bestiary/NpcS7iocNNsno6lE.htm)|Guerrlier|`@Compendium[pf2e.pathfinder-bestiary.NpcS7iocNNsno6lE]`|
|[Wasp Swarm](pathfinder-bestiary/qcZvFCAnslI9XNTR.htm)|Nuée de guêpes|`@Compendium[pf2e.pathfinder-bestiary.qcZvFCAnslI9XNTR]`|
|[Water Mephit](pathfinder-bestiary/KFf6pJmFmzj7HH9i.htm)|Méphite de l'eau|`@Compendium[pf2e.pathfinder-bestiary.KFf6pJmFmzj7HH9i]`|
|[Web Lurker](pathfinder-bestiary/ZMr28tFTA5NUcBTi.htm)|Rôdeur des toiles|`@Compendium[pf2e.pathfinder-bestiary.ZMr28tFTA5NUcBTi]`|
|[Wemmuth](pathfinder-bestiary/NTXm3ee7WZJ92Sww.htm)|Wemmuth|`@Compendium[pf2e.pathfinder-bestiary.NTXm3ee7WZJ92Sww]`|
|[Wendigo](pathfinder-bestiary/RDFvGocLW0OuHmlC.htm)|Wendigo|`@Compendium[pf2e.pathfinder-bestiary.RDFvGocLW0OuHmlC]`|
|[Werebear](pathfinder-bestiary/5U13zQ77DIcqpH9U.htm)|Ours-garou|`@Compendium[pf2e.pathfinder-bestiary.5U13zQ77DIcqpH9U]`|
|[Wererat](pathfinder-bestiary/tXHUr947sanB5tdN.htm)|Rat-garou|`@Compendium[pf2e.pathfinder-bestiary.tXHUr947sanB5tdN]`|
|[Werewolf](pathfinder-bestiary/bLMoqt9xqTZKnjxr.htm)|Loup-garou|`@Compendium[pf2e.pathfinder-bestiary.bLMoqt9xqTZKnjxr]`|
|[Wight](pathfinder-bestiary/ZPjQkKVMi3xoPcU0.htm)|Nécrophage|`@Compendium[pf2e.pathfinder-bestiary.ZPjQkKVMi3xoPcU0]`|
|[Will-o'-Wisp](pathfinder-bestiary/wRQ7TZdd0n5UIIao.htm)|Feu follet|`@Compendium[pf2e.pathfinder-bestiary.wRQ7TZdd0n5UIIao]`|
|[Winter Wolf](pathfinder-bestiary/v92cB3RBUMhysOpD.htm)|Loup arctique|`@Compendium[pf2e.pathfinder-bestiary.v92cB3RBUMhysOpD]`|
|[Wolf](pathfinder-bestiary/Oilfs8Atv2LjAsUS.htm)|Loup|`@Compendium[pf2e.pathfinder-bestiary.Oilfs8Atv2LjAsUS]`|
|[Wraith](pathfinder-bestiary/HbROgIcU9Z9m6XuD.htm)|Âme en peine|`@Compendium[pf2e.pathfinder-bestiary.HbROgIcU9Z9m6XuD]`|
|[Wyvern](pathfinder-bestiary/wuaSG22lLjQ6yali.htm)|Vouivre|`@Compendium[pf2e.pathfinder-bestiary.wuaSG22lLjQ6yali]`|
|[Xorn](pathfinder-bestiary/4npwV3fuBm3sBCPG.htm)|Xorn|`@Compendium[pf2e.pathfinder-bestiary.4npwV3fuBm3sBCPG]`|
|[Xulgath Leader](pathfinder-bestiary/I4o2Gqpr2ioiUXA9.htm)|Chef xulgath|`@Compendium[pf2e.pathfinder-bestiary.I4o2Gqpr2ioiUXA9]`|
|[Xulgath Skulker](pathfinder-bestiary/4zXn6xaaxo1DtIRk.htm)|Vadrouilleur Xulgath|`@Compendium[pf2e.pathfinder-bestiary.4zXn6xaaxo1DtIRk]`|
|[Xulgath Warrior](pathfinder-bestiary/RTzFvmdSCf5yhguy.htm)|Guerrier xulgath|`@Compendium[pf2e.pathfinder-bestiary.RTzFvmdSCf5yhguy]`|
|[Yeti](pathfinder-bestiary/7ZgQuis8r8YQyUnI.htm)|Yeti|`@Compendium[pf2e.pathfinder-bestiary.7ZgQuis8r8YQyUnI]`|
|[Young Black Dragon](pathfinder-bestiary/v0GZ6zDWhD1Y3xs9.htm)|Jeune dragon noir|`@Compendium[pf2e.pathfinder-bestiary.v0GZ6zDWhD1Y3xs9]`|
|[Young Blue Dragon](pathfinder-bestiary/ar0tszs8NAgQamHX.htm)|Jeune dragon bleu|`@Compendium[pf2e.pathfinder-bestiary.ar0tszs8NAgQamHX]`|
|[Young Brass Dragon](pathfinder-bestiary/uA0cv4OT5mQnym0V.htm)|Jeune dragon d'airain|`@Compendium[pf2e.pathfinder-bestiary.uA0cv4OT5mQnym0V]`|
|[Young Bronze Dragon](pathfinder-bestiary/uQOTCiHslczvrNpt.htm)|Jeune dragon de bronze|`@Compendium[pf2e.pathfinder-bestiary.uQOTCiHslczvrNpt]`|
|[Young Copper Dragon](pathfinder-bestiary/3W2PMV6IbVdouOB1.htm)|Jeune dragon de cuivre|`@Compendium[pf2e.pathfinder-bestiary.3W2PMV6IbVdouOB1]`|
|[Young Gold Dragon](pathfinder-bestiary/DolNTN9s2p79N8Cy.htm)|Jeune dragon d'or|`@Compendium[pf2e.pathfinder-bestiary.DolNTN9s2p79N8Cy]`|
|[Young Green Dragon](pathfinder-bestiary/xZQIChzzIYbNIEoT.htm)|Jeune dragon vert|`@Compendium[pf2e.pathfinder-bestiary.xZQIChzzIYbNIEoT]`|
|[Young Red Dragon](pathfinder-bestiary/sUGKrcx7kbHkWnwG.htm)|Jeune dragon rouge|`@Compendium[pf2e.pathfinder-bestiary.sUGKrcx7kbHkWnwG]`|
|[Young Silver Dragon](pathfinder-bestiary/YPLZkAlOuaj3F3nZ.htm)|Jeune dragon d'argent|`@Compendium[pf2e.pathfinder-bestiary.YPLZkAlOuaj3F3nZ]`|
|[Young White Dragon](pathfinder-bestiary/3OPUMuBQThM5EXjz.htm)|Jeune dragon blanc|`@Compendium[pf2e.pathfinder-bestiary.3OPUMuBQThM5EXjz]`|
|[Zaramuun](pathfinder-bestiary/31kXa1P0LUl38jYG.htm)|Zaramuun|`@Compendium[pf2e.pathfinder-bestiary.31kXa1P0LUl38jYG]`|
|[Zephyr Hawk](pathfinder-bestiary/cBHpMcVaLRPZu9po.htm)|Faucon zéphyr|`@Compendium[pf2e.pathfinder-bestiary.cBHpMcVaLRPZu9po]`|
|[Zombie Brute](pathfinder-bestiary/Ix1PziAEk9IIMYBz.htm)|Brute zombie|`@Compendium[pf2e.pathfinder-bestiary.Ix1PziAEk9IIMYBz]`|
|[Zombie Hulk](pathfinder-bestiary/FJo8VkrM7kLkHa5D.htm)|Mastodonte zombie|`@Compendium[pf2e.pathfinder-bestiary.FJo8VkrM7kLkHa5D]`|
|[Zombie Shambler](pathfinder-bestiary/0plBflWwrCWQO2RO.htm)|Titubeur zombie|`@Compendium[pf2e.pathfinder-bestiary.0plBflWwrCWQO2RO]`|


## Bestiaire 2

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Aapoph Serpentfolk](pathfinder-bestiary-2/fv91xoQJlogVbruW.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.fv91xoQJlogVbruW]`|
|[Adult Brine Dragon](pathfinder-bestiary-2/PXTQ8wTvnd5qF1hW.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.PXTQ8wTvnd5qF1hW]`|
|[Adult Cloud Dragon](pathfinder-bestiary-2/NoCtPLRoVvloZe63.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.NoCtPLRoVvloZe63]`|
|[Adult Crystal Dragon](pathfinder-bestiary-2/UJHJVPPvV0VbQqlW.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.UJHJVPPvV0VbQqlW]`|
|[Adult Magma Dragon](pathfinder-bestiary-2/FnLuqlzLl4zCE9pJ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.FnLuqlzLl4zCE9pJ]`|
|[Adult Umbral Dragon](pathfinder-bestiary-2/HzeOxVy3VXxaJmC5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HzeOxVy3VXxaJmC5]`|
|[Ahuizotl](pathfinder-bestiary-2/UgEN8JYAT6tLbryy.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.UgEN8JYAT6tLbryy]`|
|[Akata](pathfinder-bestiary-2/nRsH7unLIRWq0ZHC.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.nRsH7unLIRWq0ZHC]`|
|[Akizendri](pathfinder-bestiary-2/27R4ntwgch7eIQM6.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.27R4ntwgch7eIQM6]`|
|[Amoeba Swarm](pathfinder-bestiary-2/kapPWNOxXOLcd4VF.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.kapPWNOxXOLcd4VF]`|
|[Anancus](pathfinder-bestiary-2/aATQbGnSA0KOWG53.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.aATQbGnSA0KOWG53]`|
|[Ancient Brine Dragon](pathfinder-bestiary-2/oMZaSpBmWddKZ1KN.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.oMZaSpBmWddKZ1KN]`|
|[Ancient Cloud Dragon](pathfinder-bestiary-2/zA9tCwjyViY6ZPfS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.zA9tCwjyViY6ZPfS]`|
|[Ancient Crystal Dragon](pathfinder-bestiary-2/wtxNIFVSpVFuYfmI.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wtxNIFVSpVFuYfmI]`|
|[Ancient Magma Dragon](pathfinder-bestiary-2/JKBO2qqNrRezSb7d.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.JKBO2qqNrRezSb7d]`|
|[Ancient Umbral Dragon](pathfinder-bestiary-2/BSlvgDdLkTXU9hRF.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.BSlvgDdLkTXU9hRF]`|
|[Animate Dream](pathfinder-bestiary-2/Y9SDbANT1DWI1DKU.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Y9SDbANT1DWI1DKU]`|
|[Ankou](pathfinder-bestiary-2/DxV2vH17H789bJ6y.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.DxV2vH17H789bJ6y]`|
|[Aolaz](pathfinder-bestiary-2/WMiPblOVq5w2Fa1x.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.WMiPblOVq5w2Fa1x]`|
|[Army Ant Swarm](pathfinder-bestiary-2/8a9N2iPA5HLpkWaF.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.8a9N2iPA5HLpkWaF]`|
|[Assassin Vine](pathfinder-bestiary-2/DWQzgPL1WjXfu4rV.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.DWQzgPL1WjXfu4rV]`|
|[Athach](pathfinder-bestiary-2/CwrVQsRAeqlr1Vh0.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.CwrVQsRAeqlr1Vh0]`|
|[Attic Whisperer](pathfinder-bestiary-2/ril4ohkYrf5mGwHO.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ril4ohkYrf5mGwHO]`|
|[Augnagar](pathfinder-bestiary-2/Y1lwEf06O1ijOnwt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Y1lwEf06O1ijOnwt]`|
|[Augur](pathfinder-bestiary-2/BTQ2ARKulg5Egueg.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.BTQ2ARKulg5Egueg]`|
|[Aurumvorax](pathfinder-bestiary-2/zjPdqdQGNjO3Kto7.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.zjPdqdQGNjO3Kto7]`|
|[Azuretzi](pathfinder-bestiary-2/HBkk9MYLomjKDKr4.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HBkk9MYLomjKDKr4]`|
|[Babau](pathfinder-bestiary-2/Ud6Iy1GolgtwrKQK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Ud6Iy1GolgtwrKQK]`|
|[Badger](pathfinder-bestiary-2/PKAs8bMBMpnegmwq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.PKAs8bMBMpnegmwq]`|
|[Baobhan Sith](pathfinder-bestiary-2/SfPoGbcaQqPlgJ85.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SfPoGbcaQqPlgJ85]`|
|[Basidirond](pathfinder-bestiary-2/bAaVfrdNLfFh7iHv.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.bAaVfrdNLfFh7iHv]`|
|[Bastion Archon](pathfinder-bestiary-2/H37tfTiLiUhpYxaS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.H37tfTiLiUhpYxaS]`|
|[Bebilith](pathfinder-bestiary-2/vg3nE7p855SXkXbz.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.vg3nE7p855SXkXbz]`|
|[Behemoth Hippopotamus](pathfinder-bestiary-2/VrkmTQKB3Ln7wVTr.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.VrkmTQKB3Ln7wVTr]`|
|[Behir](pathfinder-bestiary-2/6wpHJXze0RLxl780.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6wpHJXze0RLxl780]`|
|[Belker](pathfinder-bestiary-2/QLY246Z6hP41LnCF.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.QLY246Z6hP41LnCF]`|
|[Black Bear](pathfinder-bestiary-2/xxP5FJotshmUQNtY.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xxP5FJotshmUQNtY]`|
|[Black Scorpion](pathfinder-bestiary-2/O9LzsedLTUVFtOix.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.O9LzsedLTUVFtOix]`|
|[Blindheim](pathfinder-bestiary-2/B8dOh27iarR7rZI3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.B8dOh27iarR7rZI3]`|
|[Blink Dog](pathfinder-bestiary-2/b0jMGY65XVyuKUjf.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.b0jMGY65XVyuKUjf]`|
|[Blizzardborn](pathfinder-bestiary-2/2Kw49I6EZbKKlTtK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.2Kw49I6EZbKKlTtK]`|
|[Blodeuwedd](pathfinder-bestiary-2/nzLGh9GKCk7imto3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.nzLGh9GKCk7imto3]`|
|[Blue-Ringed Octopus](pathfinder-bestiary-2/WxSpfUReYJb4StxG.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.WxSpfUReYJb4StxG]`|
|[Bodak](pathfinder-bestiary-2/jCJUvzs1FaL6PAkq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.jCJUvzs1FaL6PAkq]`|
|[Bog Mummy](pathfinder-bestiary-2/hIWzgjW2E2K6ImPA.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.hIWzgjW2E2K6ImPA]`|
|[Bog Strider](pathfinder-bestiary-2/SG8fOxo4SS7bXpav.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SG8fOxo4SS7bXpav]`|
|[Bone Prophet](pathfinder-bestiary-2/7SVhYtnBn967Hy8O.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.7SVhYtnBn967Hy8O]`|
|[Bottlenose Dolphin](pathfinder-bestiary-2/8usfZlFqdD9cchPJ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.8usfZlFqdD9cchPJ]`|
|[Bralani](pathfinder-bestiary-2/ZrDGTNxlISyxNAKX.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ZrDGTNxlISyxNAKX]`|
|[Brood Leech Swarm](pathfinder-bestiary-2/dvZM7hGNDu8olV7A.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.dvZM7hGNDu8olV7A]`|
|[Brownie](pathfinder-bestiary-2/SFpMOxBwiS529dJk.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SFpMOxBwiS529dJk]`|
|[Bythos](pathfinder-bestiary-2/qjAtPDIi7OyUGsNy.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.qjAtPDIi7OyUGsNy]`|
|[Cairn Linnorm](pathfinder-bestiary-2/Wtok8DP7ybbEJ1fB.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Wtok8DP7ybbEJ1fB]`|
|[Cairn Wight](pathfinder-bestiary-2/sMNqIHHxcVBXG08h.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.sMNqIHHxcVBXG08h]`|
|[Calathgar](pathfinder-bestiary-2/DC9b8mG1KdyXZkLp.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.DC9b8mG1KdyXZkLp]`|
|[Caligni Slayer](pathfinder-bestiary-2/cijMuXBk8YUw2RoB.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.cijMuXBk8YUw2RoB]`|
|[Carbuncle](pathfinder-bestiary-2/tdCyjq57rgfPezjT.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tdCyjq57rgfPezjT]`|
|[Carnivorous Blob](pathfinder-bestiary-2/NlMqwcN2ZaALOVSY.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.NlMqwcN2ZaALOVSY]`|
|[Carrion Golem](pathfinder-bestiary-2/sbVdWfLZ2rdPh9Ec.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.sbVdWfLZ2rdPh9Ec]`|
|[Catoblepas](pathfinder-bestiary-2/BJoM8LAVP8MA5o8v.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.BJoM8LAVP8MA5o8v]`|
|[Catrina](pathfinder-bestiary-2/t8XwrLnvMnz3Z1al.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.t8XwrLnvMnz3Z1al]`|
|[Cave Fisher](pathfinder-bestiary-2/OERK0GGnaEOthcdV.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.OERK0GGnaEOthcdV]`|
|[Cave Scorpion](pathfinder-bestiary-2/e0YvEWJ5zpU946G4.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.e0YvEWJ5zpU946G4]`|
|[Cavern Troll](pathfinder-bestiary-2/8WAc7n2M4Ks00yvj.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.8WAc7n2M4Ks00yvj]`|
|[Chernobue](pathfinder-bestiary-2/18NInoyRLIrWE0nt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.18NInoyRLIrWE0nt]`|
|[Choker](pathfinder-bestiary-2/O69kXMo6shkA606k.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.O69kXMo6shkA606k]`|
|[Chupacabra](pathfinder-bestiary-2/JCoILqEvwovT7tkj.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.JCoILqEvwovT7tkj]`|
|[Cockroach Swarm](pathfinder-bestiary-2/8HJNH3aDTJyJi3pU.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.8HJNH3aDTJyJi3pU]`|
|[Coil Spy](pathfinder-bestiary-2/PAGytLw1QnAiNYC2.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.PAGytLw1QnAiNYC2]`|
|[Compsognathus](pathfinder-bestiary-2/XZZdG5RosoYsF237.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.XZZdG5RosoYsF237]`|
|[Cornugon](pathfinder-bestiary-2/i8C4jqI3VdozylBL.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.i8C4jqI3VdozylBL]`|
|[Crawling Hand](pathfinder-bestiary-2/oSQ3Q9tzTohksAtf.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.oSQ3Q9tzTohksAtf]`|
|[Culdewen](pathfinder-bestiary-2/coNTMrHerjBIf2ig.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.coNTMrHerjBIf2ig]`|
|[Cythnigot](pathfinder-bestiary-2/QR3AC4mkpfz6KPSh.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.QR3AC4mkpfz6KPSh]`|
|[D'ziriak](pathfinder-bestiary-2/aFRGgQWWCDIiBlUl.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.aFRGgQWWCDIiBlUl]`|
|[Denizen of Leng](pathfinder-bestiary-2/tOVGYQueeJwLtn0v.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tOVGYQueeJwLtn0v]`|
|[Derghodaemon](pathfinder-bestiary-2/oKogG9AwUZ9iBsm3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.oKogG9AwUZ9iBsm3]`|
|[Destrachan](pathfinder-bestiary-2/xUtl7Nkb6iJpPUMK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xUtl7Nkb6iJpPUMK]`|
|[Devourer](pathfinder-bestiary-2/WKsogmP2X2F68Zdm.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.WKsogmP2X2F68Zdm]`|
|[Dig-Widget](pathfinder-bestiary-2/57pzXwM9qFiRTwq9.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.57pzXwM9qFiRTwq9]`|
|[Doprillu](pathfinder-bestiary-2/prNJZOU8tdRIZITT.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.prNJZOU8tdRIZITT]`|
|[Dracolisk](pathfinder-bestiary-2/7kAbAOHJBq28FOXJ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.7kAbAOHJBq28FOXJ]`|
|[Drainberry Bush](pathfinder-bestiary-2/Z9rOUqCqAaMjvgBc.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Z9rOUqCqAaMjvgBc]`|
|[Draugr](pathfinder-bestiary-2/48agy7vl9fQW1LLA.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.48agy7vl9fQW1LLA]`|
|[Dread Wraith](pathfinder-bestiary-2/ZGEf08j9q4fIPDFd.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ZGEf08j9q4fIPDFd]`|
|[Dream Spider](pathfinder-bestiary-2/Z2v611Np3J1MBkpv.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Z2v611Np3J1MBkpv]`|
|[Duneshaker Solifugid](pathfinder-bestiary-2/r3yLNfqS00g8sdrT.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.r3yLNfqS00g8sdrT]`|
|[Dust Mephit](pathfinder-bestiary-2/m40M5R7wcm5DSVK2.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.m40M5R7wcm5DSVK2]`|
|[Dweomercat](pathfinder-bestiary-2/n6cv7eCzPSs4dbHn.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.n6cv7eCzPSs4dbHn]`|
|[Earthen Destrier](pathfinder-bestiary-2/CrAsEzLl3F2vejd3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.CrAsEzLl3F2vejd3]`|
|[Elasmosaurus](pathfinder-bestiary-2/VYEPG1G64OaCQmwf.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.VYEPG1G64OaCQmwf]`|
|[Ember Fox](pathfinder-bestiary-2/6ic6oNCpWjkVAsIN.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6ic6oNCpWjkVAsIN]`|
|[Emperor Cobra](pathfinder-bestiary-2/iLZpGlyBWxS4Idbi.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.iLZpGlyBWxS4Idbi]`|
|[Eremite](pathfinder-bestiary-2/hmnNDCNV425MisdA.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.hmnNDCNV425MisdA]`|
|[Esobok](pathfinder-bestiary-2/yvmX4J21XE1wqDar.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.yvmX4J21XE1wqDar]`|
|[Evangelist](pathfinder-bestiary-2/OuimMwM3yFRy5tRK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.OuimMwM3yFRy5tRK]`|
|[Fen Mosquito Swarm](pathfinder-bestiary-2/l3kFjH4n9Shet6bz.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.l3kFjH4n9Shet6bz]`|
|[Fetchling Scout](pathfinder-bestiary-2/aVf2IojxU9ZZ64M5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.aVf2IojxU9ZZ64M5]`|
|[Filth Fire](pathfinder-bestiary-2/r5i3MkSNgNBYa5Cm.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.r5i3MkSNgNBYa5Cm]`|
|[Fire Jellyfish Swarm](pathfinder-bestiary-2/7lL7APeHQgkVxU1B.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.7lL7APeHQgkVxU1B]`|
|[Fire Yai](pathfinder-bestiary-2/a8Ih1RIr4UUV4QCL.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.a8Ih1RIr4UUV4QCL]`|
|[Fjord Linnorm](pathfinder-bestiary-2/sY3VkazdyPiMgMj1.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.sY3VkazdyPiMgMj1]`|
|[Flytrap Leshy](pathfinder-bestiary-2/vHTvuteLd2kYX3ib.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.vHTvuteLd2kYX3ib]`|
|[Froghemoth](pathfinder-bestiary-2/RFCxaIS4OkacCpxD.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.RFCxaIS4OkacCpxD]`|
|[Frost Troll](pathfinder-bestiary-2/HmeEHIXL46rBQWAa.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HmeEHIXL46rBQWAa]`|
|[Frost Worm](pathfinder-bestiary-2/mpBOeFrbtx1wMrNn.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.mpBOeFrbtx1wMrNn]`|
|[Ghonhatine](pathfinder-bestiary-2/xdIHvCBspeSXRL6A.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xdIHvCBspeSXRL6A]`|
|[Giant Amoeba](pathfinder-bestiary-2/VPDKzeTY8O1XrKLk.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.VPDKzeTY8O1XrKLk]`|
|[Giant Ant](pathfinder-bestiary-2/URYQ5goPCmrQvJf5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.URYQ5goPCmrQvJf5]`|
|[Giant Badger](pathfinder-bestiary-2/T9vzmpvpRL5xjrhd.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.T9vzmpvpRL5xjrhd]`|
|[Giant Chameleon](pathfinder-bestiary-2/r9L6tdT44kYeN1FV.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.r9L6tdT44kYeN1FV]`|
|[Giant Cockroach](pathfinder-bestiary-2/RqABskvexmp5sT7X.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.RqABskvexmp5sT7X]`|
|[Giant Crab](pathfinder-bestiary-2/l4UOJbphMT7jZ0xB.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.l4UOJbphMT7jZ0xB]`|
|[Giant Crawling Hand](pathfinder-bestiary-2/M4mDroIJlrBYfHkM.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.M4mDroIJlrBYfHkM]`|
|[Giant Dragonfly](pathfinder-bestiary-2/b5b6J3TqbcXnmKyy.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.b5b6J3TqbcXnmKyy]`|
|[Giant Dragonfly Nymph](pathfinder-bestiary-2/xarabMfYuo7yvD1N.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xarabMfYuo7yvD1N]`|
|[Giant Fly](pathfinder-bestiary-2/3DSqfobadad3pA6O.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.3DSqfobadad3pA6O]`|
|[Giant Frog](pathfinder-bestiary-2/EWoJAp5LJeL1Qfye.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.EWoJAp5LJeL1Qfye]`|
|[Giant Hippocampus](pathfinder-bestiary-2/dwyt7e1EZPjw9KBB.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.dwyt7e1EZPjw9KBB]`|
|[Giant Jellyfish](pathfinder-bestiary-2/SKooDsYDMEloMG16.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SKooDsYDMEloMG16]`|
|[Giant Leech](pathfinder-bestiary-2/JJKxWVExhUEno9j0.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.JJKxWVExhUEno9j0]`|
|[Giant Maggot](pathfinder-bestiary-2/y9D2wh63pBzIyI7i.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.y9D2wh63pBzIyI7i]`|
|[Giant Mosquito](pathfinder-bestiary-2/uBG93M52pa84qIt5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.uBG93M52pa84qIt5]`|
|[Giant Slug](pathfinder-bestiary-2/g1htYgARxNKxc7MV.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.g1htYgARxNKxc7MV]`|
|[Giant Snapping Turtle](pathfinder-bestiary-2/fVvRASaKa6SkQkYG.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.fVvRASaKa6SkQkYG]`|
|[Giant Solifugid](pathfinder-bestiary-2/C7LvY2Xxzm2xRL1M.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.C7LvY2Xxzm2xRL1M]`|
|[Giant Squid](pathfinder-bestiary-2/32HHvah2Ji6FjJnM.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.32HHvah2Ji6FjJnM]`|
|[Giant Tick](pathfinder-bestiary-2/le7VaOJyQQnl37Wa.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.le7VaOJyQQnl37Wa]`|
|[Giant Toad](pathfinder-bestiary-2/Y1jsLqhsbqoXiGQe.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Y1jsLqhsbqoXiGQe]`|
|[Giant Whiptail Centipede](pathfinder-bestiary-2/UTnrcGoCIXR95zKi.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.UTnrcGoCIXR95zKi]`|
|[Giant Wolverine](pathfinder-bestiary-2/MCZgXBC0I4Oovcjd.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.MCZgXBC0I4Oovcjd]`|
|[Glass Golem](pathfinder-bestiary-2/ABMn623izSLHA0sx.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ABMn623izSLHA0sx]`|
|[Gorgon](pathfinder-bestiary-2/cmuCFkXdn1JuVTK4.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.cmuCFkXdn1JuVTK4]`|
|[Gosreg](pathfinder-bestiary-2/MUVc2ZjwGtlpcuX1.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.MUVc2ZjwGtlpcuX1]`|
|[Granite Glyptodont](pathfinder-bestiary-2/sGI1gh7EPVSRadhY.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.sGI1gh7EPVSRadhY]`|
|[Gray Ooze](pathfinder-bestiary-2/V6OBYa51VcehPj22.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.V6OBYa51VcehPj22]`|
|[Grendel](pathfinder-bestiary-2/cPX0NmB242FqSRxa.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.cPX0NmB242FqSRxa]`|
|[Grimstalker](pathfinder-bestiary-2/F5QibbbxUeO8A3Kj.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.F5QibbbxUeO8A3Kj]`|
|[Grindylow](pathfinder-bestiary-2/N7Ej1AyPPolzboex.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.N7Ej1AyPPolzboex]`|
|[Grippli Scout](pathfinder-bestiary-2/rpq2rHlZjy5g31xo.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.rpq2rHlZjy5g31xo]`|
|[Grodair](pathfinder-bestiary-2/OVRvL7fKERLsM4KP.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.OVRvL7fKERLsM4KP]`|
|[Gylou](pathfinder-bestiary-2/yIpB2uLyeBWQjfsn.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.yIpB2uLyeBWQjfsn]`|
|[Hadrosaurid](pathfinder-bestiary-2/GnKay44MFMZkqXRi.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.GnKay44MFMZkqXRi]`|
|[Hamatula](pathfinder-bestiary-2/tCJEjNDUxyV1GzE3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tCJEjNDUxyV1GzE3]`|
|[Hellcat](pathfinder-bestiary-2/FERUCyMwvy4Nejou.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.FERUCyMwvy4Nejou]`|
|[Hezrou](pathfinder-bestiary-2/wqiFcGPARy8zmQfd.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wqiFcGPARy8zmQfd]`|
|[Hippocampus](pathfinder-bestiary-2/z39LYFGYmOsoGVSh.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.z39LYFGYmOsoGVSh]`|
|[Hippogriff](pathfinder-bestiary-2/Xv74O1mFzzP06IXl.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Xv74O1mFzzP06IXl]`|
|[Hippopotamus](pathfinder-bestiary-2/5CixyyBkCLj2VI8q.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.5CixyyBkCLj2VI8q]`|
|[Hodag](pathfinder-bestiary-2/1QWZDaBoefL5rWkz.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.1QWZDaBoefL5rWkz]`|
|[Hound Archon](pathfinder-bestiary-2/ZKTz1ABoPTtlgYIy.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ZKTz1ABoPTtlgYIy]`|
|[Hound Of Tindalos](pathfinder-bestiary-2/JgePINrFINmlsvn3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.JgePINrFINmlsvn3]`|
|[Ice Golem](pathfinder-bestiary-2/tieZOKf2qX7GsL9H.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tieZOKf2qX7GsL9H]`|
|[Ice Mephit](pathfinder-bestiary-2/6Me4Q1JDupyCCnGg.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6Me4Q1JDupyCCnGg]`|
|[Ice Yai](pathfinder-bestiary-2/wBGUIPJMm9Van9GQ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wBGUIPJMm9Van9GQ]`|
|[Icewyrm](pathfinder-bestiary-2/N3XfcwDn1Nu7milG.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.N3XfcwDn1Nu7milG]`|
|[Icicle Snake](pathfinder-bestiary-2/veyyE0brfjIoQK1q.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.veyyE0brfjIoQK1q]`|
|[Ifrit Pyrochemist](pathfinder-bestiary-2/jhr6WpTeY8GXXwK1.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.jhr6WpTeY8GXXwK1]`|
|[Iguanodon](pathfinder-bestiary-2/SsR4wiSWA7xhlkH9.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SsR4wiSWA7xhlkH9]`|
|[Imentesh](pathfinder-bestiary-2/hvKRgNiQtQJhHk9u.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.hvKRgNiQtQJhHk9u]`|
|[Intellect Devourer](pathfinder-bestiary-2/OFHr8UhsBOOgj0ef.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.OFHr8UhsBOOgj0ef]`|
|[Interlocutor](pathfinder-bestiary-2/Et2784TVZOXIbtM1.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Et2784TVZOXIbtM1]`|
|[Invidiak](pathfinder-bestiary-2/M2BkGD9YwEmCvD9D.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.M2BkGD9YwEmCvD9D]`|
|[Irlgaunt](pathfinder-bestiary-2/10JbkieWjqKA5Hzc.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.10JbkieWjqKA5Hzc]`|
|[Irnakurse](pathfinder-bestiary-2/HhSoTr6mpwV09Y4k.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HhSoTr6mpwV09Y4k]`|
|[Isqulug](pathfinder-bestiary-2/kElZCC1zMQVftoi9.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.kElZCC1zMQVftoi9]`|
|[Jabberwock](pathfinder-bestiary-2/Y0qsrBFTFMbaiLzr.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Y0qsrBFTFMbaiLzr]`|
|[Jotund Troll](pathfinder-bestiary-2/kKH3uMHbVZQmEtxj.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.kKH3uMHbVZQmEtxj]`|
|[Jyoti](pathfinder-bestiary-2/Xb0uY2tg1Cd3kZd0.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Xb0uY2tg1Cd3kZd0]`|
|[Kelpie](pathfinder-bestiary-2/FMU6L5vbiUxa1ZF7.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.FMU6L5vbiUxa1ZF7]`|
|[Korred](pathfinder-bestiary-2/xIvv8vKBa0YsTEzt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xIvv8vKBa0YsTEzt]`|
|[Leng Spider](pathfinder-bestiary-2/pxlZ8QbxMK6b3l5w.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.pxlZ8QbxMK6b3l5w]`|
|[Leprechaun](pathfinder-bestiary-2/R0YrD0izubtQYbTU.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.R0YrD0izubtQYbTU]`|
|[Lerritan](pathfinder-bestiary-2/xfcFXLbadD3KdlHW.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xfcFXLbadD3KdlHW]`|
|[Leucrotta](pathfinder-bestiary-2/T3low0wQNMO0SEtr.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.T3low0wQNMO0SEtr]`|
|[Leydroth](pathfinder-bestiary-2/GBBVBEAsznP8A5gv.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.GBBVBEAsznP8A5gv]`|
|[Living Boulder](pathfinder-bestiary-2/zAxCU99Jwky6QoBy.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.zAxCU99Jwky6QoBy]`|
|[Living Thunderclap](pathfinder-bestiary-2/mZxVXRI1Zh3HUXxn.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.mZxVXRI1Zh3HUXxn]`|
|[Lunar Naga](pathfinder-bestiary-2/DEo5AEY1i9s0kWYZ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.DEo5AEY1i9s0kWYZ]`|
|[Lurker In Light](pathfinder-bestiary-2/73qJxJK8ES4Tbp2k.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.73qJxJK8ES4Tbp2k]`|
|[Magma Scorpion](pathfinder-bestiary-2/Z3J7uGaLmlFRfwPk.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Z3J7uGaLmlFRfwPk]`|
|[Mandragora](pathfinder-bestiary-2/aMIb3Xm64NzRj8YP.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.aMIb3Xm64NzRj8YP]`|
|[Manta Ray](pathfinder-bestiary-2/ACIqdW6VsFA1TCpN.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ACIqdW6VsFA1TCpN]`|
|[Marrmora](pathfinder-bestiary-2/jCF9HjF42SPXTptP.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.jCF9HjF42SPXTptP]`|
|[Marsh Giant](pathfinder-bestiary-2/W2gd0emEVbD8EGmf.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.W2gd0emEVbD8EGmf]`|
|[Marut](pathfinder-bestiary-2/tu31BdhnxNknt7kK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tu31BdhnxNknt7kK]`|
|[Mastodon](pathfinder-bestiary-2/ZBTqN3DNjdreXCu7.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ZBTqN3DNjdreXCu7]`|
|[Megalania](pathfinder-bestiary-2/VHXsmfnOBRBXB6TL.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.VHXsmfnOBRBXB6TL]`|
|[Meladaemon](pathfinder-bestiary-2/ytsHNgRwLzabsrZt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ytsHNgRwLzabsrZt]`|
|[Melody On The Wind](pathfinder-bestiary-2/hvoIxruNUei1auQT.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.hvoIxruNUei1auQT]`|
|[Mist Stalker](pathfinder-bestiary-2/vXXEdDy8ciGJxt3i.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.vXXEdDy8ciGJxt3i]`|
|[Mohrg](pathfinder-bestiary-2/OaNtHZAraCrVLuSo.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.OaNtHZAraCrVLuSo]`|
|[Monadic Deva](pathfinder-bestiary-2/EXeR2HC4xsvHcf6K.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.EXeR2HC4xsvHcf6K]`|
|[Moonflower](pathfinder-bestiary-2/wqsmnoby2n3BOvKu.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wqsmnoby2n3BOvKu]`|
|[Morlock](pathfinder-bestiary-2/DYBZniwR1Ip4inVr.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.DYBZniwR1Ip4inVr]`|
|[Movanic Deva](pathfinder-bestiary-2/nSDXAfFOF2gEKmkM.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.nSDXAfFOF2gEKmkM]`|
|[Mudwretch](pathfinder-bestiary-2/Hwy4YrNVVr7NnsMC.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Hwy4YrNVVr7NnsMC]`|
|[Nabasu](pathfinder-bestiary-2/jQ7zBVDCEhuMOOTl.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.jQ7zBVDCEhuMOOTl]`|
|[Nalfeshnee](pathfinder-bestiary-2/YUZF9WhDmfzma3dE.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.YUZF9WhDmfzma3dE]`|
|[Necrophidius](pathfinder-bestiary-2/y2LqIPBHv2dA0pjI.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.y2LqIPBHv2dA0pjI]`|
|[Neothelid](pathfinder-bestiary-2/wxBQsGLdti2xcGVT.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wxBQsGLdti2xcGVT]`|
|[Nereid](pathfinder-bestiary-2/FHnWzK7DsniqZWzG.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.FHnWzK7DsniqZWzG]`|
|[Nixie](pathfinder-bestiary-2/LZp7LLcFzDBlrwod.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.LZp7LLcFzDBlrwod]`|
|[Norn](pathfinder-bestiary-2/XCNRztCxn6EbJ0gr.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.XCNRztCxn6EbJ0gr]`|
|[Nuckelavee](pathfinder-bestiary-2/fRLrlY25qXkOMBNG.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.fRLrlY25qXkOMBNG]`|
|[Nuglub](pathfinder-bestiary-2/bpwaqRoO1wYo15T5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.bpwaqRoO1wYo15T5]`|
|[Nyogoth](pathfinder-bestiary-2/TzdLLC68qlgrn91m.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.TzdLLC68qlgrn91m]`|
|[Ogre Spider](pathfinder-bestiary-2/Kr5LtF0bqZJAC2Wn.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Kr5LtF0bqZJAC2Wn]`|
|[Olethrodaemon](pathfinder-bestiary-2/37FAi1y5S8snofws.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.37FAi1y5S8snofws]`|
|[Onidoshi](pathfinder-bestiary-2/7EppoGdIcmd0AV9B.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.7EppoGdIcmd0AV9B]`|
|[Ooze Mephit](pathfinder-bestiary-2/5FcYbQxHehjO4w1d.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.5FcYbQxHehjO4w1d]`|
|[Orca](pathfinder-bestiary-2/RqcIiiNNEkF28ui2.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.RqcIiiNNEkF28ui2]`|
|[Oread Guard](pathfinder-bestiary-2/xrAx0xj0UsM4DG5T.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xrAx0xj0UsM4DG5T]`|
|[Ostiarius](pathfinder-bestiary-2/89isDKkB1UCbsSvU.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.89isDKkB1UCbsSvU]`|
|[Osyluth](pathfinder-bestiary-2/xjV0TmPNfKR7UcKu.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.xjV0TmPNfKR7UcKu]`|
|[Pachycephalosaurus](pathfinder-bestiary-2/rZdaxjM7CFVAEq2e.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.rZdaxjM7CFVAEq2e]`|
|[Peluda](pathfinder-bestiary-2/drtCZDDoESPSUqH7.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.drtCZDDoESPSUqH7]`|
|[Peryton](pathfinder-bestiary-2/E4aJhHWyFDxgJcy9.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.E4aJhHWyFDxgJcy9]`|
|[Petitioner](pathfinder-bestiary-2/ouGM2y2aO0yisPAs.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ouGM2y2aO0yisPAs]`|
|[Piscodaemon](pathfinder-bestiary-2/nxR3nseHT01YmOQo.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.nxR3nseHT01YmOQo]`|
|[Planetar](pathfinder-bestiary-2/cu6Bl5pFByrpinxd.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.cu6Bl5pFByrpinxd]`|
|[Polar Bear](pathfinder-bestiary-2/UqFObUjgFAlWrriA.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.UqFObUjgFAlWrriA]`|
|[Purrodaemon](pathfinder-bestiary-2/GfJLotTcrZmn6tay.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.GfJLotTcrZmn6tay]`|
|[Quetz Couatl](pathfinder-bestiary-2/12vyw160K7p3M3Mt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.12vyw160K7p3M3Mt]`|
|[Quickling](pathfinder-bestiary-2/uQBfbNaIHdt0l2QB.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.uQBfbNaIHdt0l2QB]`|
|[Quoppopak](pathfinder-bestiary-2/3u4toki9EeSRvcs2.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.3u4toki9EeSRvcs2]`|
|[Radiant Warden](pathfinder-bestiary-2/Nk3Inn5jel1VQMLt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Nk3Inn5jel1VQMLt]`|
|[Raven](pathfinder-bestiary-2/hFbfXmjMkGEzMkQ1.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.hFbfXmjMkGEzMkQ1]`|
|[Raven Swarm](pathfinder-bestiary-2/wrzlJufGEfMg5kVE.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wrzlJufGEfMg5kVE]`|
|[Ravener](pathfinder-bestiary-2/g41X4b3q2iDMLRt3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.g41X4b3q2iDMLRt3]`|
|[Ravener Husk](pathfinder-bestiary-2/Yv0005Vym1peVKHq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Yv0005Vym1peVKHq]`|
|[Reef Octopus](pathfinder-bestiary-2/RknMXlwJcbvGhk3n.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.RknMXlwJcbvGhk3n]`|
|[Revenant](pathfinder-bestiary-2/3YcsuATyahEMygNy.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.3YcsuATyahEMygNy]`|
|[Rhinoceros](pathfinder-bestiary-2/o2IbtStBj11QCRvS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.o2IbtStBj11QCRvS]`|
|[Rusalka](pathfinder-bestiary-2/U2hAALepVC90CNhQ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.U2hAALepVC90CNhQ]`|
|[Sacristan](pathfinder-bestiary-2/7oiIDnLBDpC2r2AA.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.7oiIDnLBDpC2r2AA]`|
|[Sand Sentry](pathfinder-bestiary-2/gOjrie9YndQPomU8.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.gOjrie9YndQPomU8]`|
|[Sandpoint Devil](pathfinder-bestiary-2/YFvur5htRwt9EfQs.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.YFvur5htRwt9EfQs]`|
|[Sard](pathfinder-bestiary-2/eD1kydftMIp4CL2K.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.eD1kydftMIp4CL2K]`|
|[Sarglagon](pathfinder-bestiary-2/340AwQpRXGblw1kF.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.340AwQpRXGblw1kF]`|
|[Scarecrow](pathfinder-bestiary-2/EOiOqs1vHCxZAj3T.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.EOiOqs1vHCxZAj3T]`|
|[Sceaduinar](pathfinder-bestiary-2/LcdQZ2NTG2eJswXA.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.LcdQZ2NTG2eJswXA]`|
|[Scythe Tree](pathfinder-bestiary-2/Aa652cDdKTani3DY.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Aa652cDdKTani3DY]`|
|[Sea Drake](pathfinder-bestiary-2/tIDbOvltrFsgnwgf.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tIDbOvltrFsgnwgf]`|
|[Sea Snake](pathfinder-bestiary-2/6EX8nxH3zRotLRJG.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6EX8nxH3zRotLRJG]`|
|[Shadow Drake](pathfinder-bestiary-2/WqppjHF2Y8yC6veS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.WqppjHF2Y8yC6veS]`|
|[Shadow Giant](pathfinder-bestiary-2/SpnJQIHLeiJOjEWR.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SpnJQIHLeiJOjEWR]`|
|[Shoal Linnorm](pathfinder-bestiary-2/nipVBTEgG6itWJSW.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.nipVBTEgG6itWJSW]`|
|[Shocker Lizard](pathfinder-bestiary-2/HI7SecILsRTmOtky.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HI7SecILsRTmOtky]`|
|[Shoggti](pathfinder-bestiary-2/wByXwxI5r7YRlkAK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wByXwxI5r7YRlkAK]`|
|[Skaveling](pathfinder-bestiary-2/LQL8HU6tISXZwlOw.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.LQL8HU6tISXZwlOw]`|
|[Skrik Nettle](pathfinder-bestiary-2/LtaVzD7ADtWmBWap.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.LtaVzD7ADtWmBWap]`|
|[Skulk](pathfinder-bestiary-2/uGXTQYuRNtgPt1Pp.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.uGXTQYuRNtgPt1Pp]`|
|[Slime Mold](pathfinder-bestiary-2/6tKVMKDdvbp0VhZD.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6tKVMKDdvbp0VhZD]`|
|[Snapping Turtle](pathfinder-bestiary-2/UcUlBj3brC80A8gq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.UcUlBj3brC80A8gq]`|
|[Solar](pathfinder-bestiary-2/c4uQKZaRWv9n9IHo.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.c4uQKZaRWv9n9IHo]`|
|[Soul Eater](pathfinder-bestiary-2/SY8pvLPqc2EgP9Xj.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SY8pvLPqc2EgP9Xj]`|
|[Spark Bat](pathfinder-bestiary-2/jpjhbGvaduK4iqwq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.jpjhbGvaduK4iqwq]`|
|[Spear Frog](pathfinder-bestiary-2/QHNXT5bZDqKRzwEF.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.QHNXT5bZDqKRzwEF]`|
|[Specter](pathfinder-bestiary-2/HNEUSYnInffvGPHK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HNEUSYnInffvGPHK]`|
|[Spinosaurus](pathfinder-bestiary-2/s8EjxYqdN4gOvcBh.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.s8EjxYqdN4gOvcBh]`|
|[Spiral Centurion](pathfinder-bestiary-2/q6FDEqLZQVjZfJ3x.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.q6FDEqLZQVjZfJ3x]`|
|[Spirit Naga](pathfinder-bestiary-2/h4SZ3U9at7aT0ASv.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.h4SZ3U9at7aT0ASv]`|
|[Sportlebore Swarm](pathfinder-bestiary-2/3VFHZyjSjNA1jbiO.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.3VFHZyjSjNA1jbiO]`|
|[Spriggan Bully](pathfinder-bestiary-2/qdlP3qTZtjzZXpmQ.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.qdlP3qTZtjzZXpmQ]`|
|[Spriggan Warlord](pathfinder-bestiary-2/MDdbC8XDUPFg4FCv.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.MDdbC8XDUPFg4FCv]`|
|[Star Archon](pathfinder-bestiary-2/FRTpT9rLxUBKGdin.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.FRTpT9rLxUBKGdin]`|
|[Steam Mephit](pathfinder-bestiary-2/ARw4pyNmuQxoZDke.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ARw4pyNmuQxoZDke]`|
|[Stingray](pathfinder-bestiary-2/HXWT5QxmOCn0poMt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HXWT5QxmOCn0poMt]`|
|[Striding Fire](pathfinder-bestiary-2/RUdP7qWg6pnKrPnh.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.RUdP7qWg6pnKrPnh]`|
|[Stygira](pathfinder-bestiary-2/tZkiE8hnN5e9KIzi.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.tZkiE8hnN5e9KIzi]`|
|[Suli Dune Dancer](pathfinder-bestiary-2/3dlupMwDA9qv1xWS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.3dlupMwDA9qv1xWS]`|
|[Sunflower Leshy](pathfinder-bestiary-2/Bz5wgKEVsHAyaYEx.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Bz5wgKEVsHAyaYEx]`|
|[Sylph Sneak](pathfinder-bestiary-2/Ho1Dx2dXlQZYLApP.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Ho1Dx2dXlQZYLApP]`|
|[Taiga Giant](pathfinder-bestiary-2/W1OXnXGF22uybovO.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.W1OXnXGF22uybovO]`|
|[Taiga Linnorm](pathfinder-bestiary-2/NAy7eUqFI8tGm3Ug.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.NAy7eUqFI8tGm3Ug]`|
|[Tatzlwyrm](pathfinder-bestiary-2/UnWr8QSMq2VZUQhX.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.UnWr8QSMq2VZUQhX]`|
|[Tendriculos](pathfinder-bestiary-2/yBglC9GBy6p1x87t.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.yBglC9GBy6p1x87t]`|
|[Thanadaemon](pathfinder-bestiary-2/iW6WKMVV3Ug8sa2q.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.iW6WKMVV3Ug8sa2q]`|
|[Theletos](pathfinder-bestiary-2/OaKx1ZaU25i3cypS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.OaKx1ZaU25i3cypS]`|
|[Thrasfyr](pathfinder-bestiary-2/luPebMfNmwtOZcWR.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.luPebMfNmwtOZcWR]`|
|[Thulgant](pathfinder-bestiary-2/NtezcliwnH4R1tJw.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.NtezcliwnH4R1tJw]`|
|[Thunderbird](pathfinder-bestiary-2/fpBJiCOoHDlefgpO.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.fpBJiCOoHDlefgpO]`|
|[Tick Swarm](pathfinder-bestiary-2/jgMFqFDNUWsrnnBH.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.jgMFqFDNUWsrnnBH]`|
|[Titan Centipede](pathfinder-bestiary-2/n1jTYFRCaqF1nQrf.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.n1jTYFRCaqF1nQrf]`|
|[Totenmaske](pathfinder-bestiary-2/TKlnOKpYJkg1dNwb.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.TKlnOKpYJkg1dNwb]`|
|[Triton](pathfinder-bestiary-2/HjmqeJuWdmY5imiz.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.HjmqeJuWdmY5imiz]`|
|[Trollhound](pathfinder-bestiary-2/fwjDIGzmuEJN81Qq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.fwjDIGzmuEJN81Qq]`|
|[Trumpet Archon](pathfinder-bestiary-2/B4TJHP0yX8SXHQ4x.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.B4TJHP0yX8SXHQ4x]`|
|[Twigjack](pathfinder-bestiary-2/H8KNSMb9uo2mJF3P.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.H8KNSMb9uo2mJF3P]`|
|[Two-Headed Troll](pathfinder-bestiary-2/lUYlClIPITmhzepe.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.lUYlClIPITmhzepe]`|
|[Umonlee](pathfinder-bestiary-2/4Y8UO8NloCoPxJ1E.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.4Y8UO8NloCoPxJ1E]`|
|[Undine Hydromancer](pathfinder-bestiary-2/Z4eI4EhXBWDs8bl8.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Z4eI4EhXBWDs8bl8]`|
|[Urdefhan Tormentor](pathfinder-bestiary-2/FJr7Rc9MQEwxjAcz.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.FJr7Rc9MQEwxjAcz]`|
|[Urdefhan Warrior](pathfinder-bestiary-2/e5yE4RkwLQojRYHm.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.e5yE4RkwLQojRYHm]`|
|[Vampire Squid](pathfinder-bestiary-2/3MDlKPlPhHBo1DId.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.3MDlKPlPhHBo1DId]`|
|[Vampiric Mist](pathfinder-bestiary-2/NQ6qRvpD9DIwVxt3.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.NQ6qRvpD9DIwVxt3]`|
|[Vanth](pathfinder-bestiary-2/uiTUtcXixZ6TdKYE.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.uiTUtcXixZ6TdKYE]`|
|[Vaspercham](pathfinder-bestiary-2/gVNKimslRe1k2hdV.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.gVNKimslRe1k2hdV]`|
|[Veranallia](pathfinder-bestiary-2/ePUTKSr45H7S5dQs.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ePUTKSr45H7S5dQs]`|
|[Verdurous Ooze](pathfinder-bestiary-2/zXZrUTQSI0WxAsF5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.zXZrUTQSI0WxAsF5]`|
|[Vexgit](pathfinder-bestiary-2/BbIyNJDPkGKLK0Rt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.BbIyNJDPkGKLK0Rt]`|
|[Violet Fungus](pathfinder-bestiary-2/GsrqCDf0Dc9PisEt.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.GsrqCDf0Dc9PisEt]`|
|[Viper Vine](pathfinder-bestiary-2/duaHanGHvRGhUtB5.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.duaHanGHvRGhUtB5]`|
|[Void Zombie](pathfinder-bestiary-2/cKu0rrZtEAFCosej.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.cKu0rrZtEAFCosej]`|
|[Vrolikai](pathfinder-bestiary-2/Va2LUALLnnfWX3wq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Va2LUALLnnfWX3wq]`|
|[Vrykolakas Ancient](pathfinder-bestiary-2/e0PXAtnDnJo4ggFX.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.e0PXAtnDnJo4ggFX]`|
|[Vrykolakas Master](pathfinder-bestiary-2/j43O2vBu8eu2wC5z.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.j43O2vBu8eu2wC5z]`|
|[Vrykolakas Spawn](pathfinder-bestiary-2/8IyfadYYpYXBvlXN.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.8IyfadYYpYXBvlXN]`|
|[Water Orm](pathfinder-bestiary-2/64sdDzjLWu2W2R2e.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.64sdDzjLWu2W2R2e]`|
|[Water Yai](pathfinder-bestiary-2/R0dyG06kbNYiCqRW.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.R0dyG06kbNYiCqRW]`|
|[Wereboar](pathfinder-bestiary-2/vjEHKCgMunMHKl7r.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.vjEHKCgMunMHKl7r]`|
|[Weretiger](pathfinder-bestiary-2/SYaC46yrqcyp16Dq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.SYaC46yrqcyp16Dq]`|
|[Witchfire](pathfinder-bestiary-2/6xioRweZ0T4o4X9e.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6xioRweZ0T4o4X9e]`|
|[Witchwyrd](pathfinder-bestiary-2/CvO6MAoxDFO0J5qm.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.CvO6MAoxDFO0J5qm]`|
|[Wolverine](pathfinder-bestiary-2/6YOyTHRGqkYl2Prb.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6YOyTHRGqkYl2Prb]`|
|[Wood Giant](pathfinder-bestiary-2/5l0b80kz53Ie26gq.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.5l0b80kz53Ie26gq]`|
|[Wood Golem](pathfinder-bestiary-2/7UBCht1tQfeba2Ih.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.7UBCht1tQfeba2Ih]`|
|[Wooly Rhinoceros](pathfinder-bestiary-2/dBUiB3Hyh0hN8Exc.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.dBUiB3Hyh0hN8Exc]`|
|[Worm That Walks Cultist](pathfinder-bestiary-2/WjZCNuHNKfeeHCQS.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.WjZCNuHNKfeeHCQS]`|
|[Xill](pathfinder-bestiary-2/cd2IaNGpdmHHZnrN.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.cd2IaNGpdmHHZnrN]`|
|[Yamaraj](pathfinder-bestiary-2/ABrzFoOqQohQqU6C.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.ABrzFoOqQohQqU6C]`|
|[Yellow Musk Brute](pathfinder-bestiary-2/Kw4UUxt5rorsGjsK.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Kw4UUxt5rorsGjsK]`|
|[Yellow Musk Creeper](pathfinder-bestiary-2/5FGuXfg6Ub2hCBG7.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.5FGuXfg6Ub2hCBG7]`|
|[Yellow Musk Thrall](pathfinder-bestiary-2/0rhltfBkQtYkCwXd.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.0rhltfBkQtYkCwXd]`|
|[Yeth Hound](pathfinder-bestiary-2/6jqnHzjmJ190G8Zj.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.6jqnHzjmJ190G8Zj]`|
|[Young Brine Dragon](pathfinder-bestiary-2/scSsdEmG0jkEnyLV.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.scSsdEmG0jkEnyLV]`|
|[Young Cloud Dragon](pathfinder-bestiary-2/yr5xSfvSt7vA45wx.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.yr5xSfvSt7vA45wx]`|
|[Young Crystal Dragon](pathfinder-bestiary-2/mZLOTDHhChme6mfh.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.mZLOTDHhChme6mfh]`|
|[Young Magma Dragon](pathfinder-bestiary-2/49y0g521xHESClzk.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.49y0g521xHESClzk]`|
|[Young Umbral Dragon](pathfinder-bestiary-2/nOAQehZL3045YMX1.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.nOAQehZL3045YMX1]`|
|[Zebub](pathfinder-bestiary-2/wCsfRovcZMZxSdGm.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.wCsfRovcZMZxSdGm]`|
|[Zelekhut](pathfinder-bestiary-2/9sUx2sV4E7Sy3vJT.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.9sUx2sV4E7Sy3vJT]`|
|[Zomok](pathfinder-bestiary-2/Ge5Q5I7TTksf7QyN.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.Ge5Q5I7TTksf7QyN]`|
|[Zyss Serpentfolk](pathfinder-bestiary-2/s0wn50S5mGxagSAa.htm)||`@Compendium[pf2e.pathfinder-bestiary-2.s0wn50S5mGxagSAa]`|


## Bénédictions et malédictions

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Abadar - Major Boon](boons-and-curses/m33aOQij1BDcArN9.htm)|Abadar - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.m33aOQij1BDcArN9]`|
|[Abadar - Major Curse](boons-and-curses/OaUt41v2OrQkHpM4.htm)|Abadar - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.OaUt41v2OrQkHpM4]`|
|[Abadar - Minor Boon](boons-and-curses/fXEeTSVINdWaKHhw.htm)|Abadar - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.fXEeTSVINdWaKHhw]`|
|[Abadar - Minor Curse](boons-and-curses/NeAA1BHGGkRAmBDe.htm)|Abadar - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.NeAA1BHGGkRAmBDe]`|
|[Abadar - Moderate Boon](boons-and-curses/DfXlr5qqbcjJN3gh.htm)|Abadar - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.DfXlr5qqbcjJN3gh]`|
|[Abadar - Moderate Curse](boons-and-curses/n5p8QfZdptRZp0ii.htm)|Abadar - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.n5p8QfZdptRZp0ii]`|
|[Asmodeus - Major Boon](boons-and-curses/sy5UyOvxitqX244D.htm)|Asmodéus - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.sy5UyOvxitqX244D]`|
|[Asmodeus - Major Curse](boons-and-curses/yoXU4CqZtzgKXdlB.htm)|Asmodéus - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.yoXU4CqZtzgKXdlB]`|
|[Asmodeus - Minor Boon](boons-and-curses/fV0t8xagcbqwvTpQ.htm)|Asmodéus - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.fV0t8xagcbqwvTpQ]`|
|[Asmodeus - Minor Curse](boons-and-curses/dLudHCFcj4p7KG3j.htm)|Asmodéus - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.dLudHCFcj4p7KG3j]`|
|[Asmodeus - Moderate Boon](boons-and-curses/aV1I9VmMQBaFU9M9.htm)|Asmodéus - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.aV1I9VmMQBaFU9M9]`|
|[Asmodeus - Moderate Curse](boons-and-curses/y9DNpX7krUiOq0YI.htm)|Asmodéus - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.y9DNpX7krUiOq0YI]`|
|[Calistria - Major Boon](boons-and-curses/FeCcooyIb1JDQhd7.htm)|Calistria - Bénédiction Majeure|`@Compendium[pf2e.boons-and-curses.FeCcooyIb1JDQhd7]`|
|[Calistria - Major Curse](boons-and-curses/fV940VM5RCsNwUvA.htm)|Calistria - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.fV940VM5RCsNwUvA]`|
|[Calistria - Minor Boon](boons-and-curses/yWASKzumwmv4TgSe.htm)|Calistria - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.yWASKzumwmv4TgSe]`|
|[Calistria - Minor Curse](boons-and-curses/wIjqVRP0Hm2bIfyc.htm)|Calistria - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.wIjqVRP0Hm2bIfyc]`|
|[Calistria - Moderate Boon](boons-and-curses/Ks1HaYEuSRybaq8U.htm)|Calistria - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.Ks1HaYEuSRybaq8U]`|
|[Calistria - Moderate Curse](boons-and-curses/nqOoUH4r8NEi1i2g.htm)|Calistria - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.nqOoUH4r8NEi1i2g]`|
|[Cayden Cailean - Major Boon](boons-and-curses/ZPlvqkjEv9a7J76T.htm)|Cayden Cailean - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.ZPlvqkjEv9a7J76T]`|
|[Cayden Cailean - Major Curse](boons-and-curses/QGUFWuz9uF49EIhy.htm)|Cayden cailean - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.QGUFWuz9uF49EIhy]`|
|[Cayden Cailean - Minor Boon](boons-and-curses/La6t4fVId26PkmYk.htm)|Cayden Caylean - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.La6t4fVId26PkmYk]`|
|[Cayden Cailean - Minor Curse](boons-and-curses/fLBNXULLPalAKlYe.htm)|Cayden Cailean - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.fLBNXULLPalAKlYe]`|
|[Cayden Cailean - Moderate Boon](boons-and-curses/fsKVskUhWwMgvUaT.htm)|Cayden Cailean - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.fsKVskUhWwMgvUaT]`|
|[Cayden Cailean - Moderate Curse](boons-and-curses/x1QrKuvZ2TbTvLUY.htm)|Cayden Cailean - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.x1QrKuvZ2TbTvLUY]`|
|[Desna - Major Boon](boons-and-curses/mcVlGufQ8rQRe61Y.htm)|Desna - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.mcVlGufQ8rQRe61Y]`|
|[Desna - Major Curse](boons-and-curses/z4GfG3GatAOELKyA.htm)|Desna - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.z4GfG3GatAOELKyA]`|
|[Desna - Minor Boon](boons-and-curses/LOlv11WEGUQWYKna.htm)|Desna - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.LOlv11WEGUQWYKna]`|
|[Desna - Minor Curse](boons-and-curses/Up7nUIT42zwhgZf4.htm)|Desna - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.Up7nUIT42zwhgZf4]`|
|[Desna - Moderate Boon](boons-and-curses/4m2dQ2fvVWxtokVe.htm)|Desna - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.4m2dQ2fvVWxtokVe]`|
|[Desna - Moderate Curse](boons-and-curses/CjRUOscsOwCmai8D.htm)|Desna - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.CjRUOscsOwCmai8D]`|
|[Erastil - Major Boon](boons-and-curses/nFrCY6tT2B8uxaO3.htm)|Érastil - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.nFrCY6tT2B8uxaO3]`|
|[Erastil - Major Curse](boons-and-curses/C6Ercv1ugcTsbVA9.htm)|Érastil - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.C6Ercv1ugcTsbVA9]`|
|[Erastil - Minor Boon](boons-and-curses/hrTl9kfSNrOQeNze.htm)|Érastil - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.hrTl9kfSNrOQeNze]`|
|[Erastil - Minor Curse](boons-and-curses/pwmFrNkLscXrDSPN.htm)|Érastil - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.pwmFrNkLscXrDSPN]`|
|[Erastil - Moderate Boon](boons-and-curses/CL8lLQJWd4N89QIm.htm)|Érastil - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.CL8lLQJWd4N89QIm]`|
|[Erastil - Moderate Curse](boons-and-curses/3u3vav6qfAW6hPKE.htm)|Érastil - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.3u3vav6qfAW6hPKE]`|
|[Gorum - Major Boon](boons-and-curses/8G0USrxM7d4B4EVI.htm)|Gorum - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.8G0USrxM7d4B4EVI]`|
|[Gorum - Major Curse](boons-and-curses/M78A7uQaa4Ig8pGU.htm)|Gorum - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.M78A7uQaa4Ig8pGU]`|
|[Gorum - Minor Boon](boons-and-curses/HrV31rAkNjV4KfCU.htm)|Gorum - Bénédiction Mineure|`@Compendium[pf2e.boons-and-curses.HrV31rAkNjV4KfCU]`|
|[Gorum - Minor Curse](boons-and-curses/NgwW7Wt2gO9SEwPX.htm)|Gorum - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.NgwW7Wt2gO9SEwPX]`|
|[Gorum - Moderate Boon](boons-and-curses/kstqMWge32n0Lfdz.htm)|Gorum - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.kstqMWge32n0Lfdz]`|
|[Gorum - Moderate Curse](boons-and-curses/lR9KS3pQ4gGKNspP.htm)|Gorum - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.lR9KS3pQ4gGKNspP]`|
|[Gozreh - Major Boon](boons-and-curses/vfQ7TsScMBTNiznn.htm)|Gozreh - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.vfQ7TsScMBTNiznn]`|
|[Gozreh - Major Curse](boons-and-curses/kEAw5PMLdX2gHlwR.htm)|Gozreh - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.kEAw5PMLdX2gHlwR]`|
|[Gozreh - Minor Boon](boons-and-curses/uVZrqxtOOutr4Ss9.htm)|Gozreh - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.uVZrqxtOOutr4Ss9]`|
|[Gozreh - Minor Curse](boons-and-curses/XYM7v72doPR3LwgA.htm)|Gozreh - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.XYM7v72doPR3LwgA]`|
|[Gozreh - Moderate Boon](boons-and-curses/AvjRCVzLmxapFOLV.htm)|Gozreh - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.AvjRCVzLmxapFOLV]`|
|[Gozreh - Moderate Curse](boons-and-curses/AohWtOzgk2Qr9ADl.htm)|Gozreh - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.AohWtOzgk2Qr9ADl]`|
|[Iomedae - Major Boon](boons-and-curses/dHcGihuimtCf1NOs.htm)|Iomédae - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.dHcGihuimtCf1NOs]`|
|[Iomedae - Major Curse](boons-and-curses/zWshZRICuzP7DfFV.htm)|Iomedae - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.zWshZRICuzP7DfFV]`|
|[Iomedae - Minor Boon](boons-and-curses/dTJP2Hg4ZJu4Ck2y.htm)|Iomedae - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.dTJP2Hg4ZJu4Ck2y]`|
|[Iomedae - Minor Curse](boons-and-curses/IOaepssdwTanFooc.htm)|Iomedae - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.IOaepssdwTanFooc]`|
|[Iomedae - Moderate Boon](boons-and-curses/qTmjEC528YnvPgXE.htm)|Iomedae - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.qTmjEC528YnvPgXE]`|
|[Iomedae - Moderate Curse](boons-and-curses/rA7ZM6WGnJNrWTmo.htm)|Iomedae - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.rA7ZM6WGnJNrWTmo]`|
|[Irori - Major Boon](boons-and-curses/kCCrjskDIbG0e74O.htm)|Irori - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.kCCrjskDIbG0e74O]`|
|[Irori - Major Curse](boons-and-curses/wEqPbkQMuqlL2TPn.htm)|Irori - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.wEqPbkQMuqlL2TPn]`|
|[Irori - Minor Boon](boons-and-curses/fV0Xa1Bd3BoWACgT.htm)|Irori - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.fV0Xa1Bd3BoWACgT]`|
|[Irori - Minor Curse](boons-and-curses/DSfHCm8PDL5hLzZ0.htm)|Irori - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.DSfHCm8PDL5hLzZ0]`|
|[Irori - Moderate Boon](boons-and-curses/xrczhivVmi7MrEfz.htm)|Irori - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.xrczhivVmi7MrEfz]`|
|[Irori - Moderate Curse](boons-and-curses/qhVfbZCxZJS8NmB3.htm)|Irori - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.qhVfbZCxZJS8NmB3]`|
|[Lamashtu - Major Boon](boons-and-curses/XFiI9qC4MNX4WUdh.htm)|Lamashtu - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.XFiI9qC4MNX4WUdh]`|
|[Lamashtu - Major Curse](boons-and-curses/py9v5HfCIzCoyC0C.htm)|Lamashtu - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.py9v5HfCIzCoyC0C]`|
|[Lamashtu - Minor Boon](boons-and-curses/6f8zTNIs5XXzkhkR.htm)|Lamashtu - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.6f8zTNIs5XXzkhkR]`|
|[Lamashtu - Minor Curse](boons-and-curses/5x0wEbgxYmXonBuN.htm)|Lamashtu - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.5x0wEbgxYmXonBuN]`|
|[Lamashtu - Moderate Boon](boons-and-curses/tFnPBYcAZ0X3GbI5.htm)|Lamashtu - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.tFnPBYcAZ0X3GbI5]`|
|[Lamashtu - Moderate Curse](boons-and-curses/gDBa72Y2jokr8Zzg.htm)|Lamashtu - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.gDBa72Y2jokr8Zzg]`|
|[Nethys - Major Boon](boons-and-curses/8ZNtiMxoJIP1DJ9Q.htm)|Néthys - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.8ZNtiMxoJIP1DJ9Q]`|
|[Nethys - Major Curse](boons-and-curses/4sZ9tpmPj4LIgPvU.htm)|Néthys - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.4sZ9tpmPj4LIgPvU]`|
|[Nethys - Minor Boon](boons-and-curses/jVeuBZsa4iFg0wC6.htm)|Néthys - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.jVeuBZsa4iFg0wC6]`|
|[Nethys - Minor Curse](boons-and-curses/KjPTFfxMAqCngQFB.htm)|Néthys - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.KjPTFfxMAqCngQFB]`|
|[Nethys - Moderate Boon](boons-and-curses/HdPVL76QiDhsLCA9.htm)|Néthys - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.HdPVL76QiDhsLCA9]`|
|[Nethys - Moderate Curse](boons-and-curses/CmHQGVyNZ7aOmMcd.htm)|Néthys - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.CmHQGVyNZ7aOmMcd]`|
|[Norgorber - Major Boon](boons-and-curses/lOIu9jYDAR2rDe4p.htm)|Norgorber - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.lOIu9jYDAR2rDe4p]`|
|[Norgorber - Major Curse](boons-and-curses/wREsKC8CTifEyj6v.htm)|Norgorber - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.wREsKC8CTifEyj6v]`|
|[Norgorber - Minor Boon](boons-and-curses/2EKgNifq3ozzKYfI.htm)|Norgorber - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.2EKgNifq3ozzKYfI]`|
|[Norgorber - Minor Curse](boons-and-curses/XFd7rnb9cchNw0WN.htm)|Norgorber - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.XFd7rnb9cchNw0WN]`|
|[Norgorber - Moderate Boon](boons-and-curses/hUSknn3LYmT7eKzT.htm)|Norgorber - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.hUSknn3LYmT7eKzT]`|
|[Norgorber - Moderate Curse](boons-and-curses/0PMqydlsIjj8GNnl.htm)|Norgorber - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.0PMqydlsIjj8GNnl]`|
|[Pharasma - Major Boon](boons-and-curses/U7ZJfuPLQPyoaj4M.htm)|Pharasma - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.U7ZJfuPLQPyoaj4M]`|
|[Pharasma - Major Curse](boons-and-curses/DnlAmlrrKv3jwaOq.htm)|Pharasma - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.DnlAmlrrKv3jwaOq]`|
|[Pharasma - Minor Boon](boons-and-curses/0vmwZrIhm5rWHkYh.htm)|Pharasma - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.0vmwZrIhm5rWHkYh]`|
|[Pharasma - Minor Curse](boons-and-curses/ParIWsb6B4fdhrHF.htm)|Pharasma - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.ParIWsb6B4fdhrHF]`|
|[Pharasma - Moderate Boon](boons-and-curses/xrSgGobm5QtTcABQ.htm)|Pharasma - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.xrSgGobm5QtTcABQ]`|
|[Pharasma - Moderate Curse](boons-and-curses/1hdWNyedYvuZgtPr.htm)|Pharasma - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.1hdWNyedYvuZgtPr]`|
|[Rovagug - Major Boon](boons-and-curses/cFz09CWKzff2cC73.htm)|Rovagug - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.cFz09CWKzff2cC73]`|
|[Rovagug - Major Curse](boons-and-curses/PoHos7qriDzQN7Gw.htm)|Rovagug - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.PoHos7qriDzQN7Gw]`|
|[Rovagug - Minor Boon](boons-and-curses/4FkVB4uQK4eHZJ6Z.htm)|Rovagug - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.4FkVB4uQK4eHZJ6Z]`|
|[Rovagug - Minor Curse](boons-and-curses/1nALC8yWtjBDEaOC.htm)|Rovagug - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.1nALC8yWtjBDEaOC]`|
|[Rovagug - Moderate Boon](boons-and-curses/a1OZtMQSzjYOm0P3.htm)|Rovagug - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.a1OZtMQSzjYOm0P3]`|
|[Rovagug - Moderate Curse](boons-and-curses/d198p8UBPK3VcKi9.htm)|Rovagug - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.d198p8UBPK3VcKi9]`|
|[Saranrae - Major Boon](boons-and-curses/dL6r0VfFIWlAazDW.htm)|Sarenrae - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.dL6r0VfFIWlAazDW]`|
|[Saranrae - Major Curse](boons-and-curses/DuGA57MDiwEKp7Y1.htm)|Sarenrae - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.DuGA57MDiwEKp7Y1]`|
|[Saranrae - Minor Boon](boons-and-curses/e9bdt8UH7ZvpRrLz.htm)|Sarenrae - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.e9bdt8UH7ZvpRrLz]`|
|[Saranrae - Minor Curse](boons-and-curses/VZodqtMF3qrMl8a7.htm)|Sarenrae - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.VZodqtMF3qrMl8a7]`|
|[Saranrae - Moderate Boon](boons-and-curses/S1qGyGG25YImRhmS.htm)|Sarenrae - Bénédicition modérée|`@Compendium[pf2e.boons-and-curses.S1qGyGG25YImRhmS]`|
|[Saranrae - Moderate Curse](boons-and-curses/V7FYIgphCCkYsEXF.htm)|Sarenrae - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.V7FYIgphCCkYsEXF]`|
|[Shelyn - Major Boon](boons-and-curses/l7n8n4fAoRLIn95W.htm)|Shélyn - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.l7n8n4fAoRLIn95W]`|
|[Shelyn - Major Curse](boons-and-curses/ILtU0TAUZcOcwkkG.htm)|Shélyn - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.ILtU0TAUZcOcwkkG]`|
|[Shelyn - Minor Boon](boons-and-curses/tilqIbJZLqeknTYo.htm)|Shélyn - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.tilqIbJZLqeknTYo]`|
|[Shelyn - Minor Curse](boons-and-curses/jhixjUJZlCetNRjH.htm)|Shélyn - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.jhixjUJZlCetNRjH]`|
|[Shelyn - Moderate Boon](boons-and-curses/zoJjzir97Ie7tbp0.htm)|Shélyn - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.zoJjzir97Ie7tbp0]`|
|[Shelyn - Moderate Curse](boons-and-curses/vU5leGDNAZSfPgTz.htm)|Shélyn - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.vU5leGDNAZSfPgTz]`|
|[Torag - Major Boon](boons-and-curses/AdNAeWhnq5Is3AZb.htm)|Torag - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.AdNAeWhnq5Is3AZb]`|
|[Torag - Major Curse](boons-and-curses/c2wwILmpgyXIjlfe.htm)|Torag - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.c2wwILmpgyXIjlfe]`|
|[Torag - Minor Boon](boons-and-curses/e7Y4jKHQ3ptbSuxc.htm)|Torag - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.e7Y4jKHQ3ptbSuxc]`|
|[Torag - Minor Curse](boons-and-curses/845vvISfgkk6sei0.htm)|Torag - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.845vvISfgkk6sei0]`|
|[Torag - Moderate Boon](boons-and-curses/30Xdyvplx7MfX1nA.htm)|Torag - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.30Xdyvplx7MfX1nA]`|
|[Torag - Moderate Curse](boons-and-curses/AfuH02H6ib4KYS6C.htm)|Torag - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.AfuH02H6ib4KYS6C]`|
|[Urgathoa - Major Boon](boons-and-curses/dZU3HdI2oO8LFjGq.htm)|Urgathoa - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.dZU3HdI2oO8LFjGq]`|
|[Urgathoa - Major Curse](boons-and-curses/eVRjDKt0N0qSmpCn.htm)|Ugathoa - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.eVRjDKt0N0qSmpCn]`|
|[Urgathoa - Minor Boon](boons-and-curses/jfWeNqYj3rn9EysM.htm)|Urgathoa - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.jfWeNqYj3rn9EysM]`|
|[Urgathoa - Minor Curse](boons-and-curses/XfMZ83VA7dtF7fL5.htm)|Urgathoa - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.XfMZ83VA7dtF7fL5]`|
|[Urgathoa - Moderate Boon](boons-and-curses/N29LCwYfRWHtEqrS.htm)|Urgathoa - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.N29LCwYfRWHtEqrS]`|
|[Urgathoa - Moderate Curse](boons-and-curses/9EMaYf6odFEyjdSr.htm)|Urgathoa - Malédiction Modérée|`@Compendium[pf2e.boons-and-curses.9EMaYf6odFEyjdSr]`|
|[Zon-Kuthon - Major Boon](boons-and-curses/eibN2Uf0dsHCU5rE.htm)|Zon-Kuthon - Bénédiction majeure|`@Compendium[pf2e.boons-and-curses.eibN2Uf0dsHCU5rE]`|
|[Zon-Kuthon - Major Curse](boons-and-curses/SaiJX9KBN3lC0RUy.htm)|Zon-Kuthon - Malédiction majeure|`@Compendium[pf2e.boons-and-curses.SaiJX9KBN3lC0RUy]`|
|[Zon-Kuthon - Minor Boon](boons-and-curses/wlBiDpWvjKiw9k2z.htm)|Zon-Kuthon - Bénédiction mineure|`@Compendium[pf2e.boons-and-curses.wlBiDpWvjKiw9k2z]`|
|[Zon-Kuthon - Minor Curse](boons-and-curses/lI7bDExxA2zQq1sP.htm)|Zon-Kuthon - Malédiction mineure|`@Compendium[pf2e.boons-and-curses.lI7bDExxA2zQq1sP]`|
|[Zon-Kuthon - Moderate Boon](boons-and-curses/QVEkEdxCnoGxMo9l.htm)|Zon Kuthon - Bénédiction modérée|`@Compendium[pf2e.boons-and-curses.QVEkEdxCnoGxMo9l]`|
|[Zon-Kuthon - Moderate Curse](boons-and-curses/aOVXhHVkLw9dLCdR.htm)|Zon-Kuthon - Malédiction modérée|`@Compendium[pf2e.boons-and-curses.aOVXhHVkLw9dLCdR]`|


## Capacités de classe

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Adamantine Strikes](classfeatures/monk-17-5cthRUkRqRtduVvN.htm)|Frappes d'adamantium|`@Compendium[pf2e.classfeatures.5cthRUkRqRtduVvN]`|
|[Advanced Alchemy](classfeatures/alchemist-01-Pe0zmIqyTBc2Td0I.htm)|Alchimie avancée|`@Compendium[pf2e.classfeatures.Pe0zmIqyTBc2Td0I]`|
|[Alchemical Alacrity](classfeatures/alchemist-15-Eood6pNPaJxuSgD1.htm)|Alacrité alchimique|`@Compendium[pf2e.classfeatures.Eood6pNPaJxuSgD1]`|
|[Alchemical Expertise](classfeatures/alchemist-09-3e1PlMXmlSwKoc6d.htm)|Expertise alchimique|`@Compendium[pf2e.classfeatures.3e1PlMXmlSwKoc6d]`|
|[Alchemical Mastery](classfeatures/alchemist-17-eG7FBDjCdEFzW9V9.htm)|Maîtrise alchimique|`@Compendium[pf2e.classfeatures.eG7FBDjCdEFzW9V9]`|
|[Alchemical Sciences Methodology](classfeatures/investigator-01-ln2Y1a4SxlU9sizX.htm)|Méthodologie : Sciences alchimiques|`@Compendium[pf2e.classfeatures.ln2Y1a4SxlU9sizX]`|
|[Alchemical Weapon Expertise](classfeatures/alchemist-07-4ocPy4O0OCLY0XCM.htm)|Expertise avec les armes alchimiques|`@Compendium[pf2e.classfeatures.4ocPy4O0OCLY0XCM]`|
|[Alertness (Level 11)](classfeatures/wizard-11-OZaJz4exCoz6vuuv.htm)|Vigilance (Niveau 11)|`@Compendium[pf2e.classfeatures.OZaJz4exCoz6vuuv]`|
|[Alertness (Level 3)](classfeatures/druid-03-D8CSi8c9XiRpVc5M.htm)|Vigilance (Niveau 3)|`@Compendium[pf2e.classfeatures.D8CSi8c9XiRpVc5M]`|
|[Alertness (Level 5)](classfeatures/monk-05-2o1Cj7hDayDlslqY.htm)|Vigilance (Niveau 5)|`@Compendium[pf2e.classfeatures.2o1Cj7hDayDlslqY]`|
|[Alertness (Level 9)](classfeatures/alchemist-09-TAIOtk5VvPZvv4nu.htm)|Vigilance (Niveau 9)|`@Compendium[pf2e.classfeatures.TAIOtk5VvPZvv4nu]`|
|[Anathema (Cleric)](classfeatures/cleric-01-UV1HlClbWCNcaKBZ.htm)|Anathème (Prêtre)|`@Compendium[pf2e.classfeatures.UV1HlClbWCNcaKBZ]`|
|[Anathema (Druid)](classfeatures/druid-01-nfBn8QB6HVdzpTFV.htm)|Anathème (Druide)|`@Compendium[pf2e.classfeatures.nfBn8QB6HVdzpTFV]`|
|[Ancestors Mystery](classfeatures/oracle-01-qvRlih3u7vK3FYUR.htm)|Mystère des ancètres|`@Compendium[pf2e.classfeatures.qvRlih3u7vK3FYUR]`|
|[Animal Instinct](classfeatures/barbarian-01-0FtzFbUrN56KA67z.htm)|Instinct animal|`@Compendium[pf2e.classfeatures.0FtzFbUrN56KA67z]`|
|[Animal Order](classfeatures/druid-01-POBvoXifa9HaejAg.htm)|Animal|`@Compendium[pf2e.classfeatures.POBvoXifa9HaejAg]`|
|[Antipaladin](classfeatures/champion-01-EQ6DVIQHAUXUhY6Y.htm)|Antipaladin [Chaotique Mauvais]|`@Compendium[pf2e.classfeatures.EQ6DVIQHAUXUhY6Y]`|
|[Ape Animal Instinct](classfeatures/barbarian-01-vlRvOQS1HZZqSyh7.htm)|Instinct animal - Primate|`@Compendium[pf2e.classfeatures.vlRvOQS1HZZqSyh7]`|
|[Arcane Bond](classfeatures/wizard-01-au0lwQ1nAcNQwcGh.htm)|Lien arcanique|`@Compendium[pf2e.classfeatures.au0lwQ1nAcNQwcGh]`|
|[Arcane School](classfeatures/wizard-01-7nbKDBGvwSx9T27G.htm)|École arcanique|`@Compendium[pf2e.classfeatures.7nbKDBGvwSx9T27G]`|
|[Arcane School: Abjuration](classfeatures/wizard-01-gCwcys8CnS102tji.htm)|École arcanique : Abjuration|`@Compendium[pf2e.classfeatures.gCwcys8CnS102tji]`|
|[Arcane School: Conjuration](classfeatures/wizard-01-SNeVaUBTHwvoO6kr.htm)|École arcanique : Invocation|`@Compendium[pf2e.classfeatures.SNeVaUBTHwvoO6kr]`|
|[Arcane School: Divination](classfeatures/wizard-01-yobGgrHdgjs5QW5o.htm)|École arcanique : Divination|`@Compendium[pf2e.classfeatures.yobGgrHdgjs5QW5o]`|
|[Arcane School: Enchantment](classfeatures/wizard-01-ZHwGACWQEy6kTzcP.htm)|École arcanique : Enchantement|`@Compendium[pf2e.classfeatures.ZHwGACWQEy6kTzcP]`|
|[Arcane School: Evocation](classfeatures/wizard-01-ibhml5y20g5M3Vgd.htm)|École arcanique : Évocation|`@Compendium[pf2e.classfeatures.ibhml5y20g5M3Vgd]`|
|[Arcane School: Illusion](classfeatures/wizard-01-K6hG7nH8yjmbA0Q9.htm)|École arcanique : Illusion|`@Compendium[pf2e.classfeatures.K6hG7nH8yjmbA0Q9]`|
|[Arcane School: Necromancy](classfeatures/wizard-01-uNM7qZQokRKAEd7k.htm)|École arcanique : Nécromancie|`@Compendium[pf2e.classfeatures.uNM7qZQokRKAEd7k]`|
|[Arcane School: Transmutation](classfeatures/wizard-01-rHxkPijLnQ9O9AGV.htm)|École arcanique : Transmutation|`@Compendium[pf2e.classfeatures.rHxkPijLnQ9O9AGV]`|
|[Arcane School: Universalist](classfeatures/wizard-01-qczCKdg47eAmCOUD.htm)|École arcanique : universaliste|`@Compendium[pf2e.classfeatures.qczCKdg47eAmCOUD]`|
|[Arcane Spellcasting](classfeatures/wizard-01-S6WW4Yyg4XonXGHD.htm)|Incantation arcanique|`@Compendium[pf2e.classfeatures.S6WW4Yyg4XonXGHD]`|
|[Arcane Thesis](classfeatures/wizard-01-M89l9FOnjHe63wD7.htm)|Thèse arcanique|`@Compendium[pf2e.classfeatures.M89l9FOnjHe63wD7]`|
|[Arcane Thesis: Improved Familiar Attunement](classfeatures/wizard-01-SNZ46g3u7U6x0XJj.htm)|Thèse arcanique : Amélioration de l’harmonisation avec le familier|`@Compendium[pf2e.classfeatures.SNZ46g3u7U6x0XJj]`|
|[Arcane Thesis: Metamagical Experimentation](classfeatures/wizard-01-89zWKD2CN7nRu2xp.htm)|Thèse arcanique : Expérimentation métamagique|`@Compendium[pf2e.classfeatures.89zWKD2CN7nRu2xp]`|
|[Arcane Thesis: Spell Blending](classfeatures/wizard-01-OAcxS625AXSGrQIC.htm)|Thèse arcanique : Mélange de sorts|`@Compendium[pf2e.classfeatures.OAcxS625AXSGrQIC]`|
|[Arcane Thesis: Spell Substitution](classfeatures/wizard-01-QzWXMCSGNfvvpYgF.htm)|Thèse arcanique : Substitution de sort|`@Compendium[pf2e.classfeatures.QzWXMCSGNfvvpYgF]`|
|[Arcane Thesis: Staff Nexus](classfeatures/wizard-01-Klb35AwlkNrq1gpB.htm)|Thèse arcanique : Bâton nexus|`@Compendium[pf2e.classfeatures.Klb35AwlkNrq1gpB]`|
|[Archwizard's Spellcraft](classfeatures/wizard-19-ZjwJHmjPrSs6VDez.htm)|Art magique de l'archimage|`@Compendium[pf2e.classfeatures.ZjwJHmjPrSs6VDez]`|
|[Armor Expertise (Level 11)](classfeatures/fighter-11-fRifyINZF5SKDfib.htm)|Expertise avec les armures (Niveau 11)|`@Compendium[pf2e.classfeatures.fRifyINZF5SKDfib]`|
|[Armor Expertise (Level 7)](classfeatures/champion-07-x5jaCJxsmD5sx3KB.htm)|Expertise avec les armures (Niveau 7)|`@Compendium[pf2e.classfeatures.x5jaCJxsmD5sx3KB]`|
|[Armor Mastery (Level 13)](classfeatures/champion-13-CGB1TczFhQhdQxml.htm)|Maîtrise d'armure (Niveau 13)|`@Compendium[pf2e.classfeatures.CGB1TczFhQhdQxml]`|
|[Armor Mastery (Level 17)](classfeatures/fighter-17-IPDwS5pTgU3Cq6Nl.htm)|Maîtrise des armures (Niveau 17)|`@Compendium[pf2e.classfeatures.IPDwS5pTgU3Cq6Nl]`|
|[Armor of Fury](classfeatures/barbarian-19-QTCIahokREpnAYDi.htm)|Armure de furie|`@Compendium[pf2e.classfeatures.QTCIahokREpnAYDi]`|
|[Attack of Opportunity](classfeatures/fighter-01-hmShTfPOcTaKgbf4.htm)|Attaque d’opportunité|`@Compendium[pf2e.classfeatures.hmShTfPOcTaKgbf4]`|
|[Baba Yaga Patron](classfeatures/witch-01-VVMMJdIWL7fAsQf3.htm)|Patron : Baba Yaga|`@Compendium[pf2e.classfeatures.VVMMJdIWL7fAsQf3]`|
|[Bard Weapon Expertise](classfeatures/bard-11-4lp8oG9A3zuqhPBS.htm)|expertise avec les armes du barde|`@Compendium[pf2e.classfeatures.4lp8oG9A3zuqhPBS]`|
|[Battle Mystery](classfeatures/oracle-01-gjOGOR30Czpnx3tM.htm)|Mystère du combat|`@Compendium[pf2e.classfeatures.gjOGOR30Czpnx3tM]`|
|[Battledancer Style](classfeatures/swashbuckler-01-5HoEwzLDJGTCZtFa.htm)|Style : Danseur de combat|`@Compendium[pf2e.classfeatures.5HoEwzLDJGTCZtFa]`|
|[Battlefield Surveyor](classfeatures/fighter-07-TIvzBALymvb56L79.htm)|Sentinelle au front|`@Compendium[pf2e.classfeatures.TIvzBALymvb56L79]`|
|[Bear Animal Instinct](classfeatures/barbarian-01-uGY2yddm8mZx8Yo2.htm)|Instinct animal - Ours|`@Compendium[pf2e.classfeatures.uGY2yddm8mZx8Yo2]`|
|[Black Dragon Instinct](classfeatures/barbarian-01-VNbDNiWjARtGQQAs.htm)|Instinct du dragon - Noir|`@Compendium[pf2e.classfeatures.VNbDNiWjARtGQQAs]`|
|[Bloodline](classfeatures/sorcerer-01-2goYo6VNbwC6aKF1.htm)|Lignages|`@Compendium[pf2e.classfeatures.2goYo6VNbwC6aKF1]`|
|[Bloodline Paragon](classfeatures/sorcerer-19-feCnVrPPlKhl701x.htm)|Parangon du lignage|`@Compendium[pf2e.classfeatures.feCnVrPPlKhl701x]`|
|[Bloodline Spells](classfeatures/sorcerer-01-H6ziAPvCipTPG8SH.htm)|Sorts de lignage|`@Compendium[pf2e.classfeatures.H6ziAPvCipTPG8SH]`|
|[Bloodline: Aberrant](classfeatures/sorcerer-01-3qqvnC2U8W26yae7.htm)|Lignage : Aberrant|`@Compendium[pf2e.classfeatures.3qqvnC2U8W26yae7]`|
|[Bloodline: Angelic](classfeatures/sorcerer-01-vhW3glAaEfq2DKrw.htm)|Lignage : Angélique|`@Compendium[pf2e.classfeatures.vhW3glAaEfq2DKrw]`|
|[Bloodline: Demonic](classfeatures/sorcerer-01-w5koctOVrEcpxTIq.htm)|Lignage : Démoniaque|`@Compendium[pf2e.classfeatures.w5koctOVrEcpxTIq]`|
|[Bloodline: Diabolic](classfeatures/sorcerer-01-o39zQMIdERWtmBSB.htm)|Lignage : Diabolique|`@Compendium[pf2e.classfeatures.o39zQMIdERWtmBSB]`|
|[Bloodline: Draconic](classfeatures/sorcerer-01-ZHabYxSgYK0XbjhM.htm)|Lignage : Draconique|`@Compendium[pf2e.classfeatures.ZHabYxSgYK0XbjhM]`|
|[Bloodline: Elemental](classfeatures/sorcerer-01-RXRnJcG4XSabZ35a.htm)|Lignage : Élémentaire|`@Compendium[pf2e.classfeatures.RXRnJcG4XSabZ35a]`|
|[Bloodline: Fey](classfeatures/sorcerer-01-eW3cfCH7Wpx2vik2.htm)|Lignage : Féerique|`@Compendium[pf2e.classfeatures.eW3cfCH7Wpx2vik2]`|
|[Bloodline: Genie](classfeatures/sorcerer-01-tYOMBiH3HbViNWwn.htm)|Lignage : Génie|`@Compendium[pf2e.classfeatures.tYOMBiH3HbViNWwn]`|
|[Bloodline: Hag](classfeatures/sorcerer-01-O0uXZRWMNliDbkxU.htm)|Lignage : Guenaude|`@Compendium[pf2e.classfeatures.O0uXZRWMNliDbkxU]`|
|[Bloodline: Imperial](classfeatures/sorcerer-01-ZEtJJ5UOlV5oTWWp.htm)|Lignage : Impérial|`@Compendium[pf2e.classfeatures.ZEtJJ5UOlV5oTWWp]`|
|[Bloodline: Nymph](classfeatures/sorcerer-01-5Wxjghw7lHuCxjZz.htm)|Lignage : Nymphe|`@Compendium[pf2e.classfeatures.5Wxjghw7lHuCxjZz]`|
|[Bloodline: Psychopomp](classfeatures/sorcerer-01-PpzH9tJULk5ksX9w.htm)|Lignage : Psychopompe|`@Compendium[pf2e.classfeatures.PpzH9tJULk5ksX9w]`|
|[Bloodline: Shadow](classfeatures/sorcerer-01-uoQOm41BVdSo6pAS.htm)|Lignage : Ombre|`@Compendium[pf2e.classfeatures.uoQOm41BVdSo6pAS]`|
|[Bloodline: Undead](classfeatures/sorcerer-01-7WBZ2kkhZ7JorWu2.htm)|Lignage : Mort-vivant|`@Compendium[pf2e.classfeatures.7WBZ2kkhZ7JorWu2]`|
|[Blue Dragon Instinct](classfeatures/barbarian-01-RiOww9KMu06D7wtW.htm)|Instinct du dragon - Bleu|`@Compendium[pf2e.classfeatures.RiOww9KMu06D7wtW]`|
|[Bomber](classfeatures/alchemist-01-7JbiaZ8bxODM5mzS.htm)|Artificier|`@Compendium[pf2e.classfeatures.7JbiaZ8bxODM5mzS]`|
|[Bones Mystery](classfeatures/oracle-01-IaxmCkdsPlA52spu.htm)|Mystère des ossements|`@Compendium[pf2e.classfeatures.IaxmCkdsPlA52spu]`|
|[Braggart Style](classfeatures/swashbuckler-01-KBhwFjdptrKyN5EM.htm)|Style : fanfaron|`@Compendium[pf2e.classfeatures.KBhwFjdptrKyN5EM]`|
|[Brass Dragon Instinct](classfeatures/barbarian-01-b5rvKZQCfpgBenKJ.htm)|Instinct du dragon - Airain|`@Compendium[pf2e.classfeatures.b5rvKZQCfpgBenKJ]`|
|[Bravery](classfeatures/fighter-03-GJKJafDGuX4BeAeN.htm)|Bravoure|`@Compendium[pf2e.classfeatures.GJKJafDGuX4BeAeN]`|
|[Bronze Dragon Instinct](classfeatures/barbarian-01-kdzIxHpzeRbdRqQA.htm)|Instinct du dragon - Bronze|`@Compendium[pf2e.classfeatures.kdzIxHpzeRbdRqQA]`|
|[Brutality](classfeatures/barbarian-05-EEUTd0jAyfwTLzjk.htm)|Brutalité|`@Compendium[pf2e.classfeatures.EEUTd0jAyfwTLzjk]`|
|[Bull Animal Instinct](classfeatures/barbarian-01-31sPXwmEbbcvgsM9.htm)|Instinct animal - Taureau|`@Compendium[pf2e.classfeatures.31sPXwmEbbcvgsM9]`|
|[Cat Animal Instinct](classfeatures/barbarian-01-vCNtX2LwlemhA3tu.htm)|Instinct animal - Chat|`@Compendium[pf2e.classfeatures.vCNtX2LwlemhA3tu]`|
|[Champion Code](classfeatures/champion-01-fykh5pE99O3I2sOI.htm)|Code du champion|`@Compendium[pf2e.classfeatures.fykh5pE99O3I2sOI]`|
|[Champion Expertise](classfeatures/champion-09-VgmfNKtQLgBaNi5r.htm)|Expertise du champion|`@Compendium[pf2e.classfeatures.VgmfNKtQLgBaNi5r]`|
|[Champion Mastery](classfeatures/champion-17-z5G0o04uV65zyxDB.htm)|Maîtrise du Champion|`@Compendium[pf2e.classfeatures.z5G0o04uV65zyxDB]`|
|[Champion's Reaction](classfeatures/champion-01-sXVX4ARUuo8Egrz5.htm)|Réaction de champion|`@Compendium[pf2e.classfeatures.sXVX4ARUuo8Egrz5]`|
|[Chirurgeon](classfeatures/alchemist-01-eNZnx4LISDNftbx2.htm)|Chirurgien|`@Compendium[pf2e.classfeatures.eNZnx4LISDNftbx2]`|
|[Cloistered Cleric](classfeatures/cleric-01-ZZzLMOUAtBVgV1DF.htm)|Prêtre cloîtré|`@Compendium[pf2e.classfeatures.ZZzLMOUAtBVgV1DF]`|
|[Combat Flexibility](classfeatures/fighter-09-8g6HzARbhfcgilP8.htm)|Flexibilité martiale|`@Compendium[pf2e.classfeatures.8g6HzARbhfcgilP8]`|
|[Composition Spells](classfeatures/bard-01-s0VbbQJNlSgPocui.htm)|Sorts de composition|`@Compendium[pf2e.classfeatures.s0VbbQJNlSgPocui]`|
|[Confident Finisher](classfeatures/swashbuckler-01-pyo0vmxUFIFX2GNl.htm)|Coup final assuré|`@Compendium[pf2e.classfeatures.pyo0vmxUFIFX2GNl]`|
|[Continuous Flair](classfeatures/swashbuckler-11-13QpCrR8a8XULbJa.htm)|Élégance continuelle|`@Compendium[pf2e.classfeatures.13QpCrR8a8XULbJa]`|
|[Copper Dragon Instinct](classfeatures/barbarian-01-1ZugTzJHsa94AZRW.htm)|Instinct du Dragon - Cuivre|`@Compendium[pf2e.classfeatures.1ZugTzJHsa94AZRW]`|
|[Cosmos Mystery](classfeatures/oracle-01-RI2EMRBBPNSoTJXu.htm)|Mystère du cosmos|`@Compendium[pf2e.classfeatures.RI2EMRBBPNSoTJXu]`|
|[Curse Patron](classfeatures/witch-01-NAXRmMjj0gcyD7ie.htm)|Patron : Malédiction|`@Compendium[pf2e.classfeatures.NAXRmMjj0gcyD7ie]`|
|[Debilitating Strikes](classfeatures/rogue-09-9SruVg2lZpNaYLOB.htm)|Frappes incapacitantes|`@Compendium[pf2e.classfeatures.9SruVg2lZpNaYLOB]`|
|[Deductive Improvisation](classfeatures/investigator-11-malYpr0CYL4fDGhr.htm)|Improvisation déductive|`@Compendium[pf2e.classfeatures.malYpr0CYL4fDGhr]`|
|[Deer Animal Instinct](classfeatures/barbarian-01-RQUJgDjJODO775qb.htm)|Instinct animal - Cerf|`@Compendium[pf2e.classfeatures.RQUJgDjJODO775qb]`|
|[Defensive Robes (Level 13)](classfeatures/wizard-13-gU7epgcPSm0TD1UK.htm)|Réobes défensives (Niveau 13)|`@Compendium[pf2e.classfeatures.gU7epgcPSm0TD1UK]`|
|[Deific Weapon](classfeatures/champion-01-FeBsYn2mHfMVDZvw.htm)|Arme déifique|`@Compendium[pf2e.classfeatures.FeBsYn2mHfMVDZvw]`|
|[Deity](classfeatures/cleric-01-DutW12WMFPHBoLTH.htm)|Divinité|`@Compendium[pf2e.classfeatures.DutW12WMFPHBoLTH]`|
|[Deity and Cause](classfeatures/champion-01-ehL7mnkqxN5wIkgu.htm)|Divinité et cause|`@Compendium[pf2e.classfeatures.ehL7mnkqxN5wIkgu]`|
|[Deny Advantage (Level 3)](classfeatures/rogue-03-PNpmVmD21zViDtGC.htm)|Refus d'avantage (Niveau 3)|`@Compendium[pf2e.classfeatures.PNpmVmD21zViDtGC]`|
|[Desecrator](classfeatures/champion-01-8YIA0jh64Ecz0TG6.htm)|Profanateur [Neutre mauvais]|`@Compendium[pf2e.classfeatures.8YIA0jh64Ecz0TG6]`|
|[Devastator](classfeatures/barbarian-19-VLiT503OLOM3vaDx.htm)|Dévastateur|`@Compendium[pf2e.classfeatures.VLiT503OLOM3vaDx]`|
|[Devise a Stratagem](classfeatures/investigator-01-lgo65ldX7WkXC8Ir.htm)|Concevoir un stratagème|`@Compendium[pf2e.classfeatures.lgo65ldX7WkXC8Ir]`|
|[Devotion Spells](classfeatures/champion-01-Q1VfQZp49hkhY0HY.htm)|Sorts de dévotion|`@Compendium[pf2e.classfeatures.Q1VfQZp49hkhY0HY]`|
|[Divine Ally](classfeatures/champion-03-ERwuazupczhUSZ73.htm)|Allié divin|`@Compendium[pf2e.classfeatures.ERwuazupczhUSZ73]`|
|[Divine Defense](classfeatures/cleric-13-0mJTp4LdEHBLInoe.htm)|Défense divine|`@Compendium[pf2e.classfeatures.0mJTp4LdEHBLInoe]`|
|[Divine Font](classfeatures/cleric-01-gblTFUOgolqFS9v4.htm)|Source divine|`@Compendium[pf2e.classfeatures.gblTFUOgolqFS9v4]`|
|[Divine Smite](classfeatures/champion-09-3XK573A7GH1rrLgO.htm)|Châtiment divin|`@Compendium[pf2e.classfeatures.3XK573A7GH1rrLgO]`|
|[Divine Spellcasting (Cleric)](classfeatures/cleric-01-AvNbdGSOTWNRgcxs.htm)|Incantation divine|`@Compendium[pf2e.classfeatures.AvNbdGSOTWNRgcxs]`|
|[Divine Spellcasting (Oracle)](classfeatures/oracle-01-7AVspOB6ITNzGFZi.htm)|Incantation divine (Oracle)|`@Compendium[pf2e.classfeatures.7AVspOB6ITNzGFZi]`|
|[Divine Will](classfeatures/champion-11-xygfZopqXBJ6dKBA.htm)|Résolution divine|`@Compendium[pf2e.classfeatures.xygfZopqXBJ6dKBA]`|
|[Doctrine](classfeatures/cleric-01-tyrBwBTzo5t9Zho7.htm)|Doctrine|`@Compendium[pf2e.classfeatures.tyrBwBTzo5t9Zho7]`|
|[Double Brew](classfeatures/alchemist-09-76cwNLJEm4Yetnee.htm)|Double préparation|`@Compendium[pf2e.classfeatures.76cwNLJEm4Yetnee]`|
|[Double Debilitation](classfeatures/rogue-15-W1FkMHYVDg3yTU5r.htm)|Double handicap|`@Compendium[pf2e.classfeatures.W1FkMHYVDg3yTU5r]`|
|[Dragon Instinct](classfeatures/barbarian-01-VDot7CDcXElxmkkz.htm)|Instinct du dragon|`@Compendium[pf2e.classfeatures.VDot7CDcXElxmkkz]`|
|[Druid Weapon Expertise](classfeatures/druid-11-Ra32tlqBxHzT6fzN.htm)|Expertise avec les armes de druide|`@Compendium[pf2e.classfeatures.Ra32tlqBxHzT6fzN]`|
|[Druidic Language](classfeatures/druid-01-RiAGlnnp4S21BAG3.htm)|Le druidique|`@Compendium[pf2e.classfeatures.RiAGlnnp4S21BAG3]`|
|[Druidic Order](classfeatures/druid-01-8STJEFVJISujgpMR.htm)|Ordre druidique|`@Compendium[pf2e.classfeatures.8STJEFVJISujgpMR]`|
|[Eldritch Trickster Racket](classfeatures/rogue-01-D8qtAo2w4jsqjBrM.htm)|Trafic de roublard : Mystificateur|`@Compendium[pf2e.classfeatures.D8qtAo2w4jsqjBrM]`|
|[Empiricism Methodology](classfeatures/investigator-01-g3mNzNphtVxyR9Xr.htm)|Méthodologie : empirisme|`@Compendium[pf2e.classfeatures.g3mNzNphtVxyR9Xr]`|
|[Enigma Muse](classfeatures/bard-01-4ripp6EfdVpS0d60.htm)|Muse - Énigmatique|`@Compendium[pf2e.classfeatures.4ripp6EfdVpS0d60]`|
|[Eternal Confidence](classfeatures/swashbuckler-19-ypfT3iybew6ZSIUl.htm)|Confiance éternelle|`@Compendium[pf2e.classfeatures.ypfT3iybew6ZSIUl]`|
|[Evasion (Level 15)](classfeatures/investigator-15-DqWr3LqUpT3Xi2xq.htm)|Évasion (Niveau 15)|`@Compendium[pf2e.classfeatures.DqWr3LqUpT3Xi2xq]`|
|[Evasion (Level 7)](classfeatures/swashbuckler-07-MV6XIuAgN9uSA0Da.htm)|Évasion (Niveau 7)|`@Compendium[pf2e.classfeatures.MV6XIuAgN9uSA0Da]`|
|[Exalt](classfeatures/champion-11-uptzvOLrZ3fctrl2.htm)|Éxaltation|`@Compendium[pf2e.classfeatures.uptzvOLrZ3fctrl2]`|
|[Exemplary Finisher](classfeatures/swashbuckler-09-KxpaxUSuBC7hr4F7.htm)|Coup final exemplaire|`@Compendium[pf2e.classfeatures.KxpaxUSuBC7hr4F7]`|
|[Expert Spellcaster (Level 7)](classfeatures/wizard-07-cD3nSupdCvONuHiE.htm)|Incantateur expert (Niveau 7)|`@Compendium[pf2e.classfeatures.cD3nSupdCvONuHiE]`|
|[Expert Strikes](classfeatures/monk-05-VgZIutWjFl8oZQFi.htm)|Frappes expertes|`@Compendium[pf2e.classfeatures.VgZIutWjFl8oZQFi]`|
|[Extreme Curse](classfeatures/oracle-17-F4brPlp1tHGUqyuI.htm)|Malédiction extrême|`@Compendium[pf2e.classfeatures.F4brPlp1tHGUqyuI]`|
|[Familiar (Witch)](classfeatures/witch-01-yksPhweBZYVCsE1A.htm)|Familier (Sorcière)|`@Compendium[pf2e.classfeatures.yksPhweBZYVCsE1A]`|
|[Fate Patron](classfeatures/witch-01-qMZiTugiLCEmkg8h.htm)|Patron : Destin|`@Compendium[pf2e.classfeatures.qMZiTugiLCEmkg8h]`|
|[Fencer Style](classfeatures/swashbuckler-01-Jgid6Ja6Y879COlN.htm)|Style : escrimeur|`@Compendium[pf2e.classfeatures.Jgid6Ja6Y879COlN]`|
|[Fervor Patron](classfeatures/witch-01-4IfYHrQMosJNM8hv.htm)|Patron : Ferveur|`@Compendium[pf2e.classfeatures.4IfYHrQMosJNM8hv]`|
|[Field Discovery (Bomber)](classfeatures/alchemist-05-8QAFgy9U8PxEa7Dw.htm)|Découverte de domaine de recherche (Artificier)|`@Compendium[pf2e.classfeatures.8QAFgy9U8PxEa7Dw]`|
|[Field Discovery (Chirurgeon)](classfeatures/alchemist-05-qC0Iz6SlG2i9gv6g.htm)|Découverte de domaine de recherche (Chirurgien)|`@Compendium[pf2e.classfeatures.qC0Iz6SlG2i9gv6g]`|
|[Field Discovery (Mutagenist)](classfeatures/alchemist-05-V4Jt7eDnJBLv5bDj.htm)|Découverte de domaine de recherche (Mutagèniste)|`@Compendium[pf2e.classfeatures.V4Jt7eDnJBLv5bDj]`|
|[Field Discovery (Toxicologist)](classfeatures/alchemist-05-6zo2PJGYoig7nFpR.htm)|Découverte de domaine de recherche (Toxicologiste)|`@Compendium[pf2e.classfeatures.6zo2PJGYoig7nFpR]`|
|[Fighter Expertise](classfeatures/fighter-11-bAaI7h937Nr3g93U.htm)|Expertise du Guerrier|`@Compendium[pf2e.classfeatures.bAaI7h937Nr3g93U]`|
|[Fighter Weapon Mastery](classfeatures/fighter-05-gApJtAdNb9ST4Ms9.htm)|Maîtrise martiale du guerrier|`@Compendium[pf2e.classfeatures.gApJtAdNb9ST4Ms9]`|
|[Flames Mystery](classfeatures/oracle-01-GTSvbFb36InvuH0w.htm)|Mystère des flammes|`@Compendium[pf2e.classfeatures.GTSvbFb36InvuH0w]`|
|[Flurry of Blows](classfeatures/monk-01-NLHHHiAcdnZ5ohc2.htm)|Déluge de coups|`@Compendium[pf2e.classfeatures.NLHHHiAcdnZ5ohc2]`|
|[Focus Spells](classfeatures/None-01-T25ZLQWn6O4KchLo.htm)|Sorts focalisés|`@Compendium[pf2e.classfeatures.T25ZLQWn6O4KchLo]`|
|[Forensic Medicine Methodology](classfeatures/investigator-01-O3IX7rTxXWWvDVM3.htm)|Méthodologie : Médecine forensique|`@Compendium[pf2e.classfeatures.O3IX7rTxXWWvDVM3]`|
|[Formula Book](classfeatures/alchemical-01-XPPG7nN9pxt0sjMg.htm)|Formulaire|`@Compendium[pf2e.classfeatures.XPPG7nN9pxt0sjMg]`|
|[Frog Animal Instinct](classfeatures/barbarian-01-CXZwt1e6ManeBaFV.htm)|Instinct animal - Grenouille|`@Compendium[pf2e.classfeatures.CXZwt1e6ManeBaFV]`|
|[Fury Instinct](classfeatures/barbarian-01-k7M9jedvt31AJ5ZR.htm)|Instinct de la furie|`@Compendium[pf2e.classfeatures.k7M9jedvt31AJ5ZR]`|
|[Giant Instinct](classfeatures/barbarian-01-JuKD6k7nDwfO0Ckv.htm)|Instinct de géant|`@Compendium[pf2e.classfeatures.JuKD6k7nDwfO0Ckv]`|
|[Gold Dragon Instinct](classfeatures/barbarian-01-3lxIGMbsPZLNEXQ7.htm)|Instinct du dragon - Or|`@Compendium[pf2e.classfeatures.3lxIGMbsPZLNEXQ7]`|
|[Graceful Legend](classfeatures/monk-17-JWDfzYub3JfuEtth.htm)|Légende gracieuse|`@Compendium[pf2e.classfeatures.JWDfzYub3JfuEtth]`|
|[Graceful Mastery](classfeatures/monk-13-95LI24ZSx0d4qfKX.htm)|Maîtrise gracieuse|`@Compendium[pf2e.classfeatures.95LI24ZSx0d4qfKX]`|
|[Great Fortitude (Level 3)](classfeatures/swashbuckler-03-F57Na5VxfBp56kke.htm)|Vigueur supérieure (Niveau 3)|`@Compendium[pf2e.classfeatures.F57Na5VxfBp56kke]`|
|[Great Fortitude (Level 9)](classfeatures/rogue-09-25GSAotUcDwInYgG.htm)|Vigueur supérieure (Niveau 9)|`@Compendium[pf2e.classfeatures.25GSAotUcDwInYgG]`|
|[Greater Field Discovery (Bomber)](classfeatures/alchemist-13-RGs4uR3CAvgbtBAA.htm)|Découverte de domaine de recherche supérieure (Artificier)|`@Compendium[pf2e.classfeatures.RGs4uR3CAvgbtBAA]`|
|[Greater Field Discovery (Chirurgeon)](classfeatures/alchemist-13-JJcaVijwRt9dsnac.htm)|Découverte de domaine de recherche supérieure (Chirurgien)|`@Compendium[pf2e.classfeatures.JJcaVijwRt9dsnac]`|
|[Greater Field Discovery (Mutagenist)](classfeatures/alchemist-13-1BKdOJ0HNL6Eg3xw.htm)|Découverte de domaine de recherche supérieure (Mutagèniste)|`@Compendium[pf2e.classfeatures.1BKdOJ0HNL6Eg3xw]`|
|[Greater Field Discovery (Toxicologist)](classfeatures/alchemist-13-tnqyQrhrZeDtDvcO.htm)|Découverte de domaine de recherche supérieure (Toxicologiste)|`@Compendium[pf2e.classfeatures.tnqyQrhrZeDtDvcO]`|
|[Greater Juggernaut](classfeatures/barbarian-13-TuL0UfqH14MtqYVh.htm)|Juggernaut supérieur|`@Compendium[pf2e.classfeatures.TuL0UfqH14MtqYVh]`|
|[Greater Resolve (Level 17)](classfeatures/oracle-17-5LOARurr4qWkfS9K.htm)|Résolution supérieure (Niveau 17)|`@Compendium[pf2e.classfeatures.5LOARurr4qWkfS9K]`|
|[Greater Weapon Specialization (Barbarian)](classfeatures/barbarian-15-7JjhxMFo8DMwpGx0.htm)|Spécialisation martiale supérieure (Barbare)|`@Compendium[pf2e.classfeatures.7JjhxMFo8DMwpGx0]`|
|[Greater Weapon Specialization (Level 15)](classfeatures/swashbuckler-15-Z7HX6TeFsaup7Dx9.htm)|Spécialisation martiale supérieure (Niveau 15)|`@Compendium[pf2e.classfeatures.Z7HX6TeFsaup7Dx9]`|
|[Green Dragon Instinct](classfeatures/barbarian-01-IezPDYlweTtwCqkT.htm)|Instinct du dragon - Vert|`@Compendium[pf2e.classfeatures.IezPDYlweTtwCqkT]`|
|[Gymnast Style](classfeatures/swashbuckler-01-B7RMnrHwQHlezlJT.htm)|Style : Gymnaste|`@Compendium[pf2e.classfeatures.B7RMnrHwQHlezlJT]`|
|[Heightened Senses](classfeatures/barbarian-17-7MhzrbOyue5GQsck.htm)|Sens aiguisés|`@Compendium[pf2e.classfeatures.7MhzrbOyue5GQsck]`|
|[Hero's Defiance](classfeatures/champion-19-LzB6X9vOaq3wq1FZ.htm)|Défi du héros|`@Compendium[pf2e.classfeatures.LzB6X9vOaq3wq1FZ]`|
|[Hexes](classfeatures/witch-01-9uLh5z2uPo6LDFRY.htm)|Maléfices|`@Compendium[pf2e.classfeatures.9uLh5z2uPo6LDFRY]`|
|[Hunt Prey](classfeatures/ranger-01-0nIOGpHQNHsKSFKT.htm)|Chasser une proie|`@Compendium[pf2e.classfeatures.0nIOGpHQNHsKSFKT]`|
|[Hunter's Edge](classfeatures/ranger-01-mzkkj9LEWjJPBhaq.htm)|Spécialité du Chasseur|`@Compendium[pf2e.classfeatures.mzkkj9LEWjJPBhaq]`|
|[Hunter's Edge: Flurry](classfeatures/ranger-01-6v4Rj7wWfOH1882r.htm)|spécialité du chasseur : Déluge|`@Compendium[pf2e.classfeatures.6v4Rj7wWfOH1882r]`|
|[Hunter's Edge: Outwit](classfeatures/ranger-01-NBHyoTrI8q62uDsU.htm)|Spécialité du chasseur : Ruse|`@Compendium[pf2e.classfeatures.NBHyoTrI8q62uDsU]`|
|[Hunter's Edge: Precision](classfeatures/ranger-01-u6cBjqz2fiRBadBt.htm)|Spécialité du chasseur : Précision|`@Compendium[pf2e.classfeatures.u6cBjqz2fiRBadBt]`|
|[Improved Evasion (Level 13)](classfeatures/swashbuckler-13-L5D0NwFXdLiVSnk5.htm)|Évasion améliorée (Niveau 13)|`@Compendium[pf2e.classfeatures.L5D0NwFXdLiVSnk5]`|
|[Improved Evasion (Level 15)](classfeatures/ranger-15-6XwGONPdr9SFDtDc.htm)|Évasion améliorée (Niveau 15)|`@Compendium[pf2e.classfeatures.6XwGONPdr9SFDtDc]`|
|[Improved Flexibility](classfeatures/fighter-15-W2rwudMNcAxs8VoX.htm)|Flexibilité améliorée|`@Compendium[pf2e.classfeatures.W2rwudMNcAxs8VoX]`|
|[Incredible Movement](classfeatures/monk-03-Cq6NjvcKZOMySBVj.htm)|Déplacement extraordinaire|`@Compendium[pf2e.classfeatures.Cq6NjvcKZOMySBVj]`|
|[Incredible Senses (Level 13)](classfeatures/rogue-13-nLwPMPLRne1HnL00.htm)|Sens extraordinaires (Niveau 13)|`@Compendium[pf2e.classfeatures.nLwPMPLRne1HnL00]`|
|[Incredible Senses (Level 15)](classfeatures/ranger-15-iyb5FU2BpsCCan8Q.htm)|Sens extraordinaires (Niveau 15)|`@Compendium[pf2e.classfeatures.iyb5FU2BpsCCan8Q]`|
|[Indomitable Will](classfeatures/barbarian-15-BZnqKnqKVImjSIFE.htm)|Volonté indomptable|`@Compendium[pf2e.classfeatures.BZnqKnqKVImjSIFE]`|
|[Infused Reagents](classfeatures/alchemist-01-wySB9VHOW1v3TX1L.htm)|Réactifs imprégnés|`@Compendium[pf2e.classfeatures.wySB9VHOW1v3TX1L]`|
|[Instinct](classfeatures/barbarian-01-dU7xRpg4kFd01hwZ.htm)||`@Compendium[pf2e.classfeatures.dU7xRpg4kFd01hwZ]`|
|[Interrogation Methodology](classfeatures/investigator-01-UIHUNNYZyQ3p4Vmo.htm)|Méthodologie : Interrogatoire|`@Compendium[pf2e.classfeatures.UIHUNNYZyQ3p4Vmo]`|
|[Investigator Expertise](classfeatures/investigator-09-PFvB79O2VFdiAeSj.htm)|Expertise de l'enquêteur|`@Compendium[pf2e.classfeatures.PFvB79O2VFdiAeSj]`|
|[Iron Will (Level 3)](classfeatures/ranger-03-wMyDcVNmA7xGK83S.htm)|Volonté de fer (Niveau 3)|`@Compendium[pf2e.classfeatures.wMyDcVNmA7xGK83S]`|
|[Iron Will (Level 7)](classfeatures/alchemist-07-JVCxv4HuLaaFhAf4.htm)|Volonté de fer (Niveau 7)|`@Compendium[pf2e.classfeatures.JVCxv4HuLaaFhAf4]`|
|[Juggernaut (Level 11)](classfeatures/ranger-11-Ba97T4anGhizfaCt.htm)|Juggernaut (Niveau 11)|`@Compendium[pf2e.classfeatures.Ba97T4anGhizfaCt]`|
|[Juggernaut (Level 7)](classfeatures/barbarian-07-OMZs5y16jZRW9KQK.htm)|Juggernaut (Niveau 7)|`@Compendium[pf2e.classfeatures.OMZs5y16jZRW9KQK]`|
|[Juggernaut (Level 9)](classfeatures/fighter-09-sHCFQZM0xHCOYOId.htm)|Juggernaut (Niveau 9)|`@Compendium[pf2e.classfeatures.sHCFQZM0xHCOYOId]`|
|[Keen Flair](classfeatures/swashbuckler-15-Pk3Ht0KZyFxSeL07.htm)|Élégance aigüe|`@Compendium[pf2e.classfeatures.Pk3Ht0KZyFxSeL07]`|
|[Keen Recollection](classfeatures/investigator-03-DZWQspPi4IkfXV2E.htm)|Souvenirs affûtés|`@Compendium[pf2e.classfeatures.DZWQspPi4IkfXV2E]`|
|[Ki Spells](classfeatures/None-01-1FPVkksuE2ncw9rF.htm)|Sorts de ki|`@Compendium[pf2e.classfeatures.1FPVkksuE2ncw9rF]`|
|[Leaf Order](classfeatures/druid-01-u4nlOzPj2WHkIj9l.htm)|Feuille|`@Compendium[pf2e.classfeatures.u4nlOzPj2WHkIj9l]`|
|[Legendary Armor](classfeatures/champion-17-voiSCh7ZXA2ogwiC.htm)|Armure légendaire|`@Compendium[pf2e.classfeatures.voiSCh7ZXA2ogwiC]`|
|[Legendary Spellcaster (Level 19)](classfeatures/wizard-19-Hfaa7TuLn3nE8lr3.htm)|Incantateur légendaire (Niveau 19)|`@Compendium[pf2e.classfeatures.Hfaa7TuLn3nE8lr3]`|
|[Liberator](classfeatures/champion-01-FCoMFUsth4xB4veC.htm)|Libérateur [Chaotique Bon]|`@Compendium[pf2e.classfeatures.FCoMFUsth4xB4veC]`|
|[Life Mystery](classfeatures/oracle-01-o1gGG36wpn9mxeop.htm)|Mystère de la vie|`@Compendium[pf2e.classfeatures.o1gGG36wpn9mxeop]`|
|[Light Armor Expertise (Level 13)](classfeatures/swashbuckler-13-pZYkb12t5DSwtts7.htm)|Expertise avec les armures légères (Niveau 13)|`@Compendium[pf2e.classfeatures.pZYkb12t5DSwtts7]`|
|[Light Armor Mastery (Level 19)](classfeatures/swashbuckler-19-SHpjmM4A3Sw4GgDz.htm)|Maîtrise des armures légères (Niveau 19)|`@Compendium[pf2e.classfeatures.SHpjmM4A3Sw4GgDz]`|
|[Lightning Reflexes (Level 11)](classfeatures/cleric-11-7PzcKaDGy6tIkQh4.htm)|Réflexes éclair (Niveau 11)|`@Compendium[pf2e.classfeatures.7PzcKaDGy6tIkQh4]`|
|[Lightning Reflexes (Level 13)](classfeatures/oracle-13-EJzjY6AIsTYqW0ee.htm)|Réflexes fulgurants (Niveau 13)|`@Compendium[pf2e.classfeatures.EJzjY6AIsTYqW0ee]`|
|[Lightning Reflexes (Level 3)](classfeatures/bard-03-TUOeATt52P43r5W0.htm)|Réflexes fulgurants (Niveau 3)|`@Compendium[pf2e.classfeatures.TUOeATt52P43r5W0]`|
|[Lightning Reflexes (Level 5)](classfeatures/wizard-05-rz87RgR1crWTd7j5.htm)|Réflexes fulgurants (Niveau 5)|`@Compendium[pf2e.classfeatures.rz87RgR1crWTd7j5]`|
|[Lightning Reflexes (Level 9)](classfeatures/witch-09-tfugXJHITCnArN1b.htm)|Réflexes fulgurants (Niveau 9)|`@Compendium[pf2e.classfeatures.tfugXJHITCnArN1b]`|
|[Lore Mystery](classfeatures/oracle-01-tZBb3Kh4nJcNoUFI.htm)|Mystère du savoir|`@Compendium[pf2e.classfeatures.tZBb3Kh4nJcNoUFI]`|
|[Maestro Muse](classfeatures/bard-01-YMBsi4bndRAk5CX4.htm)|Muse - Virtuose|`@Compendium[pf2e.classfeatures.YMBsi4bndRAk5CX4]`|
|[Magical Fortitude (Level 5)](classfeatures/witch-05-70jqXP2eS4tRZ0Ok.htm)|Vigueur magique (Niveau 5)|`@Compendium[pf2e.classfeatures.70jqXP2eS4tRZ0Ok]`|
|[Magical Fortitude (Level 9)](classfeatures/wizard-09-MzyPNlxrNA5OKVd7.htm)|Vigueur magique (Niveau 9)|`@Compendium[pf2e.classfeatures.MzyPNlxrNA5OKVd7]`|
|[Magnum Opus](classfeatures/bard-19-NjsOpWbbzUY2Hpk3.htm)|Magnum opus|`@Compendium[pf2e.classfeatures.NjsOpWbbzUY2Hpk3]`|
|[Major Curse](classfeatures/oracle-11-rrzItB68Er0DzKx7.htm)|Malédiction majeure|`@Compendium[pf2e.classfeatures.rrzItB68Er0DzKx7]`|
|[Master Detective](classfeatures/investigator-19-flEx8eY0NinF9XZU.htm)|Maître détective|`@Compendium[pf2e.classfeatures.flEx8eY0NinF9XZU]`|
|[Master Spellcaster (Level 15)](classfeatures/wizard-15-l1InYvhnQSz6Ucxc.htm)|Incantateur maître (Niveau 15)|`@Compendium[pf2e.classfeatures.l1InYvhnQSz6Ucxc]`|
|[Master Strike](classfeatures/rogue-19-SUUdWG0t33VKa5q4.htm)|Frappe de maître|`@Compendium[pf2e.classfeatures.SUUdWG0t33VKa5q4]`|
|[Master Strikes](classfeatures/monk-13-0iidKkzC2yy13lIf.htm)|Frappes de maître|`@Compendium[pf2e.classfeatures.0iidKkzC2yy13lIf]`|
|[Master Tricks](classfeatures/rogue-13-myvcir1LEkaVxOlE.htm)|Astuces de maître|`@Compendium[pf2e.classfeatures.myvcir1LEkaVxOlE]`|
|[Masterful Hunter](classfeatures/ranger-17-RVZC4wVy5B5W2OeS.htm)|Maître chasseur|`@Compendium[pf2e.classfeatures.RVZC4wVy5B5W2OeS]`|
|[Masterful Hunter (Flurry)](classfeatures/None-17-JhLncIB10GSQowWL.htm)||`@Compendium[pf2e.classfeatures.JhLncIB10GSQowWL]`|
|[Masterful Hunter (Outwit)](classfeatures/None-17-vWZaLE2fEKMBw3D5.htm)||`@Compendium[pf2e.classfeatures.vWZaLE2fEKMBw3D5]`|
|[Masterful Hunter (Precision)](classfeatures/None-17-BJYSUbFUGcTLaPDn.htm)||`@Compendium[pf2e.classfeatures.BJYSUbFUGcTLaPDn]`|
|[Mastermind Racket](classfeatures/rogue-01-RyOkmu0W9svavuAB.htm)|Trafic de roublard : Cerveau|`@Compendium[pf2e.classfeatures.RyOkmu0W9svavuAB]`|
|[Medium Armor Expertise (Level 11)](classfeatures/ranger-11-FCEp9jjxxgRJDJV3.htm)|Expertise avec les armures intermédiaires (Niveau 11)|`@Compendium[pf2e.classfeatures.FCEp9jjxxgRJDJV3]`|
|[Medium Armor Expertise (Level 13)](classfeatures/druid-13-tzUaTqB6GHAeffOl.htm)|Expertise avec les armures intermédiaires (Niveau 13)|`@Compendium[pf2e.classfeatures.tzUaTqB6GHAeffOl]`|
|[Metal Strikes](classfeatures/monk-09-CoRfFkisEsHE1e43.htm)|Frappes de métal|`@Compendium[pf2e.classfeatures.CoRfFkisEsHE1e43]`|
|[Methodology](classfeatures/investigator-01-uhHg9BXBiHpL5ndS.htm)|Méthodologie|`@Compendium[pf2e.classfeatures.uhHg9BXBiHpL5ndS]`|
|[Mighty Rage](classfeatures/barbarian-11-88Q33X2a0iYPkbzd.htm)|Rage formidable|`@Compendium[pf2e.classfeatures.88Q33X2a0iYPkbzd]`|
|[Miraculous Spell](classfeatures/cleric-19-3uf31A91h3ywmlqm.htm)|Sort miraculeux|`@Compendium[pf2e.classfeatures.3uf31A91h3ywmlqm]`|
|[Monk Expertise](classfeatures/monk-09-lxImO5D0qWp0gXFB.htm)|Expertise du moine|`@Compendium[pf2e.classfeatures.lxImO5D0qWp0gXFB]`|
|[Muses](classfeatures/bard-01-AIOBWGOS4nkfH3kW.htm)||`@Compendium[pf2e.classfeatures.AIOBWGOS4nkfH3kW]`|
|[Mutagenist](classfeatures/alchemist-01-tvdb1jkjl2bRZjSp.htm)|Mutagèniste|`@Compendium[pf2e.classfeatures.tvdb1jkjl2bRZjSp]`|
|[Mystery](classfeatures/oracle-01-PRJYLksQEwT39bTl.htm)|Mystère|`@Compendium[pf2e.classfeatures.PRJYLksQEwT39bTl]`|
|[Mystic Strikes](classfeatures/monk-03-D2AE8RfMlZ3D1FuV.htm)|Frappes mystiques|`@Compendium[pf2e.classfeatures.D2AE8RfMlZ3D1FuV]`|
|[Nature's Edge](classfeatures/ranger-09-j2R64kwUgEJ1TudD.htm)|Avantage naturel|`@Compendium[pf2e.classfeatures.j2R64kwUgEJ1TudD]`|
|[Night Patron](classfeatures/witch-01-XFTWJO6txmLNRLae.htm)|Patron : Nuit|`@Compendium[pf2e.classfeatures.XFTWJO6txmLNRLae]`|
|[Occult Spellcasting](classfeatures/bard-01-fEOj0eOBe34qYdAa.htm)|Incantation occulte|`@Compendium[pf2e.classfeatures.fEOj0eOBe34qYdAa]`|
|[On the Case](classfeatures/investigator-01-6FasgIXUJ1X8ekRn.htm)|Sur l'affaire|`@Compendium[pf2e.classfeatures.6FasgIXUJ1X8ekRn]`|
|[Opportune Riposte](classfeatures/swashbuckler-03-Jtn7IugykXDlIoZq.htm)|riposte opportune|`@Compendium[pf2e.classfeatures.Jtn7IugykXDlIoZq]`|
|[Oracular Clarity](classfeatures/oracle-19-571c1aGnvNVwfF6b.htm)|Clarté oraculaire|`@Compendium[pf2e.classfeatures.571c1aGnvNVwfF6b]`|
|[Oracular Curse](classfeatures/oracle-01-ibX2EhKkyUtbOHLj.htm)|Malédiction oraculaire|`@Compendium[pf2e.classfeatures.ibX2EhKkyUtbOHLj]`|
|[Order of the Chain](classfeatures/None-00-YrJj8UI0XpkHv0Ho.htm)||`@Compendium[pf2e.classfeatures.YrJj8UI0XpkHv0Ho]`|
|[Order of the Gate](classfeatures/None-00-igMHwREgpM9GsvLs.htm)||`@Compendium[pf2e.classfeatures.igMHwREgpM9GsvLs]`|
|[Order of the Godclaw](classfeatures/None-00-UTRDN1TAieBMjwP1.htm)||`@Compendium[pf2e.classfeatures.UTRDN1TAieBMjwP1]`|
|[Order of the Nail](classfeatures/None-00-ub9gwFXnMuKvhnPL.htm)||`@Compendium[pf2e.classfeatures.ub9gwFXnMuKvhnPL]`|
|[Order of the Pyre](classfeatures/None-00-zGxO2cETUsXuvqRu.htm)||`@Compendium[pf2e.classfeatures.zGxO2cETUsXuvqRu]`|
|[Order of the Rack](classfeatures/None-00-lVdfcITy5bkywW5f.htm)||`@Compendium[pf2e.classfeatures.lVdfcITy5bkywW5f]`|
|[Order of the Scourge](classfeatures/None-00-t01K3DB2qHnbt1q3.htm)||`@Compendium[pf2e.classfeatures.t01K3DB2qHnbt1q3]`|
|[Paladin](classfeatures/champion-01-peEXunfbSD8WcMFk.htm)|Paladin (Loyal Bon)|`@Compendium[pf2e.classfeatures.peEXunfbSD8WcMFk]`|
|[Panache](classfeatures/swashbuckler-01-LzYi0OuOoypNb6jd.htm)|Panache|`@Compendium[pf2e.classfeatures.LzYi0OuOoypNb6jd]`|
|[Path to Perfection (Fortitude)](classfeatures/monk-07-KIqptJsjq9pS9CP7.htm)|Voie vers la Perfection - Vigueur|`@Compendium[pf2e.classfeatures.KIqptJsjq9pS9CP7]`|
|[Path to Perfection (Reflex)](classfeatures/monk-07-Bwr9G9IR4ynm5wzz.htm)|Voie vers la perfection - Réflexes|`@Compendium[pf2e.classfeatures.Bwr9G9IR4ynm5wzz]`|
|[Path to Perfection (Will)](classfeatures/monk-07-7lanxgmoOHNdtDe2.htm)|Voie vers la Perfection - Volonté|`@Compendium[pf2e.classfeatures.7lanxgmoOHNdtDe2]`|
|[Patron](classfeatures/witch-01-KPtF29AaeX2sJW0K.htm)|Patron|`@Compendium[pf2e.classfeatures.KPtF29AaeX2sJW0K]`|
|[Patron Theme](classfeatures/witch-01-nocYmxbi4rqCC2qS.htm)|Thême de patron|`@Compendium[pf2e.classfeatures.nocYmxbi4rqCC2qS]`|
|[Patron's Gift](classfeatures/witch-19-cDnFXfl3i5Z2l7JP.htm)|Don du patron|`@Compendium[pf2e.classfeatures.cDnFXfl3i5Z2l7JP]`|
|[Perfected Form](classfeatures/monk-19-KmTfg7Sg5va4yU00.htm)|Forme parfaite|`@Compendium[pf2e.classfeatures.KmTfg7Sg5va4yU00]`|
|[Perpetual Infusions (Bomber)](classfeatures/alchemist-07-DFQDtT1Van4fFEHi.htm)|Infusions perpétuelles (Artificier)|`@Compendium[pf2e.classfeatures.DFQDtT1Van4fFEHi]`|
|[Perpetual Infusions (Chirurgeon)](classfeatures/alchemist-07-fzvIe6FwwCuIdnjX.htm)|Infusions perpétuelles (Chirurgien)|`@Compendium[pf2e.classfeatures.fzvIe6FwwCuIdnjX]`|
|[Perpetual Infusions (Mutagenist)](classfeatures/alchemist-07-Dug1oaVYejLmYEFt.htm)|Infusions perpétuelles (Mutagèniste)|`@Compendium[pf2e.classfeatures.Dug1oaVYejLmYEFt]`|
|[Perpetual Infusions (Toxicologist)](classfeatures/alchemist-07-LlZ5R50z9j8jysZL.htm)|Infusions perpétuelles (Toxicologiste)|`@Compendium[pf2e.classfeatures.LlZ5R50z9j8jysZL]`|
|[Perpetual Perfection (Bomber)](classfeatures/alchemist-17-xO90iBD8XNGyaCkz.htm)|Perfection perpétuelle (Artificier)|`@Compendium[pf2e.classfeatures.xO90iBD8XNGyaCkz]`|
|[Perpetual Perfection (Chirurgeon)](classfeatures/alchemist-17-YByJ9O7oe8wxfbqs.htm)|Perfection perpétuelle (Chirurgien)|`@Compendium[pf2e.classfeatures.YByJ9O7oe8wxfbqs]`|
|[Perpetual Perfection (Mutagenist)](classfeatures/alchemist-17-CGetAmSbv06fW7GT.htm)|Perfection perpétuelle (Mutagèniste)|`@Compendium[pf2e.classfeatures.CGetAmSbv06fW7GT]`|
|[Perpetual Perfection (Toxicologist)](classfeatures/alchemist-17-3R19zS7gERhEX87F.htm)|Perfection perpétuelle (Toxicologiste)|`@Compendium[pf2e.classfeatures.3R19zS7gERhEX87F]`|
|[Perpetual Potency (Bomber)](classfeatures/alchemist-11-8rEVg03QJ71ic3PP.htm)|Efficacité perpétuelle (Artificier)|`@Compendium[pf2e.classfeatures.8rEVg03QJ71ic3PP]`|
|[Perpetual Potency (Chirurgeon)](classfeatures/alchemist-11-VS5vkqUQu4n7E28Y.htm)|Efficacité perpétuelle (Chirurgien)|`@Compendium[pf2e.classfeatures.VS5vkqUQu4n7E28Y]`|
|[Perpetual Potency (Mutagenist)](classfeatures/alchemist-11-mZFqRLYOQEqKA8ri.htm)|Efficacité perpétuelle (Mutagèniste)|`@Compendium[pf2e.classfeatures.mZFqRLYOQEqKA8ri]`|
|[Perpetual Potency (Toxicologist)](classfeatures/alchemist-11-JOdbVu14phvdjhaY.htm)|Efficacité perpétuelle (Toxicologiste)|`@Compendium[pf2e.classfeatures.JOdbVu14phvdjhaY]`|
|[Polymath Muse](classfeatures/bard-01-y0jGimYdMGDJWrEq.htm)|Muse - Touche-à-tout|`@Compendium[pf2e.classfeatures.y0jGimYdMGDJWrEq]`|
|[Powerful Alchemy](classfeatures/alchemist-05-7JK2a1D3VeWDcObo.htm)|Alchimie puissante|`@Compendium[pf2e.classfeatures.7JK2a1D3VeWDcObo]`|
|[Powerful Fist](classfeatures/monk-01-SB8UJ8rZmvbcBweJ.htm)|Poings puissants|`@Compendium[pf2e.classfeatures.SB8UJ8rZmvbcBweJ]`|
|[Precise Strike](classfeatures/swashbuckler-01-RQH6vigvhmiYKKjg.htm)|Frappe précise|`@Compendium[pf2e.classfeatures.RQH6vigvhmiYKKjg]`|
|[Primal Hierophant](classfeatures/druid-19-nzgb43mQmLgaqDoQ.htm)|Hiérophante primordial|`@Compendium[pf2e.classfeatures.nzgb43mQmLgaqDoQ]`|
|[Primal Spellcasting](classfeatures/druid-01-b8pnRxGuNzG0buuh.htm)|Incantation primordiale|`@Compendium[pf2e.classfeatures.b8pnRxGuNzG0buuh]`|
|[Quick Alchemy](classfeatures/alchemist-01-sPtl05wwTpqFI0lL.htm)|Alchimie rapide|`@Compendium[pf2e.classfeatures.sPtl05wwTpqFI0lL]`|
|[Quick Rage](classfeatures/barbarian-17-qMtyQGUllPdgpzUo.htm)|Rage rapide|`@Compendium[pf2e.classfeatures.qMtyQGUllPdgpzUo]`|
|[Rage](classfeatures/mental-01-WZUCvxqbigXos1L9.htm)|Rage|`@Compendium[pf2e.classfeatures.WZUCvxqbigXos1L9]`|
|[Raging Resistance](classfeatures/barbarian-09-ie6xDX9GMEcA2Iuq.htm)|Résistance enragée|`@Compendium[pf2e.classfeatures.ie6xDX9GMEcA2Iuq]`|
|[Ranger Expertise](classfeatures/ranger-09-5likl5SAxQPrQ3KF.htm)|Expertise du rôdeur|`@Compendium[pf2e.classfeatures.5likl5SAxQPrQ3KF]`|
|[Ranger Weapon Expertise](classfeatures/ranger-05-QhoW8ivPvYmWzyEZ.htm)|Expertise avec les armes du rôdeur|`@Compendium[pf2e.classfeatures.QhoW8ivPvYmWzyEZ]`|
|[Red Dragon Instinct](classfeatures/barbarian-01-hyHgLQCDMSrR4RfE.htm)|Instinct du dragon - Rouge|`@Compendium[pf2e.classfeatures.hyHgLQCDMSrR4RfE]`|
|[Redeemer](classfeatures/champion-01-UyuwFp0jQqYL2AdF.htm)|Rédempteur (Neutre Bon)|`@Compendium[pf2e.classfeatures.UyuwFp0jQqYL2AdF]`|
|[Research Field](classfeatures/alchemist-01-cU2ofQLj7pg6wTSi.htm)||`@Compendium[pf2e.classfeatures.cU2ofQLj7pg6wTSi]`|
|[Resolve (Level 11)](classfeatures/investigator-11-vv63fioCtOvDIdF2.htm)|Résolution (Niveau 11)|`@Compendium[pf2e.classfeatures.vv63fioCtOvDIdF2]`|
|[Resolve (Level 17)](classfeatures/wizard-17-9WjZSliQZJlyGvUi.htm)|Résolution (Niveau 17)|`@Compendium[pf2e.classfeatures.9WjZSliQZJlyGvUi]`|
|[Resolve (Level 7)](classfeatures/oracle-07-JQAujUXjczVnYDEI.htm)|Résolution (Niveau 7)|`@Compendium[pf2e.classfeatures.JQAujUXjczVnYDEI]`|
|[Resolve (Level 9)](classfeatures/cleric-09-D2g6sZQAWaTccviQ.htm)|Résolution (Niveau 9)|`@Compendium[pf2e.classfeatures.D2g6sZQAWaTccviQ]`|
|[Revelation Spells](classfeatures/oracle-01-NXUOtO9NytHQurlg.htm)|Sorts de révélation|`@Compendium[pf2e.classfeatures.NXUOtO9NytHQurlg]`|
|[Rogue Expertise](classfeatures/rogue-11-f3Dh32EU4VsHu01b.htm)|Expertise du Roublard|`@Compendium[pf2e.classfeatures.f3Dh32EU4VsHu01b]`|
|[Rogue's Racket](classfeatures/rogue-01-uGuCGQvUmioFV2Bd.htm)|Trafics de roublard|`@Compendium[pf2e.classfeatures.uGuCGQvUmioFV2Bd]`|
|[Ruffian Racket](classfeatures/rogue-01-3KPZ7svIO6kmmEKH.htm)|Trafic de roublard : Voyou|`@Compendium[pf2e.classfeatures.3KPZ7svIO6kmmEKH]`|
|[Rune Patron](classfeatures/witch-01-ejmSQOJR5lJv1pzh.htm)|Patron : Rune|`@Compendium[pf2e.classfeatures.ejmSQOJR5lJv1pzh]`|
|[Scoundrel Racket](classfeatures/rogue-01-ZvfxtUMtfIOLYHyg.htm)|Trafic de roublard : Scélérat|`@Compendium[pf2e.classfeatures.ZvfxtUMtfIOLYHyg]`|
|[Second Path to Perfection (Fortitude)](classfeatures/monk-11-XZnPwZ0ohlDXlFea.htm)|Deuxième voie vers la Perfection - Vigueur|`@Compendium[pf2e.classfeatures.XZnPwZ0ohlDXlFea]`|
|[Second Path to Perfection (Reflex)](classfeatures/monk-11-yDL9l9Klki6gE2ZD.htm)|Deuxième voie vers la Perfection -Réflexes|`@Compendium[pf2e.classfeatures.yDL9l9Klki6gE2ZD]`|
|[Second Path to Perfection (Will)](classfeatures/monk-11-RVPhB0RqmoJg7xI6.htm)|Deuxième voie vers la Perfection - Volonté|`@Compendium[pf2e.classfeatures.RVPhB0RqmoJg7xI6]`|
|[Second Skin](classfeatures/ranger-19-phwQ2MrDZ13D2HxC.htm)|Seconde peau|`@Compendium[pf2e.classfeatures.phwQ2MrDZ13D2HxC]`|
|[Shark Animal Instinct](classfeatures/barbarian-01-OJmI1L4dhQfz8vze.htm)|Instinct animal - Requin|`@Compendium[pf2e.classfeatures.OJmI1L4dhQfz8vze]`|
|[Shield Block (Level 1)](classfeatures/fighter-01-eZNCckLzbH3GyncH.htm)|Blocage au bouclier (Niveau 1)|`@Compendium[pf2e.classfeatures.eZNCckLzbH3GyncH]`|
|[Signature Spells (Level 3)](classfeatures/sorcerer-03-VKRjmXxBFLrJK01c.htm)|Sorts emblématiques (Niveau 3)|`@Compendium[pf2e.classfeatures.VKRjmXxBFLrJK01c]`|
|[Silver Dragon Instinct](classfeatures/barbarian-01-Z2eWkfXblU0QxFx1.htm)|Instinct du Dragon - Argent|`@Compendium[pf2e.classfeatures.Z2eWkfXblU0QxFx1]`|
|[Simple Weapon Expertise](classfeatures/sorcerer-11-4uvohEHSm17wY7gc.htm)|Expertise avec les armes simples|`@Compendium[pf2e.classfeatures.4uvohEHSm17wY7gc]`|
|[Skillful Lessons](classfeatures/investigator-03-dmK1wya8GBi9MmCB.htm)|Leçons accomplies|`@Compendium[pf2e.classfeatures.dmK1wya8GBi9MmCB]`|
|[Slippery Mind](classfeatures/rogue-17-xmZ7oeTDcQVXegUP.htm)|Esprit fuyant|`@Compendium[pf2e.classfeatures.xmZ7oeTDcQVXegUP]`|
|[Snake Animal Instinct](classfeatures/barbarian-01-pIYWMCNnYDQfSRQh.htm)|Instinct animal - Serpent|`@Compendium[pf2e.classfeatures.pIYWMCNnYDQfSRQh]`|
|[Sneak Attack](classfeatures/rogue-01-j1JE61quDxdge4mg.htm)|Attaque sournoise|`@Compendium[pf2e.classfeatures.j1JE61quDxdge4mg]`|
|[Sorcerer Spellcasting](classfeatures/sorcerer-01-gmnx7e1g08bppbqt.htm)|Incantation de l'ensorceleur|`@Compendium[pf2e.classfeatures.gmnx7e1g08bppbqt]`|
|[Spell Repertoire (Bard)](classfeatures/bard-01-6FsusoMYxxjyIkVh.htm)|Répertoire de sorts (Barde)|`@Compendium[pf2e.classfeatures.6FsusoMYxxjyIkVh]`|
|[Spell Repertoire (Oracle)](classfeatures/oracle-01-cFe6vFb3gSDyNeS9.htm)|Répertoire de sorts (Oracle)|`@Compendium[pf2e.classfeatures.cFe6vFb3gSDyNeS9]`|
|[Spell Repertoire (Sorcerer)](classfeatures/sorcerer-01-lURKSJZAGKVD6cH9.htm)|Répertoire de sorts (Ensorceleur)|`@Compendium[pf2e.classfeatures.lURKSJZAGKVD6cH9]`|
|[Spirit Instinct](classfeatures/barbarian-01-TQqv9Q5mB4PW6LH9.htm)|Instinct spirituel|`@Compendium[pf2e.classfeatures.TQqv9Q5mB4PW6LH9]`|
|[Storm Order](classfeatures/druid-01-acqqlYmti8D9QJi0.htm)|Tempête|`@Compendium[pf2e.classfeatures.acqqlYmti8D9QJi0]`|
|[Strategic Strike](classfeatures/investigator-01-2Fe4YZCvAr9Yf6w7.htm)|Frappe stratégique|`@Compendium[pf2e.classfeatures.2Fe4YZCvAr9Yf6w7]`|
|[Stylish Tricks](classfeatures/swashbuckler-03-pthjQIK9pDxnbER6.htm)|Astuces de style|`@Compendium[pf2e.classfeatures.pthjQIK9pDxnbER6]`|
|[Superstition Instinct](classfeatures/barbarian-01-SCYSjUbMmw8JD9P9.htm)|Instinct superstitieux|`@Compendium[pf2e.classfeatures.SCYSjUbMmw8JD9P9]`|
|[Surprise Attack](classfeatures/rogue-01-w6rMqmGzhUahdnA7.htm)|Attaque surprise|`@Compendium[pf2e.classfeatures.w6rMqmGzhUahdnA7]`|
|[Swashbuckler Expertise](classfeatures/swashbuckler-09-U74JoAcLHTOsZG6q.htm)|Expertise du bretteur|`@Compendium[pf2e.classfeatures.U74JoAcLHTOsZG6q]`|
|[Swashbuckler's Style](classfeatures/swashbuckler-01-beW1OqibVQ3fBvRw.htm)|Style du bretteur|`@Compendium[pf2e.classfeatures.beW1OqibVQ3fBvRw]`|
|[Swift Prey](classfeatures/ranger-19-bBGb1LcffXEqar0p.htm)|Changement rapide de proie|`@Compendium[pf2e.classfeatures.bBGb1LcffXEqar0p]`|
|[Tempest Mystery](classfeatures/oracle-01-W9cF7wZztLDb1WGY.htm)|Mystère de la tempête|`@Compendium[pf2e.classfeatures.W9cF7wZztLDb1WGY]`|
|[The Tenets of Evil](classfeatures/champion-01-JiY2ZB4FkK8RJm4T.htm)|Les principes du Mal|`@Compendium[pf2e.classfeatures.JiY2ZB4FkK8RJm4T]`|
|[The Tenets of Good](classfeatures/champion-01-nxZYP3KGfTSkaW6J.htm)|Les principes du bien|`@Compendium[pf2e.classfeatures.nxZYP3KGfTSkaW6J]`|
|[Thief Racket](classfeatures/rogue-01-wAh2riuFRzz0edPl.htm)|Trafic de roublard : Voleur|`@Compendium[pf2e.classfeatures.wAh2riuFRzz0edPl]`|
|[Third Path to Perfection (Fortitude)](classfeatures/monk-15-dUMsM0yDTCdV31p6.htm)|Troisième voie vers la perfection -|`@Compendium[pf2e.classfeatures.dUMsM0yDTCdV31p6]`|
|[Third Path to Perfection (Reflex)](classfeatures/monk-15-8kukH9c4h82e3qjl.htm)|Troisième voie vers la perfection - Réflexes|`@Compendium[pf2e.classfeatures.8kukH9c4h82e3qjl]`|
|[Third Path to Perfection (Will)](classfeatures/monk-15-oVNRYF0FHbH8NsJD.htm)|Troisième voie vers la perfection - Volonté|`@Compendium[pf2e.classfeatures.oVNRYF0FHbH8NsJD]`|
|[Toxicologist](classfeatures/alchemist-01-P9quO9XZi3OWFe1k.htm)|Toxicologiste|`@Compendium[pf2e.classfeatures.P9quO9XZi3OWFe1k]`|
|[Trackless Step](classfeatures/ranger-05-PeZi7E9lI4vz8EGY.htm)|Absence de traces|`@Compendium[pf2e.classfeatures.PeZi7E9lI4vz8EGY]`|
|[Tyrant](classfeatures/champion-01-HiIvez0TqESbleB5.htm)|Tyran [Loyal mauvais]|`@Compendium[pf2e.classfeatures.HiIvez0TqESbleB5]`|
|[Versatile Legend](classfeatures/fighter-19-0H2LxtiZTJ275pSD.htm)|Légende polyvalente|`@Compendium[pf2e.classfeatures.0H2LxtiZTJ275pSD]`|
|[Vigilant Senses (Level 11)](classfeatures/swashbuckler-11-NTp146fjLreL5zsj.htm)|Vigilance (Niveau 11)|`@Compendium[pf2e.classfeatures.NTp146fjLreL5zsj]`|
|[Vigilant Senses (Level 7)](classfeatures/rogue-07-0npO4rPscGm0dX13.htm)|Vigilance (Niveau 7)|`@Compendium[pf2e.classfeatures.0npO4rPscGm0dX13]`|
|[Vivacious Speed](classfeatures/swashbuckler-03-8BOFeRE7ZfJ02N0O.htm)|Vitesse exubérante|`@Compendium[pf2e.classfeatures.8BOFeRE7ZfJ02N0O]`|
|[Warden Spells](classfeatures/ranger-01-w3HysrCgDs5uFXKX.htm)|Sorts de gardien|`@Compendium[pf2e.classfeatures.w3HysrCgDs5uFXKX]`|
|[Warpriest](classfeatures/cleric-01-0Aocw3igLwna9cjp.htm)|Prêtre combattant|`@Compendium[pf2e.classfeatures.0Aocw3igLwna9cjp]`|
|[Warrior Muse](classfeatures/bard-01-N03BtRvjX9TeHRa4.htm)|Muse - Combattant|`@Compendium[pf2e.classfeatures.N03BtRvjX9TeHRa4]`|
|[Weapon Expertise (Level 11)](classfeatures/witch-11-O99eXctsEjEpuBwe.htm)|Expertise avec les armes (Niveau 11)|`@Compendium[pf2e.classfeatures.O99eXctsEjEpuBwe]`|
|[Weapon Expertise (Level 5)](classfeatures/investigator-05-9XLUh9iMepZesdmc.htm)|Expertise avec les armes (Niveau 5)|`@Compendium[pf2e.classfeatures.9XLUh9iMepZesdmc]`|
|[Weapon Expertise (Swashbuckler)](classfeatures/swashbuckler-05-F5BHEav90oOJ2LwN.htm)|Expertise martiale (Bretteur)|`@Compendium[pf2e.classfeatures.F5BHEav90oOJ2LwN]`|
|[Weapon Fury](classfeatures/barbarian-13-ejP4jVQkS48uKRFz.htm)|Fureur armée|`@Compendium[pf2e.classfeatures.ejP4jVQkS48uKRFz]`|
|[Weapon Legend](classfeatures/fighter-13-F5VenhIQMDkeGvmV.htm)|Légende martiale|`@Compendium[pf2e.classfeatures.F5VenhIQMDkeGvmV]`|
|[Weapon Mastery (Level 13)](classfeatures/swashbuckler-13-i6563IU7x4L9oRgC.htm)|Maîtrise avec les armes (Niveau 13)|`@Compendium[pf2e.classfeatures.i6563IU7x4L9oRgC]`|
|[Weapon Specialization (Barbarian)](classfeatures/barbarian-07-9EqIasqfI8YIM3Pt.htm)|Spécialisation martiale (Barbare)|`@Compendium[pf2e.classfeatures.9EqIasqfI8YIM3Pt]`|
|[Weapon Specialization (Level 13)](classfeatures/wizard-13-1NGTc0gqEtwaFqUK.htm)|Spécialisation avec les armes (Niveau 13)|`@Compendium[pf2e.classfeatures.1NGTc0gqEtwaFqUK]`|
|[Weapon Specialization (Level 7)](classfeatures/swashbuckler-07-WiM7X4xmpMx4s6LD.htm)|Spécialisation avec les armes (Niveau 7)|`@Compendium[pf2e.classfeatures.WiM7X4xmpMx4s6LD]`|
|[Weapon Tricks](classfeatures/rogue-05-v8UNEJR5IDKi8yqa.htm)|Astuces martiales|`@Compendium[pf2e.classfeatures.v8UNEJR5IDKi8yqa]`|
|[White Dragon Instinct](classfeatures/barbarian-01-2esqOHCn4GcZ4zYD.htm)|Instinct du dragon - Blanc|`@Compendium[pf2e.classfeatures.2esqOHCn4GcZ4zYD]`|
|[Wild Empathy](classfeatures/druid-01-d5BFFHXFJYKs5LXr.htm)|Empathie sauvage|`@Compendium[pf2e.classfeatures.d5BFFHXFJYKs5LXr]`|
|[Wild Order](classfeatures/druid-01-v0EjtiwdeMj8ykI0.htm)|Féroce|`@Compendium[pf2e.classfeatures.v0EjtiwdeMj8ykI0]`|
|[Wild Patron](classfeatures/witch-01-x2gzQMPvLwHWDdAC.htm)|Patron : Sauvage|`@Compendium[pf2e.classfeatures.x2gzQMPvLwHWDdAC]`|
|[Wild Stride](classfeatures/ranger-11-RlwE99yKnhq8FUuy.htm)|Déplacement facilité en milieu naturel|`@Compendium[pf2e.classfeatures.RlwE99yKnhq8FUuy]`|
|[Winter Patron](classfeatures/witch-01-qf12ubZ07Q0z0NcN.htm)|Patron : Hiver|`@Compendium[pf2e.classfeatures.qf12ubZ07Q0z0NcN]`|
|[Wit Style](classfeatures/swashbuckler-01-4lGhbEjlEoGP4scl.htm)|Style : Esprit|`@Compendium[pf2e.classfeatures.4lGhbEjlEoGP4scl]`|
|[Witch Lessons](classfeatures/witch-01-SOan0fqyFTrkqJLV.htm)|Leçons de sorcière|`@Compendium[pf2e.classfeatures.SOan0fqyFTrkqJLV]`|
|[Witch Spellcasting](classfeatures/witch-01-zT6QiTMxxj8JYoN9.htm)|Incantation de sorcière|`@Compendium[pf2e.classfeatures.zT6QiTMxxj8JYoN9]`|
|[Wizard Weapon Expertise](classfeatures/wizard-11-GBsC2cARoFiqMi9V.htm)|Expertise avec les armes du magicien|`@Compendium[pf2e.classfeatures.GBsC2cARoFiqMi9V]`|
|[Wolf Animal Instinct](classfeatures/barbarian-01-xX6KnYYgHlPGoTG6.htm)|Instinct animal - Loup|`@Compendium[pf2e.classfeatures.xX6KnYYgHlPGoTG6]`|


## Classes

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Alchemist](classes/XwfcJuskrhI9GIjX.htm)|Alchimiste|`@Compendium[pf2e.classes.XwfcJuskrhI9GIjX]`|
|[Barbarian](classes/YDRiP7uVvr9WRhOI.htm)|Barbare|`@Compendium[pf2e.classes.YDRiP7uVvr9WRhOI]`|
|[Bard](classes/3gweRQ5gn7szIWAv.htm)|Barde|`@Compendium[pf2e.classes.3gweRQ5gn7szIWAv]`|
|[Champion](classes/x8iwnpdLbfcoZkHA.htm)|Champion|`@Compendium[pf2e.classes.x8iwnpdLbfcoZkHA]`|
|[Cleric](classes/EizrWvUPMS67Pahd.htm)|Prêtre|`@Compendium[pf2e.classes.EizrWvUPMS67Pahd]`|
|[Druid](classes/7s57JDCaiYYCAdFx.htm)|Druide|`@Compendium[pf2e.classes.7s57JDCaiYYCAdFx]`|
|[Fighter](classes/8zn3cD6GSmoo1LW4.htm)|Guerrier|`@Compendium[pf2e.classes.8zn3cD6GSmoo1LW4]`|
|[Investigator](classes/4wrSCyX6akmyo7Wj.htm)|Enquêteur|`@Compendium[pf2e.classes.4wrSCyX6akmyo7Wj]`|
|[Monk](classes/YPxpk9JbMnKjbNLc.htm)|Moine|`@Compendium[pf2e.classes.YPxpk9JbMnKjbNLc]`|
|[Oracle](classes/pWHx4SXcft9O2udP.htm)|Oracle|`@Compendium[pf2e.classes.pWHx4SXcft9O2udP]`|
|[Ranger](classes/Yix76sfxrIlltSTJ.htm)|Rôdeur|`@Compendium[pf2e.classes.Yix76sfxrIlltSTJ]`|
|[Rogue](classes/LO9STvskJemPkiAI.htm)|Roublard|`@Compendium[pf2e.classes.LO9STvskJemPkiAI]`|
|[Sorcerer](classes/15Yc1r6s9CEhSTMe.htm)|Ensorceleur|`@Compendium[pf2e.classes.15Yc1r6s9CEhSTMe]`|
|[Swashbuckler](classes/uJ5aCzlw34GGdWjp.htm)|Bretteur|`@Compendium[pf2e.classes.uJ5aCzlw34GGdWjp]`|
|[Witch](classes/bYDXk9HUMKOuym9h.htm)|Sorcière|`@Compendium[pf2e.classes.bYDXk9HUMKOuym9h]`|
|[Wizard](classes/RwjIZzIxzPpUglnK.htm)|Magicien|`@Compendium[pf2e.classes.RwjIZzIxzPpUglnK]`|


## Conditions

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Blinded](conditionitems/XgEqL1kFApUbl5Z2.htm)|Aveuglé|`@Compendium[pf2e.conditionitems.XgEqL1kFApUbl5Z2]`|
|[Broken](conditionitems/6dNUvdb1dhToNDj3.htm)|Brisé|`@Compendium[pf2e.conditionitems.6dNUvdb1dhToNDj3]`|
|[Clumsy](conditionitems/i3OJZU2nk64Df3xm.htm)|Maladroit|`@Compendium[pf2e.conditionitems.i3OJZU2nk64Df3xm]`|
|[Concealed](conditionitems/DmAIPqOBomZ7H95W.htm)|Masqué|`@Compendium[pf2e.conditionitems.DmAIPqOBomZ7H95W]`|
|[Confused](conditionitems/yblD8fOR1J8rDwEQ.htm)|Confus|`@Compendium[pf2e.conditionitems.yblD8fOR1J8rDwEQ]`|
|[Controlled](conditionitems/9qGBRpbX9NEwtAAr.htm)|Contrôlé|`@Compendium[pf2e.conditionitems.9qGBRpbX9NEwtAAr]`|
|[Dazzled](conditionitems/TkIyaNPgTZFBCCuh.htm)|Ébloui|`@Compendium[pf2e.conditionitems.TkIyaNPgTZFBCCuh]`|
|[Deafened](conditionitems/9PR9y0bi4JPKnHPR.htm)|Sourd|`@Compendium[pf2e.conditionitems.9PR9y0bi4JPKnHPR]`|
|[Doomed](conditionitems/3uh1r86TzbQvosxv.htm)|Condamné|`@Compendium[pf2e.conditionitems.3uh1r86TzbQvosxv]`|
|[Drained](conditionitems/4D2KBtexWXa6oUMR.htm)|Drainé|`@Compendium[pf2e.conditionitems.4D2KBtexWXa6oUMR]`|
|[Dying](conditionitems/yZRUzMqrMmfLu0V1.htm)|Mourant|`@Compendium[pf2e.conditionitems.yZRUzMqrMmfLu0V1]`|
|[Encumbered](conditionitems/D5mg6Tc7Jzrj6ro7.htm)|Surchargé|`@Compendium[pf2e.conditionitems.D5mg6Tc7Jzrj6ro7]`|
|[Enfeebled](conditionitems/MIRkyAjyBeXivMa7.htm)|Affaibli|`@Compendium[pf2e.conditionitems.MIRkyAjyBeXivMa7]`|
|[Fascinated](conditionitems/AdPVz7rbaVSRxHFg.htm)|Fasciné|`@Compendium[pf2e.conditionitems.AdPVz7rbaVSRxHFg]`|
|[Fatigued](conditionitems/HL2l2VRSaQHu9lUw.htm)|Fatigué|`@Compendium[pf2e.conditionitems.HL2l2VRSaQHu9lUw]`|
|[Flat-Footed](conditionitems/AJh5ex99aV6VTggg.htm)|Pris au dépourvu|`@Compendium[pf2e.conditionitems.AJh5ex99aV6VTggg]`|
|[Fleeing](conditionitems/sDPxOjQ9kx2RZE8D.htm)|En fuite|`@Compendium[pf2e.conditionitems.sDPxOjQ9kx2RZE8D]`|
|[Friendly](conditionitems/v66R7FdOf11l94im.htm)|Amical|`@Compendium[pf2e.conditionitems.v66R7FdOf11l94im]`|
|[Frightened](conditionitems/TBSHQspnbcqxsmjL.htm)|Effrayé|`@Compendium[pf2e.conditionitems.TBSHQspnbcqxsmjL]`|
|[Grabbed](conditionitems/kWc1fhmv9LBiTuei.htm)|Agrippé/empoigné|`@Compendium[pf2e.conditionitems.kWc1fhmv9LBiTuei]`|
|[Helpful](conditionitems/v44P3WUcU1j0115l.htm)|Serviable|`@Compendium[pf2e.conditionitems.v44P3WUcU1j0115l]`|
|[Hidden](conditionitems/iU0fEDdBp3rXpTMC.htm)|Caché|`@Compendium[pf2e.conditionitems.iU0fEDdBp3rXpTMC]`|
|[Hostile](conditionitems/ud7gTLwPeklzYSXG.htm)|Hostile|`@Compendium[pf2e.conditionitems.ud7gTLwPeklzYSXG]`|
|[Immobilized](conditionitems/eIcWbB5o3pP6OIMe.htm)|Immobilisé|`@Compendium[pf2e.conditionitems.eIcWbB5o3pP6OIMe]`|
|[Indifferent](conditionitems/fuG8dgthlDWfWjIA.htm)|Indifférent|`@Compendium[pf2e.conditionitems.fuG8dgthlDWfWjIA]`|
|[Invisible](conditionitems/zJxUflt9np0q4yML.htm)|Invisible|`@Compendium[pf2e.conditionitems.zJxUflt9np0q4yML]`|
|[Observed](conditionitems/1wQY3JYyhMYeeV2G.htm)|Observé|`@Compendium[pf2e.conditionitems.1wQY3JYyhMYeeV2G]`|
|[Paralyzed](conditionitems/6uEgoh53GbXuHpTF.htm)|Paralysé|`@Compendium[pf2e.conditionitems.6uEgoh53GbXuHpTF]`|
|[Persistent Damage](conditionitems/lDVqvLKA6eF3Df60.htm)|Dégâts persistants|`@Compendium[pf2e.conditionitems.lDVqvLKA6eF3Df60]`|
|[Petrified](conditionitems/dTwPJuKgBQCMxixg.htm)|Pétrifié|`@Compendium[pf2e.conditionitems.dTwPJuKgBQCMxixg]`|
|[Prone](conditionitems/j91X7x0XSomq8d60.htm)|À terre|`@Compendium[pf2e.conditionitems.j91X7x0XSomq8d60]`|
|[Quickened](conditionitems/nlCjDvLMf2EkV2dl.htm)|Accéléré|`@Compendium[pf2e.conditionitems.nlCjDvLMf2EkV2dl]`|
|[Restrained](conditionitems/VcDeM8A5oI6VqhbM.htm)|Entravé|`@Compendium[pf2e.conditionitems.VcDeM8A5oI6VqhbM]`|
|[Sickened](conditionitems/fesd1n5eVhpCSS18.htm)|Malade|`@Compendium[pf2e.conditionitems.fesd1n5eVhpCSS18]`|
|[Slowed](conditionitems/xYTAsEpcJE1Ccni3.htm)|Ralenti|`@Compendium[pf2e.conditionitems.xYTAsEpcJE1Ccni3]`|
|[Stunned](conditionitems/dfCMdR4wnpbYNTix.htm)|Étourdi|`@Compendium[pf2e.conditionitems.dfCMdR4wnpbYNTix]`|
|[Stupefied](conditionitems/e1XGnhKNSQIm5IXg.htm)|Stupéfié|`@Compendium[pf2e.conditionitems.e1XGnhKNSQIm5IXg]`|
|[Unconscious](conditionitems/fBnFDH2MTzgFijKf.htm)|Inconscient|`@Compendium[pf2e.conditionitems.fBnFDH2MTzgFijKf]`|
|[Undetected](conditionitems/VRSef5y1LmL2Hkjf.htm)|Non détecté|`@Compendium[pf2e.conditionitems.VRSef5y1LmL2Hkjf]`|
|[Unfriendly](conditionitems/I1ffBVISxLr2gC4u.htm)|Inamical|`@Compendium[pf2e.conditionitems.I1ffBVISxLr2gC4u]`|
|[Unnoticed](conditionitems/9evPzg9E6muFcoSk.htm)|Inaperçu|`@Compendium[pf2e.conditionitems.9evPzg9E6muFcoSk]`|
|[Wounded](conditionitems/Yl48xTdMh3aeQYL2.htm)|Blessé|`@Compendium[pf2e.conditionitems.Yl48xTdMh3aeQYL2]`|


## Dangers

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Armageddon Orb](hazards/BsZ6o2YrwVovKfNh.htm)|Orbe de l'armageddon|`@Compendium[pf2e.hazards.BsZ6o2YrwVovKfNh]`|
|[Banshee's Symphony](hazards/mMXHyWdmmAN0GPvG.htm)|Symphonie d'outre-tombe|`@Compendium[pf2e.hazards.mMXHyWdmmAN0GPvG]`|
|[Bloodthirsty Urge](hazards/su4TRBOoso4vjkoK.htm)|Pulsion sanguinaire|`@Compendium[pf2e.hazards.su4TRBOoso4vjkoK]`|
|[Bottomless Pit](hazards/xkqjwu1ox0pQLOnb.htm)|Trappe sans fond|`@Compendium[pf2e.hazards.xkqjwu1ox0pQLOnb]`|
|[Brown Mold](hazards/Q8bXKgDm8eguqThB.htm)|Moisissure brune|`@Compendium[pf2e.hazards.Q8bXKgDm8eguqThB]`|
|[Confounding Betrayal](hazards/sFaAWmy1szDRmFtk.htm)|Trahison confondante|`@Compendium[pf2e.hazards.sFaAWmy1szDRmFtk]`|
|[Dance Of Death](hazards/ZUCGvc2dTJUlM9dC.htm)|Danse de mort|`@Compendium[pf2e.hazards.ZUCGvc2dTJUlM9dC]`|
|[Darkside Mirror](hazards/4jJxLlimyQaIVXlt.htm)|Miroir aux sombres reflets|`@Compendium[pf2e.hazards.4jJxLlimyQaIVXlt]`|
|[Drowning Pit](hazards/OekigjNLNp9XENjx.htm)|Fosse de noyade|`@Compendium[pf2e.hazards.OekigjNLNp9XENjx]`|
|[Electric Latch Rune](hazards/491qhVbjsHnOuMZW.htm)|Rune de loquet électrique|`@Compendium[pf2e.hazards.491qhVbjsHnOuMZW]`|
|[Eternal Flame](hazards/O0qA1ElCOgYGEBtL.htm)|Flamme éternelle|`@Compendium[pf2e.hazards.O0qA1ElCOgYGEBtL]`|
|[Fireball Rune](hazards/2GAOUxDfoA48uCWP.htm)|Rune de boule de feu|`@Compendium[pf2e.hazards.2GAOUxDfoA48uCWP]`|
|[Flensing Blades](hazards/g9HovYB4pfHgIML9.htm)|Lames dépeçantes|`@Compendium[pf2e.hazards.g9HovYB4pfHgIML9]`|
|[Frozen Moment](hazards/8gAoSgBJN8QqzP1R.htm)|Instant figé|`@Compendium[pf2e.hazards.8gAoSgBJN8QqzP1R]`|
|[Ghostly Choir](hazards/d3YklujLpBFC5HfB.htm)|Chœur fantômatique|`@Compendium[pf2e.hazards.d3YklujLpBFC5HfB]`|
|[Grasp Of The Damned](hazards/siylw0zIh1g4VnCW.htm)|Emprise des damnés|`@Compendium[pf2e.hazards.siylw0zIh1g4VnCW]`|
|[Green Slime](hazards/LLPsEKLoVmoPleJS.htm)|Substance gluante verte|`@Compendium[pf2e.hazards.LLPsEKLoVmoPleJS]`|
|[Hallucination Powder Trap](hazards/uEZ4Jv2wNyukJTRL.htm)|Piège à poudre hallucinogène|`@Compendium[pf2e.hazards.uEZ4Jv2wNyukJTRL]`|
|[Hammer Of Forbiddance](hazards/tbwGr6FIr5WpvQ6l.htm)|Marteau d'interdiction|`@Compendium[pf2e.hazards.tbwGr6FIr5WpvQ6l]`|
|[Hampering Web](hazards/98rS64gLzy1ReXoR.htm)|Toile ralentissante|`@Compendium[pf2e.hazards.98rS64gLzy1ReXoR]`|
|[Hidden Pit](hazards/BHq5wpQU8hQEke8D.htm)|Fosse dissimulée|`@Compendium[pf2e.hazards.BHq5wpQU8hQEke8D]`|
|[Jealous Abjurer](hazards/inUlZWE1isqnTRc5.htm)|Abjurateur jaloux|`@Compendium[pf2e.hazards.inUlZWE1isqnTRc5]`|
|[Lava Flume Tube](hazards/oNLgR1iq6MVvNRWo.htm)|Déverseur de lave|`@Compendium[pf2e.hazards.oNLgR1iq6MVvNRWo]`|
|[Perilous Flash Flood](hazards/lgyGhpyPosriQUzE.htm)|Inondation périlleuse rapide|`@Compendium[pf2e.hazards.lgyGhpyPosriQUzE]`|
|[Pharaoh's Ward](hazards/m4PRYxFq9ojcwesh.htm)|Sceau du pharaon|`@Compendium[pf2e.hazards.m4PRYxFq9ojcwesh]`|
|[Planar Rift](hazards/yM4G2LvMwvkIRx0G.htm)|Faille planaire|`@Compendium[pf2e.hazards.yM4G2LvMwvkIRx0G]`|
|[Plummeting Doom](hazards/gB9WkJtH88jJQa5Z.htm)|Chute tragique|`@Compendium[pf2e.hazards.gB9WkJtH88jJQa5Z]`|
|[Poisoned Dart Gallery](hazards/lVqVDjXnHboMif7F.htm)|Couloir de fléchettes empoisonnées|`@Compendium[pf2e.hazards.lVqVDjXnHboMif7F]`|
|[Poisoned Lock](hazards/v2xIxZ9ZZ6fJyATF.htm)|Serrure empoisonnée|`@Compendium[pf2e.hazards.v2xIxZ9ZZ6fJyATF]`|
|[Polymorph Trap](hazards/H8CPGJn81JSTCRNx.htm)|Piège de métamorphose|`@Compendium[pf2e.hazards.H8CPGJn81JSTCRNx]`|
|[Quicksand](hazards/C6nFe8SCWJ8FmLOT.htm)|Sables mouvants|`@Compendium[pf2e.hazards.C6nFe8SCWJ8FmLOT]`|
|[Scythe Blades](hazards/7VqibTAEXXX6PIhh.htm)|Lames de faux|`@Compendium[pf2e.hazards.7VqibTAEXXX6PIhh]`|
|[Second Chance](hazards/8ewUvJlvn6LVjoXJ.htm)|Seconde chance|`@Compendium[pf2e.hazards.8ewUvJlvn6LVjoXJ]`|
|[Shrieker](hazards/vTdWEBzJzltMM6r4.htm)|Criard|`@Compendium[pf2e.hazards.vTdWEBzJzltMM6r4]`|
|[Slamming Door](hazards/4O7wKZdeAemTEbvG.htm)|Claque-porte|`@Compendium[pf2e.hazards.4O7wKZdeAemTEbvG]`|
|[Snowfall](hazards/VA4VL3kVUxBYbwRf.htm)|Chute de neige|`@Compendium[pf2e.hazards.VA4VL3kVUxBYbwRf]`|
|[Spear Launcher](hazards/vlMuFskctUvjJe8X.htm)|Lance épieu|`@Compendium[pf2e.hazards.vlMuFskctUvjJe8X]`|
|[Spectral Reflection](hazards/Uw2iVgbbeyn3mOjt.htm)|Reflet spectral|`@Compendium[pf2e.hazards.Uw2iVgbbeyn3mOjt]`|
|[Spinning Blade Pillar](hazards/HnPd9Vqh5NHKEdRq.htm)|Colonne de lames tournoyantes|`@Compendium[pf2e.hazards.HnPd9Vqh5NHKEdRq]`|
|[Summoning Rune](hazards/nO4osrBRnpWKFCMP.htm)|Rune de convocation|`@Compendium[pf2e.hazards.nO4osrBRnpWKFCMP]`|
|[Telekinetic Swarm Trap](hazards/AM3YY2Zfe2ChJHd7.htm)|Piège de nuée télékinésique|`@Compendium[pf2e.hazards.AM3YY2Zfe2ChJHd7]`|
|[Titanic Flytrap](hazards/6In2S3lDnxNgZ2np.htm)|Gobe-mouche titanesque|`@Compendium[pf2e.hazards.6In2S3lDnxNgZ2np]`|
|[Treacherous Scree](hazards/gFt2nzQrVgXM9tmJ.htm)|Éboulis traître|`@Compendium[pf2e.hazards.gFt2nzQrVgXM9tmJ]`|
|[Vorpal Executioner](hazards/J4YChuob7MIPT5Mq.htm)|Bourreau vorpal|`@Compendium[pf2e.hazards.J4YChuob7MIPT5Mq]`|
|[Web Lurker Deadfall](hazards/0dg9YrjsDi6Ap3jF.htm)||`@Compendium[pf2e.hazards.0dg9YrjsDi6Ap3jF]`|
|[Web Lurker Noose](hazards/Or0jjL8xS3GyiMq0.htm)||`@Compendium[pf2e.hazards.Or0jjL8xS3GyiMq0]`|
|[Wheel Of Misery](hazards/H2GX04CQXLPQHT8h.htm)|Roue du malheur|`@Compendium[pf2e.hazards.H2GX04CQXLPQHT8h]`|
|[Yellow Mold](hazards/A93flWUsot3FmC7t.htm)|Moisissure jaune|`@Compendium[pf2e.hazards.A93flWUsot3FmC7t]`|


## Dons

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[A Home In Every Port](feats/general-11-muMOxZyduEFv8UT6.htm)|Une maison dans chaque port|`@Compendium[pf2e.feats-srd.muMOxZyduEFv8UT6]`|
|[Aasimar's Mercy](feats/ancestry-13-V6iJ4v15Qc8awVCH.htm)|Pitié de l'Aasimar|`@Compendium[pf2e.feats-srd.V6iJ4v15Qc8awVCH]`|
|[Absorb Spell](feats/archetype-14-bEdFgywri7fABhBT.htm)|Absorption de sort|`@Compendium[pf2e.feats-srd.bEdFgywri7fABhBT]`|
|[Abundant Step](feats/class-06-65gjc5KE4ZRoocbi.htm)|Pas chassé|`@Compendium[pf2e.feats-srd.65gjc5KE4ZRoocbi]`|
|[Accelerating Touch](feats/class-04-aiHbS8FGNYAQBF62.htm)|Contact accélérant|`@Compendium[pf2e.feats-srd.aiHbS8FGNYAQBF62]`|
|[Acclimatization](feats/archetype-04-uhU0KajD09h5bw4e.htm)|Acclimatation|`@Compendium[pf2e.feats-srd.uhU0KajD09h5bw4e]`|
|[Accompany](feats/class-08-oTTddwzF9TPNkMyd.htm)|Accompagnement|`@Compendium[pf2e.feats-srd.oTTddwzF9TPNkMyd]`|
|[Accurate Flurry](feats/class-20-84ML8enTqOOdXA8O.htm)|Déluge précis|`@Compendium[pf2e.feats-srd.84ML8enTqOOdXA8O]`|
|[Accursed Claws](feats/ancestry-09-KsFrWhIPVLOqxV07.htm)|Griffes maudites|`@Compendium[pf2e.feats-srd.KsFrWhIPVLOqxV07]`|
|[Accursed Clay Fist](feats/archetype-10-tMsAj0H0B9XZQjtH.htm)|Poing de glaise maudit|`@Compendium[pf2e.feats-srd.tMsAj0H0B9XZQjtH]`|
|[Achaekek's Grip](feats/archetype-08-BASCKOPvNGgoHGid.htm)|Étreinte d'Achaékek|`@Compendium[pf2e.feats-srd.BASCKOPvNGgoHGid]`|
|[Acquired Tolerance](feats/archetype-08-nx2nB10rypAuspAa.htm)|Tolérance acquise|`@Compendium[pf2e.feats-srd.nx2nB10rypAuspAa]`|
|[Acrobat Dedication](feats/archetype-02-aFygWxgSv82WyCsl.htm)|Dévouement d'Acrobate|`@Compendium[pf2e.feats-srd.aFygWxgSv82WyCsl]`|
|[Acrobatic Performer](feats/skill-01-f0faBEUPtspdutKx.htm)|Représentation acrobatique|`@Compendium[pf2e.feats-srd.f0faBEUPtspdutKx]`|
|[Acute Scent](feats/class-02-dgmjiToPHC3Yf5I3.htm)|Odorat aiguisé|`@Compendium[pf2e.feats-srd.dgmjiToPHC3Yf5I3]`|
|[Acute Vision](feats/class-01-YdpGPLN0QFTZIhbk.htm)|Vision perçante|`@Compendium[pf2e.feats-srd.YdpGPLN0QFTZIhbk]`|
|[Adapted Cantrip](feats/ancestry-01-X9tKWtQrAcmn26Nv.htm)|Tour de magie adapté|`@Compendium[pf2e.feats-srd.X9tKWtQrAcmn26Nv]`|
|[Adaptive Adept](feats/ancestry-05-DBWfPOYZaupwo3rz.htm)|Adepte de l’adaptation|`@Compendium[pf2e.feats-srd.DBWfPOYZaupwo3rz]`|
|[Adaptive Mask Familiar](feats/class-06-FkOtiB52wIOi7SP7.htm)|Familier masque adaptatif|`@Compendium[pf2e.feats-srd.FkOtiB52wIOi7SP7]`|
|[Additional Companion](feats/archetype-04-zBLrZE5aCkpaTK2N.htm)|Compagnon supplémentaire|`@Compendium[pf2e.feats-srd.zBLrZE5aCkpaTK2N]`|
|[Additional Lore](feats/skill-01-BocFD2KV0qgUC76x.htm)|Connaissances supplémentaires|`@Compendium[pf2e.feats-srd.BocFD2KV0qgUC76x]`|
|[Additional Recollection](feats/class-06-nU0r77AZXMXIlti6.htm)|Souvenirs supplémentaires|`@Compendium[pf2e.feats-srd.nU0r77AZXMXIlti6]`|
|[Additional Shadow Magic](feats/archetype-12-NM5a1tF0OW5mVYdR.htm)|Magie de l'ombre supplémentaire|`@Compendium[pf2e.feats-srd.NM5a1tF0OW5mVYdR]`|
|[Adopted Ancestry](feats/general-01-ihN8gkHSdPG9Trte.htm)|Ascendance adoptive|`@Compendium[pf2e.feats-srd.ihN8gkHSdPG9Trte]`|
|[Adrenaline Rush](feats/class-01-BBj6jrdyff7QOgjH.htm)|Montée d'adrénaline|`@Compendium[pf2e.feats-srd.BBj6jrdyff7QOgjH]`|
|[Adroit Manipulation](feats/ancestry-01-Ugw1zZStQhg6iz8h.htm)|Manipulation adroite|`@Compendium[pf2e.feats-srd.Ugw1zZStQhg6iz8h]`|
|[Advanced Arcana](feats/archetype-06-Tg3Iyq55xW9PTSW9.htm)|Arcanes avancés|`@Compendium[pf2e.feats-srd.Tg3Iyq55xW9PTSW9]`|
|[Advanced Blood Potency](feats/archetype-06-S25iRw2X9hmGRMyO.htm)|Pouvoir du sang avancé|`@Compendium[pf2e.feats-srd.S25iRw2X9hmGRMyO]`|
|[Advanced Bloodline](feats/class-06-eZrftEihfuJBldG5.htm)|Lignage avancé|`@Compendium[pf2e.feats-srd.eZrftEihfuJBldG5]`|
|[Advanced Bow Training](feats/archetype-06-7RcCvziQBLL7Bumu.htm)|Entraînement à l'arc avancé|`@Compendium[pf2e.feats-srd.7RcCvziQBLL7Bumu]`|
|[Advanced Concoction](feats/archetype-06-HtH8MONAzx4eYuJY.htm)|Concoction avancée|`@Compendium[pf2e.feats-srd.HtH8MONAzx4eYuJY]`|
|[Advanced Deduction](feats/archetype-06-L1rCuwsCKWd9zlS3.htm)|Déduction avancée|`@Compendium[pf2e.feats-srd.L1rCuwsCKWd9zlS3]`|
|[Advanced Deity's Domain](feats/class-08-JHJBmiyILzWdFRJO.htm)|Domaine avancé de la divinité|`@Compendium[pf2e.feats-srd.JHJBmiyILzWdFRJO]`|
|[Advanced Devotion](feats/archetype-06-7JjNWSuutkjrMrd0.htm)|Dévotion avancée|`@Compendium[pf2e.feats-srd.7JjNWSuutkjrMrd0]`|
|[Advanced Dogma](feats/archetype-06-tkMEdh0gM07teWkx.htm)|Dogme avancé|`@Compendium[pf2e.feats-srd.tkMEdh0gM07teWkx]`|
|[Advanced Domain](feats/class-08-uR44wELN9OlU68cL.htm)|Domaine avancé|`@Compendium[pf2e.feats-srd.uR44wELN9OlU68cL]`|
|[Advanced First Aid](feats/skill-07-17FAYfreumeKbSGr.htm)|Premiers soins avancés|`@Compendium[pf2e.feats-srd.17FAYfreumeKbSGr]`|
|[Advanced Flair](feats/archetype-06-SHhiLn0OSILEXNOj.htm)|Élégance avancée|`@Compendium[pf2e.feats-srd.SHhiLn0OSILEXNOj]`|
|[Advanced Fury](feats/archetype-06-lt9bQDI7ZXPA7wPw.htm)|Furie avancée|`@Compendium[pf2e.feats-srd.lt9bQDI7ZXPA7wPw]`|
|[Advanced General Training](feats/ancestry-13-Ht6b8H9DpA9lWzAg.htm)|Entraînement général avancé|`@Compendium[pf2e.feats-srd.Ht6b8H9DpA9lWzAg]`|
|[Advanced Hunter's Trick](feats/archetype-06-f754txt1ZyhVWXHk.htm)|Astuce du chasseur avancées|`@Compendium[pf2e.feats-srd.f754txt1ZyhVWXHk]`|
|[Advanced Kata](feats/archetype-06-TYP0Ee4o3p9LDodd.htm)|Kata avancé|`@Compendium[pf2e.feats-srd.TYP0Ee4o3p9LDodd]`|
|[Advanced Maneuver](feats/archetype-06-euWpgjPNcDjeXAWQ.htm)|Manoeuvre avancés|`@Compendium[pf2e.feats-srd.euWpgjPNcDjeXAWQ]`|
|[Advanced Muse's Whispers](feats/archetype-06-eTqWgfojpvuigdvx.htm)|Murmures de la muse avancés|`@Compendium[pf2e.feats-srd.eTqWgfojpvuigdvx]`|
|[Advanced Mysteries](feats/archetype-06-PxTRE0mFEO3tyt8h.htm)|Mystères avancés|`@Compendium[pf2e.feats-srd.PxTRE0mFEO3tyt8h]`|
|[Advanced Order Training](feats/archetype-12-KvKg9pBOpk2oLeO1.htm)|Entraînement de l'Ordre avancé|`@Compendium[pf2e.feats-srd.KvKg9pBOpk2oLeO1]`|
|[Advanced Red Mantis Magic](feats/archetype-06-bzKBUK6CH8tuLCfo.htm)|Magie avancée des Mantes rouges|`@Compendium[pf2e.feats-srd.bzKBUK6CH8tuLCfo]`|
|[Advanced Revelation](feats/class-06-FPVe3o7YctBicSQa.htm)|Révélation avancée|`@Compendium[pf2e.feats-srd.FPVe3o7YctBicSQa]`|
|[Advanced School Spell](feats/class-08-5hUj7glY8YnO5sBI.htm)|Sort d'école perfectionné|`@Compendium[pf2e.feats-srd.5hUj7glY8YnO5sBI]`|
|[Advanced Trickery](feats/archetype-06-rXY2fhyteYhaQnMl.htm)|Ruse avancée|`@Compendium[pf2e.feats-srd.rXY2fhyteYhaQnMl]`|
|[Advanced Weapon Training](feats/class-06-nDjTJq7PEbvRktnb.htm)|Formation au maniement d'armes évoluées|`@Compendium[pf2e.feats-srd.nDjTJq7PEbvRktnb]`|
|[Advanced Wilding](feats/archetype-06-l9K62T7qMCvJXUoY.htm)|Milieu sauvage avancé|`@Compendium[pf2e.feats-srd.l9K62T7qMCvJXUoY]`|
|[Advanced Witchcraft](feats/archetype-06-pIG5hWjZtzZJ3VOZ.htm)|Sorcellerie avancée|`@Compendium[pf2e.feats-srd.pIG5hWjZtzZJ3VOZ]`|
|[Advantageous Assault](feats/class-06-Y8Rdt4gHBGAUf2zL.htm)|Assaut avantageux|`@Compendium[pf2e.feats-srd.Y8Rdt4gHBGAUf2zL]`|
|[Aegis of Arnisant](feats/archetype-12-Zn2ySapQ2gtgyWgW.htm)|Égide d'Arnisant|`@Compendium[pf2e.feats-srd.Zn2ySapQ2gtgyWgW]`|
|[Aerobatics Mastery](feats/skill-07-pKnX7MGZayqZui0Z.htm)|Maître trapéziste|`@Compendium[pf2e.feats-srd.pKnX7MGZayqZui0Z]`|
|[Affliction Mercy](feats/class-12-nsFnOLqYSkGWFhLD.htm)|Soulagement d'affliction|`@Compendium[pf2e.feats-srd.nsFnOLqYSkGWFhLD]`|
|[After You](feats/class-02-3kH0fGOIoYvPNQsq.htm)|Après vous|`@Compendium[pf2e.feats-srd.3kH0fGOIoYvPNQsq]`|
|[Ageless Patience](feats/ancestry-05-k8nWKHLYvAKMuwLd.htm)|Patience infinie|`@Compendium[pf2e.feats-srd.k8nWKHLYvAKMuwLd]`|
|[Aggravating Scratch](feats/ancestry-09-G6rCbMrHacYWNu1K.htm)|Grattement aggravant|`@Compendium[pf2e.feats-srd.G6rCbMrHacYWNu1K]`|
|[Aggressive Block](feats/class-02-19OLlTrxvEtDAOHc.htm)|Blocage agressif|`@Compendium[pf2e.feats-srd.19OLlTrxvEtDAOHc]`|
|[Agile Grace](feats/class-10-BJKTUGplI9nwhJxg.htm)|Agilité gracieuse|`@Compendium[pf2e.feats-srd.BJKTUGplI9nwhJxg]`|
|[Agile Maneuvers](feats/class-06-Ba6SLqAghsZgqhua.htm)|Manœuvres agiles|`@Compendium[pf2e.feats-srd.Ba6SLqAghsZgqhua]`|
|[Agonizing Rebuke](feats/ancestry-05-7hwTeZNq6Jmzmtz4.htm)|Réprimande douloureuse|`@Compendium[pf2e.feats-srd.7hwTeZNq6Jmzmtz4]`|
|[Alchemical Crafting](feats/skill-01-is3Oz9wt11lNq62K.htm)|Artisanat alchimique|`@Compendium[pf2e.feats-srd.is3Oz9wt11lNq62K]`|
|[Alchemical Discoveries](feats/class-04-0FNLI8APwj9NsBDa.htm)|Découvertes alchimiques|`@Compendium[pf2e.feats-srd.0FNLI8APwj9NsBDa]`|
|[Alchemical Familiar](feats/class-01-loC0wIyIrsG43Zrd.htm)|Familier alchimique|`@Compendium[pf2e.feats-srd.loC0wIyIrsG43Zrd]`|
|[Alchemical Savant](feats/class-01-ypir96PNY6LBSIWT.htm)|Savant alchimiste|`@Compendium[pf2e.feats-srd.ypir96PNY6LBSIWT]`|
|[Alchemical Scholar](feats/ancestry-01-nyNsIePvpovlDAws.htm)|Étudiant alchimique|`@Compendium[pf2e.feats-srd.nyNsIePvpovlDAws]`|
|[Alchemist Dedication](feats/archetype-02-CJMkxlxHiHZQYDCz.htm)|Dévouement de l'alchimiste|`@Compendium[pf2e.feats-srd.CJMkxlxHiHZQYDCz]`|
|[Aldori Duelist Dedication](feats/archetype-02-pI97a5xSg4LbBY1g.htm)|Dévouement de Duelliste Aldori|`@Compendium[pf2e.feats-srd.pI97a5xSg4LbBY1g]`|
|[Aldori Parry](feats/archetype-04-NZgjqVV2HYzLCvvA.htm)|Parade Aldorie|`@Compendium[pf2e.feats-srd.NZgjqVV2HYzLCvvA]`|
|[Aldori Riposte](feats/archetype-06-8RppI1i4LfI0CYsX.htm)|Riposte Aldorie|`@Compendium[pf2e.feats-srd.8RppI1i4LfI0CYsX]`|
|[Align Armament](feats/class-08-xtXWw3cUnVB25XSV.htm)|Arsenal aligné|`@Compendium[pf2e.feats-srd.xtXWw3cUnVB25XSV]`|
|[Align Ki](feats/class-06-6iDd7CTzxkvMp6lB.htm)|Ki aligné|`@Compendium[pf2e.feats-srd.6iDd7CTzxkvMp6lB]`|
|[All In My Head](feats/class-18-wqAdzjRUOvTpKFKq.htm)|Tout çà est dans ma tête|`@Compendium[pf2e.feats-srd.wqAdzjRUOvTpKFKq]`|
|[Allegro](feats/class-14-rOx7r8ygmPHPC6qF.htm)|Allégro|`@Compendium[pf2e.feats-srd.rOx7r8ygmPHPC6qF]`|
|[Ally's Shelter](feats/ancestry-05-j0mlvJcuYGFuMG2S.htm)|Abri de l'allié|`@Compendium[pf2e.feats-srd.j0mlvJcuYGFuMG2S]`|
|[Ambushing Knockdown](feats/class-08-ffdXSxl4lVFrOvyQ.htm)|Embuscade assomante|`@Compendium[pf2e.feats-srd.ffdXSxl4lVFrOvyQ]`|
|[Amplifying Touch](feats/class-12-Cy5W8U4yN9P1EvBy.htm)|Toucher amplificateur|`@Compendium[pf2e.feats-srd.Cy5W8U4yN9P1EvBy]`|
|[Analyze Idiolect](feats/archetype-06-TZASOwBqVveGjw77.htm)|Analyser l'idiolecte|`@Compendium[pf2e.feats-srd.TZASOwBqVveGjw77]`|
|[Analyze Weakness](feats/class-06-3PHHiZjX16Dwyt65.htm)|Analyse de faiblesse|`@Compendium[pf2e.feats-srd.3PHHiZjX16Dwyt65]`|
|[Ancestral Blood Magic](feats/class-01-avCmIwmwJH7d7gri.htm)|Sang magique ancestral|`@Compendium[pf2e.feats-srd.avCmIwmwJH7d7gri]`|
|[Ancestral Linguistics](feats/ancestry-01-eCWQU16hRLfN1KaX.htm)|Linguistique ancestrale|`@Compendium[pf2e.feats-srd.eCWQU16hRLfN1KaX]`|
|[Ancestral Longevity](feats/ancestry-01-WoLh16gyDp8y9WOZ.htm)|Longévité ancestrale|`@Compendium[pf2e.feats-srd.WoLh16gyDp8y9WOZ]`|
|[Ancestral Mage](feats/class-10-YNk0BekymS3bBvCT.htm)|Mage ancestral|`@Compendium[pf2e.feats-srd.YNk0BekymS3bBvCT]`|
|[Ancestral Paragon](feats/general-03-m7KjpkAAh9PptJsY.htm)|Parangon ancestral|`@Compendium[pf2e.feats-srd.m7KjpkAAh9PptJsY]`|
|[Ancestral Suspicion](feats/ancestry-05-scNrNhnGTgPIzoj7.htm)|Suspicion ancestrale|`@Compendium[pf2e.feats-srd.scNrNhnGTgPIzoj7]`|
|[Ancestral Weaponry](feats/class-02-VQz5VypVRLCloapa.htm)|Armes ancestrales|`@Compendium[pf2e.feats-srd.VQz5VypVRLCloapa]`|
|[Anchoring Aura](feats/class-14-b5K067Pma4Il9IeD.htm)|Aura d'ancrage|`@Compendium[pf2e.feats-srd.b5K067Pma4Il9IeD]`|
|[Angel Of Death](feats/archetype-10-3poGYUYCBTmbeCUs.htm)|Ange de la mort|`@Compendium[pf2e.feats-srd.3poGYUYCBTmbeCUs]`|
|[Angelic Magic](feats/ancestry-09-esKk5XrnlqRayDPG.htm)|Magie des anges|`@Compendium[pf2e.feats-srd.esKk5XrnlqRayDPG]`|
|[Angelkin](feats/ancestry-01-ydgCsYsgqSkFWEDK.htm)|Type angélique|`@Compendium[pf2e.feats-srd.ydgCsYsgqSkFWEDK]`|
|[Animal Accomplice](feats/ancestry-01-ED58GzldWb82yc2q.htm)|Complice animal|`@Compendium[pf2e.feats-srd.ED58GzldWb82yc2q]`|
|[Animal Companion](feats/class-01-f2Pl5dWEL9ZvEyI1.htm)|Compagnon animal (Druide)|`@Compendium[pf2e.feats-srd.f2Pl5dWEL9ZvEyI1]`|
|[Animal Elocutionist](feats/ancestry-05-evwCimenReYvcruj.htm)|Orateur animalier|`@Compendium[pf2e.feats-srd.evwCimenReYvcruj]`|
|[Animal Feature](feats/class-04-it2i6OXfGIizokpg.htm)|Trait animal|`@Compendium[pf2e.feats-srd.it2i6OXfGIizokpg]`|
|[Animal Rage](feats/class-08-1VLOhyq0IFMY2rqh.htm)|Rage animale|`@Compendium[pf2e.feats-srd.1VLOhyq0IFMY2rqh]`|
|[Animal Skin](feats/class-06-ZPclfDmiHzEqblry.htm)|Cuir animal|`@Compendium[pf2e.feats-srd.ZPclfDmiHzEqblry]`|
|[Animal Trainer Dedication](feats/archetype-02-m3ANSHYfBrFyFUvo.htm)|Dévouement de Dompteur|`@Compendium[pf2e.feats-srd.m3ANSHYfBrFyFUvo]`|
|[Ankle Bite](feats/ancestry-05-PncXj46fgSwTWRl6.htm)|Morsure à la cheville|`@Compendium[pf2e.feats-srd.PncXj46fgSwTWRl6]`|
|[Annihilating Swing](feats/class-20-7QLLwcSKNGPWdOGG.htm)|Coup annihilateur|`@Compendium[pf2e.feats-srd.7QLLwcSKNGPWdOGG]`|
|[Annotate Composition](feats/class-10-PIVC14saumGNKWbo.htm)|Composition annotée|`@Compendium[pf2e.feats-srd.PIVC14saumGNKWbo]`|
|[Anoint Ally](feats/class-02-bRftzbFvSF1pilIo.htm)|Oindre un allié|`@Compendium[pf2e.feats-srd.bRftzbFvSF1pilIo]`|
|[Antagonize](feats/class-02-Wx12NUjqTOjFrEoW.htm)|Contrarier|`@Compendium[pf2e.feats-srd.Wx12NUjqTOjFrEoW]`|
|[Anticipate Ambush](feats/class-06-5N6rLz4mdJg0NrQH.htm)|Embuscade anticipée|`@Compendium[pf2e.feats-srd.5N6rLz4mdJg0NrQH]`|
|[Apex Companion](feats/class-20-p353WH847errsNvh.htm)|Apogée du compagnon|`@Compendium[pf2e.feats-srd.p353WH847errsNvh]`|
|[Arcane Breadth](feats/archetype-08-DwU6CmK4KsH8A3hu.htm)|Amplitude arcanique|`@Compendium[pf2e.feats-srd.DwU6CmK4KsH8A3hu]`|
|[Arcane Evolution](feats/class-04-qxh4evekG28Gt1vj.htm)|Évolution arcanique|`@Compendium[pf2e.feats-srd.qxh4evekG28Gt1vj]`|
|[Arcane School Spell](feats/archetype-04-QrShJGrvmWPBj4oN.htm)|Sort d'école arcanique|`@Compendium[pf2e.feats-srd.QrShJGrvmWPBj4oN]`|
|[Arcane Sense](feats/skill-01-blMeVamjGz4ODWxq.htm)|Sens arcanique|`@Compendium[pf2e.feats-srd.blMeVamjGz4ODWxq]`|
|[Arcane Sensitivity](feats/archetype-06-z8bozNJvUjBoKLPA.htm)||`@Compendium[pf2e.feats-srd.z8bozNJvUjBoKLPA]`|
|[Arcane Tattoos](feats/ancestry-01-Dq4JSejEdCzGNeTc.htm)|Tatouages arcaniques|`@Compendium[pf2e.feats-srd.Dq4JSejEdCzGNeTc]`|
|[Archaeologist Dedication](feats/archetype-02-PRKe5rWYZMZgEpFU.htm)|Dévouement d'archéologue|`@Compendium[pf2e.feats-srd.PRKe5rWYZMZgEpFU]`|
|[Archaeologist's Luck](feats/archetype-08-dmXd68ilbuGR6eUP.htm)|Chance de l'archéologue|`@Compendium[pf2e.feats-srd.dmXd68ilbuGR6eUP]`|
|[Archaeologist's Warning](feats/archetype-06-7O0PrMoXd5L8dRfg.htm)|Avertissement de l'archéologue|`@Compendium[pf2e.feats-srd.7O0PrMoXd5L8dRfg]`|
|[Archer Dedication](feats/archetype-02-4QFElZoWjg1X0vsg.htm)|Dévouement de l'Archer|`@Compendium[pf2e.feats-srd.4QFElZoWjg1X0vsg]`|
|[Archer's Aim](feats/archetype-08-egmb8p3ZIYtx5aQN.htm)|Visée de l'Archer|`@Compendium[pf2e.feats-srd.egmb8p3ZIYtx5aQN]`|
|[Archon Magic](feats/ancestry-09-1WheVs50iwMBi6KC.htm)|Magie des archons|`@Compendium[pf2e.feats-srd.1WheVs50iwMBi6KC]`|
|[Archwizard's Might](feats/class-20-tP26mgaFPpr6df1i.htm)|Puissance de l'archimage|`@Compendium[pf2e.feats-srd.tP26mgaFPpr6df1i]`|
|[Ardent Armiger](feats/archetype-04-iKOcwFCbNX1a2OFT.htm)|Fervent écuyer|`@Compendium[pf2e.feats-srd.iKOcwFCbNX1a2OFT]`|
|[Armiger's Mobility](feats/archetype-08-Olx796SgBbHUFeHc.htm)|Mobilité de l'Écuyer|`@Compendium[pf2e.feats-srd.Olx796SgBbHUFeHc]`|
|[Armor Assist](feats/skill-01-Lc4dJZivRwU3QEmT.htm)|Aide armure|`@Compendium[pf2e.feats-srd.Lc4dJZivRwU3QEmT]`|
|[Armor Proficiency](feats/general-01-BStw1cANwx5baL6d.htm)|Maîtrise d’armure|`@Compendium[pf2e.feats-srd.BStw1cANwx5baL6d]`|
|[Armor Rune Shifter](feats/archetype-10-81xlcIFJIAtsmXmd.htm)|Changerune d'armure|`@Compendium[pf2e.feats-srd.81xlcIFJIAtsmXmd]`|
|[Armor Specialist](feats/archetype-06-7NdMHszAiiveihoW.htm)|Spécialiste de l'armure|`@Compendium[pf2e.feats-srd.7NdMHszAiiveihoW]`|
|[Armored Exercise](feats/archetype-14-9zfcIXDG2mDpiypp.htm)|Exercice en armure|`@Compendium[pf2e.feats-srd.9zfcIXDG2mDpiypp]`|
|[Armored Rebuff](feats/archetype-08-jwcNyPDVw313KXZU.htm)|Repousser avec l'armure|`@Compendium[pf2e.feats-srd.jwcNyPDVw313KXZU]`|
|[Armored Rest](feats/archetype-10-OmjTt8eR1Q3SmkPp.htm)|Repos en armure|`@Compendium[pf2e.feats-srd.OmjTt8eR1Q3SmkPp]`|
|[Armored Stealth](feats/skill-02-lB9MVGCJr7aJQuIH.htm)|Discrétion en armure|`@Compendium[pf2e.feats-srd.lB9MVGCJr7aJQuIH]`|
|[Arrow Snatching](feats/class-08-dTPVRVzfVBlBUV2l.htm)|Capture de projectiles|`@Compendium[pf2e.feats-srd.dTPVRVzfVBlBUV2l]`|
|[Arrow of Death](feats/archetype-18-6iOLxitjqHujH1Tj.htm)|Flèche de mort|`@Compendium[pf2e.feats-srd.6iOLxitjqHujH1Tj]`|
|[Artokus's Fire](feats/class-06-iJrHJKNGxV4z4Qi7.htm)|Feu d'Artokus|`@Compendium[pf2e.feats-srd.iJrHJKNGxV4z4Qi7]`|
|[Assassin Dedication](feats/archetype-02-iBmqKjsq4iTtoqvl.htm)|Dévouement d'Assassin|`@Compendium[pf2e.feats-srd.iBmqKjsq4iTtoqvl]`|
|[Assassinate](feats/archetype-12-iJxbrXAdxhLqdT5E.htm)|Assassinat|`@Compendium[pf2e.feats-srd.iJxbrXAdxhLqdT5E]`|
|[Assisting Shot](feats/class-02-UiQbjeqBUFjUtgUR.htm)|Tir de soutien|`@Compendium[pf2e.feats-srd.UiQbjeqBUFjUtgUR]`|
|[Assurance](feats/skill-01-W6Gl9ePmItfDHji0.htm)|Assurance|`@Compendium[pf2e.feats-srd.W6Gl9ePmItfDHji0]`|
|[Assured Identification](feats/skill-02-XmF4q4rzKWg55vG4.htm)|Identification assurée|`@Compendium[pf2e.feats-srd.XmF4q4rzKWg55vG4]`|
|[Assured Knowledge](feats/class-06-c6CS97Zs0DPmInaI.htm)|Connaissance assurée|`@Compendium[pf2e.feats-srd.c6CS97Zs0DPmInaI]`|
|[Assured Ritualist](feats/archetype-10-v482u7QboZEbhgvv.htm)|Ritualiste assuré|`@Compendium[pf2e.feats-srd.v482u7QboZEbhgvv]`|
|[Astonishing Explosion](feats/class-20-HEZeZcBWQR1QeWDo.htm)|Explosion époustouflante|`@Compendium[pf2e.feats-srd.HEZeZcBWQR1QeWDo]`|
|[Athletic Might](feats/ancestry-05-Wwr8VilSybQgVtin.htm)|Puissance athlétique|`@Compendium[pf2e.feats-srd.Wwr8VilSybQgVtin]`|
|[Athletic Strategist](feats/class-02-Tu1hOEr6Ko9Df54L.htm)|Stratège athlétique|`@Compendium[pf2e.feats-srd.Tu1hOEr6Ko9Df54L]`|
|[Attack of Opportunity](feats/class-06-NMWXHGWUcZGoLDKb.htm)|Attaque d'Opportunité|`@Compendium[pf2e.feats-srd.NMWXHGWUcZGoLDKb]`|
|[Attack of Opportunity (Fighter)](feats/class-01-O2MwufzEHbuThoGr.htm)|Attaque d’opportunité (Guerrier)|`@Compendium[pf2e.feats-srd.O2MwufzEHbuThoGr]`|
|[Attunement to Stone](feats/archetype-10-nDNmqez9McmrjBAV.htm)|Harmonisation avec la pierre|`@Compendium[pf2e.feats-srd.nDNmqez9McmrjBAV]`|
|[Aura Of Despair](feats/class-04-wjnfdh6WzN7HbmeE.htm)|Aura de désespoir|`@Compendium[pf2e.feats-srd.wjnfdh6WzN7HbmeE]`|
|[Aura Of Preservation](feats/class-14-z7kwVNaCB4oJs3Fe.htm)|Aura de préservation|`@Compendium[pf2e.feats-srd.z7kwVNaCB4oJs3Fe]`|
|[Aura of Courage](feats/class-04-TIwk07T0OxSbcOpJ.htm)|Aura de courage|`@Compendium[pf2e.feats-srd.TIwk07T0OxSbcOpJ]`|
|[Aura of Faith](feats/class-12-7sFhBYoz5GSBFNbY.htm)|Aura de foi|`@Compendium[pf2e.feats-srd.7sFhBYoz5GSBFNbY]`|
|[Aura of Life](feats/class-14-YawVDUc9uzREIAnO.htm)|Aura de vie|`@Compendium[pf2e.feats-srd.YawVDUc9uzREIAnO]`|
|[Aura of Righteousness](feats/class-14-erCOcFZJPT2O3gwC.htm)|Aura de vertu|`@Compendium[pf2e.feats-srd.erCOcFZJPT2O3gwC]`|
|[Aura of Unbreakable Virtue](feats/class-20-evMhKTjzdiuDKwMX.htm)|Aura de vertu indéfectible|`@Compendium[pf2e.feats-srd.evMhKTjzdiuDKwMX]`|
|[Aura of Vengeance](feats/class-14-Ice8oNOTbPFXyOww.htm)|Aura de vengeance|`@Compendium[pf2e.feats-srd.Ice8oNOTbPFXyOww]`|
|[Auspicious Mount](feats/class-16-uixN9wbfe0veOHRn.htm)|Monture de bon augure|`@Compendium[pf2e.feats-srd.uixN9wbfe0veOHRn]`|
|[Automatic Knowledge](feats/skill-02-H3I2X0f7v4EzwxuN.htm)|Savoir automatique|`@Compendium[pf2e.feats-srd.H3I2X0f7v4EzwxuN]`|
|[Avalanche Strike](feats/archetype-16-SGgK4BoUooA0HhTj.htm)|Frappe avalanche|`@Compendium[pf2e.feats-srd.SGgK4BoUooA0HhTj]`|
|[Avatar's Audience](feats/class-20-opeP0JF9WGmNG0pb.htm)|Audience de l'avatar|`@Compendium[pf2e.feats-srd.opeP0JF9WGmNG0pb]`|
|[Avenge Ally](feats/ancestry-13-LTcpBbnngfuYTdB0.htm)|Venger un allié|`@Compendium[pf2e.feats-srd.LTcpBbnngfuYTdB0]`|
|[Avenge in Glory](feats/ancestry-01-gKDRnsBPBdhJB0FI.htm)|Revanche dans la gloire|`@Compendium[pf2e.feats-srd.gKDRnsBPBdhJB0FI]`|
|[Awesome Blow](feats/class-14-rCnaBbk0M1gBVHjG.htm)|Coup fabuleux|`@Compendium[pf2e.feats-srd.rCnaBbk0M1gBVHjG]`|
|[Axe Climber](feats/archetype-04-nAgAICjPd4BSQlAj.htm)|Escalade à la hache|`@Compendium[pf2e.feats-srd.nAgAICjPd4BSQlAj]`|
|[Axe Thrower](feats/archetype-06-pewPAMlURmTqBqJx.htm)|Lanceur de haches|`@Compendium[pf2e.feats-srd.pewPAMlURmTqBqJx]`|
|[Azaersi's Roads](feats/class-17-LoZY48txEgHZxewj.htm)|Routes d'Azaersi|`@Compendium[pf2e.feats-srd.LoZY48txEgHZxewj]`|
|[Azata Magic](feats/ancestry-09-4hFeaF4MlqIHb6gb.htm)|Magie des azatas|`@Compendium[pf2e.feats-srd.4hFeaF4MlqIHb6gb]`|
|[Back To Back](feats/archetype-08-E7eceezD3NDmBVBb.htm)|Dos à dos|`@Compendium[pf2e.feats-srd.E7eceezD3NDmBVBb]`|
|[Backup Disguise](feats/skill-02-7F3sTNRoNsQgD8tX.htm)|Déguisement de secours|`@Compendium[pf2e.feats-srd.7F3sTNRoNsQgD8tX]`|
|[Banishing Blow](feats/class-20-dwloLQzWgwjJWzXt.htm)|Coup bannissant|`@Compendium[pf2e.feats-srd.dwloLQzWgwjJWzXt]`|
|[Barbarian Dedication](feats/archetype-02-WVU0c8rgcpGSRqSi.htm)|Dévouement de barbare|`@Compendium[pf2e.feats-srd.WVU0c8rgcpGSRqSi]`|
|[Barbarian Resiliency](feats/archetype-04-HpT1GlcnkCBnDnVF.htm)|Résilience du barbare|`@Compendium[pf2e.feats-srd.HpT1GlcnkCBnDnVF]`|
|[Bard Dedication](feats/archetype-02-dIH771mt4PcVTyAs.htm)|Dévouement du barde|`@Compendium[pf2e.feats-srd.dIH771mt4PcVTyAs]`|
|[Bardic Lore](feats/class-01-uVXEZblPRuCyPRua.htm)|Connaissance bardique|`@Compendium[pf2e.feats-srd.uVXEZblPRuCyPRua]`|
|[Bargain Hunter](feats/skill-01-5nc5ridFBfYpn2Om.htm)|Chasseur de bonnes affaires|`@Compendium[pf2e.feats-srd.5nc5ridFBfYpn2Om]`|
|[Bark and Tendril](feats/ancestry-09-2kAyZ3LB28BDhXKa.htm)|Écorce et vrille|`@Compendium[pf2e.feats-srd.2kAyZ3LB28BDhXKa]`|
|[Barreling Charge](feats/class-04-to6s7QanfhHukW5r.htm)|Charge irrépressible|`@Compendium[pf2e.feats-srd.to6s7QanfhHukW5r]`|
|[Bashing Charge](feats/class-02-A4sV0cRU9I8ztbHY.htm)|Charge destructrice|`@Compendium[pf2e.feats-srd.A4sV0cRU9I8ztbHY]`|
|[Basic Arcana](feats/archetype-04-DBsqWivnSaEo8jz5.htm)|Arcanes basiques|`@Compendium[pf2e.feats-srd.DBsqWivnSaEo8jz5]`|
|[Basic Bard Spellcasting](feats/archetype-04-NSyzjkDdQU2A75mX.htm)|Incantation du barde basique|`@Compendium[pf2e.feats-srd.NSyzjkDdQU2A75mX]`|
|[Basic Blood Potency](feats/archetype-04-nT9Z4OPDTOg2AGYc.htm)|Pouvoir du sang basique|`@Compendium[pf2e.feats-srd.nT9Z4OPDTOg2AGYc]`|
|[Basic Bloodline Spell](feats/archetype-04-wBqQsXzqObrZM9Va.htm)|Sort de lignage basique|`@Compendium[pf2e.feats-srd.wBqQsXzqObrZM9Va]`|
|[Basic Cleric Spellcasting](feats/archetype-04-gc24C5CyWgqn1Lbl.htm)|Incantation du prêtre basique|`@Compendium[pf2e.feats-srd.gc24C5CyWgqn1Lbl]`|
|[Basic Concoction](feats/archetype-04-h5ZT9i79BFVJ0VfE.htm)|Concoction basique|`@Compendium[pf2e.feats-srd.h5ZT9i79BFVJ0VfE]`|
|[Basic Deduction](feats/archetype-04-X0NFLIn1bqj6bnd0.htm)|Déduction basique|`@Compendium[pf2e.feats-srd.X0NFLIn1bqj6bnd0]`|
|[Basic Devotion](feats/archetype-04-uKBT0D9gxdwMcwNl.htm)|Dévotion basique|`@Compendium[pf2e.feats-srd.uKBT0D9gxdwMcwNl]`|
|[Basic Dogma](feats/archetype-04-7Pb1WL8abrPBTPrH.htm)|Dogme basique|`@Compendium[pf2e.feats-srd.7Pb1WL8abrPBTPrH]`|
|[Basic Druid Spellcasting](feats/archetype-04-PNG7e39mEhq1MorG.htm)|Incantation du Druide basique|`@Compendium[pf2e.feats-srd.PNG7e39mEhq1MorG]`|
|[Basic Eldritch Archer Spellcasting](feats/archetype-08-5MG4dBTsFZVbHcX7.htm)|Incantation basique de l'Archer mystique|`@Compendium[pf2e.feats-srd.5MG4dBTsFZVbHcX7]`|
|[Basic Flair](feats/archetype-04-sE7x3QAel4VGdkgn.htm)|Élégance basique|`@Compendium[pf2e.feats-srd.sE7x3QAel4VGdkgn]`|
|[Basic Fury](feats/archetype-04-Sr6CcCXceV8ALAmB.htm)|Furie basique|`@Compendium[pf2e.feats-srd.Sr6CcCXceV8ALAmB]`|
|[Basic Hunter's Trick](feats/archetype-04-mV911W6MTGMvHPbE.htm)|Astuce du chasseur basique|`@Compendium[pf2e.feats-srd.mV911W6MTGMvHPbE]`|
|[Basic Kata](feats/archetype-04-BridkNEysTuSOOLM.htm)|Kata basique|`@Compendium[pf2e.feats-srd.BridkNEysTuSOOLM]`|
|[Basic Lesson](feats/class-02-wsq8nncD25Q1fRn2.htm)|Leçon basique|`@Compendium[pf2e.feats-srd.wsq8nncD25Q1fRn2]`|
|[Basic Maneuver](feats/archetype-04-baD02AcIpU7xUBlD.htm)|Manœuvre basique|`@Compendium[pf2e.feats-srd.baD02AcIpU7xUBlD]`|
|[Basic Muse's Whispers](feats/archetype-04-JM0umAUx30mAkuTz.htm)|Murmures de la muse basiques|`@Compendium[pf2e.feats-srd.JM0umAUx30mAkuTz]`|
|[Basic Mysteries](feats/archetype-04-NdgMxlz5I1ddT0Zi.htm)|Mystères basiques|`@Compendium[pf2e.feats-srd.NdgMxlz5I1ddT0Zi]`|
|[Basic Oracle Spellcasting](feats/archetype-04-7ycF0fgSw1ovUPit.htm)|Incantation de l'Oracle basique|`@Compendium[pf2e.feats-srd.7ycF0fgSw1ovUPit]`|
|[Basic Red Mantis Magic](feats/archetype-04-6RjilN93bgy34y3H.htm)|Magie basique des Mantes rouges|`@Compendium[pf2e.feats-srd.6RjilN93bgy34y3H]`|
|[Basic Scroll Cache](feats/archetype-06-ROAUR1GhC19Pjk9C.htm)|Réserve basique de parchemins|`@Compendium[pf2e.feats-srd.ROAUR1GhC19Pjk9C]`|
|[Basic Sorcerer Spellcasting](feats/archetype-04-AvfswILmh3BwNbyR.htm)|Incantation de l'ensorceleur basique|`@Compendium[pf2e.feats-srd.AvfswILmh3BwNbyR]`|
|[Basic Trickery](feats/archetype-04-xMTC8UOerwGyCtaN.htm)|Ruse basique|`@Compendium[pf2e.feats-srd.xMTC8UOerwGyCtaN]`|
|[Basic Wilding](feats/archetype-04-kUa8PBjKmBk04zmc.htm)|Milieu sauvage basique|`@Compendium[pf2e.feats-srd.kUa8PBjKmBk04zmc]`|
|[Basic Witch Spellcasting](feats/archetype-04-nU5Pow4HMzoDHa8Z.htm)|Incantation basique de la Sorcière|`@Compendium[pf2e.feats-srd.nU5Pow4HMzoDHa8Z]`|
|[Basic Witchcraft](feats/archetype-04-geESDWQVvwScyPph.htm)|Sorcellerie basique|`@Compendium[pf2e.feats-srd.geESDWQVvwScyPph]`|
|[Basic Wizard Spellcasting](feats/archetype-04-SHUfHzElHZPXJFiP.htm)|Incantation du magicien basique|`@Compendium[pf2e.feats-srd.SHUfHzElHZPXJFiP]`|
|[Bastion Dedication](feats/archetype-02-s6wXcQYmHbew14gC.htm)|Dévouement de Bastion|`@Compendium[pf2e.feats-srd.s6wXcQYmHbew14gC]`|
|[Battle Assessment](feats/class-04-B6jXVgfPuPWWLx2K.htm)|Evaluation martiale|`@Compendium[pf2e.feats-srd.B6jXVgfPuPWWLx2K]`|
|[Battle Cry](feats/skill-07-ePObIpaJDgDb9CQj.htm)|Cri de guerre|`@Compendium[pf2e.feats-srd.ePObIpaJDgDb9CQj]`|
|[Battle Medicine](feats/skill-01-wYerMk6F1RZb0Fwt.htm)|Médecine militaire|`@Compendium[pf2e.feats-srd.wYerMk6F1RZb0Fwt]`|
|[Battle Medicine (Forensic Medicine)](feats/skill-01-f4HG7vKj4WGxlqG2.htm)|Médecine militaire (Médecine forensique)|`@Compendium[pf2e.feats-srd.f4HG7vKj4WGxlqG2]`|
|[Battle Planner](feats/skill-02-qvLcZGsV0HP2O0CG.htm)|Planificateur de bataille|`@Compendium[pf2e.feats-srd.qvLcZGsV0HP2O0CG]`|
|[Battle Prayer](feats/skill-07-nBlzWZnmYuFHrMyV.htm)|Prière de bataille|`@Compendium[pf2e.feats-srd.nBlzWZnmYuFHrMyV]`|
|[Battleforger](feats/ancestry-09-mnH68QcFRtkMbNE0.htm)|forgeron de bataille|`@Compendium[pf2e.feats-srd.mnH68QcFRtkMbNE0]`|
|[Beast Speaker](feats/archetype-04-J8BSHRkmP3QLknwF.htm)|Locuteur bestial|`@Compendium[pf2e.feats-srd.J8BSHRkmP3QLknwF]`|
|[Beast Trainer](feats/ancestry-01-wtTye8OrC9cuK7YP.htm)|Dresseur|`@Compendium[pf2e.feats-srd.wtTye8OrC9cuK7YP]`|
|[Beastmaster Bond](feats/archetype-10-3R09Hl6IDMgPcSs0.htm)|Lien du Maître des bêtes|`@Compendium[pf2e.feats-srd.3R09Hl6IDMgPcSs0]`|
|[Beastmaster Dedication](feats/archetype-02-wdmUH6hdat7jpEWt.htm)|Dévouement de Maître des bêtes|`@Compendium[pf2e.feats-srd.wdmUH6hdat7jpEWt]`|
|[Beastmaster's Call](feats/archetype-12-5kUQuIP8N57MXhuz.htm)|Appel du Maître des bêtes|`@Compendium[pf2e.feats-srd.5kUQuIP8N57MXhuz]`|
|[Beastmaster's Trance](feats/archetype-06-Ckglzh4dXcGWPNS3.htm)|Transe du Maître des bêtes|`@Compendium[pf2e.feats-srd.Ckglzh4dXcGWPNS3]`|
|[Bellflower Dedication](feats/archetype-06-dwL0Y0P2x4pn2cft.htm)|Dévouement de la Campanule|`@Compendium[pf2e.feats-srd.dwL0Y0P2x4pn2cft]`|
|[Bespell Weapon](feats/class-04-tWBK7Zbt80JlPryC.htm)|Arme enchantée|`@Compendium[pf2e.feats-srd.tWBK7Zbt80JlPryC]`|
|[Bestiary Scholar](feats/archetype-10-ADgQzThbtGKvp6hy.htm)|Bestiaire de l'érudit|`@Compendium[pf2e.feats-srd.ADgQzThbtGKvp6hy]`|
|[Between The Scales](feats/ancestry-09-zL6zUrt44tZYyuh4.htm)|Entre les écailles|`@Compendium[pf2e.feats-srd.zL6zUrt44tZYyuh4]`|
|[Big Mouth](feats/ancestry-09-cCv9pgyXzZ0TQmZg.htm)|Large bouche|`@Compendium[pf2e.feats-srd.cCv9pgyXzZ0TQmZg]`|
|[Biographical Eye](feats/skill-07-WqTqHPDbargixuej.htm)|Œil biographique|`@Compendium[pf2e.feats-srd.WqTqHPDbargixuej]`|
|[Bizarre Magic](feats/skill-07-9o2VSlMQVPB4LN09.htm)|Magie bizarre|`@Compendium[pf2e.feats-srd.9o2VSlMQVPB4LN09]`|
|[Black Cat Curse](feats/ancestry-13-fX5FybM93HIQRRd1.htm)|Malédiction du chat noir|`@Compendium[pf2e.feats-srd.fX5FybM93HIQRRd1]`|
|[Blade of Justice](feats/class-12-NtaOLg9meDYfg8aV.htm)|Lame de justice|`@Compendium[pf2e.feats-srd.NtaOLg9meDYfg8aV]`|
|[Blade of Law](feats/archetype-12-tBfalnbUZLkG9gs1.htm)|Lame de la Loi|`@Compendium[pf2e.feats-srd.tBfalnbUZLkG9gs1]`|
|[Blade of the Crimson Oath](feats/archetype-12-WO23FvCo8IYVB5RF.htm)|Lame du Serment écarlate|`@Compendium[pf2e.feats-srd.WO23FvCo8IYVB5RF]`|
|[Blank Slate](feats/class-16-05k4nkjazjjEUoGu.htm)|Page blanche|`@Compendium[pf2e.feats-srd.05k4nkjazjjEUoGu]`|
|[Blaze Of Revelation](feats/class-18-nLidn7L2z61Ktjzk.htm)|Brasier de révélation|`@Compendium[pf2e.feats-srd.nLidn7L2z61Ktjzk]`|
|[Blazing Talon Surge](feats/class-06-XYaaj872JOO9CAws.htm)|Déferlement d'ergots enflammés|`@Compendium[pf2e.feats-srd.XYaaj872JOO9CAws]`|
|[Bleeding Finisher](feats/class-08-dxujgA0NgiEvA0H8.htm)|Botte hémorragique|`@Compendium[pf2e.feats-srd.dxujgA0NgiEvA0H8]`|
|[Bless Tonic](feats/skill-07-UHyoXbp8O6idQ6ee.htm)|Tonique béni|`@Compendium[pf2e.feats-srd.UHyoXbp8O6idQ6ee]`|
|[Bless Toxin](feats/skill-07-dz1rHYk3n9pUFfgm.htm)|Toxine bénie|`@Compendium[pf2e.feats-srd.dz1rHYk3n9pUFfgm]`|
|[Blessed Blood (Aasimar)](feats/ancestry-05-BuaOQzmRNgIhGsfN.htm)|Sang béni (Aasimar)|`@Compendium[pf2e.feats-srd.BuaOQzmRNgIhGsfN]`|
|[Blessed Blood (Sorcerer)](feats/class-01-82ATEfDMPkZDxV5H.htm)|Sang béni (Ensorceleur)|`@Compendium[pf2e.feats-srd.82ATEfDMPkZDxV5H]`|
|[Blessed Denial](feats/archetype-12-kKoqqXOTdRYROmVV.htm)|Déni de l'Élu|`@Compendium[pf2e.feats-srd.kKoqqXOTdRYROmVV]`|
|[Blessed One Dedication](feats/archetype-02-eBdajOzs8kiJDic2.htm)|Dévouement d'Élu divin|`@Compendium[pf2e.feats-srd.eBdajOzs8kiJDic2]`|
|[Blessed Sacrifice](feats/archetype-04-NrCloSukzSUCprsM.htm)|Sacrifice de l'Élu divin|`@Compendium[pf2e.feats-srd.NrCloSukzSUCprsM]`|
|[Blessed Spell](feats/archetype-08-V3nNlrdU2OxYJAjx.htm)|Sort de l'Élu divin|`@Compendium[pf2e.feats-srd.V3nNlrdU2OxYJAjx]`|
|[Blessings of the Five](feats/bonus-00-cMkpLgadlLCzDOv0.htm)||`@Compendium[pf2e.feats-srd.cMkpLgadlLCzDOv0]`|
|[Blind-Fight](feats/class-08-y2XeMe1F18lIyo59.htm)|Combat en aveugle|`@Compendium[pf2e.feats-srd.y2XeMe1F18lIyo59]`|
|[Blood Component Substitution](feats/class-12-va7YMidXZW21oFwA.htm)|Substitution d'une composante sanguine|`@Compendium[pf2e.feats-srd.va7YMidXZW21oFwA]`|
|[Bloodletting Fangs](feats/ancestry-09-YDHr12qVA3XRjkLP.htm)|Saignée de Crocs|`@Compendium[pf2e.feats-srd.YDHr12qVA3XRjkLP]`|
|[Bloodline Breadth](feats/archetype-08-UfuYHdozZD586RWd.htm)|Amplitude de lignage|`@Compendium[pf2e.feats-srd.UfuYHdozZD586RWd]`|
|[Bloodline Conduit](feats/class-20-AmfO4FHmfFr0oNi9.htm)|Conduit du lignage|`@Compendium[pf2e.feats-srd.AmfO4FHmfFr0oNi9]`|
|[Bloodline Focus](feats/class-12-zSTPB1FFWMfA1JPi.htm)|Focalisation du lignage|`@Compendium[pf2e.feats-srd.zSTPB1FFWMfA1JPi]`|
|[Bloodline Metamorphosis](feats/class-20-GIKySPq1n7xUmICw.htm)|Métamorphose du lignage|`@Compendium[pf2e.feats-srd.GIKySPq1n7xUmICw]`|
|[Bloodline Mutation](feats/class-20-N1ajKcWRo3O0oMQg.htm)|Mutation du lignage|`@Compendium[pf2e.feats-srd.N1ajKcWRo3O0oMQg]`|
|[Bloodline Perfection](feats/class-20-6SEDoht4dXEJE5SW.htm)|Perfection du lignage|`@Compendium[pf2e.feats-srd.6SEDoht4dXEJE5SW]`|
|[Bloodline Resistance](feats/class-08-zf6Poru1jNmrO3kk.htm)|Résistance du lignage|`@Compendium[pf2e.feats-srd.zf6Poru1jNmrO3kk]`|
|[Bloodline Wellspring](feats/class-18-yUuvixlhM4mcjKMb.htm)|Source du lignage|`@Compendium[pf2e.feats-srd.yUuvixlhM4mcjKMb]`|
|[Bloody Blows](feats/ancestry-05-JvTSfyCkG70bmY7f.htm)|Coups sanglants|`@Compendium[pf2e.feats-srd.JvTSfyCkG70bmY7f]`|
|[Bloody Debilitation](feats/class-12-QicYF43HqgpOBLzo.htm)|Handicap sanglant|`@Compendium[pf2e.feats-srd.QicYF43HqgpOBLzo]`|
|[Boaster's Challenge](feats/archetype-06-UtUT6JngJbQRHySX.htm)|Défi de vantardise|`@Compendium[pf2e.feats-srd.UtUT6JngJbQRHySX]`|
|[Bolera's Interrogation](feats/archetype-06-cBYTVYqw1EFVEuzs.htm)||`@Compendium[pf2e.feats-srd.cBYTVYqw1EFVEuzs]`|
|[Bon Mot](feats/skill-01-0GF2j54roPFIDmXf.htm)|Bon mot|`@Compendium[pf2e.feats-srd.0GF2j54roPFIDmXf]`|
|[Bond Conservation](feats/class-08-vhHKUooXX3PYqGaU.htm)|Conservation du lien|`@Compendium[pf2e.feats-srd.vhHKUooXX3PYqGaU]`|
|[Bonded Animal](feats/skill-02-pLjvgeqwHrYdg411.htm)|Animal lié|`@Compendium[pf2e.feats-srd.pLjvgeqwHrYdg411]`|
|[Bonded Focus](feats/class-14-QC2ecMZ57MRJlxco.htm)|Focalisateur lié|`@Compendium[pf2e.feats-srd.QC2ecMZ57MRJlxco]`|
|[Boneyard's Call](feats/ancestry-17-wfOGyLnuVMFo7Rwy.htm)|Appel du Cimetière|`@Compendium[pf2e.feats-srd.wfOGyLnuVMFo7Rwy]`|
|[Boulder Roll](feats/ancestry-05-ZZxePfQkBPuTHkt1.htm)|Pas du roc|`@Compendium[pf2e.feats-srd.ZZxePfQkBPuTHkt1]`|
|[Bounce Back](feats/ancestry-13-MR4X38qgBj5tmkMw.htm)|Rebondir|`@Compendium[pf2e.feats-srd.MR4X38qgBj5tmkMw]`|
|[Bouncy Goblin](feats/ancestry-01-AlhyF8LCW011w9kq.htm)|Gobelin rebondissant|`@Compendium[pf2e.feats-srd.AlhyF8LCW011w9kq]`|
|[Boundless Reprisals](feats/class-20-IMArawT1Sc2PTcYM.htm)|Représailles illimitées|`@Compendium[pf2e.feats-srd.IMArawT1Sc2PTcYM]`|
|[Bounty Hunter Dedication](feats/archetype-02-tKaesXO5nlZ2sspx.htm)|Dévouement de chasseur de primes|`@Compendium[pf2e.feats-srd.tKaesXO5nlZ2sspx]`|
|[Bravo's Determination](feats/archetype-08-YTZhLWtrEnV9Pjf2.htm)|Détermination du bravache|`@Compendium[pf2e.feats-srd.YTZhLWtrEnV9Pjf2]`|
|[Brawling Focus](feats/class-02-VeVHWTrzE3aDm3rx.htm)|Focalisation du lutteur|`@Compendium[pf2e.feats-srd.VeVHWTrzE3aDm3rx]`|
|[Breath Control](feats/general-01-aJUXbe9HJVvv0Mxa.htm)|Contrôle de la respiration|`@Compendium[pf2e.feats-srd.aJUXbe9HJVvv0Mxa]`|
|[Breath Of The Dragon](feats/archetype-08-Sr75bQtqmCM6dyAM.htm)|Souffle de dragon|`@Compendium[pf2e.feats-srd.Sr75bQtqmCM6dyAM]`|
|[Bright Lion Dedication](feats/archetype-02-rJ1XxUEACaQA9Lyo.htm)|Dévouement de Lion radieux|`@Compendium[pf2e.feats-srd.rJ1XxUEACaQA9Lyo]`|
|[Brightness Seeker](feats/ancestry-09-8aRa9VHoDKl9B1Z1.htm)|Aspirant à l'illumination|`@Compendium[pf2e.feats-srd.8aRa9VHoDKl9B1Z1]`|
|[Brine May](feats/ancestry-01-jEJ6AWCctirMT7p0.htm)|Vierge de saumure|`@Compendium[pf2e.feats-srd.jEJ6AWCctirMT7p0]`|
|[Brutal Beating](feats/class-02-DYUlMxh7TDgEh9xB.htm)|Raclée brutale|`@Compendium[pf2e.feats-srd.DYUlMxh7TDgEh9xB]`|
|[Brutal Bully](feats/class-06-oUhwrijg4rClCplO.htm)|Cogneur brutal|`@Compendium[pf2e.feats-srd.oUhwrijg4rClCplO]`|
|[Brutal Critical](feats/class-18-oNHmfpe8ezZ3eKDD.htm)|Critique brutal|`@Compendium[pf2e.feats-srd.oNHmfpe8ezZ3eKDD]`|
|[Brutal Finish](feats/class-12-tGXJU6yx7bYuyLvd.htm)|Final brutal|`@Compendium[pf2e.feats-srd.tGXJU6yx7bYuyLvd]`|
|[Brutish Shove](feats/class-02-gt9pvIicabSOe6pB.htm)|Poussée brutale|`@Compendium[pf2e.feats-srd.gt9pvIicabSOe6pB]`|
|[Buckler Dance](feats/class-10-tDWc2LQNl0Op1Auq.htm)|Danse de la targe|`@Compendium[pf2e.feats-srd.tDWc2LQNl0Op1Auq]`|
|[Buckler Expertise](feats/class-01-C5tSxOwDILefw4zq.htm)|Expertise de la targe|`@Compendium[pf2e.feats-srd.C5tSxOwDILefw4zq]`|
|[Bullseye](feats/class-08-0XGLdVbEIISOOuuO.htm)|Dans le mille|`@Compendium[pf2e.feats-srd.0XGLdVbEIISOOuuO]`|
|[Bullying Staff](feats/archetype-04-iMh9Ve8Kt8ZcdKU0.htm)|Bâton harcelant|`@Compendium[pf2e.feats-srd.iMh9Ve8Kt8ZcdKU0]`|
|[Burn It!](feats/ancestry-01-sJ7WTLDwAbIA9Elc.htm)|Brûle !|`@Compendium[pf2e.feats-srd.sJ7WTLDwAbIA9Elc]`|
|[Burrow Elocutionist](feats/ancestry-01-48X3xSWxI20RPOr9.htm)|Orateur des terriers|`@Compendium[pf2e.feats-srd.48X3xSWxI20RPOr9]`|
|[Cackle](feats/class-01-0BR61rW4JFOfO7T7.htm)|Gloussement|`@Compendium[pf2e.feats-srd.0BR61rW4JFOfO7T7]`|
|[Cadence Call](feats/archetype-06-a9zzu4kb7vstq0HQ.htm)|Imprimer la cadence|`@Compendium[pf2e.feats-srd.a9zzu4kb7vstq0HQ]`|
|[Calculated Splash](feats/class-04-UjWLK86BgHxz3Itd.htm)|Éclaboussure calculée|`@Compendium[pf2e.feats-srd.UjWLK86BgHxz3Itd]`|
|[Call And Response](feats/class-08-JOq4Xe49A04YycRz.htm)|Appel et réponse|`@Compendium[pf2e.feats-srd.JOq4Xe49A04YycRz]`|
|[Call Bonded Item](feats/class-04-9j90iE61ZToFR8cu.htm)|Appeler l'objet lié|`@Compendium[pf2e.feats-srd.9j90iE61ZToFR8cu]`|
|[Call Your Shot](feats/archetype-08-SfBXPmADMFiZIBQt.htm)|Annoncer votre coup|`@Compendium[pf2e.feats-srd.SfBXPmADMFiZIBQt]`|
|[Call of the Wild](feats/class-02-ZvPgibovxwiN8Wse.htm)|Appel de la nature|`@Compendium[pf2e.feats-srd.ZvPgibovxwiN8Wse]`|
|[Called](feats/ancestry-05-2vlQ09QIBli5u9Gz.htm)|Appelé|`@Compendium[pf2e.feats-srd.2vlQ09QIBli5u9Gz]`|
|[Callow May](feats/ancestry-01-IFvjnLMw3ht8f84U.htm)|Vierge novice|`@Compendium[pf2e.feats-srd.IFvjnLMw3ht8f84U]`|
|[Camouflage](feats/class-10-qqe5VmcO1s8iTOfd.htm)|Camouflage|`@Compendium[pf2e.feats-srd.qqe5VmcO1s8iTOfd]`|
|[Canny Acumen](feats/general-01-i4yRvVwvXbGZDsD1.htm)|Perspicacité astucieuse|`@Compendium[pf2e.feats-srd.i4yRvVwvXbGZDsD1]`|
|[Cantrip Expansion](feats/class-02-A7ofsoPva0UtjqrX.htm)|Expansion de tour de magie|`@Compendium[pf2e.feats-srd.A7ofsoPva0UtjqrX]`|
|[Caravan Leader](feats/general-11-5ZsmRm7HvFAw2XDZ.htm)|Chef de caravane|`@Compendium[pf2e.feats-srd.5ZsmRm7HvFAw2XDZ]`|
|[Careful Explorer](feats/archetype-04-qQh8wnslOagixxD1.htm)|Explorateur vigilant|`@Compendium[pf2e.feats-srd.qQh8wnslOagixxD1]`|
|[Cartwheel Dodge](feats/archetype-12-lmAuoHPxzQdaSUmN.htm)|Esquive des chars|`@Compendium[pf2e.feats-srd.lmAuoHPxzQdaSUmN]`|
|[Cascade Bearer's Flexibility](feats/class-06-YOU5eCD5S4cS6Qeu.htm)|Flexibilité des Détenteurs des flots|`@Compendium[pf2e.feats-srd.YOU5eCD5S4cS6Qeu]`|
|[Cascade Bearer's Spellcasting](feats/archetype-10-V5FKAmcYQF4KRELG.htm)|Incantation des Détenteurs des flots|`@Compendium[pf2e.feats-srd.V5FKAmcYQF4KRELG]`|
|[Cast Down](feats/class-06-lGCFVYjL9Lp5m9Ex.htm)|Incantation renversante|`@Compendium[pf2e.feats-srd.lGCFVYjL9Lp5m9Ex]`|
|[Castigating Weapon](feats/class-10-tSmd0cxq9wokSCh4.htm)|Arme Punitive|`@Compendium[pf2e.feats-srd.tSmd0cxq9wokSCh4]`|
|[Cat Fall](feats/skill-01-LQw0yIMDUJJkq1nD.htm)|Chute féline|`@Compendium[pf2e.feats-srd.LQw0yIMDUJJkq1nD]`|
|[Cat's Luck](feats/ancestry-01-7qBuzHY8kEG8SdEP.htm)|Chance féline|`@Compendium[pf2e.feats-srd.7qBuzHY8kEG8SdEP]`|
|[Caterwaul](feats/ancestry-13-nePEcAp7lTL35uyx.htm)|Feuler|`@Compendium[pf2e.feats-srd.nePEcAp7lTL35uyx]`|
|[Catfolk Lore](feats/ancestry-01-eqBWab1J5Be24YAl.htm)|Connaissance des Hommes-félins|`@Compendium[pf2e.feats-srd.eqBWab1J5Be24YAl]`|
|[Catfolk Weapon Expertise](feats/ancestry-13-dgpoTae18H4zc9fH.htm)|Expertise avec les armes des hommes-félins|`@Compendium[pf2e.feats-srd.dgpoTae18H4zc9fH]`|
|[Catfolk Weapon Familiarity](feats/ancestry-01-bEh5qUgX5eFaQwzU.htm)|Familiarité avec les armes des hommes-félins|`@Compendium[pf2e.feats-srd.bEh5qUgX5eFaQwzU]`|
|[Catfolk Weapon Rake](feats/ancestry-05-8Mo0fKRAz1TVbrr1.htm)|Armes griffantes des hommes-félins|`@Compendium[pf2e.feats-srd.8Mo0fKRAz1TVbrr1]`|
|[Cauldron](feats/class-01-zUtdBd3IbM7UX0AD.htm)|Chaudron|`@Compendium[pf2e.feats-srd.zUtdBd3IbM7UX0AD]`|
|[Cautious Curiosity](feats/ancestry-09-ZPK2Un5ChzeNc9Dx.htm)|Curiosité précautionneuse|`@Compendium[pf2e.feats-srd.ZPK2Un5ChzeNc9Dx]`|
|[Cautious Delver](feats/archetype-10-EpWgrMznGbm8gceW.htm)|Fouilleur précautionneux|`@Compendium[pf2e.feats-srd.EpWgrMznGbm8gceW]`|
|[Cavalier Dedication](feats/archetype-02-5Q69PI8jdQVkb1ZT.htm)|Dévouement de Cavalier|`@Compendium[pf2e.feats-srd.5Q69PI8jdQVkb1ZT]`|
|[Cavalier's Banner](feats/archetype-04-CAaXGhHDMRM3Pt4J.htm)|Bannière du Cavalier|`@Compendium[pf2e.feats-srd.CAaXGhHDMRM3Pt4J]`|
|[Cavalier's Charge](feats/archetype-04-Vab3XIirjs3KQh3t.htm)|Charge du cavalier|`@Compendium[pf2e.feats-srd.Vab3XIirjs3KQh3t]`|
|[Cave Climber](feats/ancestry-09-bo4JG09pkoS7ywSZ.htm)|Grimpeur des cavernes|`@Compendium[pf2e.feats-srd.bo4JG09pkoS7ywSZ]`|
|[Ceaseless Shadows](feats/ancestry-13-PfHvTR0L9ShjhWiP.htm)|Ombres incessantes|`@Compendium[pf2e.feats-srd.PfHvTR0L9ShjhWiP]`|
|[Celebrity Dedication](feats/archetype-02-MNArjo5Z5LUYITQm.htm)|Dévouement de Célébrité|`@Compendium[pf2e.feats-srd.MNArjo5Z5LUYITQm]`|
|[Celestial Eyes](feats/ancestry-01-akCO62yBCJLjjCZJ.htm)|Yeux célestes|`@Compendium[pf2e.feats-srd.akCO62yBCJLjjCZJ]`|
|[Celestial Form](feats/class-18-VAbfepohLNtubfi3.htm)|Forme Céleste|`@Compendium[pf2e.feats-srd.VAbfepohLNtubfi3]`|
|[Celestial Lore](feats/ancestry-01-sKDCoxMz2yKWLGRJ.htm)|Connaissance céleste|`@Compendium[pf2e.feats-srd.sKDCoxMz2yKWLGRJ]`|
|[Celestial Mount](feats/class-20-AXXlinOc2lq08NPH.htm)|Monture céleste|`@Compendium[pf2e.feats-srd.AXXlinOc2lq08NPH]`|
|[Celestial Resistance](feats/ancestry-05-8CSewrCVEmL8sjnk.htm)|Résistance céleste|`@Compendium[pf2e.feats-srd.8CSewrCVEmL8sjnk]`|
|[Celestial Strikes](feats/ancestry-13-Ot1jI91ccnB2ayfk.htm)|Frappes célestes|`@Compendium[pf2e.feats-srd.Ot1jI91ccnB2ayfk]`|
|[Celestial Wings](feats/ancestry-09-vtCrMziYxNyj8kP7.htm)|Ailes célestes|`@Compendium[pf2e.feats-srd.vtCrMziYxNyj8kP7]`|
|[Celestial Word](feats/ancestry-17-KL3Pk10ReItAHTw9.htm)|Parole céleste|`@Compendium[pf2e.feats-srd.KL3Pk10ReItAHTw9]`|
|[Certain Strike](feats/class-10-h5DzKmKDADGhvmF9.htm)|Frappe Certaine|`@Compendium[pf2e.feats-srd.h5DzKmKDADGhvmF9]`|
|[Champion Dedication](feats/archetype-02-0YjetnLVIUz9zQt5.htm)|Dévouement du Champion|`@Compendium[pf2e.feats-srd.0YjetnLVIUz9zQt5]`|
|[Champion Resiliency](feats/archetype-04-qWb5IxkBUpJWKSLf.htm)|Résilence du champion|`@Compendium[pf2e.feats-srd.qWb5IxkBUpJWKSLf]`|
|[Champion's Reaction](feats/archetype-06-7wk6yVM3OJdc4LEU.htm)|Réaction de champion|`@Compendium[pf2e.feats-srd.7wk6yVM3OJdc4LEU]`|
|[Champion's Sacrifice](feats/class-12-U4AoJMBhJaFq5O1S.htm)|Sacrifice du Champion|`@Compendium[pf2e.feats-srd.U4AoJMBhJaFq5O1S]`|
|[Changeling Lore](feats/ancestry-01-4mLlHpD41101H0Iy.htm)|Connaissance des Changelins|`@Compendium[pf2e.feats-srd.4mLlHpD41101H0Iy]`|
|[Channel Smite](feats/class-04-o5q9FBrPsAYqEl5w.htm)|Châtiment canalisé|`@Compendium[pf2e.feats-srd.o5q9FBrPsAYqEl5w]`|
|[Channeled Succor](feats/class-08-0PcVi7eav6PMLOPl.htm)|Secours canalisé|`@Compendium[pf2e.feats-srd.0PcVi7eav6PMLOPl]`|
|[Charged Creation](feats/archetype-12-ADVrDjp75fr2BYMa.htm)|Création chargée|`@Compendium[pf2e.feats-srd.ADVrDjp75fr2BYMa]`|
|[Charlatan](feats/skill-01-lQs2i9L09MQiZSPC.htm)|Charlatan|`@Compendium[pf2e.feats-srd.lQs2i9L09MQiZSPC]`|
|[Charmed Life](feats/class-02-DkoxNw9tsFFXrfJY.htm)|Vie chanceuse|`@Compendium[pf2e.feats-srd.DkoxNw9tsFFXrfJY]`|
|[Charming Liar](feats/skill-01-B6HbYsLBWb1RR6Fx.htm)|Menteur charismatique|`@Compendium[pf2e.feats-srd.B6HbYsLBWb1RR6Fx]`|
|[Cheat Death](feats/class-12-D2KSVHPRlBEibrV8.htm)|Trompe la mort|`@Compendium[pf2e.feats-srd.D2KSVHPRlBEibrV8]`|
|[Cheek Pouches](feats/ancestry-01-Pox93XMBaFmeLIDM.htm)|Abajoues|`@Compendium[pf2e.feats-srd.Pox93XMBaFmeLIDM]`|
|[Chemical Contagion](feats/class-16-IA8p9oYmsCbipmhw.htm)|Contagion chimique|`@Compendium[pf2e.feats-srd.IA8p9oYmsCbipmhw]`|
|[Chosen of Lamashtu](feats/ancestry-05-eVn6hBjNjTB4liKw.htm)|Élu de Lamashtu|`@Compendium[pf2e.feats-srd.eVn6hBjNjTB4liKw]`|
|[City Scavenger](feats/ancestry-01-g388ImzpenYBoiEF.htm)|Charognard urbain|`@Compendium[pf2e.feats-srd.g388ImzpenYBoiEF]`|
|[Clan Protector](feats/ancestry-05-ya9YlhUA4WspUlHB.htm)|Protecteur de clan|`@Compendium[pf2e.feats-srd.ya9YlhUA4WspUlHB]`|
|[Clan's Edge](feats/ancestry-01-X7wFUWjYjYhzpejU.htm)|Avantage du clan|`@Compendium[pf2e.feats-srd.X7wFUWjYjYhzpejU]`|
|[Claws Of The Dragon](feats/archetype-04-uMjczqcXuteoP7lf.htm)|Griffes de dragon|`@Compendium[pf2e.feats-srd.uMjczqcXuteoP7lf]`|
|[Clear The Way](feats/archetype-06-goFxIDlbWd8GN0kj.htm)|Nettoyer le passage|`@Compendium[pf2e.feats-srd.goFxIDlbWd8GN0kj]`|
|[Cleave](feats/class-06-GE96a0UGPYM74qjI.htm)|Coup tranchant|`@Compendium[pf2e.feats-srd.GE96a0UGPYM74qjI]`|
|[Cleric Dedication](feats/archetype-02-smCDaPlpRDA47xjK.htm)|Dévouement du prêtre|`@Compendium[pf2e.feats-srd.smCDaPlpRDA47xjK]`|
|[Clever Counterspell](feats/class-12-IMPP5pa8AmvCby4W.htm)|Contresort astucieux|`@Compendium[pf2e.feats-srd.IMPP5pa8AmvCby4W]`|
|[Clever Gambit](feats/class-02-D1o7GUraoFFzjaub.htm)|Manœuvre intelligente|`@Compendium[pf2e.feats-srd.D1o7GUraoFFzjaub]`|
|[Clever Improviser](feats/ancestry-05-KcbSxOPYC5CUqbZQ.htm)|Improvisateur astucieux|`@Compendium[pf2e.feats-srd.KcbSxOPYC5CUqbZQ]`|
|[Climbing Claws](feats/ancestry-05-TlMSleKR8Bh0EuSW.htm)|Griffes du grimpeur|`@Compendium[pf2e.feats-srd.TlMSleKR8Bh0EuSW]`|
|[Cling](feats/ancestry-09-SiedJ6hnDLEGeeBj.htm)|Cramponné|`@Compendium[pf2e.feats-srd.SiedJ6hnDLEGeeBj]`|
|[Clinging Shadows Initiate](feats/class-08-QkKMile0qqmuVY67.htm)|Initié des ombres tenaces|`@Compendium[pf2e.feats-srd.QkKMile0qqmuVY67]`|
|[Cloud Jump](feats/skill-15-TVFfTP9fHRidwBlW.htm)|Saut plané|`@Compendium[pf2e.feats-srd.TVFfTP9fHRidwBlW]`|
|[Cloud Step](feats/class-16-r7FGPKl5e0xB4tuj.htm)|Pas nuageux|`@Compendium[pf2e.feats-srd.r7FGPKl5e0xB4tuj]`|
|[Clue Them All In](feats/class-08-264KzmKMK4zqi6AR.htm)|Mise en commun des indices|`@Compendium[pf2e.feats-srd.264KzmKMK4zqi6AR]`|
|[Cobble Dancer](feats/ancestry-13-MII0ybm76DkDxsId.htm)|Danseur des pavés|`@Compendium[pf2e.feats-srd.MII0ybm76DkDxsId]`|
|[Cobra Envenom](feats/class-10-xQuNswWB3eg1UM28.htm)|Posture du cobra|`@Compendium[pf2e.feats-srd.xQuNswWB3eg1UM28]`|
|[Cobra Stance](feats/class-04-AkV4Jyllo6nlK2Sl.htm)|Posture du Cobra|`@Compendium[pf2e.feats-srd.AkV4Jyllo6nlK2Sl]`|
|[Cognitive Loophole](feats/class-16-wPJFEUOXwf7y5jN3.htm)|Échappatoire cognitive|`@Compendium[pf2e.feats-srd.wPJFEUOXwf7y5jN3]`|
|[Collateral Thrash](feats/class-16-WY7CjISdz6uwXwIb.htm)|Maltraitance collatérale|`@Compendium[pf2e.feats-srd.WY7CjISdz6uwXwIb]`|
|[Combat Assessment](feats/class-01-5FyvwI24mnROzh61.htm)|Évaluation martiale|`@Compendium[pf2e.feats-srd.5FyvwI24mnROzh61]`|
|[Combat Climber](feats/skill-01-09PurtIanNUPfNRq.htm)|Combattant-grimpeur|`@Compendium[pf2e.feats-srd.09PurtIanNUPfNRq]`|
|[Combat Grab](feats/class-02-dF1HR6TqyMz03o6F.htm)|Empoignade en combat|`@Compendium[pf2e.feats-srd.dF1HR6TqyMz03o6F]`|
|[Combat Reading](feats/class-04-kqW6d3Dfk4nApd7y.htm)|Lecture du combat|`@Compendium[pf2e.feats-srd.kqW6d3Dfk4nApd7y]`|
|[Combat Reflexes](feats/class-10-rzaoi5Roef9zO22G.htm)|Attaque réflexe|`@Compendium[pf2e.feats-srd.rzaoi5Roef9zO22G]`|
|[Combination Finisher](feats/class-06-7KT4huf0iPaBGD7R.htm)|Botte combinée|`@Compendium[pf2e.feats-srd.7KT4huf0iPaBGD7R]`|
|[Combine Elixirs](feats/class-06-jSkJIWPfSZZzvYzq.htm)|Combiner des élixirs|`@Compendium[pf2e.feats-srd.jSkJIWPfSZZzvYzq]`|
|[Come and Get Me](feats/class-10-Z9gzWFk3qom2z904.htm)|Je t'attends|`@Compendium[pf2e.feats-srd.Z9gzWFk3qom2z904]`|
|[Command Attention](feats/archetype-10-N0gJ4Q69nslbdXHg.htm)|Attirer l'attention|`@Compendium[pf2e.feats-srd.N0gJ4Q69nslbdXHg]`|
|[Command Undead](feats/class-04-xYakFeP6olBsxpZN.htm)|Contrôle des morts-vivants|`@Compendium[pf2e.feats-srd.xYakFeP6olBsxpZN]`|
|[Communal Healing](feats/class-02-yExxOkHN1PN37hUa.htm)|Guérison collective|`@Compendium[pf2e.feats-srd.yExxOkHN1PN37hUa]`|
|[Communal Sustain](feats/archetype-12-a2wXdQHiIoj3lHoe.htm)|Maintien commun|`@Compendium[pf2e.feats-srd.a2wXdQHiIoj3lHoe]`|
|[Companion's Cry](feats/class-04-TORYSZMLMAGgsSEW.htm)|Appel au compagnon|`@Compendium[pf2e.feats-srd.TORYSZMLMAGgsSEW]`|
|[Conceal Spell](feats/class-02-sIeuPW0j39fTZm08.htm)|Sort dissimulé|`@Compendium[pf2e.feats-srd.sIeuPW0j39fTZm08]`|
|[Concealing Legerdemain](feats/skill-01-B9cQLRHtXoLlF0iR.htm)|Tour de passe-passe|`@Compendium[pf2e.feats-srd.B9cQLRHtXoLlF0iR]`|
|[Conceited Mindset](feats/class-02-aXdY2wgn0ItWwTr0.htm)|Esprit vaniteux|`@Compendium[pf2e.feats-srd.aXdY2wgn0ItWwTr0]`|
|[Confabulator](feats/skill-02-6Z4e3B9vePYpibcy.htm)|Affabulateur|`@Compendium[pf2e.feats-srd.6Z4e3B9vePYpibcy]`|
|[Connect The Dots](feats/class-06-gwIgB6bMh0sruyX7.htm)|Relier les points|`@Compendium[pf2e.feats-srd.gwIgB6bMh0sruyX7]`|
|[Connections](feats/skill-02-iCv8KTZ5PcF4GqeV.htm)|Relations|`@Compendium[pf2e.feats-srd.iCv8KTZ5PcF4GqeV]`|
|[Consult The Spirits](feats/skill-07-fvYwsHM9O1twQa5N.htm)|Consulter les esprits|`@Compendium[pf2e.feats-srd.fvYwsHM9O1twQa5N]`|
|[Consume Spell](feats/class-14-d8yggbcJsKKyHip7.htm)|Consommer un sort|`@Compendium[pf2e.feats-srd.d8yggbcJsKKyHip7]`|
|[Contagious Rage](feats/class-20-nJe8eQUrIpKWLXh5.htm)|Rage contagieuse|`@Compendium[pf2e.feats-srd.nJe8eQUrIpKWLXh5]`|
|[Continual Recovery](feats/skill-02-c85a69mB1urW2Se2.htm)|Récupération continue|`@Compendium[pf2e.feats-srd.c85a69mB1urW2Se2]`|
|[Contortionist](feats/archetype-04-JO3mcFvxjRp1V8XK.htm)|Contortionniste|`@Compendium[pf2e.feats-srd.JO3mcFvxjRp1V8XK]`|
|[Convincing Illusion](feats/class-06-bSXcyu7ExWq9qUzG.htm)|Illusion convaincante|`@Compendium[pf2e.feats-srd.bSXcyu7ExWq9qUzG]`|
|[Cooperative Nature](feats/ancestry-01-lwLcUHQMOqfaNND4.htm)|Nature coopérative|`@Compendium[pf2e.feats-srd.lwLcUHQMOqfaNND4]`|
|[Cooperative Soul](feats/ancestry-09-Z2EiYMtGoNnwW6Tk.htm)|Âme coopérative|`@Compendium[pf2e.feats-srd.Z2EiYMtGoNnwW6Tk]`|
|[Coordinated Charge](feats/archetype-12-KYF9e4oeSjHKgbwM.htm)|Charge Coordonnée|`@Compendium[pf2e.feats-srd.KYF9e4oeSjHKgbwM]`|
|[Coordinated Distraction](feats/class-12-XyQpqznuO5LGFvhz.htm)|Diversion coordonnée|`@Compendium[pf2e.feats-srd.XyQpqznuO5LGFvhz]`|
|[Corrupted Shield](feats/class-06-uH3ZRkXPsXi1ChO2.htm)|Bouclier corrompu|`@Compendium[pf2e.feats-srd.uH3ZRkXPsXi1ChO2]`|
|[Counter Perform](feats/archetype-06-t5zeg3m9rEnWnYXY.htm)|Contre-représentation|`@Compendium[pf2e.feats-srd.t5zeg3m9rEnWnYXY]`|
|[Counterspell (Prepared)](feats/class-01-EpBG4CFMNSZQx7vI.htm)|Contresort (Préparé)|`@Compendium[pf2e.feats-srd.EpBG4CFMNSZQx7vI]`|
|[Counterspell (Spontaneous)](feats/class-01-deoHKUzpzT7iwWhL.htm)|Contresort (Spontané)|`@Compendium[pf2e.feats-srd.deoHKUzpzT7iwWhL]`|
|[Counterspell (Witch)](feats/class-01-Ikqay8buYlb85bwd.htm)|Contresort (Sorcière)|`@Compendium[pf2e.feats-srd.Ikqay8buYlb85bwd]`|
|[Courageous Advance](feats/class-04-sv3ywEHaab9oZ3Nj.htm)|Progression vaillante|`@Compendium[pf2e.feats-srd.sv3ywEHaab9oZ3Nj]`|
|[Courageous Assault](feats/class-10-Asb0UsQqeATsxqFJ.htm)|Assaut vaillant|`@Compendium[pf2e.feats-srd.Asb0UsQqeATsxqFJ]`|
|[Courageous Onslaught](feats/class-16-8C9qo5LL9J0UTNGc.htm)|Charge vaillante|`@Compendium[pf2e.feats-srd.8C9qo5LL9J0UTNGc]`|
|[Courageous Opportunity](feats/class-08-nEmaHLsZEBru1Jjv.htm)|Opportunité vaillante|`@Compendium[pf2e.feats-srd.nEmaHLsZEBru1Jjv]`|
|[Courteous Comeback](feats/ancestry-01-DwnnmTNOvpLbp7jJ.htm)|Retour courtois|`@Compendium[pf2e.feats-srd.DwnnmTNOvpLbp7jJ]`|
|[Courtly Graces](feats/skill-01-JtjnFsOToBLnSRO9.htm)|Manières courtoises|`@Compendium[pf2e.feats-srd.JtjnFsOToBLnSRO9]`|
|[Craft Anything](feats/skill-15-v62QzTwHOT3t86cL.htm)|Fabrication facilitée|`@Compendium[pf2e.feats-srd.v62QzTwHOT3t86cL]`|
|[Craft Facsimile](feats/archetype-06-xLD9EmkEmT8hgwv7.htm)|Fabriquer un fac-similé|`@Compendium[pf2e.feats-srd.xLD9EmkEmT8hgwv7]`|
|[Craft Philosopher's Stone](feats/class-20-zOK6IFSz3DIBRjEw.htm)|Fabrication de la pierre philosophale|`@Compendium[pf2e.feats-srd.zOK6IFSz3DIBRjEw]`|
|[Crafter's Appraisal](feats/skill-01-voYr7ygVcWmlg1f4.htm)|Évaluation artisanale|`@Compendium[pf2e.feats-srd.voYr7ygVcWmlg1f4]`|
|[Crane Flutter](feats/class-06-S14S52HjszTgIy4l.htm)|Battement d'aile de la grue|`@Compendium[pf2e.feats-srd.S14S52HjszTgIy4l]`|
|[Crane Stance](feats/class-01-bf7NCeKqDClaqhTR.htm)|Posture de la grue|`@Compendium[pf2e.feats-srd.bf7NCeKqDClaqhTR]`|
|[Cremate Undead](feats/class-08-nLTTph2tgwcQghVq.htm)|Incinération de morts-vivants|`@Compendium[pf2e.feats-srd.nLTTph2tgwcQghVq]`|
|[Criminal Connections](feats/skill-02-LDUWw2TeEEH0KA6M.htm)|Réseau criminel|`@Compendium[pf2e.feats-srd.LDUWw2TeEEH0KA6M]`|
|[Crimson Shroud](feats/archetype-06-M2V1vqAziOkWV30B.htm)|Voile pourpre|`@Compendium[pf2e.feats-srd.M2V1vqAziOkWV30B]`|
|[Cringe](feats/ancestry-01-WoKiYMCXV27szBdy.htm)|Recroquevillé|`@Compendium[pf2e.feats-srd.WoKiYMCXV27szBdy]`|
|[Critical Debilitation](feats/class-12-lPTcPIshChHWz4J6.htm)|Handicap critique|`@Compendium[pf2e.feats-srd.lPTcPIshChHWz4J6]`|
|[Cross the Final Horizon](feats/archetype-20-8YSwzLNlmBLoEyUj.htm)|Traverser l'horizon final|`@Compendium[pf2e.feats-srd.8YSwzLNlmBLoEyUj]`|
|[Crossblooded Evolution](feats/class-08-CN7tu5H6wTe9ENmO.htm)|Évolution métisée|`@Compendium[pf2e.feats-srd.CN7tu5H6wTe9ENmO]`|
|[Crossbow Ace](feats/class-01-CpjN7v1QN8TQFcvI.htm)|Arbalétrier émérite|`@Compendium[pf2e.feats-srd.CpjN7v1QN8TQFcvI]`|
|[Crossbow Terror](feats/archetype-06-Xhphe5Lsa4kuU4RG.htm)|Terreur à l'arbalète|`@Compendium[pf2e.feats-srd.Xhphe5Lsa4kuU4RG]`|
|[Crowd Mastery](feats/archetype-06-DR6vessIpXTLM6Xa.htm)|Maître des foules|`@Compendium[pf2e.feats-srd.DR6vessIpXTLM6Xa]`|
|[Crude Communication](feats/archetype-08-sbYDDDUWYN6Qx71k.htm)|Ébauche de communication|`@Compendium[pf2e.feats-srd.sbYDDDUWYN6Qx71k]`|
|[Cruelty](feats/class-04-vxA0VRN10OwUkGAr.htm)|Cruauté|`@Compendium[pf2e.feats-srd.vxA0VRN10OwUkGAr]`|
|[Crushing Grab](feats/class-02-hGjmbTNBUiJqYvsE.htm)|Poigne écrasante|`@Compendium[pf2e.feats-srd.hGjmbTNBUiJqYvsE]`|
|[Crystal Keeper Dedication](feats/archetype-04-ELdUj5ihdivlgb3H.htm)|Dévouement de Gardien du Cristal|`@Compendium[pf2e.feats-srd.ELdUj5ihdivlgb3H]`|
|[Crystal Ward Spells](feats/archetype-04-NrZt98r47sPSQ06j.htm)|Magie des glyphes de cristal|`@Compendium[pf2e.feats-srd.NrZt98r47sPSQ06j]`|
|[Cultural Adaptability](feats/ancestry-05-Z56DtGBd3AcZOCeG.htm)|Adaptabilité culturelle|`@Compendium[pf2e.feats-srd.Z56DtGBd3AcZOCeG]`|
|[Cunning Climber](feats/ancestry-09-swsMURQBMXZpjWl8.htm)|Escalade ingénieuse|`@Compendium[pf2e.feats-srd.swsMURQBMXZpjWl8]`|
|[Current Spell](feats/class-06-ajesR7y0jWzqjAgc.htm)|Sort à contre courant|`@Compendium[pf2e.feats-srd.ajesR7y0jWzqjAgc]`|
|[Cut From The Air](feats/class-10-6xBu4BewIkOIt9M0.htm)|Découper en l'air|`@Compendium[pf2e.feats-srd.6xBu4BewIkOIt9M0]`|
|[Cut the Bonds](feats/archetype-12-r0twuF5nxXN5lkLk.htm)|Couper les liens|`@Compendium[pf2e.feats-srd.r0twuF5nxXN5lkLk]`|
|[Daemon Magic](feats/ancestry-09-UKYO5kiOnCY1hgCD.htm)|Magie des dæmons|`@Compendium[pf2e.feats-srd.UKYO5kiOnCY1hgCD]`|
|[Dance Underfoot](feats/ancestry-09-O9z1MJpEaf6Y1Acd.htm)|Danse sous les pieds|`@Compendium[pf2e.feats-srd.O9z1MJpEaf6Y1Acd]`|
|[Dancing Leaf](feats/class-02-OWhTAowdMvBZnCrT.htm)|Feuille dansante|`@Compendium[pf2e.feats-srd.OWhTAowdMvBZnCrT]`|
|[Dandy Dedication](feats/archetype-02-4M5zIlJ8EvjQDtg9.htm)|Dévouement du Dandy|`@Compendium[pf2e.feats-srd.4M5zIlJ8EvjQDtg9]`|
|[Dangerous Sorcery](feats/class-01-emRfSVvU8ZAH9UdK.htm)|Sorcellerie dangereuse|`@Compendium[pf2e.feats-srd.emRfSVvU8ZAH9UdK]`|
|[Daredevil's Gambit](feats/class-12-6DkylvU5RF1O6DTT.htm)|Manœuvre téméraire|`@Compendium[pf2e.feats-srd.6DkylvU5RF1O6DTT]`|
|[Daring Act](feats/archetype-06-cny7ouhsoiNsWJ7X.htm)|Acte culotté|`@Compendium[pf2e.feats-srd.cny7ouhsoiNsWJ7X]`|
|[Daring Flourish](feats/archetype-10-O1C6pMTOxIrWBO3G.htm)|Sophistication culottée|`@Compendium[pf2e.feats-srd.O1C6pMTOxIrWBO3G]`|
|[Darkseer](feats/ancestry-05-XM4zR0q9rBxjR3lG.htm)|Voyant ombral|`@Compendium[pf2e.feats-srd.XM4zR0q9rBxjR3lG]`|
|[Dazing Blow](feats/class-06-2HoDwBAmPIAoKUVF.htm)|Coup étourdissant|`@Compendium[pf2e.feats-srd.2HoDwBAmPIAoKUVF]`|
|[Dead Reckoning](feats/archetype-04-68Kc4UyhnP4l8mxq.htm)|Calcul de position|`@Compendium[pf2e.feats-srd.68Kc4UyhnP4l8mxq]`|
|[Deadly Aim](feats/class-08-enPAJ1oSDutts7ic.htm)|Visée mortelle|`@Compendium[pf2e.feats-srd.enPAJ1oSDutts7ic]`|
|[Deadly Grace](feats/class-16-e8ChYAaQ9Aa8aZES.htm)|Grace mortelle|`@Compendium[pf2e.feats-srd.e8ChYAaQ9Aa8aZES]`|
|[Deadly Poison Weapon](feats/class-12-AYXherMu9gFTyXjp.htm)|Arme empoisonnée mortelle|`@Compendium[pf2e.feats-srd.AYXherMu9gFTyXjp]`|
|[Deadly Simplicity](feats/class-01-DfLkIIg2reyYW3a8.htm)|Simplicité mortelle|`@Compendium[pf2e.feats-srd.DfLkIIg2reyYW3a8]`|
|[Deadly Strikes](feats/class-20-TLCeFMDRXFB46sa8.htm)|Frappes mortelles|`@Compendium[pf2e.feats-srd.TLCeFMDRXFB46sa8]`|
|[Death's Door](feats/archetype-14-PSpwdvuddC9kXONz.htm)|Porte de la mort|`@Compendium[pf2e.feats-srd.PSpwdvuddC9kXONz]`|
|[Death's Drums](feats/ancestry-09-ZcKW5n7F0oAqRw5o.htm)|Tambours de la mort|`@Compendium[pf2e.feats-srd.ZcKW5n7F0oAqRw5o]`|
|[Debilitating Bomb](feats/class-06-sv4LeEbkOJyLen10.htm)|Bombe incapacitante|`@Compendium[pf2e.feats-srd.sv4LeEbkOJyLen10]`|
|[Debilitating Dichotomy](feats/class-08-iy9XKih5jIAdv67c.htm)|Dichotomie débilitante|`@Compendium[pf2e.feats-srd.iy9XKih5jIAdv67c]`|
|[Debilitating Shot](feats/class-10-gSc4ZUXkesN5vKrm.htm)|Tir incapacitant|`@Compendium[pf2e.feats-srd.gSc4ZUXkesN5vKrm]`|
|[Deceptive Worship](feats/skill-01-r7cgrrHh75R8UEqN.htm)|Culte trompeur|`@Compendium[pf2e.feats-srd.r7cgrrHh75R8UEqN]`|
|[Decry Thief](feats/archetype-04-4HHw2DjTxdv1jBZd.htm)|Fléau des voleurs|`@Compendium[pf2e.feats-srd.4HHw2DjTxdv1jBZd]`|
|[Dedication to the Five](feats/bonus-00-UHejQS4uCNGRI45t.htm)||`@Compendium[pf2e.feats-srd.UHejQS4uCNGRI45t]`|
|[Deep Lore](feats/class-18-iTtnN49D8ZJ2Ilur.htm)|Savoir approfondi|`@Compendium[pf2e.feats-srd.iTtnN49D8ZJ2Ilur]`|
|[Deeper Dabbler](feats/archetype-08-PTXZ2C3AV8tZf0iX.htm)|Amateur approfondi|`@Compendium[pf2e.feats-srd.PTXZ2C3AV8tZf0iX]`|
|[Defend Mount](feats/archetype-06-sflJhnFzYfqZ2tDy.htm)|Défendre sa monture|`@Compendium[pf2e.feats-srd.sflJhnFzYfqZ2tDy]`|
|[Defensive Coordination](feats/class-06-515N9nl9ChZwLWKR.htm)|Coordination défensive|`@Compendium[pf2e.feats-srd.515N9nl9ChZwLWKR]`|
|[Defensive Recovery](feats/class-12-d1ktdX1Fk37dG5ms.htm)|Récupération défensive|`@Compendium[pf2e.feats-srd.d1ktdX1Fk37dG5ms]`|
|[Defensive Roll](feats/class-14-aPt0WfFoeLTzyQRA.htm)|Roulade défensive|`@Compendium[pf2e.feats-srd.aPt0WfFoeLTzyQRA]`|
|[Defiance Unto Death](feats/ancestry-05-5AnJTAZnuWrkL8fa.htm)|Défi jusqu'à la mort|`@Compendium[pf2e.feats-srd.5AnJTAZnuWrkL8fa]`|
|[Deflect Arrow](feats/class-04-sgaqlDFTVC7Ryurt.htm)|Parade de projectiles|`@Compendium[pf2e.feats-srd.sgaqlDFTVC7Ryurt]`|
|[Deft Cooperation](feats/archetype-04-8pmd9gMl3TamFx3u.htm)|Collaboration avisée|`@Compendium[pf2e.feats-srd.8pmd9gMl3TamFx3u]`|
|[Defy The Darkness](feats/ancestry-05-G1BPXzTzrUE4IndV.htm)|Défier l'obscurité|`@Compendium[pf2e.feats-srd.G1BPXzTzrUE4IndV]`|
|[Deimatic Display](feats/class-08-UFVw57jWNC4UCfyN.htm)|Intimidation déimatique|`@Compendium[pf2e.feats-srd.UFVw57jWNC4UCfyN]`|
|[Deity's Domain](feats/class-01-FXKIALDXAzEBfj5A.htm)|Domaine de la divinité|`@Compendium[pf2e.feats-srd.FXKIALDXAzEBfj5A]`|
|[Deity's Protection](feats/class-14-DUb1VWSbTjdsbAkQ.htm)|Protection de la divinité|`@Compendium[pf2e.feats-srd.DUb1VWSbTjdsbAkQ]`|
|[Delay Trap](feats/class-08-Ar6W97iun6yYI8Df.htm)|Retardement de piège|`@Compendium[pf2e.feats-srd.Ar6W97iun6yYI8Df]`|
|[Demanding Challenge](feats/archetype-10-n0693gmx3k9wvb1N.htm)|Défi exigeant|`@Compendium[pf2e.feats-srd.n0693gmx3k9wvb1N]`|
|[Demolition Charge](feats/class-02-KCwXj3y7Nm4e3NbI.htm)|Explosion de démolition|`@Compendium[pf2e.feats-srd.KCwXj3y7Nm4e3NbI]`|
|[Demon Magic](feats/ancestry-09-3N6T7AQETVKaAwiR.htm)|Magie des démons|`@Compendium[pf2e.feats-srd.3N6T7AQETVKaAwiR]`|
|[Denier of Destruction](feats/class-20-siegOEdEpevAJNFw.htm)|Négateur de destruction|`@Compendium[pf2e.feats-srd.siegOEdEpevAJNFw]`|
|[Deny Support](feats/class-10-SYVM6Z3sS50e5Vbd.htm)|Dénier le soutien|`@Compendium[pf2e.feats-srd.SYVM6Z3sS50e5Vbd]`|
|[Derring-do](feats/class-10-SlMkuKMny7hWdNxL.htm)|Bravoure|`@Compendium[pf2e.feats-srd.SlMkuKMny7hWdNxL]`|
|[Desperate Finisher](feats/class-14-nvPxCUOCMaYdhLp1.htm)|Coup de grâce désespéré|`@Compendium[pf2e.feats-srd.nvPxCUOCMaYdhLp1]`|
|[Desperate Prayer](feats/class-01-WYaKRREZUSH0jel5.htm)|Prière désespérée|`@Compendium[pf2e.feats-srd.WYaKRREZUSH0jel5]`|
|[Destructive Block](feats/archetype-10-dY0jhJOEj6DHc0ud.htm)|Blocage destructif|`@Compendium[pf2e.feats-srd.dY0jhJOEj6DHc0ud]`|
|[Detective's Readiness](feats/class-04-5cYFHKQK6OZCwavI.htm)|Vivacité du détective|`@Compendium[pf2e.feats-srd.5cYFHKQK6OZCwavI]`|
|[Determination](feats/class-14-wdkbfWKEjAFXAxto.htm)|Détermination|`@Compendium[pf2e.feats-srd.wdkbfWKEjAFXAxto]`|
|[Determined Dash](feats/class-10-vgsMKjAbRDNxT5TK.htm)|Élan déterminé|`@Compendium[pf2e.feats-srd.vgsMKjAbRDNxT5TK]`|
|[Determined Lore Seeker](feats/archetype-12-OyOpHPOXC08bffVR.htm)|Chercheur de connaissances déterminé|`@Compendium[pf2e.feats-srd.OyOpHPOXC08bffVR]`|
|[Devil Allies](feats/bonus-00-w7dOgJvqAqyqSeSQ.htm)||`@Compendium[pf2e.feats-srd.w7dOgJvqAqyqSeSQ]`|
|[Devil Magic](feats/ancestry-09-DunjA0vLZeozcNxu.htm)|Magie des diables|`@Compendium[pf2e.feats-srd.DunjA0vLZeozcNxu]`|
|[Devil's Advocate](feats/ancestry-01-8nF3r3NHW2uSRgwb.htm)|Avocat du diable|`@Compendium[pf2e.feats-srd.8nF3r3NHW2uSRgwb]`|
|[Devoted Focus](feats/class-10-FIsMdgvGji5Nci8l.htm)|Focalisation dévouée|`@Compendium[pf2e.feats-srd.FIsMdgvGji5Nci8l]`|
|[Diabolic Certitude](feats/skill-04-WxL8NMW9JQ5igu0C.htm)|Certitude diabolique|`@Compendium[pf2e.feats-srd.WxL8NMW9JQ5igu0C]`|
|[Diamond Fists](feats/class-18-zTulA4sVXwLRm28Z.htm)|Poings de diamant|`@Compendium[pf2e.feats-srd.zTulA4sVXwLRm28Z]`|
|[Diamond Soul](feats/class-12-GMrJdGwajADbL1y5.htm)|Âme de diamant|`@Compendium[pf2e.feats-srd.GMrJdGwajADbL1y5]`|
|[Didactic Strike](feats/class-16-6fojN9yBJByTZ1Q9.htm)|Frappe didactique|`@Compendium[pf2e.feats-srd.6fojN9yBJByTZ1Q9]`|
|[Diehard](feats/general-01-I0BhPWqYf1bbzEYg.htm)|Dur à cuir|`@Compendium[pf2e.feats-srd.I0BhPWqYf1bbzEYg]`|
|[Different Worlds](feats/general-01-z1Z22gTp7J1VRLSR.htm)|Mondes différents|`@Compendium[pf2e.feats-srd.z1Z22gTp7J1VRLSR]`|
|[Dig Quickly](feats/ancestry-01-PsLne80WUsD4IFa6.htm)|Creuser rapidement|`@Compendium[pf2e.feats-srd.PsLne80WUsD4IFa6]`|
|[Directed Audience](feats/class-02-ot0uyFtnC1Whz5bp.htm)|Public choisi|`@Compendium[pf2e.feats-srd.ot0uyFtnC1Whz5bp]`|
|[Directed Channel](feats/class-04-iojlXjVdbzi1fZGt.htm)|Canalisation dirigée|`@Compendium[pf2e.feats-srd.iojlXjVdbzi1fZGt]`|
|[Directional Bombs](feats/class-06-ozvYhY4hG1deXly8.htm)|Bombes directionnelles|`@Compendium[pf2e.feats-srd.ozvYhY4hG1deXly8]`|
|[Dirge of Doom](feats/class-06-vWrGwqy4AhHMPz8V.htm)|Chant funeste|`@Compendium[pf2e.feats-srd.vWrGwqy4AhHMPz8V]`|
|[Disarming Assault](feats/class-08-nL82Dzh0QwkNkJDA.htm)|Assaut désarmant|`@Compendium[pf2e.feats-srd.nL82Dzh0QwkNkJDA]`|
|[Disarming Block](feats/archetype-04-dSSwRyuhKTq1VubX.htm)|Blocage désarmant|`@Compendium[pf2e.feats-srd.dSSwRyuhKTq1VubX]`|
|[Disarming Flair](feats/class-01-1MWL2uEmyiOfYtJn.htm)|Élégance désarmante|`@Compendium[pf2e.feats-srd.1MWL2uEmyiOfYtJn]`|
|[Disarming Stance](feats/class-06-1p5ErCp33nGOzEsk.htm)|Posture désarmante|`@Compendium[pf2e.feats-srd.1p5ErCp33nGOzEsk]`|
|[Disarming Twist](feats/class-10-4VbfFPuFpbGLMMKF.htm)|Torsion désarmante|`@Compendium[pf2e.feats-srd.4VbfFPuFpbGLMMKF]`|
|[Discerning Strike](feats/archetype-06-OjvE7gaQgWiBqOhY.htm)|Frappe discernante|`@Compendium[pf2e.feats-srd.OjvE7gaQgWiBqOhY]`|
|[Disciple's Breath](feats/archetype-16-cHOALlKY1XsCj3Fe.htm)|Souffle du disciple|`@Compendium[pf2e.feats-srd.cHOALlKY1XsCj3Fe]`|
|[Discordant Voice](feats/class-18-8JMOgtB3XG7o6ffW.htm)|Voix discordante|`@Compendium[pf2e.feats-srd.8JMOgtB3XG7o6ffW]`|
|[Discreet Inquiry](feats/skill-02-uF0ATN2Zw1Q67ew2.htm)|Enquête discrète|`@Compendium[pf2e.feats-srd.uF0ATN2Zw1Q67ew2]`|
|[Disillusionment](feats/bonus-00-RkvAuikI0kIZlQTU.htm)||`@Compendium[pf2e.feats-srd.RkvAuikI0kIZlQTU]`|
|[Disorienting Opening](feats/class-08-j4zGMRiTi5t6guMF.htm)|Ouverture déroutante|`@Compendium[pf2e.feats-srd.j4zGMRiTi5t6guMF]`|
|[Dispelling Slice](feats/class-16-6AXAJuMEMpxU27PJ.htm)|Coupe dissipante|`@Compendium[pf2e.feats-srd.6AXAJuMEMpxU27PJ]`|
|[Disrupt Ki](feats/class-12-P13ZhZcR67Ev0vrS.htm)|Perturbation du ki|`@Compendium[pf2e.feats-srd.P13ZhZcR67Ev0vrS]`|
|[Disrupt Prey](feats/class-04-qav9ec9cR4lFcz3C.htm)|Interruption de la proie|`@Compendium[pf2e.feats-srd.qav9ec9cR4lFcz3C]`|
|[Disruptive Stance](feats/class-10-OEGhbRgW6wRbccns.htm)|Posture perturbatrice|`@Compendium[pf2e.feats-srd.OEGhbRgW6wRbccns]`|
|[Distracting Feint](feats/class-02-hLYoIjH8gPEVXyWG.htm)|Feinte de diversion|`@Compendium[pf2e.feats-srd.hLYoIjH8gPEVXyWG]`|
|[Distracting Flattery](feats/archetype-04-jzflcD1XnBp2bSZI.htm)|Flatterie de diversion|`@Compendium[pf2e.feats-srd.jzflcD1XnBp2bSZI]`|
|[Distracting Performance](feats/skill-02-4UXyMtXLaOxuH6Js.htm)|Représentation de diversion|`@Compendium[pf2e.feats-srd.4UXyMtXLaOxuH6Js]`|
|[Distracting Shadows](feats/ancestry-01-vcfeHDoaWEZtEcfz.htm)|Ombres distrayantes|`@Compendium[pf2e.feats-srd.vcfeHDoaWEZtEcfz]`|
|[Distracting Shot](feats/class-12-BQrhDpLIp9zjbjEP.htm)|Tir déroutant|`@Compendium[pf2e.feats-srd.BQrhDpLIp9zjbjEP]`|
|[Disturbing Defense](feats/archetype-04-zWEu9xuAxBnPoSrv.htm)|Défense perturbante|`@Compendium[pf2e.feats-srd.zWEu9xuAxBnPoSrv]`|
|[Disturbing Knowledge](feats/skill-07-hkSuxXOc9qBleJbd.htm)|Connaissances perturbantes|`@Compendium[pf2e.feats-srd.hkSuxXOc9qBleJbd]`|
|[Diverse Armor Expert](feats/archetype-14-jEq4JcZb0LpKOZy1.htm)|Armures diverses expert|`@Compendium[pf2e.feats-srd.jEq4JcZb0LpKOZy1]`|
|[Diverse Mystery](feats/class-16-2HeRmbcHcsRMccir.htm)|Mystère diversifié|`@Compendium[pf2e.feats-srd.2HeRmbcHcsRMccir]`|
|[Diverse Recognition](feats/archetype-08-yoeMOIgH8Snw1JCQ.htm)|Reconnaissance multiple|`@Compendium[pf2e.feats-srd.yoeMOIgH8Snw1JCQ]`|
|[Diverse Weapon Expert](feats/archetype-12-pu1U9ZWVVG1Lc94t.htm)|Armes diverses expert|`@Compendium[pf2e.feats-srd.pu1U9ZWVVG1Lc94t]`|
|[Diverting Vortex](feats/class-06-PqZZSo06BH5N7x7C.htm)|Vortex déroutant|`@Compendium[pf2e.feats-srd.PqZZSo06BH5N7x7C]`|
|[Divine Access](feats/class-04-8EbIznFgkG7PHqlE.htm)|Accès divin|`@Compendium[pf2e.feats-srd.8EbIznFgkG7PHqlE]`|
|[Divine Aegis](feats/class-02-xoIxiRtBVHV27Rvd.htm)|Égide divine|`@Compendium[pf2e.feats-srd.xoIxiRtBVHV27Rvd]`|
|[Divine Ally](feats/archetype-06-lix0Utu4g8mQ0ZtI.htm)|Allié divin|`@Compendium[pf2e.feats-srd.lix0Utu4g8mQ0ZtI]`|
|[Divine Breadth](feats/archetype-08-XRIWWrXfghsQce4S.htm)|Amplitude divine|`@Compendium[pf2e.feats-srd.XRIWWrXfghsQce4S]`|
|[Divine Countermeasures](feats/ancestry-09-ovMIxhiStlPE7tty.htm)|Contre mesures divines|`@Compendium[pf2e.feats-srd.ovMIxhiStlPE7tty]`|
|[Divine Effusion](feats/class-18-t1sM6Xj9T07fqpwN.htm)|Effusion divine|`@Compendium[pf2e.feats-srd.t1sM6Xj9T07fqpwN]`|
|[Divine Evolution](feats/class-04-WHOCaVobY7N3UTtA.htm)|Évolution divine|`@Compendium[pf2e.feats-srd.WHOCaVobY7N3UTtA]`|
|[Divine Grace](feats/class-02-tJduF6N83l5khRow.htm)|Grâce divine|`@Compendium[pf2e.feats-srd.tJduF6N83l5khRow]`|
|[Divine Guidance](feats/skill-15-dYMxP8SsHrwOze8v.htm)|Assistance divine|`@Compendium[pf2e.feats-srd.dYMxP8SsHrwOze8v]`|
|[Divine Health](feats/class-04-GNMy7NYfF3AQwHpN.htm)|Santé divine|`@Compendium[pf2e.feats-srd.GNMy7NYfF3AQwHpN]`|
|[Divine Reflexes](feats/class-14-EvSfoYmuCDCRAvaF.htm)|Réflexes divins|`@Compendium[pf2e.feats-srd.EvSfoYmuCDCRAvaF]`|
|[Divine Wall](feats/class-12-jNeIaFUFSGUXoSON.htm)|Mur divin|`@Compendium[pf2e.feats-srd.jNeIaFUFSGUXoSON]`|
|[Divine Weapon](feats/class-06-Trj5azJlaOk5jgBi.htm)|Arme divine|`@Compendium[pf2e.feats-srd.Trj5azJlaOk5jgBi]`|
|[Diviner Sense](feats/class-12-3WUL8ExEkZDRYeBu.htm)|Sens du devin|`@Compendium[pf2e.feats-srd.3WUL8ExEkZDRYeBu]`|
|[Doctor's Visitation](feats/archetype-04-1fBHZpM3Z3MQtzvi.htm)|Tournée du médecin|`@Compendium[pf2e.feats-srd.1fBHZpM3Z3MQtzvi]`|
|[Dodge Away](feats/archetype-06-0qGLCpggCcOVkbtT.htm)|S'esquiver au loin|`@Compendium[pf2e.feats-srd.0qGLCpggCcOVkbtT]`|
|[Dodging Roll](feats/class-12-nBWoZ311FXFJC8Zl.htm)|Dérobade|`@Compendium[pf2e.feats-srd.nBWoZ311FXFJC8Zl]`|
|[Domain Acumen](feats/class-02-qmFWCHOuubEl7VpX.htm)|Sagacité du domaine|`@Compendium[pf2e.feats-srd.qmFWCHOuubEl7VpX]`|
|[Domain Fluency](feats/class-12-ENoRkTXtdfsbs98S.htm)|Maîtrise du domaine|`@Compendium[pf2e.feats-srd.ENoRkTXtdfsbs98S]`|
|[Domain Focus](feats/class-12-vVhgYkOU9mPTGTxF.htm)|Focalisation du domaine|`@Compendium[pf2e.feats-srd.vVhgYkOU9mPTGTxF]`|
|[Domain Initiate](feats/class-01-hT4INKGtly4QY8KN.htm)|Initié du domaine|`@Compendium[pf2e.feats-srd.hT4INKGtly4QY8KN]`|
|[Domain Wellspring](feats/class-18-ouKDey5RHQKN9YBT.htm)|Source du domaine|`@Compendium[pf2e.feats-srd.ouKDey5RHQKN9YBT]`|
|[Double Prey](feats/class-12-pbD4lfAPkK1NNag0.htm)|Deux proies|`@Compendium[pf2e.feats-srd.pbD4lfAPkK1NNag0]`|
|[Double Shot](feats/class-04-PwcmmJOLY8C9JHau.htm)|Double tir|`@Compendium[pf2e.feats-srd.PwcmmJOLY8C9JHau]`|
|[Double Slice](feats/class-01-onde0SxLoxLBTnvm.htm)|Double taille|`@Compendium[pf2e.feats-srd.onde0SxLoxLBTnvm]`|
|[Doublespeak](feats/skill-07-qibU5SZPFHMNnpAP.htm)|Double langage|`@Compendium[pf2e.feats-srd.qibU5SZPFHMNnpAP]`|
|[Dracomancer](feats/ancestry-09-lbiFj4At5BxotaNY.htm)|Dracomancien|`@Compendium[pf2e.feats-srd.lbiFj4At5BxotaNY]`|
|[Draconic Arrogance](feats/class-01-EOmTf95t03y4IGdp.htm)|Arrogance draconique|`@Compendium[pf2e.feats-srd.EOmTf95t03y4IGdp]`|
|[Draconic Scent](feats/archetype-04-2UKf5IiUbpUbOC9a.htm)|Odorat draconique|`@Compendium[pf2e.feats-srd.2UKf5IiUbpUbOC9a]`|
|[Dragging Strike](feats/class-02-yAgFDUU8HfVK4KTy.htm)|Frappe entraînante|`@Compendium[pf2e.feats-srd.yAgFDUU8HfVK4KTy]`|
|[Dragon Arcana](feats/archetype-04-gMPnLWLlHoNU9Lqv.htm)|Arcanes de dragon|`@Compendium[pf2e.feats-srd.gMPnLWLlHoNU9Lqv]`|
|[Dragon Disciple Dedication](feats/archetype-02-oo34CloLefFRi72w.htm)|Dévouement du Disciple draconique|`@Compendium[pf2e.feats-srd.oo34CloLefFRi72w]`|
|[Dragon Prince](feats/ancestry-09-Q1lGguNI4SqPwgVn.htm)|Prince dragon|`@Compendium[pf2e.feats-srd.Q1lGguNI4SqPwgVn]`|
|[Dragon Roar](feats/class-06-SELSj1vvVLx5cP72.htm)|Rugissement du dragon|`@Compendium[pf2e.feats-srd.SELSj1vvVLx5cP72]`|
|[Dragon Shape](feats/class-12-p0jZhb8PSswUsZaz.htm)|Morphologie draconique|`@Compendium[pf2e.feats-srd.p0jZhb8PSswUsZaz]`|
|[Dragon Spit](feats/ancestry-01-UsJtnrqWOs7puRZa.htm)|Crachat du dragon|`@Compendium[pf2e.feats-srd.UsJtnrqWOs7puRZa]`|
|[Dragon Stance](feats/class-01-8sy3sHwOHS4ImwvJ.htm)|Posture du dragon|`@Compendium[pf2e.feats-srd.8sy3sHwOHS4ImwvJ]`|
|[Dragon Transformation](feats/class-16-lgEihn7deZwHczGE.htm)|Transformation en dragon|`@Compendium[pf2e.feats-srd.lgEihn7deZwHczGE]`|
|[Dragon's Breath](feats/ancestry-09-BaVO8UU5ZkL8OZZj.htm)|Souffle de dragon|`@Compendium[pf2e.feats-srd.BaVO8UU5ZkL8OZZj]`|
|[Dragon's Presence](feats/ancestry-01-SPyvwsiSghySIEw2.htm)|Présence draconique|`@Compendium[pf2e.feats-srd.SPyvwsiSghySIEw2]`|
|[Dragon's Rage Breath](feats/class-06-3uavnVbCsqTvzpgt.htm)|Souffle de rage du dragon|`@Compendium[pf2e.feats-srd.3uavnVbCsqTvzpgt]`|
|[Dragon's Rage Wings](feats/class-12-bqZkAFS6eq9TKXMO.htm)|Ailes de rage du dragon|`@Compendium[pf2e.feats-srd.bqZkAFS6eq9TKXMO]`|
|[Dragonslayer Oath](feats/class-02-0lZeJHgk4ASO0clU.htm)|Serment du pourfendeur de dragons|`@Compendium[pf2e.feats-srd.0lZeJHgk4ASO0clU]`|
|[Dread Marshal Stance](feats/archetype-04-R7c4PyTNkZb0yvoT.htm)|Posture du terrible Capitaine|`@Compendium[pf2e.feats-srd.R7c4PyTNkZb0yvoT]`|
|[Dread Striker](feats/class-04-9SYnbjFgyucjhN5e.htm)|Frappeur d'effroi|`@Compendium[pf2e.feats-srd.9SYnbjFgyucjhN5e]`|
|[Dream May](feats/ancestry-01-kqnFdIhToKTnOpMl.htm)|Vierge onirique|`@Compendium[pf2e.feats-srd.kqnFdIhToKTnOpMl]`|
|[Druid Dedication](feats/archetype-02-vhw3n86TEs6laopA.htm)|Dévouement du druide|`@Compendium[pf2e.feats-srd.vhw3n86TEs6laopA]`|
|[Dual Finisher](feats/class-08-PP1gfRCc1YwnQGxp.htm)|Botte duale|`@Compendium[pf2e.feats-srd.PP1gfRCc1YwnQGxp]`|
|[Dual Onslaught](feats/archetype-14-EVNd9hZs49b1pScR.htm)|Massacre à deux armes|`@Compendium[pf2e.feats-srd.EVNd9hZs49b1pScR]`|
|[Dual Thrower](feats/archetype-04-zfTmb78yGZzNpgU3.htm)|Lanceur à deux mains|`@Compendium[pf2e.feats-srd.zfTmb78yGZzNpgU3]`|
|[Dual-Handed Assault](feats/class-04-HHAGiBYVv8nyUEsd.htm)|Assaut à deux mains|`@Compendium[pf2e.feats-srd.HHAGiBYVv8nyUEsd]`|
|[Dual-Weapon Blitz](feats/archetype-10-04Smj4ZsEBD8WIXv.htm)|Attaque éclair à deux armes|`@Compendium[pf2e.feats-srd.04Smj4ZsEBD8WIXv]`|
|[Dual-Weapon Reload](feats/archetype-04-sjChYEuEWPqndCSK.htm)|Rechargement à deux armes|`@Compendium[pf2e.feats-srd.sjChYEuEWPqndCSK]`|
|[Dual-Weapon Warrior Dedication](feats/archetype-02-YhnCjiHNlS3nCeoC.htm)|Dévouement de combattant à deux armes|`@Compendium[pf2e.feats-srd.YhnCjiHNlS3nCeoC]`|
|[Dualistic Synergy](feats/class-08-9EqUTnbV8WHE2aKm.htm)|Synergie duale|`@Compendium[pf2e.feats-srd.9EqUTnbV8WHE2aKm]`|
|[Dubious Knowledge](feats/skill-01-1Bt7uCW2WI4sM84P.htm)|Savoir douteux|`@Compendium[pf2e.feats-srd.1Bt7uCW2WI4sM84P]`|
|[Dueling Dance (Fighter)](feats/class-12-FYz5eQeTox9IDkSd.htm)|Danse en duel (Guerrier)|`@Compendium[pf2e.feats-srd.FYz5eQeTox9IDkSd]`|
|[Dueling Dance (Swashbuckler)](feats/class-10-9VGmE7X4aK2W8YWj.htm)|Danse en duel (Bretteur)|`@Compendium[pf2e.feats-srd.9VGmE7X4aK2W8YWj]`|
|[Dueling Parry (Fighter)](feats/class-02-n7nQQR940OvFbw7T.htm)|Parade en duel (Guerrier)|`@Compendium[pf2e.feats-srd.n7nQQR940OvFbw7T]`|
|[Dueling Parry (Swashbuckler)](feats/class-01-WcY7H7mRiK2VDquV.htm)|Parade en duel (Bretteur)|`@Compendium[pf2e.feats-srd.WcY7H7mRiK2VDquV]`|
|[Dueling Riposte](feats/class-08-mf2cdCRV8uowOMOm.htm)|Riposte en duel|`@Compendium[pf2e.feats-srd.mf2cdCRV8uowOMOm]`|
|[Duelist Dedication](feats/archetype-02-r72qcTupGzyRDiGe.htm)|Dévouement du Dueliste|`@Compendium[pf2e.feats-srd.r72qcTupGzyRDiGe]`|
|[Duelist's Challenge](feats/archetype-04-5op3m0gwZjL4udit.htm)|Défi du duelliste|`@Compendium[pf2e.feats-srd.5op3m0gwZjL4udit]`|
|[Duelist's Edge](feats/archetype-04-Bk07joho2dUG3lVw.htm)|Avantage du duelliste|`@Compendium[pf2e.feats-srd.Bk07joho2dUG3lVw]`|
|[Duskwalker Lore](feats/ancestry-01-O5v8yaeCbjKeXfyi.htm)|Connaissance des crépusculaires|`@Compendium[pf2e.feats-srd.O5v8yaeCbjKeXfyi]`|
|[Duskwalker Magic](feats/ancestry-09-PxWdf4HAhZ8fUB3R.htm)|Magie des crépusculaires|`@Compendium[pf2e.feats-srd.PxWdf4HAhZ8fUB3R]`|
|[Dwarven Doughtiness](feats/ancestry-01-UJ8AqzkkDqRCMNFW.htm)|Bravoure naine|`@Compendium[pf2e.feats-srd.UJ8AqzkkDqRCMNFW]`|
|[Dwarven Lore](feats/ancestry-01-9bnkx6VgcO5mOk5b.htm)|Connaissance des nains|`@Compendium[pf2e.feats-srd.9bnkx6VgcO5mOk5b]`|
|[Dwarven Reinforcement](feats/ancestry-05-FGrIFDobGRFBPOuM.htm)|Renforcement nain|`@Compendium[pf2e.feats-srd.FGrIFDobGRFBPOuM]`|
|[Dwarven Weapon Cunning](feats/ancestry-05-vW7tWi3jK7z2Clen.htm)|Connaissances des armes naines|`@Compendium[pf2e.feats-srd.vW7tWi3jK7z2Clen]`|
|[Dwarven Weapon Expertise](feats/ancestry-13-tpkih32Ch2wCA9R5.htm)|Expertise avec les armes naines|`@Compendium[pf2e.feats-srd.tpkih32Ch2wCA9R5]`|
|[Dwarven Weapon Familiarity](feats/ancestry-01-CXS0ryG2SODSobm9.htm)|Familiarité avec les armes naines|`@Compendium[pf2e.feats-srd.CXS0ryG2SODSobm9]`|
|[Earned Glory](feats/ancestry-01-nfERPRCITBp970HO.htm)|Gloire méritée|`@Compendium[pf2e.feats-srd.nfERPRCITBp970HO]`|
|[Earworm](feats/class-14-AOLf6QX068LR9L9e.htm)|Rengaine|`@Compendium[pf2e.feats-srd.AOLf6QX068LR9L9e]`|
|[Easily Dismissed](feats/ancestry-05-ZRI1OXaaa4ZC6EK1.htm)|Facilement négligé|`@Compendium[pf2e.feats-srd.ZRI1OXaaa4ZC6EK1]`|
|[Eat Fortune](feats/ancestry-05-rFmJVDdB313EibTs.htm)|Avaler la chance|`@Compendium[pf2e.feats-srd.rFmJVDdB313EibTs]`|
|[Ebb And Flow](feats/class-14-xSJaOcdhqDF1CBs3.htm)|Flux et reflux|`@Compendium[pf2e.feats-srd.xSJaOcdhqDF1CBs3]`|
|[Echoes In Stone](feats/ancestry-09-nKkbEKbE9vfKWKdd.htm)|Échos dans la roche|`@Compendium[pf2e.feats-srd.nKkbEKbE9vfKWKdd]`|
|[Echoing Channel](feats/class-18-T4Xm8vYtnGMOM0Cw.htm)|Canalisation répétée|`@Compendium[pf2e.feats-srd.T4Xm8vYtnGMOM0Cw]`|
|[Echoing Spell](feats/class-18-NvEYf0jIETEu2LtP.htm)|Sort en écho|`@Compendium[pf2e.feats-srd.NvEYf0jIETEu2LtP]`|
|[Eclectic Obsession](feats/ancestry-05-0ihK3qYItmi8eVZs.htm)|Obsession éclectique|`@Compendium[pf2e.feats-srd.0ihK3qYItmi8eVZs]`|
|[Eclectic Polymath](feats/class-12-yaxf1Tpk5iwPCSpW.htm)|Touche-à-tout éclectique|`@Compendium[pf2e.feats-srd.yaxf1Tpk5iwPCSpW]`|
|[Eclectic Skill](feats/class-08-TOyqtUUnOkOLl1Pm.htm)|Compétence éclectique|`@Compendium[pf2e.feats-srd.TOyqtUUnOkOLl1Pm]`|
|[Eclectic Sword Training](feats/ancestry-09-KRgzuwwjT30KKvV4.htm)|Entraînement éclectique à l'épée|`@Compendium[pf2e.feats-srd.KRgzuwwjT30KKvV4]`|
|[Edgewatch Detective Dedication](feats/archetype-02-6b8DiNx4c1zPZ7RP.htm)||`@Compendium[pf2e.feats-srd.6b8DiNx4c1zPZ7RP]`|
|[Educate Allies](feats/class-06-9cHQua33V35JPE3U.htm)|Alliés instruits|`@Compendium[pf2e.feats-srd.9cHQua33V35JPE3U]`|
|[Educated Assessment](feats/class-12-a2Owk9WI4pjSPuHf.htm)|Évaluation érudite|`@Compendium[pf2e.feats-srd.a2Owk9WI4pjSPuHf]`|
|[Efficient Alchemy (Alchemist)](feats/class-04-c7jNms3ZQ8eaMUqv.htm)|Alchimie efficiente|`@Compendium[pf2e.feats-srd.c7jNms3ZQ8eaMUqv]`|
|[Efficient Alchemy (Paragon)](feats/class-20-2FBZ0apnmZ7b61ct.htm)|Alchimie efficiente|`@Compendium[pf2e.feats-srd.2FBZ0apnmZ7b61ct]`|
|[Efficient Rituals](feats/archetype-08-jYKKnr41OqQrf7hv.htm)|Rituels efficaces|`@Compendium[pf2e.feats-srd.jYKKnr41OqQrf7hv]`|
|[Effortless Concentration](feats/class-16-rgs6OZJYCgi5At8J.htm)|Concentration aisée|`@Compendium[pf2e.feats-srd.rgs6OZJYCgi5At8J]`|
|[Eidetic Memorization](feats/archetype-10-JwosaDYoqfPiFMYa.htm)|Mémorisation parfaite|`@Compendium[pf2e.feats-srd.JwosaDYoqfPiFMYa]`|
|[Elaborate Flourish](feats/class-04-pCVegyXxNibF4ulp.htm)|Fioriture élaborée|`@Compendium[pf2e.feats-srd.pCVegyXxNibF4ulp]`|
|[Elastic Mutagen](feats/class-10-FLQWJ2CIv9mCfSAx.htm)|Mutagène élastique|`@Compendium[pf2e.feats-srd.FLQWJ2CIv9mCfSAx]`|
|[Eldritch Archer Dedication](feats/archetype-06-oyLkqhDGwGGj40ME.htm)|Dévouement d'Archer mystique|`@Compendium[pf2e.feats-srd.oyLkqhDGwGGj40ME]`|
|[Eldritch Debilitations](feats/class-10-kUv9eiP8Zhck70WZ.htm)|Handicaps mystiques|`@Compendium[pf2e.feats-srd.kUv9eiP8Zhck70WZ]`|
|[Eldritch Nails](feats/class-04-m4FOYkzuqNjU0ETq.htm)|Ongles mystiques|`@Compendium[pf2e.feats-srd.m4FOYkzuqNjU0ETq]`|
|[Elemental Fist](feats/class-02-BU4NBIBkVZxdWLLH.htm)|Poing élémentaire|`@Compendium[pf2e.feats-srd.BU4NBIBkVZxdWLLH]`|
|[Elemental Shape](feats/class-10-F0MYBfiyOD8YHq5t.htm)|Morphologie élémentaire|`@Compendium[pf2e.feats-srd.F0MYBfiyOD8YHq5t]`|
|[Elemental Summons](feats/class-04-011wnYpjIwCEzFtl.htm)|Convocations élémentaires|`@Compendium[pf2e.feats-srd.011wnYpjIwCEzFtl]`|
|[Elemental Wrath](feats/ancestry-01-yMfZulJcoSomQ6dO.htm)|Courroux élémentaire|`@Compendium[pf2e.feats-srd.yMfZulJcoSomQ6dO]`|
|[Elf Atavism](feats/ancestry-01-7BVl9lFf0wuTjBgM.htm)|Atavisme elfique|`@Compendium[pf2e.feats-srd.7BVl9lFf0wuTjBgM]`|
|[Elf Step](feats/ancestry-09-aIm2qi4JZerthZmF.htm)|Pas elfique|`@Compendium[pf2e.feats-srd.aIm2qi4JZerthZmF]`|
|[Elite Dracomancer](feats/ancestry-13-I9cJYC7anz7HmpcJ.htm)|Dracomancien d'élite|`@Compendium[pf2e.feats-srd.I9cJYC7anz7HmpcJ]`|
|[Elucidating Mercy](feats/class-10-647GZOyhWgcjw6Jg.htm)|Soulagement clarifiant|`@Compendium[pf2e.feats-srd.647GZOyhWgcjw6Jg]`|
|[Elude the Divine](feats/archetype-08-vVyX9IG8flxg81mc.htm)|Éluder le divin|`@Compendium[pf2e.feats-srd.vVyX9IG8flxg81mc]`|
|[Elven Aloofness](feats/ancestry-01-y7Or0CbcQBDdS9yG.htm)|Détachement elfique|`@Compendium[pf2e.feats-srd.y7Or0CbcQBDdS9yG]`|
|[Elven Instincts](feats/ancestry-05-4M36jGbeSDfZFM38.htm)|Instincts elfiques|`@Compendium[pf2e.feats-srd.4M36jGbeSDfZFM38]`|
|[Elven Lore](feats/ancestry-01-zki5qdM5IQcAiscM.htm)|Connaissance des elfes|`@Compendium[pf2e.feats-srd.zki5qdM5IQcAiscM]`|
|[Elven Verve](feats/ancestry-01-eD3KZJV8ACLt2xns.htm)|Verve elfique|`@Compendium[pf2e.feats-srd.eD3KZJV8ACLt2xns]`|
|[Elven Weapon Elegance](feats/ancestry-05-zc32MIxa1E6357D6.htm)|Élégance avec les armes elfiques|`@Compendium[pf2e.feats-srd.zc32MIxa1E6357D6]`|
|[Elven Weapon Expertise](feats/ancestry-13-wruf4sMh3P7o8k1P.htm)|Expertise avec les armes elfiques|`@Compendium[pf2e.feats-srd.wruf4sMh3P7o8k1P]`|
|[Elven Weapon Familiarity](feats/ancestry-01-4NKyZVkmWjDyyIYZ.htm)|Familiarité avec les armes elfiques|`@Compendium[pf2e.feats-srd.4NKyZVkmWjDyyIYZ]`|
|[Emblazon Antimagic](feats/class-12-xAFdoKl7aOP9rVkl.htm)|Antimagie blasonnée|`@Compendium[pf2e.feats-srd.xAFdoKl7aOP9rVkl]`|
|[Emblazon Armament](feats/class-02-7RFjTTzznsdPcaYB.htm)|Arsenal blasonné|`@Compendium[pf2e.feats-srd.7RFjTTzznsdPcaYB]`|
|[Emblazon Divinity](feats/class-20-DMECB9RwLAhY0T9o.htm)|Divinité blasonnée|`@Compendium[pf2e.feats-srd.DMECB9RwLAhY0T9o]`|
|[Emblazon Energy](feats/class-08-rKZE8BA9IQHSSWoW.htm)|Énergie blasonnée|`@Compendium[pf2e.feats-srd.rKZE8BA9IQHSSWoW]`|
|[Embrace The Pain](feats/class-12-j20djiiuVwUf8MqL.htm)|Épouser la douleur|`@Compendium[pf2e.feats-srd.j20djiiuVwUf8MqL]`|
|[Emerald Boughs Accustomation](feats/archetype-06-EZ24QwnFteLCrgLg.htm)|Habitudes des Branches d'émeraude|`@Compendium[pf2e.feats-srd.EZ24QwnFteLCrgLg]`|
|[Emerald Boughs Hideaway](feats/archetype-10-cxAvcFiZRT3ZVhie.htm)|Cachette des Branches d'Émeraude|`@Compendium[pf2e.feats-srd.cxAvcFiZRT3ZVhie]`|
|[Emergency Medical Assistance](feats/archetype-12-9AZjpeeS824VsYv8.htm)|Assistance médical d'urgence|`@Compendium[pf2e.feats-srd.9AZjpeeS824VsYv8]`|
|[Empathetic Plea](feats/ancestry-01-MS53Ds75BT379ZFm.htm)|Supplique empathique|`@Compendium[pf2e.feats-srd.MS53Ds75BT379ZFm]`|
|[Empty Body](feats/class-18-6GUl9WG7OKvfVQo4.htm)|Désertion de l'âme|`@Compendium[pf2e.feats-srd.6GUl9WG7OKvfVQo4]`|
|[Empyreal Blessing](feats/ancestry-05-n3CbbtK4fgBznIMf.htm)|Bénédiction empyréenne|`@Compendium[pf2e.feats-srd.n3CbbtK4fgBznIMf]`|
|[Enchanting Arrow](feats/archetype-08-m8iP2OCzit9WUrMD.htm)|Flèche enchantée|`@Compendium[pf2e.feats-srd.m8iP2OCzit9WUrMD]`|
|[Encouraging Words](feats/skill-02-dUnT3HWMFD3d2eBJ.htm)|Paroles encourageantes|`@Compendium[pf2e.feats-srd.dUnT3HWMFD3d2eBJ]`|
|[Endemic Herbs](feats/archetype-06-BQzExsEZrwGsJD66.htm)|Herbes endémiques|`@Compendium[pf2e.feats-srd.BQzExsEZrwGsJD66]`|
|[Endurance of the Rooted Tree](feats/class-20-RYUb5oxd46Yvdypz.htm)|Endurance de l'arbre enraciné|`@Compendium[pf2e.feats-srd.RYUb5oxd46Yvdypz]`|
|[Endure Death's Touch](feats/archetype-10-8d3QxoKmqSkB9Mcj.htm)|Endurer le contact de la mort|`@Compendium[pf2e.feats-srd.8d3QxoKmqSkB9Mcj]`|
|[Enduring Alchemy](feats/class-04-jBeuyq0Aged45YAc.htm)|Alchimie durable|`@Compendium[pf2e.feats-srd.jBeuyq0Aged45YAc]`|
|[Enduring Debilitation](feats/class-20-RzfWrOqHL2GcK0rr.htm)|Incapacité persistante|`@Compendium[pf2e.feats-srd.RzfWrOqHL2GcK0rr]`|
|[Enduring Quickness](feats/class-20-txLcSHu6kEfmrJj1.htm)|Rapidité persistante|`@Compendium[pf2e.feats-srd.txLcSHu6kEfmrJj1]`|
|[Energetic Resonance](feats/class-06-yUpZcrQHrz4mflKQ.htm)|Résonance énergétique|`@Compendium[pf2e.feats-srd.yUpZcrQHrz4mflKQ]`|
|[Energized Font](feats/ancestry-05-NBDwiz1NDioc2eMP.htm)|Source d'énergie|`@Compendium[pf2e.feats-srd.NBDwiz1NDioc2eMP]`|
|[Energy Ablation](feats/class-02-iS4Vc2zv7vgL5mnX.htm)|Emprunt énergétique|`@Compendium[pf2e.feats-srd.iS4Vc2zv7vgL5mnX]`|
|[Energy Blessed](feats/ancestry-09-B5HiNholWMwYdHTC.htm)|Énergie bénie|`@Compendium[pf2e.feats-srd.B5HiNholWMwYdHTC]`|
|[Energy Fusion](feats/class-10-MgqRwyL8PWyYvoZs.htm)|Fusion d'énergies|`@Compendium[pf2e.feats-srd.MgqRwyL8PWyYvoZs]`|
|[Energy Ward](feats/class-10-lG4dYrnkE42IgnGG.htm)|Protection énergétique|`@Compendium[pf2e.feats-srd.lG4dYrnkE42IgnGG]`|
|[Enforce Oath](feats/class-12-YZ138OqflDhrkqmR.htm)|Serment renforcé|`@Compendium[pf2e.feats-srd.YZ138OqflDhrkqmR]`|
|[Enhanced Familiar](feats/class-02-N7dTFxpjXGn4ddq8.htm)|Familier amélioré|`@Compendium[pf2e.feats-srd.N7dTFxpjXGn4ddq8]`|
|[Enigma's Knowledge](feats/class-12-8cbSVw8RnVzy5USe.htm)|Connaissance de l'énigme|`@Compendium[pf2e.feats-srd.8cbSVw8RnVzy5USe]`|
|[Enlarge Companion](feats/class-08-Tr2SnOE2WqFIIWIK.htm)|Compagnon aggrandi|`@Compendium[pf2e.feats-srd.Tr2SnOE2WqFIIWIK]`|
|[Enlightened Presence](feats/class-16-ROnjdPMvH0vkkWjQ.htm)|Présence éclairée|`@Compendium[pf2e.feats-srd.ROnjdPMvH0vkkWjQ]`|
|[Enthralling Allure](feats/ancestry-05-Zz5A8Yg0jGSK8GNu.htm)|Attrait fascinant|`@Compendium[pf2e.feats-srd.Zz5A8Yg0jGSK8GNu]`|
|[Entourage](feats/skill-07-6vwLzzrFfeiR9pm0.htm)|Entourage|`@Compendium[pf2e.feats-srd.6vwLzzrFfeiR9pm0]`|
|[Entreat With Forebears](feats/class-02-EaIczkGVI5DUo3c9.htm)|Implorer les ancêtres|`@Compendium[pf2e.feats-srd.EaIczkGVI5DUo3c9]`|
|[Envenom Fangs](feats/ancestry-05-IY86Kvopp4ACuQsw.htm)|Crocs empoisonnés|`@Compendium[pf2e.feats-srd.IY86Kvopp4ACuQsw]`|
|[Environmental Explorer](feats/archetype-04-vbHF6HEC9jQorFGl.htm)|Éclaireur environnemental|`@Compendium[pf2e.feats-srd.vbHF6HEC9jQorFGl]`|
|[Environmental Grace](feats/skill-07-u6tLp3zTBweq7CxO.htm)|Grâce environnementale|`@Compendium[pf2e.feats-srd.u6tLp3zTBweq7CxO]`|
|[Ephemeral Tracking](feats/class-06-BBN5G6epRWXGwZHv.htm)|Pistage éphémère|`@Compendium[pf2e.feats-srd.BBN5G6epRWXGwZHv]`|
|[Eschew Materials](feats/class-01-AD2eQu6SjLhUGD6Z.htm)|Dispense de composant matériel|`@Compendium[pf2e.feats-srd.AD2eQu6SjLhUGD6Z]`|
|[Esoteric Oath](feats/class-02-u3qBWp8ecalSwEk4.htm)|Serment ésotérique|`@Compendium[pf2e.feats-srd.u3qBWp8ecalSwEk4]`|
|[Esoteric Polymath](feats/class-02-4HZTLPKPteEFsa7n.htm)|Touche-à-tout ésotérique|`@Compendium[pf2e.feats-srd.4HZTLPKPteEFsa7n]`|
|[Esteemed Visitor](feats/ancestry-01-gC5EnlP38t1vTlWt.htm)|Visiteur estimé|`@Compendium[pf2e.feats-srd.gC5EnlP38t1vTlWt]`|
|[Eternal Bane](feats/class-16-pzmob1HqVKZfL0BY.htm)|Imprécation éternelle|`@Compendium[pf2e.feats-srd.pzmob1HqVKZfL0BY]`|
|[Eternal Blessing](feats/class-16-kMLvQnx2vY7F3bjI.htm)|Bénédiction éternelle|`@Compendium[pf2e.feats-srd.kMLvQnx2vY7F3bjI]`|
|[Eternal Composition](feats/class-18-yFoBVSOCnC2R2r8s.htm)|Composition perpétuelle|`@Compendium[pf2e.feats-srd.yFoBVSOCnC2R2r8s]`|
|[Eternal Elixir](feats/class-16-giOEclnMp8txkRSU.htm)|Élixir éternel|`@Compendium[pf2e.feats-srd.giOEclnMp8txkRSU]`|
|[Eternal Wings](feats/ancestry-17-OWL6ZNVWMU0AFqvZ.htm)|Ailes éternelles|`@Compendium[pf2e.feats-srd.OWL6ZNVWMU0AFqvZ]`|
|[Evangelize](feats/skill-07-YgbcLfAEdi4xxvX5.htm)|Évagélisation|`@Compendium[pf2e.feats-srd.YgbcLfAEdi4xxvX5]`|
|[Evasiveness (Rogue)](feats/class-12-VD446AflrQ3kO1al.htm)|Dérobade|`@Compendium[pf2e.feats-srd.VD446AflrQ3kO1al]`|
|[Evasiveness (Swashbuckler)](feats/archetype-12-BJfIGuUMItalNYet.htm)|Dérobade (Bretteur)|`@Compendium[pf2e.feats-srd.BJfIGuUMItalNYet]`|
|[Everstand Stance](feats/class-01-6GN1zh3RcnZhrzxP.htm)|Posture toujours en position|`@Compendium[pf2e.feats-srd.6GN1zh3RcnZhrzxP]`|
|[Everstand Strike](feats/class-04-3y459uK2qfWtS9q4.htm)|Frappe toujours en position|`@Compendium[pf2e.feats-srd.3y459uK2qfWtS9q4]`|
|[Everyone Duck!](feats/archetype-10-amf2zZuW299eiHAZ.htm)|À couvert !|`@Compendium[pf2e.feats-srd.amf2zZuW299eiHAZ]`|
|[Everyone's A Suspect](feats/class-20-w4dijKncXx0ssBOQ.htm)|Chacun est un suspect|`@Compendium[pf2e.feats-srd.w4dijKncXx0ssBOQ]`|
|[Exacting Strike](feats/class-01-8tS5NzytLmgbq5hF.htm)|Frappe contrôlée|`@Compendium[pf2e.feats-srd.8tS5NzytLmgbq5hF]`|
|[Exhort The Faithful](feats/skill-02-QvBIYW6aAqoiyim3.htm)|Exhorter les croyants|`@Compendium[pf2e.feats-srd.QvBIYW6aAqoiyim3]`|
|[Expand Aura](feats/class-16-H7Ocx80td7Sx7Cqn.htm)|Aura étendue|`@Compendium[pf2e.feats-srd.H7Ocx80td7Sx7Cqn]`|
|[Expanded Domain Initiate](feats/class-04-vQ4DNfpktmaqdgdM.htm)|Initié du domaine étendu|`@Compendium[pf2e.feats-srd.vQ4DNfpktmaqdgdM]`|
|[Expanded Luck](feats/ancestry-05-69h9D3syUYLgIPr7.htm)|Chance étendue|`@Compendium[pf2e.feats-srd.69h9D3syUYLgIPr7]`|
|[Expanded Splash](feats/class-10-gyVcJfZTmBytLsXq.htm)|Éclaboussure élargie|`@Compendium[pf2e.feats-srd.gyVcJfZTmBytLsXq]`|
|[Expeditious Advance](feats/archetype-06-0Gao1ez4dGH6dIZ2.htm)|Accélération motrice|`@Compendium[pf2e.feats-srd.0Gao1ez4dGH6dIZ2]`|
|[Expeditious Search](feats/general-07-GdZLxDtFXaQI3Fop.htm)|Fouille accélérée|`@Compendium[pf2e.feats-srd.GdZLxDtFXaQI3Fop]`|
|[Experienced Professional](feats/skill-01-sMm0UfYxEPpq2Yzd.htm)|Professionnel expérimenté|`@Compendium[pf2e.feats-srd.sMm0UfYxEPpq2Yzd]`|
|[Experienced Smuggler](feats/skill-01-t3btih0O5RUwWynt.htm)|Contrebandier expérimenté|`@Compendium[pf2e.feats-srd.t3btih0O5RUwWynt]`|
|[Experienced Tracker](feats/skill-01-urQZwmzg2kS53vd5.htm)|Pisteur expérimenté|`@Compendium[pf2e.feats-srd.urQZwmzg2kS53vd5]`|
|[Expert Alchemy](feats/archetype-06-soHLtpMM9h3AE7PD.htm)|Alchimie expert|`@Compendium[pf2e.feats-srd.soHLtpMM9h3AE7PD]`|
|[Expert Backstabber](feats/archetype-04-OQAo3Us0ODGYdNNn.htm)|Traître expert|`@Compendium[pf2e.feats-srd.OQAo3Us0ODGYdNNn]`|
|[Expert Bard Spellcasting](feats/archetype-12-0RB3f3J7gOEv3fni.htm)|Incantation du Barde expert|`@Compendium[pf2e.feats-srd.0RB3f3J7gOEv3fni]`|
|[Expert Cleric Spellcasting](feats/archetype-12-PLTOCEAvqBS05pZu.htm)|Incantantation du prêtre expert|`@Compendium[pf2e.feats-srd.PLTOCEAvqBS05pZu]`|
|[Expert Disassembler](feats/archetype-07-TkCy7jZUjhD8IypE.htm)|Démontage expert|`@Compendium[pf2e.feats-srd.TkCy7jZUjhD8IypE]`|
|[Expert Drill Sergeant](feats/ancestry-05-Fj3ufCawOM6fZB24.htm)|Sergent instructeur expert|`@Compendium[pf2e.feats-srd.Fj3ufCawOM6fZB24]`|
|[Expert Druid Spellcasting](feats/archetype-12-saEwTvJuiemEIfLm.htm)|Incantation du druide expert|`@Compendium[pf2e.feats-srd.saEwTvJuiemEIfLm]`|
|[Expert Eldritch Archer Spellcasting](feats/archetype-12-eoEYZJNdmvA5GfyK.htm)|Incantation expert de l'Archer mystique|`@Compendium[pf2e.feats-srd.eoEYZJNdmvA5GfyK]`|
|[Expert Herbalism](feats/archetype-06-owJorCBZmUi5lIV0.htm)|Herboriste expert|`@Compendium[pf2e.feats-srd.owJorCBZmUi5lIV0]`|
|[Expert Longevity](feats/ancestry-09-vfuHVSuExvtyajkW.htm)|Longévité experte|`@Compendium[pf2e.feats-srd.vfuHVSuExvtyajkW]`|
|[Expert Oracle Spellcasting](feats/archetype-12-FtO8DjjMLBtWiRhZ.htm)|Incantation d'Oracle experte|`@Compendium[pf2e.feats-srd.FtO8DjjMLBtWiRhZ]`|
|[Expert Poisoner](feats/archetype-06-VruIzuysxw4tY6rk.htm)|Empoisonneur expert|`@Compendium[pf2e.feats-srd.VruIzuysxw4tY6rk]`|
|[Expert Scroll Cache](feats/archetype-12-UrOj9TROtn8nuxPf.htm)|Réserve experte de parchemins|`@Compendium[pf2e.feats-srd.UrOj9TROtn8nuxPf]`|
|[Expert Sorcerer Spellcasting](feats/archetype-12-1k5PZAYth8u4Fqyr.htm)|Incantation de l'ensorceleur expert|`@Compendium[pf2e.feats-srd.1k5PZAYth8u4Fqyr]`|
|[Expert Witch Spellcasting](feats/archetype-12-x7vMKBSrxXmfs5C2.htm)|Incantation de sorcière experte|`@Compendium[pf2e.feats-srd.x7vMKBSrxXmfs5C2]`|
|[Expert Wizard Spellcasting](feats/archetype-12-hpCBELEKGA4ynYv4.htm)|Incantation du magicien expert|`@Compendium[pf2e.feats-srd.hpCBELEKGA4ynYv4]`|
|[Exploitive Bomb](feats/class-16-hr9maYUbtrNxpBPw.htm)|Bombe d'érosion|`@Compendium[pf2e.feats-srd.hr9maYUbtrNxpBPw]`|
|[Explosive Death Drop](feats/class-14-OEwNLolzBarx8icm.htm)|Projection explosive|`@Compendium[pf2e.feats-srd.OEwNLolzBarx8icm]`|
|[Express Rider](feats/skill-01-gArdEleFCvUHtdGk.htm)|Chevaucheur rapide|`@Compendium[pf2e.feats-srd.gArdEleFCvUHtdGk]`|
|[Extend Armament Alignment](feats/class-14-T3XFrLIBzir9IqD5.htm)|Prolongement de l'arsenal aligné|`@Compendium[pf2e.feats-srd.T3XFrLIBzir9IqD5]`|
|[Extend Elixir](feats/class-12-asRbkgW59DZUpvAq.htm)|Élixir étendu|`@Compendium[pf2e.feats-srd.asRbkgW59DZUpvAq]`|
|[Extra Squishy](feats/ancestry-01-2bNd89jYmEO8wSay.htm)|Extra spongieux|`@Compendium[pf2e.feats-srd.2bNd89jYmEO8wSay]`|
|[Eye For Numbers](feats/skill-01-0N8TtGSk5enoLBZ8.htm)|Œil pour les nombres|`@Compendium[pf2e.feats-srd.0N8TtGSk5enoLBZ8]`|
|[Eye For Treasure](feats/ancestry-01-nB8BD9rIg9hfFGns.htm)|Œil appréciateur|`@Compendium[pf2e.feats-srd.nB8BD9rIg9hfFGns]`|
|[Eye of Ozem](feats/archetype-04-FVDozRTuCQQzD97D.htm)|Œil d'Ozem|`@Compendium[pf2e.feats-srd.FVDozRTuCQQzD97D]`|
|[Eye of the Arclords](feats/skill-02-OtV7esAwza1U6Kwr.htm)|Œil des Seigneurs de l'Arc|`@Compendium[pf2e.feats-srd.OtV7esAwza1U6Kwr]`|
|[Eyes Of Night](feats/ancestry-01-5EpkOj9CFOjt8vsK.htm)|Yeux de la nuit|`@Compendium[pf2e.feats-srd.5EpkOj9CFOjt8vsK]`|
|[Fabricated Connections](feats/archetype-07-e6s2nIvlTycuzlR9.htm)|Réalisations inventées|`@Compendium[pf2e.feats-srd.e6s2nIvlTycuzlR9]`|
|[Fade Away](feats/ancestry-09-qr1E37Tla555tvIO.htm)|Disparition|`@Compendium[pf2e.feats-srd.qr1E37Tla555tvIO]`|
|[Fading](feats/archetype-10-orTWiRwIQEc9FGJQ.htm)|Effacement|`@Compendium[pf2e.feats-srd.orTWiRwIQEc9FGJQ]`|
|[False Faith](feats/class-01-6thEFvU7aMPmLrly.htm)|Foi de façade|`@Compendium[pf2e.feats-srd.6thEFvU7aMPmLrly]`|
|[Familiar](feats/class-01-bcxIg7wi8ZAhvhOD.htm)|Familier|`@Compendium[pf2e.feats-srd.bcxIg7wi8ZAhvhOD]`|
|[Familiar Conduit](feats/archetype-04-IYt6pMqiTocTrixA.htm)|Familier conducteur|`@Compendium[pf2e.feats-srd.IYt6pMqiTocTrixA]`|
|[Familiar Form](feats/archetype-08-EGtuOZ3E9y0qZ1oJ.htm)|Forme du familier|`@Compendium[pf2e.feats-srd.EGtuOZ3E9y0qZ1oJ]`|
|[Familiar Mascot](feats/archetype-04-Hwvrive2vBIqZUcE.htm)|Familier mascotte|`@Compendium[pf2e.feats-srd.Hwvrive2vBIqZUcE]`|
|[Familiar Master Dedication](feats/archetype-02-4zaU3GlTGMNqLFS8.htm)|Dévouement de Maître familier|`@Compendium[pf2e.feats-srd.4zaU3GlTGMNqLFS8]`|
|[Familiar's Eyes](feats/class-12-Q4puGx4kBMXy45fa.htm)|Yeux du familier|`@Compendium[pf2e.feats-srd.Q4puGx4kBMXy45fa]`|
|[Familiar's Language](feats/class-02-gKoNWXem1ikEqE2d.htm)|Langue du familier|`@Compendium[pf2e.feats-srd.gKoNWXem1ikEqE2d]`|
|[Fancy Moves](feats/skill-04-mwZzcwYVcTvxbXDl.htm)|Mouvements élaborés|`@Compendium[pf2e.feats-srd.mwZzcwYVcTvxbXDl]`|
|[Fane's Escape](feats/class-04-9SdFlVQW4vM8ggh8.htm)|Évasion du joueur|`@Compendium[pf2e.feats-srd.9SdFlVQW4vM8ggh8]`|
|[Fane's Fourberie](feats/class-02-80CEAB05TP5ki9iW.htm)|Fourberie de Fane|`@Compendium[pf2e.feats-srd.80CEAB05TP5ki9iW]`|
|[Fang Sharpener](feats/ancestry-01-FnGOkNyLF4w3FyqZ.htm)|Aiguiseur de crocs|`@Compendium[pf2e.feats-srd.FnGOkNyLF4w3FyqZ]`|
|[Fangs](feats/ancestry-01-Y8sKn8NH1wC7Mrui.htm)|Crocs|`@Compendium[pf2e.feats-srd.Y8sKn8NH1wC7Mrui]`|
|[Fantastic Leap](feats/class-12-qI5ZyuNVME95iXhJ.htm)|Bond fantastique|`@Compendium[pf2e.feats-srd.qI5ZyuNVME95iXhJ]`|
|[Far Lobber](feats/class-01-BJHsCiBLdjgJo6zM.htm)|Lanceur à grande distance|`@Compendium[pf2e.feats-srd.BJHsCiBLdjgJo6zM]`|
|[Far Shot](feats/class-04-r6dvGxru3FWNLVE2.htm)|Tir de loin|`@Compendium[pf2e.feats-srd.r6dvGxru3FWNLVE2]`|
|[Far Throw](feats/class-06-VYilg64xX9XpHeJr.htm)|Lancer lointain|`@Compendium[pf2e.feats-srd.VYilg64xX9XpHeJr]`|
|[Farabellus Flip](feats/class-04-PH5b61x3iJSKP3Xi.htm)|Retournement de Farabellus|`@Compendium[pf2e.feats-srd.PH5b61x3iJSKP3Xi]`|
|[Fascinating Performance](feats/skill-01-7LB00jkh6JaJr3vS.htm)|Représentation fascinante|`@Compendium[pf2e.feats-srd.7LB00jkh6JaJr3vS]`|
|[Fast Channel](feats/class-14-jwQERVkjtnlFp3Ec.htm)|Canalisation rapide|`@Compendium[pf2e.feats-srd.jwQERVkjtnlFp3Ec]`|
|[Fast Movement](feats/class-04-SCDSgeJU7vULvHmo.htm)|Déplacement rapide|`@Compendium[pf2e.feats-srd.SCDSgeJU7vULvHmo]`|
|[Fast Recovery](feats/general-01-N8Xz5fuW6o7GW124.htm)|Récupération rapide|`@Compendium[pf2e.feats-srd.N8Xz5fuW6o7GW124]`|
|[Fatal Aria](feats/class-20-3w2SktSOZdG8f6Qr.htm)|Aria fatale|`@Compendium[pf2e.feats-srd.3w2SktSOZdG8f6Qr]`|
|[Favored Enemy](feats/class-04-oGu9AtUAx0SpRXy8.htm)|Ennemi juré|`@Compendium[pf2e.feats-srd.oGu9AtUAx0SpRXy8]`|
|[Favored Terrain](feats/class-02-sgo7J9BVofBqwlsF.htm)|Environnement de prédilection|`@Compendium[pf2e.feats-srd.sgo7J9BVofBqwlsF]`|
|[Fear No Law, Fear No One](feats/bonus-00-AxjjAoU1IZaNUGGS.htm)||`@Compendium[pf2e.feats-srd.AxjjAoU1IZaNUGGS]`|
|[Fearsome Brute](feats/class-10-1k3H7cnARIzAVCsm.htm)|Brute effrayante|`@Compendium[pf2e.feats-srd.1k3H7cnARIzAVCsm]`|
|[Feather Step](feats/general-01-c9fHUSI5lRdXu1Ic.htm)|Pas de plume|`@Compendium[pf2e.feats-srd.c9fHUSI5lRdXu1Ic]`|
|[Felicitous Riposte](feats/class-16-D71A2ZQfz9MVndqI.htm)|Riposte heureuse|`@Compendium[pf2e.feats-srd.D71A2ZQfz9MVndqI]`|
|[Felling Shot](feats/class-12-Ij6BBPzZvOFZ3prs.htm)|Tir renversant|`@Compendium[pf2e.feats-srd.Ij6BBPzZvOFZ3prs]`|
|[Felling Strike](feats/class-08-Ad0XBuETAkMD6doj.htm)|Tir renversant|`@Compendium[pf2e.feats-srd.Ad0XBuETAkMD6doj]`|
|[Feral Mutagen](feats/class-08-gQ2EvesPqLbISLQV.htm)|Mutagène sauvage|`@Compendium[pf2e.feats-srd.gQ2EvesPqLbISLQV]`|
|[Ferocious Beasts](feats/ancestry-13-wteuGNhOXLvBudRQ.htm)|Bêtes féroces|`@Compendium[pf2e.feats-srd.wteuGNhOXLvBudRQ]`|
|[Ferocious Shape](feats/class-08-Le30algCdKIsxmeK.htm)|Morphologie féroce|`@Compendium[pf2e.feats-srd.Le30algCdKIsxmeK]`|
|[Fey Caller](feats/class-08-8rE5zLEVe4putosB.htm)|Celui qui appelle les fées|`@Compendium[pf2e.feats-srd.8rE5zLEVe4putosB]`|
|[Fey Fellowship](feats/ancestry-01-FWCULKnVXhSPL0ST.htm)|Camaraderie féerique|`@Compendium[pf2e.feats-srd.FWCULKnVXhSPL0ST]`|
|[Fey Influence](feats/general-05-dIIqejy4JAVuF0I8.htm)|Influence féérique|`@Compendium[pf2e.feats-srd.dIIqejy4JAVuF0I8]`|
|[Fiend's Door](feats/ancestry-13-rCajlx2KjGxzabAJ.htm)|Porte du fiélon|`@Compendium[pf2e.feats-srd.rCajlx2KjGxzabAJ]`|
|[Fiendish Eyes](feats/ancestry-01-LUXBuTAuK1glHOkJ.htm)|Yeux fiélons|`@Compendium[pf2e.feats-srd.LUXBuTAuK1glHOkJ]`|
|[Fiendish Form](feats/class-18-gtXy5gMNU0NDvkBL.htm)|Forme fiélone|`@Compendium[pf2e.feats-srd.gtXy5gMNU0NDvkBL]`|
|[Fiendish Lore](feats/ancestry-01-5g7OFtvYQ7wPPJHC.htm)|Connaissance des fiélons|`@Compendium[pf2e.feats-srd.5g7OFtvYQ7wPPJHC]`|
|[Fiendish Mount](feats/class-20-KTMzVCd6xAqCvxa5.htm)|Monture fiélone|`@Compendium[pf2e.feats-srd.KTMzVCd6xAqCvxa5]`|
|[Fiendish Resistance](feats/ancestry-05-eJ6CytjWx2sGnKnC.htm)|Résistance fiélone|`@Compendium[pf2e.feats-srd.eJ6CytjWx2sGnKnC]`|
|[Fiendish Strikes](feats/ancestry-13-xjE123p2rPTsz1eV.htm)|Frappes fiélones|`@Compendium[pf2e.feats-srd.xjE123p2rPTsz1eV]`|
|[Fiendish Wings](feats/ancestry-09-jNemcS5GqH8mnjV6.htm)|Ailes fiélones|`@Compendium[pf2e.feats-srd.jNemcS5GqH8mnjV6]`|
|[Fiendish Word](feats/ancestry-17-2pYCGPCQDHD3o7Jz.htm)|Parole fiélone|`@Compendium[pf2e.feats-srd.2pYCGPCQDHD3o7Jz]`|
|[Fiendsbane Oath](feats/class-02-aM6eAeyk5twmxCKe.htm)|Serment du fléau des fiélons|`@Compendium[pf2e.feats-srd.aM6eAeyk5twmxCKe]`|
|[Fighter Dedication](feats/archetype-02-mvNa9KfQooHYEXoA.htm)|Dévouement de guerrier|`@Compendium[pf2e.feats-srd.mvNa9KfQooHYEXoA]`|
|[Fighter Resiliency](feats/archetype-04-PLz1oIEGJojVUBsW.htm)|Résilience du Guerrier|`@Compendium[pf2e.feats-srd.PLz1oIEGJojVUBsW]`|
|[Finishing Follow-through](feats/class-02-DY6pNO3GzHeKSxmQ.htm)|Suivi de la botte|`@Compendium[pf2e.feats-srd.DY6pNO3GzHeKSxmQ]`|
|[Finishing Precision](feats/archetype-04-X5gNhaYNx1xu6NoH.htm)|Finition précise|`@Compendium[pf2e.feats-srd.X5gNhaYNx1xu6NoH]`|
|[Firebrand Braggart Dedication](feats/class-04-NagTqSLK8bAlo2nY.htm)|Dévouement d'Agitateur vantard|`@Compendium[pf2e.feats-srd.NagTqSLK8bAlo2nY]`|
|[First Revelation](feats/archetype-04-HdhnAm9SNfDqxRSN.htm)|Première révélation|`@Compendium[pf2e.feats-srd.HdhnAm9SNfDqxRSN]`|
|[First World Adept](feats/ancestry-09-WUVNZoIZvr9XFv2x.htm)|Adepte du premier monde|`@Compendium[pf2e.feats-srd.WUVNZoIZvr9XFv2x]`|
|[First World Magic](feats/ancestry-01-yJ8Ez5dEscIk1xr5.htm)|Magie du Premier monde|`@Compendium[pf2e.feats-srd.yJ8Ez5dEscIk1xr5]`|
|[Flamboyant Athlete](feats/class-04-tmGsnUkPv8SIhBgn.htm)|Athlète flamboyant|`@Compendium[pf2e.feats-srd.tmGsnUkPv8SIhBgn]`|
|[Flamboyant Cruelty](feats/class-08-SAOtGk9k8veaX3Ww.htm)|Cruauté flamboyante|`@Compendium[pf2e.feats-srd.SAOtGk9k8veaX3Ww]`|
|[Flamboyant Leap](feats/class-14-TdwC9rTGgtF4CQ25.htm)|Bond flamboyant|`@Compendium[pf2e.feats-srd.TdwC9rTGgtF4CQ25]`|
|[Fleet](feats/general-01-Ux73dmoF8KnavyUD.htm)|Vélocité|`@Compendium[pf2e.feats-srd.Ux73dmoF8KnavyUD]`|
|[Fleeting Shadow](feats/archetype-06-hE6fchGuHuPIeKlO.htm)|Ombre flottante|`@Compendium[pf2e.feats-srd.hE6fchGuHuPIeKlO]`|
|[Flensing Slice](feats/archetype-08-F6VlPyZZpqV6d2CS.htm)|Taille dépeçante|`@Compendium[pf2e.feats-srd.F6VlPyZZpqV6d2CS]`|
|[Flexible Halcyon Spellcasting](feats/archetype-12-UqA9GdO2pGQwg9cd.htm)|Incantation paisible flexible|`@Compendium[pf2e.feats-srd.UqA9GdO2pGQwg9cd]`|
|[Flexible Ritualist](feats/archetype-06-bmWvMfYxZbZtigDp.htm)|Ritualiste polyvalent|`@Compendium[pf2e.feats-srd.bmWvMfYxZbZtigDp]`|
|[Flexible Studies](feats/class-01-9bgl6qYWKHzqWZj0.htm)|Études diversifiées|`@Compendium[pf2e.feats-srd.9bgl6qYWKHzqWZj0]`|
|[Flicker](feats/archetype-12-SyxXnSk2R0AM9HSn.htm)|Image vacillante|`@Compendium[pf2e.feats-srd.SyxXnSk2R0AM9HSn]`|
|[Flinging Blow](feats/class-16-uL6q4wtwvuP8I4po.htm)|Coup catapultant|`@Compendium[pf2e.feats-srd.uL6q4wtwvuP8I4po]`|
|[Flinging Shove](feats/class-12-d6Vb8D9yOX93mdUI.htm)|Poussée catapulteuse|`@Compendium[pf2e.feats-srd.d6Vb8D9yOX93mdUI]`|
|[Flourishing Finish](feats/archetype-12-isdTXU8bV7ZVOAuQ.htm)|Mise à mort spectaculaire|`@Compendium[pf2e.feats-srd.isdTXU8bV7ZVOAuQ]`|
|[Flurry of Maneuvers](feats/class-04-wz2edbLFnDKDNWWZ.htm)|Déluges de manoeuvres|`@Compendium[pf2e.feats-srd.wz2edbLFnDKDNWWZ]`|
|[Flying Blade](feats/class-01-qgNc5XwjsaWET0Op.htm)|Lame volante|`@Compendium[pf2e.feats-srd.qgNc5XwjsaWET0Op]`|
|[Flying Kick](feats/class-04-wB1ONG2uO7RnD1iC.htm)|Coup de pied sauté|`@Compendium[pf2e.feats-srd.wB1ONG2uO7RnD1iC]`|
|[Focus Ally](feats/archetype-16-zodscjgydZRUSOLO.htm)|Focalisation d'allié|`@Compendium[pf2e.feats-srd.zodscjgydZRUSOLO]`|
|[Focused Fascination](feats/class-01-5GqjEM22n78Vmdpe.htm)|Fascination concentrée|`@Compendium[pf2e.feats-srd.5GqjEM22n78Vmdpe]`|
|[Focused Juggler](feats/archetype-04-lYVAGuHU47Ixyuxy.htm)|Jongleur concentré|`@Compendium[pf2e.feats-srd.lYVAGuHU47Ixyuxy]`|
|[Focused Shot](feats/class-12-d7DQhCJKYcLxpHen.htm)|Tir concentré|`@Compendium[pf2e.feats-srd.d7DQhCJKYcLxpHen]`|
|[Foil Senses](feats/skill-07-IJQJBnD5CjKvFYEx.htm)|Déjouer les sens|`@Compendium[pf2e.feats-srd.IJQJBnD5CjKvFYEx]`|
|[Folksy Patter](feats/ancestry-01-0YXRPqCaOQ3G73hh.htm)|Boniment campagnard|`@Compendium[pf2e.feats-srd.0YXRPqCaOQ3G73hh]`|
|[Follow-up Assault](feats/class-08-9Q0tPGtOawPTU2TU.htm)|Assaut complémentaire|`@Compendium[pf2e.feats-srd.9Q0tPGtOawPTU2TU]`|
|[Follow-up Strike](feats/archetype-06-m0ot9Qydb9SYWHis.htm)|Frappe enchaînée|`@Compendium[pf2e.feats-srd.m0ot9Qydb9SYWHis]`|
|[Font of Knowledge](feats/class-10-o8ogNJ53l1JDIJud.htm)|Puits de connaissances|`@Compendium[pf2e.feats-srd.o8ogNJ53l1JDIJud]`|
|[Foolproof Instructions](feats/archetype-08-agWNAYqgyV58jlxm.htm)|Instructions infaillibles|`@Compendium[pf2e.feats-srd.agWNAYqgyV58jlxm]`|
|[Forager](feats/skill-01-8qebBeOJsyRIchcu.htm)|Glaneur|`@Compendium[pf2e.feats-srd.8qebBeOJsyRIchcu]`|
|[Forced Entry](feats/archetype-06-uwJQUFLymAWtJu1a.htm)|Entrée forcée|`@Compendium[pf2e.feats-srd.uwJQUFLymAWtJu1a]`|
|[Forceful Shot](feats/archetype-14-0qL4a3CarG1e0pfB.htm)|Tir repoussant|`@Compendium[pf2e.feats-srd.0qL4a3CarG1e0pfB]`|
|[Forcible Energy](feats/class-12-UJafwv306v75Syy7.htm)|Énergie vigoureuse|`@Compendium[pf2e.feats-srd.UJafwv306v75Syy7]`|
|[Forensic Acumen](feats/skill-01-Ws9JlysHcFoz6WAQ.htm)|Perspicacité forensique|`@Compendium[pf2e.feats-srd.Ws9JlysHcFoz6WAQ]`|
|[Foresee Danger](feats/class-12-pVDgiaqu1RbCOhuv.htm)|Danger anticipé|`@Compendium[pf2e.feats-srd.pVDgiaqu1RbCOhuv]`|
|[Forest Stealth](feats/ancestry-05-zgriBCYR4TmBoDqO.htm)|Discrétion forestière|`@Compendium[pf2e.feats-srd.zgriBCYR4TmBoDqO]`|
|[Forestall Curse](feats/class-14-NNeRv9Gcua1kMp4s.htm)|Malédiction Contrecarrée|`@Compendium[pf2e.feats-srd.NNeRv9Gcua1kMp4s]`|
|[Forge-Day's Rest](feats/ancestry-01-ZxiAMposVPDNPwxI.htm)|Repos du jour de forge|`@Compendium[pf2e.feats-srd.ZxiAMposVPDNPwxI]`|
|[Forlorn](feats/ancestry-01-AogIo1gHLdz7DyHx.htm)|Délaissé|`@Compendium[pf2e.feats-srd.AogIo1gHLdz7DyHx]`|
|[Form Control](feats/class-04-RsluSLtSWq1vN8Hc.htm)|Contrôle de la forme|`@Compendium[pf2e.feats-srd.RsluSLtSWq1vN8Hc]`|
|[Form Lock](feats/class-14-0EY2WQC3Hb6Mitgz.htm)|Verrouillage de forme|`@Compendium[pf2e.feats-srd.0EY2WQC3Hb6Mitgz]`|
|[Form Of The Bat](feats/ancestry-13-H965m1koFvY4FQkF.htm)|Forme de la chauve-souris|`@Compendium[pf2e.feats-srd.H965m1koFvY4FQkF]`|
|[Form Of The Fiend](feats/ancestry-01-qEOiC2YXYzc8ZZFS.htm)|Forme du fiélon|`@Compendium[pf2e.feats-srd.qEOiC2YXYzc8ZZFS]`|
|[Form Retention](feats/class-08-l8KQgN8icNrzYIav.htm)|Conservation de forme|`@Compendium[pf2e.feats-srd.l8KQgN8icNrzYIav]`|
|[Formation Master](feats/ancestry-13-QHPtlFvTpgx1GE7S.htm)|Maître du combat en formation|`@Compendium[pf2e.feats-srd.QHPtlFvTpgx1GE7S]`|
|[Formation Training](feats/ancestry-05-qJw7zEMn0k5kp9ti.htm)|Combat en formation|`@Compendium[pf2e.feats-srd.qJw7zEMn0k5kp9ti]`|
|[Fortified Flesh](feats/archetype-08-7vOVPzsVuyE5a3Rp.htm)|Renforcement de la chair|`@Compendium[pf2e.feats-srd.7vOVPzsVuyE5a3Rp]`|
|[Fortutious Shift](feats/ancestry-09-Twhkz2FfzaZezVnG.htm)|Phasage fortuit|`@Compendium[pf2e.feats-srd.Twhkz2FfzaZezVnG]`|
|[Freeze It!](feats/ancestry-09-LhpE0NsfNwYP6MOz.htm)|Gêle-le !|`@Compendium[pf2e.feats-srd.LhpE0NsfNwYP6MOz]`|
|[Fresh Ingredients](feats/skill-02-cmuvvPJvt2R16vGe.htm)|Ingrédients frais|`@Compendium[pf2e.feats-srd.cmuvvPJvt2R16vGe]`|
|[Friendly Toss](feats/class-08-V9kShXu84NlORfcg.htm)|Lancer amical|`@Compendium[pf2e.feats-srd.V9kShXu84NlORfcg]`|
|[Frightening Appearance](feats/archetype-12-Ano4tRq88V39eyPq.htm)|Apparition effrayante|`@Compendium[pf2e.feats-srd.Ano4tRq88V39eyPq]`|
|[Fulminating Synergy](feats/archetype-16-XpZkzUV9PwUHvmyq.htm)|Synergie fulminante|`@Compendium[pf2e.feats-srd.XpZkzUV9PwUHvmyq]`|
|[Furious Bully](feats/class-08-cA1IIy6UEsgETXiX.htm)|Cogneur brutal|`@Compendium[pf2e.feats-srd.cA1IIy6UEsgETXiX]`|
|[Furious Finish](feats/class-02-5gnBhockV7O32jTR.htm)|Final furieux|`@Compendium[pf2e.feats-srd.5gnBhockV7O32jTR]`|
|[Furious Focus](feats/class-06-y61mDkTqk2k77b4x.htm)|Focalisation furieuse|`@Compendium[pf2e.feats-srd.y61mDkTqk2k77b4x]`|
|[Furious Grab](feats/class-12-7HPXQvPH3ovwtVae.htm)|Empoignaded furieuse|`@Compendium[pf2e.feats-srd.7HPXQvPH3ovwtVae]`|
|[Furious Sprint](feats/class-10-p2I4o9Cc6UrXvjhO.htm)|Course furieuse|`@Compendium[pf2e.feats-srd.p2I4o9Cc6UrXvjhO]`|
|[Furious Vengeance](feats/class-16-Oyml3OGNy468z3XI.htm)|Vengeance furieuse|`@Compendium[pf2e.feats-srd.Oyml3OGNy468z3XI]`|
|[Fuse Stance](feats/class-20-kceciNwoldkzAMbq.htm)|Fusion de postures|`@Compendium[pf2e.feats-srd.kceciNwoldkzAMbq]`|
|[Gang Up](feats/class-06-XRahcvEPEAEdGUn8.htm)|Attaque groupée|`@Compendium[pf2e.feats-srd.XRahcvEPEAEdGUn8]`|
|[Garden Path](feats/archetype-08-bXoGskH0SYfdcEtJ.htm)|Chemin du jardin|`@Compendium[pf2e.feats-srd.bXoGskH0SYfdcEtJ]`|
|[Gaze of Veracity](feats/class-10-MgLUbsvAkIA4fsZW.htm)|Regard de véracité|`@Compendium[pf2e.feats-srd.MgLUbsvAkIA4fsZW]`|
|[Gecko's Grip](feats/ancestry-05-kGed3uQbZ7x5SBB8.htm)|Prise du gecko|`@Compendium[pf2e.feats-srd.kGed3uQbZ7x5SBB8]`|
|[General Training](feats/ancestry-01-D3SuA5MaKucO1flE.htm)|Formation généraliste|`@Compendium[pf2e.feats-srd.D3SuA5MaKucO1flE]`|
|[Genius Mutagen](feats/class-16-mj1pVVFtqGLKgCQM.htm)|Mutagène de génie|`@Compendium[pf2e.feats-srd.mj1pVVFtqGLKgCQM]`|
|[Ghost Hunter](feats/ancestry-01-DGtUIMliflzGXc6E.htm)|Chasseur de fantômes|`@Compendium[pf2e.feats-srd.DGtUIMliflzGXc6E]`|
|[Ghost Hunter Dedication](feats/archetype-02-mkp6lhBbTASEmKwY.htm)|Dévouement de chasseur de fantômes|`@Compendium[pf2e.feats-srd.mkp6lhBbTASEmKwY]`|
|[Ghost Strike](feats/archetype-06-pM0g4ColXTiQ3gTa.htm)|Frappe spectrale|`@Compendium[pf2e.feats-srd.pM0g4ColXTiQ3gTa]`|
|[Giant Snare](feats/archetype-10-SgXvw6rzk2lhTpXL.htm)|Piège artisanal géant|`@Compendium[pf2e.feats-srd.SgXvw6rzk2lhTpXL]`|
|[Giant's Lunge](feats/class-14-MFqFvuiYDAoADcft.htm)|Fente de géant|`@Compendium[pf2e.feats-srd.MFqFvuiYDAoADcft]`|
|[Giant's Stature](feats/class-06-W21jKAcG0GtEtBiK.htm)|Stature de géant|`@Compendium[pf2e.feats-srd.W21jKAcG0GtEtBiK]`|
|[Glad-Hand](feats/skill-02-cc8O47KFsODReoBe.htm)|Cordialité forcée|`@Compendium[pf2e.feats-srd.cc8O47KFsODReoBe]`|
|[Gladiator Dedication](feats/archetype-02-Dm0YMEvSY0qg0jA0.htm)|Dévouement du Gladiateur|`@Compendium[pf2e.feats-srd.Dm0YMEvSY0qg0jA0]`|
|[Glean Contents](feats/skill-01-WeQGWvlWdeLeOlCN.htm)|Glaner des informations|`@Compendium[pf2e.feats-srd.WeQGWvlWdeLeOlCN]`|
|[Glean Lore](feats/class-01-MJW4VP7PjVAX131C.htm)|Glaner le savoir|`@Compendium[pf2e.feats-srd.MJW4VP7PjVAX131C]`|
|[Glib Mutagen](feats/class-14-HxkAhxcGvua6SkfS.htm)|Mutagène de bagou|`@Compendium[pf2e.feats-srd.HxkAhxcGvua6SkfS]`|
|[Gloomseer](feats/ancestry-01-sL2GmYve5NXJ0wc1.htm)|Voyant pénombral|`@Compendium[pf2e.feats-srd.sL2GmYve5NXJ0wc1]`|
|[Glyph Expert](feats/archetype-08-qUBr1YsQw3BSNy9c.htm)|Expert des glyphes|`@Compendium[pf2e.feats-srd.qUBr1YsQw3BSNy9c]`|
|[Gnome Obsession](feats/ancestry-01-mxhGH4FVYXJwb0BC.htm)|Obsession Gnome|`@Compendium[pf2e.feats-srd.mxhGH4FVYXJwb0BC]`|
|[Gnome Polyglot](feats/ancestry-01-ccWf2F5DqiqFwiQ1.htm)|Polyglotte gnome|`@Compendium[pf2e.feats-srd.ccWf2F5DqiqFwiQ1]`|
|[Gnome Weapon Expertise](feats/ancestry-13-j49fEU2TWJUaxD30.htm)|Expertise avec les armes gnomes|`@Compendium[pf2e.feats-srd.j49fEU2TWJUaxD30]`|
|[Gnome Weapon Familiarity](feats/ancestry-01-3zr5Gt5LgHsMNpSO.htm)|Familiarité avec les armes gnomes|`@Compendium[pf2e.feats-srd.3zr5Gt5LgHsMNpSO]`|
|[Gnome Weapon Innovator](feats/ancestry-05-OqJMjuK61uST7AlM.htm)|Innovation avec les armes gnomes|`@Compendium[pf2e.feats-srd.OqJMjuK61uST7AlM]`|
|[Goading Feint](feats/class-01-hXYnwpi95E77qfAu.htm)|Feinte provoquante|`@Compendium[pf2e.feats-srd.hXYnwpi95E77qfAu]`|
|[Goblin Lore](feats/ancestry-01-sm6Y3fTcltxE8N0p.htm)|Connaissance des gobelins|`@Compendium[pf2e.feats-srd.sm6Y3fTcltxE8N0p]`|
|[Goblin Scuttle](feats/ancestry-01-UwH0sGIthv8kiPUt.htm)|Précipitation gobeline|`@Compendium[pf2e.feats-srd.UwH0sGIthv8kiPUt]`|
|[Goblin Song](feats/ancestry-01-QZb0Utg0WFPf2Qg0.htm)|Chant Gobelin|`@Compendium[pf2e.feats-srd.QZb0Utg0WFPf2Qg0]`|
|[Goblin Weapon Expertise](feats/ancestry-13-607fIUg4JjG0aIRx.htm)|Expertise avec les armes gobelines|`@Compendium[pf2e.feats-srd.607fIUg4JjG0aIRx]`|
|[Goblin Weapon Familiarity](feats/ancestry-01-OYjzfTeWU7RJBT7v.htm)|Familiarité avec les armes gobelines|`@Compendium[pf2e.feats-srd.OYjzfTeWU7RJBT7v]`|
|[Goblin Weapon Frenzy](feats/ancestry-05-FLuv8uI1KoodNgY4.htm)|Frénésie avec les armes gobelines|`@Compendium[pf2e.feats-srd.FLuv8uI1KoodNgY4]`|
|[Godless Healing](feats/skill-02-yTLGclKtWVFZLKIz.htm)|Guérison athée|`@Compendium[pf2e.feats-srd.yTLGclKtWVFZLKIz]`|
|[Golden Body](feats/class-20-uwp7Y4LNtPbhELjS.htm)|Corps doré|`@Compendium[pf2e.feats-srd.uwp7Y4LNtPbhELjS]`|
|[Golem Grafter Dedication](feats/archetype-08-MxNb97qr1yMhbjiP.htm)|Dévouement de Greffeur de golem|`@Compendium[pf2e.feats-srd.MxNb97qr1yMhbjiP]`|
|[Gorilla Pound](feats/class-06-nRjyyDulHnP5OewA.htm)|Battement du gorille|`@Compendium[pf2e.feats-srd.nRjyyDulHnP5OewA]`|
|[Gorilla Stance](feats/class-01-DqD7htz8Sd1dh3BT.htm)|Posture de gorille|`@Compendium[pf2e.feats-srd.DqD7htz8Sd1dh3BT]`|
|[Gossip Lore](feats/archetype-04-7jqBwXq9jVsghCva.htm)|Connaissance des ragots|`@Compendium[pf2e.feats-srd.7jqBwXq9jVsghCva]`|
|[Graceful Leaper](feats/archetype-07-63sSYk5yqiAyZGb9.htm)|Sauteur gracieux|`@Compendium[pf2e.feats-srd.63sSYk5yqiAyZGb9]`|
|[Graceful Poise](feats/class-16-rFaUJtB46scuAidY.htm)|Aisance gracieuse|`@Compendium[pf2e.feats-srd.rFaUJtB46scuAidY]`|
|[Grasping Reach](feats/ancestry-01-weYZzyMmlCIC2TZt.htm)|Allonge agrippante|`@Compendium[pf2e.feats-srd.weYZzyMmlCIC2TZt]`|
|[Grave Sense](feats/archetype-06-eXNkcM7gtCGC7udi.htm)|Perception du tombeau|`@Compendium[pf2e.feats-srd.eXNkcM7gtCGC7udi]`|
|[Grave's Voice](feats/archetype-04-shDyS87L0eiabyHw.htm)|Voix du Tombeau|`@Compendium[pf2e.feats-srd.shDyS87L0eiabyHw]`|
|[Gravesight](feats/ancestry-01-hxCqQPjlyVI57vQt.htm)|Vision du tombeau|`@Compendium[pf2e.feats-srd.hxCqQPjlyVI57vQt]`|
|[Gravity Weapon](feats/class-01-iT39wlCEC1aWaSx7.htm)|Arme pesante|`@Compendium[pf2e.feats-srd.iT39wlCEC1aWaSx7]`|
|[Great Boaster](feats/archetype-08-4Ek3Kyle2DsCPQQm.htm)|Grande vantardise|`@Compendium[pf2e.feats-srd.4Ek3Kyle2DsCPQQm]`|
|[Great Cleave](feats/class-10-cznEQ1W61MSaXW0u.htm)|Grand coup tranchant|`@Compendium[pf2e.feats-srd.cznEQ1W61MSaXW0u]`|
|[Great Tengu Form](feats/ancestry-17-ABXUfGoeBgCyFasg.htm)|Forme du grand tengu|`@Compendium[pf2e.feats-srd.ABXUfGoeBgCyFasg]`|
|[Greater Bloodline](feats/class-10-DpRMdytpPiCypmkJ.htm)|Lignage supérieur|`@Compendium[pf2e.feats-srd.DpRMdytpPiCypmkJ]`|
|[Greater Crossblooded Evolution](feats/class-18-doD3jZylVXZ0oHWO.htm)|Évolution métisée supérieure|`@Compendium[pf2e.feats-srd.doD3jZylVXZ0oHWO]`|
|[Greater Cruelty](feats/class-08-u2rvvAqZBugZgcYg.htm)|Cruauté supérieure|`@Compendium[pf2e.feats-srd.u2rvvAqZBugZgcYg]`|
|[Greater Debilitating Bomb](feats/class-10-Cn4w9U7uk5m1bb2S.htm)|Bombe incapacitante supérieure|`@Compendium[pf2e.feats-srd.Cn4w9U7uk5m1bb2S]`|
|[Greater Distracting Shot](feats/class-16-vDJRIKS27md3LudA.htm)|Tir déroutant supérieur|`@Compendium[pf2e.feats-srd.vDJRIKS27md3LudA]`|
|[Greater Interpose](feats/class-14-ygPrwqiyDr1frUHw.htm)|Interposition supérieure|`@Compendium[pf2e.feats-srd.ygPrwqiyDr1frUHw]`|
|[Greater Lesson](feats/class-06-qDfTqetM9UEpp8ty.htm)|Leçon supérieure|`@Compendium[pf2e.feats-srd.qDfTqetM9UEpp8ty]`|
|[Greater Magical Edification](feats/archetype-10-jBHDVsTVOBeoMoO4.htm)|Édification magique supérieure|`@Compendium[pf2e.feats-srd.jBHDVsTVOBeoMoO4]`|
|[Greater Magical Scholastics](feats/archetype-10-ErKwliHplziJY2BW.htm)|Études magiques supérieures|`@Compendium[pf2e.feats-srd.ErKwliHplziJY2BW]`|
|[Greater Mental Evolution](feats/class-16-8HYfYT4fHtxXP199.htm)|Évolution mentale supérieure|`@Compendium[pf2e.feats-srd.8HYfYT4fHtxXP199]`|
|[Greater Merciful Elixir](feats/class-14-QpnZwabXOVICJL5i.htm)|Élixir miséricordieux supérieur|`@Compendium[pf2e.feats-srd.QpnZwabXOVICJL5i]`|
|[Greater Mercy](feats/class-08-kZdcoaTD849QalR9.htm)|Soulagement supérieur|`@Compendium[pf2e.feats-srd.kZdcoaTD849QalR9]`|
|[Greater Physical Evolution](feats/class-12-O1qdoz5N3G4yvHcH.htm)|Évolution physique supérieure|`@Compendium[pf2e.feats-srd.O1qdoz5N3G4yvHcH]`|
|[Greater Revelation](feats/class-12-HSW3N9pfHhM7upRB.htm)|Révélation supérieure|`@Compendium[pf2e.feats-srd.HSW3N9pfHhM7upRB]`|
|[Greater Spell Runes](feats/archetype-10-hRJV7byfPUHx1b9P.htm)|Rune de sort supérieure|`@Compendium[pf2e.feats-srd.hRJV7byfPUHx1b9P]`|
|[Greater Spiritual Evolution](feats/class-12-gVLICIDQMvWN5D89.htm)|Évolution spirituelle supérieure|`@Compendium[pf2e.feats-srd.gVLICIDQMvWN5D89]`|
|[Greater Vital Evolution](feats/class-16-ND3nKsXCDBShUgYc.htm)|Évolution vitale supérieure|`@Compendium[pf2e.feats-srd.ND3nKsXCDBShUgYc]`|
|[Green Empathy](feats/class-06-3vjOXL9ZD4ibaJL6.htm)|Empathie végétale|`@Compendium[pf2e.feats-srd.3vjOXL9ZD4ibaJL6]`|
|[Green Tongue](feats/class-12-vwBD55BRDOatp4ZV.htm)|Langue végétale|`@Compendium[pf2e.feats-srd.vwBD55BRDOatp4ZV]`|
|[Grievous Blow](feats/archetype-08-C3zKTQecexSbezhT.htm)|Coup douloureux|`@Compendium[pf2e.feats-srd.C3zKTQecexSbezhT]`|
|[Grim Insight](feats/ancestry-01-zs2FFGI88zB7EaBT.htm)|Intérieur sombre|`@Compendium[pf2e.feats-srd.zs2FFGI88zB7EaBT]`|
|[Grimspawn](feats/ancestry-01-nyhQ9xB0rkoAoNbf.htm)|Rejeton sombre|`@Compendium[pf2e.feats-srd.nyhQ9xB0rkoAoNbf]`|
|[Group Aid](feats/ancestry-09-sGdREpnSJDzEacub.htm)|Aide de groupe|`@Compendium[pf2e.feats-srd.sGdREpnSJDzEacub]`|
|[Group Coercion](feats/skill-01-gUqvezs2zzoTXFAI.htm)|Contraindre un groupe|`@Compendium[pf2e.feats-srd.gUqvezs2zzoTXFAI]`|
|[Group Impression](feats/skill-01-KpFetnUqTiweypZk.htm)|Bonne impression de groupe|`@Compendium[pf2e.feats-srd.KpFetnUqTiweypZk]`|
|[Grovel](feats/ancestry-05-gS9FYlD0Vt8yyZkP.htm)|Servilité|`@Compendium[pf2e.feats-srd.gS9FYlD0Vt8yyZkP]`|
|[Gruesome Strike](feats/class-12-PEszRpnrcB7VPS9G.htm)|Frappe horrible|`@Compendium[pf2e.feats-srd.PEszRpnrcB7VPS9G]`|
|[Guarded Movement](feats/class-04-fO1vRDEfl9pysfLU.htm)|Déplacement circonspect|`@Compendium[pf2e.feats-srd.fO1vRDEfl9pysfLU]`|
|[Guardian's Deflection (Fighter)](feats/class-06-JdCRxwgtdQkJ1Ha6.htm)|Déviation du gardien (Guerrier)|`@Compendium[pf2e.feats-srd.JdCRxwgtdQkJ1Ha6]`|
|[Guardian's Deflection (Swashbuckler)](feats/class-04-YJIzE2RhGRGfbt9j.htm)|Déviation du gardien (Bretteur)|`@Compendium[pf2e.feats-srd.YJIzE2RhGRGfbt9j]`|
|[Guiding Finish](feats/class-14-9t6Kfk8Yw8WJYY8Z.htm)|Final directif|`@Compendium[pf2e.feats-srd.9t6Kfk8Yw8WJYY8Z]`|
|[Guiding Luck](feats/ancestry-09-k0sXAn4PPq5nW9al.htm)|Chance directrice|`@Compendium[pf2e.feats-srd.k0sXAn4PPq5nW9al]`|
|[Guiding Riposte](feats/class-14-1DaSVLJEdJWYOWek.htm)|Riposte directive|`@Compendium[pf2e.feats-srd.1DaSVLJEdJWYOWek]`|
|[Hag Claws](feats/ancestry-01-Qb25uu1gT5CDMSWb.htm)|Griffes de guenaude|`@Compendium[pf2e.feats-srd.Qb25uu1gT5CDMSWb]`|
|[Hag Magic](feats/ancestry-13-t6GBBIwX7hvvxYyV.htm)|Magie des guenaudes|`@Compendium[pf2e.feats-srd.t6GBBIwX7hvvxYyV]`|
|[Hag's Sight](feats/ancestry-01-WzJaArukCUf9hpeP.htm)|Vision de la guenaude|`@Compendium[pf2e.feats-srd.WzJaArukCUf9hpeP]`|
|[Halcyon Speaker Dedication](feats/class-06-7fU6e3HIT4NvwLYa.htm)|Dévouement de l'Orateur paisible|`@Compendium[pf2e.feats-srd.7fU6e3HIT4NvwLYa]`|
|[Halcyon Spellcasting Adept](feats/archetype-14-LDv6RVuDXJ9nOfhj.htm)|Adepte de l'Incantation paisible|`@Compendium[pf2e.feats-srd.LDv6RVuDXJ9nOfhj]`|
|[Halcyon Spellcasting Initiated](feats/archetype-10-HgBksiMTUibPK36M.htm)|Initié de l'Incantation paisible|`@Compendium[pf2e.feats-srd.HgBksiMTUibPK36M]`|
|[Halcyon Spellcasting Sage](feats/archetype-18-iuLLw1X5RjRcR4rH.htm)|Sage de l'Incantation paisible|`@Compendium[pf2e.feats-srd.iuLLw1X5RjRcR4rH]`|
|[Halfling Ingenuity](feats/ancestry-05-jNrpvEqfncdGZPak.htm)|Ingénuité halfeline|`@Compendium[pf2e.feats-srd.jNrpvEqfncdGZPak]`|
|[Halfling Lore](feats/ancestry-01-VU07hybqzUXIJ6l2.htm)|Connaissance des halfelins|`@Compendium[pf2e.feats-srd.VU07hybqzUXIJ6l2]`|
|[Halfling Luck](feats/ancestry-01-ZbRVqf14RTJJIZXG.htm)|Chance halfeline|`@Compendium[pf2e.feats-srd.ZbRVqf14RTJJIZXG]`|
|[Halfling Weapon Expertise](feats/ancestry-13-gsW6mCMBWquLM3bj.htm)|Expertise avec les armes halfelines|`@Compendium[pf2e.feats-srd.gsW6mCMBWquLM3bj]`|
|[Halfling Weapon Familiarity](feats/ancestry-01-2ebcYbg68pCZfAFQ.htm)|Familiarité avec les armes halfelines|`@Compendium[pf2e.feats-srd.2ebcYbg68pCZfAFQ]`|
|[Halfling Weapon Trickster](feats/ancestry-05-9WsYVbe47aSADs1Q.htm)|Ruse avec les armes halfelines|`@Compendium[pf2e.feats-srd.9WsYVbe47aSADs1Q]`|
|[Halo](feats/ancestry-01-PGVXjbAi1Fa4uTmD.htm)|Halo|`@Compendium[pf2e.feats-srd.PGVXjbAi1Fa4uTmD]`|
|[Hammer Quake](feats/archetype-14-CR9NcAIPTT4oWSEy.htm)|Séisme du Marteau|`@Compendium[pf2e.feats-srd.CR9NcAIPTT4oWSEy]`|
|[Hand of the Apprentice](feats/class-01-pD1oDbUDkNtHadGY.htm)|Main de l'apprenti|`@Compendium[pf2e.feats-srd.pD1oDbUDkNtHadGY]`|
|[Handy with your Paws](feats/ancestry-01-1newzNV5nkLvZ9KE.htm)|Adroit de vos pattes|`@Compendium[pf2e.feats-srd.1newzNV5nkLvZ9KE]`|
|[Hard Tail](feats/ancestry-01-0qeYP84FfQueggkx.htm)|Queue dure|`@Compendium[pf2e.feats-srd.0qeYP84FfQueggkx]`|
|[Hardy Traveler](feats/ancestry-09-aznyI5mfMdEFSDr8.htm)|Voyageur robuste|`@Compendium[pf2e.feats-srd.aznyI5mfMdEFSDr8]`|
|[Harming Hands](feats/class-01-1W0a6YCyoYv8dm4e.htm)|Mains blessantes|`@Compendium[pf2e.feats-srd.1W0a6YCyoYv8dm4e]`|
|[Harmlessly Cute](feats/ancestry-01-GYdLf1LhvSUeu95Y.htm)|Inoffensivement mignon|`@Compendium[pf2e.feats-srd.GYdLf1LhvSUeu95Y]`|
|[Harmonize](feats/class-06-21YWBdoXGmj60vdI.htm)|Harmoniser|`@Compendium[pf2e.feats-srd.21YWBdoXGmj60vdI]`|
|[Harrying Strike](feats/archetype-10-fCsIyglmpb7NYwiy.htm)|Frappe harassante|`@Compendium[pf2e.feats-srd.fCsIyglmpb7NYwiy]`|
|[Haughty Obstinacy](feats/ancestry-01-RmiMUZlae6yGUyXY.htm)|Obstination arrogante|`@Compendium[pf2e.feats-srd.RmiMUZlae6yGUyXY]`|
|[Hazard Finder](feats/class-08-16MOW7deoOoDwE9z.htm)|Dénicheur de dangers|`@Compendium[pf2e.feats-srd.16MOW7deoOoDwE9z]`|
|[Head Stomp](feats/class-04-agfosPInBLQXNQfa.htm)|Piétiner la tête|`@Compendium[pf2e.feats-srd.agfosPInBLQXNQfa]`|
|[Heal Animal](feats/archetype-04-OJxEF5FONTtEdbpP.htm)|Guérison des animaux|`@Compendium[pf2e.feats-srd.OJxEF5FONTtEdbpP]`|
|[Heal Companion](feats/class-01-FhxkU6OftQeecpQW.htm)|Guérison du compagnon|`@Compendium[pf2e.feats-srd.FhxkU6OftQeecpQW]`|
|[Heal Mount](feats/class-08-LnSMRHjMArCkE4w1.htm)|Guérison de destrier|`@Compendium[pf2e.feats-srd.LnSMRHjMArCkE4w1]`|
|[Healing Bomb](feats/class-04-xXHwktc9SymSY8d6.htm)|Bombe de guérison|`@Compendium[pf2e.feats-srd.xXHwktc9SymSY8d6]`|
|[Healing Hands](feats/class-01-zYhcEX4JnrZ08HfV.htm)|Mains guérisseuses|`@Compendium[pf2e.feats-srd.zYhcEX4JnrZ08HfV]`|
|[Healing Touch](feats/archetype-04-jlLWz8e7PpLFt0Ed.htm)|Contact guérisseur|`@Compendium[pf2e.feats-srd.jlLWz8e7PpLFt0Ed]`|
|[Healing Transformation](feats/class-10-85V3vdew0gykEtmu.htm)|Transformation curative|`@Compendium[pf2e.feats-srd.85V3vdew0gykEtmu]`|
|[Heaven's Thunder](feats/archetype-06-qJdbK8vgIqeHU7bu.htm)|Tonnerre du Paradis|`@Compendium[pf2e.feats-srd.qJdbK8vgIqeHU7bu]`|
|[Hefty Hauler](feats/skill-01-C0Tcelg3BAPhML6J.htm)|Portefaix|`@Compendium[pf2e.feats-srd.C0Tcelg3BAPhML6J]`|
|[Heir of the Saoc](feats/ancestry-09-MTp2j4N4H4wj07pH.htm)|Héritier de Saoc|`@Compendium[pf2e.feats-srd.MTp2j4N4H4wj07pH]`|
|[Hell's Armaments](feats/archetype-12-J5s7NeFHYuFSdhrX.htm)|Arsenal infernal|`@Compendium[pf2e.feats-srd.J5s7NeFHYuFSdhrX]`|
|[Hellknight Armiger Dedication](feats/archetype-02-qhs0mWbaLKsdckZX.htm)|Dévouement d'Écuyer des Chevaliers infernaux|`@Compendium[pf2e.feats-srd.qhs0mWbaLKsdckZX]`|
|[Hellknight Dedication](feats/archetype-06-oNh2vedij8xbDbph.htm)|Dévouement de Chevalier infernal|`@Compendium[pf2e.feats-srd.oNh2vedij8xbDbph]`|
|[Hellknight Order Cross-Training](feats/class-12-iFEecf9o6uhJxWcG.htm)|Entraînement croisé de l'Ordre de Chevalier infernal|`@Compendium[pf2e.feats-srd.iFEecf9o6uhJxWcG]`|
|[Hellknight Signifer Dedication](feats/archetype-06-62hpJOuvYYSa4X7u.htm)|Dévouement du Signifer|`@Compendium[pf2e.feats-srd.62hpJOuvYYSa4X7u]`|
|[Hellspawn](feats/ancestry-01-EXVHePH8alsTZ5TB.htm)|Rejeton infernal|`@Compendium[pf2e.feats-srd.EXVHePH8alsTZ5TB]`|
|[Helpful Halfling](feats/ancestry-09-gWyCNTWUhxneOBne.htm)|Halfelin utile|`@Compendium[pf2e.feats-srd.gWyCNTWUhxneOBne]`|
|[Herbalist Dedication](feats/archetype-02-5CRt5Dy9eLv5LpRF.htm)|Dévouement d'herboriste|`@Compendium[pf2e.feats-srd.5CRt5Dy9eLv5LpRF]`|
|[Heroes' Call](feats/ancestry-09-7cM7uKRKQDWz5eeu.htm)|Appel des héros|`@Compendium[pf2e.feats-srd.7cM7uKRKQDWz5eeu]`|
|[Heroic Presence](feats/ancestry-17-iVwsLYjOJbfvL0Pe.htm)|Présence héroïque|`@Compendium[pf2e.feats-srd.iVwsLYjOJbfvL0Pe]`|
|[Heroic Recovery](feats/class-10-KR78kinMmAZQHeoa.htm)|Récupération héroïque|`@Compendium[pf2e.feats-srd.KR78kinMmAZQHeoa]`|
|[Hex Focus](feats/class-12-p2tFR4yBauu8t3mC.htm)|Refocalisation de maléfice|`@Compendium[pf2e.feats-srd.p2tFR4yBauu8t3mC]`|
|[Hex Master](feats/class-20-5H3Sk1yhalQQzUys.htm)|Maître des maléfices|`@Compendium[pf2e.feats-srd.5H3Sk1yhalQQzUys]`|
|[Hex Wellspring](feats/class-18-LKxAuH0mLyzNygIY.htm)|Source de maléfice|`@Compendium[pf2e.feats-srd.LKxAuH0mLyzNygIY]`|
|[Hidden Magic](feats/archetype-04-K3Au5071pfvNwGob.htm)|Magie cachée|`@Compendium[pf2e.feats-srd.K3Au5071pfvNwGob]`|
|[Hidden Paragon](feats/class-20-Kl1O0WK37KMTumv1.htm)|Parangon de discrétion|`@Compendium[pf2e.feats-srd.Kl1O0WK37KMTumv1]`|
|[Hideous Ululation](feats/archetype-04-cQptGH6RUYZmS41Q.htm)|Ululement hideux|`@Compendium[pf2e.feats-srd.cQptGH6RUYZmS41Q]`|
|[Hierophant's Power](feats/class-20-Chu6s3xVnpOB64GH.htm)|Puissance du Hiérophante|`@Compendium[pf2e.feats-srd.Chu6s3xVnpOB64GH]`|
|[High-quality Scrounger](feats/archetype-06-vW8dGtOD3rZVOJoq.htm)|Bricolage de haute qualité|`@Compendium[pf2e.feats-srd.vW8dGtOD3rZVOJoq]`|
|[Hireling Manager](feats/general-03-Wb3FHiDuY6Nuc0N0.htm)|Chef des employés|`@Compendium[pf2e.feats-srd.Wb3FHiDuY6Nuc0N0]`|
|[Hobgoblin Lore](feats/ancestry-01-13zaW8ZHRWnRe2pj.htm)|Connaissance hobgobeline|`@Compendium[pf2e.feats-srd.13zaW8ZHRWnRe2pj]`|
|[Hobgoblin Weapon Discipline](feats/ancestry-05-B6eoSmowSFNlhj9h.htm)|Discipline des armes hobgobelines|`@Compendium[pf2e.feats-srd.B6eoSmowSFNlhj9h]`|
|[Hobgoblin Weapon Expertise](feats/ancestry-13-XPVqOtHLN6jduHYs.htm)|Expertise avec les armes hobgobelines|`@Compendium[pf2e.feats-srd.XPVqOtHLN6jduHYs]`|
|[Hobgoblin Weapon Familiarity](feats/ancestry-01-n9CBjyiB17srkyr4.htm)|Familiarité avec les armes hobgobelines|`@Compendium[pf2e.feats-srd.n9CBjyiB17srkyr4]`|
|[Hobnobber](feats/skill-01-4RjDxgvNXNl5GG9d.htm)|Animal social|`@Compendium[pf2e.feats-srd.4RjDxgvNXNl5GG9d]`|
|[Hold Mark](feats/ancestry-05-aQNsD2t0Tb4vToA4.htm)|Marque portée|`@Compendium[pf2e.feats-srd.aQNsD2t0Tb4vToA4]`|
|[Holistic Care](feats/archetype-06-1WfvvjjObPKeZyid.htm)|Soins holistiques|`@Compendium[pf2e.feats-srd.1WfvvjjObPKeZyid]`|
|[Holy Castigation](feats/class-01-142QRyK2aPIrJu48.htm)|Punition sacrée|`@Compendium[pf2e.feats-srd.142QRyK2aPIrJu48]`|
|[Homeward Bound](feats/ancestry-17-UKHIKipBvOGhzcSQ.htm)|Rentrer à la maison|`@Compendium[pf2e.feats-srd.UKHIKipBvOGhzcSQ]`|
|[Horizon Walker Dedication](feats/archetype-02-pZMBq7gn66SEEA0n.htm)|Dévouement d'Arpenteur d'horizon|`@Compendium[pf2e.feats-srd.pZMBq7gn66SEEA0n]`|
|[House of Imaginary Walls](feats/class-10-dutiFC41YFllm8fM.htm)|Demeure aux murs imaginaires|`@Compendium[pf2e.feats-srd.dutiFC41YFllm8fM]`|
|[Hungry Goblin](feats/ancestry-09-DnfVQGLk3PAl8UWh.htm)|Gobelin affamé|`@Compendium[pf2e.feats-srd.DnfVQGLk3PAl8UWh]`|
|[Hunted Shot](feats/class-01-c3b7DhnDBC7YEgRG.htm)|Tir du chasseur|`@Compendium[pf2e.feats-srd.c3b7DhnDBC7YEgRG]`|
|[Hunter's Aim](feats/class-02-GFtNQvpzuqtsdOTG.htm)|Visée du chasseur|`@Compendium[pf2e.feats-srd.GFtNQvpzuqtsdOTG]`|
|[Hunter's Luck](feats/class-04-N4TpzEzuFbInSgvz.htm)|Chance du chasseur|`@Compendium[pf2e.feats-srd.N4TpzEzuFbInSgvz]`|
|[Hunter's Vision](feats/class-10-W7aT1UJOVFkYdQti.htm)|Vision du chasseur|`@Compendium[pf2e.feats-srd.W7aT1UJOVFkYdQti]`|
|[Hurling Charge](feats/archetype-04-9KvsO72JJ3pfkG4U.htm)|Lancer en chargeant|`@Compendium[pf2e.feats-srd.9KvsO72JJ3pfkG4U]`|
|[Hymn Of Healing](feats/class-01-jGTRRCqxn1FIBxE2.htm)|Hymne de guérison|`@Compendium[pf2e.feats-srd.jGTRRCqxn1FIBxE2]`|
|[Illusion Sense](feats/ancestry-01-tFgsBRsEo9ZEA5fU.htm)|Perception des illusions|`@Compendium[pf2e.feats-srd.tFgsBRsEo9ZEA5fU]`|
|[Impaling Briars](feats/class-16-urYHB6VHhqvPMSy7.htm)|Ronces empaleuses|`@Compendium[pf2e.feats-srd.urYHB6VHhqvPMSy7]`|
|[Impaling Finisher](feats/class-04-Qfn7lmOeXfBtpG4O.htm)|Botte empalante|`@Compendium[pf2e.feats-srd.Qfn7lmOeXfBtpG4O]`|
|[Impaling Thrust](feats/class-14-guSjEQS3WuXJqQxf.htm)|Poussée empalante|`@Compendium[pf2e.feats-srd.guSjEQS3WuXJqQxf]`|
|[Impassable Wall Stance](feats/class-08-YeyOqNFKaeuOTiJr.htm)|Posture du mur infranchissable|`@Compendium[pf2e.feats-srd.YeyOqNFKaeuOTiJr]`|
|[Impeccable Crafting](feats/skill-07-PiUe3tpv7UVtnfvS.htm)|Artisanat impeccable|`@Compendium[pf2e.feats-srd.PiUe3tpv7UVtnfvS]`|
|[Implausible Infiltration](feats/class-18-fLrwddS607eRFfHA.htm)|Infiltration invraisemblable|`@Compendium[pf2e.feats-srd.fLrwddS607eRFfHA]`|
|[Implausible Purchase (Investigator)](feats/class-16-RqccUKf8DCPnsYXJ.htm)|Achat improbable (Enquêteur)|`@Compendium[pf2e.feats-srd.RqccUKf8DCPnsYXJ]`|
|[Implausible Purchase (Rogue)](feats/class-18-zCASpQconMmtJKQN.htm)|Achat improbable (Roublard)|`@Compendium[pf2e.feats-srd.zCASpQconMmtJKQN]`|
|[Imposing Destrier](feats/class-10-07jxXvRZ8nD3JLF4.htm)|Destrier imposant|`@Compendium[pf2e.feats-srd.07jxXvRZ8nD3JLF4]`|
|[Impossible Flurry](feats/class-18-OqHfUQQorVBkx34j.htm)|Déluge invraisemblable|`@Compendium[pf2e.feats-srd.OqHfUQQorVBkx34j]`|
|[Impossible Polymath](feats/class-18-L5n4PvQYhpl2WM9e.htm)|Touche-à-tout invraisemblable|`@Compendium[pf2e.feats-srd.L5n4PvQYhpl2WM9e]`|
|[Impossible Riposte](feats/class-14-HquaVwjOLSPzcJgB.htm)|Riposte invraisemblable|`@Compendium[pf2e.feats-srd.HquaVwjOLSPzcJgB]`|
|[Impossible Snares](feats/class-20-rwTbN2A2ZO7CdKoC.htm)|Pièges artisanaux invraisemblables|`@Compendium[pf2e.feats-srd.rwTbN2A2ZO7CdKoC]`|
|[Impossible Striker](feats/class-20-mSqzGGttJvj4LxK9.htm)|Frappeur impossible|`@Compendium[pf2e.feats-srd.mSqzGGttJvj4LxK9]`|
|[Impossible Technique](feats/class-20-srWsvDDdz77yieY1.htm)|Technique invraisemblable|`@Compendium[pf2e.feats-srd.srWsvDDdz77yieY1]`|
|[Impossible Volley](feats/class-18-NY2AkQscVIHEC8hQ.htm)|Volée invraisemblable|`@Compendium[pf2e.feats-srd.NY2AkQscVIHEC8hQ]`|
|[Impressive Landing](feats/class-10-QO8l5Dao8HnaFQE4.htm)|Atterrissage impressionnant|`@Compendium[pf2e.feats-srd.QO8l5Dao8HnaFQE4]`|
|[Impressive Mount](feats/archetype-04-Nxke8WzifQafSa4I.htm)|Monture impressionnante|`@Compendium[pf2e.feats-srd.Nxke8WzifQafSa4I]`|
|[Impressive Performance](feats/skill-01-xqAdXRd2gSQcqp5E.htm)|Représentation impressionnante|`@Compendium[pf2e.feats-srd.xqAdXRd2gSQcqp5E]`|
|[Improbable Elixirs](feats/class-18-nnsoFOtuHnpz2QHc.htm)|Élixirs improbables|`@Compendium[pf2e.feats-srd.nnsoFOtuHnpz2QHc]`|
|[Improved Command Undead](feats/class-10-A0ZQpGSB7pvIDiou.htm)|Contrôle des morts-vivants amélioré|`@Compendium[pf2e.feats-srd.A0ZQpGSB7pvIDiou]`|
|[Improved Communal Healing](feats/class-04-IOdk7bOJ4dgYVh9I.htm)|Guérison collective améliorée|`@Compendium[pf2e.feats-srd.IOdk7bOJ4dgYVh9I]`|
|[Improved Dueling Riposte](feats/class-12-mgs7vxq6d3hQoswa.htm)|Riposte en duel améliorée|`@Compendium[pf2e.feats-srd.mgs7vxq6d3hQoswa]`|
|[Improved Familiar (Familiar Master)](feats/archetype-06-9C6a6FXuPqWjXy8K.htm)|Familier amélioré (Maître familier)|`@Compendium[pf2e.feats-srd.9C6a6FXuPqWjXy8K]`|
|[Improved Familiar (Witch)](feats/class-04-2VKV7jLRTxWyVjGa.htm)|Familier amélioré (Sorcière)|`@Compendium[pf2e.feats-srd.2VKV7jLRTxWyVjGa]`|
|[Improved Knockback](feats/class-12-HPETR6zq8L6YJyi1.htm)|Repoussement amélioré|`@Compendium[pf2e.feats-srd.HPETR6zq8L6YJyi1]`|
|[Improved Knockdown](feats/class-10-yTh9QwAf0hadP91j.htm)|Renversement amélioré|`@Compendium[pf2e.feats-srd.yTh9QwAf0hadP91j]`|
|[Improved Poison Weapon](feats/class-08-9Eufa07qvXG41QmG.htm)|Arme empoisonnée améliorée|`@Compendium[pf2e.feats-srd.9Eufa07qvXG41QmG]`|
|[Improved Reflexive Shield](feats/class-16-fEfEabn53bubYVVT.htm)|Bouclier instinctif amélioré|`@Compendium[pf2e.feats-srd.fEfEabn53bubYVVT]`|
|[Improved Swift Banishment](feats/class-18-mCXoiMLAbGHGsZS3.htm)|Bannissement rapide amélioré|`@Compendium[pf2e.feats-srd.mCXoiMLAbGHGsZS3]`|
|[Improved Twin Riposte (Fighter)](feats/class-14-w2v5LZmpJy0MBxo5.htm)|Riposte jumelée améliorée (Guerrier)|`@Compendium[pf2e.feats-srd.w2v5LZmpJy0MBxo5]`|
|[Improved Twin Riposte (Ranger)](feats/class-16-Cgk4By6gEomD2bJ0.htm)|Riposte jumelée améliorée|`@Compendium[pf2e.feats-srd.Cgk4By6gEomD2bJ0]`|
|[Improvisational Defender](feats/ancestry-01-KQVE4FsDd9RFpWRz.htm)|Défenseur improvisé|`@Compendium[pf2e.feats-srd.KQVE4FsDd9RFpWRz]`|
|[Improvisational Warrior](feats/ancestry-09-VazbV1s93eZqUZIu.htm)|Combattant improvisé|`@Compendium[pf2e.feats-srd.VazbV1s93eZqUZIu]`|
|[Improvise Tool](feats/skill-01-3HChkcD1IRqv4DbA.htm)|Outil improvisé|`@Compendium[pf2e.feats-srd.3HChkcD1IRqv4DbA]`|
|[Improvised Crafting](feats/archetype-08-RcQv16RK80R6c4id.htm)|Artisanat improvisé|`@Compendium[pf2e.feats-srd.RcQv16RK80R6c4id]`|
|[Improvised Critical](feats/archetype-08-YDpSnLmnSLsItP45.htm)|Critique improvisé|`@Compendium[pf2e.feats-srd.YDpSnLmnSLsItP45]`|
|[Improvised Pummel](feats/archetype-04-FSxugo3zTgRhW7Og.htm)|Tabassage improvisé|`@Compendium[pf2e.feats-srd.FSxugo3zTgRhW7Og]`|
|[Improvised Repair](feats/general-03-xT593tHyPkumPuzz.htm)|Réparation improvisée|`@Compendium[pf2e.feats-srd.xT593tHyPkumPuzz]`|
|[Incredible Aim](feats/class-08-DGO6kyjw2bQG7dbY.htm)|Visée extraordinaire|`@Compendium[pf2e.feats-srd.DGO6kyjw2bQG7dbY]`|
|[Incredible Beastmaster Companion](feats/archetype-08-aEws1NR19Lbu1Kio.htm)|Formidable compagnon (Maître des bêtes)|`@Compendium[pf2e.feats-srd.aEws1NR19Lbu1Kio]`|
|[Incredible Companion (Druid)](feats/class-08-qgW8uHJXJGl3DKBS.htm)|Formidable compagnon (Druide)|`@Compendium[pf2e.feats-srd.qgW8uHJXJGl3DKBS]`|
|[Incredible Companion (Ranger)](feats/class-10-U52NMdeSNbjSSRHE.htm)|Formidable compagnon (Rôdeur)|`@Compendium[pf2e.feats-srd.U52NMdeSNbjSSRHE]`|
|[Incredible Familiar (Familiar Master)](feats/archetype-10-KcNXoSvULnuQjC9a.htm)|Formidable familier (Maître familier)|`@Compendium[pf2e.feats-srd.KcNXoSvULnuQjC9a]`|
|[Incredible Familiar (Witch)](feats/class-08-tPb0FVkNDE89ACbC.htm)|Formidable familier (Sorcière)|`@Compendium[pf2e.feats-srd.tPb0FVkNDE89ACbC]`|
|[Incredible Ferocity](feats/ancestry-13-GELhS1k0vHEi1PK3.htm)|Férocité extraordinaire|`@Compendium[pf2e.feats-srd.GELhS1k0vHEi1PK3]`|
|[Incredible Improvisation](feats/ancestry-09-lHcDb9oXUdFupRdi.htm)|Improvisation extraordinaire|`@Compendium[pf2e.feats-srd.lHcDb9oXUdFupRdi]`|
|[Incredible Initiative](feats/general-01-2kwXTUjYYhoAGySr.htm)|Initiative extraordinaire|`@Compendium[pf2e.feats-srd.2kwXTUjYYhoAGySr]`|
|[Incredible Investiture](feats/general-11-4vu6P3cYoOOeUbLK.htm)|Investiture extraordinaire|`@Compendium[pf2e.feats-srd.4vu6P3cYoOOeUbLK]`|
|[Incredible Luck (Halfling)](feats/ancestry-13-iTsLr3zEaGZ45zez.htm)|Chance incroyable (Halfelin)|`@Compendium[pf2e.feats-srd.iTsLr3zEaGZ45zez]`|
|[Incredible Luck (Swashbuckler)](feats/class-18-uAdnQZSkUuxcpEwz.htm)|Chance incroyable (Bretteur)|`@Compendium[pf2e.feats-srd.uAdnQZSkUuxcpEwz]`|
|[Incredible Mount](feats/archetype-08-AYBfwGImT28lUdue.htm)|Formidable monture|`@Compendium[pf2e.feats-srd.AYBfwGImT28lUdue]`|
|[Incredible Ricochet](feats/class-12-fCDC53WOOYrsyVIR.htm)|Ricochet extraordinaire|`@Compendium[pf2e.feats-srd.fCDC53WOOYrsyVIR]`|
|[Incredible Scout](feats/general-11-aFoVHsuInMOkTZoQ.htm)|Éclaireur incroyable|`@Compendium[pf2e.feats-srd.aFoVHsuInMOkTZoQ]`|
|[Inexhaustible Countermoves](feats/class-20-jG9YwAAvNbCShumf.htm)|Contremesures inépuisables|`@Compendium[pf2e.feats-srd.jG9YwAAvNbCShumf]`|
|[Infinite Possibilities](feats/class-18-LAHiW98iPJKplFyK.htm)|Possibilités infinies|`@Compendium[pf2e.feats-srd.LAHiW98iPJKplFyK]`|
|[Influence Nature](feats/skill-07-f5JOSyW1tKMpz6hU.htm)|Influencer la nature|`@Compendium[pf2e.feats-srd.f5JOSyW1tKMpz6hU]`|
|[Innate Magical Intuition](feats/archetype-08-QYZovCDjOhBb1u01.htm)|Intuition magique innée|`@Compendium[pf2e.feats-srd.QYZovCDjOhBb1u01]`|
|[Inner Strength](feats/class-06-OY1Ewg0dbCp52Hl5.htm)|Force intérieure|`@Compendium[pf2e.feats-srd.OY1Ewg0dbCp52Hl5]`|
|[Innocuous](feats/ancestry-01-E1sjYRb4zsZVrzrN.htm)|Inoffensif|`@Compendium[pf2e.feats-srd.E1sjYRb4zsZVrzrN]`|
|[Inoculation](feats/skill-01-sMCpihnBEpx18GBD.htm)|Vaccination|`@Compendium[pf2e.feats-srd.sMCpihnBEpx18GBD]`|
|[Insect Shape](feats/class-06-OWedlrKGsVZVkSnT.htm)|Morphologie d'insecte|`@Compendium[pf2e.feats-srd.OWedlrKGsVZVkSnT]`|
|[Insistent Command](feats/archetype-08-rhVL28qFl760qJQe.htm)|Ordre insistant|`@Compendium[pf2e.feats-srd.rhVL28qFl760qJQe]`|
|[Inspirational Focus](feats/class-12-UIRcjHxuSedoDOj4.htm)|Focalisation inspirante|`@Compendium[pf2e.feats-srd.UIRcjHxuSedoDOj4]`|
|[Inspirational Performance](feats/archetype-08-CIRseixX8dr36ZQK.htm)|Représentation inspirante|`@Compendium[pf2e.feats-srd.CIRseixX8dr36ZQK]`|
|[Inspire Competence](feats/class-02-eUXgY8W3fShlW7pd.htm)|Inspiration talentueuse|`@Compendium[pf2e.feats-srd.eUXgY8W3fShlW7pd]`|
|[Inspire Defense](feats/class-04-sahJHnojXO9eEXVE.htm)|Inspiration défensive|`@Compendium[pf2e.feats-srd.sahJHnojXO9eEXVE]`|
|[Inspire Heroics](feats/class-08-FjuuX0vUWlYchRNM.htm)|Inspiration héroïque|`@Compendium[pf2e.feats-srd.FjuuX0vUWlYchRNM]`|
|[Inspire Imitation](feats/ancestry-05-pqezW5dqha1I32Ld.htm)|Exemplarité|`@Compendium[pf2e.feats-srd.pqezW5dqha1I32Ld]`|
|[Inspired Stratagem](feats/class-08-9pTQrhbeF348bYky.htm)|Stratagème inspiré|`@Compendium[pf2e.feats-srd.9pTQrhbeF348bYky]`|
|[Inspiring Marshal Stance](feats/archetype-04-bvOsJNeI0ewvQsFa.htm)|Posture du Capitaine inspirant|`@Compendium[pf2e.feats-srd.bvOsJNeI0ewvQsFa]`|
|[Instant Opening](feats/class-14-9zH7IOsmhRBEqXAV.htm)|Ouverture instantanée|`@Compendium[pf2e.feats-srd.9zH7IOsmhRBEqXAV]`|
|[Instinct Ability](feats/archetype-06-4T9HHOdTk3yVbeoO.htm)|Pouvoir d'instinct|`@Compendium[pf2e.feats-srd.4T9HHOdTk3yVbeoO]`|
|[Instinctive Strike](feats/class-08-LVTquA3DpqCJDika.htm)|Frappe instinctive|`@Compendium[pf2e.feats-srd.LVTquA3DpqCJDika]`|
|[Instrument Of Slaughter](feats/class-16-i98NcWSAbWmNmBik.htm)|Instrument du massacre|`@Compendium[pf2e.feats-srd.i98NcWSAbWmNmBik]`|
|[Instrument of Zeal](feats/class-16-h08Vfel5iIAARWdy.htm)|Instrument de zèle|`@Compendium[pf2e.feats-srd.h08Vfel5iIAARWdy]`|
|[Interweave Dispel](feats/class-14-bjNeSAldeTzRcEaQ.htm)|Dissipation entremêlée|`@Compendium[pf2e.feats-srd.bjNeSAldeTzRcEaQ]`|
|[Intimidating Glare](feats/skill-01-xQMz6eDgX75WX2ce.htm)|Regard intimidant|`@Compendium[pf2e.feats-srd.xQMz6eDgX75WX2ce]`|
|[Intimidating Prowess](feats/skill-02-XFJiGllNZp8Xebda.htm)|Prouesse intimidante|`@Compendium[pf2e.feats-srd.XFJiGllNZp8Xebda]`|
|[Intimidating Strike](feats/class-02-lkcM4V3VDAtjlR9P.htm)|Frappe intimidante|`@Compendium[pf2e.feats-srd.lkcM4V3VDAtjlR9P]`|
|[Intuitive Cooperation](feats/ancestry-01-AjdmoGFoSIyx1mxd.htm)|Coopération intuitive|`@Compendium[pf2e.feats-srd.AjdmoGFoSIyx1mxd]`|
|[Intuitive Illusions](feats/ancestry-05-MwjnRpVn3br88Caj.htm)|Illusions intuitives|`@Compendium[pf2e.feats-srd.MwjnRpVn3br88Caj]`|
|[Inventive Offensive](feats/ancestry-01-SrSYEHqOLXWuj65e.htm)|Offensive inventive|`@Compendium[pf2e.feats-srd.SrSYEHqOLXWuj65e]`|
|[Inventor](feats/skill-07-XR95taODq1sq82Du.htm)|Inventeur|`@Compendium[pf2e.feats-srd.XR95taODq1sq82Du]`|
|[Investigate Haunting](feats/archetype-04-bgnf7USFkqsNh8j1.htm)|Enquêter sur les apparitions|`@Compendium[pf2e.feats-srd.bgnf7USFkqsNh8j1]`|
|[Investigator Dedication](feats/archetype-02-CZXhJS55rG5H6PpB.htm)|Dévouement d'Enquêteur|`@Compendium[pf2e.feats-srd.CZXhJS55rG5H6PpB]`|
|[Investigator's Stratagem](feats/archetype-06-7XcQ8Ygz5cubGxdC.htm)|Stratagème d'enquêteur|`@Compendium[pf2e.feats-srd.7XcQ8Ygz5cubGxdC]`|
|[Invigorating Mercy](feats/class-06-fx50Ivl1ERxTijpT.htm)|Soulagement revigorant|`@Compendium[pf2e.feats-srd.fx50Ivl1ERxTijpT]`|
|[Invincible Mutagen](feats/class-12-uAh31Hnp1EZSjd40.htm)|Mutagène invincible|`@Compendium[pf2e.feats-srd.uAh31Hnp1EZSjd40]`|
|[Invoke Disaster](feats/class-18-2RaDe6Fi4t7S2IDF.htm)|Invocation de désastre|`@Compendium[pf2e.feats-srd.2RaDe6Fi4t7S2IDF]`|
|[Invoke the Crimson Oath](feats/archetype-08-tH5w8vIfFCFNVP7T.htm)|Invoquer le Serment écarlate|`@Compendium[pf2e.feats-srd.tH5w8vIfFCFNVP7T]`|
|[Invulnerable Juggernaut](feats/class-20-VSyuTWRuxdmgq2HS.htm)|Juggernaut invulnérable|`@Compendium[pf2e.feats-srd.VSyuTWRuxdmgq2HS]`|
|[Iron Fists](feats/ancestry-01-JP5pptkl1Fx1JK4m.htm)|Poings de fer|`@Compendium[pf2e.feats-srd.JP5pptkl1Fx1JK4m]`|
|[Iron Lung](feats/archetype-14-8PMxl8o5YXET58Pn.htm)|Poumon d'acier|`@Compendium[pf2e.feats-srd.8PMxl8o5YXET58Pn]`|
|[Iron Repercussions](feats/class-01-XttSGDuAsRDTuvgS.htm)|Répercussions de fer|`@Compendium[pf2e.feats-srd.XttSGDuAsRDTuvgS]`|
|[Ironblood Stance](feats/class-08-x9cYkB8DrUBBwqJd.htm)|Posture du sang-de-fer|`@Compendium[pf2e.feats-srd.x9cYkB8DrUBBwqJd]`|
|[Ironblood Surge](feats/class-14-IqDbNiwHQH1xApo9.htm)|Afflux de sang de fer|`@Compendium[pf2e.feats-srd.IqDbNiwHQH1xApo9]`|
|[Irrepressible](feats/ancestry-09-cRMUYCnkkE9lSEhh.htm)|Irrépressible|`@Compendium[pf2e.feats-srd.cRMUYCnkkE9lSEhh]`|
|[Irriseni Ice-Witch](feats/ancestry-13-arlZTqfppOAXBhdw.htm)|Sorcière de glace Irriseni|`@Compendium[pf2e.feats-srd.arlZTqfppOAXBhdw]`|
|[Iruxi Unarmed Cunning](feats/ancestry-05-5iULRBRDOryuWX6t.htm)|ruse à mains nues iruxie|`@Compendium[pf2e.feats-srd.5iULRBRDOryuWX6t]`|
|[Iruxi Unarmed Expertise](feats/ancestry-13-Tn2iMLKvhZki1O3S.htm)|Expertise à mains nues Iruxi|`@Compendium[pf2e.feats-srd.Tn2iMLKvhZki1O3S]`|
|[Jalmeri Heavenseeker Dedication](feats/archetype-04-Ze9vb0wlRmWWqnXC.htm)|Dévouement de Chercheur du Paradis du Jalmeray|`@Compendium[pf2e.feats-srd.Ze9vb0wlRmWWqnXC]`|
|[Janatimo's Secret Lesson](feats/archetype-12-CQfxxsRxf1BuUH4o.htm)|Leçon secrète de Janatimo|`@Compendium[pf2e.feats-srd.CQfxxsRxf1BuUH4o]`|
|[Judgement of the Monolith](feats/archetype-12-YidYY7k2gvny9eSY.htm)|Jugement du Monolithe|`@Compendium[pf2e.feats-srd.YidYY7k2gvny9eSY]`|
|[Juggernaut's Fortitude](feats/archetype-12-jPBqvEH2jLlvDr6M.htm)|Vigueur du juggernaut|`@Compendium[pf2e.feats-srd.jPBqvEH2jLlvDr6M]`|
|[Juggle](feats/skill-02-AYb8PmGJ37HwIMwj.htm)|Jonglage|`@Compendium[pf2e.feats-srd.AYb8PmGJ37HwIMwj]`|
|[Juggler Dedication](feats/archetype-02-mNsNl6w3l5rXx8dL.htm)|Dévouement du Jongleur|`@Compendium[pf2e.feats-srd.mNsNl6w3l5rXx8dL]`|
|[Junk Tinker](feats/ancestry-01-YMKtEoNwHKA713Cx.htm)|Bricoleur de rebuts|`@Compendium[pf2e.feats-srd.YMKtEoNwHKA713Cx]`|
|[Just One More Thing](feats/class-10-YluQPhevo0LKdF1p.htm)|Encore une chose|`@Compendium[pf2e.feats-srd.YluQPhevo0LKdF1p]`|
|[Just The Facts](feats/class-20-ZfycfbXlPXZlSqw5.htm)|Juste les faits|`@Compendium[pf2e.feats-srd.ZfycfbXlPXZlSqw5]`|
|[Ka Stone Ritual](feats/archetype-04-CZQgH17ZxoBiVXLa.htm)|Rituel de la pierre Ka|`@Compendium[pf2e.feats-srd.CZQgH17ZxoBiVXLa]`|
|[Keen Follower](feats/general-03-fD9xjrnPfJ8aQxYA.htm)|Suivant appliqué|`@Compendium[pf2e.feats-srd.fD9xjrnPfJ8aQxYA]`|
|[Keen Recollection](feats/archetype-06-Ig431EeRy3FKMmMq.htm)|Souvenirs affûtés|`@Compendium[pf2e.feats-srd.Ig431EeRy3FKMmMq]`|
|[Keep Pace](feats/archetype-06-OzvvsyjAWWij4mmm.htm)|Conserver le rythme|`@Compendium[pf2e.feats-srd.OzvvsyjAWWij4mmm]`|
|[Keep Up Appearances](feats/ancestry-01-pQQhrcj1u6hUUc8L.htm)|Conserver les apparences|`@Compendium[pf2e.feats-srd.pQQhrcj1u6hUUc8L]`|
|[Ki Blast](feats/class-06-10DbphslCihq8mxQ.htm)|Explosion ki|`@Compendium[pf2e.feats-srd.10DbphslCihq8mxQ]`|
|[Ki Center](feats/class-18-wIRZDXOacqWfI670.htm)|Centrer le ki|`@Compendium[pf2e.feats-srd.wIRZDXOacqWfI670]`|
|[Ki Form](feats/class-18-oYFsw3LdgsN2QKVs.htm)|Forme ki|`@Compendium[pf2e.feats-srd.oYFsw3LdgsN2QKVs]`|
|[Ki Rush](feats/class-01-yA9tsWT9uzL2REnw.htm)|Ruée Ki|`@Compendium[pf2e.feats-srd.yA9tsWT9uzL2REnw]`|
|[Ki Strike](feats/class-01-3a07jkWezSuORSpS.htm)|Frappe ki|`@Compendium[pf2e.feats-srd.3a07jkWezSuORSpS]`|
|[Kip Up](feats/skill-07-gBSPbQRXdagZTUwY.htm)|Saut carpé|`@Compendium[pf2e.feats-srd.gBSPbQRXdagZTUwY]`|
|[Kneecap](feats/ancestry-05-1muxLx8Vacn2SLHc.htm)|Rotule|`@Compendium[pf2e.feats-srd.1muxLx8Vacn2SLHc]`|
|[Kneel for No God](feats/ancestry-09-wGzKq2cx1b2Ycl4u.htm)|Ne s'agenouiller pour aucun dieu|`@Compendium[pf2e.feats-srd.wGzKq2cx1b2Ycl4u]`|
|[Knight Reclaimant Dedication](feats/archetype-06-H86VNC6cNWhBI8Ed.htm)|Dévouement de Chevalier récupérateur|`@Compendium[pf2e.feats-srd.H86VNC6cNWhBI8Ed]`|
|[Knight Vigilant Dedication](feats/class-06-1YFrl8I6ZGo7BIM9.htm)|Chevalier vigilant|`@Compendium[pf2e.feats-srd.1YFrl8I6ZGo7BIM9]`|
|[Knight in Shining Armor](feats/archetype-12-XgUQ6Tm9LKxcZGHW.htm)|Chevalier en armure étincelante|`@Compendium[pf2e.feats-srd.XgUQ6Tm9LKxcZGHW]`|
|[Knockback](feats/class-10-pVLdMOqYwul745k3.htm)|Repousser|`@Compendium[pf2e.feats-srd.pVLdMOqYwul745k3]`|
|[Knockback Strike](feats/class-10-2OYJOFaEkhc8dFbl.htm)|Frappe repoussante|`@Compendium[pf2e.feats-srd.2OYJOFaEkhc8dFbl]`|
|[Knockdown](feats/class-04-39CqlOzlHjEhh0E4.htm)|Renversement|`@Compendium[pf2e.feats-srd.39CqlOzlHjEhh0E4]`|
|[Know Oneself](feats/ancestry-01-shp63QZvw9xvkVvC.htm)|Connais toi toi-même|`@Compendium[pf2e.feats-srd.shp63QZvw9xvkVvC]`|
|[Know Your Own](feats/ancestry-01-o4LycqplO14zn6It.htm)|Connaître vos semblables|`@Compendium[pf2e.feats-srd.o4LycqplO14zn6It]`|
|[Know the Beat](feats/archetype-03-9lyFxaoZjF1ZjVN9.htm)|Connaissance du Milieu|`@Compendium[pf2e.feats-srd.9lyFxaoZjF1ZjVN9]`|
|[Know-It-All](feats/class-08-1qJCMbs3zcPMWDux.htm)|Je-sais-tout|`@Compendium[pf2e.feats-srd.1qJCMbs3zcPMWDux]`|
|[Known Weaknesses](feats/class-01-iWvpq3uDZcXvBJj8.htm)|Faiblesses connues|`@Compendium[pf2e.feats-srd.iWvpq3uDZcXvBJj8]`|
|[Kobold Breath](feats/ancestry-01-PPUNMjRLQYnmwQvF.htm)|Souffle de kobold|`@Compendium[pf2e.feats-srd.PPUNMjRLQYnmwQvF]`|
|[Kobold Lore](feats/ancestry-01-IElFaS5i10MFYIvq.htm)|Connaissance des kobolds|`@Compendium[pf2e.feats-srd.IElFaS5i10MFYIvq]`|
|[Kreighton's Cognitive Crossover](feats/skill-04-kIXFNPFBStOKunq4.htm)|Mélange cognitif de Kreighton|`@Compendium[pf2e.feats-srd.kIXFNPFBStOKunq4]`|
|[Lab Rat](feats/ancestry-05-QE8asCPqyrdenll0.htm)|Rat de labo|`@Compendium[pf2e.feats-srd.QE8asCPqyrdenll0]`|
|[Lasting Coercion](feats/skill-02-6vnbC90UQ3I57RrQ.htm)|Contrainte durable|`@Compendium[pf2e.feats-srd.6vnbC90UQ3I57RrQ]`|
|[Lasting Doubt](feats/class-12-56BMXlQlZtg39SMV.htm)|Doute Tenace|`@Compendium[pf2e.feats-srd.56BMXlQlZtg39SMV]`|
|[Lastwall Sentry Dedication](feats/archetype-02-bWYnjNa2RknMoibg.htm)|Dévouement de Sentinelle de Dernier-Rempart|`@Compendium[pf2e.feats-srd.bWYnjNa2RknMoibg]`|
|[Lastwall Warden](feats/archetype-10-V5Y9r31BCTaCiNzi.htm)|Gardien du Dernier-rempart|`@Compendium[pf2e.feats-srd.V5Y9r31BCTaCiNzi]`|
|[Lawbringer](feats/ancestry-01-g3oJlWGHc74qX2z5.htm)|Porteur de loi|`@Compendium[pf2e.feats-srd.g3oJlWGHc74qX2z5]`|
|[Lead Climber](feats/skill-02-vjQ3VUpYlTAAIx3b.htm)|Premier de cordée|`@Compendium[pf2e.feats-srd.vjQ3VUpYlTAAIx3b]`|
|[Lead Investigator](feats/class-18-YWRdk9oGMY6WmgIC.htm)|Enquêteur principal|`@Compendium[pf2e.feats-srd.YWRdk9oGMY6WmgIC]`|
|[Lead The Pack](feats/archetype-16-Aeea44xyJqYArFun.htm)|Mener la meute|`@Compendium[pf2e.feats-srd.Aeea44xyJqYArFun]`|
|[Leading Dance](feats/class-04-ZHPSASbvbbshq1zG.htm)|Mener la danse|`@Compendium[pf2e.feats-srd.ZHPSASbvbbshq1zG]`|
|[Leave an Opening](feats/class-14-5dAFkOYPz8PPdFrw.htm)|Laisser une ouverture|`@Compendium[pf2e.feats-srd.5dAFkOYPz8PPdFrw]`|
|[Leech-Clipper](feats/ancestry-01-OZtoTusMmCJymObT.htm)|Coupe-limace|`@Compendium[pf2e.feats-srd.OZtoTusMmCJymObT]`|
|[Legendary Codebreaker](feats/skill-15-Zf4yiLDdxHPovEQI.htm)|Cryptanalyste légendaire|`@Compendium[pf2e.feats-srd.Zf4yiLDdxHPovEQI]`|
|[Legendary Guide](feats/skill-15-Vk7BzAb3D9r226sI.htm)|Guide légendaire|`@Compendium[pf2e.feats-srd.Vk7BzAb3D9r226sI]`|
|[Legendary Linguist](feats/skill-15-n0S0tJiOJPQk0Rne.htm)|Linguiste légendaire|`@Compendium[pf2e.feats-srd.n0S0tJiOJPQk0Rne]`|
|[Legendary Medic](feats/skill-15-Kk4AMZtpQnLEgN0b.htm)|Infirmier légendaire|`@Compendium[pf2e.feats-srd.Kk4AMZtpQnLEgN0b]`|
|[Legendary Monster Hunter](feats/class-16-Veyt4x2Kb5YcPGTv.htm)|Chasseur de monstres légendaires|`@Compendium[pf2e.feats-srd.Veyt4x2Kb5YcPGTv]`|
|[Legendary Negotiation](feats/skill-15-A0TNeMNvyY8QpmLz.htm)|Négociation légendaire|`@Compendium[pf2e.feats-srd.A0TNeMNvyY8QpmLz]`|
|[Legendary Performer](feats/skill-15-467qQoiy6bjWU1G8.htm)|Artiste légendaire|`@Compendium[pf2e.feats-srd.467qQoiy6bjWU1G8]`|
|[Legendary Professional](feats/skill-15-GZba7ped7ZxYHchf.htm)|Professionnel légendaire|`@Compendium[pf2e.feats-srd.GZba7ped7ZxYHchf]`|
|[Legendary Rider](feats/archetype-20-epNrbgmjZDjJe7Ry.htm)|Chevaucheur légendaire|`@Compendium[pf2e.feats-srd.epNrbgmjZDjJe7Ry]`|
|[Legendary Shot](feats/class-20-B3Fz46wzUIzrxWsA.htm)|Tir légendaire|`@Compendium[pf2e.feats-srd.B3Fz46wzUIzrxWsA]`|
|[Legendary Sneak](feats/skill-15-n0urrOL8YlnVBVRQ.htm)|Furtivité légendaire|`@Compendium[pf2e.feats-srd.n0urrOL8YlnVBVRQ]`|
|[Legendary Survivalist](feats/skill-15-Ta61ObC8Lk7BxTFO.htm)|Survivaliste légendaire|`@Compendium[pf2e.feats-srd.Ta61ObC8Lk7BxTFO]`|
|[Legendary Thief](feats/skill-15-IZbjUaZI5zHTd1Vp.htm)|Voleur légendaire|`@Compendium[pf2e.feats-srd.IZbjUaZI5zHTd1Vp]`|
|[Legs of Stone](feats/archetype-12-PeBz8f9h8Y4OFdws.htm)|Jambes de pierre|`@Compendium[pf2e.feats-srd.PeBz8f9h8Y4OFdws]`|
|[Lengthy Diversion](feats/skill-01-HEBXaS656MZTiWFu.htm)|Diversion interminable|`@Compendium[pf2e.feats-srd.HEBXaS656MZTiWFu]`|
|[Leshy Familiar](feats/class-01-7y1BCJLrdk9mKXlc.htm)|Familier léchi|`@Compendium[pf2e.feats-srd.7y1BCJLrdk9mKXlc]`|
|[Leshy Familiar Secrets](feats/class-04-baz18CdB13DVMHV9.htm)|Secrets du familier léchi|`@Compendium[pf2e.feats-srd.baz18CdB13DVMHV9]`|
|[Leshy Glide](feats/ancestry-05-ShH7Wl7xfJL07DZC.htm)|Vol-plané léchi|`@Compendium[pf2e.feats-srd.ShH7Wl7xfJL07DZC]`|
|[Leshy Lore](feats/ancestry-01-i5W5aGWEiyo1vt2d.htm)|Connaissance des Léchis|`@Compendium[pf2e.feats-srd.i5W5aGWEiyo1vt2d]`|
|[Leshy Superstition](feats/ancestry-01-fYVFBnv9aVHv1UNg.htm)|Superstition léchi|`@Compendium[pf2e.feats-srd.fYVFBnv9aVHv1UNg]`|
|[Lethal Finisher](feats/class-18-XfCPAoNdF2XMnH7K.htm)|Botte létale|`@Compendium[pf2e.feats-srd.XfCPAoNdF2XMnH7K]`|
|[Levering Strike](feats/archetype-08-jIMeialR9CBo1bx9.htm)|Frappe levier|`@Compendium[pf2e.feats-srd.jIMeialR9CBo1bx9]`|
|[Leyline Conduit](feats/class-20-BQkk7qSSRTFc5jNG.htm)|Conduit Tellurique|`@Compendium[pf2e.feats-srd.BQkk7qSSRTFc5jNG]`|
|[Liberating Stride](feats/class-12-1JTEWgonWlmeCE3w.htm)|Foulée libératrice|`@Compendium[pf2e.feats-srd.1JTEWgonWlmeCE3w]`|
|[Lie Detector](feats/class-04-I10dkdvL6kAnqZWA.htm)|Détecteur de mensonge|`@Compendium[pf2e.feats-srd.I10dkdvL6kAnqZWA]`|
|[Lie To Me](feats/skill-01-Dvz54d6aPhjsmUux.htm)|Détecteur de mensonges|`@Compendium[pf2e.feats-srd.Dvz54d6aPhjsmUux]`|
|[Life Leap](feats/ancestry-09-aOIZvx5fx5jVHHOO.htm)|Bond vital|`@Compendium[pf2e.feats-srd.aOIZvx5fx5jVHHOO]`|
|[Life-Giving Magic](feats/ancestry-01-hSzNtRNwrma81Eeq.htm)|Magie de la vie|`@Compendium[pf2e.feats-srd.hSzNtRNwrma81Eeq]`|
|[Lifesense](feats/ancestry-05-iNrR4WOB1UO9iiNE.htm)|Sens de la vie|`@Compendium[pf2e.feats-srd.iNrR4WOB1UO9iiNE]`|
|[Light From Darkness](feats/ancestry-09-IQ9C7glCXVgYecz1.htm)|De l'obscurité à la lumière|`@Compendium[pf2e.feats-srd.IQ9C7glCXVgYecz1]`|
|[Light Paws](feats/ancestry-05-EdVMwFRNV1LX1VWh.htm)|Pattes légères|`@Compendium[pf2e.feats-srd.EdVMwFRNV1LX1VWh]`|
|[Light Step](feats/class-06-4o9g5g12yyrfZ3Xd.htm)|Pas léger|`@Compendium[pf2e.feats-srd.4o9g5g12yyrfZ3Xd]`|
|[Light of Revelation](feats/class-04-jQ8JntOdBFHPw5S4.htm)|Lumière de révélation|`@Compendium[pf2e.feats-srd.jQ8JntOdBFHPw5S4]`|
|[Lightning Snares](feats/class-12-OrNcWu1Y7c2O5zU6.htm)|Pose éclair de pièges artisanaux|`@Compendium[pf2e.feats-srd.OrNcWu1Y7c2O5zU6]`|
|[Lightslayer Oath](feats/class-02-9WzZc110jCNnjjRz.htm)|Serment de tueur de lumière|`@Compendium[pf2e.feats-srd.9WzZc110jCNnjjRz]`|
|[Lingering Composition](feats/class-01-sVjATEo8eqkAosNp.htm)|Composition prolongée|`@Compendium[pf2e.feats-srd.sVjATEo8eqkAosNp]`|
|[Linguist Dedication](feats/archetype-02-u6SDVWbzHnBBPNMo.htm)|Dévouement du linguiste|`@Compendium[pf2e.feats-srd.u6SDVWbzHnBBPNMo]`|
|[Linked Focus](feats/class-04-JGfJPx6xkx11zHlW.htm)|Focalisation liée|`@Compendium[pf2e.feats-srd.JGfJPx6xkx11zHlW]`|
|[Lion Blade Dedication](feats/archetype-02-zNxbsYgQgPVxdTLV.htm)|Dévouement de Lame du Lion|`@Compendium[pf2e.feats-srd.zNxbsYgQgPVxdTLV]`|
|[Litany Against Sloth](feats/class-10-RWccAJJ2PaOI0Byp.htm)|Litanie contre la paresse|`@Compendium[pf2e.feats-srd.RWccAJJ2PaOI0Byp]`|
|[Litany Against Wrath](feats/class-06-YhpR5bOquHx2JuIj.htm)|Litanie contre la colère|`@Compendium[pf2e.feats-srd.YhpR5bOquHx2JuIj]`|
|[Litany Of Depravity](feats/class-14-WEUFs37Ids3ZRrqa.htm)|Litanie de dépravation|`@Compendium[pf2e.feats-srd.WEUFs37Ids3ZRrqa]`|
|[Litany Of Self-interest](feats/class-10-qKpaZF0U9VV0YwTJ.htm)|Litanie d'égoïsme|`@Compendium[pf2e.feats-srd.qKpaZF0U9VV0YwTJ]`|
|[Litany of Righteousness](feats/class-14-RbHacJSoe6XHT8Ks.htm)|Litanie de vertu|`@Compendium[pf2e.feats-srd.RbHacJSoe6XHT8Ks]`|
|[Living Hair](feats/class-02-XHaxSBOaFMnBbBKt.htm)|Cheveux vivants|`@Compendium[pf2e.feats-srd.XHaxSBOaFMnBbBKt]`|
|[Living Monolith Dedication](feats/archetype-02-c4ao7b2T92fiM8jR.htm)|Dévouement de Monolithe vivant|`@Compendium[pf2e.feats-srd.c4ao7b2T92fiM8jR]`|
|[Living Rune](feats/archetype-06-MM9NcRyXWX2LFiuF.htm)|Rune vivante|`@Compendium[pf2e.feats-srd.MM9NcRyXWX2LFiuF]`|
|[Lizardfolk Lore](feats/ancestry-01-FhCsnHjdIUyKteCM.htm)|Connaissance des hommes-lézards|`@Compendium[pf2e.feats-srd.FhCsnHjdIUyKteCM]`|
|[Loaner Spell](feats/class-08-80DU0IvIzOIBGuUa.htm)|Sort d'emprunt|`@Compendium[pf2e.feats-srd.80DU0IvIzOIBGuUa]`|
|[Lobbed Attack](feats/archetype-08-XkmrLhyAoxQTLnza.htm)|Attaque lobbée|`@Compendium[pf2e.feats-srd.XkmrLhyAoxQTLnza]`|
|[Locate Lawbreakers](feats/bonus-00-jrDCPvkruz8y740Z.htm)||`@Compendium[pf2e.feats-srd.jrDCPvkruz8y740Z]`|
|[Log Roll](feats/archetype-04-SjJ8BOy5sc8p2H5E.htm)|Rouleur de troncs|`@Compendium[pf2e.feats-srd.SjJ8BOy5sc8p2H5E]`|
|[Long-Nosed Form](feats/ancestry-05-WPz97m5FNlbLIQ6p.htm)|Forme au long nez|`@Compendium[pf2e.feats-srd.WPz97m5FNlbLIQ6p]`|
|[Lore Seeker](feats/archetype-08-BFgrPAK2v3GSKQ5e.htm)|Chercheur de connaissances|`@Compendium[pf2e.feats-srd.BFgrPAK2v3GSKQ5e]`|
|[Loremaster Dedication](feats/archetype-02-lyD6eXl4wc7Pq61q.htm)|Dévouement du Maître savant|`@Compendium[pf2e.feats-srd.lyD6eXl4wc7Pq61q]`|
|[Loremaster's Etude](feats/class-02-VToVEOxiyy53AcEp.htm)|Étude du maître savant|`@Compendium[pf2e.feats-srd.VToVEOxiyy53AcEp]`|
|[Lost in the Crowd](feats/archetype-04-d8RfatiK9UOQANLz.htm)|Se perdre dans la foule|`@Compendium[pf2e.feats-srd.d8RfatiK9UOQANLz]`|
|[Loud Singer](feats/ancestry-05-7AuOURsTkkjrRekp.htm)|Chanteur puissant|`@Compendium[pf2e.feats-srd.7AuOURsTkkjrRekp]`|
|[Loyal Empath](feats/ancestry-05-VK2vAl1SfI4Qrtkt.htm)|Empathe loyal|`@Compendium[pf2e.feats-srd.VK2vAl1SfI4Qrtkt]`|
|[Loyal Warhorse](feats/class-06-axvGLVoAvuD9jU78.htm)|Cheval de bataille fidèle|`@Compendium[pf2e.feats-srd.axvGLVoAvuD9jU78]`|
|[Lucky Keepsake](feats/ancestry-09-zsubK6PaY58fOYCb.htm)|Grigri porte-bonheur|`@Compendium[pf2e.feats-srd.zsubK6PaY58fOYCb]`|
|[Lunge](feats/class-02-rTkr1EqpAN6YtnAh.htm)|Fente|`@Compendium[pf2e.feats-srd.rTkr1EqpAN6YtnAh]`|
|[Lunging Stance](feats/skill-12-ZghzLmYgeE19GqjP.htm)|Posture de fente|`@Compendium[pf2e.feats-srd.ZghzLmYgeE19GqjP]`|
|[Magaambyan Attendant Dedication](feats/archetype-02-ZhToff996PnTESwb.htm)|Dévouement du Gardien de Magaambya|`@Compendium[pf2e.feats-srd.ZhToff996PnTESwb]`|
|[Mage Hunter](feats/class-06-mqLPCNdCSNyY7gyI.htm)|Chasseur de mage|`@Compendium[pf2e.feats-srd.mqLPCNdCSNyY7gyI]`|
|[Magic Arrow](feats/archetype-08-vUFvcrvszXlHvz2Y.htm)|Flèche magique|`@Compendium[pf2e.feats-srd.vUFvcrvszXlHvz2Y]`|
|[Magic Finder](feats/archetype-08-9WMLIcHpwkQpUQfz.htm)|Découvreur de magie|`@Compendium[pf2e.feats-srd.9WMLIcHpwkQpUQfz]`|
|[Magic Hands](feats/class-06-Su4nbNnR0mjgusTT.htm)|Mains magiques|`@Compendium[pf2e.feats-srd.Su4nbNnR0mjgusTT]`|
|[Magic Hide](feats/class-02-y8VecqdECqyH1h6o.htm)|Peau magique|`@Compendium[pf2e.feats-srd.y8VecqdECqyH1h6o]`|
|[Magic Rider](feats/ancestry-17-QXZFvsZDduxwTJYM.htm)|Chevaucheur de magie|`@Compendium[pf2e.feats-srd.QXZFvsZDduxwTJYM]`|
|[Magic Sense](feats/class-12-AXy4A7zTYk1JAiOV.htm)|Sens magique|`@Compendium[pf2e.feats-srd.AXy4A7zTYk1JAiOV]`|
|[Magic Warrior Aspect](feats/archetype-04-G43fiFtxFR24CWRs.htm)|Aspect du Guerrier magique|`@Compendium[pf2e.feats-srd.G43fiFtxFR24CWRs]`|
|[Magic Warrior Dedication](feats/archetype-02-5FOqVC9Q0eEKEf3w.htm)|Dévouement du Guerrier Magique|`@Compendium[pf2e.feats-srd.5FOqVC9Q0eEKEf3w]`|
|[Magic Warrior Transformation](feats/archetype-04-x62kd1CYJNqO2ZsS.htm)|Transformation de Guerrier magique|`@Compendium[pf2e.feats-srd.x62kd1CYJNqO2ZsS]`|
|[Magical Adaptation](feats/archetype-10-DPaZurhC9uyIWPcu.htm)|Adaptation magique|`@Compendium[pf2e.feats-srd.DPaZurhC9uyIWPcu]`|
|[Magical Crafting](feats/skill-02-xWY5omyIcILNR7y1.htm)|Artisanat magique|`@Compendium[pf2e.feats-srd.xWY5omyIcILNR7y1]`|
|[Magical Edification](feats/archetype-04-chmQgMamyaZX90Tc.htm)|Édification magique|`@Compendium[pf2e.feats-srd.chmQgMamyaZX90Tc]`|
|[Magical Scholastics](feats/archetype-04-AuIE19F9rY3YvXf6.htm)|Études magiques|`@Compendium[pf2e.feats-srd.AuIE19F9rY3YvXf6]`|
|[Magical Shorthand](feats/skill-02-v7Bt6hjmzYnLFLeG.htm)|Sténographie magique|`@Compendium[pf2e.feats-srd.v7Bt6hjmzYnLFLeG]`|
|[Magical Trickster](feats/class-04-mxO7u59ms58q7zyj.htm)|Trompeur magique|`@Compendium[pf2e.feats-srd.mxO7u59ms58q7zyj]`|
|[Major Lesson](feats/class-10-ZFkCMl63ogK55Otq.htm)|Leçon majeure|`@Compendium[pf2e.feats-srd.ZFkCMl63ogK55Otq]`|
|[Maker of Miracles](feats/class-20-QDjpZKOrWIV1G8XJ.htm)|Faiseur de miracles|`@Compendium[pf2e.feats-srd.QDjpZKOrWIV1G8XJ]`|
|[Makeshift Strike](feats/archetype-08-UcIyf7bTDf6RwydU.htm)|Frappe expéditive|`@Compendium[pf2e.feats-srd.UcIyf7bTDf6RwydU]`|
|[Malicious Bane](feats/ancestry-05-14u6604yUXvoiIf3.htm)|Imprécation malicieuse|`@Compendium[pf2e.feats-srd.14u6604yUXvoiIf3]`|
|[Manifold Edge](feats/class-18-Dcr63tofZUome1Ze.htm)|Avantages multiples|`@Compendium[pf2e.feats-srd.Dcr63tofZUome1Ze]`|
|[Mantis Form](feats/archetype-08-4YeHOPzo3zOhZQCh.htm)|Forme de la mante|`@Compendium[pf2e.feats-srd.4YeHOPzo3zOhZQCh]`|
|[Many Guises](feats/archetype-08-jZalt2bFGjK8XXcP.htm)|Aspects nombreux|`@Compendium[pf2e.feats-srd.jZalt2bFGjK8XXcP]`|
|[Marsh Runner](feats/ancestry-01-itjTPh76mZfJCBxQ.htm)|Coureur des marais|`@Compendium[pf2e.feats-srd.itjTPh76mZfJCBxQ]`|
|[Marshal Dedication](feats/archetype-02-MYzRfNExDYp25rro.htm)|Dévouement de Capitaine|`@Compendium[pf2e.feats-srd.MYzRfNExDYp25rro]`|
|[Martial Artist Dedication](feats/archetype-02-zn7arorE3VJLNYsb.htm)|Dévouement d'Artiste martial|`@Compendium[pf2e.feats-srd.zn7arorE3VJLNYsb]`|
|[Martial Exercise](feats/archetype-10-lKeJruYQutWlNXyZ.htm)|Exercice martial|`@Compendium[pf2e.feats-srd.lKeJruYQutWlNXyZ]`|
|[Martial Experience](feats/ancestry-05-qXl2cOh3wL3QszCy.htm)|Expérience martiale|`@Compendium[pf2e.feats-srd.qXl2cOh3wL3QszCy]`|
|[Martial Performance](feats/class-01-q1iP3SjAF5uceI0M.htm)|Représentation martiale|`@Compendium[pf2e.feats-srd.q1iP3SjAF5uceI0M]`|
|[Martyr](feats/class-08-fFfRsvDavUsTBDF2.htm)|Martyr|`@Compendium[pf2e.feats-srd.fFfRsvDavUsTBDF2]`|
|[Mask Familiar](feats/archetype-04-GByIIkeHN3JWkZIZ.htm)|Familier masque|`@Compendium[pf2e.feats-srd.GByIIkeHN3JWkZIZ]`|
|[Masked Casting](feats/archetype-08-4xeDjt8eARzAGARP.htm)|Incantation masquée|`@Compendium[pf2e.feats-srd.4xeDjt8eARzAGARP]`|
|[Master Alchemy](feats/archetype-12-f6k9lIrIS4SfnCnG.htm)|Alchimie maître|`@Compendium[pf2e.feats-srd.f6k9lIrIS4SfnCnG]`|
|[Master Bard Spellcasting](feats/archetype-18-eJNzP21lFPV3zWkm.htm)|Incantation du barde maître|`@Compendium[pf2e.feats-srd.eJNzP21lFPV3zWkm]`|
|[Master Cleric Spellcasting](feats/archetype-18-bc6tcXSzakyCbQsS.htm)|Incantation du prêtre maître|`@Compendium[pf2e.feats-srd.bc6tcXSzakyCbQsS]`|
|[Master Druid Spellcasting](feats/archetype-18-218WeRqUqdnguRdS.htm)|Incantation du druide maître|`@Compendium[pf2e.feats-srd.218WeRqUqdnguRdS]`|
|[Master Eldritch Archer Spellcasting](feats/archetype-18-6kz7FPdxDrsPiNti.htm)|Incantation de maître de l'Archer mystique|`@Compendium[pf2e.feats-srd.6kz7FPdxDrsPiNti]`|
|[Master Monster Hunter](feats/class-10-5csEOLLbYUWJDJoS.htm)|Maître chasseur de monstres|`@Compendium[pf2e.feats-srd.5csEOLLbYUWJDJoS]`|
|[Master Oracle Spellcasting](feats/archetype-18-ZkGhDEpqe7fzJuSr.htm)|Incantation d'Oracle maître|`@Compendium[pf2e.feats-srd.ZkGhDEpqe7fzJuSr]`|
|[Master Scroll Cache](feats/archetype-18-lIg5Gzz7W70jfbk1.htm)|Réserve maîtresse de parchemins|`@Compendium[pf2e.feats-srd.lIg5Gzz7W70jfbk1]`|
|[Master Sorcerer Spellcasting](feats/archetype-18-phD0PbElkEeldZ2U.htm)|Incantation de l'ensorceleur maître|`@Compendium[pf2e.feats-srd.phD0PbElkEeldZ2U]`|
|[Master Spotter (Investigator)](feats/archetype-12-t8CAK8ylu23PUxbn.htm)|Dénicheur maître (Enquêteur)|`@Compendium[pf2e.feats-srd.t8CAK8ylu23PUxbn]`|
|[Master Spotter (Ranger)](feats/archetype-12-rW1q7x5CMf9Rh1bi.htm)|Dénicheur maître (Rôdeur)|`@Compendium[pf2e.feats-srd.rW1q7x5CMf9Rh1bi]`|
|[Master Witch Spellcasting](feats/archetype-18-i7ibUqJCl1GXRFEa.htm)|Incantation de sorcière maître|`@Compendium[pf2e.feats-srd.i7ibUqJCl1GXRFEa]`|
|[Master Wizard Spellcasting](feats/archetype-18-PMckhnGYMyiwUNiL.htm)|Incantation du magicien maître|`@Compendium[pf2e.feats-srd.PMckhnGYMyiwUNiL]`|
|[Master of Many Styles](feats/class-16-qX9ZtfaAj6rxrVA7.htm)|Maître des multiples postures|`@Compendium[pf2e.feats-srd.qX9ZtfaAj6rxrVA7]`|
|[Master's Counterspell](feats/archetype-16-EDdJFwarNIJIkP2E.htm)|Maître des contresorts|`@Compendium[pf2e.feats-srd.EDdJFwarNIJIkP2E]`|
|[Masterful Companion](feats/class-18-AwxJcaIrutqMcUC8.htm)|Compagnon magistral|`@Compendium[pf2e.feats-srd.AwxJcaIrutqMcUC8]`|
|[Masterful Obfuscation](feats/archetype-10-TNV1cs1VFqdj4D2M.htm)|Obscurcissement magistral|`@Compendium[pf2e.feats-srd.TNV1cs1VFqdj4D2M]`|
|[Mature Animal Companion (Druid)](feats/class-04-yozSCfdLFHVBbTxj.htm)|Compagnon animal adulte (Druide)|`@Compendium[pf2e.feats-srd.yozSCfdLFHVBbTxj]`|
|[Mature Animal Companion (Ranger)](feats/class-06-tpkJXDpSuGznfzGJ.htm)|Compagnon animal adulte (Rodeur)|`@Compendium[pf2e.feats-srd.tpkJXDpSuGznfzGJ]`|
|[Mature Beastmaster Companion](feats/archetype-04-uCElsebJ45ltmZMT.htm)|Compagnon animal adulte (Maître des bêtes)|`@Compendium[pf2e.feats-srd.uCElsebJ45ltmZMT]`|
|[Mature Trained Companion](feats/archetype-06-n3eOMWQd4kdR3A0l.htm)|Compagnon entraîné adulte|`@Compendium[pf2e.feats-srd.n3eOMWQd4kdR3A0l]`|
|[Mauler Dedication](feats/archetype-02-AimSmPyiMhJZVZRq.htm)|Dévouement de Cogneur|`@Compendium[pf2e.feats-srd.AimSmPyiMhJZVZRq]`|
|[Medic Dedication](feats/archetype-02-MJg24e9fJd7OASvF.htm)|Dévouement de Médecin|`@Compendium[pf2e.feats-srd.MJg24e9fJd7OASvF]`|
|[Medical Researcher](feats/skill-01-6GO3dtFJnsNnSwWz.htm)|Chercheur en médecine|`@Compendium[pf2e.feats-srd.6GO3dtFJnsNnSwWz]`|
|[Meditative Focus](feats/class-12-fcFrxvqbIX6k71os.htm)|Focalisation méditative|`@Compendium[pf2e.feats-srd.fcFrxvqbIX6k71os]`|
|[Meditative Wellspring](feats/class-18-Y5irKSCSBn8z1Qgx.htm)|Source méditative|`@Compendium[pf2e.feats-srd.Y5irKSCSBn8z1Qgx]`|
|[Medusa's Wrath](feats/class-16-DAFF7zJphhDPcAws.htm)|Courroux de la méduse|`@Compendium[pf2e.feats-srd.DAFF7zJphhDPcAws]`|
|[Mega Bomb](feats/class-20-mMMIHLVSr8fyvVQL.htm)|Mégabombe|`@Compendium[pf2e.feats-srd.mMMIHLVSr8fyvVQL]`|
|[Melodious Spell](feats/class-04-kh4bTBgi3C9CjwHK.htm)|Sort mélodieux|`@Compendium[pf2e.feats-srd.kh4bTBgi3C9CjwHK]`|
|[Merciful Elixir](feats/class-10-Mvw3ZFrdwMHedAxY.htm)|Élixir miséricordieux|`@Compendium[pf2e.feats-srd.Mvw3ZFrdwMHedAxY]`|
|[Mercy](feats/class-04-zilwynzk8lIujgZo.htm)|Soulagement|`@Compendium[pf2e.feats-srd.zilwynzk8lIujgZo]`|
|[Mesmerizing Gaze](feats/archetype-06-gOHBzx5Rqa6TZcrm.htm)|Regard hypnotisant|`@Compendium[pf2e.feats-srd.gOHBzx5Rqa6TZcrm]`|
|[Metamagic Channel](feats/class-20-l0Qy74a7CILdE4Th.htm)|Canalisation métamagique|`@Compendium[pf2e.feats-srd.l0Qy74a7CILdE4Th]`|
|[Metamagic Mastery](feats/class-20-kKZ4gnT7okaWS6tB.htm)|Maîtrise de la métamagie (Ensorceleur)|`@Compendium[pf2e.feats-srd.kKZ4gnT7okaWS6tB]`|
|[Methodical Debilitations](feats/class-10-CkK7WwaWnrLXK9sW.htm)|Handicaps méthodiques|`@Compendium[pf2e.feats-srd.CkK7WwaWnrLXK9sW]`|
|[Mighty Bulwark](feats/archetype-10-5TPKikTyN7lrCvzY.htm)|Rempart puissant|`@Compendium[pf2e.feats-srd.5TPKikTyN7lrCvzY]`|
|[Mighty Dragon Shape](feats/archetype-18-DFY3X7Mgl9rESQuu.htm)|Puissante forme de dragon|`@Compendium[pf2e.feats-srd.DFY3X7Mgl9rESQuu]`|
|[Mimic Protections](feats/class-20-mjdLmXLCNaRgMLVw.htm)|Imiter les protections|`@Compendium[pf2e.feats-srd.mjdLmXLCNaRgMLVw]`|
|[Mindblank Mutagen](feats/class-18-C9nb7H5u2ElBXvCR.htm)|Mutagène d’esprit vide|`@Compendium[pf2e.feats-srd.C9nb7H5u2ElBXvCR]`|
|[Minion Guise](feats/skill-04-rAbfuZ1mc3lUYH41.htm)|Aspect de sbire|`@Compendium[pf2e.feats-srd.rAbfuZ1mc3lUYH41]`|
|[Minor Magic](feats/class-02-FrlhErsjR6fEn6kX.htm)|Magie mineure|`@Compendium[pf2e.feats-srd.FrlhErsjR6fEn6kX]`|
|[Miracle Worker](feats/class-18-m2pHkmyGvkwqfSSN.htm)|Fabricateur de miracles|`@Compendium[pf2e.feats-srd.m2pHkmyGvkwqfSSN]`|
|[Miraculous Possibility](feats/class-18-7mGMHb7irGKZ0eQo.htm)|Possibilité miraculeuse|`@Compendium[pf2e.feats-srd.7mGMHb7irGKZ0eQo]`|
|[Mirror Shield](feats/class-10-kQEIPYoKTt69yXxV.htm)|Bouclier miroir|`@Compendium[pf2e.feats-srd.kQEIPYoKTt69yXxV]`|
|[Mist Child](feats/ancestry-05-CXgTn2sE7Do11rlv.htm)|Enfant brumeux|`@Compendium[pf2e.feats-srd.CXgTn2sE7Do11rlv]`|
|[Mixed Maneuver](feats/class-08-jCIBYryi6Y3JwmqH.htm)|Manœuvre mixte|`@Compendium[pf2e.feats-srd.jCIBYryi6Y3JwmqH]`|
|[Mobile Finisher](feats/class-12-Px1QZY0NdO9WAQQS.htm)|Botte mobile|`@Compendium[pf2e.feats-srd.Px1QZY0NdO9WAQQS]`|
|[Mobile Magical Combat](feats/archetype-16-AyP23H3WJkEAIEKd.htm)|Combat magique mobile|`@Compendium[pf2e.feats-srd.AyP23H3WJkEAIEKd]`|
|[Mobile Shot Stance](feats/class-08-rByA8NDI6ZtNgBeT.htm)|Posture de tir mobile|`@Compendium[pf2e.feats-srd.rByA8NDI6ZtNgBeT]`|
|[Mobility](feats/class-02-RAymYRO2SLNNzKVk.htm)|Mobilité|`@Compendium[pf2e.feats-srd.RAymYRO2SLNNzKVk]`|
|[Moment of Clarity](feats/class-01-vRXPsMbyuvoPvbAQ.htm)|Moment de lucidité|`@Compendium[pf2e.feats-srd.vRXPsMbyuvoPvbAQ]`|
|[Monastic Archer Stance](feats/class-01-YG2RxXE9SMfwo6wP.htm)|Posture de l'archer monastique|`@Compendium[pf2e.feats-srd.YG2RxXE9SMfwo6wP]`|
|[Monastic Weaponry](feats/class-01-YW4FdWgzTwRFneFF.htm)|Armes monacales|`@Compendium[pf2e.feats-srd.YW4FdWgzTwRFneFF]`|
|[Monk Dedication](feats/archetype-02-renUpSO6MJXPSXow.htm)|Dévouement du moine|`@Compendium[pf2e.feats-srd.renUpSO6MJXPSXow]`|
|[Monk Moves](feats/archetype-08-TGFbFyv0AUi5gAGO.htm)|Déplacement du moine|`@Compendium[pf2e.feats-srd.TGFbFyv0AUi5gAGO]`|
|[Monk Resiliency](feats/archetype-04-nc5G99d20PI9JKCK.htm)|Résilience du moine|`@Compendium[pf2e.feats-srd.nc5G99d20PI9JKCK]`|
|[Monk's Flurry](feats/archetype-10-bBORql3kKsSyXllk.htm)|Déluge de coups du moine|`@Compendium[pf2e.feats-srd.bBORql3kKsSyXllk]`|
|[Monster Hunter](feats/class-01-YTTJqRKH8QZl6al2.htm)|Chasseur de monstres|`@Compendium[pf2e.feats-srd.YTTJqRKH8QZl6al2]`|
|[Monster Warden](feats/class-02-2yq3l8e2Vzz34yRV.htm)|Garde-monstres|`@Compendium[pf2e.feats-srd.2yq3l8e2Vzz34yRV]`|
|[Monstrosity Shape](feats/class-16-54JzsYCx3uoj7Wlz.htm)|Morphologie monstrueuse|`@Compendium[pf2e.feats-srd.54JzsYCx3uoj7Wlz]`|
|[Monstrous Peacemaker](feats/ancestry-01-ZZzgqFTOtUgnzSLZ.htm)|Médiateur monstrueux|`@Compendium[pf2e.feats-srd.ZZzgqFTOtUgnzSLZ]`|
|[Mortal Healing](feats/skill-02-mEk2POFNU1Q0TQg2.htm)|Soins mortels|`@Compendium[pf2e.feats-srd.mEk2POFNU1Q0TQg2]`|
|[Mortification](feats/archetype-04-gqQK7dDrGPkQDfQQ.htm)|Mortification|`@Compendium[pf2e.feats-srd.gqQK7dDrGPkQDfQQ]`|
|[Mountain Quake](feats/class-14-hO4sKslTrSQMLbGx.htm)|Tremblement de la montagne|`@Compendium[pf2e.feats-srd.hO4sKslTrSQMLbGx]`|
|[Mountain Stance](feats/class-01-ZL5UU9quCTvcWzfY.htm)|Posture de la montagne|`@Compendium[pf2e.feats-srd.ZL5UU9quCTvcWzfY]`|
|[Mountain Stronghold](feats/class-06-n2hawNmzW7DBn1Lm.htm)|Bastion de la montagne|`@Compendium[pf2e.feats-srd.n2hawNmzW7DBn1Lm]`|
|[Mountain's Stoutness](feats/ancestry-09-COP89tjrNhEucuRW.htm)|Robustesse de la montagne|`@Compendium[pf2e.feats-srd.COP89tjrNhEucuRW]`|
|[Mounted Shield](feats/archetype-06-oGOxrqT7DHI43SVk.htm)|Bouclier monté|`@Compendium[pf2e.feats-srd.oGOxrqT7DHI43SVk]`|
|[Mug](feats/class-04-htOsE4hnSj2gzKdi.htm)|Arracher avec violence|`@Compendium[pf2e.feats-srd.htOsE4hnSj2gzKdi]`|
|[Multifarious Muse](feats/class-02-a898miJnjgD93ZsX.htm)|Muse polyvalente|`@Compendium[pf2e.feats-srd.a898miJnjgD93ZsX]`|
|[Multilingual](feats/skill-01-P9HCz0uR6xPHuw72.htm)|Polyglotte|`@Compendium[pf2e.feats-srd.P9HCz0uR6xPHuw72]`|
|[Multilingual Cipher](feats/archetype-04-B4nuabUvA1rk7Hej.htm)|Chiffrage polyglotte|`@Compendium[pf2e.feats-srd.B4nuabUvA1rk7Hej]`|
|[Multishot Stance](feats/class-16-RzhnxgiAopWILCvs.htm)|Posture de tirs multiples|`@Compendium[pf2e.feats-srd.RzhnxgiAopWILCvs]`|
|[Multitalented](feats/ancestry-09-C80vQCKQBGRaqcmq.htm)|Multiples talents|`@Compendium[pf2e.feats-srd.C80vQCKQBGRaqcmq]`|
|[Murderer's Circle](feats/archetype-14-IdKfWg48qMieuzl7.htm)|Cercle meurtrier|`@Compendium[pf2e.feats-srd.IdKfWg48qMieuzl7]`|
|[Murksight](feats/class-08-8CLbJAtgSfwxk2rk.htm)|Vision dans la brume|`@Compendium[pf2e.feats-srd.8CLbJAtgSfwxk2rk]`|
|[Musetouched](feats/ancestry-01-eSP2938INGUG9b3w.htm)|Touché par la muse|`@Compendium[pf2e.feats-srd.eSP2938INGUG9b3w]`|
|[Mutable Familiar](feats/archetype-08-ADgFB8hjUgcXS4bF.htm)|Familier altérable|`@Compendium[pf2e.feats-srd.ADgFB8hjUgcXS4bF]`|
|[Mysterious Breadth](feats/archetype-08-y4Cws9vZj3Bf9uqH.htm)|Élargissement mystérieux|`@Compendium[pf2e.feats-srd.y4Cws9vZj3Bf9uqH]`|
|[Mysterious Repertoire](feats/class-14-Ek3nCIFRreqnSxAQ.htm)|Répertoire mystérieux|`@Compendium[pf2e.feats-srd.Ek3nCIFRreqnSxAQ]`|
|[Mystery Conduit](feats/class-20-eIW5p8qqvsx2MFkY.htm)|Conduit mystérieux|`@Compendium[pf2e.feats-srd.eIW5p8qqvsx2MFkY]`|
|[Nameless Anonymity](feats/archetype-06-l60Ua5Ugv85GnF9b.htm)|Anonymat|`@Compendium[pf2e.feats-srd.l60Ua5Ugv85GnF9b]`|
|[Natural Ambition](feats/ancestry-01-PodajLVxqYSAqVox.htm)|Ambition naturelle|`@Compendium[pf2e.feats-srd.PodajLVxqYSAqVox]`|
|[Natural Illusionist](feats/ancestry-05-HKGyFj2w5dzkf3SW.htm)|Illusionniste naturel|`@Compendium[pf2e.feats-srd.HKGyFj2w5dzkf3SW]`|
|[Natural Medicine](feats/skill-01-WC4xLBGmBsdOdHWu.htm)|Médecine naturelle|`@Compendium[pf2e.feats-srd.WC4xLBGmBsdOdHWu]`|
|[Natural Performer](feats/ancestry-01-ojp39fVYqFBGAw38.htm)|Réprésentation naturelle|`@Compendium[pf2e.feats-srd.ojp39fVYqFBGAw38]`|
|[Natural Skill](feats/ancestry-01-Rfyhlyql1GSoDkI2.htm)|Talent naturel|`@Compendium[pf2e.feats-srd.Rfyhlyql1GSoDkI2]`|
|[Necromantic Physiology](feats/ancestry-05-GgerQCCsGibaGWq0.htm)|Physiologie nécromantique|`@Compendium[pf2e.feats-srd.GgerQCCsGibaGWq0]`|
|[Necromantic Resistance](feats/archetype-04-MnqDDUekqd40HTZc.htm)|Résistance à la nécromancie|`@Compendium[pf2e.feats-srd.MnqDDUekqd40HTZc]`|
|[Necromantic Tenacity](feats/archetype-08-PcDvw2vWTzBEIX7k.htm)|Ténacité contre la nécromancie|`@Compendium[pf2e.feats-srd.PcDvw2vWTzBEIX7k]`|
|[Necrotic Infusion](feats/class-04-6erzXaxxvcXYnL9H.htm)|Injection nécrotique|`@Compendium[pf2e.feats-srd.6erzXaxxvcXYnL9H]`|
|[Never Tire](feats/archetype-04-Btu0tA6SEBS6K1hE.htm)|Jamais fatigué|`@Compendium[pf2e.feats-srd.Btu0tA6SEBS6K1hE]`|
|[Night Magic](feats/ancestry-09-iRpVW1DPKVxlzIzt.htm)|Magie de la nuit|`@Compendium[pf2e.feats-srd.iRpVW1DPKVxlzIzt]`|
|[Nimble Crawl](feats/skill-02-5I97q0FfAeXcUQhs.htm)|Ramper agilement|`@Compendium[pf2e.feats-srd.5I97q0FfAeXcUQhs]`|
|[Nimble Dodge](feats/class-01-dNH8OHEvx3vI9NBQ.htm)|Esquive agile|`@Compendium[pf2e.feats-srd.dNH8OHEvx3vI9NBQ]`|
|[Nimble Elf](feats/ancestry-01-kCO4r8NOm8E2T3eH.htm)|Elfe preste|`@Compendium[pf2e.feats-srd.kCO4r8NOm8E2T3eH]`|
|[Nimble Hooves](feats/ancestry-01-Ffi8L4EDO5OH5tpA.htm)|Sabots agiles|`@Compendium[pf2e.feats-srd.Ffi8L4EDO5OH5tpA]`|
|[Nimble Roll](feats/class-08-F6ZAceuDpiM9bUiF.htm)|Roulade agile|`@Compendium[pf2e.feats-srd.F6ZAceuDpiM9bUiF]`|
|[Nimble Shield Hand](feats/archetype-06-sKuhYCfCbXeRWivv.htm)|Main du bouclier agile|`@Compendium[pf2e.feats-srd.sKuhYCfCbXeRWivv]`|
|[No Cause For Alarm](feats/skill-01-6ON8DjFXSMITZleX.htm)|Pas besoin de s'alarmer|`@Compendium[pf2e.feats-srd.6ON8DjFXSMITZleX]`|
|[No Escape](feats/class-02-lT8XlX1Ig900BblS.htm)|Pas d’échappatoire|`@Compendium[pf2e.feats-srd.lT8XlX1Ig900BblS]`|
|[Nocturnal Sense](feats/class-06-k0NNa5Ko4XhDdBYB.htm)|Sens nocturne|`@Compendium[pf2e.feats-srd.k0NNa5Ko4XhDdBYB]`|
|[Nonlethal Spell](feats/class-02-6QOcQ8ooP1vjQACX.htm)|Sort non létal|`@Compendium[pf2e.feats-srd.6QOcQ8ooP1vjQACX]`|
|[Numb to Death](feats/general-07-gfMP2aMs3YGONVeB.htm)|Insensible à la mort|`@Compendium[pf2e.feats-srd.gfMP2aMs3YGONVeB]`|
|[Observant Explorer](feats/archetype-04-3QLWe5oS9jGJ0Oq4.htm)|Éclaireur observateur|`@Compendium[pf2e.feats-srd.3QLWe5oS9jGJ0Oq4]`|
|[Occult Breadth](feats/archetype-08-Yvxr1Q2TslWiKqqi.htm)|Amplitude occulte|`@Compendium[pf2e.feats-srd.Yvxr1Q2TslWiKqqi]`|
|[Occult Evolution](feats/class-04-lL2fQJ2oRyBgga8Q.htm)|Évolution occulte|`@Compendium[pf2e.feats-srd.lL2fQJ2oRyBgga8Q]`|
|[Occult Resistance](feats/ancestry-09-zPJ2NyMv97AfkN3P.htm)|Résistance occulte|`@Compendium[pf2e.feats-srd.zPJ2NyMv97AfkN3P]`|
|[Oddity Identification](feats/skill-01-hDGosy2ZTwnyctEP.htm)|Identification de bizarreries|`@Compendium[pf2e.feats-srd.hDGosy2ZTwnyctEP]`|
|[Ode To Ouroboros](feats/class-10-cvo3DIL0BIRrDkQ6.htm)|Ode à Ouroboros|`@Compendium[pf2e.feats-srd.cvo3DIL0BIRrDkQ6]`|
|[One For All](feats/class-01-bCizH4ByTwbLcYA1.htm)|Un pour tous|`@Compendium[pf2e.feats-srd.bCizH4ByTwbLcYA1]`|
|[One-Inch Punch](feats/class-06-jaAnxfXVmUQy0IKU.htm)|Coup à distance courte|`@Compendium[pf2e.feats-srd.jaAnxfXVmUQy0IKU]`|
|[One-Millimeter Punch](feats/class-16-4caP26xpkQajkaDp.htm)|Coup à infime distance|`@Compendium[pf2e.feats-srd.4caP26xpkQajkaDp]`|
|[One-Toed Hop](feats/ancestry-05-fD3RSV9nIkJsW6lD.htm)|Bond sur un orteil|`@Compendium[pf2e.feats-srd.fD3RSV9nIkJsW6lD]`|
|[Ongoing Investigation](feats/class-04-KWXoo738KuddWMOB.htm)|Investigation continue|`@Compendium[pf2e.feats-srd.KWXoo738KuddWMOB]`|
|[Ongoing Selfishness](feats/class-01-MRMW0EiuOO20pzG2.htm)|Égoïsme continu|`@Compendium[pf2e.feats-srd.MRMW0EiuOO20pzG2]`|
|[Ongoing Strategy](feats/class-10-CAk1NNG4aO0VuHnZ.htm)|Stratégie continue|`@Compendium[pf2e.feats-srd.CAk1NNG4aO0VuHnZ]`|
|[Ooze Empathy](feats/archetype-08-2mww0DmokYJXUEoA.htm)|Empathie avec les vases|`@Compendium[pf2e.feats-srd.2mww0DmokYJXUEoA]`|
|[Oozemorph Dedication](feats/archetype-02-20jNBiIIxaiOVyi0.htm)|Dévouement de Vasemorphe|`@Compendium[pf2e.feats-srd.20jNBiIIxaiOVyi0]`|
|[Opportune Backstab](feats/class-08-PuyvasWeofGMrhpu.htm)|Coup de poignard opportuniste|`@Compendium[pf2e.feats-srd.PuyvasWeofGMrhpu]`|
|[Opportune Throw](feats/archetype-16-2LdncNMDNJW5Oeyu.htm)|Lancer opportun|`@Compendium[pf2e.feats-srd.2LdncNMDNJW5Oeyu]`|
|[Opportunist](feats/archetype-04-OqU6QXkMrZqToEEi.htm)|Opportuniste|`@Compendium[pf2e.feats-srd.OqU6QXkMrZqToEEi]`|
|[Opportunistic Grapple](feats/archetype-08-BVHDkBa4JMmmj5Sn.htm)|Prise opportuniste|`@Compendium[pf2e.feats-srd.BVHDkBa4JMmmj5Sn]`|
|[Oracle Dedication](feats/archetype-02-OUNj8nXTHwGcEdlh.htm)|Dévouement d'Oracle|`@Compendium[pf2e.feats-srd.OUNj8nXTHwGcEdlh]`|
|[Oracular Providence](feats/class-20-PExiZZTSP4p7TZaW.htm)|Providence oraculaire|`@Compendium[pf2e.feats-srd.PExiZZTSP4p7TZaW]`|
|[Oracular Warning](feats/class-10-Gcliatty0MGYbTVV.htm)|Avertissement oraculaire|`@Compendium[pf2e.feats-srd.Gcliatty0MGYbTVV]`|
|[Orc Ferocity](feats/ancestry-01-PlhPpdwIV0rIAJ8K.htm)|Férocité orque|`@Compendium[pf2e.feats-srd.PlhPpdwIV0rIAJ8K]`|
|[Orc Lore](feats/ancestry-01-254C4rqrE8APDfCf.htm)|Connaissance Orque|`@Compendium[pf2e.feats-srd.254C4rqrE8APDfCf]`|
|[Orc Sight](feats/ancestry-01-SCgqIo8VQZZKZ1Ws.htm)|Vision Orque|`@Compendium[pf2e.feats-srd.SCgqIo8VQZZKZ1Ws]`|
|[Orc Superstition](feats/ancestry-01-EFhh3AenX7wtAmrs.htm)|Superstition Orque|`@Compendium[pf2e.feats-srd.EFhh3AenX7wtAmrs]`|
|[Orc Weapon Carnage](feats/ancestry-05-Y3IUIsopvN13vEvZ.htm)|Carnage avec les armes orques|`@Compendium[pf2e.feats-srd.Y3IUIsopvN13vEvZ]`|
|[Orc Weapon Expertise](feats/ancestry-13-g6M5mapOXVf0g9BG.htm)|Expertise avec les armes orques|`@Compendium[pf2e.feats-srd.g6M5mapOXVf0g9BG]`|
|[Orc Weapon Familiarity](feats/ancestry-01-1tVC0mcxl8sTCg4U.htm)|Familiarité avec les armes orques|`@Compendium[pf2e.feats-srd.1tVC0mcxl8sTCg4U]`|
|[Order Explorer](feats/class-02-9onf3uGvZRnNLPR6.htm)|Explorateur des ordres|`@Compendium[pf2e.feats-srd.9onf3uGvZRnNLPR6]`|
|[Order Magic](feats/class-04-oHdUwzUUblg3neCT.htm)|Magie des ordres|`@Compendium[pf2e.feats-srd.oHdUwzUUblg3neCT]`|
|[Order Spell](feats/archetype-04-TaoV7ArAuZjxpeQB.htm)|Sort d'ordre|`@Compendium[pf2e.feats-srd.TaoV7ArAuZjxpeQB]`|
|[Order Training](feats/class-08-AgMFfp6fdNZ1mAxn.htm)|Entraînement de l'Ordre|`@Compendium[pf2e.feats-srd.AgMFfp6fdNZ1mAxn]`|
|[Ornate Tattoo](feats/ancestry-05-8RZ4VKXJgtl1aN27.htm)|Tatouage ornemental|`@Compendium[pf2e.feats-srd.8RZ4VKXJgtl1aN27]`|
|[Orthographic Mastery](feats/archetype-04-5qXw5Gl9TxbPMZLB.htm)|Maîtrise orthographique|`@Compendium[pf2e.feats-srd.5qXw5Gl9TxbPMZLB]`|
|[Otherworldly Acumen](feats/ancestry-09-KgiYiDz1lWoBMRlF.htm)|Perspicacité surnaturelle|`@Compendium[pf2e.feats-srd.KgiYiDz1lWoBMRlF]`|
|[Otherworldly Magic](feats/ancestry-01-Pat4H0VbmApblZxc.htm)|Magie surnaturelle|`@Compendium[pf2e.feats-srd.Pat4H0VbmApblZxc]`|
|[Overcrowd](feats/ancestry-09-LK6niBb38mEraYRS.htm)|Bondé|`@Compendium[pf2e.feats-srd.LK6niBb38mEraYRS]`|
|[Overextending Feint](feats/class-01-9CaaU5szcf3mtJXD.htm)|Feinte précautionneuse|`@Compendium[pf2e.feats-srd.9CaaU5szcf3mtJXD]`|
|[Overlooked Mastermind](feats/ancestry-01-KuD4Yplwcdolhjsu.htm)|Génie négligé|`@Compendium[pf2e.feats-srd.KuD4Yplwcdolhjsu]`|
|[Overpowering Charge](feats/class-10-C0ozuEhrKh9A1wMO.htm)|Charge surpuissante|`@Compendium[pf2e.feats-srd.C0ozuEhrKh9A1wMO]`|
|[Oversized Throw](feats/class-04-tIE2umG4rQOxm8D8.htm)|Lancer surdimensionné|`@Compendium[pf2e.feats-srd.tIE2umG4rQOxm8D8]`|
|[Overwhelming Blow](feats/class-16-eOwjwVAbl99ZTy5D.htm)|Coup surpuissant|`@Compendium[pf2e.feats-srd.eOwjwVAbl99ZTy5D]`|
|[Overwhelming Breath](feats/class-12-Dwxi1q1OWB1ufFvy.htm)|Souffle surpuissant|`@Compendium[pf2e.feats-srd.Dwxi1q1OWB1ufFvy]`|
|[Overwhelming Energy](feats/class-10-tXNfWDa6P7bCKrCt.htm)|Énergie écrasante|`@Compendium[pf2e.feats-srd.tXNfWDa6P7bCKrCt]`|
|[Pack Rat](feats/ancestry-01-hc4lWhWekIVb0wjL.htm)|Rat empaqueteur|`@Compendium[pf2e.feats-srd.hc4lWhWekIVb0wjL]`|
|[Pale Horse](feats/class-12-dLof0i6LPcChJSBR.htm)|Cheval pâle|`@Compendium[pf2e.feats-srd.dLof0i6LPcChJSBR]`|
|[Panache Paragon](feats/class-20-Oo3yRbmrgqi8dmVs.htm)|Parangon de panache|`@Compendium[pf2e.feats-srd.Oo3yRbmrgqi8dmVs]`|
|[Paradoxical Mystery](feats/class-20-fMAUsLdBd5SDoHBz.htm)|Mystère paradoxal|`@Compendium[pf2e.feats-srd.fMAUsLdBd5SDoHBz]`|
|[Paragon Battle Medicine](feats/skill-07-xOMwuKCf02aFzyp3.htm)|Médecin militaire modèle|`@Compendium[pf2e.feats-srd.xOMwuKCf02aFzyp3]`|
|[Paragon's Guard](feats/class-12-hPDerDCYmag3s0dP.htm)|Protection du Paragon|`@Compendium[pf2e.feats-srd.hPDerDCYmag3s0dP]`|
|[Parry And Riposte](feats/class-18-uIL1pbp7jhYMjYLS.htm)|Parade et riposte|`@Compendium[pf2e.feats-srd.uIL1pbp7jhYMjYLS]`|
|[Parthenogenic Hatchling](feats/ancestry-01-U12Sh43QuY85Prdm.htm)|Couvée parthénogène|`@Compendium[pf2e.feats-srd.U12Sh43QuY85Prdm]`|
|[Parting Shot](feats/class-04-4PvmSnyp3URIqJUM.htm)|Tir en partant|`@Compendium[pf2e.feats-srd.4PvmSnyp3URIqJUM]`|
|[Party Crasher](feats/archetype-07-wqhxZwB1TR8fvpHP.htm)|Parasite mondain|`@Compendium[pf2e.feats-srd.wqhxZwB1TR8fvpHP]`|
|[Path Of Iron](feats/archetype-14-S450JMWfF90oOcv9.htm)|Passage de fer|`@Compendium[pf2e.feats-srd.S450JMWfF90oOcv9]`|
|[Pathfinder Agent Dedication](feats/archetype-02-QnB5ArlO4wlRXQ2E.htm)|Dévouement d'Agent des éclaireurs|`@Compendium[pf2e.feats-srd.QnB5ArlO4wlRXQ2E]`|
|[Patron's Breadth](feats/archetype-08-Wz0LLKjEi8GfKloV.htm)|Amplitude du patron|`@Compendium[pf2e.feats-srd.Wz0LLKjEi8GfKloV]`|
|[Patron's Truth](feats/class-20-SelPslNtTfzxp7fs.htm)|Vérité du patron|`@Compendium[pf2e.feats-srd.SelPslNtTfzxp7fs]`|
|[Peafowl Stance](feats/class-04-C3MgEkPNaIhTddbr.htm)|Posture du paon|`@Compendium[pf2e.feats-srd.C3MgEkPNaIhTddbr]`|
|[Peafowl Strut](feats/class-10-u2fgdFIdQDplKOS3.htm)|Pavane du paon|`@Compendium[pf2e.feats-srd.u2fgdFIdQDplKOS3]`|
|[Peculiar Anatomy](feats/archetype-14-As8cRK5jVzf62fEd.htm)|Anatomie bizarre|`@Compendium[pf2e.feats-srd.As8cRK5jVzf62fEd]`|
|[Peer Beyond](feats/archetype-08-aKFYL4b5Bi7qla2j.htm)|Vision de l'au-delà|`@Compendium[pf2e.feats-srd.aKFYL4b5Bi7qla2j]`|
|[Penetrating Projectile](feats/class-16-tBMMKWYfzD2OMD30.htm)|Projectile pénétrant|`@Compendium[pf2e.feats-srd.tBMMKWYfzD2OMD30]`|
|[Penetrating Shot](feats/class-10-otBBb0ndASgPdAXW.htm)|Tir pénétrant|`@Compendium[pf2e.feats-srd.otBBb0ndASgPdAXW]`|
|[Perfect Clarity](feats/class-18-JkQjKyzfhMWLr9Gs.htm)|Lucidité parfaite|`@Compendium[pf2e.feats-srd.JkQjKyzfhMWLr9Gs]`|
|[Perfect Debilitation](feats/class-18-HBhLR980Q0cb2rxp.htm)|Neutralisation parfaite|`@Compendium[pf2e.feats-srd.HBhLR980Q0cb2rxp]`|
|[Perfect Distraction](feats/class-16-MjhCcwGKyI5dpNIY.htm)|Diversion parfaite|`@Compendium[pf2e.feats-srd.MjhCcwGKyI5dpNIY]`|
|[Perfect Encore](feats/class-20-xhuXpOFOxDpJgngm.htm)|Rappel parfait|`@Compendium[pf2e.feats-srd.xhuXpOFOxDpJgngm]`|
|[Perfect Finisher](feats/class-14-BuaTJxALqxM5EZav.htm)|Botte parfaite|`@Compendium[pf2e.feats-srd.BuaTJxALqxM5EZav]`|
|[Perfect Form Control](feats/class-18-xAIUuSw5A85XEStY.htm)|Contrôle parfait de la forme|`@Compendium[pf2e.feats-srd.xAIUuSw5A85XEStY]`|
|[Perfect Ki Adept](feats/archetype-06-aQjWHbjK1pk8HESM.htm)|Adepte parfait du ki|`@Compendium[pf2e.feats-srd.aQjWHbjK1pk8HESM]`|
|[Perfect Mutagen](feats/class-20-0208T5UrkTY2ombM.htm)|Mutagène parfait|`@Compendium[pf2e.feats-srd.0208T5UrkTY2ombM]`|
|[Perfect Shot](feats/class-18-zrIrpVOvbGS6a3ux.htm)|Tir parfait|`@Compendium[pf2e.feats-srd.zrIrpVOvbGS6a3ux]`|
|[Perfect Strike](feats/archetype-04-Y4gYfrxjSir2Enui.htm)|Coup parfait|`@Compendium[pf2e.feats-srd.Y4gYfrxjSir2Enui]`|
|[Perfection's Path](feats/archetype-12-1ZbLYY5m2YALrrgA.htm)|Voie de la perfection|`@Compendium[pf2e.feats-srd.1ZbLYY5m2YALrrgA]`|
|[Performance Weapon Expert](feats/archetype-16-giupBd3dyOwdeoFl.htm)|Expert avec les armes de scène|`@Compendium[pf2e.feats-srd.giupBd3dyOwdeoFl]`|
|[Performative Weapons Training](feats/archetype-06-SOG0yVNtiDsaNbIO.htm)|Entraînement aux armes de scène|`@Compendium[pf2e.feats-srd.SOG0yVNtiDsaNbIO]`|
|[Perpetual Breadth](feats/class-08-Mvay7CiSN8snJ7DK.htm)|Élargissement perpétuel|`@Compendium[pf2e.feats-srd.Mvay7CiSN8snJ7DK]`|
|[Perpetual Scout](feats/archetype-06-eHvJqBHYqx7UjpPg.htm)|Éclaireur perpétuel|`@Compendium[pf2e.feats-srd.eHvJqBHYqx7UjpPg]`|
|[Persistent Creation](feats/archetype-08-L9aBLPG2veBkVow9.htm)|Création persistante|`@Compendium[pf2e.feats-srd.L9aBLPG2veBkVow9]`|
|[Persistent Mutagen](feats/class-16-GKP2dHkgQw1o0k8g.htm)|Mutagène persistant|`@Compendium[pf2e.feats-srd.GKP2dHkgQw1o0k8g]`|
|[Pervasive Superstition](feats/ancestry-09-VphmK5JRR35SyEhV.htm)|Superstition omniprésente|`@Compendium[pf2e.feats-srd.VphmK5JRR35SyEhV]`|
|[Pesh Skin](feats/class-12-hPanopG3TbXKr52O.htm)|Peau de pesh|`@Compendium[pf2e.feats-srd.hPanopG3TbXKr52O]`|
|[Phase Arrow](feats/archetype-16-LRSTjjBNNlD0XZX8.htm)|Flèche de phase|`@Compendium[pf2e.feats-srd.LRSTjjBNNlD0XZX8]`|
|[Phonetic Training](feats/archetype-04-Wsrw68pklQyaScMX.htm)|Entraînement phonétique|`@Compendium[pf2e.feats-srd.Wsrw68pklQyaScMX]`|
|[Physical Training](feats/archetype-08-8Sdw31YToKhBJ4v4.htm)|Entraînement physique|`@Compendium[pf2e.feats-srd.8Sdw31YToKhBJ4v4]`|
|[Pick Up The Pace](feats/general-03-38uOVS8fLZxraUrg.htm)|Donner le rythme|`@Compendium[pf2e.feats-srd.38uOVS8fLZxraUrg]`|
|[Pickpocket](feats/skill-01-jDdOqFmZLwE4dblQ.htm)|Vol à la tire|`@Compendium[pf2e.feats-srd.jDdOqFmZLwE4dblQ]`|
|[Pied Piping](feats/class-20-UOxDJt8Y7SiCR4xq.htm)|Flûte de Hamelin|`@Compendium[pf2e.feats-srd.UOxDJt8Y7SiCR4xq]`|
|[Pilgrim's Token](feats/skill-01-BqceQIKE0lwIS98s.htm)|Amulette du Pélerin|`@Compendium[pf2e.feats-srd.BqceQIKE0lwIS98s]`|
|[Pin to the Spot](feats/archetype-14-ZTxiM8NExDmxHJDf.htm)|Épingler sur place|`@Compendium[pf2e.feats-srd.ZTxiM8NExDmxHJDf]`|
|[Pinch Time](feats/ancestry-09-8ukixWL8MBJOhPbW.htm)|Temps contracté|`@Compendium[pf2e.feats-srd.8ukixWL8MBJOhPbW]`|
|[Pinning Fire](feats/class-08-AiV2xFhYB90KHt2x.htm)|Tir clouant|`@Compendium[pf2e.feats-srd.AiV2xFhYB90KHt2x]`|
|[Pinpoint Poisoner](feats/class-08-XtIPmZ3Ihq5NJHP2.htm)|Empoisonneur opportun|`@Compendium[pf2e.feats-srd.XtIPmZ3Ihq5NJHP2]`|
|[Pirate Dedication](feats/archetype-02-l1eCHNahsBb7rUkT.htm)|Dévouement du Pirate|`@Compendium[pf2e.feats-srd.l1eCHNahsBb7rUkT]`|
|[Pirate Weapon Training](feats/archetype-04-i86JRWsFRpfEJnZP.htm)|Entraînement avec les armes de pirate|`@Compendium[pf2e.feats-srd.i86JRWsFRpfEJnZP]`|
|[Pitborn](feats/ancestry-01-gxQheZ4xuDWwyzy4.htm)|Né de la fosse|`@Compendium[pf2e.feats-srd.gxQheZ4xuDWwyzy4]`|
|[Pivot Strike](feats/archetype-14-RnxullWsNdbU7fuH.htm)|Frappe pivotante|`@Compendium[pf2e.feats-srd.RnxullWsNdbU7fuH]`|
|[Planar Survival](feats/skill-07-Yj4mpROEjdCjQzMd.htm)|Survie planaire|`@Compendium[pf2e.feats-srd.Yj4mpROEjdCjQzMd]`|
|[Plant Evidence](feats/class-01-eww9tuiXPnZFZ3DU.htm)|Placer une preuve|`@Compendium[pf2e.feats-srd.eww9tuiXPnZFZ3DU]`|
|[Plant Shape](feats/class-10-I9rSWQyueWHQyNxe.htm)|Morphologie végétales|`@Compendium[pf2e.feats-srd.I9rSWQyueWHQyNxe]`|
|[Play To The Crowd](feats/archetype-04-KrYvJ5n06yHCipCZ.htm)|Jouer pour la foule|`@Compendium[pf2e.feats-srd.KrYvJ5n06yHCipCZ]`|
|[Plentiful Snares](feats/archetype-12-wGaxWwJhIXbMJft1.htm)|Pièges artisanaux nombreux|`@Compendium[pf2e.feats-srd.wGaxWwJhIXbMJft1]`|
|[Plot The Future](feats/class-14-N7CM5CmHuZ1cylV9.htm)|Conspirer l'avenir|`@Compendium[pf2e.feats-srd.N7CM5CmHuZ1cylV9]`|
|[Point-Blank Shot](feats/class-01-Yl2wYv24v5kw95aS.htm)|Tir à bout portant|`@Compendium[pf2e.feats-srd.Yl2wYv24v5kw95aS]`|
|[Poison Coat](feats/archetype-06-eHkMcQQ4ejRAFJAt.htm)|Recouvert de poison|`@Compendium[pf2e.feats-srd.eHkMcQQ4ejRAFJAt]`|
|[Poison Resistance](feats/class-02-0lieFKdwgNVXep7u.htm)|Résistance au poison|`@Compendium[pf2e.feats-srd.0lieFKdwgNVXep7u]`|
|[Poison Weapon](feats/class-04-ALbosSUygdq4T1Yk.htm)|Arme empoisonnée|`@Compendium[pf2e.feats-srd.ALbosSUygdq4T1Yk]`|
|[Poisoner Dedication](feats/archetype-02-y7DDs03GtDnmhxFp.htm)|Dévouement d'Empoisonneur|`@Compendium[pf2e.feats-srd.y7DDs03GtDnmhxFp]`|
|[Poisoner's Twist](feats/archetype-04-pKoW1X95LjmWn5Jq.htm)|Ajout de l'empoisonneur|`@Compendium[pf2e.feats-srd.pKoW1X95LjmWn5Jq]`|
|[Portentous Spell](feats/class-16-rRbMOxX1QTHIIwAi.htm)|Sort de mauvais augure|`@Compendium[pf2e.feats-srd.rRbMOxX1QTHIIwAi]`|
|[Positioning Assault](feats/class-08-TEH73yqZBqByO624.htm)|Assaut de positionnement|`@Compendium[pf2e.feats-srd.TEH73yqZBqByO624]`|
|[Posse](feats/archetype-04-N0YcU8mIJnQ4C2N6.htm)|Détachement|`@Compendium[pf2e.feats-srd.N0YcU8mIJnQ4C2N6]`|
|[Potent Poisoner](feats/class-10-zKL1lRcIbFblp2M2.htm)|Empoisonneur puissant|`@Compendium[pf2e.feats-srd.zKL1lRcIbFblp2M2]`|
|[Poultice Preparation](feats/archetype-04-4BbfHsGPRHPHfIGY.htm)|Préparation de cataplasme|`@Compendium[pf2e.feats-srd.4BbfHsGPRHPHfIGY]`|
|[Power Attack](feats/class-01-2xk4jdwcCfmasYfT.htm)|Attaque en puissance|`@Compendium[pf2e.feats-srd.2xk4jdwcCfmasYfT]`|
|[Powerful Leap](feats/skill-02-HJYQlmGTdtyGWr6a.htm)|Bond puissant|`@Compendium[pf2e.feats-srd.HJYQlmGTdtyGWr6a]`|
|[Powerful Shove](feats/class-04-oIE88rIDEFNm83Mr.htm)|Poussée en puissance|`@Compendium[pf2e.feats-srd.oIE88rIDEFNm83Mr]`|
|[Powerful Snares](feats/class-08-27R8yZcY2uXH6pZN.htm)|Redoutables pièges artisanaux|`@Compendium[pf2e.feats-srd.27R8yZcY2uXH6pZN]`|
|[Powerful Sneak](feats/class-18-Hzzf7bi8xBMi6DCL.htm)|Puissante furtivité|`@Compendium[pf2e.feats-srd.Hzzf7bi8xBMi6DCL]`|
|[Practiced Defender](feats/archetype-10-vu0R9VfRZ6RWTczZ.htm)|Défenseur expérimenté|`@Compendium[pf2e.feats-srd.vu0R9VfRZ6RWTczZ]`|
|[Practiced Guidance](feats/archetype-08-0Bu48kn3Deq9gHQE.htm)|Guide Exercé|`@Compendium[pf2e.feats-srd.0Bu48kn3Deq9gHQE]`|
|[Practiced Paddler](feats/ancestry-05-I3EMC4pqkEWrodpq.htm)|Pagayeur accompli|`@Compendium[pf2e.feats-srd.I3EMC4pqkEWrodpq]`|
|[Prairie Rider](feats/ancestry-01-A9HQ2bMAge2aGgWx.htm)|Chevaucheur des prairies|`@Compendium[pf2e.feats-srd.A9HQ2bMAge2aGgWx]`|
|[Prayer Attack](feats/archetype-14-AtpMrGXaMPJtDIDR.htm)|Attaque prière|`@Compendium[pf2e.feats-srd.AtpMrGXaMPJtDIDR]`|
|[Precious Arrow](feats/archetype-08-MlxfNv98LZKfYl64.htm)|Flèche précieuse|`@Compendium[pf2e.feats-srd.MlxfNv98LZKfYl64]`|
|[Precise Debilitations](feats/class-10-wvJBIzgS298ZRp6w.htm)|Handicaps précis|`@Compendium[pf2e.feats-srd.wvJBIzgS298ZRp6w]`|
|[Precise Finisher](feats/class-06-qeLpqH2cMSmIrILV.htm)|Botte précise|`@Compendium[pf2e.feats-srd.qeLpqH2cMSmIrILV]`|
|[Predator's Pounce](feats/class-12-aFUxNGur3Hma8DKy.htm)|Bond de prédateur|`@Compendium[pf2e.feats-srd.aFUxNGur3Hma8DKy]`|
|[Predictable!](feats/class-04-cErltcAC7OVnIyO1.htm)|Prévisible !|`@Compendium[pf2e.feats-srd.cErltcAC7OVnIyO1]`|
|[Predictive Purchase (Investigator)](feats/class-06-QSuwyX84U26OLzZI.htm)|Achat intuitif (Enquêteur)|`@Compendium[pf2e.feats-srd.QSuwyX84U26OLzZI]`|
|[Predictive Purchase (Rogue)](feats/class-08-Yw0qVCDu94Y5TgxQ.htm)|Achat intuitif (Roublard)|`@Compendium[pf2e.feats-srd.Yw0qVCDu94Y5TgxQ]`|
|[Premonition Of Avoidance](feats/class-01-KYl1rWFOHe0e6VqJ.htm)|Évitement prémonitoire|`@Compendium[pf2e.feats-srd.KYl1rWFOHe0e6VqJ]`|
|[Premonition Of Clarity](feats/class-14-2h8a6pKhXTXwpJjP.htm)|Clarté prémonitoire|`@Compendium[pf2e.feats-srd.2h8a6pKhXTXwpJjP]`|
|[Preparation](feats/class-12-zzMugLCUkQQPa2qT.htm)|Préparation|`@Compendium[pf2e.feats-srd.zzMugLCUkQQPa2qT]`|
|[Prescient Consumable](feats/general-07-lHFz4MmebvPqTb0A.htm)|Consommable prescient|`@Compendium[pf2e.feats-srd.lHFz4MmebvPqTb0A]`|
|[Prescient Planner](feats/general-03-bh2jHyyYrkLMsIdX.htm)|Planificateur prescient|`@Compendium[pf2e.feats-srd.bh2jHyyYrkLMsIdX]`|
|[Prevailing Position](feats/class-10-Xw7qG0SHepXx24vl.htm)|Position prédominante|`@Compendium[pf2e.feats-srd.Xw7qG0SHepXx24vl]`|
|[Pride in Arms](feats/ancestry-09-CCY6VsGjp5fdmM6K.htm)|Fierté militaire|`@Compendium[pf2e.feats-srd.CCY6VsGjp5fdmM6K]`|
|[Primal Aegis](feats/class-18-AbjVJIBdNjbQbVnV.htm)|Égide primordiale|`@Compendium[pf2e.feats-srd.AbjVJIBdNjbQbVnV]`|
|[Primal Breadth](feats/archetype-08-niKCBA11zzgGT1PU.htm)|Amplitude primordiale|`@Compendium[pf2e.feats-srd.niKCBA11zzgGT1PU]`|
|[Primal Evolution](feats/class-04-K3TasgeZLJQ84qtZ.htm)|Évolution primordiale|`@Compendium[pf2e.feats-srd.K3TasgeZLJQ84qtZ]`|
|[Primal Focus](feats/class-12-RSUmrIiFBEchdM8B.htm)|Focalisation primordiale|`@Compendium[pf2e.feats-srd.RSUmrIiFBEchdM8B]`|
|[Primal Summons](feats/class-12-I00uuseTfPypVgLQ.htm)|Convocations primordiales|`@Compendium[pf2e.feats-srd.I00uuseTfPypVgLQ]`|
|[Primal Wellspring](feats/class-18-SzWeWBuzg3e0k98A.htm)|Source primordiale|`@Compendium[pf2e.feats-srd.SzWeWBuzg3e0k98A]`|
|[Pristine Weapon](feats/class-10-nWxNV9pFeBHV671W.htm)|Arme immaculée|`@Compendium[pf2e.feats-srd.nWxNV9pFeBHV671W]`|
|[Project Persona](feats/ancestry-05-twnZopGlB392hmqH.htm)|Projeter un personnage|`@Compendium[pf2e.feats-srd.twnZopGlB392hmqH]`|
|[Protect Ally](feats/archetype-08-ASWqQ6RB7cfCsUo0.htm)|Protéger un allié|`@Compendium[pf2e.feats-srd.ASWqQ6RB7cfCsUo0]`|
|[Protective Sheath](feats/ancestry-05-sTqdFqWVL9yxi5wt.htm)|Fourreau protecteur|`@Compendium[pf2e.feats-srd.sTqdFqWVL9yxi5wt]`|
|[Provocator Dedication](feats/archetype-10-VJ3wIsfmabK02SNg.htm)|Dévouement du Provocator|`@Compendium[pf2e.feats-srd.VJ3wIsfmabK02SNg]`|
|[Quah Bond](feats/ancestry-01-liqDAd5xk2b3xzeE.htm)|Lien Quah|`@Compendium[pf2e.feats-srd.liqDAd5xk2b3xzeE]`|
|[Quaking Stomp](feats/class-20-FMjihpGLn9eQ14Gw.htm)|Piétinement sismique|`@Compendium[pf2e.feats-srd.FMjihpGLn9eQ14Gw]`|
|[Quick Alchemy](feats/archetype-04-NlEZ0piDxg9buXCL.htm)|Alchimie rapide|`@Compendium[pf2e.feats-srd.NlEZ0piDxg9buXCL]`|
|[Quick Block](feats/class-08-DalEte4OQyLzR7nd.htm)|Blocage éclair|`@Compendium[pf2e.feats-srd.DalEte4OQyLzR7nd]`|
|[Quick Bomber](feats/class-01-xbg1scOIT7RI9Fij.htm)|Artificier rapide|`@Compendium[pf2e.feats-srd.xbg1scOIT7RI9Fij]`|
|[Quick Change](feats/archetype-07-v1PtAazmEFhTp6fZ.htm)|Changement rapide|`@Compendium[pf2e.feats-srd.v1PtAazmEFhTp6fZ]`|
|[Quick Climb](feats/skill-07-P04Hw8E6WAWARKHP.htm)|Escalade rapide|`@Compendium[pf2e.feats-srd.P04Hw8E6WAWARKHP]`|
|[Quick Coercion](feats/skill-01-ar2DUlvDK4LDcH9J.htm)|Contrainte rapide|`@Compendium[pf2e.feats-srd.ar2DUlvDK4LDcH9J]`|
|[Quick Contacts](feats/skill-02-ThoOsKjn5xCuZUqM.htm)|Contacts rapides|`@Compendium[pf2e.feats-srd.ThoOsKjn5xCuZUqM]`|
|[Quick Disguise](feats/skill-02-e4KB4pSkx2lDBNw3.htm)|Déguisement rapide|`@Compendium[pf2e.feats-srd.e4KB4pSkx2lDBNw3]`|
|[Quick Draw](feats/class-02-qFt6zyWVX1njJf1l.htm)|Arme en main|`@Compendium[pf2e.feats-srd.qFt6zyWVX1njJf1l]`|
|[Quick Fix](feats/archetype-04-RlhvppSmQRqL2RUe.htm)|Fixation rapide|`@Compendium[pf2e.feats-srd.RlhvppSmQRqL2RUe]`|
|[Quick Identification](feats/skill-01-IlOQuCQIhjJpig3S.htm)|Identification rapide|`@Compendium[pf2e.feats-srd.IlOQuCQIhjJpig3S]`|
|[Quick Juggler](feats/archetype-04-otLs7XzMXR1cZKGe.htm)|Jongleur rapide|`@Compendium[pf2e.feats-srd.otLs7XzMXR1cZKGe]`|
|[Quick Jump](feats/skill-01-ZBhvJ9O8MvBFAlhq.htm)|Saut rapide|`@Compendium[pf2e.feats-srd.ZBhvJ9O8MvBFAlhq]`|
|[Quick Mount](feats/archetype-04-G9l2g7sDpPVbZJza.htm)|Vite en selle|`@Compendium[pf2e.feats-srd.G9l2g7sDpPVbZJza]`|
|[Quick Recognition](feats/skill-07-2rSyTfPgAmNAo01r.htm)|Reconnaissance rapide|`@Compendium[pf2e.feats-srd.2rSyTfPgAmNAo01r]`|
|[Quick Repair](feats/skill-01-ASy9AKEIRxPYUi5o.htm)|Réparation rapide|`@Compendium[pf2e.feats-srd.ASy9AKEIRxPYUi5o]`|
|[Quick Reversal](feats/class-04-SnIx3FhBuuq6AZD0.htm)|Revirement rapide|`@Compendium[pf2e.feats-srd.SnIx3FhBuuq6AZD0]`|
|[Quick Shield Block](feats/class-08-pRqcm5P2ZFihSpVI.htm)|Blocage au bouclier éclair|`@Compendium[pf2e.feats-srd.pRqcm5P2ZFihSpVI]`|
|[Quick Shot](feats/archetype-04-xRTlbvvBzURgC6M2.htm)|Tir rapide|`@Compendium[pf2e.feats-srd.xRTlbvvBzURgC6M2]`|
|[Quick Snares](feats/class-06-KW6K5Zv4J7ClWkKA.htm)|Pose rapide de pièges artisanaux|`@Compendium[pf2e.feats-srd.KW6K5Zv4J7ClWkKA]`|
|[Quick Squeeze](feats/skill-01-3G8xUlgCjRmRJNfP.htm)|Se faufiler rapidement|`@Compendium[pf2e.feats-srd.3G8xUlgCjRmRJNfP]`|
|[Quick Stow (Ratfolk)](feats/ancestry-05-LAArv2uv6TOkTzQO.htm)|Rangement Rapide (Homme-rat)|`@Compendium[pf2e.feats-srd.LAArv2uv6TOkTzQO]`|
|[Quick Stow (Swordmaster)](feats/archetype-08-i7hNUqiJsB8hgIET.htm)|Rangement rapide (Maître des épées)|`@Compendium[pf2e.feats-srd.i7hNUqiJsB8hgIET]`|
|[Quick Study](feats/archetype-04-aLJsBBZzlUK3G8MW.htm)|Étude rapide|`@Compendium[pf2e.feats-srd.aLJsBBZzlUK3G8MW]`|
|[Quick Swim](feats/skill-07-bFoh3267kNLk68cU.htm)|Nage rapide|`@Compendium[pf2e.feats-srd.bFoh3267kNLk68cU]`|
|[Quick Unlock](feats/skill-07-7GmXKrkzmInkFyEr.htm)|Déverrouillage rapide|`@Compendium[pf2e.feats-srd.7GmXKrkzmInkFyEr]`|
|[Quicken Heartbeat](feats/archetype-10-i8MnyasCDo3j65Xd.htm)|Battements de cœur accélérés|`@Compendium[pf2e.feats-srd.i8MnyasCDo3j65Xd]`|
|[Quickened Casting](feats/class-10-z2ptq23nNBOeEI7H.htm)|Incantation accélérée|`@Compendium[pf2e.feats-srd.z2ptq23nNBOeEI7H]`|
|[Quiet Allies](feats/skill-02-pekKtubQTkG9m1xK.htm)|Alliés silencieux|`@Compendium[pf2e.feats-srd.pekKtubQTkG9m1xK]`|
|[Quivering Palm](feats/class-16-g1wBP9Z5HRqDe9FE.htm)|Paume vibratoire|`@Compendium[pf2e.feats-srd.g1wBP9Z5HRqDe9FE]`|
|[Radiant Blade Master](feats/class-20-jYEMVfrXJLpXS6aC.htm)|Maître lame radieux|`@Compendium[pf2e.feats-srd.jYEMVfrXJLpXS6aC]`|
|[Radiant Blade Spirit](feats/class-10-h5ksUZlrVGBjq6p4.htm)|Esprit lame radieux|`@Compendium[pf2e.feats-srd.h5ksUZlrVGBjq6p4]`|
|[Radiant Infusion](feats/class-04-JcXzKwrdMkNszrJQ.htm)|Imprégnation irradiante|`@Compendium[pf2e.feats-srd.JcXzKwrdMkNszrJQ]`|
|[Raging Athlete](feats/class-04-sEWYOllJ6rYoXK4K.htm)|Athlète enragé|`@Compendium[pf2e.feats-srd.sEWYOllJ6rYoXK4K]`|
|[Raging Intimidation](feats/class-01-XseJI9XhKNtZN8pv.htm)|Intimidation enragée|`@Compendium[pf2e.feats-srd.XseJI9XhKNtZN8pv]`|
|[Raging Thrower](feats/class-01-5pGMffGKkBTZKvjw.htm)|Lanceur enragé|`@Compendium[pf2e.feats-srd.5pGMffGKkBTZKvjw]`|
|[Rain of Embers Stance](feats/class-01-rbiMK71SvGZGRLJ1.htm)|Posture de la pluie de charbons ardents|`@Compendium[pf2e.feats-srd.rbiMK71SvGZGRLJ1]`|
|[Rain-Scribe Mobility](feats/archetype-10-nobsCgNmsDX6aKR5.htm)|Mobilité du Scribe de la pluie|`@Compendium[pf2e.feats-srd.nobsCgNmsDX6aKR5]`|
|[Rain-Scribe Sustenance](feats/archetype-06-jRJqKkm6NnHcL8HO.htm)|Subsistance du Scribe de la pluie|`@Compendium[pf2e.feats-srd.jRJqKkm6NnHcL8HO]`|
|[Rallying Charge](feats/archetype-06-df4cBV3qZn3qNUmP.htm)|Charge de ralliement|`@Compendium[pf2e.feats-srd.df4cBV3qZn3qNUmP]`|
|[Rampaging Ferocity](feats/ancestry-17-9eaUS0jJCpxuNXO5.htm)|Férocité dévastatrice|`@Compendium[pf2e.feats-srd.9eaUS0jJCpxuNXO5]`|
|[Ranged Reprisal](feats/class-01-iTdbkP07UFMOo1rI.htm)|Représailles à distance|`@Compendium[pf2e.feats-srd.iTdbkP07UFMOo1rI]`|
|[Ranger Dedication](feats/archetype-02-R00qiDE5pBctgtyU.htm)|Dévouement du rôdeur|`@Compendium[pf2e.feats-srd.R00qiDE5pBctgtyU]`|
|[Ranger Resiliency](feats/archetype-04-JKvP4pFHzwWsHu2n.htm)|Résilience du rôdeur|`@Compendium[pf2e.feats-srd.JKvP4pFHzwWsHu2n]`|
|[Ranger's Bramble](feats/class-06-Ea6Z5cxeBCCtPD5R.htm)|Roncier du rôdeur|`@Compendium[pf2e.feats-srd.Ea6Z5cxeBCCtPD5R]`|
|[Rapid Affixture](feats/skill-07-x9xA8P2Vlz98He7C.htm)|Fixation rapide|`@Compendium[pf2e.feats-srd.x9xA8P2Vlz98He7C]`|
|[Rapid Mantel](feats/skill-02-FJK7JTLSgugRIlvS.htm)|Accroche rapide|`@Compendium[pf2e.feats-srd.FJK7JTLSgugRIlvS]`|
|[Rapid Response](feats/class-02-OcBaEnGdDm6CuSnr.htm)|réponse rapide|`@Compendium[pf2e.feats-srd.OcBaEnGdDm6CuSnr]`|
|[Rat Familiar](feats/ancestry-01-W2LmEXJH75tyeCSn.htm)|Familier rat|`@Compendium[pf2e.feats-srd.W2LmEXJH75tyeCSn]`|
|[Rat Form](feats/ancestry-09-SuEmijj909yxmYOO.htm)|Forme de rat|`@Compendium[pf2e.feats-srd.SuEmijj909yxmYOO]`|
|[Rat Magic](feats/ancestry-05-8MMYFIudSDkdo8cx.htm)|Magie du rat|`@Compendium[pf2e.feats-srd.8MMYFIudSDkdo8cx]`|
|[Ratfolk Lore](feats/ancestry-01-mQYO501xyMgtIQ3W.htm)|Connaissance des Hommes-rats|`@Compendium[pf2e.feats-srd.mQYO501xyMgtIQ3W]`|
|[Ratspeak](feats/ancestry-01-VDiMapgJoFI3CCol.htm)|Langage des rats|`@Compendium[pf2e.feats-srd.VDiMapgJoFI3CCol]`|
|[Ravening's Desperation](feats/skill-01-d8AjCqU30z7IOpos.htm)|Énergie du désespoir|`@Compendium[pf2e.feats-srd.d8AjCqU30z7IOpos]`|
|[Razor Claws](feats/ancestry-01-VatkAzfBYjA6z6OP.htm)|Griffes rasoir|`@Compendium[pf2e.feats-srd.VatkAzfBYjA6z6OP]`|
|[Razzle-Dazzle](feats/ancestry-01-VcnOEAM3UR7oS0D5.htm)|Éblouissement|`@Compendium[pf2e.feats-srd.VcnOEAM3UR7oS0D5]`|
|[Reach Spell](feats/class-01-BWomK7EVY0WXxWgh.htm)|Sort éloigné|`@Compendium[pf2e.feats-srd.BWomK7EVY0WXxWgh]`|
|[Reactive Distraction](feats/class-20-LDIZtE7saDLSBduG.htm)|Diversion réactive|`@Compendium[pf2e.feats-srd.LDIZtE7saDLSBduG]`|
|[Reactive Interference](feats/class-12-xBqDeQFzvuDfqhZC.htm)|Interférence réactive|`@Compendium[pf2e.feats-srd.xBqDeQFzvuDfqhZC]`|
|[Reactive Pursuit](feats/class-04-VVsYBmVi2E1u9E5Z.htm)|Poursuite réactive|`@Compendium[pf2e.feats-srd.VVsYBmVi2E1u9E5Z]`|
|[Reactive Shield](feats/class-01-w8Ycgeq2zfyshtoS.htm)|Bouclier réactif|`@Compendium[pf2e.feats-srd.w8Ycgeq2zfyshtoS]`|
|[Reactive Transformation](feats/class-14-4Q9Q41KLPYJMdV4b.htm)|Transformation réactive|`@Compendium[pf2e.feats-srd.4Q9Q41KLPYJMdV4b]`|
|[Read Disaster](feats/class-08-HLC9g1pwluDl6vy7.htm)|Lire le désastre|`@Compendium[pf2e.feats-srd.HLC9g1pwluDl6vy7]`|
|[Read Lips](feats/skill-01-yUuU9xyotrpwpTyC.htm)|Lire sur les lèvres|`@Compendium[pf2e.feats-srd.yUuU9xyotrpwpTyC]`|
|[Read Shibboleths](feats/archetype-07-Crd3qMecF9FYHjuH.htm)|Lire les signes distinctifs|`@Compendium[pf2e.feats-srd.Crd3qMecF9FYHjuH]`|
|[Reaper of Repose](feats/archetype-12-xmccXo6U7P0IMM3z.htm)|Violeur de repos|`@Compendium[pf2e.feats-srd.xmccXo6U7P0IMM3z]`|
|[Reason Rapidly](feats/class-12-pm9PS32YNLJ2wp4o.htm)|Déduction rapide|`@Compendium[pf2e.feats-srd.pm9PS32YNLJ2wp4o]`|
|[Rebounding Toss](feats/class-02-5SBFayX7JqKYANwa.htm)|Lancer rebondissant|`@Compendium[pf2e.feats-srd.5SBFayX7JqKYANwa]`|
|[Reckless Abandon (Barbarian)](feats/class-16-EWeso1zDkCLGlnsW.htm)|Dangereux abandon (Barbare)|`@Compendium[pf2e.feats-srd.EWeso1zDkCLGlnsW]`|
|[Reckless Abandon (Goblin)](feats/ancestry-17-fqw1ELaqavuKLHIj.htm)|Dangereux abandon (Gobelin)|`@Compendium[pf2e.feats-srd.fqw1ELaqavuKLHIj]`|
|[Reclaim Spell](feats/class-20-LLrGafdJij7qiWZi.htm)|Sort Repris|`@Compendium[pf2e.feats-srd.LLrGafdJij7qiWZi]`|
|[Recognize Spell](feats/skill-01-MjQyTcV8Jiv1Jtln.htm)|Reconnaître des sorts|`@Compendium[pf2e.feats-srd.MjQyTcV8Jiv1Jtln]`|
|[Recognize Threat](feats/archetype-08-5vRXZcGAbqKRoaqL.htm)|Reconnaître la menace|`@Compendium[pf2e.feats-srd.5vRXZcGAbqKRoaqL]`|
|[Recollect Studies](feats/archetype-12-QoPooHpBjPh1sjRD.htm)|Rappel de l'enseignement|`@Compendium[pf2e.feats-srd.QoPooHpBjPh1sjRD]`|
|[Reconstruct The Scene](feats/class-16-Bp02C07s4RTS4vsV.htm)|Reconstituer la scène|`@Compendium[pf2e.feats-srd.Bp02C07s4RTS4vsV]`|
|[Recover Spell](feats/archetype-10-9Qn5E7Ujye9KdxOj.htm)|Récupération de sort|`@Compendium[pf2e.feats-srd.9Qn5E7Ujye9KdxOj]`|
|[Red Herring](feats/class-02-wNr02jsG5nRF23YO.htm)|Fausse piste|`@Compendium[pf2e.feats-srd.wNr02jsG5nRF23YO]`|
|[Red Mantis Assassin Dedication](feats/archetype-02-UuPZ7drPBnSmI8Eo.htm)|Dévouement d'Assassin des Mantes rouges|`@Compendium[pf2e.feats-srd.UuPZ7drPBnSmI8Eo]`|
|[Reflect Spell](feats/class-14-k72W0qMXsX5ekJTF.htm)|Sort réfléchi|`@Compendium[pf2e.feats-srd.k72W0qMXsX5ekJTF]`|
|[Reflecting Riposte](feats/class-20-Yec6UwJf5FLvAbZ4.htm)|Riposte réfléchissante|`@Compendium[pf2e.feats-srd.Yec6UwJf5FLvAbZ4]`|
|[Reflexive Catch](feats/archetype-04-irDFSzKCaF4ux3bx.htm)|Réception instinctive|`@Compendium[pf2e.feats-srd.irDFSzKCaF4ux3bx]`|
|[Reflexive Grip](feats/archetype-08-jaekke9HomT4PZ9b.htm)|Emprise réflexive|`@Compendium[pf2e.feats-srd.jaekke9HomT4PZ9b]`|
|[Reflexive Riposte](feats/class-10-uotQ9yqetPoAWrfW.htm)|Riposte machinale|`@Compendium[pf2e.feats-srd.uotQ9yqetPoAWrfW]`|
|[Reflexive Shield](feats/class-06-dHUoQVzDa9Cf4QCG.htm)|Bouclier instinctif|`@Compendium[pf2e.feats-srd.dHUoQVzDa9Cf4QCG]`|
|[Rejuvenating Touch](feats/class-18-xZrTjUub7V09sXZF.htm)|Toucher rajeunissant|`@Compendium[pf2e.feats-srd.xZrTjUub7V09sXZF]`|
|[Relentless Disarm](feats/archetype-06-9E1FLGp4CNBEwiZE.htm)|Désarmement acharné|`@Compendium[pf2e.feats-srd.9E1FLGp4CNBEwiZE]`|
|[Relentless Stalker](feats/class-02-e9NcDk6ds5YebGvb.htm)|Harceleur infatigable|`@Compendium[pf2e.feats-srd.e9NcDk6ds5YebGvb]`|
|[Relentless Wings](feats/ancestry-17-CuyuwFht0rVbGMca.htm)|Ailes sans repos|`@Compendium[pf2e.feats-srd.CuyuwFht0rVbGMca]`|
|[Reliable Luck](feats/ancestry-17-QgNo1s6nVbKPU4St.htm)|Chance fiable|`@Compendium[pf2e.feats-srd.QgNo1s6nVbKPU4St]`|
|[Remediate](feats/class-16-H5uZqYVClk3s62ce.htm)|Rééquilibrer|`@Compendium[pf2e.feats-srd.H5uZqYVClk3s62ce]`|
|[Remember Your Training](feats/archetype-04-YhclkX1nfyUU8RtO.htm)|Entraînement mémorable|`@Compendium[pf2e.feats-srd.YhclkX1nfyUU8RtO]`|
|[Remorseless Lash](feats/ancestry-01-iRztLGEK5OfjZTPg.htm)|Fouetter sans remord|`@Compendium[pf2e.feats-srd.iRztLGEK5OfjZTPg]`|
|[Remote Trigger](feats/archetype-06-sk5HspGGnLW8b6bX.htm)|Déclenchement à distance|`@Compendium[pf2e.feats-srd.sk5HspGGnLW8b6bX]`|
|[Renewed Vigor](feats/class-08-3hhCDWPm021hvicR.htm)|Regain de vigueur|`@Compendium[pf2e.feats-srd.3hhCDWPm021hvicR]`|
|[Replenishment of War](feats/class-10-aMef2VM4mSxl0pmy.htm)|Récupération martiale|`@Compendium[pf2e.feats-srd.aMef2VM4mSxl0pmy]`|
|[Reprepare Spell](feats/class-18-a1TSGGsA6b5gjP3H.htm)|Recyclage de sort|`@Compendium[pf2e.feats-srd.a1TSGGsA6b5gjP3H]`|
|[Reptile Speaker](feats/ancestry-01-GEvaoKgQteMrd4ub.htm)|Locuteur reptilien|`@Compendium[pf2e.feats-srd.GEvaoKgQteMrd4ub]`|
|[Rescuer's Press](feats/archetype-04-wscmghwNCXvZtKsz.htm)|Poussée du sauveteur|`@Compendium[pf2e.feats-srd.wscmghwNCXvZtKsz]`|
|[Resilient Touch](feats/class-10-tCuMXQ0yMrCNwzqW.htm)|Toucher de résilience|`@Compendium[pf2e.feats-srd.tCuMXQ0yMrCNwzqW]`|
|[Resist Ruin](feats/ancestry-13-FqZKSSBU7M4zhsXM.htm)|Résister à la ruine|`@Compendium[pf2e.feats-srd.FqZKSSBU7M4zhsXM]`|
|[Resounding Blow](feats/class-10-9kY9B5WgtEleOicn.htm)|Coup retentissant|`@Compendium[pf2e.feats-srd.9kY9B5WgtEleOicn]`|
|[Resounding Bravery](feats/class-08-dTO1ShJovbzrKUY4.htm)|Bravoure retentissante|`@Compendium[pf2e.feats-srd.dTO1ShJovbzrKUY4]`|
|[Resounding Finale](feats/class-16-lhSqWHXK1JShUabF.htm)|Final retentissant|`@Compendium[pf2e.feats-srd.lhSqWHXK1JShUabF]`|
|[Resourceful Ritualist](feats/archetype-06-dAckQkpg1qyTz8od.htm)|Ritualiste plein de ressources|`@Compendium[pf2e.feats-srd.dAckQkpg1qyTz8od]`|
|[Resurrectionist](feats/class-16-Dr6h8WRW6xnLRfxr.htm)|Résurrecteur|`@Compendium[pf2e.feats-srd.Dr6h8WRW6xnLRfxr]`|
|[Resuscitate](feats/archetype-16-lleedxE6fTDSK6og.htm)|Ressusciter|`@Compendium[pf2e.feats-srd.lleedxE6fTDSK6og]`|
|[Retain Absorbed Spell](feats/archetype-18-S3hN7qK7aiCDTrpM.htm)|Maintien de sort absorbé|`@Compendium[pf2e.feats-srd.S3hN7qK7aiCDTrpM]`|
|[Return Fire](feats/class-06-OINfbwNZGnlyMqPR.htm)|Tir retourné|`@Compendium[pf2e.feats-srd.OINfbwNZGnlyMqPR]`|
|[Returning Throw](feats/ancestry-09-HfebybiUNW8mXOfP.htm)|Lancer retourné|`@Compendium[pf2e.feats-srd.HfebybiUNW8mXOfP]`|
|[Reveal Beasts](feats/bonus-00-PPSH5vdf90KC95jJ.htm)||`@Compendium[pf2e.feats-srd.PPSH5vdf90KC95jJ]`|
|[Reveal Machinations](feats/skill-15-XZcd1wFHy111klu2.htm)|Révéler la machination|`@Compendium[pf2e.feats-srd.XZcd1wFHy111klu2]`|
|[Revealing Stab](feats/class-06-JHcvySfCM9uYNb9N.htm)|Perforation révélatrice|`@Compendium[pf2e.feats-srd.JHcvySfCM9uYNb9N]`|
|[Reverberate](feats/class-12-ecV3Nljvs4FOBS27.htm)|Entrer en résonance|`@Compendium[pf2e.feats-srd.ecV3Nljvs4FOBS27]`|
|[Reverse Engineering](feats/archetype-04-f0s3WwaJN5f2UTYY.htm)|Déduction de formule|`@Compendium[pf2e.feats-srd.f0s3WwaJN5f2UTYY]`|
|[Revivifying Mutagen](feats/class-02-2rJP5TqvfazNeNmY.htm)|Mutagène revivifiant|`@Compendium[pf2e.feats-srd.2rJP5TqvfazNeNmY]`|
|[Ricochet Feint](feats/class-12-VIjI8PtkTFjeAA6a.htm)|Feinte de ricochet|`@Compendium[pf2e.feats-srd.VIjI8PtkTFjeAA6a]`|
|[Ricochet Stance (Fighter)](feats/class-06-tRHjUCl0xqG97nok.htm)|Posture du ricochet (Guerrier)|`@Compendium[pf2e.feats-srd.tRHjUCl0xqG97nok]`|
|[Ricochet Stance (Rogue)](feats/class-08-RsNvCSrCN7czHC0G.htm)|Posture du ricochet (Roublard)|`@Compendium[pf2e.feats-srd.RsNvCSrCN7czHC0G]`|
|[Ride](feats/general-01-Rq5wkA8DtsmbzoGV.htm)|Chevaucher|`@Compendium[pf2e.feats-srd.Rq5wkA8DtsmbzoGV]`|
|[Righteous Resistance](feats/bonus-00-Kz4E8heU12IGcYoi.htm)||`@Compendium[pf2e.feats-srd.Kz4E8heU12IGcYoi]`|
|[Risky Surgery](feats/skill-01-bkZgWFSFV4cAf5Ot.htm)|Chirurgie risquée|`@Compendium[pf2e.feats-srd.bkZgWFSFV4cAf5Ot]`|
|[Rites Of Convocation](feats/class-04-j01dM0ZAC7KzShx0.htm)|Rites de convocation|`@Compendium[pf2e.feats-srd.j01dM0ZAC7KzShx0]`|
|[Rites Of Transfiguration](feats/class-14-wa9ZGBTlFuwOjPpH.htm)|Rites de transfiguration|`@Compendium[pf2e.feats-srd.wa9ZGBTlFuwOjPpH]`|
|[Ritual Researcher](feats/class-04-DUuCOQ9FiZf7vS5b.htm)|Chercheur de rituels|`@Compendium[pf2e.feats-srd.DUuCOQ9FiZf7vS5b]`|
|[Ritual Reversion](feats/ancestry-05-kpn4R65YlD38iAIS.htm)|Rituel de réversibilité|`@Compendium[pf2e.feats-srd.kpn4R65YlD38iAIS]`|
|[Ritualist Dedication](feats/archetype-04-kU4K9jj9qnktoAaQ.htm)|Dévouement du Ritualiste|`@Compendium[pf2e.feats-srd.kU4K9jj9qnktoAaQ]`|
|[Robust Recovery](feats/skill-02-3ZerjLH8ls3JT6cD.htm)|Récupération vigoureuse|`@Compendium[pf2e.feats-srd.3ZerjLH8ls3JT6cD]`|
|[Rock Runner](feats/ancestry-01-NIUSBGMmdqhkYtmo.htm)|Arpenteur des roches|`@Compendium[pf2e.feats-srd.NIUSBGMmdqhkYtmo]`|
|[Rogue Dedication](feats/archetype-02-bCWieNDC1CD35tin.htm)|Dévouement du roublard|`@Compendium[pf2e.feats-srd.bCWieNDC1CD35tin]`|
|[Roll with It](feats/ancestry-09-ATiQDz27aiBTAt17.htm)|Roule avec çà|`@Compendium[pf2e.feats-srd.ATiQDz27aiBTAt17]`|
|[Root Magic](feats/skill-01-22P7IFyhrF7Fbw8B.htm)|Magie des racines|`@Compendium[pf2e.feats-srd.22P7IFyhrF7Fbw8B]`|
|[Rope Runner](feats/archetype-04-wpqKltAoJjRQgWow.htm)|Gabier éprouvé|`@Compendium[pf2e.feats-srd.wpqKltAoJjRQgWow]`|
|[Rough Rider](feats/ancestry-01-GF9kkULUYowgjEWM.htm)|Cavalier farouche|`@Compendium[pf2e.feats-srd.GF9kkULUYowgjEWM]`|
|[Round Ears](feats/ancestry-01-ecJms2jtd6cZ5rQK.htm)|Oreilles arrondies|`@Compendium[pf2e.feats-srd.ecJms2jtd6cZ5rQK]`|
|[Rubbery Skin](feats/archetype-10-wUHnauB3atxO1RIo.htm)|Peau caoutchouteuse|`@Compendium[pf2e.feats-srd.wUHnauB3atxO1RIo]`|
|[Rugged Survivalist](feats/archetype-12-m7AOg13xEJRHyoTt.htm)|Survivant robuste|`@Compendium[pf2e.feats-srd.m7AOg13xEJRHyoTt]`|
|[Runescarred Dedication](feats/archetype-02-luKAFJAvdbAgEwV7.htm)|Dévouement de scarifié des runes|`@Compendium[pf2e.feats-srd.luKAFJAvdbAgEwV7]`|
|[Running Reload](feats/class-04-meQJfsKVar9tm6c9.htm)|Rechargement en courant|`@Compendium[pf2e.feats-srd.meQJfsKVar9tm6c9]`|
|[Sabotage](feats/bonus-04-vDeG0N4kzTBDTC2b.htm)|Sabotage|`@Compendium[pf2e.feats-srd.vDeG0N4kzTBDTC2b]`|
|[Sacred Defender](feats/class-20-JaBDNtNYYDfTGYad.htm)|Défenseur sacré|`@Compendium[pf2e.feats-srd.JaBDNtNYYDfTGYad]`|
|[Sacred Defense](feats/skill-07-gHBdjbEnIK8clK8u.htm)|Défense sacrée|`@Compendium[pf2e.feats-srd.gHBdjbEnIK8clK8u]`|
|[Sacred Ki](feats/class-04-Ux0DSklFlIlcvnhO.htm)|Ki sacré|`@Compendium[pf2e.feats-srd.Ux0DSklFlIlcvnhO]`|
|[Sacrifice Armor](feats/class-08-bYijGvCvCmJnW6aA.htm)|Armure sacrifiée|`@Compendium[pf2e.feats-srd.bYijGvCvCmJnW6aA]`|
|[Safe House](feats/archetype-04-vKzAIJuyr9SU2JzU.htm)|Planque sûre|`@Compendium[pf2e.feats-srd.vKzAIJuyr9SU2JzU]`|
|[Safeguarded Spell](feats/class-08-6KiB0SLYB1p8Th5U.htm)|Protégé du sort|`@Compendium[pf2e.feats-srd.6KiB0SLYB1p8Th5U]`|
|[Sanctify Water](feats/skill-07-P9dVBWB8nYZt4AFA.htm)|Sanctifier l'eau|`@Compendium[pf2e.feats-srd.P9dVBWB8nYZt4AFA]`|
|[Saoc Astrology](feats/ancestry-01-csoGdGuWasEw3boD.htm)|Astrologie saoc|`@Compendium[pf2e.feats-srd.csoGdGuWasEw3boD]`|
|[Sap Life](feats/class-02-TiNlehXIDEnIl95M.htm)|Saper la vie|`@Compendium[pf2e.feats-srd.TiNlehXIDEnIl95M]`|
|[Savage Critical](feats/class-18-v88bFLoJEF3YfJKb.htm)|Critique violent|`@Compendium[pf2e.feats-srd.v88bFLoJEF3YfJKb]`|
|[Saving Slash](feats/archetype-10-F3ZBkDEWZ24NOR2j.htm)|Parade salvatrice|`@Compendium[pf2e.feats-srd.F3ZBkDEWZ24NOR2j]`|
|[Scalding Spit](feats/ancestry-09-ulQzdBOnZH9LQu8M.htm)|Crachat brûlant|`@Compendium[pf2e.feats-srd.ulQzdBOnZH9LQu8M]`|
|[Scales Of The Dragon](feats/archetype-04-fT37dtsByEIc3glC.htm)|Écailles de dragon|`@Compendium[pf2e.feats-srd.fT37dtsByEIc3glC]`|
|[Scalpel's Point](feats/class-04-hYu6XxARNJYdf8Qe.htm)|Pointe du scalpel|`@Compendium[pf2e.feats-srd.hYu6XxARNJYdf8Qe]`|
|[Scamper](feats/ancestry-01-xzpMQ2ZRn9zC23XG.htm)|Détaler|`@Compendium[pf2e.feats-srd.xzpMQ2ZRn9zC23XG]`|
|[Scamper Underfoot](feats/ancestry-01-viFTJfZusRPx0G2q.htm)|Détaler par dessous|`@Compendium[pf2e.feats-srd.viFTJfZusRPx0G2q]`|
|[Scare to Death](feats/skill-15-mZttsiWl1ql5NvrH.htm)|Mort de peur|`@Compendium[pf2e.feats-srd.mZttsiWl1ql5NvrH]`|
|[Scarecrow](feats/archetype-08-RivbJYEBUyfLwPh7.htm)|Épouvantail|`@Compendium[pf2e.feats-srd.RivbJYEBUyfLwPh7]`|
|[Scavenger's Search](feats/ancestry-01-zaGD9Og2p8Opa0oJ.htm)|Fouille du récupérateur|`@Compendium[pf2e.feats-srd.zaGD9Og2p8Opa0oJ]`|
|[Scholastic Identification](feats/archetype-07-g3zmkEVJJIjE32fY.htm)|Études de l'identification|`@Compendium[pf2e.feats-srd.g3zmkEVJJIjE32fY]`|
|[Schooled In Secrets](feats/skill-01-lEgYzFHransLkSvI.htm)|Éduqué dans les secrets|`@Compendium[pf2e.feats-srd.lEgYzFHransLkSvI]`|
|[Scintillating Spell](feats/class-16-DSmYJvCHMvZCP0aD.htm)|Sort scintillant|`@Compendium[pf2e.feats-srd.DSmYJvCHMvZCP0aD]`|
|[Scouring Rage](feats/class-06-VqVgcqmG6xmYuDbK.htm)|Rage débordante|`@Compendium[pf2e.feats-srd.VqVgcqmG6xmYuDbK]`|
|[Scout Dedication](feats/archetype-02-qMa2fIP2nqrFzHrq.htm)|Dévouement de l'Éclaireur|`@Compendium[pf2e.feats-srd.qMa2fIP2nqrFzHrq]`|
|[Scout's Charge](feats/archetype-04-6CE1nVGxt192AUGk.htm)|Assaut de l'Éclaireur|`@Compendium[pf2e.feats-srd.6CE1nVGxt192AUGk]`|
|[Scout's Pounce](feats/archetype-10-jvQoupE76OeUpjZp.htm)|Bond de l'Éclaireur|`@Compendium[pf2e.feats-srd.jvQoupE76OeUpjZp]`|
|[Scout's Speed](feats/archetype-06-elbj75qsUerbM725.htm)|Vitesse de l'Éclaireur|`@Compendium[pf2e.feats-srd.elbj75qsUerbM725]`|
|[Scout's Warning](feats/class-04-Tpcq3Lk7qEOZ3LDP.htm)|Avertissement de l'éclaireur|`@Compendium[pf2e.feats-srd.Tpcq3Lk7qEOZ3LDP]`|
|[Scroll Savant](feats/class-10-u5DBg0LrBUKP0JsJ.htm)|Virtuose des parchemins|`@Compendium[pf2e.feats-srd.u5DBg0LrBUKP0JsJ]`|
|[Scroll Trickster Dedication](feats/archetype-02-uxHWqFbYD0ZvkeF8.htm)|Dévouement d'Usurpateur de parchemins|`@Compendium[pf2e.feats-srd.uxHWqFbYD0ZvkeF8]`|
|[Scrollmaster Dedication](feats/class-06-uhfZtjbfJ8pZIWrF.htm)|Dévouement du Maître des parchemins|`@Compendium[pf2e.feats-srd.uhfZtjbfJ8pZIWrF]`|
|[Scrounger Dedication](feats/archetype-02-CaeCSWFWytWv8Fgu.htm)|Dévouement du Bricoleur|`@Compendium[pf2e.feats-srd.CaeCSWFWytWv8Fgu]`|
|[Seasoned](feats/skill-01-1Eceqc6zbMj2x0q9.htm)|Assaisonné|`@Compendium[pf2e.feats-srd.1Eceqc6zbMj2x0q9]`|
|[Second Ally](feats/class-08-VZczZNj3ozCj1Lzk.htm)|Deuxième allié|`@Compendium[pf2e.feats-srd.VZczZNj3ozCj1Lzk]`|
|[Second Chance Spell](feats/class-18-XafnXTx9Bn0kN1RG.htm)|Sort de la seconde chance|`@Compendium[pf2e.feats-srd.XafnXTx9Bn0kN1RG]`|
|[Second Shield](feats/archetype-06-g8ZMeg1YFg9WZj3I.htm)|Second bouclier|`@Compendium[pf2e.feats-srd.g8ZMeg1YFg9WZj3I]`|
|[Second Sting](feats/class-12-COe0bYyVCyC78rzP.htm)|Second dard|`@Compendium[pf2e.feats-srd.COe0bYyVCyC78rzP]`|
|[Second Wind](feats/class-02-4bvoePh1p3ZGgqhP.htm)|Second souffle|`@Compendium[pf2e.feats-srd.4bvoePh1p3ZGgqhP]`|
|[Secret Speech](feats/skill-01-wbjTkaKRygpaZS0r.htm)|Langage secret|`@Compendium[pf2e.feats-srd.wbjTkaKRygpaZS0r]`|
|[Seedpod](feats/ancestry-01-dWGa6cFSVrASTEfd.htm)|Cosse|`@Compendium[pf2e.feats-srd.dWGa6cFSVrASTEfd]`|
|[Seek Injustice](feats/bonus-00-1xpqOpguTUAeFsdO.htm)||`@Compendium[pf2e.feats-srd.1xpqOpguTUAeFsdO]`|
|[Seeker Arrow](feats/archetype-14-roItUHUbBqhHfwJr.htm)|Flèche chercheuse|`@Compendium[pf2e.feats-srd.roItUHUbBqhHfwJr]`|
|[Selective Energy](feats/class-06-yyt2I2lGbRndXjbc.htm)|Énergie sélective|`@Compendium[pf2e.feats-srd.yyt2I2lGbRndXjbc]`|
|[Selfless Parry](feats/archetype-08-zytTsipimVTmPc5U.htm)|Parade Désintéressée|`@Compendium[pf2e.feats-srd.zytTsipimVTmPc5U]`|
|[Sense Alignment](feats/archetype-04-iyXw5PnevT2jT8kU.htm)||`@Compendium[pf2e.feats-srd.iyXw5PnevT2jT8kU]`|
|[Sense Allies](feats/ancestry-05-m5JYglEObpWC3dhP.htm)|Perception des alliés|`@Compendium[pf2e.feats-srd.m5JYglEObpWC3dhP]`|
|[Sense Chaos](feats/archetype-08-3XFKXB3ffeIkrQYe.htm)|Perception du chaos|`@Compendium[pf2e.feats-srd.3XFKXB3ffeIkrQYe]`|
|[Sense Evil](feats/class-08-EnpbhZr94ZyZI4hb.htm)|Perception du mal|`@Compendium[pf2e.feats-srd.EnpbhZr94ZyZI4hb]`|
|[Sense Good](feats/class-08-bPqRneuJPqeXc65G.htm)|Perception du bien|`@Compendium[pf2e.feats-srd.bPqRneuJPqeXc65G]`|
|[Sense The Unseen](feats/class-14-DM9rjXZrmx2MFX7k.htm)|Percevoir l'inaperçu|`@Compendium[pf2e.feats-srd.DM9rjXZrmx2MFX7k]`|
|[Sense Thoughts](feats/ancestry-09-LRBzEzpS19z3Eghd.htm)|Perception des pensées|`@Compendium[pf2e.feats-srd.LRBzEzpS19z3Eghd]`|
|[Sentinel Dedication](feats/archetype-02-8foxmfC6FFT3oYpV.htm)|Dévouement de Sentinelle|`@Compendium[pf2e.feats-srd.8foxmfC6FFT3oYpV]`|
|[Settlement Scholastics](feats/skill-04-R7BO8br4BjCmpjit.htm)|Études d'une implantation|`@Compendium[pf2e.feats-srd.R7BO8br4BjCmpjit]`|
|[Shackles of Law](feats/bonus-00-AGiIjgLOFuahmwiT.htm)||`@Compendium[pf2e.feats-srd.AGiIjgLOFuahmwiT]`|
|[Shadow Hunter](feats/class-18-f1acyuIGYVp2BpKc.htm)|Chasseur de l'ombre|`@Compendium[pf2e.feats-srd.f1acyuIGYVp2BpKc]`|
|[Shadow Illusion](feats/archetype-12-C4m59yjuDmZLnTqu.htm)|Illusion de l'ombre|`@Compendium[pf2e.feats-srd.C4m59yjuDmZLnTqu]`|
|[Shadow Magic](feats/archetype-10-tha0L3Z6608JrUwN.htm)|Magie de l'ombre|`@Compendium[pf2e.feats-srd.tha0L3Z6608JrUwN]`|
|[Shadow Mark](feats/skill-02-N7IsnLDFt73r7x56.htm)|Marque d'ombre|`@Compendium[pf2e.feats-srd.N7IsnLDFt73r7x56]`|
|[Shadow Master](feats/archetype-18-BqcAwmNjDlKEI84X.htm)|Maître de l'ombre|`@Compendium[pf2e.feats-srd.BqcAwmNjDlKEI84X]`|
|[Shadow Pact](feats/ancestry-13-ACeqRSWr4CEwLZgO.htm)|Pacte d'ombre|`@Compendium[pf2e.feats-srd.ACeqRSWr4CEwLZgO]`|
|[Shadow Power](feats/archetype-14-F7Ao9p17ocf3JVvy.htm)|Pouvoir de l'ombre|`@Compendium[pf2e.feats-srd.F7Ao9p17ocf3JVvy]`|
|[Shadow Self](feats/ancestry-17-7YvOqcdp9Z0RALMp.htm)|Ombre de soi-même|`@Compendium[pf2e.feats-srd.7YvOqcdp9Z0RALMp]`|
|[Shadow Sneak Attack](feats/archetype-10-N16lctDPZpvk9Khq.htm)|Attaque sournoise de l'ombre|`@Compendium[pf2e.feats-srd.N16lctDPZpvk9Khq]`|
|[Shadow of the Wilds](feats/ancestry-01-kDklfrprKTuTpcEE.htm)|Ombre des étendues sauvages|`@Compendium[pf2e.feats-srd.kDklfrprKTuTpcEE]`|
|[Shadow's Web](feats/class-14-AfTMuAln2f0Pa3Lj.htm)|Toile de l'ombre|`@Compendium[pf2e.feats-srd.AfTMuAln2f0Pa3Lj]`|
|[Shadowdancer Dedication](feats/archetype-08-1AgirzUGkyDdmENy.htm)|Dévouement de Danseur de l'ombre|`@Compendium[pf2e.feats-srd.1AgirzUGkyDdmENy]`|
|[Shake it Off](feats/class-02-auv1lss6LxM0q3gz.htm)|Reprends-toi|`@Compendium[pf2e.feats-srd.auv1lss6LxM0q3gz]`|
|[Shall not Falter, Shall not Rout](feats/archetype-10-4AezsqaQRFtX024w.htm)|Tu ne faibliras pas, tu ne prendras pas la fuite|`@Compendium[pf2e.feats-srd.4AezsqaQRFtX024w]`|
|[Shameless Request](feats/skill-07-KIK2Eza9TK47MEb2.htm)|Requête sans vergogne|`@Compendium[pf2e.feats-srd.KIK2Eza9TK47MEb2]`|
|[Shape Of The Dragon](feats/archetype-14-eR0sifECG27CC4do.htm)|Forme de dragon|`@Compendium[pf2e.feats-srd.eR0sifECG27CC4do]`|
|[Shaped Contaminant](feats/class-12-9u0uW1vZThRayXk2.htm)|Contaminant formé|`@Compendium[pf2e.feats-srd.9u0uW1vZThRayXk2]`|
|[Share Rage](feats/class-08-vayNZR1bTzU1oUa3.htm)|Rage partagée|`@Compendium[pf2e.feats-srd.vayNZR1bTzU1oUa3]`|
|[Share Thoughts](feats/ancestry-01-G8WgbujrrnMQUQ8E.htm)|Partager les pensées|`@Compendium[pf2e.feats-srd.G8WgbujrrnMQUQ8E]`|
|[Shared Assault](feats/class-12-oUcB71V1jVaM8SFx.htm)|Assaut partagé|`@Compendium[pf2e.feats-srd.oUcB71V1jVaM8SFx]`|
|[Shared Avoidance](feats/class-10-wIJC00ODLq9WYc1m.htm)|Évitement partagé|`@Compendium[pf2e.feats-srd.wIJC00ODLq9WYc1m]`|
|[Shared Clarity](feats/class-18-ybrx1nsg5J0L8d3j.htm)|Clarté partagée|`@Compendium[pf2e.feats-srd.ybrx1nsg5J0L8d3j]`|
|[Shared Luck (Catfolk)](feats/ancestry-09-d022Gp8PjS4Q0ZAC.htm)|Chance partagée (Homme-félin)|`@Compendium[pf2e.feats-srd.d022Gp8PjS4Q0ZAC]`|
|[Shared Luck (Halfling)](feats/ancestry-05-f5Vkk2rM6tCe2zQn.htm)|Chance partagée|`@Compendium[pf2e.feats-srd.f5Vkk2rM6tCe2zQn]`|
|[Shared Prey](feats/class-14-Aqhsx5duEpBgaPB0.htm)|Proie commune|`@Compendium[pf2e.feats-srd.Aqhsx5duEpBgaPB0]`|
|[Shared Replenishment](feats/class-12-mTkbgFOHJUBl0Qwg.htm)|Récupération partagée|`@Compendium[pf2e.feats-srd.mTkbgFOHJUBl0Qwg]`|
|[Shared Stratagem](feats/class-02-N4wCUZH2KG6FoGqh.htm)|Stratagème partagé|`@Compendium[pf2e.feats-srd.N4wCUZH2KG6FoGqh]`|
|[Shared Synergy](feats/class-10-8NQaqtHheeMUNGYr.htm)|Synergie partagée|`@Compendium[pf2e.feats-srd.8NQaqtHheeMUNGYr]`|
|[Sharp Fangs](feats/ancestry-01-6oJ1EdANHgYCfCeF.htm)|Crocs aiguisés|`@Compendium[pf2e.feats-srd.6oJ1EdANHgYCfCeF]`|
|[Shatter Defenses](feats/class-06-zbxqYhmn7KbqR2Sb.htm)|Briser les défenses|`@Compendium[pf2e.feats-srd.zbxqYhmn7KbqR2Sb]`|
|[Shattering Blows](feats/class-16-RL7faGkymMFLAqTU.htm)|Coups fracassants|`@Compendium[pf2e.feats-srd.RL7faGkymMFLAqTU]`|
|[Shattering Strike (Monk)](feats/class-16-E4xubBMtj81kX5Bk.htm)|Frappe fracassante|`@Compendium[pf2e.feats-srd.E4xubBMtj81kX5Bk]`|
|[Shattering Strike (Weapon Improviser)](feats/archetype-10-B7VMXObJSNVI0ZGJ.htm)|Frappe fracassante (Improvisateur d'arme)|`@Compendium[pf2e.feats-srd.B7VMXObJSNVI0ZGJ]`|
|[Shed Tail](feats/ancestry-05-t3IzY8uSyFj3aGmh.htm)|Autotomie caudale|`@Compendium[pf2e.feats-srd.t3IzY8uSyFj3aGmh]`|
|[Sheltering Slab](feats/ancestry-05-sGSf5BdopT0zWOWs.htm)|Bloc protecteur|`@Compendium[pf2e.feats-srd.sGSf5BdopT0zWOWs]`|
|[Shield Block](feats/general-01-jM72TjJ965jocBV8.htm)|Blocage au bouclier|`@Compendium[pf2e.feats-srd.jM72TjJ965jocBV8]`|
|[Shield Of Faith](feats/class-10-w3qyriA1YnzXaas3.htm)|Bouclier de la foi|`@Compendium[pf2e.feats-srd.w3qyriA1YnzXaas3]`|
|[Shield Paragon](feats/class-20-P9swngiLXbhMegQ8.htm)|Paragon du bouclier|`@Compendium[pf2e.feats-srd.P9swngiLXbhMegQ8]`|
|[Shield Salvation](feats/archetype-12-4bB3N36DmqllGJNx.htm)|Sauvetage du bouclier|`@Compendium[pf2e.feats-srd.4bB3N36DmqllGJNx]`|
|[Shield Warden](feats/class-06-4p2OvcH21ruk55Tj.htm)|Gardien au bouclier|`@Compendium[pf2e.feats-srd.4p2OvcH21ruk55Tj]`|
|[Shield of Grace](feats/class-16-7IDFHh2ZJLaB1y59.htm)|Bouclier de grâce|`@Compendium[pf2e.feats-srd.7IDFHh2ZJLaB1y59]`|
|[Shield of Reckoning](feats/class-10-VsTmB32x9673ONJ0.htm)|Bouclier du jugement|`@Compendium[pf2e.feats-srd.VsTmB32x9673ONJ0]`|
|[Shielded Stride](feats/class-04-tHqlcgcxHXzqLHPZ.htm)|Marche au bouclier|`@Compendium[pf2e.feats-srd.tHqlcgcxHXzqLHPZ]`|
|[Shining Oath](feats/class-02-vQiUYpdgYnXg0oDX.htm)|Serment de lumière|`@Compendium[pf2e.feats-srd.vQiUYpdgYnXg0oDX]`|
|[Shoony Lore](feats/ancestry-01-yCaWcKlpbAfebqlO.htm)|Connaissance des Shoonis|`@Compendium[pf2e.feats-srd.yCaWcKlpbAfebqlO]`|
|[Shooting Stars Stance](feats/class-02-6cQSPqXoAO6oJl0i.htm)|Posture des étoiles lancées|`@Compendium[pf2e.feats-srd.6cQSPqXoAO6oJl0i]`|
|[Shory Aerialist](feats/ancestry-13-plhQDES7yb6xDAXL.htm)|Aérialiste Rivain|`@Compendium[pf2e.feats-srd.plhQDES7yb6xDAXL]`|
|[Shory Aeromancer](feats/ancestry-09-Eoy0zhpf8tYrTHN4.htm)|Aéromancien Rivain|`@Compendium[pf2e.feats-srd.Eoy0zhpf8tYrTHN4]`|
|[Shoulder Catastrophe](feats/archetype-12-Oa41bfBRO36lf1aE.htm)|Endosser la catastrophe|`@Compendium[pf2e.feats-srd.Oa41bfBRO36lf1aE]`|
|[Shove Down](feats/class-06-z5fUX9jeqfAViOd8.htm)|Envoyer au tapis|`@Compendium[pf2e.feats-srd.z5fUX9jeqfAViOd8]`|
|[Shoving Sweep](feats/archetype-08-7p2tNqYHsg6u05cU.htm)|Balayage repoussant|`@Compendium[pf2e.feats-srd.7p2tNqYHsg6u05cU]`|
|[Side by Side (Druid)](feats/class-10-j5Xjr8vZuBhCixIr.htm)|Côte à côte (Druide)|`@Compendium[pf2e.feats-srd.j5Xjr8vZuBhCixIr]`|
|[Side by Side (Ranger)](feats/class-12-5YcnoTYKvEtkWiHh.htm)|Côte à côte (Rôdeur)|`@Compendium[pf2e.feats-srd.5YcnoTYKvEtkWiHh]`|
|[Sidestep](feats/class-08-5d4AyZ0Y6Ht1OwIa.htm)|Pas de côté|`@Compendium[pf2e.feats-srd.5d4AyZ0Y6Ht1OwIa]`|
|[Sign Language](feats/skill-01-aAoFc10cOpxGypOY.htm)|Langue des signes|`@Compendium[pf2e.feats-srd.aAoFc10cOpxGypOY]`|
|[Signature Spell Expansion](feats/class-10-UjYHf7rlWTFJ0v0A.htm)|Expansion des sorts emblématiques|`@Compendium[pf2e.feats-srd.UjYHf7rlWTFJ0v0A]`|
|[Signifer Armor Expertise](feats/archetype-12-wDo5dsSmyJqfmPgj.htm)|Expertise de l'armure du Signifer|`@Compendium[pf2e.feats-srd.wDo5dsSmyJqfmPgj]`|
|[Signifer's Sight](feats/archetype-10-TyjW9VGtlH0Zkm0I.htm)|Vision du Signifer|`@Compendium[pf2e.feats-srd.TyjW9VGtlH0Zkm0I]`|
|[Silence Heresy](feats/bonus-00-tPhhaCbaQqwenkzx.htm)||`@Compendium[pf2e.feats-srd.tPhhaCbaQqwenkzx]`|
|[Silencing Strike](feats/class-10-Yk3QGpalWDn5MhBV.htm)|Frappe muselante|`@Compendium[pf2e.feats-srd.Yk3QGpalWDn5MhBV]`|
|[Silent Spell](feats/class-04-JlGZFW3mCNxWPKvX.htm)|Sort silencieux|`@Compendium[pf2e.feats-srd.JlGZFW3mCNxWPKvX]`|
|[Silver's Refrain](feats/class-12-mZcI1NKtQhteAQLn.htm)|Refrain d'argent|`@Compendium[pf2e.feats-srd.mZcI1NKtQhteAQLn]`|
|[Simple Crystal Magic](feats/archetype-06-DpmdmRNMg6LZpNB0.htm)|Magie des cristaux basique|`@Compendium[pf2e.feats-srd.DpmdmRNMg6LZpNB0]`|
|[Siphon Power](feats/class-16-L0f3c0DkT7FLQF9W.htm)|Pouvoir siphonné|`@Compendium[pf2e.feats-srd.L0f3c0DkT7FLQF9W]`|
|[Skill Mastery (Investigator)](feats/archetype-08-v4O6eDiSOkzQZHmT.htm)|Maîtrise des compétences (Enquêteur)|`@Compendium[pf2e.feats-srd.v4O6eDiSOkzQZHmT]`|
|[Skill Mastery (Rogue)](feats/class-08-c9rhGmKft1BVT4JO.htm)|Maîtrise des compétences (Roublard)|`@Compendium[pf2e.feats-srd.c9rhGmKft1BVT4JO]`|
|[Skill Training](feats/skill-01-P6icK2DbRoZ3H6kc.htm)|Compétence qualifiante|`@Compendium[pf2e.feats-srd.P6icK2DbRoZ3H6kc]`|
|[Skillful Tail](feats/ancestry-05-aVRuchEAJIvnd70k.htm)|Queue agile|`@Compendium[pf2e.feats-srd.aVRuchEAJIvnd70k]`|
|[Skim Scroll](feats/archetype-08-HIyuVIh2XSDz3h2j.htm)|Parchemin parcouru|`@Compendium[pf2e.feats-srd.HIyuVIh2XSDz3h2j]`|
|[Skirmish Strike](feats/class-06-4I1Kq53Qfzrrmg2E.htm)|Frappe d'escarmouche|`@Compendium[pf2e.feats-srd.4I1Kq53Qfzrrmg2E]`|
|[Skitter](feats/general-03-wPHZhgKzNw4VcCFt.htm)|Glisseur|`@Compendium[pf2e.feats-srd.wPHZhgKzNw4VcCFt]`|
|[Skittering Scuttle](feats/ancestry-09-dtmLxbSy2H8h8e4N.htm)|Précipitation hâtive|`@Compendium[pf2e.feats-srd.dtmLxbSy2H8h8e4N]`|
|[Sky and Heaven Stance](feats/archetype-06-UjEeHamC2C8JfgJz.htm)|Posture du Ciel et du Paradis|`@Compendium[pf2e.feats-srd.UjEeHamC2C8JfgJz]`|
|[Skyseeker](feats/archetype-08-gepQGtV8Ftr0JJ6O.htm)|Cherche-ciel|`@Compendium[pf2e.feats-srd.gepQGtV8Ftr0JJ6O]`|
|[Slag May](feats/ancestry-01-xTe8lNBp76jsrhYh.htm)|Vierge crasseuse|`@Compendium[pf2e.feats-srd.xTe8lNBp76jsrhYh]`|
|[Sleeper Hold](feats/class-10-E1WXnYE2QwhHQxQb.htm)|Prise du sommeil|`@Compendium[pf2e.feats-srd.E1WXnYE2QwhHQxQb]`|
|[Slice and Swipe](feats/class-06-cTQMtd2IVlvgJwAn.htm)|Couper et dérober|`@Compendium[pf2e.feats-srd.cTQMtd2IVlvgJwAn]`|
|[Slippery Secrets](feats/skill-07-RiuZT3H4QZIIEQXJ.htm)|Secrets insaisissables|`@Compendium[pf2e.feats-srd.RiuZT3H4QZIIEQXJ]`|
|[Sly Striker](feats/class-08-AbgHCPWOKULeXrJ2.htm)|Frappe perfide|`@Compendium[pf2e.feats-srd.AbgHCPWOKULeXrJ2]`|
|[Smash From The Air](feats/class-18-7ATVpDUM6pRq6HOR.htm)|Fracasser en l'air|`@Compendium[pf2e.feats-srd.7ATVpDUM6pRq6HOR]`|
|[Smite Evil](feats/class-06-xgvKXeTxns0gIdAn.htm)|Châtiment du mal|`@Compendium[pf2e.feats-srd.xgvKXeTxns0gIdAn]`|
|[Smite Good](feats/class-06-T5xFirAE8VLL5Lbu.htm)|Châtiment du bien|`@Compendium[pf2e.feats-srd.T5xFirAE8VLL5Lbu]`|
|[Smoke Bomb](feats/class-02-ODnXQHvFoK7tIVpu.htm)|Bombe fumigène|`@Compendium[pf2e.feats-srd.ODnXQHvFoK7tIVpu]`|
|[Snagging Strike](feats/class-01-mWCiu9Hl1WxajSLa.htm)|Frappe déconcertante|`@Compendium[pf2e.feats-srd.mWCiu9Hl1WxajSLa]`|
|[Snap Out Of It! (Marshal)](feats/archetype-04-zbnL5OP4zVaNFcq8.htm)|Secoue-toi ! (Capitaine)|`@Compendium[pf2e.feats-srd.zbnL5OP4zVaNFcq8]`|
|[Snap Out Of It! (Pathfinder Agent)](feats/archetype-08-n3vpCWPjXAInRTyR.htm)|Secoue-toi ! (Agent des Éclaireurs)|`@Compendium[pf2e.feats-srd.n3vpCWPjXAInRTyR]`|
|[Snap Shot](feats/class-06-0UdHPOv3DX8TY9yb.htm)|Tir soudain|`@Compendium[pf2e.feats-srd.0UdHPOv3DX8TY9yb]`|
|[Snare Crafting](feats/skill-01-DMetdzt1VJL2Y62i.htm)|Fabrication de pièges artisanaux|`@Compendium[pf2e.feats-srd.DMetdzt1VJL2Y62i]`|
|[Snare Genius](feats/ancestry-05-8DIzXO1YpsU3DpJw.htm)|Génie des pièges artisanaux|`@Compendium[pf2e.feats-srd.8DIzXO1YpsU3DpJw]`|
|[Snare Hopping](feats/class-02-lIrPwGpJk9TldZ4c.htm)|Piège artisanal bondissant|`@Compendium[pf2e.feats-srd.lIrPwGpJk9TldZ4c]`|
|[Snare Setter](feats/ancestry-01-eMmdBpbMrpIuGowo.htm)|Poseur de pièges artisanaux|`@Compendium[pf2e.feats-srd.eMmdBpbMrpIuGowo]`|
|[Snare Specialist](feats/class-04-0haS0qXR9xTYKoTG.htm)|Spécialiste des pièges artisanaux|`@Compendium[pf2e.feats-srd.0haS0qXR9xTYKoTG]`|
|[Snarecrafter Dedication](feats/archetype-02-4MUbwilvb9dI0X59.htm)|Dévouement de Fabricant de pièges artisanaux|`@Compendium[pf2e.feats-srd.4MUbwilvb9dI0X59]`|
|[Sneak Attacker](feats/archetype-04-bCAvo59b5RyW12iI.htm)|Agresseur furtif|`@Compendium[pf2e.feats-srd.bCAvo59b5RyW12iI]`|
|[Sneak Savant](feats/class-10-2Nu4ZdKQM8hx8x5D.htm)|Science de la Furtivité|`@Compendium[pf2e.feats-srd.2Nu4ZdKQM8hx8x5D]`|
|[Soaring Flight](feats/ancestry-09-Tl8yckXeTCHnwrlM.htm)|Voler haut|`@Compendium[pf2e.feats-srd.Tl8yckXeTCHnwrlM]`|
|[Soaring Shape](feats/class-08-wNHUryoRzlfDCFAd.htm)|Morphologie volante|`@Compendium[pf2e.feats-srd.wNHUryoRzlfDCFAd]`|
|[Sociable](feats/ancestry-01-tn7K5lbnF87rZ659.htm)|Sociable|`@Compendium[pf2e.feats-srd.tn7K5lbnF87rZ659]`|
|[Social Purview](feats/archetype-04-rNPeOwFZE5Ma18JJ.htm)|Identité notoire|`@Compendium[pf2e.feats-srd.rNPeOwFZE5Ma18JJ]`|
|[Sodbuster](feats/ancestry-09-K9ixUl7PrNbBHGdA.htm)|Briseur de motte|`@Compendium[pf2e.feats-srd.K9ixUl7PrNbBHGdA]`|
|[Solar Rejuvenation](feats/ancestry-09-velPTcpjLXPnaYrm.htm)|Rajeunissement solaire|`@Compendium[pf2e.feats-srd.velPTcpjLXPnaYrm]`|
|[Solid Lead](feats/class-02-WZWSaAwuDgne7Z0c.htm)|Piste solide|`@Compendium[pf2e.feats-srd.WZWSaAwuDgne7Z0c]`|
|[Song Of Marching](feats/class-06-0zSoSPwC4cpqRewj.htm)|Chanson de marche|`@Compendium[pf2e.feats-srd.0zSoSPwC4cpqRewj]`|
|[Song Of Strength](feats/class-02-FkN9QX1W2Iv56bkn.htm)|Chanson de force|`@Compendium[pf2e.feats-srd.FkN9QX1W2Iv56bkn]`|
|[Song of the Fallen](feats/class-20-4E4121lbfWgxui4y.htm)|Chanson des morts au combat|`@Compendium[pf2e.feats-srd.4E4121lbfWgxui4y]`|
|[Soothing Ballad](feats/class-14-LmdOWCDffhBiyzM3.htm)|Ballade apaisante|`@Compendium[pf2e.feats-srd.LmdOWCDffhBiyzM3]`|
|[Soothing Mist](feats/class-04-LJw5tRrX0dMnm9Vq.htm)|Brume apaisante|`@Compendium[pf2e.feats-srd.LJw5tRrX0dMnm9Vq]`|
|[Sorcerer Dedication](feats/archetype-02-5UQagWB13Z8xR5Z6.htm)|Dévouement de l'ensorceleur|`@Compendium[pf2e.feats-srd.5UQagWB13Z8xR5Z6]`|
|[Soulsight](feats/class-08-HdcGtwWGUx5hQ4Jl.htm)|Vision de l'âme|`@Compendium[pf2e.feats-srd.HdcGtwWGUx5hQ4Jl]`|
|[Sow Rumor](feats/skill-02-sLEawQueTV1wGn0B.htm)|Semer des rumeurs|`@Compendium[pf2e.feats-srd.sLEawQueTV1wGn0B]`|
|[Sow Spell](feats/class-14-TyWFsX9DliAdAVs8.htm)|Sort semé|`@Compendium[pf2e.feats-srd.TyWFsX9DliAdAVs8]`|
|[Speak with Kindred](feats/ancestry-05-KYKK1vLqGIxXH5Tu.htm)|Parler avec ses semblables|`@Compendium[pf2e.feats-srd.KYKK1vLqGIxXH5Tu]`|
|[Speaking Sky](feats/archetype-12-bTNgHDqzfoqOLWu3.htm)|Ciel parlant|`@Compendium[pf2e.feats-srd.bTNgHDqzfoqOLWu3]`|
|[Specialized Beastmaster Companion](feats/archetype-14-bki36RiEM5FR4aiT.htm)|Compagnon spécialisé (Maître des bêtes)|`@Compendium[pf2e.feats-srd.bki36RiEM5FR4aiT]`|
|[Specialized Companion (Animal Trainer)](feats/archetype-18-hdt3RHZljLrO49kq.htm)|Compagnon spécialisé (Dompteur)|`@Compendium[pf2e.feats-srd.hdt3RHZljLrO49kq]`|
|[Specialized Companion (Druid)](feats/class-14-3r5rg0BCqSh5RBNS.htm)|Compagnon spécialisé (Druide)|`@Compendium[pf2e.feats-srd.3r5rg0BCqSh5RBNS]`|
|[Specialized Companion (Ranger)](feats/class-16-Fai5VMyrtOrYC5JL.htm)|Compagnon spécialisé (Rodeur)|`@Compendium[pf2e.feats-srd.Fai5VMyrtOrYC5JL]`|
|[Specialized Mount](feats/archetype-14-wijzB1FDUT7SC86a.htm)|Monture spécialisée|`@Compendium[pf2e.feats-srd.wijzB1FDUT7SC86a]`|
|[Specialty Crafting (Alchemy)](feats/skill-01-XVnpntqV6w94PoOb.htm)|Artisanat spécialisé (Alchimie)|`@Compendium[pf2e.feats-srd.XVnpntqV6w94PoOb]`|
|[Specialty Crafting (Artistry)](feats/skill-01-22HUfCajd6IkATOR.htm)|Artisanat spécialisé (Sens artistique)|`@Compendium[pf2e.feats-srd.22HUfCajd6IkATOR]`|
|[Specialty Crafting (Blacksmithing)](feats/skill-01-S8mPrjJjuObotmjZ.htm)|Artisanat spécialisé (Forge)|`@Compendium[pf2e.feats-srd.S8mPrjJjuObotmjZ]`|
|[Specialty Crafting (Bookmaking)](feats/skill-01-59FN13nU6agKT4oa.htm)|Artisanat spécialisé (Confection de livres)|`@Compendium[pf2e.feats-srd.59FN13nU6agKT4oa]`|
|[Specialty Crafting (Glassmaking)](feats/skill-01-YmzLnj4ojlf9INNP.htm)|Artisanat spécialisé (Verrerie)|`@Compendium[pf2e.feats-srd.YmzLnj4ojlf9INNP]`|
|[Specialty Crafting (Leatherworking)](feats/skill-01-WddEyALYHfpeUjZB.htm)|Artisanat spécialisé (Maroquinerie)|`@Compendium[pf2e.feats-srd.WddEyALYHfpeUjZB]`|
|[Specialty Crafting (Pottery)](feats/skill-01-8XC2xJ7Y1xlr6Xor.htm)|Artisanat spécialisé (Poterie)|`@Compendium[pf2e.feats-srd.8XC2xJ7Y1xlr6Xor]`|
|[Specialty Crafting (Shipbuilding)](feats/skill-01-tbIMWrIRI9P9cubM.htm)|Artisanat spécialisé (Construction navale)|`@Compendium[pf2e.feats-srd.tbIMWrIRI9P9cubM]`|
|[Specialty Crafting (Stonemasonry)](feats/skill-01-TWkwy0uQvTGlgEFG.htm)|Artisanat spécialisé (Taille de pierre)|`@Compendium[pf2e.feats-srd.TWkwy0uQvTGlgEFG]`|
|[Specialty Crafting (Tailoring)](feats/skill-01-B06QKNvKYqxYGwPU.htm)|Artisanat spécialisé (Couture)|`@Compendium[pf2e.feats-srd.B06QKNvKYqxYGwPU]`|
|[Specialty Crafting (Weaving)](feats/skill-01-dTjzxCy9pku5gL0F.htm)|Artisanat spécialisé (Tissage)|`@Compendium[pf2e.feats-srd.dTjzxCy9pku5gL0F]`|
|[Specialty Crafting (Woodworking)](feats/skill-01-YvoiGWZhtbHH6NMF.htm)|Artisanat spécialisé (Ébénisterie)|`@Compendium[pf2e.feats-srd.YvoiGWZhtbHH6NMF]`|
|[Speedy Rituals](feats/archetype-14-TQCpXi1hwYX6VIhp.htm)|Rituels rapides|`@Compendium[pf2e.feats-srd.TQCpXi1hwYX6VIhp]`|
|[Spell Combination](feats/class-20-dJ1ZviNMpt4ID7lc.htm)|Combinaison de sorts|`@Compendium[pf2e.feats-srd.dJ1ZviNMpt4ID7lc]`|
|[Spell Devourer](feats/ancestry-13-pedHIDAVLFzzjGO1.htm)|Dévoreur de sort|`@Compendium[pf2e.feats-srd.pedHIDAVLFzzjGO1]`|
|[Spell Mastery](feats/class-20-xhiwito5kneP4sjV.htm)|Maîtrise des sorts|`@Compendium[pf2e.feats-srd.xhiwito5kneP4sjV]`|
|[Spell Penetration](feats/class-06-52cygjzHfSD0YhEA.htm)|Efficacité des sorts accrue|`@Compendium[pf2e.feats-srd.52cygjzHfSD0YhEA]`|
|[Spell Relay](feats/class-06-zwEaXGKqnlBTllfE.htm)|Relais de sort|`@Compendium[pf2e.feats-srd.zwEaXGKqnlBTllfE]`|
|[Spell Runes](feats/archetype-04-FItD6HmjasjbLdgS.htm)|Rune de sort|`@Compendium[pf2e.feats-srd.FItD6HmjasjbLdgS]`|
|[Spell Shroud](feats/class-14-DT1O80hPD7MX6oWp.htm)|Suaire de sort|`@Compendium[pf2e.feats-srd.DT1O80hPD7MX6oWp]`|
|[Spell Tinker](feats/class-16-1re3J4hWW7raXIRB.htm)|Bricolage de sort|`@Compendium[pf2e.feats-srd.1re3J4hWW7raXIRB]`|
|[Spellbook Prodigy](feats/class-01-FCzfh8QHMo7QJpAM.htm)|Prodige du grimoire|`@Compendium[pf2e.feats-srd.FCzfh8QHMo7QJpAM]`|
|[Spellmaster Dedication](feats/class-06-vuApM8xHOZs4o6oS.htm)|Dévouement du Maître des sorts|`@Compendium[pf2e.feats-srd.vuApM8xHOZs4o6oS]`|
|[Spellmaster's Resilience](feats/archetype-12-dWkf6LhYBfBkeyOA.htm)|Résilience du Maître des sorts|`@Compendium[pf2e.feats-srd.dWkf6LhYBfBkeyOA]`|
|[Spellmaster's Tenacity](feats/archetype-14-lPoP5TFfq266kR6g.htm)|Tenacité du maître des sorts|`@Compendium[pf2e.feats-srd.lPoP5TFfq266kR6g]`|
|[Spellmaster's Ward](feats/archetype-10-YdGHQjhUrNvP18AA.htm)|Protection du maître des sorts|`@Compendium[pf2e.feats-srd.YdGHQjhUrNvP18AA]`|
|[Spirit Soother](feats/ancestry-05-2RmsQrLySNYQ4uIn.htm)|Pacificateur d'esprit|`@Compendium[pf2e.feats-srd.2RmsQrLySNYQ4uIn]`|
|[Spirit Spells](feats/archetype-04-Daap4ugeDZQWoPCx.htm)|Sorts spirituels|`@Compendium[pf2e.feats-srd.Daap4ugeDZQWoPCx]`|
|[Spirit Strikes](feats/ancestry-09-S7z1LbnSRlBep8rO.htm)|Frappes spirituelles|`@Compendium[pf2e.feats-srd.S7z1LbnSRlBep8rO]`|
|[Spirit's Wrath](feats/class-12-gO729iC9b5ypes2K.htm)|Colère de l'esprit|`@Compendium[pf2e.feats-srd.gO729iC9b5ypes2K]`|
|[Spirits' Interference](feats/class-06-hjApw8AvYVuqQk2W.htm)|Interférence spirituelle|`@Compendium[pf2e.feats-srd.hjApw8AvYVuqQk2W]`|
|[Spiritual Disruption](feats/bonus-00-FlsAYAGEiZg1gg7D.htm)||`@Compendium[pf2e.feats-srd.FlsAYAGEiZg1gg7D]`|
|[Spiritual Explorer](feats/archetype-04-pfBVx5xBfwKd1iVL.htm)|Explorateur spirituel|`@Compendium[pf2e.feats-srd.pfBVx5xBfwKd1iVL]`|
|[Spiritual Guides](feats/class-04-cEu8BUS41dlPyPGW.htm)|Guides spirituels|`@Compendium[pf2e.feats-srd.cEu8BUS41dlPyPGW]`|
|[Spiritual Sense](feats/class-06-HB0jvWCdim1p91q1.htm)|Sens spirituel|`@Compendium[pf2e.feats-srd.HB0jvWCdim1p91q1]`|
|[Splendid Companion](feats/archetype-12-AvN95M5eVLLEu2qk.htm)|Formidable compagnon|`@Compendium[pf2e.feats-srd.AvN95M5eVLLEu2qk]`|
|[Splinter Faith](feats/class-01-dGFQvkDRmyvvf4IQ.htm)|Foi schismatique|`@Compendium[pf2e.feats-srd.dGFQvkDRmyvvf4IQ]`|
|[Split Hex](feats/class-18-KpF7RenGBXIMMGPX.htm)|Maléfice divisé|`@Compendium[pf2e.feats-srd.KpF7RenGBXIMMGPX]`|
|[Split Shot](feats/class-04-DS0XlHfi3ztb3ET7.htm)|Tir divisé|`@Compendium[pf2e.feats-srd.DS0XlHfi3ztb3ET7]`|
|[Split Slot](feats/class-06-WAx7RABHDvVVcRI8.htm)|Emplacement divisé|`@Compendium[pf2e.feats-srd.WAx7RABHDvVVcRI8]`|
|[Spot Translate](feats/archetype-04-jsXDbQAAH3yMchPU.htm)|Traduction simultanée|`@Compendium[pf2e.feats-srd.jsXDbQAAH3yMchPU]`|
|[Spring Attack](feats/class-12-d1jQ0HyIOyUdCCaN.htm)|Attaque éclair|`@Compendium[pf2e.feats-srd.d1jQ0HyIOyUdCCaN]`|
|[Spring from the Shadows](feats/class-12-emjWa77ltL5FytvA.htm)|Bondir depuis les ombres|`@Compendium[pf2e.feats-srd.emjWa77ltL5FytvA]`|
|[Springing Leaper](feats/ancestry-05-3HuiLoQuJKLAh5rV.htm)|Sauteur bondissant|`@Compendium[pf2e.feats-srd.3HuiLoQuJKLAh5rV]`|
|[Spy's Countermeasures](feats/archetype-10-2xxFg9yRuCDpME3z.htm)|Contre-mesures d'espion|`@Compendium[pf2e.feats-srd.2xxFg9yRuCDpME3z]`|
|[Squawk!](feats/ancestry-01-CCmiEmS7ZgyQUfhn.htm)|Croa !|`@Compendium[pf2e.feats-srd.CCmiEmS7ZgyQUfhn]`|
|[Staff Acrobat Dedication](feats/archetype-02-LoeoiYOpxSaEkWKv.htm)|Dévouement du Funambule|`@Compendium[pf2e.feats-srd.LoeoiYOpxSaEkWKv]`|
|[Staff Sweep](feats/archetype-06-dPmJ91qawZW2U8K3.htm)|Balayage au bâton|`@Compendium[pf2e.feats-srd.dPmJ91qawZW2U8K3]`|
|[Stage Fighting](feats/archetype-04-1wTXeBrYU6BVEEOn.htm)|Combat chorégraphié|`@Compendium[pf2e.feats-srd.1wTXeBrYU6BVEEOn]`|
|[Stance Savant (Fighter)](feats/class-14-yeSyGnYDkl2GUNmu.htm)|Maître des postures|`@Compendium[pf2e.feats-srd.yeSyGnYDkl2GUNmu]`|
|[Stance Savant (Monk)](feats/class-12-LI9VtCaL5ZRk0Wo8.htm)|Maître des postures|`@Compendium[pf2e.feats-srd.LI9VtCaL5ZRk0Wo8]`|
|[Stand Still](feats/class-04-Mj1KTiAwwovm7K9f.htm)|Ne bouge plus|`@Compendium[pf2e.feats-srd.Mj1KTiAwwovm7K9f]`|
|[Startling Appearance](feats/archetype-06-qg8TlLJRgvjzW9YK.htm)|Apparition surprenante|`@Compendium[pf2e.feats-srd.qg8TlLJRgvjzW9YK]`|
|[Stave off Catastrophe](feats/archetype-18-rmO7FP410nvCjFBB.htm)|Éviter la catastrophe|`@Compendium[pf2e.feats-srd.rmO7FP410nvCjFBB]`|
|[Stay Down!](feats/class-14-2KBKXkRthBXpw48X.htm)|Reste à terre !|`@Compendium[pf2e.feats-srd.2KBKXkRthBXpw48X]`|
|[Steadfast Ally](feats/ancestry-13-s0OqtQOeYOGkBMYG.htm)|Allié inébranlable|`@Compendium[pf2e.feats-srd.s0OqtQOeYOGkBMYG]`|
|[Steady Balance](feats/skill-01-CnqMJR8e9jqJR7MM.htm)|Équilibre stable|`@Compendium[pf2e.feats-srd.CnqMJR8e9jqJR7MM]`|
|[Steady Spellcasting](feats/class-06-uiGsVmvRfujQQRlK.htm)|Incantation fiable|`@Compendium[pf2e.feats-srd.uiGsVmvRfujQQRlK]`|
|[Steal Essence](feats/class-20-0VUx8g8GJzuxvLSa.htm)|Essence dérobée|`@Compendium[pf2e.feats-srd.0VUx8g8GJzuxvLSa]`|
|[Steal Spell](feats/class-16-xjwlP306nuda2z03.htm)|Sort volé|`@Compendium[pf2e.feats-srd.xjwlP306nuda2z03]`|
|[Steal the Sky](feats/archetype-10-DFtbxytrOrmkQRfm.htm)|Voler le ciel|`@Compendium[pf2e.feats-srd.DFtbxytrOrmkQRfm]`|
|[Stealthy Companion](feats/class-14-MwozkE6aj42WZ7Z1.htm)|Compagnon furtif|`@Compendium[pf2e.feats-srd.MwozkE6aj42WZ7Z1]`|
|[Steel Skin](feats/skill-04-uxwHHjWs3ehqtG4b.htm)|Peau d'acier|`@Compendium[pf2e.feats-srd.uxwHHjWs3ehqtG4b]`|
|[Steel Your Resolve](feats/general-03-jFmdevE4nKevovzo.htm)|Puiser dans votre résolution|`@Compendium[pf2e.feats-srd.jFmdevE4nKevovzo]`|
|[Steel Yourself!](feats/archetype-04-4Y7wKFogGB0LZ5ZA.htm)|Prépare toi !|`@Compendium[pf2e.feats-srd.4Y7wKFogGB0LZ5ZA]`|
|[Stella's Stab and Snag](feats/class-06-IEbnal1VJySrhxFR.htm)|Frappe et emporte de Stella|`@Compendium[pf2e.feats-srd.IEbnal1VJySrhxFR]`|
|[Step Lively](feats/ancestry-05-l3PosipTLXANeoT8.htm)|Pas vif|`@Compendium[pf2e.feats-srd.l3PosipTLXANeoT8]`|
|[Sticky Bomb](feats/class-08-zXKfKKOxht0b0XNL.htm)|Bombe collante|`@Compendium[pf2e.feats-srd.zXKfKKOxht0b0XNL]`|
|[Sticky Poison](feats/class-06-b7isszc8C75V3okn.htm)|Poison collant|`@Compendium[pf2e.feats-srd.b7isszc8C75V3okn]`|
|[Stone Blood](feats/archetype-06-CoVBrlyMToANvt2v.htm)|Sang de pierre|`@Compendium[pf2e.feats-srd.CoVBrlyMToANvt2v]`|
|[Stone Bones](feats/ancestry-09-3Y1k2eAMqCdcCGmK.htm)|Os de pierre|`@Compendium[pf2e.feats-srd.3Y1k2eAMqCdcCGmK]`|
|[Stone Communion](feats/archetype-14-llawV63qzdynbOkx.htm)|Communion avec la pierre|`@Compendium[pf2e.feats-srd.llawV63qzdynbOkx]`|
|[Stonecunning](feats/ancestry-01-HOUHs5rahwIsQoBf.htm)|Connaissance de la pierre|`@Compendium[pf2e.feats-srd.HOUHs5rahwIsQoBf]`|
|[Stonegate](feats/ancestry-17-UquGIEFKhutgJsEz.htm)|Passepierre|`@Compendium[pf2e.feats-srd.UquGIEFKhutgJsEz]`|
|[Stonewalker](feats/ancestry-09-KTLdk65OOAVixqtY.htm)|Marchepierre|`@Compendium[pf2e.feats-srd.KTLdk65OOAVixqtY]`|
|[Storm Born](feats/class-01-UpEjRfQkCJCruAfb.htm)|Enfant de la tempête|`@Compendium[pf2e.feats-srd.UpEjRfQkCJCruAfb]`|
|[Storm Retribution](feats/class-06-F1DVDJRARfdb1Kjz.htm)|Châtiment de la tempête|`@Compendium[pf2e.feats-srd.F1DVDJRARfdb1Kjz]`|
|[Storm's Lash](feats/ancestry-01-bWyY9NtLU5wXr03y.htm)|Fouet de la tempête|`@Compendium[pf2e.feats-srd.bWyY9NtLU5wXr03y]`|
|[Strategic Assessment](feats/class-04-IvgLDtXmZwzbVJj1.htm)|Évaluation stratégique|`@Compendium[pf2e.feats-srd.IvgLDtXmZwzbVJj1]`|
|[Strategic Bypass](feats/class-14-NgUB5toKxBd8RJmm.htm)|Contournement stratégique|`@Compendium[pf2e.feats-srd.NgUB5toKxBd8RJmm]`|
|[Straveika](feats/ancestry-01-PVkAEBlRSJHe3JCz.htm)|straveika|`@Compendium[pf2e.feats-srd.PVkAEBlRSJHe3JCz]`|
|[Streetwise](feats/skill-01-X2jGFfLU5qI5XVot.htm)|Savoir urbain|`@Compendium[pf2e.feats-srd.X2jGFfLU5qI5XVot]`|
|[Strong Arm](feats/class-02-OiY0L3WvjwlQQ4iG.htm)|Bras puissant|`@Compendium[pf2e.feats-srd.OiY0L3WvjwlQQ4iG]`|
|[Stubborn Persistence](feats/ancestry-13-55XNy1TVETEMc0vf.htm)|Persévérance obstinée|`@Compendium[pf2e.feats-srd.55XNy1TVETEMc0vf]`|
|[Student Of The Dueling Arts](feats/archetype-12-gHHnMCBi1gvG5wTL.htm)|Étudiant des arts du duel|`@Compendium[pf2e.feats-srd.gHHnMCBi1gvG5wTL]`|
|[Student of Perfection Dedication](feats/archetype-02-eHjqNXgylSuvA887.htm)|Dévouement de l'Étudiant en perfection|`@Compendium[pf2e.feats-srd.eHjqNXgylSuvA887]`|
|[Student of the Canon](feats/skill-01-x7EMZNMavris2aHY.htm)|Etudiant du droit canon|`@Compendium[pf2e.feats-srd.x7EMZNMavris2aHY]`|
|[Studious Capacity](feats/class-16-QGpcyvIezLMgmTia.htm)|Grande capacité d'étude|`@Compendium[pf2e.feats-srd.QGpcyvIezLMgmTia]`|
|[Stumbling Feint](feats/class-06-kYA6LkDw4AzKI156.htm)|Feinte chancelante|`@Compendium[pf2e.feats-srd.kYA6LkDw4AzKI156]`|
|[Stumbling Stance](feats/class-01-7FRYyKXDKjGoANYj.htm)|Posture chancelante|`@Compendium[pf2e.feats-srd.7FRYyKXDKjGoANYj]`|
|[Stunning Appearance](feats/archetype-16-EwCDbWg8yPxDWF4a.htm)|Apparition étourdissante|`@Compendium[pf2e.feats-srd.EwCDbWg8yPxDWF4a]`|
|[Stunning Finisher](feats/class-08-jkBzlMB4TS1sS2Fm.htm)|Botte étourdissante|`@Compendium[pf2e.feats-srd.jkBzlMB4TS1sS2Fm]`|
|[Stunning Fist](feats/class-02-8bMmGBO6gsMfWZk3.htm)|Poing étourdissant|`@Compendium[pf2e.feats-srd.8bMmGBO6gsMfWZk3]`|
|[Sturdy Bindings](feats/bonus-00-XOAtP64xiSRIofsY.htm)||`@Compendium[pf2e.feats-srd.XOAtP64xiSRIofsY]`|
|[Subjective Truth](feats/archetype-07-oXoQ9wwOmDe0hwbU.htm)|Vérité subjective|`@Compendium[pf2e.feats-srd.oXoQ9wwOmDe0hwbU]`|
|[Subtle Delivery](feats/class-01-QZ00D2xdJnbWFzml.htm)|Libération subtile|`@Compendium[pf2e.feats-srd.QZ00D2xdJnbWFzml]`|
|[Subtle Shank](feats/class-08-ZdL8pPPV0QCkBML1.htm)|Surin subtil|`@Compendium[pf2e.feats-srd.ZdL8pPPV0QCkBML1]`|
|[Subtle Theft](feats/skill-01-hVZbnsDuXihggylt.htm)|Vol subtil|`@Compendium[pf2e.feats-srd.hVZbnsDuXihggylt]`|
|[Sudden Charge](feats/class-01-qQt3CMrhLkUV1wCv.htm)|Charge soudaine|`@Compendium[pf2e.feats-srd.qQt3CMrhLkUV1wCv]`|
|[Sudden Leap](feats/class-08-BkKWTt3ufaCN2ZdI.htm)|Bond soudain|`@Compendium[pf2e.feats-srd.BkKWTt3ufaCN2ZdI]`|
|[Summon Celestial Kin](feats/ancestry-13-L231BR4815B6hwKT.htm)|Convoquer un céleste apparenté|`@Compendium[pf2e.feats-srd.L231BR4815B6hwKT]`|
|[Summon Fiendish Kin](feats/ancestry-13-05wQPB1Z20DOy4rH.htm)|Convoquer un fiélon apparenté|`@Compendium[pf2e.feats-srd.05wQPB1Z20DOy4rH]`|
|[Sun Blade](feats/class-04-TNpoEG0cUEAuSju7.htm)|Lame solaire|`@Compendium[pf2e.feats-srd.TNpoEG0cUEAuSju7]`|
|[Sun's Fury](feats/archetype-06-vqLt1qjdrflTmdsw.htm)|Furie du soleil|`@Compendium[pf2e.feats-srd.vqLt1qjdrflTmdsw]`|
|[Sunder Enchantment](feats/class-14-RpXWOgLWQLGdx74I.htm)|Destruction d'enchantement|`@Compendium[pf2e.feats-srd.RpXWOgLWQLGdx74I]`|
|[Sunder Spell](feats/class-12-8INrcMUv5vzWMG3X.htm)|Destruction de sort|`@Compendium[pf2e.feats-srd.8INrcMUv5vzWMG3X]`|
|[Superior Bond](feats/class-14-GsrwoQ7DIjERXuPf.htm)|Lien supérieur|`@Compendium[pf2e.feats-srd.GsrwoQ7DIjERXuPf]`|
|[Superior Sight](feats/class-20-Uof5QNeGklGnks1h.htm)|Vision supérieure|`@Compendium[pf2e.feats-srd.Uof5QNeGklGnks1h]`|
|[Supernatural Charm](feats/ancestry-05-Rr7GdUjXhGhV04Pe.htm)|Charme surnaturel|`@Compendium[pf2e.feats-srd.Rr7GdUjXhGhV04Pe]`|
|[Supernatural Senses](feats/class-04-so4v9xjBFaoJ8EQs.htm)|Sens surnaturels|`@Compendium[pf2e.feats-srd.so4v9xjBFaoJ8EQs]`|
|[Supertaster](feats/general-07-fOIUmDGa9gkeCHA0.htm)|Supergoûteur|`@Compendium[pf2e.feats-srd.fOIUmDGa9gkeCHA0]`|
|[Sure Feet](feats/ancestry-01-ehvHedIwPcxq9cRt.htm)|Pied assuré|`@Compendium[pf2e.feats-srd.ehvHedIwPcxq9cRt]`|
|[Sure Foot](feats/archetype-04-HRVCODLOrhjRDtGb.htm)|Pied sûr|`@Compendium[pf2e.feats-srd.HRVCODLOrhjRDtGb]`|
|[Surface Culture](feats/ancestry-01-hkMQGiTJCLVAWHy0.htm)|Culture de la surface|`@Compendium[pf2e.feats-srd.hkMQGiTJCLVAWHy0]`|
|[Surging Focus](feats/class-08-MRxQDZFNPpUKC0CL.htm)|Focalisation déferlante|`@Compendium[pf2e.feats-srd.MRxQDZFNPpUKC0CL]`|
|[Surging Might](feats/class-10-YV4X9u5Yuf0xvoCh.htm)|Puissance déferlante|`@Compendium[pf2e.feats-srd.YV4X9u5Yuf0xvoCh]`|
|[Surprise Attack](feats/archetype-04-io6eJGrw701WbmYe.htm)|Attaque surprise|`@Compendium[pf2e.feats-srd.io6eJGrw701WbmYe]`|
|[Surprise Snare](feats/archetype-04-wqrOVv9gnqF4nlLR.htm)|Piège artisanal surprise|`@Compendium[pf2e.feats-srd.wqrOVv9gnqF4nlLR]`|
|[Surprise Strike](feats/archetype-06-04RgXKFVC2A6Ryn6.htm)|Frappe surprise|`@Compendium[pf2e.feats-srd.04RgXKFVC2A6Ryn6]`|
|[Surreptitious Spellcaster](feats/archetype-08-fHWYICk6cSePr30c.htm)|Lanceur de sort subreptice|`@Compendium[pf2e.feats-srd.fHWYICk6cSePr30c]`|
|[Survey Wildlife](feats/skill-01-tGIXuk0XeWmG04CX.htm)|Observation de la nature|`@Compendium[pf2e.feats-srd.tGIXuk0XeWmG04CX]`|
|[Survivor of Desolation](feats/archetype-08-n7LTwKUSATLaQ9FD.htm)|Survivant de la désolation|`@Compendium[pf2e.feats-srd.n7LTwKUSATLaQ9FD]`|
|[Suspect of Opportunity](feats/class-10-Sc9clbAXe97vlzxM.htm)|Suspect opportun|`@Compendium[pf2e.feats-srd.Sc9clbAXe97vlzxM]`|
|[Svetocher](feats/ancestry-01-P1dk0LTWkQ1LT1ai.htm)|Svetocher|`@Compendium[pf2e.feats-srd.P1dk0LTWkQ1LT1ai]`|
|[Swaggering Initiative](feats/class-04-NHheDmNB7L4REmlr.htm)|Initiative arrogante|`@Compendium[pf2e.feats-srd.NHheDmNB7L4REmlr]`|
|[Swap Investment](feats/archetype-12-xNejAvuRXKYq2D6A.htm)|Échange d'investissement|`@Compendium[pf2e.feats-srd.xNejAvuRXKYq2D6A]`|
|[Swashbuckler Dedication](feats/archetype-02-eAlrvPVb8qt8Lruw.htm)|Dévouement de Bretteur|`@Compendium[pf2e.feats-srd.eAlrvPVb8qt8Lruw]`|
|[Swashbuckler's Riposte](feats/archetype-06-lZ0swL9EEUgbAuaZ.htm)|Ripose du bretteur|`@Compendium[pf2e.feats-srd.lZ0swL9EEUgbAuaZ]`|
|[Swashbuckler's Speed](feats/archetype-08-RU86cGTryRAdaEqx.htm)|Vitesse du Bretteur|`@Compendium[pf2e.feats-srd.RU86cGTryRAdaEqx]`|
|[Swift Banishment](feats/class-14-t3unBu3PX6AO0uIW.htm)|Bannissement rapide|`@Compendium[pf2e.feats-srd.t3unBu3PX6AO0uIW]`|
|[Swift Elusion](feats/class-16-TqODgJL1VS3eokhX.htm)|Échappatoire rapide|`@Compendium[pf2e.feats-srd.TqODgJL1VS3eokhX]`|
|[Swift River](feats/class-18-IxggfXunfldeVOsQ.htm)|Libre comme l’eau|`@Compendium[pf2e.feats-srd.IxggfXunfldeVOsQ]`|
|[Swift Sneak](feats/skill-07-Imvu2RV2ggjJ2HZt.htm)|Furtivité rapide|`@Compendium[pf2e.feats-srd.Imvu2RV2ggjJ2HZt]`|
|[Swift Swimmer](feats/ancestry-05-iliy0ONIb8Hw6muA.htm)|Nageur rapide|`@Compendium[pf2e.feats-srd.iliy0ONIb8Hw6muA]`|
|[Swift Tracker](feats/class-06-c5ns35FLvvxjimuH.htm)|Pistage accéléré|`@Compendium[pf2e.feats-srd.c5ns35FLvvxjimuH]`|
|[Swipe](feats/class-04-JbrVcOf82oFXk3mY.htm)|Frappe transversale (Barbare)|`@Compendium[pf2e.feats-srd.JbrVcOf82oFXk3mY]`|
|[Swordmaster Dedication](feats/class-06-SHpVHkPxtQggD9Cf.htm)|Dévouement de Maître des épées|`@Compendium[pf2e.feats-srd.SHpVHkPxtQggD9Cf]`|
|[Symphony Of Blood](feats/ancestry-17-nJRcbt72wRk5Rmc4.htm)|Symphonie de sang|`@Compendium[pf2e.feats-srd.nJRcbt72wRk5Rmc4]`|
|[Symphony Of The Unfettered Heart](feats/class-10-VJl2xHDKr0HxTUrs.htm)|Symphonie du cœur affranchi|`@Compendium[pf2e.feats-srd.VJl2xHDKr0HxTUrs]`|
|[Symphony of the Muse](feats/class-20-sfxLo9kz2WkCQiy4.htm)|Symphonie de la muse|`@Compendium[pf2e.feats-srd.sfxLo9kz2WkCQiy4]`|
|[Syncretism](feats/class-01-yVC5pVZaWczYWGTa.htm)|Syncrétisme|`@Compendium[pf2e.feats-srd.yVC5pVZaWczYWGTa]`|
|[Synergistic Spell](feats/archetype-20-WAWaew53zpTFVwQM.htm)|Sort synergique|`@Compendium[pf2e.feats-srd.WAWaew53zpTFVwQM]`|
|[Tactical Cadence](feats/archetype-14-4dQcLroKQ13QYIT3.htm)|Cadence tactique|`@Compendium[pf2e.feats-srd.4dQcLroKQ13QYIT3]`|
|[Tactical Debilitations](feats/class-10-Xdmz09kzLjIxWz9C.htm)|Handicaps tactiques|`@Compendium[pf2e.feats-srd.Xdmz09kzLjIxWz9C]`|
|[Tactical Entry](feats/class-08-MhoGCLKI5zxQ4SFD.htm)|Entrée tactique|`@Compendium[pf2e.feats-srd.MhoGCLKI5zxQ4SFD]`|
|[Tail Spin](feats/ancestry-05-rnEfO5eyRw7Fywzb.htm)|Queue renversante|`@Compendium[pf2e.feats-srd.rnEfO5eyRw7Fywzb]`|
|[Tail Whip](feats/ancestry-01-LLQ5SO2c44uXDFJk.htm)|Queue fouet|`@Compendium[pf2e.feats-srd.LLQ5SO2c44uXDFJk]`|
|[Takedown Expert](feats/class-01-yPWNbTqOIKdkwaVq.htm)|Expert de la mise au tapis|`@Compendium[pf2e.feats-srd.yPWNbTqOIKdkwaVq]`|
|[Talisman Dabbler Dedication](feats/archetype-02-1t5479E6bdvFs4E7.htm)|Dévouement d'Amateur de talismans|`@Compendium[pf2e.feats-srd.1t5479E6bdvFs4E7]`|
|[Talismanic Sage](feats/archetype-14-uPikeCzrTrgzEJT8.htm)|Sage talismanique|`@Compendium[pf2e.feats-srd.uPikeCzrTrgzEJT8]`|
|[Tangle Of Battle](feats/class-10-GLbl3qoWCvvjJr4S.htm)|Enchevêtrement de la bataille|`@Compendium[pf2e.feats-srd.GLbl3qoWCvvjJr4S]`|
|[Tangled Forest Rake](feats/class-14-ewbt80Yin18k6oLq.htm)|Ratissage de la forêt enchevêtrée|`@Compendium[pf2e.feats-srd.ewbt80Yin18k6oLq]`|
|[Tangled Forest Stance](feats/class-08-2tUdsoPEnW9yS8so.htm)|Posture de la forêt enchevêtrée|`@Compendium[pf2e.feats-srd.2tUdsoPEnW9yS8so]`|
|[Target Of Opportunity](feats/archetype-14-2FJwXMTJycSZY80Q.htm)|Cible d'opportunité|`@Compendium[pf2e.feats-srd.2FJwXMTJycSZY80Q]`|
|[Targeting Finisher](feats/class-10-DLkMoVb8qb4qxnx3.htm)|Botté ciblée|`@Compendium[pf2e.feats-srd.DLkMoVb8qb4qxnx3]`|
|[Targeting Shot](feats/class-14-HVwmYfSLhrnCksHV.htm)|Tir de ciblage|`@Compendium[pf2e.feats-srd.HVwmYfSLhrnCksHV]`|
|[Telluric Power](feats/ancestry-13-ADDOwO0QWOXAV85x.htm)|Puissance tellurique|`@Compendium[pf2e.feats-srd.ADDOwO0QWOXAV85x]`|
|[Tempest-Sun Redirection](feats/archetype-06-FJdcQU6yjDVDBC4r.htm)|Redirection du Soleil tempétueux|`@Compendium[pf2e.feats-srd.FJdcQU6yjDVDBC4r]`|
|[Tempest-Sun Shielding](feats/archetype-10-sTUicpQkhiFVtMK1.htm)|Protection du Soleil-tempétueux|`@Compendium[pf2e.feats-srd.sTUicpQkhiFVtMK1]`|
|[Temporary Potions](feats/class-10-SY6bU7DOyfs22cJX.htm)|Potions temporaires|`@Compendium[pf2e.feats-srd.SY6bU7DOyfs22cJX]`|
|[Tenacious Blood Magic](feats/class-20-UlCbjWWVEmfvaf5a.htm)|Sang magique tenace|`@Compendium[pf2e.feats-srd.UlCbjWWVEmfvaf5a]`|
|[Tenacious Toxins](feats/class-04-FdP21jbjHHGpHut1.htm)|Toxines tenaces|`@Compendium[pf2e.feats-srd.FdP21jbjHHGpHut1]`|
|[Tengu Lore](feats/ancestry-01-GjQZMmw2sz8OyLxj.htm)|Connaissance des Tengu|`@Compendium[pf2e.feats-srd.GjQZMmw2sz8OyLxj]`|
|[Tengu Weapon Expertise](feats/ancestry-13-8JOLT1UX5BVG4kVY.htm)|Epertise avec les armes tengu|`@Compendium[pf2e.feats-srd.8JOLT1UX5BVG4kVY]`|
|[Tengu Weapon Familiarity](feats/ancestry-01-AmFv3ClkAVRowHLI.htm)|Familiarité avec les armes tengu|`@Compendium[pf2e.feats-srd.AmFv3ClkAVRowHLI]`|
|[Tengu Weapon Study](feats/ancestry-05-X6tVQ5S7H7GuCZux.htm)|Étude des armes tengu|`@Compendium[pf2e.feats-srd.X6tVQ5S7H7GuCZux]`|
|[Tense Negotiator](feats/archetype-12-K5pXeeJLmdE8XuvM.htm)|Négociateur tendu|`@Compendium[pf2e.feats-srd.K5pXeeJLmdE8XuvM]`|
|[Terraforming Spell](feats/class-16-RMSVWMFoLUk0P1cC.htm)|Sort de terraformage|`@Compendium[pf2e.feats-srd.RMSVWMFoLUk0P1cC]`|
|[Terrain Advantage](feats/ancestry-09-FrzskqwNWexKY5BA.htm)|Avantage du terrain|`@Compendium[pf2e.feats-srd.FrzskqwNWexKY5BA]`|
|[Terrain Expertise](feats/skill-01-4tTkRyOQ0VuRBac3.htm)|Expertise du terrain|`@Compendium[pf2e.feats-srd.4tTkRyOQ0VuRBac3]`|
|[Terrain Expertise (Aquatic)](feats/skill-01-2hoU2p8vVM8pPHh7.htm)||`@Compendium[pf2e.feats-srd.2hoU2p8vVM8pPHh7]`|
|[Terrain Expertise (Arctic)](feats/skill-01-Dk02ex6pScuNQUBA.htm)||`@Compendium[pf2e.feats-srd.Dk02ex6pScuNQUBA]`|
|[Terrain Expertise (Desert)](feats/skill-01-f3JBnLJNOkt6BFkg.htm)||`@Compendium[pf2e.feats-srd.f3JBnLJNOkt6BFkg]`|
|[Terrain Expertise (Forest)](feats/skill-01-nqaSdrxAE43lZZtI.htm)||`@Compendium[pf2e.feats-srd.nqaSdrxAE43lZZtI]`|
|[Terrain Expertise (Mountain)](feats/skill-01-3VfFj32G1HXOUNzg.htm)||`@Compendium[pf2e.feats-srd.3VfFj32G1HXOUNzg]`|
|[Terrain Expertise (Plains)](feats/skill-01-PjFaiagyYzqLTQ6i.htm)||`@Compendium[pf2e.feats-srd.PjFaiagyYzqLTQ6i]`|
|[Terrain Expertise (Sky)](feats/skill-01-SRwYgPYcZNdtuqEZ.htm)||`@Compendium[pf2e.feats-srd.SRwYgPYcZNdtuqEZ]`|
|[Terrain Expertise (Swamp)](feats/skill-01-LdBzrTEZlNL4m5nI.htm)||`@Compendium[pf2e.feats-srd.LdBzrTEZlNL4m5nI]`|
|[Terrain Expertise (Underground)](feats/skill-01-zWOUgpeWb0oA0u7I.htm)||`@Compendium[pf2e.feats-srd.zWOUgpeWb0oA0u7I]`|
|[Terrain Master](feats/class-08-VKNxblSUxYXQYlLr.htm)|Maître de l'environnement|`@Compendium[pf2e.feats-srd.VKNxblSUxYXQYlLr]`|
|[Terrain Scout](feats/archetype-04-KOf6kmwAZaUJSDW9.htm)|Éclaireur de terrain|`@Compendium[pf2e.feats-srd.KOf6kmwAZaUJSDW9]`|
|[Terrain Stalker](feats/skill-01-beyw5bdA5hkQbmaG.htm)|Traqueur de terrain|`@Compendium[pf2e.feats-srd.beyw5bdA5hkQbmaG]`|
|[Terrain Transposition](feats/class-10-H0tffYs7ODMQ3JJj.htm)|Transposition de terrain|`@Compendium[pf2e.feats-srd.H0tffYs7ODMQ3JJj]`|
|[Terrified Retreat](feats/skill-07-dc8X2Mbtwq6kGp7F.htm)|Retraite terrifiée|`@Compendium[pf2e.feats-srd.dc8X2Mbtwq6kGp7F]`|
|[Terrifying Howl](feats/class-10-HjinlKihkadhkQ4Z.htm)|Hurlement terrifiant|`@Compendium[pf2e.feats-srd.HjinlKihkadhkQ4Z]`|
|[Terrifying Resistance](feats/skill-02-iDimfGmQHacwxeh2.htm)|Résistance terrifiante|`@Compendium[pf2e.feats-srd.iDimfGmQHacwxeh2]`|
|[That's Odd](feats/class-01-pIHjH1x0AVtiX5Tv.htm)|C'est étrange|`@Compendium[pf2e.feats-srd.pIHjH1x0AVtiX5Tv]`|
|[The Harder They Fall](feats/class-04-k4QU2edqSoB23foo.htm)|Plus dure est la chute|`@Compendium[pf2e.feats-srd.k4QU2edqSoB23foo]`|
|[Theoretical Acumen](feats/ancestry-01-MXkklchuimVSHZyd.htm)|Perspicacité théorique|`@Compendium[pf2e.feats-srd.MXkklchuimVSHZyd]`|
|[Thorough Reports](feats/archetype-04-0Dy8RlFqrzCVOTl4.htm)|Rapports détaillés|`@Compendium[pf2e.feats-srd.0Dy8RlFqrzCVOTl4]`|
|[Thorough Research](feats/class-06-WjEwsu4kkexNvDcN.htm)|Recherche minutieuse|`@Compendium[pf2e.feats-srd.WjEwsu4kkexNvDcN]`|
|[Thorough Search](feats/general-03-9QQnv7nFpsNCGE89.htm)|Fouille minutieuse|`@Compendium[pf2e.feats-srd.9QQnv7nFpsNCGE89]`|
|[Thousand Faces](feats/class-04-cxwDj2gZ7kJdP4hs.htm)|Mille visages|`@Compendium[pf2e.feats-srd.cxwDj2gZ7kJdP4hs]`|
|[Thrash](feats/class-08-rMPL11JRcmlutvRi.htm)|Maltraitance|`@Compendium[pf2e.feats-srd.rMPL11JRcmlutvRi]`|
|[Through Spell](feats/class-10-R40U8hF0hWyRUze8.htm)|Sort traversant|`@Compendium[pf2e.feats-srd.R40U8hF0hWyRUze8]`|
|[Tiger Slash](feats/class-06-6J2ZSGNsXPKPcJGV.htm)|Taillade du tigre|`@Compendium[pf2e.feats-srd.6J2ZSGNsXPKPcJGV]`|
|[Tiger Stance](feats/class-01-VCjAlOvjNvFJOsG5.htm)|Posture du tigre|`@Compendium[pf2e.feats-srd.VCjAlOvjNvFJOsG5]`|
|[Tiller's Aid](feats/archetype-10-6Hv4kpHj8IexNUey.htm)|Aide du Laboureur|`@Compendium[pf2e.feats-srd.6Hv4kpHj8IexNUey]`|
|[Tiller's Drive](feats/archetype-10-Wo0HNBh6CCgPN3Co.htm)|Conduite du Laboureur|`@Compendium[pf2e.feats-srd.Wo0HNBh6CCgPN3Co]`|
|[Timeless Body](feats/class-14-5cxkVY3mBsbYWd5K.htm)|Éternelle jeunesse|`@Compendium[pf2e.feats-srd.5cxkVY3mBsbYWd5K]`|
|[Timeless Nature](feats/class-14-TpWS2b9ISHnXVfZg.htm)|Nature intemporelle|`@Compendium[pf2e.feats-srd.TpWS2b9ISHnXVfZg]`|
|[Tinkering Fingers](feats/ancestry-01-wTT8ieLvfWsZZaWT.htm)|Doigts bricoleurs|`@Compendium[pf2e.feats-srd.wTT8ieLvfWsZZaWT]`|
|[Titan Slinger](feats/ancestry-01-50CRpoP5XS1MVbu8.htm)|Frondeur de titans|`@Compendium[pf2e.feats-srd.50CRpoP5XS1MVbu8]`|
|[Titan Wrestler](feats/skill-01-KxaYlC50zzHysJj8.htm)|Lutteur contre les Titans|`@Compendium[pf2e.feats-srd.KxaYlC50zzHysJj8]`|
|[Titan's Stature](feats/class-12-nI67dTzKYg5kKMsd.htm)|Stature de titan|`@Compendium[pf2e.feats-srd.nI67dTzKYg5kKMsd]`|
|[To Battle!](feats/archetype-08-cU5NdcwnMkFQNPjh.htm)|Au Combat!|`@Compendium[pf2e.feats-srd.cU5NdcwnMkFQNPjh]`|
|[To the Ends of the Earth](feats/class-20-HLCeP87w7qEy8PUH.htm)|Jusqu’aux confins du monde|`@Compendium[pf2e.feats-srd.HLCeP87w7qEy8PUH]`|
|[Tomb-Watcher's Glare](feats/ancestry-05-oL6f86dy4yyx5N54.htm)|Regard du gardemort|`@Compendium[pf2e.feats-srd.oL6f86dy4yyx5N54]`|
|[Tongue of Sun and Moon](feats/class-14-ct4dJHBn1Dj4cx4B.htm)|Langue du soleil et de la lune|`@Compendium[pf2e.feats-srd.ct4dJHBn1Dj4cx4B]`|
|[Tools Of The Trade](feats/archetype-04-mRHkGMLecd5aaj2R.htm)|Outils indispensables|`@Compendium[pf2e.feats-srd.mRHkGMLecd5aaj2R]`|
|[Topple Foe](feats/archetype-10-7x1r2w7C7A4Uy7wG.htm)|Renverser l'adversaire|`@Compendium[pf2e.feats-srd.7x1r2w7C7A4Uy7wG]`|
|[Toppling Dance](feats/ancestry-13-l78KwojlT0AWvS4l.htm)|Danse renversante|`@Compendium[pf2e.feats-srd.l78KwojlT0AWvS4l]`|
|[Torch Goblin](feats/ancestry-05-U5FcfRvveTKtgebq.htm)|Gobelin torche|`@Compendium[pf2e.feats-srd.U5FcfRvveTKtgebq]`|
|[Tough Tumbler](feats/ancestry-05-9CIy4cJJTcRM30Vz.htm)|Cascadeur coriace|`@Compendium[pf2e.feats-srd.9CIy4cJJTcRM30Vz]`|
|[Toughness](feats/general-01-AmP0qu7c5dlBSath.htm)|Robustesse|`@Compendium[pf2e.feats-srd.AmP0qu7c5dlBSath]`|
|[Trailblazing Stride](feats/bonus-00-PpJURSJFEHzhutdp.htm)||`@Compendium[pf2e.feats-srd.PpJURSJFEHzhutdp]`|
|[Train Animal](feats/skill-01-nowEaHgIyij7im8F.htm)|Dressage animalier|`@Compendium[pf2e.feats-srd.nowEaHgIyij7im8F]`|
|[Trampling Charge](feats/archetype-10-t6sey3cyV8n7a78l.htm)|Charge piétinante|`@Compendium[pf2e.feats-srd.t6sey3cyV8n7a78l]`|
|[Transcribe Moment](feats/archetype-08-PaUpesy5lyDLlwud.htm)|Transcrire le moment|`@Compendium[pf2e.feats-srd.PaUpesy5lyDLlwud]`|
|[Trap Finder](feats/class-01-oA866uVEFu1OrAX0.htm)|Dénicheur de pièges|`@Compendium[pf2e.feats-srd.oA866uVEFu1OrAX0]`|
|[Treat Condition](feats/skill-04-rfnEcjxIFqwlJwJT.htm)|Soigner un état|`@Compendium[pf2e.feats-srd.rfnEcjxIFqwlJwJT]`|
|[Tree Climber (Elf)](feats/ancestry-09-qS2VcFLez4PLOpIS.htm)|Grimpeur aux arbres (Elfe)|`@Compendium[pf2e.feats-srd.qS2VcFLez4PLOpIS]`|
|[Tree Climber (Goblin)](feats/ancestry-05-XYtnVNKt6uPcRrdH.htm)|Grimpeur aux arbres (Gobelin)|`@Compendium[pf2e.feats-srd.XYtnVNKt6uPcRrdH]`|
|[Triangle Shot](feats/class-18-62glnJI2o0KnHULB.htm)|Tir triangulé|`@Compendium[pf2e.feats-srd.62glnJI2o0KnHULB]`|
|[Trick Magic Item](feats/skill-01-uR62fVC9FyQAMCO1.htm)|Usurpation d'objet magique|`@Compendium[pf2e.feats-srd.uR62fVC9FyQAMCO1]`|
|[Trickster's Ace](feats/class-18-POrE3ZgBRdBL9MsW.htm)|Atout du trompeur|`@Compendium[pf2e.feats-srd.POrE3ZgBRdBL9MsW]`|
|[Triple Shot](feats/class-06-hkdP5tsTAoqJDR8v.htm)|Triple tir|`@Compendium[pf2e.feats-srd.hkdP5tsTAoqJDR8v]`|
|[Triple Threat](feats/class-20-EHorYedQ8r05qAtk.htm)|Triple menace|`@Compendium[pf2e.feats-srd.EHorYedQ8r05qAtk]`|
|[Triple Time](feats/class-04-B0T6p3kcrOfSvLqQ.htm)|A trois temps|`@Compendium[pf2e.feats-srd.B0T6p3kcrOfSvLqQ]`|
|[Triumphant Inspiration](feats/class-14-RHLfM9NlIlHTH85w.htm)|Inspiration triomphante|`@Compendium[pf2e.feats-srd.RHLfM9NlIlHTH85w]`|
|[True Debilitating Bomb](feats/class-14-2HMvAB6mIVwvwtjT.htm)|Bombe incapacitante ultime|`@Compendium[pf2e.feats-srd.2HMvAB6mIVwvwtjT]`|
|[True Hypercognition](feats/class-14-Xk9inG3pln4UKbs3.htm)|Hypercognition ultime|`@Compendium[pf2e.feats-srd.Xk9inG3pln4UKbs3]`|
|[True Perception](feats/general-19-uudiUylT09WnHN7e.htm)|Perception véritable|`@Compendium[pf2e.feats-srd.uudiUylT09WnHN7e]`|
|[True Shapeshifter](feats/class-20-mGzPR7M9H733j2wN.htm)|Métamorphe ultime|`@Compendium[pf2e.feats-srd.mGzPR7M9H733j2wN]`|
|[Tumble Behind (Rogue)](feats/class-01-YKqMuuC8j35NFh92.htm)|Passer derrière (Roublard)|`@Compendium[pf2e.feats-srd.YKqMuuC8j35NFh92]`|
|[Tumble Behind (Swashbuckler)](feats/class-02-fU7d5P6WrfAirgip.htm)|Passer derrière (Bretteur)|`@Compendium[pf2e.feats-srd.fU7d5P6WrfAirgip]`|
|[Tumbling Opportunist](feats/archetype-10-Yazy4gex46FLwsph.htm)|Opportuniste acrobatique|`@Compendium[pf2e.feats-srd.Yazy4gex46FLwsph]`|
|[Tumbling Strike](feats/archetype-08-jfsmtOx4QgXB19BL.htm)|Frappe acrobatique|`@Compendium[pf2e.feats-srd.jfsmtOx4QgXB19BL]`|
|[Tupilaq Carver](feats/ancestry-01-m9tKNsZQQjHdsmEN.htm)|Graveur Tupilaq|`@Compendium[pf2e.feats-srd.m9tKNsZQQjHdsmEN]`|
|[Turn Undead](feats/class-02-PGyzFBwuTgypU8cD.htm)|Renvoi des morts-vivants|`@Compendium[pf2e.feats-srd.PGyzFBwuTgypU8cD]`|
|[Turpin Rowe Lumberjack Dedication](feats/archetype-02-0icxuITlxUBfeomX.htm)|Dévouement de Bûcheron de Turpin Rowe|`@Compendium[pf2e.feats-srd.0icxuITlxUBfeomX]`|
|[Tusks (Half-Orc)](feats/ancestry-01-wAeI18wyWVwfUrIP.htm)|Défenses (demi-orc)|`@Compendium[pf2e.feats-srd.wAeI18wyWVwfUrIP]`|
|[Tusks (Orc)](feats/ancestry-01-M5195FvfCN3X7gi9.htm)|Défenses (orc)|`@Compendium[pf2e.feats-srd.M5195FvfCN3X7gi9]`|
|[Tweak Appearances](feats/skill-02-vWtPxwND60EpxBAU.htm)|Modifier les apparences|`@Compendium[pf2e.feats-srd.vWtPxwND60EpxBAU]`|
|[Twin Distraction](feats/class-04-eXuCYDzj0UJOxNu9.htm)|Distraction jumelée|`@Compendium[pf2e.feats-srd.eXuCYDzj0UJOxNu9]`|
|[Twin Feint](feats/class-01-9XALeVNcmlIxf3tJ.htm)|Feinte jumelée|`@Compendium[pf2e.feats-srd.9XALeVNcmlIxf3tJ]`|
|[Twin Parry](feats/class-04-Y8LHfkzGyOhPlUou.htm)|Parade jumelée|`@Compendium[pf2e.feats-srd.Y8LHfkzGyOhPlUou]`|
|[Twin Riposte](feats/class-10-GJIAecRq1bD2r8O0.htm)|Riposte jumelée|`@Compendium[pf2e.feats-srd.GJIAecRq1bD2r8O0]`|
|[Twin Takedown](feats/class-01-Gw0wGXikhAhiGoud.htm)|Agression jumelée|`@Compendium[pf2e.feats-srd.Gw0wGXikhAhiGoud]`|
|[Twinned Defense (Fighter)](feats/class-16-xjLbabfyQzBNT4y1.htm)|Défense jumelée (Guerrier)|`@Compendium[pf2e.feats-srd.xjLbabfyQzBNT4y1]`|
|[Twinned Defense (Swashbuckler)](feats/class-14-kTRGAST9J9ZxJZ4A.htm)|Défense jumelée (Bretteur)|`@Compendium[pf2e.feats-srd.kTRGAST9J9ZxJZ4A]`|
|[Twist the Knife](feats/class-06-5Jc2ySGLVi053qpz.htm)|Remuer le couteau|`@Compendium[pf2e.feats-srd.5Jc2ySGLVi053qpz]`|
|[Twitchy](feats/ancestry-01-Bni2NcuQn6Z546RE.htm)|Nerveux|`@Compendium[pf2e.feats-srd.Bni2NcuQn6Z546RE]`|
|[Two-Weapon Flurry](feats/class-14-1SvBUzVH5tp0lmn5.htm)|Déluge à deux armes|`@Compendium[pf2e.feats-srd.1SvBUzVH5tp0lmn5]`|
|[Ubiquitous Snares](feats/class-16-bX2WI5k0afqPpCfm.htm)|Profusion de pièges artisanaux|`@Compendium[pf2e.feats-srd.bX2WI5k0afqPpCfm]`|
|[Ultimate Flexibility](feats/class-20-sayRIHE2V6vuxr4r.htm)|Flexibilité ultime|`@Compendium[pf2e.feats-srd.sayRIHE2V6vuxr4r]`|
|[Ultimate Mercy](feats/class-18-Rb16bcCiovwRqVgN.htm)|Soulagement suprême|`@Compendium[pf2e.feats-srd.Rb16bcCiovwRqVgN]`|
|[Ultimate Polymath](feats/class-20-QSBuAkJ5GMLcuZg9.htm)|Touche-à-tout ultime|`@Compendium[pf2e.feats-srd.QSBuAkJ5GMLcuZg9]`|
|[Ultimate Skirmisher](feats/class-20-2sCzFjq8sKvBR3Jh.htm)|Escarmoucheur suprême|`@Compendium[pf2e.feats-srd.2sCzFjq8sKvBR3Jh]`|
|[Unassuming Dedication](feats/ancestry-01-A1RzkLCj7mXJI1IY.htm)|Acharnement sans prétention|`@Compendium[pf2e.feats-srd.A1RzkLCj7mXJI1IY]`|
|[Unbalancing Blow](feats/class-02-4f65nrWVzTG8Njj8.htm)|Coup déséquilibrant|`@Compendium[pf2e.feats-srd.4f65nrWVzTG8Njj8]`|
|[Unbalancing Finisher](feats/class-02-VXA50vhIRCBt4vvP.htm)|Botte déséquilibrante|`@Compendium[pf2e.feats-srd.VXA50vhIRCBt4vvP]`|
|[Unbalancing Sweep](feats/class-12-pmz1itHp13JtcrjW.htm)|Balayage déséquilibrant|`@Compendium[pf2e.feats-srd.pmz1itHp13JtcrjW]`|
|[Unbreakable-er Goblin](feats/ancestry-13-99WRahrMC91D6MMe.htm)|Gobelin encore plus incassable|`@Compendium[pf2e.feats-srd.99WRahrMC91D6MMe]`|
|[Unburdened Iron](feats/ancestry-01-C1R4wd6G46CAVIn7.htm)|Fer allégé|`@Compendium[pf2e.feats-srd.C1R4wd6G46CAVIn7]`|
|[Uncanny Bombs](feats/class-12-9Ht1eyBHsB1swpeE.htm)|Bombes incroyables|`@Compendium[pf2e.feats-srd.9Ht1eyBHsB1swpeE]`|
|[Uncanny Dodge](feats/archetype-10-4Cc0gQauzUqcYdLw.htm)|Esquive instinctive|`@Compendium[pf2e.feats-srd.4Cc0gQauzUqcYdLw]`|
|[Uncanny Suction](feats/archetype-12-MD33E76f2olLnDZb.htm)|Succion étrange|`@Compendium[pf2e.feats-srd.MD33E76f2olLnDZb]`|
|[Unconventional Expertise](feats/ancestry-13-aqTlEPtd5gzs1Lxg.htm)|Expertise non conventionnelle|`@Compendium[pf2e.feats-srd.aqTlEPtd5gzs1Lxg]`|
|[Unconventional Weaponry](feats/ancestry-01-49mBVNF4SK6iYdJm.htm)|Armement non conventionnel|`@Compendium[pf2e.feats-srd.49mBVNF4SK6iYdJm]`|
|[Undaunted](feats/ancestry-01-1RygexXEjCKuR3Ps.htm)|Téméraire|`@Compendium[pf2e.feats-srd.1RygexXEjCKuR3Ps]`|
|[Undead Slayer](feats/ancestry-05-ZOJsLNziWQ7Ri3x9.htm)|Tueur de morts-vivants|`@Compendium[pf2e.feats-srd.ZOJsLNziWQ7Ri3x9]`|
|[Underground Network](feats/skill-02-gydOsP9VsdRw3Wg1.htm)|Réseau souterrain|`@Compendium[pf2e.feats-srd.gydOsP9VsdRw3Wg1]`|
|[Underhanded Assault](feats/class-02-eoDDjIGAA67Z7rQt.htm)|Assaut sournois|`@Compendium[pf2e.feats-srd.eoDDjIGAA67Z7rQt]`|
|[Underwater Marauder](feats/skill-01-iOY6YfGBaOvMNAor.htm)|Maraudeur aquatique|`@Compendium[pf2e.feats-srd.iOY6YfGBaOvMNAor]`|
|[Underworld Investigator](feats/class-01-pgIrr8xrlCXbRAeo.htm)|Enquêteur de la pègre|`@Compendium[pf2e.feats-srd.pgIrr8xrlCXbRAeo]`|
|[Undying Ferocity](feats/ancestry-09-zZOycr00XDVTKuXa.htm)|Férocité immortelle|`@Compendium[pf2e.feats-srd.zZOycr00XDVTKuXa]`|
|[Unexpected Shift](feats/ancestry-01-AgR1OPBHDvwV5wKD.htm)|Phasage inattendu|`@Compendium[pf2e.feats-srd.AgR1OPBHDvwV5wKD]`|
|[Unfazed Assessment](feats/archetype-08-ZyQYP7i26DWhMNux.htm)|Estimation imperturbable|`@Compendium[pf2e.feats-srd.ZyQYP7i26DWhMNux]`|
|[Unfettered Halfling](feats/ancestry-01-uuD8Z9jUG61GmenX.htm)|Halfelin affranchi|`@Compendium[pf2e.feats-srd.uuD8Z9jUG61GmenX]`|
|[Unhampered Passage](feats/ancestry-09-RFXzsfEgz7WbDCQO.htm)|Passage libre|`@Compendium[pf2e.feats-srd.RFXzsfEgz7WbDCQO]`|
|[Unified Theory](feats/skill-15-hrq3NOpS6148aVY1.htm)|Théorie unifiée|`@Compendium[pf2e.feats-srd.hrq3NOpS6148aVY1]`|
|[Unimpeded Step](feats/class-01-epfQ2PwNDLwyY31t.htm)|Pas sans entrave|`@Compendium[pf2e.feats-srd.epfQ2PwNDLwyY31t]`|
|[Universal Longevity](feats/ancestry-13-EiiCCJqWnN5RYMV4.htm)|Longévité universelle|`@Compendium[pf2e.feats-srd.EiiCCJqWnN5RYMV4]`|
|[Universal Versatility](feats/class-08-rpxFVUp8BuF31DYg.htm)|Polyvalence universelle|`@Compendium[pf2e.feats-srd.rpxFVUp8BuF31DYg]`|
|[Unmistakable Lore](feats/skill-02-XvX1EyxWbbBF32NV.htm)|Connaissances incontestables|`@Compendium[pf2e.feats-srd.XvX1EyxWbbBF32NV]`|
|[Unnerving Prowess](feats/archetype-06-V7bwuYADV8huWeF7.htm)|Prouesse désarçonnante|`@Compendium[pf2e.feats-srd.V7bwuYADV8huWeF7]`|
|[Unravel Mysteries](feats/archetype-08-2Tla5D1vpGioh42x.htm)|Résolution des mystères|`@Compendium[pf2e.feats-srd.2Tla5D1vpGioh42x]`|
|[Unrivaled Builder](feats/ancestry-13-wHxXVknciaD4X8Ch.htm)|Constructeur sans rival|`@Compendium[pf2e.feats-srd.wHxXVknciaD4X8Ch]`|
|[Unseat](feats/archetype-10-39RJF47FLYr5gZ8p.htm)|Désarçonner|`@Compendium[pf2e.feats-srd.39RJF47FLYr5gZ8p]`|
|[Unshakable Idealism](feats/archetype-08-VqzXfdbQQ2e8Hndr.htm)|Idéalisme Inébranlable|`@Compendium[pf2e.feats-srd.VqzXfdbQQ2e8Hndr]`|
|[Unstable Concoction](feats/class-10-tY3Zg0l14CdoKPpt.htm)|Concoction instable|`@Compendium[pf2e.feats-srd.tY3Zg0l14CdoKPpt]`|
|[Unstoppable Juggernaut](feats/class-20-YCqHaqn0TxdiGxiW.htm)|Juggernaut inarrêtable|`@Compendium[pf2e.feats-srd.YCqHaqn0TxdiGxiW]`|
|[Untrained Improvisation](feats/general-03-9jGaBxLUtevZYcZO.htm)|Improvisation inexpérimentée|`@Compendium[pf2e.feats-srd.9jGaBxLUtevZYcZO]`|
|[Unusual Composition](feats/class-10-tonJdHGheKZ16tMI.htm)|Composition insolite|`@Compendium[pf2e.feats-srd.tonJdHGheKZ16tMI]`|
|[Unwavering Mien](feats/ancestry-01-uW0VSyy75YrsvtWz.htm)|Volonté inébranlable|`@Compendium[pf2e.feats-srd.uW0VSyy75YrsvtWz]`|
|[Uzunjati Recollection](feats/archetype-10-UCen3Tq6BJlNI7rx.htm)|Mémoire Uzunjati|`@Compendium[pf2e.feats-srd.UCen3Tq6BJlNI7rx]`|
|[Uzunjati Storytelling](feats/archetype-06-my4uFR8cnDC4mJE2.htm)|Narration Uzunjati|`@Compendium[pf2e.feats-srd.my4uFR8cnDC4mJE2]`|
|[Vacate Vision](feats/archetype-06-2nSk6oOLBXCEbAhc.htm)|Vision vide|`@Compendium[pf2e.feats-srd.2nSk6oOLBXCEbAhc]`|
|[Vampire Lore](feats/ancestry-01-i0iyW1I7TylEgpV6.htm)|Connaissance des vampires|`@Compendium[pf2e.feats-srd.i0iyW1I7TylEgpV6]`|
|[Vandal](feats/ancestry-05-7ZwXUlnqjj9zEYbO.htm)|Vandale|`@Compendium[pf2e.feats-srd.7ZwXUlnqjj9zEYbO]`|
|[Vengeful Hatred](feats/ancestry-01-ngEd8PgsyAARuTQ9.htm)|Soif de vengeance|`@Compendium[pf2e.feats-srd.ngEd8PgsyAARuTQ9]`|
|[Vengeful Oath](feats/class-02-Fs9ZMHZNCBo7B5Zc.htm)|Serment de vengeance|`@Compendium[pf2e.feats-srd.Fs9ZMHZNCBo7B5Zc]`|
|[Vengeful Strike](feats/class-14-0kkiE74cyHyxdPe6.htm)|Frappe vindicative|`@Compendium[pf2e.feats-srd.0kkiE74cyHyxdPe6]`|
|[Verdant Metamorphosis](feats/class-14-orjVLLoziFTmf1mz.htm)|Métamorphose verdoyante|`@Compendium[pf2e.feats-srd.orjVLLoziFTmf1mz]`|
|[Verdant Presence](feats/class-20-Lbpm0OrQb4u2LVtj.htm)|Présence verdoyante|`@Compendium[pf2e.feats-srd.Lbpm0OrQb4u2LVtj]`|
|[Verdant Weapon](feats/class-01-qPFWEyihvbWsCcUv.htm)|Arme verdoyante|`@Compendium[pf2e.feats-srd.qPFWEyihvbWsCcUv]`|
|[Vernai Training](feats/archetype-12-cppQZwnuoXqX8mgF.htm)|Entraînement Vernai|`@Compendium[pf2e.feats-srd.cppQZwnuoXqX8mgF]`|
|[Versatile Font](feats/class-02-yZ7EcM9CLddZz8Hl.htm)|Source polyvalente|`@Compendium[pf2e.feats-srd.yZ7EcM9CLddZz8Hl]`|
|[Versatile Performance](feats/class-01-jBp91q4uzwd4FeSX.htm)|Polyvalence artistique|`@Compendium[pf2e.feats-srd.jBp91q4uzwd4FeSX]`|
|[Versatile Signature](feats/class-04-toFhkS9QbObxg6cp.htm)|Polyvalence des sorts emblématiques|`@Compendium[pf2e.feats-srd.toFhkS9QbObxg6cp]`|
|[Very Sneaky](feats/ancestry-01-oeGowXO2P6rHbZfY.htm)|Très furtif|`@Compendium[pf2e.feats-srd.oeGowXO2P6rHbZfY]`|
|[Very, Very Sneaky](feats/ancestry-13-u0DA0gkrZxXb0Hle.htm)|Très très furtif|`@Compendium[pf2e.feats-srd.u0DA0gkrZxXb0Hle]`|
|[Vexing Tumble](feats/class-06-9CXQhg4YprPhqzoL.htm)|Acrobaties vexantes|`@Compendium[pf2e.feats-srd.9CXQhg4YprPhqzoL]`|
|[Vibrant Display](feats/ancestry-01-txgCXcNMDe9kO7N8.htm)|Affichage dynamique|`@Compendium[pf2e.feats-srd.txgCXcNMDe9kO7N8]`|
|[Vicious Debilitations](feats/class-10-zt5CWn3UrPViwaB3.htm)|Handicaps cruels|`@Compendium[pf2e.feats-srd.zt5CWn3UrPViwaB3]`|
|[Vicious Incisors](feats/ancestry-01-QHwajD5n8P3oS9Wb.htm)|Incisives vicieuses|`@Compendium[pf2e.feats-srd.QHwajD5n8P3oS9Wb]`|
|[Vicious Vengeance](feats/class-01-KaCpXuYuho3nnDUy.htm)|Vengeance vicieuse|`@Compendium[pf2e.feats-srd.KaCpXuYuho3nnDUy]`|
|[Victorious Vigor](feats/ancestry-05-YeVDITJplindA27l.htm)|Vigueur dans la victoire|`@Compendium[pf2e.feats-srd.YeVDITJplindA27l]`|
|[Vigil's Walls Rise Anew](feats/archetype-10-Jnhkl2BOhxxRCTpp.htm)|Les murs de Vigie se dressent de nouveau|`@Compendium[pf2e.feats-srd.Jnhkl2BOhxxRCTpp]`|
|[Vigilante Dedication](feats/archetype-02-uqIiSDbv80TbRwTQ.htm)|Dévouement de Justicier|`@Compendium[pf2e.feats-srd.uqIiSDbv80TbRwTQ]`|
|[Vigorous Health](feats/ancestry-01-D2tyUKQiDSq3JO1Z.htm)|Santé vigoureuse|`@Compendium[pf2e.feats-srd.D2tyUKQiDSq3JO1Z]`|
|[Vigorous Inspiration](feats/class-14-4SKech3n0F38HrR5.htm)|Inspiration vigoureuse|`@Compendium[pf2e.feats-srd.4SKech3n0F38HrR5]`|
|[Viking Dedication](feats/archetype-02-JuEUNrvsxOYdXeYL.htm)|Dévouement de viking|`@Compendium[pf2e.feats-srd.JuEUNrvsxOYdXeYL]`|
|[Viking Shieldbearer](feats/ancestry-01-BoqMvGy1jXpsaBbo.htm)|Porteur de bouclier viking|`@Compendium[pf2e.feats-srd.BoqMvGy1jXpsaBbo]`|
|[Viking Weapon Familiarity](feats/archetype-04-j9Rp4fOZIxizyvYy.htm)|Familiarité avec les armes vikings|`@Compendium[pf2e.feats-srd.j9Rp4fOZIxizyvYy]`|
|[Viking Weapon Specialist](feats/archetype-06-rS8uNb0C5GBHnKHH.htm)|Spécialiste des armes viking|`@Compendium[pf2e.feats-srd.rS8uNb0C5GBHnKHH]`|
|[Vile Desecration](feats/class-01-PqImBJ2JrPo5oFxc.htm)|Vile profanation|`@Compendium[pf2e.feats-srd.PqImBJ2JrPo5oFxc]`|
|[Virtue-Forged Tattoos](feats/ancestry-09-Ezk3OgfPaRlEEyAD.htm)|Tatouages vertus-forgés|`@Compendium[pf2e.feats-srd.Ezk3OgfPaRlEEyAD]`|
|[Virtuosic Performer](feats/skill-01-lFwfUvH2708rl6i3.htm)|Artiste virtuose|`@Compendium[pf2e.feats-srd.lFwfUvH2708rl6i3]`|
|[Virtuosic Performer (Acting)](feats/skill-01-PmUK6NQlbSqZM3aY.htm)||`@Compendium[pf2e.feats-srd.PmUK6NQlbSqZM3aY]`|
|[Virtuosic Performer (Comedy)](feats/skill-01-Y5ASDpZ574Pn9CEx.htm)||`@Compendium[pf2e.feats-srd.Y5ASDpZ574Pn9CEx]`|
|[Virtuosic Performer (Dance)](feats/skill-01-TnF9hfOywfD8cE2v.htm)||`@Compendium[pf2e.feats-srd.TnF9hfOywfD8cE2v]`|
|[Virtuosic Performer (Keyboards)](feats/skill-01-noRMXU5xs133vEFB.htm)||`@Compendium[pf2e.feats-srd.noRMXU5xs133vEFB]`|
|[Virtuosic Performer (Oratory)](feats/skill-01-PpMybW2sh3yjfVJz.htm)||`@Compendium[pf2e.feats-srd.PpMybW2sh3yjfVJz]`|
|[Virtuosic Performer (Percussion)](feats/skill-01-nmwk8yShqoYPW1QG.htm)||`@Compendium[pf2e.feats-srd.nmwk8yShqoYPW1QG]`|
|[Virtuosic Performer (Singing)](feats/skill-01-FmfVehPphZWyrgX1.htm)||`@Compendium[pf2e.feats-srd.FmfVehPphZWyrgX1]`|
|[Virtuosic Performer (Strings)](feats/skill-01-sYYmBpRlRtWf5Tbh.htm)||`@Compendium[pf2e.feats-srd.sYYmBpRlRtWf5Tbh]`|
|[Virtuosic Performer (Winds)](feats/skill-01-zhIaJewomiJTkKFV.htm)||`@Compendium[pf2e.feats-srd.zhIaJewomiJTkKFV]`|
|[Vision Of Weakness](feats/class-04-ww5AM2yFs0lqQhmD.htm)|Vision de faiblesse|`@Compendium[pf2e.feats-srd.ww5AM2yFs0lqQhmD]`|
|[Vivacious Bravado](feats/class-08-2MHzEh1KUQEbhjUf.htm)|Bravade vivifiante|`@Compendium[pf2e.feats-srd.2MHzEh1KUQEbhjUf]`|
|[Vivacious Conduit](feats/ancestry-09-7vUOlVqjheZV0Nmc.htm)|Conduit vivace|`@Compendium[pf2e.feats-srd.7vUOlVqjheZV0Nmc]`|
|[Voice Of The Night](feats/ancestry-01-5VkVWZq7ZqH6RaAW.htm)|Voix de la nuit|`@Compendium[pf2e.feats-srd.5VkVWZq7ZqH6RaAW]`|
|[Walk The Plank](feats/archetype-08-TzBP8yiZQHNhei1V.htm)|Marcher sur la planche|`@Compendium[pf2e.feats-srd.TzBP8yiZQHNhei1V]`|
|[Wall Jump](feats/skill-07-X8iSUF1m0eezmrjs.htm)|Saut de mur|`@Compendium[pf2e.feats-srd.X8iSUF1m0eezmrjs]`|
|[Wall Run](feats/class-08-7IsHNime3WneCan6.htm)|Course à la verticale|`@Compendium[pf2e.feats-srd.7IsHNime3WneCan6]`|
|[Wandering Heart](feats/ancestry-13-ne7nVluvvVXMvuB1.htm)|Cœur vagabond|`@Compendium[pf2e.feats-srd.ne7nVluvvVXMvuB1]`|
|[Wandering Oasis](feats/class-12-BlfgmJHjDyTVGdPs.htm)|Oasis itinérante|`@Compendium[pf2e.feats-srd.BlfgmJHjDyTVGdPs]`|
|[Ward Against Corruption](feats/ancestry-05-NFsQOa3ynthYLVj6.htm)|Protection contre la corruption|`@Compendium[pf2e.feats-srd.NFsQOa3ynthYLVj6]`|
|[Ward Casting](feats/archetype-10-Qjn9ErWY6wuIK9z6.htm)|Incantation protégée|`@Compendium[pf2e.feats-srd.Qjn9ErWY6wuIK9z6]`|
|[Ward Medic](feats/skill-02-8c61nOIr5AM3KxZi.htm)|Infirmier|`@Compendium[pf2e.feats-srd.8c61nOIr5AM3KxZi]`|
|[Ward Mind](feats/archetype-16-S8Hda5OtajS9gpqM.htm)|Protection de l'esprit|`@Compendium[pf2e.feats-srd.S8Hda5OtajS9gpqM]`|
|[Warden's Boon](feats/class-08-oHra9QanDFpAZ4hh.htm)|Aide du protecteur|`@Compendium[pf2e.feats-srd.oHra9QanDFpAZ4hh]`|
|[Warden's Focus](feats/class-12-2aFtxqRPnC4OXUGC.htm)|Focalisation du gardien|`@Compendium[pf2e.feats-srd.2aFtxqRPnC4OXUGC]`|
|[Warden's Guidance](feats/class-14-6gtm0YCaDFpyVy35.htm)|Directive du protecteur|`@Compendium[pf2e.feats-srd.6gtm0YCaDFpyVy35]`|
|[Warden's Step](feats/class-10-oD4JyvTJj4kwe5vb.htm)|Foulée du protecteur|`@Compendium[pf2e.feats-srd.oD4JyvTJj4kwe5vb]`|
|[Warden's Wellspring](feats/class-18-wBCt1wcgrjduwHbi.htm)|Source du gardien|`@Compendium[pf2e.feats-srd.wBCt1wcgrjduwHbi]`|
|[Warding Light](feats/archetype-04-J1MURfqf0kbxrKG9.htm)|Lumière protectrice|`@Compendium[pf2e.feats-srd.J1MURfqf0kbxrKG9]`|
|[Warding Rune](feats/archetype-06-gZ4VdWKr3ag2BAvF.htm)|Rune de garde|`@Compendium[pf2e.feats-srd.gZ4VdWKr3ag2BAvF]`|
|[Warren Digger](feats/ancestry-13-8H72RC7QI1i8wjJ1.htm)|Creuseur de terrier|`@Compendium[pf2e.feats-srd.8H72RC7QI1i8wjJ1]`|
|[Warren Navigator](feats/ancestry-01-9Orkgjgfx8AILuqD.htm)|Navigateur de terrier|`@Compendium[pf2e.feats-srd.9Orkgjgfx8AILuqD]`|
|[Wary Disarmament](feats/skill-02-RLBYJiGMVkaGL5w9.htm)|Désarmement prudent|`@Compendium[pf2e.feats-srd.RLBYJiGMVkaGL5w9]`|
|[Wary Skulker](feats/ancestry-09-fu1cTh93zgGweduf.htm)|Rôdeur prudent|`@Compendium[pf2e.feats-srd.fu1cTh93zgGweduf]`|
|[Watch Your Back](feats/class-06-zZCyJhsaugHB6mZW.htm)|Fais attention|`@Compendium[pf2e.feats-srd.zZCyJhsaugHB6mZW]`|
|[Watch and Learn](feats/archetype-04-nU8W1QoA9hl3h6nR.htm)|Regarde et apprends|`@Compendium[pf2e.feats-srd.nU8W1QoA9hl3h6nR]`|
|[Watchful Halfling](feats/ancestry-01-zAZJpgeEf5TWvdq4.htm)|Halfelin vigilant|`@Compendium[pf2e.feats-srd.zAZJpgeEf5TWvdq4]`|
|[Water Sprint](feats/skill-07-MTO0spetPKyIa4sT.htm)|Courir sur l'eau|`@Compendium[pf2e.feats-srd.MTO0spetPKyIa4sT]`|
|[Water Step](feats/class-06-6gLWr3xghsHSFwxc.htm)|Foulée sur l'eau|`@Compendium[pf2e.feats-srd.6gLWr3xghsHSFwxc]`|
|[Wavetouched Paragon](feats/ancestry-05-AcgxKcA5I3dNTWLr.htm)|Parangon touché par la vague|`@Compendium[pf2e.feats-srd.AcgxKcA5I3dNTWLr]`|
|[Wayfinder Resonance Infiltrator](feats/archetype-08-o98nJriWE95xgweg.htm)|Infiltrateur de résonance de guide|`@Compendium[pf2e.feats-srd.o98nJriWE95xgweg]`|
|[Wayfinder Resonance Tinkerer](feats/archetype-04-UyMQ1X8KLSZvm7AT.htm)|Bricoleur de résonance de guide|`@Compendium[pf2e.feats-srd.UyMQ1X8KLSZvm7AT]`|
|[Weapon Improviser Dedication](feats/archetype-02-MndcBMz6I7e6SRqx.htm)|Dévouement d'improvisateur d'arme|`@Compendium[pf2e.feats-srd.MndcBMz6I7e6SRqx]`|
|[Weapon Proficiency](feats/general-01-x9wxQ61HNkAVbDHr.htm)|Maîtrise d'arme|`@Compendium[pf2e.feats-srd.x9wxQ61HNkAVbDHr]`|
|[Weapon Supremacy](feats/class-20-JEFPufbvaCeiA0Zo.htm)|Suprématie martiale|`@Compendium[pf2e.feats-srd.JEFPufbvaCeiA0Zo]`|
|[Weapon-Rune Shifter](feats/archetype-10-5nzeyuvzKL4T8eLt.htm)|Changerune d'arme|`@Compendium[pf2e.feats-srd.5nzeyuvzKL4T8eLt]`|
|[Weight of Guilt](feats/class-01-2c9awqDem5OLK47S.htm)|Poids de la culpabilité|`@Compendium[pf2e.feats-srd.2c9awqDem5OLK47S]`|
|[Well-Groomed](feats/ancestry-09-h11M3QrIHKLj2ezy.htm)|Bien entretenu|`@Compendium[pf2e.feats-srd.h11M3QrIHKLj2ezy]`|
|[Well-Met Traveler](feats/ancestry-01-9t4x8vRmQikEl4vP.htm)|Voyageur affable|`@Compendium[pf2e.feats-srd.9t4x8vRmQikEl4vP]`|
|[Well-Versed](feats/class-01-iX5HEqRImhKzfPR2.htm)|Connaisseur|`@Compendium[pf2e.feats-srd.iX5HEqRImhKzfPR2]`|
|[Westyr's Wayfinder Repository](feats/archetype-06-pkH4DPmMWcimMov7.htm)|Dépôt de guide de Westyr|`@Compendium[pf2e.feats-srd.pkH4DPmMWcimMov7]`|
|[Whirling Blade Stance](feats/class-14-IaiEZaA8erufMUCr.htm)|Posture de la lame tournoyante|`@Compendium[pf2e.feats-srd.IaiEZaA8erufMUCr]`|
|[Whirling Throw](feats/class-06-w0nSRBNwexM5Dh0D.htm)|Projection en rotation|`@Compendium[pf2e.feats-srd.w0nSRBNwexM5Dh0D]`|
|[Whirlwind Stance](feats/archetype-10-wZZyasfIqwiJBQAQ.htm)|Posture tourbillonnante|`@Compendium[pf2e.feats-srd.wZZyasfIqwiJBQAQ]`|
|[Whirlwind Strike](feats/class-14-AGydz5DKJ2KHSO4S.htm)|Frappe tourbillonnante (Barbare)|`@Compendium[pf2e.feats-srd.AGydz5DKJ2KHSO4S]`|
|[Whodunnit?](feats/class-08-yTUIiE9LXBZaA7aG.htm)|Quiafaitçà ?|`@Compendium[pf2e.feats-srd.yTUIiE9LXBZaA7aG]`|
|[Wholeness of Body](feats/class-04-UWG1USE0L2ZxEPiO.htm)|Plénitude physique|`@Compendium[pf2e.feats-srd.UWG1USE0L2ZxEPiO]`|
|[Widen Spell](feats/class-01-FoWO4RnHRwfEIC7Q.htm)|Sort élargi|`@Compendium[pf2e.feats-srd.FoWO4RnHRwfEIC7Q]`|
|[Widen the Gap](feats/archetype-10-mnhsG4l53YJkJIeY.htm)|Élargir la faille|`@Compendium[pf2e.feats-srd.mnhsG4l53YJkJIeY]`|
|[Wild Empathy](feats/class-02-54BhwBHyp2jp3e26.htm)|Empathie sauvage|`@Compendium[pf2e.feats-srd.54BhwBHyp2jp3e26]`|
|[Wild Shape](feats/class-01-VaIHQzOE5ibmbtqU.htm)|Morphologie sauvage|`@Compendium[pf2e.feats-srd.VaIHQzOE5ibmbtqU]`|
|[Wild Strider](feats/archetype-12-VN3OHDYcnLaw0nW1.htm)|Arpenteur des milieux naturels|`@Compendium[pf2e.feats-srd.VN3OHDYcnLaw0nW1]`|
|[Wild Winds Gust](feats/class-14-wKx7BWdQu5sEjL9j.htm)|Bourrasque des vents violents|`@Compendium[pf2e.feats-srd.wKx7BWdQu5sEjL9j]`|
|[Wild Winds Initiate](feats/class-08-CL9pFxxMXqzIyg4j.htm)|Initié des vents violents|`@Compendium[pf2e.feats-srd.CL9pFxxMXqzIyg4j]`|
|[Wildborn Adept](feats/ancestry-05-3NZSRyoulnzsi3sn.htm)|Adepte sauvage|`@Compendium[pf2e.feats-srd.3NZSRyoulnzsi3sn]`|
|[Wildborn Magic](feats/ancestry-01-af2fSePLvqMNNp0r.htm)|Magie sauvage|`@Compendium[pf2e.feats-srd.af2fSePLvqMNNp0r]`|
|[Wilderness Spotter](feats/skill-02-BV5jpSifVJsTwoO7.htm)|Observateur des étendues sauvages|`@Compendium[pf2e.feats-srd.BV5jpSifVJsTwoO7]`|
|[Wilderness Spotter (Aquatic)](feats/skill-02-gEkGIRX4bXbMHZ2W.htm)||`@Compendium[pf2e.feats-srd.gEkGIRX4bXbMHZ2W]`|
|[Wilderness Spotter (Arctic)](feats/skill-02-EATCTjJ2tGKFmlEY.htm)||`@Compendium[pf2e.feats-srd.EATCTjJ2tGKFmlEY]`|
|[Wilderness Spotter (Desert)](feats/skill-02-BWTE31PHCWc1txlw.htm)||`@Compendium[pf2e.feats-srd.BWTE31PHCWc1txlw]`|
|[Wilderness Spotter (Forest)](feats/skill-02-gjWpnUYOWvTuphET.htm)||`@Compendium[pf2e.feats-srd.gjWpnUYOWvTuphET]`|
|[Wilderness Spotter (Mountain)](feats/skill-02-pWYWXSk2ghyqRBTP.htm)||`@Compendium[pf2e.feats-srd.pWYWXSk2ghyqRBTP]`|
|[Wilderness Spotter (Plains)](feats/skill-02-cA8yxrKWvxk251rq.htm)||`@Compendium[pf2e.feats-srd.cA8yxrKWvxk251rq]`|
|[Wilderness Spotter (Sky)](feats/skill-02-yBZAmXr265EHPpj3.htm)||`@Compendium[pf2e.feats-srd.yBZAmXr265EHPpj3]`|
|[Wilderness Spotter (Swamp)](feats/skill-02-XWsLzllAjrW8LYT5.htm)||`@Compendium[pf2e.feats-srd.XWsLzllAjrW8LYT5]`|
|[Wilderness Spotter (Underground)](feats/skill-02-R07lTtA1VJzFvQ8p.htm)||`@Compendium[pf2e.feats-srd.R07lTtA1VJzFvQ8p]`|
|[Wind Caller](feats/class-08-Jj6sVfIX81tgvSY4.htm)|Celui qui appelle le vent|`@Compendium[pf2e.feats-srd.Jj6sVfIX81tgvSY4]`|
|[Wind Jump](feats/class-10-FWIQ3m5KZDWzDg47.htm)|Saut du vent|`@Compendium[pf2e.feats-srd.FWIQ3m5KZDWzDg47]`|
|[Winding Flow](feats/class-10-vXH0HWMHzevA1Wox.htm)|Fluidité détournée|`@Compendium[pf2e.feats-srd.vXH0HWMHzevA1Wox]`|
|[Wings Of The Dragon](feats/archetype-12-Uzz7oZit3FNO9FxO.htm)|Ailes du dragon|`@Compendium[pf2e.feats-srd.Uzz7oZit3FNO9FxO]`|
|[Wish Alchemy](feats/class-20-TsgW87kYudNr6Bfp.htm)|Alchimie des souhaits|`@Compendium[pf2e.feats-srd.TsgW87kYudNr6Bfp]`|
|[Witch Dedication](feats/archetype-02-y0vdu6DGhKKElmE6.htm)|Dévouement de Sorcière|`@Compendium[pf2e.feats-srd.y0vdu6DGhKKElmE6]`|
|[Witch Warden](feats/ancestry-01-u8gmBNHgb880vN3S.htm)|Garde-sorcière|`@Compendium[pf2e.feats-srd.u8gmBNHgb880vN3S]`|
|[Witch's Bottle](feats/class-08-oEjRfI4ATIFxDCzL.htm)|Bouteille de la sorcière|`@Compendium[pf2e.feats-srd.oEjRfI4ATIFxDCzL]`|
|[Witch's Charge](feats/class-06-52QyoGaysrfBOy5H.htm)|Fardeau de la sorcière|`@Compendium[pf2e.feats-srd.52QyoGaysrfBOy5H]`|
|[Witch's Communion](feats/class-10-4IeAAmx2vZEHmRLX.htm)|Communion de la sorcière|`@Compendium[pf2e.feats-srd.4IeAAmx2vZEHmRLX]`|
|[Witch's Hut](feats/class-20-Sq9muixjFptJO8Ae.htm)|Hutte de sorcière|`@Compendium[pf2e.feats-srd.Sq9muixjFptJO8Ae]`|
|[Wizard Dedication](feats/archetype-02-oQ6EZRFXbHWCx08C.htm)|Dévouement du magicien|`@Compendium[pf2e.feats-srd.oQ6EZRFXbHWCx08C]`|
|[Wolf Drag](feats/class-06-uJpghjJtNbqKUxRd.htm)|Traction du loup|`@Compendium[pf2e.feats-srd.uJpghjJtNbqKUxRd]`|
|[Wolf Stance](feats/class-01-AN9jY1JVcU20Qdw6.htm)|Posture du loup|`@Compendium[pf2e.feats-srd.AN9jY1JVcU20Qdw6]`|
|[Woodcraft](feats/ancestry-01-Y1bEisU8jVCsIYk3.htm)|Forestier|`@Compendium[pf2e.feats-srd.Y1bEisU8jVCsIYk3]`|
|[Woodland Stride](feats/class-04-lKgpe2JKaeLjgnYF.htm)|Déplacement facilité en forêt|`@Compendium[pf2e.feats-srd.lKgpe2JKaeLjgnYF]`|
|[Wortwitch](feats/class-01-zY6y802bOouMYYFV.htm)|Sorcière herbacée|`@Compendium[pf2e.feats-srd.zY6y802bOouMYYFV]`|
|[Wounded Rage](feats/class-04-CLKlavik0540j5bl.htm)|Rage blessée|`@Compendium[pf2e.feats-srd.CLKlavik0540j5bl]`|
|[Wyrmbane Aura](feats/class-14-5NyX1WnXEO40yEaT.htm)|Aura anti-draconique|`@Compendium[pf2e.feats-srd.5NyX1WnXEO40yEaT]`|
|[You're Next](feats/class-01-gjec0ts3wkFbjvHN.htm)|Tu es le suivant|`@Compendium[pf2e.feats-srd.gjec0ts3wkFbjvHN]`|
|[Zephyr Guard Dedication](feats/archetype-02-UyyrFtPWOo0qvXOv.htm)|Dévouement de Garde Zéphyr|`@Compendium[pf2e.feats-srd.UyyrFtPWOo0qvXOv]`|


## Effets des sorts

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Effect: Aberrant Blood Magic](spell-effects/01-UQ7vZgmfK0VSFS8A.htm)||`@Compendium[pf2e.spell-effects.UQ7vZgmfK0VSFS8A]`|
|[Effect: Aeon Stone (Black Pearl)](spell-effects/12-p2aGtovaY1feytws.htm)||`@Compendium[pf2e.spell-effects.p2aGtovaY1feytws]`|
|[Effect: Aeon Stone (Orange Prism) (Arcana)](spell-effects/16-yvabfuAO74pvH8hh.htm)||`@Compendium[pf2e.spell-effects.yvabfuAO74pvH8hh]`|
|[Effect: Aeon Stone (Orange Prism) (Nature)](spell-effects/16-etJW0w4CiSFgMrWP.htm)||`@Compendium[pf2e.spell-effects.etJW0w4CiSFgMrWP]`|
|[Effect: Aeon Stone (Orange Prism) (Occultism)](spell-effects/16-vOU4Yv2MyAfYBbmF.htm)||`@Compendium[pf2e.spell-effects.vOU4Yv2MyAfYBbmF]`|
|[Effect: Aeon Stone (Orange Prism) (Religion)](spell-effects/16-FbFl95WRpzrrijh3.htm)||`@Compendium[pf2e.spell-effects.FbFl95WRpzrrijh3]`|
|[Effect: Angelic Blood Magic](spell-effects/01-s1tulrmW6teTFjVd.htm)||`@Compendium[pf2e.spell-effects.s1tulrmW6teTFjVd]`|
|[Effect: Anklets of Alacrity](spell-effects/17-iK6JeCsZwm5Vakks.htm)||`@Compendium[pf2e.spell-effects.iK6JeCsZwm5Vakks]`|
|[Effect: Antidote (Greater)](spell-effects/10-QuZ5frBMJF3gi7RY.htm)|Effet : Antidote (Supérieur)|`@Compendium[pf2e.spell-effects.QuZ5frBMJF3gi7RY]`|
|[Effect: Antidote (Lesser)](spell-effects/01-7z1iY4AaNEAIKuAU.htm)|Effet : Antidote (inférieur)|`@Compendium[pf2e.spell-effects.7z1iY4AaNEAIKuAU]`|
|[Effect: Antidote (Major)](spell-effects/14-1mKjaWC65KWPuFR4.htm)|Effet : Antidote (Majeur)|`@Compendium[pf2e.spell-effects.1mKjaWC65KWPuFR4]`|
|[Effect: Antidote (Moderate)](spell-effects/06-mi4Md1fB2XThCand.htm)|Effet : Antidote (Modéré)|`@Compendium[pf2e.spell-effects.mi4Md1fB2XThCand]`|
|[Effect: Antiplague (Greater)](spell-effects/10-R106i7WCXvHLGMTu.htm)|Effet : Antimaladie (Supérieur)|`@Compendium[pf2e.spell-effects.R106i7WCXvHLGMTu]`|
|[Effect: Antiplague (Lesser)](spell-effects/01-kkDbalYEavzRpYlp.htm)|Effet : Antimaladie (Inférieur)|`@Compendium[pf2e.spell-effects.kkDbalYEavzRpYlp]`|
|[Effect: Antiplague (Major)](spell-effects/14-qVKrrKpTghgMIuGH.htm)|Effet : Antimaladie (Majeur)|`@Compendium[pf2e.spell-effects.qVKrrKpTghgMIuGH]`|
|[Effect: Antiplague (Moderate)](spell-effects/06-HeRHBo2NaKy5IxhU.htm)|Effet : Antimaladie (Modéré)|`@Compendium[pf2e.spell-effects.HeRHBo2NaKy5IxhU]`|
|[Effect: Applereed Mutagen (Greater)](spell-effects/18-tTBJ33UGtzXjWOJp.htm)||`@Compendium[pf2e.spell-effects.tTBJ33UGtzXjWOJp]`|
|[Effect: Applereed Mutagen (Lesser)](spell-effects/04-xVAdPzFaSvJXPMKv.htm)||`@Compendium[pf2e.spell-effects.xVAdPzFaSvJXPMKv]`|
|[Effect: Applereed Mutagen (Moderate)](spell-effects/12-fYjvLx9DHIdCHdDx.htm)||`@Compendium[pf2e.spell-effects.fYjvLx9DHIdCHdDx]`|
|[Effect: Barding of the Zephyr](spell-effects/10-P7Y7pO2ulZ5wBgxU.htm)||`@Compendium[pf2e.spell-effects.P7Y7pO2ulZ5wBgxU]`|
|[Effect: Belt of 5 Kings (Wearer)](spell-effects/09-cg5qyeMJUh6b4fta.htm)||`@Compendium[pf2e.spell-effects.cg5qyeMJUh6b4fta]`|
|[Effect: Belt of the Five Kings (Allies)](spell-effects/09-fbSFwwp60AuDDKpK.htm)||`@Compendium[pf2e.spell-effects.fbSFwwp60AuDDKpK]`|
|[Effect: Bestial Mutagen (Greater)](spell-effects/11-kwD0wuW5Ndkc9YXB.htm)||`@Compendium[pf2e.spell-effects.kwD0wuW5Ndkc9YXB]`|
|[Effect: Bestial Mutagen (Lesser)](spell-effects/01-fIpzDpuwLdIS4tW5.htm)|Effet : Mutagène bestial (Inférieur)|`@Compendium[pf2e.spell-effects.fIpzDpuwLdIS4tW5]`|
|[Effect: Bestial Mutagen (Major)](spell-effects/17-1ouUo8lLK6H79Rqh.htm)||`@Compendium[pf2e.spell-effects.1ouUo8lLK6H79Rqh]`|
|[Effect: Bestial Mutagen (Moderate)](spell-effects/03-xFQRiVU6h8EA6Lw9.htm)||`@Compendium[pf2e.spell-effects.xFQRiVU6h8EA6Lw9]`|
|[Effect: Bloodhound Mask (Greater)](spell-effects/11-MI5OCkF9IXmD2lPF.htm)||`@Compendium[pf2e.spell-effects.MI5OCkF9IXmD2lPF]`|
|[Effect: Bloodhound Mask (Lesser)](spell-effects/02-S4MZzALqFoXJsr6o.htm)|Effet : Masque du limier (Inférieur)|`@Compendium[pf2e.spell-effects.S4MZzALqFoXJsr6o]`|
|[Effect: Bloodhound Mask (Moderate)](spell-effects/06-wFF0SZs1Hcf87Kk1.htm)||`@Compendium[pf2e.spell-effects.wFF0SZs1Hcf87Kk1]`|
|[Effect: Boots of Elvenkind](spell-effects/05-88kqcDmsoAEddzUt.htm)||`@Compendium[pf2e.spell-effects.88kqcDmsoAEddzUt]`|
|[Effect: Boulderhead Bock](spell-effects/02-2C1HuKDQDGFZuv7l.htm)|Effet : Bock têtederoc|`@Compendium[pf2e.spell-effects.2C1HuKDQDGFZuv7l]`|
|[Effect: Bracelets of Dashing](spell-effects/03-lLP56tbG689TNKW5.htm)||`@Compendium[pf2e.spell-effects.lLP56tbG689TNKW5]`|
|[Effect: Bracers of Missile Deflection](spell-effects/03-PeiuJ951kkBPTCSM.htm)||`@Compendium[pf2e.spell-effects.PeiuJ951kkBPTCSM]`|
|[Effect: Bravo's Brew (Greater)](spell-effects/15-q1EhQ716bPSgJVnC.htm)||`@Compendium[pf2e.spell-effects.q1EhQ716bPSgJVnC]`|
|[Effect: Bravo's Brew (Lesser)](spell-effects/02-eh7EqmDBDW30ShCu.htm)|Effet : Breuvage de bravoure (Inférieur)|`@Compendium[pf2e.spell-effects.eh7EqmDBDW30ShCu]`|
|[Effect: Bravo's Brew (Moderate)](spell-effects/10-wTZnKkT0K4Tdy8mD.htm)||`@Compendium[pf2e.spell-effects.wTZnKkT0K4Tdy8mD]`|
|[Effect: Breastplate of Command](spell-effects/10-G0lG7IIZnCZtYi6v.htm)||`@Compendium[pf2e.spell-effects.G0lG7IIZnCZtYi6v]`|
|[Effect: Bronze Bull Pendant](spell-effects/02-Cxa7MdgMCUoMqbKm.htm)|Effet : Pendentif de taureau de bronze|`@Compendium[pf2e.spell-effects.Cxa7MdgMCUoMqbKm]`|
|[Effect: Celestial Armor](spell-effects/13-gDefAEEMXVVZgqXH.htm)||`@Compendium[pf2e.spell-effects.gDefAEEMXVVZgqXH]`|
|[Effect: Cheetah's Elixir (Greater)](spell-effects/09-PeuUz7JaabCgl6Yh.htm)||`@Compendium[pf2e.spell-effects.PeuUz7JaabCgl6Yh]`|
|[Effect: Cheetah's Elixir (Lesser)](spell-effects/01-lNWACCNe9RYgaFxb.htm)|Effet : Élixir du Guépard (Inférieur)|`@Compendium[pf2e.spell-effects.lNWACCNe9RYgaFxb]`|
|[Effect: Cheetah's Elixir (Moderate)](spell-effects/05-j9zVZwRBVAcnpEkE.htm)||`@Compendium[pf2e.spell-effects.j9zVZwRBVAcnpEkE]`|
|[Effect: Clandestine Cloak](spell-effects/06-Uadsb25G18pKdZ2e.htm)||`@Compendium[pf2e.spell-effects.Uadsb25G18pKdZ2e]`|
|[Effect: Clandestine Cloak (Greater)](spell-effects/10-uXEp1rPU5fY4OiBf.htm)||`@Compendium[pf2e.spell-effects.uXEp1rPU5fY4OiBf]`|
|[Effect: Cloak of Elvenkind](spell-effects/07-eeGWTG9ZAha4IIOY.htm)||`@Compendium[pf2e.spell-effects.eeGWTG9ZAha4IIOY]`|
|[Effect: Cloak of Elvenkind (Greater)](spell-effects/12-viCX9fZzTWGuoO85.htm)||`@Compendium[pf2e.spell-effects.viCX9fZzTWGuoO85]`|
|[Effect: Cloak of the Bat](spell-effects/10-ioGzmVSmMGXWWBYb.htm)||`@Compendium[pf2e.spell-effects.ioGzmVSmMGXWWBYb]`|
|[Effect: Cognitive Mutagen (Greater)](spell-effects/11-qit1mLbJUyRTYcPU.htm)||`@Compendium[pf2e.spell-effects.qit1mLbJUyRTYcPU]`|
|[Effect: Cognitive Mutagen (Lesser)](spell-effects/01-jaBMZKdoywOTrQvP.htm)|Effet : Mutagène cognitif (Inférieur)|`@Compendium[pf2e.spell-effects.jaBMZKdoywOTrQvP]`|
|[Effect: Cognitive Mutagen (Major)](spell-effects/17-RT1BxXrbbGgk40Ti.htm)||`@Compendium[pf2e.spell-effects.RT1BxXrbbGgk40Ti]`|
|[Effect: Cognitive Mutagen (Moderate)](spell-effects/03-ztxW3lBPRcesF7wK.htm)||`@Compendium[pf2e.spell-effects.ztxW3lBPRcesF7wK]`|
|[Effect: Darkvision Elixir](spell-effects/01-a3hqeAXpmMZgMWuB.htm)|Effet : Élixir de vision dans le noir|`@Compendium[pf2e.spell-effects.a3hqeAXpmMZgMWuB]`|
|[Effect: Darkvision Elixir (Greater)](spell-effects/08-7vCenP9j6FuHRv5C.htm)||`@Compendium[pf2e.spell-effects.7vCenP9j6FuHRv5C]`|
|[Effect: Darkvision Elixir (Lesser)](spell-effects/02-7UL8belWmo7U5YGM.htm)||`@Compendium[pf2e.spell-effects.7UL8belWmo7U5YGM]`|
|[Effect: Darkvision Elixir (Moderate)](spell-effects/04-bcxVvIbuZWOvsKcA.htm)||`@Compendium[pf2e.spell-effects.bcxVvIbuZWOvsKcA]`|
|[Effect: Demonic Blood Magic (Self)](spell-effects/01-aKRo5TIhUtu0kyEr.htm)||`@Compendium[pf2e.spell-effects.aKRo5TIhUtu0kyEr]`|
|[Effect: Demonic Blood Magic (Target)](spell-effects/01-yfbP64r4a9e5oyli.htm)||`@Compendium[pf2e.spell-effects.yfbP64r4a9e5oyli]`|
|[Effect: Diabolic Blood Magic (Self)](spell-effects/01-n1vhmOd7aNiuR3nk.htm)||`@Compendium[pf2e.spell-effects.n1vhmOd7aNiuR3nk]`|
|[Effect: Diplomat's Badge](spell-effects/05-pAMyEbJzWBoYoGhs.htm)||`@Compendium[pf2e.spell-effects.pAMyEbJzWBoYoGhs]`|
|[Effect: Draconic Blood Magic](spell-effects/01-FNTTeJHiK6iOjrSq.htm)||`@Compendium[pf2e.spell-effects.FNTTeJHiK6iOjrSq]`|
|[Effect: Dragon Turtle Scale](spell-effects/04-4tepFOJLhZSelPoa.htm)||`@Compendium[pf2e.spell-effects.4tepFOJLhZSelPoa]`|
|[Effect: Drakeheart Mutagen (Greater)](spell-effects/11-qwoLV4awdezlEJ60.htm)||`@Compendium[pf2e.spell-effects.qwoLV4awdezlEJ60]`|
|[Effect: Drakeheart Mutagen (Lesser)](spell-effects/01-GBBjw61g4ekJymT0.htm)|Effet : Mutagène cœur de drake (Inférieur)|`@Compendium[pf2e.spell-effects.GBBjw61g4ekJymT0]`|
|[Effect: Drakeheart Mutagen (Major)](spell-effects/17-vFOr2JAJxiVvvn2E.htm)||`@Compendium[pf2e.spell-effects.vFOr2JAJxiVvvn2E]`|
|[Effect: Drakeheart Mutagen (Moderate)](spell-effects/03-BV8RPntjc9FUzD3g.htm)||`@Compendium[pf2e.spell-effects.BV8RPntjc9FUzD3g]`|
|[Effect: Dueling Cape](spell-effects/00-7dLsA9PAb5ij7Bc6.htm)|Effet : Cape de duel|`@Compendium[pf2e.spell-effects.7dLsA9PAb5ij7Bc6]`|
|[Effect: Dueling Parry](spell-effects/01-1nCwQErK6hpkNvfw.htm)|Effet : Parade en duel|`@Compendium[pf2e.spell-effects.1nCwQErK6hpkNvfw]`|
|[Effect: Eagle Eye Elixir (Greater)](spell-effects/10-kgotU0sFmtAHYySB.htm)||`@Compendium[pf2e.spell-effects.kgotU0sFmtAHYySB]`|
|[Effect: Eagle Eye Elixir (Lesser)](spell-effects/01-VCypzSu659eC6jNi.htm)|Effet : Élixir d'œil du faucon (Inférieur)|`@Compendium[pf2e.spell-effects.VCypzSu659eC6jNi]`|
|[Effect: Eagle Eye Elixir (Major)](spell-effects/16-wyLEew86nhNUXASu.htm)||`@Compendium[pf2e.spell-effects.wyLEew86nhNUXASu]`|
|[Effect: Eagle Eye Elixir (Moderate)](spell-effects/05-Wa4317cqU4lJ8vAQ.htm)||`@Compendium[pf2e.spell-effects.Wa4317cqU4lJ8vAQ]`|
|[Effect: Earplugs](spell-effects/00-NE7Fm5YnUhD4ySX3.htm)|Bouchons d'oreilles|`@Compendium[pf2e.spell-effects.NE7Fm5YnUhD4ySX3]`|
|[Effect: Elemental Blood Magic (Self)](spell-effects/01-3gGBZHcUFsHLJeQH.htm)||`@Compendium[pf2e.spell-effects.3gGBZHcUFsHLJeQH]`|
|[Effect: Elixir of Life (Greater)](spell-effects/13-Z9oPh462q82IYIZ6.htm)||`@Compendium[pf2e.spell-effects.Z9oPh462q82IYIZ6]`|
|[Effect: Elixir of Life (Lesser)](spell-effects/05-EpB7yJPEuG6ez4z3.htm)||`@Compendium[pf2e.spell-effects.EpB7yJPEuG6ez4z3]`|
|[Effect: Elixir of Life (Major)](spell-effects/15-PpLxndUSgzgs6dd0.htm)||`@Compendium[pf2e.spell-effects.PpLxndUSgzgs6dd0]`|
|[Effect: Elixir of Life (Minor)](spell-effects/01-lPRuIRbu0rHBkoKY.htm)|Effet : Élixir de vie (Mineur)|`@Compendium[pf2e.spell-effects.lPRuIRbu0rHBkoKY]`|
|[Effect: Elixir of Life (Moderate)](spell-effects/09-Yxssrnh9UZJAM0V7.htm)||`@Compendium[pf2e.spell-effects.Yxssrnh9UZJAM0V7]`|
|[Effect: Elixir of Life (True)](spell-effects/19-9MeHc072G4L8AJkp.htm)||`@Compendium[pf2e.spell-effects.9MeHc072G4L8AJkp]`|
|[Effect: Euryale (Curse) Card](spell-effects/00-8ersuvNJXX00XaIQ.htm)|Effet : Carte Euryale (malédiction)|`@Compendium[pf2e.spell-effects.8ersuvNJXX00XaIQ]`|
|[Effect: Eye of the Arclords](spell-effects/01-5IGz4iheaiUWm5KR.htm)||`@Compendium[pf2e.spell-effects.5IGz4iheaiUWm5KR]`|
|[Effect: Fey Blood Magic](spell-effects/01-rJpkKaPRGaH0pLse.htm)||`@Compendium[pf2e.spell-effects.rJpkKaPRGaH0pLse]`|
|[Effect: Genie Blood Magic (Self)](spell-effects/01-9AUcoY48H5LrVZiF.htm)||`@Compendium[pf2e.spell-effects.9AUcoY48H5LrVZiF]`|
|[Effect: Genie Blood Magic (Target)](spell-effects/01-KVbS7AbhQdeuA0J6.htm)||`@Compendium[pf2e.spell-effects.KVbS7AbhQdeuA0J6]`|
|[Effect: Glaive of the Artist](spell-effects/14-EpNflrkmWzQ0lEb4.htm)||`@Compendium[pf2e.spell-effects.EpNflrkmWzQ0lEb4]`|
|[Effect: Glamorous Buckler](spell-effects/02-iEkH8BKLMUa2wxLX.htm)|Effet : Targe étincelante|`@Compendium[pf2e.spell-effects.iEkH8BKLMUa2wxLX]`|
|[Effect: Golden Legion Epaulet](spell-effects/03-bP40jr6wE6MCsRvY.htm)|Effet : Épaulette de la légion dorée|`@Compendium[pf2e.spell-effects.bP40jr6wE6MCsRvY]`|
|[Effect: Hag Blood Magic](spell-effects/01-6fb15XuSV4TNuVAT.htm)||`@Compendium[pf2e.spell-effects.6fb15XuSV4TNuVAT]`|
|[Effect: Imperial Blood Magic](spell-effects/01-vguxP8ukwVTWWWaA.htm)||`@Compendium[pf2e.spell-effects.vguxP8ukwVTWWWaA]`|
|[Effect: Improvised Weapon](spell-effects/00-eSIYyxi6uTKiP6W5.htm)|Effet : arme improvisée|`@Compendium[pf2e.spell-effects.eSIYyxi6uTKiP6W5]`|
|[Effect: Inspiring Spotlight](spell-effects/09-E4B02mJmNexQLa8F.htm)||`@Compendium[pf2e.spell-effects.E4B02mJmNexQLa8F]`|
|[Effect: Juggernaut Mutagen (Greater)](spell-effects/11-xLilBqqf34ZJYO9i.htm)||`@Compendium[pf2e.spell-effects.xLilBqqf34ZJYO9i]`|
|[Effect: Juggernaut Mutagen (Lesser)](spell-effects/01-1l139A2Qik4lBHKO.htm)|Effet : Mutagène de Juggernaut (Inférieur)|`@Compendium[pf2e.spell-effects.1l139A2Qik4lBHKO]`|
|[Effect: Juggernaut Mutagen (Major)](spell-effects/17-2PNo8u4wxSbz5WEs.htm)||`@Compendium[pf2e.spell-effects.2PNo8u4wxSbz5WEs]`|
|[Effect: Juggernaut Mutagen (Moderate)](spell-effects/03-fUrZ4xcMJz0CfTyG.htm)||`@Compendium[pf2e.spell-effects.fUrZ4xcMJz0CfTyG]`|
|[Effect: Lastwall Soup](spell-effects/01-6A8jsLR7upLGuRiv.htm)|Effet : Soupe de Dernier-Rempart|`@Compendium[pf2e.spell-effects.6A8jsLR7upLGuRiv]`|
|[Effect: Lover's Gloves](spell-effects/08-3O5lvuX4VHqtpCkU.htm)||`@Compendium[pf2e.spell-effects.3O5lvuX4VHqtpCkU]`|
|[Effect: Nymph Blood Magic (Self)](spell-effects/01-SVGW8CLKwixFlnTv.htm)||`@Compendium[pf2e.spell-effects.SVGW8CLKwixFlnTv]`|
|[Effect: Nymph Blood Magic (Target)](spell-effects/01-ruRAfGJnik7lRavk.htm)||`@Compendium[pf2e.spell-effects.ruRAfGJnik7lRavk]`|
|[Effect: Panache](spell-effects/01-uBJsxCzNhje8m8jj.htm)|Effet : Panache|`@Compendium[pf2e.spell-effects.uBJsxCzNhje8m8jj]`|
|[Effect: Potion of Flying (Greater)](spell-effects/15-Zdh2uO1vVYJmaqld.htm)||`@Compendium[pf2e.spell-effects.Zdh2uO1vVYJmaqld]`|
|[Effect: Potion of Flying (Standard)](spell-effects/08-Mf9EBLhYmZerf0nS.htm)||`@Compendium[pf2e.spell-effects.Mf9EBLhYmZerf0nS]`|
|[Effect: Potion of Swimming (Greater)](spell-effects/11-ModBoFdCi7YQU4gP.htm)||`@Compendium[pf2e.spell-effects.ModBoFdCi7YQU4gP]`|
|[Effect: Potion of Swimming (Moderate)](spell-effects/06-W3xQBLj5hLOtb6Tj.htm)||`@Compendium[pf2e.spell-effects.W3xQBLj5hLOtb6Tj]`|
|[Effect: Psychopomp Blood Magic (Self)](spell-effects/01-7BFd8A9HFrmg6vwL.htm)||`@Compendium[pf2e.spell-effects.7BFd8A9HFrmg6vwL]`|
|[Effect: Quicksilver Mutagen (Greater)](spell-effects/11-2Bds6d4UGQZqYSZM.htm)||`@Compendium[pf2e.spell-effects.2Bds6d4UGQZqYSZM]`|
|[Effect: Quicksilver Mutagen (Lesser)](spell-effects/01-6PNLBIdlqqWNCFMy.htm)|Effet : Mutagène vif-argent|`@Compendium[pf2e.spell-effects.6PNLBIdlqqWNCFMy]`|
|[Effect: Quicksilver Mutagen (Major)](spell-effects/17-988f6NpOo4YzFzIr.htm)||`@Compendium[pf2e.spell-effects.988f6NpOo4YzFzIr]`|
|[Effect: Quicksilver Mutagen (Moderate)](spell-effects/03-VPtsrpbP0AE642al.htm)||`@Compendium[pf2e.spell-effects.VPtsrpbP0AE642al]`|
|[Effect: Rage](spell-effects/01-z3uyCMBddrPK5umr.htm)||`@Compendium[pf2e.spell-effects.z3uyCMBddrPK5umr]`|
|[Effect: Salve of Slipperiness](spell-effects/05-MCny5ohCGf09a7Wl.htm)||`@Compendium[pf2e.spell-effects.MCny5ohCGf09a7Wl]`|
|[Effect: Sea Touch Elixir (Greater)](spell-effects/05-RRusoN3HEGnDO1Dg.htm)||`@Compendium[pf2e.spell-effects.RRusoN3HEGnDO1Dg]`|
|[Effect: Sea Touch Elixir (Lesser)](spell-effects/05-thOpQunbQr77XWdF.htm)||`@Compendium[pf2e.spell-effects.thOpQunbQr77XWdF]`|
|[Effect: Sea Touch Elixir (Moderate)](spell-effects/12-9keegq0GdS1eSrNr.htm)||`@Compendium[pf2e.spell-effects.9keegq0GdS1eSrNr]`|
|[Effect: Serene Mutagen (Greater)](spell-effects/11-jw6Tr9FbErjLAFLQ.htm)||`@Compendium[pf2e.spell-effects.jw6Tr9FbErjLAFLQ]`|
|[Effect: Serene Mutagen (Lesser)](spell-effects/01-5xgapIXn5DwbXHKh.htm)|Effet : Mutagène de sérénité (Inférieur)|`@Compendium[pf2e.spell-effects.5xgapIXn5DwbXHKh]`|
|[Effect: Serene Mutagen (Major)](spell-effects/17-t7VUJHSUT6bkVUjg.htm)||`@Compendium[pf2e.spell-effects.t7VUJHSUT6bkVUjg]`|
|[Effect: Serene Mutagen (Moderate)](spell-effects/03-yrbz0rZzp8aZEqbv.htm)||`@Compendium[pf2e.spell-effects.yrbz0rZzp8aZEqbv]`|
|[Effect: Shadow Blood Magic (Perception)](spell-effects/01-Nv70aqcQgCBpDYp8.htm)||`@Compendium[pf2e.spell-effects.Nv70aqcQgCBpDYp8]`|
|[Effect: Shadow Blood Magic (Stealth)](spell-effects/01-OqH6IaeOwRWkGPrk.htm)||`@Compendium[pf2e.spell-effects.OqH6IaeOwRWkGPrk]`|
|[Effect: Shield of the Unified Legion](spell-effects/20-ah41XCrV4LFsVyzl.htm)||`@Compendium[pf2e.spell-effects.ah41XCrV4LFsVyzl]`|
|[Effect: Shielding Salve](spell-effects/01-16tOZk4qy329s2aK.htm)||`@Compendium[pf2e.spell-effects.16tOZk4qy329s2aK]`|
|[Effect: Shrinking Potion (Greater)](spell-effects/08-zd85Ny1RS46OL0TD.htm)||`@Compendium[pf2e.spell-effects.zd85Ny1RS46OL0TD]`|
|[Effect: Silvertongue Mutagen (Greater)](spell-effects/11-oAewXfq9c0ecaSfw.htm)||`@Compendium[pf2e.spell-effects.oAewXfq9c0ecaSfw]`|
|[Effect: Silvertongue Mutagen (Lesser)](spell-effects/01-dpIrjd1UPY7EnWUD.htm)|Effet : Mutagène de langue dorée (Inférieur)|`@Compendium[pf2e.spell-effects.dpIrjd1UPY7EnWUD]`|
|[Effect: Silvertongue Mutagen (Major)](spell-effects/17-9FfFhu2kl2wMTsiI.htm)||`@Compendium[pf2e.spell-effects.9FfFhu2kl2wMTsiI]`|
|[Effect: Silvertongue Mutagen (Moderate)](spell-effects/03-v5Ht1V4MZvRKRBjL.htm)||`@Compendium[pf2e.spell-effects.v5Ht1V4MZvRKRBjL]`|
|[Effect: Sixfingers Elixir (Greater)](spell-effects/17-TkRuKKYyPHTGPfgf.htm)||`@Compendium[pf2e.spell-effects.TkRuKKYyPHTGPfgf]`|
|[Effect: Sixfingers Elixir (Lesser)](spell-effects/05-XrlChFETfe8avLsX.htm)||`@Compendium[pf2e.spell-effects.XrlChFETfe8avLsX]`|
|[Effect: Sixfingers Elixir (Moderate)](spell-effects/11-qzRcSQ0HTTp58hV2.htm)||`@Compendium[pf2e.spell-effects.qzRcSQ0HTTp58hV2]`|
|[Effect: Skeptic's Elixir (Greater)](spell-effects/11-TsWUTODTVi487SEz.htm)||`@Compendium[pf2e.spell-effects.TsWUTODTVi487SEz]`|
|[Effect: Skeptic's Elixir (Lesser)](spell-effects/01-5Gof60StUppR2Xn9.htm)||`@Compendium[pf2e.spell-effects.5Gof60StUppR2Xn9]`|
|[Effect: Skeptic's Elixir (Moderate)](spell-effects/06-mG6S6zm6hxaF7Tla.htm)||`@Compendium[pf2e.spell-effects.mG6S6zm6hxaF7Tla]`|
|[Effect: Skinstitch Salve](spell-effects/07-zlSNbMDIlTOpcO8R.htm)||`@Compendium[pf2e.spell-effects.zlSNbMDIlTOpcO8R]`|
|[Effect: Slippers of Spider Climbing](spell-effects/07-W3BCLbX6j1IqL0uB.htm)||`@Compendium[pf2e.spell-effects.W3BCLbX6j1IqL0uB]`|
|[Effect: Spellguard Blade](spell-effects/07-zqKzWGLODgIvtiKf.htm)||`@Compendium[pf2e.spell-effects.zqKzWGLODgIvtiKf]`|
|[Effect: Spellguard Shield](spell-effects/06-lBMhT2W2raYMa8JS.htm)||`@Compendium[pf2e.spell-effects.lBMhT2W2raYMa8JS]`|
|[Effect: Stalwart's ring](spell-effects/05-5uK3fmGlfJrbWQz4.htm)||`@Compendium[pf2e.spell-effects.5uK3fmGlfJrbWQz4]`|
|[Effect: Stole of Civility](spell-effects/09-i0tm2ZHekp7rGGR3.htm)||`@Compendium[pf2e.spell-effects.i0tm2ZHekp7rGGR3]`|
|[Effect: Stone Body Mutagen (Greater)](spell-effects/15-b9DTIJyBT8kvIBpj.htm)||`@Compendium[pf2e.spell-effects.b9DTIJyBT8kvIBpj]`|
|[Effect: Stone Body Mutagen (Lesser)](spell-effects/05-PEPOd38VfVzQMKG5.htm)||`@Compendium[pf2e.spell-effects.PEPOd38VfVzQMKG5]`|
|[Effect: Stone Body Mutagen (Moderate)](spell-effects/12-1xHHvQlW4pRR89qj.htm)||`@Compendium[pf2e.spell-effects.1xHHvQlW4pRR89qj]`|
|[Effect: Stone Fist Elixir](spell-effects/04-AMhUb42NAJ1aisZp.htm)||`@Compendium[pf2e.spell-effects.AMhUb42NAJ1aisZp]`|
|[Effect: Sun Orchid Poultice](spell-effects/18-kwOtHtmlH69ctK0O.htm)||`@Compendium[pf2e.spell-effects.kwOtHtmlH69ctK0O]`|
|[Effect: Throne Card](spell-effects/00-Zb8RYgmzCI6fQE0o.htm)|Effet: Carte Trône|`@Compendium[pf2e.spell-effects.Zb8RYgmzCI6fQE0o]`|
|[Effect: Thurible of Revelation](spell-effects/03-lO95TwgihBdTilAB.htm)||`@Compendium[pf2e.spell-effects.lO95TwgihBdTilAB]`|
|[Effect: Thurible of Revelation (Greater)](spell-effects/18-QapoFh0tbUgMwSIB.htm)||`@Compendium[pf2e.spell-effects.QapoFh0tbUgMwSIB]`|
|[Effect: Thurible of Revelation (Moderate)](spell-effects/10-UlalLihKzDxcOdXL.htm)||`@Compendium[pf2e.spell-effects.UlalLihKzDxcOdXL]`|
|[Effect: Treat Wounds Immunity](spell-effects/01-Lb4q2bBAgxamtix5.htm)||`@Compendium[pf2e.spell-effects.Lb4q2bBAgxamtix5]`|
|[Effect: Triton's Conch](spell-effects/09-9PASRixhNM0ogqmG.htm)||`@Compendium[pf2e.spell-effects.9PASRixhNM0ogqmG]`|
|[Effect: Vaultbreaker's Harness](spell-effects/06-7MgpgF8tOXOiDEwv.htm)||`@Compendium[pf2e.spell-effects.7MgpgF8tOXOiDEwv]`|
|[Effect: Vermin Repellent Agent (Greater)](spell-effects/12-VrYfR2WuyA15zFhq.htm)||`@Compendium[pf2e.spell-effects.VrYfR2WuyA15zFhq]`|
|[Effect: Vermin Repellent Agent (Lesser)](spell-effects/03-OAN5Fj21PJPhIqRU.htm)||`@Compendium[pf2e.spell-effects.OAN5Fj21PJPhIqRU]`|
|[Effect: Vermin Repellent Agent (Major)](spell-effects/16-e6dXfbKzv5sNr1zh.htm)||`@Compendium[pf2e.spell-effects.e6dXfbKzv5sNr1zh]`|
|[Effect: Vermin Repellent Agent (Moderate)](spell-effects/08-Hnt3Trd7TiFICB06.htm)||`@Compendium[pf2e.spell-effects.Hnt3Trd7TiFICB06]`|
|[Effect: Winged Boots](spell-effects/10-1S51uIRb9bnZtpFU.htm)||`@Compendium[pf2e.spell-effects.1S51uIRb9bnZtpFU]`|
|[Spell Effect: Bless](spell-effects/Gqy7K6FnbLtwGpud.htm)|Effet de sort : Bénédiction|`@Compendium[pf2e.spell-effects.Gqy7K6FnbLtwGpud]`|
|[Spell Effect: Enlarge](spell-effects/02-sPCWrhUHqlbGhYSD.htm)||`@Compendium[pf2e.spell-effects.sPCWrhUHqlbGhYSD]`|
|[Spell Effect: Enlarge (Heightened 4th)](spell-effects/04-41WThj17MZBXTO2X.htm)||`@Compendium[pf2e.spell-effects.41WThj17MZBXTO2X]`|
|[Spell Effect: Guidance](spell-effects/01-3qHKBDF7lrHw8jFK.htm)|Effet : Assistance divine|`@Compendium[pf2e.spell-effects.3qHKBDF7lrHw8jFK]`|
|[Spell Effect: Heroism](spell-effects/03-l9HRQggofFGIxEse.htm)||`@Compendium[pf2e.spell-effects.l9HRQggofFGIxEse]`|
|[Spell Effect: Inspire Courage](spell-effects/01-beReeFroAx24hj83.htm)|Effet : Inspiration Vaillante|`@Compendium[pf2e.spell-effects.beReeFroAx24hj83]`|
|[Spell Effect: Mage Armor](spell-effects/01-qkwb5DD3zmKwvbk0.htm)|Effet : Armure du mage|`@Compendium[pf2e.spell-effects.qkwb5DD3zmKwvbk0]`|
|[Spell Effect: Magic Weapon](spell-effects/01-GnWkI3T3LYRlm3X8.htm)||`@Compendium[pf2e.spell-effects.GnWkI3T3LYRlm3X8]`|
|[Spell Effect: Protective Ward](spell-effects/01-dWbg2gACxMkSnZag.htm)||`@Compendium[pf2e.spell-effects.dWbg2gACxMkSnZag]`|
|[Spell Effect: Shield](spell-effects/01-Jemq5UknGdMO7b73.htm)|Effet : Bouclier|`@Compendium[pf2e.spell-effects.Jemq5UknGdMO7b73]`|
|[Spell Effect: Shrink](spell-effects/02-LXf1Cqi1zyo4DaLv.htm)||`@Compendium[pf2e.spell-effects.LXf1Cqi1zyo4DaLv]`|
|[Stance: Clinging Shadows Stance](spell-effects/00-uFYvW3kFP9iyNfVX.htm)||`@Compendium[pf2e.spell-effects.uFYvW3kFP9iyNfVX]`|
|[Stance: Cobra Stance](spell-effects/00-CgxYa0lrLUjS2ZhI.htm)||`@Compendium[pf2e.spell-effects.CgxYa0lrLUjS2ZhI]`|
|[Stance: Crane Stance](spell-effects/00-nwkYZs6YwXYAJ4ps.htm)||`@Compendium[pf2e.spell-effects.nwkYZs6YwXYAJ4ps]`|
|[Stance: Dragon Stance](spell-effects/00-qUowHpn79Dpt1hVn.htm)||`@Compendium[pf2e.spell-effects.qUowHpn79Dpt1hVn]`|
|[Stance: Gorilla Stance](spell-effects/00-RozqjLocahvQWERr.htm)||`@Compendium[pf2e.spell-effects.RozqjLocahvQWERr]`|
|[Stance: Ironblood Stance](spell-effects/00-tPKXLtDJ3bzJcXlv.htm)||`@Compendium[pf2e.spell-effects.tPKXLtDJ3bzJcXlv]`|
|[Stance: Mountain Stance](spell-effects/00-gYpy9XBPScIlY93p.htm)||`@Compendium[pf2e.spell-effects.gYpy9XBPScIlY93p]`|
|[Stance: Rain of Embers Stance](spell-effects/00-Im5JBInybWFbHEYS.htm)||`@Compendium[pf2e.spell-effects.Im5JBInybWFbHEYS]`|
|[Stance: Stumbling Stance](spell-effects/00-BCyGDKcplkJiSAKJ.htm)||`@Compendium[pf2e.spell-effects.BCyGDKcplkJiSAKJ]`|
|[Stance: Tangled Forest Stance](spell-effects/00-PMHwCrnh9W4sMu5b.htm)||`@Compendium[pf2e.spell-effects.PMHwCrnh9W4sMu5b]`|
|[Stance: Tiger Stance](spell-effects/00-pf9yvKNg6jZLrE30.htm)||`@Compendium[pf2e.spell-effects.pf9yvKNg6jZLrE30]`|
|[Stance: Wild Winds Stance](spell-effects/00-eeAlh6edygcZIz9c.htm)||`@Compendium[pf2e.spell-effects.eeAlh6edygcZIz9c]`|
|[Stance: Wolf Stance](spell-effects/00-b2kWJuCPj1rDMdwz.htm)|Posture: Posture du loup|`@Compendium[pf2e.spell-effects.b2kWJuCPj1rDMdwz]`|


## Guide du MJ

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Abandoned in a Distant Land](gmg-srd/bNmSwKSFk6SWWrlj.htm)|Abandonné dans un pays lointain|`@Compendium[pf2e.gmg-srd.bNmSwKSFk6SWWrlj]`|
|[Academic Community](gmg-srd/fojjq3VXShnNSOsZ.htm)|Communauté universitaire|`@Compendium[pf2e.gmg-srd.fojjq3VXShnNSOsZ]`|
|[Academy Trained](gmg-srd/j9kx5CzfulaFTMOE.htm)|Formation universitaire|`@Compendium[pf2e.gmg-srd.j9kx5CzfulaFTMOE]`|
|[Accidental Fall](gmg-srd/eADadfrHPkGNTjEB.htm)|Chute accidentelle|`@Compendium[pf2e.gmg-srd.eADadfrHPkGNTjEB]`|
|[Accusation of Theft](gmg-srd/sHC4quiLAzdUe2uT.htm)|Accusation de vol|`@Compendium[pf2e.gmg-srd.sHC4quiLAzdUe2uT]`|
|[Animal Helpers](gmg-srd/LekxXsOgEwMODqCS.htm)|Amis des animaux|`@Compendium[pf2e.gmg-srd.LekxXsOgEwMODqCS]`|
|[Another Ancestry's Settlement](gmg-srd/VnrGXdVthTceOlXV.htm)|Une communauté d'une autre ascendance|`@Compendium[pf2e.gmg-srd.VnrGXdVthTceOlXV]`|
|[Attained a Magical Gift](gmg-srd/FOs2eLbehUzPh9p9.htm)|Dépositaire d'un talent magique|`@Compendium[pf2e.gmg-srd.FOs2eLbehUzPh9p9]`|
|[Balance](gmg-srd/amEb5dGmSvvAJz7J.htm)|Balance|`@Compendium[pf2e.gmg-srd.amEb5dGmSvvAJz7J]`|
|[Betrayed](gmg-srd/e4nkDjB1Y6nqaK2G.htm)|Trahi|`@Compendium[pf2e.gmg-srd.e4nkDjB1Y6nqaK2G]`|
|[Bullied](gmg-srd/W2ccu0zeLNHDlCXe.htm)|Persécuté|`@Compendium[pf2e.gmg-srd.W2ccu0zeLNHDlCXe]`|
|[Called Before Judges](gmg-srd/yZUZDw8TXX1MvZ4P.htm)|Convoqué devant les juges|`@Compendium[pf2e.gmg-srd.yZUZDw8TXX1MvZ4P]`|
|[Captured by Giants](gmg-srd/idZxjeEv8B3x44r1.htm)|Capturé par des géants|`@Compendium[pf2e.gmg-srd.idZxjeEv8B3x44r1]`|
|[Claimed an Inheritance](gmg-srd/bvhckAGjcCJVs0jn.htm)|Bénéficiaire d'un héritage|`@Compendium[pf2e.gmg-srd.bvhckAGjcCJVs0jn]`|
|[Coastal Community](gmg-srd/vD1y8mIHxxw7c8J1.htm)|Communauté côtière|`@Compendium[pf2e.gmg-srd.vD1y8mIHxxw7c8J1]`|
|[Comet](gmg-srd/NSG1YxOYAtSowNJa.htm)|Comète|`@Compendium[pf2e.gmg-srd.NSG1YxOYAtSowNJa]`|
|[Comrade-in-Arms](gmg-srd/nzOpdMVLPhoNTxzR.htm)|Fréres d'armes|`@Compendium[pf2e.gmg-srd.nzOpdMVLPhoNTxzR]`|
|[Cosmopolitan City](gmg-srd/HkMzwjBTGgQgsOCW.htm)|Cité cosmopolite|`@Compendium[pf2e.gmg-srd.HkMzwjBTGgQgsOCW]`|
|[Desperate Intimidation](gmg-srd/G9058881e1ci9X1C.htm)|Intimidation désespérée|`@Compendium[pf2e.gmg-srd.G9058881e1ci9X1C]`|
|[Died](gmg-srd/jtgfTYpcv4FFpWsL.htm)|Mort|`@Compendium[pf2e.gmg-srd.jtgfTYpcv4FFpWsL]`|
|[Donjon](gmg-srd/llfG8iMTFMzVIF9s.htm)|Donjon|`@Compendium[pf2e.gmg-srd.llfG8iMTFMzVIF9s]`|
|[Dullard](gmg-srd/BAs1fCaUTmvRjvVT.htm)|Idiot|`@Compendium[pf2e.gmg-srd.BAs1fCaUTmvRjvVT]`|
|[Euryale (curse)](gmg-srd/jVh90bfsC6w6iVyy.htm)|Euryale (malédiction)|`@Compendium[pf2e.gmg-srd.jVh90bfsC6w6iVyy]`|
|[Fell In with a Bad Crowd](gmg-srd/wIGRRmXtzed74qQq.htm)|Mauvaises fréquentations|`@Compendium[pf2e.gmg-srd.wIGRRmXtzed74qQq]`|
|[Flames](gmg-srd/TG9L9JvKpomFQUfl.htm)|Flammes|`@Compendium[pf2e.gmg-srd.TG9L9JvKpomFQUfl]`|
|[Fool](gmg-srd/C4KgMYzmyWVpe721.htm)|Fou|`@Compendium[pf2e.gmg-srd.C4KgMYzmyWVpe721]`|
|[Front Lines](gmg-srd/SyEl079jcULQp7Ra.htm)|Ligne de Front|`@Compendium[pf2e.gmg-srd.SyEl079jcULQp7Ra]`|
|[Frontier](gmg-srd/eCB10E0MSvD9s008.htm)|Confins|`@Compendium[pf2e.gmg-srd.eCB10E0MSvD9s008]`|
|[Gem](gmg-srd/qFgPiHDBVa3VSNgC.htm)|Gemme|`@Compendium[pf2e.gmg-srd.qFgPiHDBVa3VSNgC]`|
|[Had Your First Kill](gmg-srd/tmrnZkUzHVC0s6D9.htm)|Premier meurtre|`@Compendium[pf2e.gmg-srd.tmrnZkUzHVC0s6D9]`|
|[Had an Ordinary Childhood](gmg-srd/Runwg9xhNrN79JBN.htm)|Enfance ordinaire|`@Compendium[pf2e.gmg-srd.Runwg9xhNrN79JBN]`|
|[Homelessness](gmg-srd/niZPxqupDMfIXxJs.htm)|Vagabondage|`@Compendium[pf2e.gmg-srd.niZPxqupDMfIXxJs]`|
|[Itinerant](gmg-srd/5KDGPcdZpdZumUYy.htm)|Itinérant|`@Compendium[pf2e.gmg-srd.5KDGPcdZpdZumUYy]`|
|[Jester](gmg-srd/SbfCzPannCdZEB7c.htm)|Bouffon|`@Compendium[pf2e.gmg-srd.SbfCzPannCdZEB7c]`|
|[Key](gmg-srd/ccG9pxYQ9Gg6262b.htm)|Clef|`@Compendium[pf2e.gmg-srd.ccG9pxYQ9Gg6262b]`|
|[Kidnapped](gmg-srd/dcP3oqjsl85p6R2Z.htm)||`@Compendium[pf2e.gmg-srd.dcP3oqjsl85p6R2Z]`|
|[Kindly Witch](gmg-srd/JHQI2skcksaLMPqA.htm)||`@Compendium[pf2e.gmg-srd.JHQI2skcksaLMPqA]`|
|[Knight](gmg-srd/iA9QWuYATBPB6thI.htm)||`@Compendium[pf2e.gmg-srd.iA9QWuYATBPB6thI]`|
|[Liberators](gmg-srd/OHKKQ7zTf61vz35h.htm)||`@Compendium[pf2e.gmg-srd.OHKKQ7zTf61vz35h]`|
|[Lost in the Wilderness](gmg-srd/a0mJO7io1hcwMAjJ.htm)||`@Compendium[pf2e.gmg-srd.a0mJO7io1hcwMAjJ]`|
|[Magician](gmg-srd/UGxZ6vhfq7vQFXTt.htm)|Magicien|`@Compendium[pf2e.gmg-srd.UGxZ6vhfq7vQFXTt]`|
|[Matter of Might](gmg-srd/652cIWAXc8zAxkac.htm)||`@Compendium[pf2e.gmg-srd.652cIWAXc8zAxkac]`|
|[Mercantile Expertise](gmg-srd/7qsQBWD5sSGz7Fik.htm)||`@Compendium[pf2e.gmg-srd.7qsQBWD5sSGz7Fik]`|
|[Met a Fantastic Creature](gmg-srd/xmgvqyfwYPBXVELo.htm)|Rencontre avec une créature fantastique|`@Compendium[pf2e.gmg-srd.xmgvqyfwYPBXVELo]`|
|[Metropolis](gmg-srd/3Uwfn0ui7tS277iE.htm)|Métropole|`@Compendium[pf2e.gmg-srd.3Uwfn0ui7tS277iE]`|
|[Missing Child](gmg-srd/upnKpiEflAhhFLNQ.htm)|Enfant perdu|`@Compendium[pf2e.gmg-srd.upnKpiEflAhhFLNQ]`|
|[Moon](gmg-srd/c8bBOo2RPxStZozM.htm)||`@Compendium[pf2e.gmg-srd.c8bBOo2RPxStZozM]`|
|[Patron of the Arts](gmg-srd/rDbdifseYxfEWgKg.htm)|Bienfaiteur des arts|`@Compendium[pf2e.gmg-srd.rDbdifseYxfEWgKg]`|
|[Privileged Position](gmg-srd/nsZ93vUEFucsLwRz.htm)||`@Compendium[pf2e.gmg-srd.nsZ93vUEFucsLwRz]`|
|[Raided](gmg-srd/TNruMcXK4wizclQc.htm)|Pillage|`@Compendium[pf2e.gmg-srd.TNruMcXK4wizclQc]`|
|[Relationship Ender](gmg-srd/WXQbdAmQM9HBjc0R.htm)|Briseur de relations|`@Compendium[pf2e.gmg-srd.WXQbdAmQM9HBjc0R]`|
|[Religious Community](gmg-srd/Gr4v8ROPCxi4hvAO.htm)||`@Compendium[pf2e.gmg-srd.Gr4v8ROPCxi4hvAO]`|
|[Religious Students](gmg-srd/PVdcBHjBVySBchG1.htm)|Séminaristes|`@Compendium[pf2e.gmg-srd.PVdcBHjBVySBchG1]`|
|[Rival Trackers](gmg-srd/vMTOwBTqs8JeE8bH.htm)|Pisteurs rivaux|`@Compendium[pf2e.gmg-srd.vMTOwBTqs8JeE8bH]`|
|[Robbed](gmg-srd/Ip6kSKGhl3XHQZ93.htm)||`@Compendium[pf2e.gmg-srd.Ip6kSKGhl3XHQZ93]`|
|[Rogue](gmg-srd/xe0iqQ3XSnDV4tLp.htm)|Traître|`@Compendium[pf2e.gmg-srd.xe0iqQ3XSnDV4tLp]`|
|[Ruin](gmg-srd/x0vU6WsHcXUNY20y.htm)|Ruine|`@Compendium[pf2e.gmg-srd.x0vU6WsHcXUNY20y]`|
|[Seeking Accolades](gmg-srd/y4quheEoo2bcmM6g.htm)|Chercheur d'attention|`@Compendium[pf2e.gmg-srd.y4quheEoo2bcmM6g]`|
|[Simple Village](gmg-srd/Gm4AtuFNRgOnzsmF.htm)||`@Compendium[pf2e.gmg-srd.Gm4AtuFNRgOnzsmF]`|
|[Skull](gmg-srd/xGkT6QZGfYE3kBqn.htm)|Crâne|`@Compendium[pf2e.gmg-srd.xGkT6QZGfYE3kBqn]`|
|[Slander](gmg-srd/cD51eAUtbGlp5UKr.htm)||`@Compendium[pf2e.gmg-srd.cD51eAUtbGlp5UKr]`|
|[Social Maneuvering](gmg-srd/yqCZWcX9e3iwmvSS.htm)|Manipulations sociales|`@Compendium[pf2e.gmg-srd.yqCZWcX9e3iwmvSS]`|
|[Spy](gmg-srd/VEFcfA4NnWdJSRtp.htm)|Espion|`@Compendium[pf2e.gmg-srd.VEFcfA4NnWdJSRtp]`|
|[Star](gmg-srd/6NOZe9gUfVEedECW.htm)||`@Compendium[pf2e.gmg-srd.6NOZe9gUfVEedECW]`|
|[Sun](gmg-srd/Alf16WU6qK8w1jmq.htm)||`@Compendium[pf2e.gmg-srd.Alf16WU6qK8w1jmq]`|
|[Survived a Disaster](gmg-srd/pwn7lPcwqyGNZyOo.htm)|Survivant d'un désastre|`@Compendium[pf2e.gmg-srd.pwn7lPcwqyGNZyOo]`|
|[Talons](gmg-srd/QxQ0PheFSIJU9rfH.htm)|Serres|`@Compendium[pf2e.gmg-srd.QxQ0PheFSIJU9rfH]`|
|[The Academic](gmg-srd/ziWhyyygUG3Lemdu.htm)|Le professeur|`@Compendium[pf2e.gmg-srd.ziWhyyygUG3Lemdu]`|
|[The Boss](gmg-srd/re8nmHRiQdU63AYk.htm)|Le patron|`@Compendium[pf2e.gmg-srd.re8nmHRiQdU63AYk]`|
|[The Champion](gmg-srd/CW53y8R730XoNQSG.htm)||`@Compendium[pf2e.gmg-srd.CW53y8R730XoNQSG]`|
|[The Confidante](gmg-srd/mHocbJQKBzbHM790.htm)||`@Compendium[pf2e.gmg-srd.mHocbJQKBzbHM790]`|
|[The Crafter](gmg-srd/F7Tf6APgUKjnCZgv.htm)||`@Compendium[pf2e.gmg-srd.F7Tf6APgUKjnCZgv]`|
|[The Criminal](gmg-srd/MCIp76IWZ2pk7dyz.htm)||`@Compendium[pf2e.gmg-srd.MCIp76IWZ2pk7dyz]`|
|[The Dead One](gmg-srd/8NKe8N5NsULPShPX.htm)||`@Compendium[pf2e.gmg-srd.8NKe8N5NsULPShPX]`|
|[The Fates](gmg-srd/Dyg3vJnEnReKyYHU.htm)||`@Compendium[pf2e.gmg-srd.Dyg3vJnEnReKyYHU]`|
|[The Fiend](gmg-srd/QhNWhs0isjxE67lu.htm)|Le fiélon|`@Compendium[pf2e.gmg-srd.QhNWhs0isjxE67lu]`|
|[The Fool](gmg-srd/3g3uXFsHbfcad8eA.htm)||`@Compendium[pf2e.gmg-srd.3g3uXFsHbfcad8eA]`|
|[The Hunter](gmg-srd/pd6ZzGL5TA58Fa3p.htm)||`@Compendium[pf2e.gmg-srd.pd6ZzGL5TA58Fa3p]`|
|[The Liege Lord](gmg-srd/ejRPZwAlci3jTli9.htm)||`@Compendium[pf2e.gmg-srd.ejRPZwAlci3jTli9]`|
|[The Lover](gmg-srd/mg7oMp0cNa0GJMCS.htm)||`@Compendium[pf2e.gmg-srd.mg7oMp0cNa0GJMCS]`|
|[The Mentor](gmg-srd/Rh5XWscABJ43TLme.htm)|Le mentor|`@Compendium[pf2e.gmg-srd.Rh5XWscABJ43TLme]`|
|[The Mercenary](gmg-srd/Cfd7ndT61VjR52OZ.htm)||`@Compendium[pf2e.gmg-srd.Cfd7ndT61VjR52OZ]`|
|[The Mystic](gmg-srd/Erqgqv8WEtnzBtTu.htm)||`@Compendium[pf2e.gmg-srd.Erqgqv8WEtnzBtTu]`|
|[The Pariah](gmg-srd/uK1fqqsUNk7FZsOk.htm)|Le paria|`@Compendium[pf2e.gmg-srd.uK1fqqsUNk7FZsOk]`|
|[The Relative](gmg-srd/YR0uxB5CPPueFjBs.htm)|Le parent|`@Compendium[pf2e.gmg-srd.YR0uxB5CPPueFjBs]`|
|[The Seer](gmg-srd/CwyYDIAAuRll2i5R.htm)||`@Compendium[pf2e.gmg-srd.CwyYDIAAuRll2i5R]`|
|[The Void](gmg-srd/CfD81LilQAtGuvyx.htm)||`@Compendium[pf2e.gmg-srd.CfD81LilQAtGuvyx]`|
|[The Wanderer](gmg-srd/KtkDaTrkBwwupKcE.htm)||`@Compendium[pf2e.gmg-srd.KtkDaTrkBwwupKcE]`|
|[The Well-Connected](gmg-srd/Xi5nvMNJrzsy09ws.htm)|L'ami aux nombreuses relations|`@Compendium[pf2e.gmg-srd.Xi5nvMNJrzsy09ws]`|
|[Throne](gmg-srd/IJ3nFDu2YcoP6ynd.htm)|Trône|`@Compendium[pf2e.gmg-srd.IJ3nFDu2YcoP6ynd]`|
|[Timely Cure](gmg-srd/BbgtR5LmB9ZdPpx1.htm)||`@Compendium[pf2e.gmg-srd.BbgtR5LmB9ZdPpx1]`|
|[Trade Town](gmg-srd/OdD3NuxI5hNAEOVx.htm)||`@Compendium[pf2e.gmg-srd.OdD3NuxI5hNAEOVx]`|
|[Trained by a Mentor](gmg-srd/2cyhXgGkmoFe3Phw.htm)|Formé par un Mentor|`@Compendium[pf2e.gmg-srd.2cyhXgGkmoFe3Phw]`|
|[Underground](gmg-srd/AL562TPAhLCFX62S.htm)|Sous terre|`@Compendium[pf2e.gmg-srd.AL562TPAhLCFX62S]`|
|[Vizier](gmg-srd/cIlNXxPJuy8MtaDM.htm)||`@Compendium[pf2e.gmg-srd.cIlNXxPJuy8MtaDM]`|
|[Wasteland Survivors](gmg-srd/bRQz8uVlYtQ1cwwI.htm)||`@Compendium[pf2e.gmg-srd.bRQz8uVlYtQ1cwwI]`|
|[Witnessed War](gmg-srd/oDhTOO1WjEZk1qfD.htm)||`@Compendium[pf2e.gmg-srd.oDhTOO1WjEZk1qfD]`|
|[Won a Competition](gmg-srd/YsY9euHOGCGc3mTE.htm)|Vainqueur d'une compétition|`@Compendium[pf2e.gmg-srd.YsY9euHOGCGc3mTE]`|


## Macros PF2e

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Academic Conference](pathfinder-society-boons/XoamInggkxLh6Zo0.htm)||`@Compendium[pf2e.pathfinder-society-boons.XoamInggkxLh6Zo0]`|
|[Adversary Lore](pathfinder-society-boons/XBf4Uz3n0AwSmpXx.htm)||`@Compendium[pf2e.pathfinder-society-boons.XBf4Uz3n0AwSmpXx]`|
|[AoA #146: Mwangi Summoner](pathfinder-society-boons/DWcmLzzT1eYk76c7.htm)||`@Compendium[pf2e.pathfinder-society-boons.DWcmLzzT1eYk76c7]`|
|[AoA #147: Boon A - Heroic Membership](pathfinder-society-boons/Nadcb08xcTAoPBEX.htm)||`@Compendium[pf2e.pathfinder-society-boons.Nadcb08xcTAoPBEX]`|
|[AoA #147: Boon B - Expanded Summoning](pathfinder-society-boons/WV4J7NMgKXNlXonW.htm)||`@Compendium[pf2e.pathfinder-society-boons.WV4J7NMgKXNlXonW]`|
|[AoA #148: Boon A - Crystal Clear](pathfinder-society-boons/VLjkX7HpDqKaidNd.htm)||`@Compendium[pf2e.pathfinder-society-boons.VLjkX7HpDqKaidNd]`|
|[AoA #148: Boon B - Expanded Summoning](pathfinder-society-boons/CjW54WSPDcykzqj9.htm)||`@Compendium[pf2e.pathfinder-society-boons.CjW54WSPDcykzqj9]`|
|[AoA #149: Boon A - Advanced Training](pathfinder-society-boons/8cMFcCa3h9XfWNkZ.htm)||`@Compendium[pf2e.pathfinder-society-boons.8cMFcCa3h9XfWNkZ]`|
|[AoA #149: Boon B - Katapesh Companion](pathfinder-society-boons/3K8yzfU2t9chVv6h.htm)||`@Compendium[pf2e.pathfinder-society-boons.3K8yzfU2t9chVv6h]`|
|[AoA #149: Boon C - Expanded Summoning](pathfinder-society-boons/wcOnL9nFDfCkNVMZ.htm)||`@Compendium[pf2e.pathfinder-society-boons.wcOnL9nFDfCkNVMZ]`|
|[AoA #150: Boon A - Legacy of Ashes](pathfinder-society-boons/grkmoNwwHOROKER7.htm)||`@Compendium[pf2e.pathfinder-society-boons.grkmoNwwHOROKER7]`|
|[AoA #150: Boon B - Paragon of Promise](pathfinder-society-boons/We3sJV72xtdPeMjp.htm)||`@Compendium[pf2e.pathfinder-society-boons.We3sJV72xtdPeMjp]`|
|[Beginnings and Endings](pathfinder-society-boons/rdY0vyXfrIlkQgqd.htm)||`@Compendium[pf2e.pathfinder-society-boons.rdY0vyXfrIlkQgqd]`|
|[Bequethal](pathfinder-society-boons/GRiNBy8yv4sWp1ua.htm)||`@Compendium[pf2e.pathfinder-society-boons.GRiNBy8yv4sWp1ua]`|
|[Bring Them Back Alive](pathfinder-society-boons/SI37oVZxjwXew3E4.htm)||`@Compendium[pf2e.pathfinder-society-boons.SI37oVZxjwXew3E4]`|
|[Charitable Adventure](pathfinder-society-boons/bMp6W5kPNyqBoOxm.htm)||`@Compendium[pf2e.pathfinder-society-boons.bMp6W5kPNyqBoOxm]`|
|[Consummate Dabbler](pathfinder-society-boons/5tWlPSGW5AEk10GJ.htm)||`@Compendium[pf2e.pathfinder-society-boons.5tWlPSGW5AEk10GJ]`|
|[Crafter's Workshop](pathfinder-society-boons/gdkQ04RgMJZ0QxbZ.htm)||`@Compendium[pf2e.pathfinder-society-boons.gdkQ04RgMJZ0QxbZ]`|
|[Curse Breaker](pathfinder-society-boons/oOB1uKvoBMGj7ykZ.htm)||`@Compendium[pf2e.pathfinder-society-boons.oOB1uKvoBMGj7ykZ]`|
|[Eager Protege](pathfinder-society-boons/0zhQQK3gvu75ac1Q.htm)||`@Compendium[pf2e.pathfinder-society-boons.0zhQQK3gvu75ac1Q]`|
|[Envoy's Alliance Champion](pathfinder-society-boons/jzJ575vAsw2MaRgo.htm)||`@Compendium[pf2e.pathfinder-society-boons.jzJ575vAsw2MaRgo]`|
|[Envoy's Alliance Champion, Improved](pathfinder-society-boons/l8WNWPFUdxcqEG6e.htm)||`@Compendium[pf2e.pathfinder-society-boons.l8WNWPFUdxcqEG6e]`|
|[Exemplary Recruiter](pathfinder-society-boons/ocS6B3AV1n7yHFd5.htm)||`@Compendium[pf2e.pathfinder-society-boons.ocS6B3AV1n7yHFd5]`|
|[Exotic Edge](pathfinder-society-boons/UWZfORgk1yVNV0RF.htm)||`@Compendium[pf2e.pathfinder-society-boons.UWZfORgk1yVNV0RF]`|
|[FoP - Fiery Companion](pathfinder-society-boons/2QzXBsURLO7xECxO.htm)||`@Compendium[pf2e.pathfinder-society-boons.2QzXBsURLO7xECxO]`|
|[FoP - Noala's Lessons](pathfinder-society-boons/4lLhGgG30TMu8tIx.htm)||`@Compendium[pf2e.pathfinder-society-boons.4lLhGgG30TMu8tIx]`|
|[Grand Archive Champion](pathfinder-society-boons/8fbBYNkFQgU3IQV0.htm)||`@Compendium[pf2e.pathfinder-society-boons.8fbBYNkFQgU3IQV0]`|
|[Grand Archive Champion, Improved](pathfinder-society-boons/Tmqa8Q9zaYr0uj6F.htm)||`@Compendium[pf2e.pathfinder-society-boons.Tmqa8Q9zaYr0uj6F]`|
|[Heroic Defiance](pathfinder-society-boons/hghR0f5IetZBvyDR.htm)||`@Compendium[pf2e.pathfinder-society-boons.hghR0f5IetZBvyDR]`|
|[Heroic Hustle](pathfinder-society-boons/Wz2F0qk4rxZeH311.htm)||`@Compendium[pf2e.pathfinder-society-boons.Wz2F0qk4rxZeH311]`|
|[Heroic Inspiration](pathfinder-society-boons/0QRG6TAVwCWL2Zr8.htm)||`@Compendium[pf2e.pathfinder-society-boons.0QRG6TAVwCWL2Zr8]`|
|[Heroic Intervention](pathfinder-society-boons/En3RdoXDAtyXdNKq.htm)||`@Compendium[pf2e.pathfinder-society-boons.En3RdoXDAtyXdNKq]`|
|[Heroic Recall](pathfinder-society-boons/wkgBg5UD3bChfl4b.htm)||`@Compendium[pf2e.pathfinder-society-boons.wkgBg5UD3bChfl4b]`|
|[Heroic Resurgence](pathfinder-society-boons/UNlaVxb0pDov3efw.htm)||`@Compendium[pf2e.pathfinder-society-boons.UNlaVxb0pDov3efw]`|
|[Hireling](pathfinder-society-boons/sBXsfT0uYltF8TFE.htm)||`@Compendium[pf2e.pathfinder-society-boons.sBXsfT0uYltF8TFE]`|
|[Hireling, Expert](pathfinder-society-boons/1VWMLe12ZFqF9jah.htm)||`@Compendium[pf2e.pathfinder-society-boons.1VWMLe12ZFqF9jah]`|
|[Hireling, Master](pathfinder-society-boons/ca869lJo8E51WcW4.htm)||`@Compendium[pf2e.pathfinder-society-boons.ca869lJo8E51WcW4]`|
|[Hireling, Professional](pathfinder-society-boons/mrlQrZd0yBwOj2lk.htm)||`@Compendium[pf2e.pathfinder-society-boons.mrlQrZd0yBwOj2lk]`|
|[Home Region](pathfinder-society-boons/KXO4CUklzuJ0tZnw.htm)||`@Compendium[pf2e.pathfinder-society-boons.KXO4CUklzuJ0tZnw]`|
|[Horizon Hunters Champion](pathfinder-society-boons/ph7CO8ong1sZmrNc.htm)||`@Compendium[pf2e.pathfinder-society-boons.ph7CO8ong1sZmrNc]`|
|[Horizon Hunters Champion, Improved](pathfinder-society-boons/Sc9gT40T28tFhSzq.htm)||`@Compendium[pf2e.pathfinder-society-boons.Sc9gT40T28tFhSzq]`|
|[Leader By Example](pathfinder-society-boons/hy1gMMpRmujuw8i4.htm)||`@Compendium[pf2e.pathfinder-society-boons.hy1gMMpRmujuw8i4]`|
|[Leshy Companion](pathfinder-society-boons/Mu5GQ9ZEz3geAYxK.htm)||`@Compendium[pf2e.pathfinder-society-boons.Mu5GQ9ZEz3geAYxK]`|
|[Menace Under Otari](pathfinder-society-boons/Vmg31SsMjjimghzP.htm)||`@Compendium[pf2e.pathfinder-society-boons.Vmg31SsMjjimghzP]`|
|[Mentor, Combat](pathfinder-society-boons/vYSA5M9fgBdYiGLT.htm)||`@Compendium[pf2e.pathfinder-society-boons.vYSA5M9fgBdYiGLT]`|
|[Mentor, Magical](pathfinder-society-boons/95R55tdobViU7ZWe.htm)||`@Compendium[pf2e.pathfinder-society-boons.95R55tdobViU7ZWe]`|
|[Mentor, Protective](pathfinder-society-boons/PXtBhRVHM28angjV.htm)||`@Compendium[pf2e.pathfinder-society-boons.PXtBhRVHM28angjV]`|
|[Mentor, Rugged](pathfinder-society-boons/DkqU67wxvbDu0Qrs.htm)||`@Compendium[pf2e.pathfinder-society-boons.DkqU67wxvbDu0Qrs]`|
|[Mentor, Skillful](pathfinder-society-boons/J1hUP5SI4MJvlK36.htm)||`@Compendium[pf2e.pathfinder-society-boons.J1hUP5SI4MJvlK36]`|
|[Mentor, Worldly](pathfinder-society-boons/9w8TdCsGEpttXYuy.htm)||`@Compendium[pf2e.pathfinder-society-boons.9w8TdCsGEpttXYuy]`|
|[Meticulous Appraisal](pathfinder-society-boons/MN3MlIx7djj7WDNY.htm)||`@Compendium[pf2e.pathfinder-society-boons.MN3MlIx7djj7WDNY]`|
|[Multicultural Training](pathfinder-society-boons/bcsusUErwGQYHj6d.htm)||`@Compendium[pf2e.pathfinder-society-boons.bcsusUErwGQYHj6d]`|
|[Naturalist](pathfinder-society-boons/0FwLrQb5EeQDAUNW.htm)||`@Compendium[pf2e.pathfinder-society-boons.0FwLrQb5EeQDAUNW]`|
|[Off-Hours Study](pathfinder-society-boons/XkNW2dq5hwWOViYg.htm)||`@Compendium[pf2e.pathfinder-society-boons.XkNW2dq5hwWOViYg]`|
|[Practiced Medic](pathfinder-society-boons/awcOD618fuXZN3rh.htm)||`@Compendium[pf2e.pathfinder-society-boons.awcOD618fuXZN3rh]`|
|[Preserve](pathfinder-society-boons/WGjcz2mkgGB1qCWw.htm)||`@Compendium[pf2e.pathfinder-society-boons.WGjcz2mkgGB1qCWw]`|
|[Promotional Accessory](pathfinder-society-boons/aG5cua0K2SlurXxw.htm)||`@Compendium[pf2e.pathfinder-society-boons.aG5cua0K2SlurXxw]`|
|[Promotional Service Award](pathfinder-society-boons/cjjRFoOE1GtjEA3F.htm)||`@Compendium[pf2e.pathfinder-society-boons.cjjRFoOE1GtjEA3F]`|
|[Promotional Vestments](pathfinder-society-boons/SDDbh7KmDn5HUbst.htm)||`@Compendium[pf2e.pathfinder-society-boons.SDDbh7KmDn5HUbst]`|
|[Radiant Oath Champion](pathfinder-society-boons/M8cJ8qOwWj7XN6h1.htm)||`@Compendium[pf2e.pathfinder-society-boons.M8cJ8qOwWj7XN6h1]`|
|[Radiant Oath Champion, Improved](pathfinder-society-boons/qaSlz43CHPiCoIOC.htm)||`@Compendium[pf2e.pathfinder-society-boons.qaSlz43CHPiCoIOC]`|
|[Resist Corruption](pathfinder-society-boons/rFgWvC1UADsavY5J.htm)||`@Compendium[pf2e.pathfinder-society-boons.rFgWvC1UADsavY5J]`|
|[Resurrection Plan](pathfinder-society-boons/M2lGGGaPIOimeA31.htm)||`@Compendium[pf2e.pathfinder-society-boons.M2lGGGaPIOimeA31]`|
|[S1-00 - Nexian Researcher](pathfinder-society-boons/YVpNrVBLRrHjaR4C.htm)||`@Compendium[pf2e.pathfinder-society-boons.YVpNrVBLRrHjaR4C]`|
|[S1-01 - Engraved Wayfinder](pathfinder-society-boons/P1sdPHrbAfqrEAM3.htm)||`@Compendium[pf2e.pathfinder-society-boons.P1sdPHrbAfqrEAM3]`|
|[S1-01 - Society Connections](pathfinder-society-boons/ig06ikA6rnNP1JUb.htm)||`@Compendium[pf2e.pathfinder-society-boons.ig06ikA6rnNP1JUb]`|
|[S1-02 - Cryptid Scholar](pathfinder-society-boons/632pDmCtbQxBlE3d.htm)||`@Compendium[pf2e.pathfinder-society-boons.632pDmCtbQxBlE3d]`|
|[S1-03 - Team Player](pathfinder-society-boons/1welewgoB0z86Nms.htm)||`@Compendium[pf2e.pathfinder-society-boons.1welewgoB0z86Nms]`|
|[S1-04 - A Thorny Situation](pathfinder-society-boons/6oTXrz56Evik64Fy.htm)||`@Compendium[pf2e.pathfinder-society-boons.6oTXrz56Evik64Fy]`|
|[S1-04 - Touched by the Storm](pathfinder-society-boons/XybjaZJlmBFFztwJ.htm)||`@Compendium[pf2e.pathfinder-society-boons.XybjaZJlmBFFztwJ]`|
|[S1-05 - Experienced Mountaineer](pathfinder-society-boons/GIdtqEFKKgwPmDBq.htm)||`@Compendium[pf2e.pathfinder-society-boons.GIdtqEFKKgwPmDBq]`|
|[S1-06 - Traveler of the Spirit Road](pathfinder-society-boons/WVOP420jZ4jSTYI8.htm)||`@Compendium[pf2e.pathfinder-society-boons.WVOP420jZ4jSTYI8]`|
|[S1-07 - Blast from the Past](pathfinder-society-boons/0izXWwYqRGQq74mG.htm)||`@Compendium[pf2e.pathfinder-society-boons.0izXWwYqRGQq74mG]`|
|[S1-07 - To Seal and Protect](pathfinder-society-boons/T7gyK9LV14wI7Xdy.htm)||`@Compendium[pf2e.pathfinder-society-boons.T7gyK9LV14wI7Xdy]`|
|[S1-08 - River Kingdoms Politician](pathfinder-society-boons/lJ1lNwGxV0IrsN2z.htm)||`@Compendium[pf2e.pathfinder-society-boons.lJ1lNwGxV0IrsN2z]`|
|[S1-09 - Ally of the Iruxi](pathfinder-society-boons/ZtLTqT4G5q8SLJCT.htm)||`@Compendium[pf2e.pathfinder-society-boons.ZtLTqT4G5q8SLJCT]`|
|[S1-09 - Iruxi Bane](pathfinder-society-boons/wdjRk8JaZGP1mjqD.htm)||`@Compendium[pf2e.pathfinder-society-boons.wdjRk8JaZGP1mjqD]`|
|[S1-10 - Tarnbreaker Champions](pathfinder-society-boons/giyLRNefaC3IsFay.htm)||`@Compendium[pf2e.pathfinder-society-boons.giyLRNefaC3IsFay]`|
|[S1-11 - Devil's Keep](pathfinder-society-boons/ePZOmPbGXzrEaXXs.htm)||`@Compendium[pf2e.pathfinder-society-boons.ePZOmPbGXzrEaXXs]`|
|[S1-11 - Diggen the Liar](pathfinder-society-boons/fTndDUqUDSVXM0Qm.htm)||`@Compendium[pf2e.pathfinder-society-boons.fTndDUqUDSVXM0Qm]`|
|[S1-12 - Valais's Assurance](pathfinder-society-boons/XOVDOCsvNBX5LS8j.htm)||`@Compendium[pf2e.pathfinder-society-boons.XOVDOCsvNBX5LS8j]`|
|[S1-13 - Convention Hero](pathfinder-society-boons/xVMouu7E7Xy7AGER.htm)||`@Compendium[pf2e.pathfinder-society-boons.xVMouu7E7Xy7AGER]`|
|[S1-14 - Big Game Hunter](pathfinder-society-boons/JznxnYTs38GYpW1p.htm)||`@Compendium[pf2e.pathfinder-society-boons.JznxnYTs38GYpW1p]`|
|[S1-15 - Finadar Leshy](pathfinder-society-boons/hxp0fDMrB7BT3y3T.htm)||`@Compendium[pf2e.pathfinder-society-boons.hxp0fDMrB7BT3y3T]`|
|[S1-16 - Blood Offering](pathfinder-society-boons/DcMAGJWq2G5kGRWn.htm)||`@Compendium[pf2e.pathfinder-society-boons.DcMAGJWq2G5kGRWn]`|
|[S1-17 - Fey Influence](pathfinder-society-boons/taUqYoHuWH9uEbsn.htm)||`@Compendium[pf2e.pathfinder-society-boons.taUqYoHuWH9uEbsn]`|
|[S1-18 - Light in the Dark](pathfinder-society-boons/Y69ZRmuoj0XK9AZm.htm)||`@Compendium[pf2e.pathfinder-society-boons.Y69ZRmuoj0XK9AZm]`|
|[S1-18 - Narsen's Web](pathfinder-society-boons/tX2vxY8RSdcBayEu.htm)||`@Compendium[pf2e.pathfinder-society-boons.tX2vxY8RSdcBayEu]`|
|[S1-19 - Iolite Trainee Hobgoblin](pathfinder-society-boons/p8TKPInhya6oab1w.htm)||`@Compendium[pf2e.pathfinder-society-boons.p8TKPInhya6oab1w]`|
|[S1-20 - Waters of Warlock's Barrow](pathfinder-society-boons/YVOyEfO8tTx4jj2q.htm)||`@Compendium[pf2e.pathfinder-society-boons.YVOyEfO8tTx4jj2q]`|
|[S1-21 - Maze Walker](pathfinder-society-boons/ZZvIzyH6SwyVkEYm.htm)||`@Compendium[pf2e.pathfinder-society-boons.ZZvIzyH6SwyVkEYm]`|
|[S1-22 - Doom Averted](pathfinder-society-boons/iRLU5DTWY16HIqcc.htm)||`@Compendium[pf2e.pathfinder-society-boons.iRLU5DTWY16HIqcc]`|
|[S1-23 - Seasoned Diplomat](pathfinder-society-boons/0CqkYRFUlb0tB9li.htm)||`@Compendium[pf2e.pathfinder-society-boons.0CqkYRFUlb0tB9li]`|
|[S1-25 - Grand Finale](pathfinder-society-boons/PbEqXHWiyzii4g8T.htm)||`@Compendium[pf2e.pathfinder-society-boons.PbEqXHWiyzii4g8T]`|
|[S1-Q01 - Sand Slide](pathfinder-society-boons/WYHPJyza1BJgtFq4.htm)||`@Compendium[pf2e.pathfinder-society-boons.WYHPJyza1BJgtFq4]`|
|[S1-Q02 - One-Who-Waits](pathfinder-society-boons/6gVFdrqhs0mihRao.htm)||`@Compendium[pf2e.pathfinder-society-boons.6gVFdrqhs0mihRao]`|
|[S1-Q02 - Student of the Unforgiving Fire](pathfinder-society-boons/y8sk7Q4uAtacYdR2.htm)||`@Compendium[pf2e.pathfinder-society-boons.y8sk7Q4uAtacYdR2]`|
|[S1-Q03 - Legacy of the Gorget](pathfinder-society-boons/NR19RYJLveHrFYvf.htm)||`@Compendium[pf2e.pathfinder-society-boons.NR19RYJLveHrFYvf]`|
|[S1-Q04 - Fane's Friend](pathfinder-society-boons/prHShOQxNpLXwlss.htm)||`@Compendium[pf2e.pathfinder-society-boons.prHShOQxNpLXwlss]`|
|[S1-Q04 - Stella's Associate](pathfinder-society-boons/RGSw5tAc4ALp97Qe.htm)||`@Compendium[pf2e.pathfinder-society-boons.RGSw5tAc4ALp97Qe]`|
|[S1-Q06 - Secrets of the Jistkan Alchemists](pathfinder-society-boons/MLn6UbZsU4PphXLE.htm)||`@Compendium[pf2e.pathfinder-society-boons.MLn6UbZsU4PphXLE]`|
|[S1-Q07 - Amateur Adjuster](pathfinder-society-boons/DD1mlAEaXdM1pBze.htm)||`@Compendium[pf2e.pathfinder-society-boons.DD1mlAEaXdM1pBze]`|
|[S1-Q08 - Numerian Archaeologist](pathfinder-society-boons/yNTePhAPVMguqUVA.htm)||`@Compendium[pf2e.pathfinder-society-boons.yNTePhAPVMguqUVA]`|
|[S1-Q09 - Wayfinder Connections](pathfinder-society-boons/RJYIefY9Skx8CAgw.htm)||`@Compendium[pf2e.pathfinder-society-boons.RJYIefY9Skx8CAgw]`|
|[S1-Q10 - Sewer Dragon Recruit](pathfinder-society-boons/MuhZZkLR8y9eEndN.htm)||`@Compendium[pf2e.pathfinder-society-boons.MuhZZkLR8y9eEndN]`|
|[S1-Q11 - Amateur Genealogist](pathfinder-society-boons/B7fDgC6tcv41lcP7.htm)||`@Compendium[pf2e.pathfinder-society-boons.B7fDgC6tcv41lcP7]`|
|[S1-Q12 - Fruit Basket](pathfinder-society-boons/RLzWznPsb2VpbxTy.htm)||`@Compendium[pf2e.pathfinder-society-boons.RLzWznPsb2VpbxTy]`|
|[S2-00 - Spark of Life (levels 1-2)](pathfinder-society-boons/G7fN5K1TfmNhTb2w.htm)||`@Compendium[pf2e.pathfinder-society-boons.G7fN5K1TfmNhTb2w]`|
|[S2-00 - Spark of Life (levels 3-6)](pathfinder-society-boons/ybrynsFe4a0jJcgz.htm)||`@Compendium[pf2e.pathfinder-society-boons.ybrynsFe4a0jJcgz]`|
|[S2-00 - Spark of Life (levels 7-8)](pathfinder-society-boons/6XUhCWU0h0qbgL5Z.htm)||`@Compendium[pf2e.pathfinder-society-boons.6XUhCWU0h0qbgL5Z]`|
|[S2-03 - Fluent in Cyclops](pathfinder-society-boons/CKyGNdBL8Qt4ysKt.htm)||`@Compendium[pf2e.pathfinder-society-boons.CKyGNdBL8Qt4ysKt]`|
|[S2-08 - Baba Yaga's Tutelage](pathfinder-society-boons/rJfATB750MbndGeP.htm)||`@Compendium[pf2e.pathfinder-society-boons.rJfATB750MbndGeP]`|
|[Secondary Initiation](pathfinder-society-boons/opaFl76Nbz1dITLz.htm)||`@Compendium[pf2e.pathfinder-society-boons.opaFl76Nbz1dITLz]`|
|[Sellback Plan](pathfinder-society-boons/3lHqim7ttvoMmVXW.htm)||`@Compendium[pf2e.pathfinder-society-boons.3lHqim7ttvoMmVXW]`|
|[Society Recruiter](pathfinder-society-boons/KZrXvKlAVcpLgGF8.htm)||`@Compendium[pf2e.pathfinder-society-boons.KZrXvKlAVcpLgGF8]`|
|[Storied Talent](pathfinder-society-boons/M1flDLvjKBH9QPcx.htm)||`@Compendium[pf2e.pathfinder-society-boons.M1flDLvjKBH9QPcx]`|
|[Swift Traveler](pathfinder-society-boons/K21KQsrR3RMcJ1du.htm)||`@Compendium[pf2e.pathfinder-society-boons.K21KQsrR3RMcJ1du]`|
|[Translator](pathfinder-society-boons/rddlbymeG9KbQYQM.htm)||`@Compendium[pf2e.pathfinder-society-boons.rddlbymeG9KbQYQM]`|
|[Unparalleled Scholarship](pathfinder-society-boons/CW73M2apBaQh6wLm.htm)||`@Compendium[pf2e.pathfinder-society-boons.CW73M2apBaQh6wLm]`|
|[Untarnished Reputation](pathfinder-society-boons/Gmzby7whs5O0wuYC.htm)||`@Compendium[pf2e.pathfinder-society-boons.Gmzby7whs5O0wuYC]`|
|[Vault Delver](pathfinder-society-boons/G0hv2CI7TKcUcSVs.htm)||`@Compendium[pf2e.pathfinder-society-boons.G0hv2CI7TKcUcSVs]`|
|[Verdant Wheel Champion](pathfinder-society-boons/1FWpmtZHOLRvEIdp.htm)||`@Compendium[pf2e.pathfinder-society-boons.1FWpmtZHOLRvEIdp]`|
|[Verdant Wheel Champion, Improved](pathfinder-society-boons/cyrntY2zgDyzxQGO.htm)||`@Compendium[pf2e.pathfinder-society-boons.cyrntY2zgDyzxQGO]`|
|[Vigilant Seal Champion](pathfinder-society-boons/6frkVrsCgTgFhMjl.htm)||`@Compendium[pf2e.pathfinder-society-boons.6frkVrsCgTgFhMjl]`|
|[Vigilant Seal Champion, Improved](pathfinder-society-boons/CLRt3iFqWlMwCNVl.htm)||`@Compendium[pf2e.pathfinder-society-boons.CLRt3iFqWlMwCNVl]`|
|[Wayfinder](pathfinder-society-boons/SxUBWlQj6BT5g4Rw.htm)||`@Compendium[pf2e.pathfinder-society-boons.SxUBWlQj6BT5g4Rw]`|
|[Wayfinder, Adamant](pathfinder-society-boons/BEaLe5M23Q7LKTLL.htm)||`@Compendium[pf2e.pathfinder-society-boons.BEaLe5M23Q7LKTLL]`|
|[Wayfinder, Esoteric](pathfinder-society-boons/SujEcE3H5n9kx1PI.htm)||`@Compendium[pf2e.pathfinder-society-boons.SujEcE3H5n9kx1PI]`|
|[Wayfinder, Harmonic](pathfinder-society-boons/4O3qxD3I4LUjl2Vq.htm)||`@Compendium[pf2e.pathfinder-society-boons.4O3qxD3I4LUjl2Vq]`|
|[Wayfinder, Rugged](pathfinder-society-boons/U6QqFDstHNjEeOUS.htm)||`@Compendium[pf2e.pathfinder-society-boons.U6QqFDstHNjEeOUS]`|
|[World Traveller](pathfinder-society-boons/CPcMqEd4H5yIVl3i.htm)||`@Compendium[pf2e.pathfinder-society-boons.CPcMqEd4H5yIVl3i]`|


## Sorts

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Aberrant Whispers](spells/enc-03-qhJfRnkCRrMI4G1O.htm)|Murmures aberrants|`@Compendium[pf2e.spells-srd.qhJfRnkCRrMI4G1O]`|
|[Abundant Step](spells/con-04-Az3PmWnlWSb5ELX9.htm)|Pas chassé|`@Compendium[pf2e.spells-srd.Az3PmWnlWSb5ELX9]`|
|[Abyssal Pact](spells/con-01-tsKnoBuBbKMXkiz5.htm)|Pacte abyssal|`@Compendium[pf2e.spells-srd.tsKnoBuBbKMXkiz5]`|
|[Abyssal Plague](spells/nec-05-vJuaxTd6q11OjGqA.htm)|Peste abyssale|`@Compendium[pf2e.spells-srd.vJuaxTd6q11OjGqA]`|
|[Abyssal Wrath](spells/evo-05-crF4g9jRN1y84MSD.htm)|Colère abyssale|`@Compendium[pf2e.spells-srd.crF4g9jRN1y84MSD]`|
|[Access Lore](spells/div-03-LbPLNWlLCxKCo5gF.htm)|Accès à la connaissance|`@Compendium[pf2e.spells-srd.LbPLNWlLCxKCo5gF]`|
|[Achaekek's Clutch](spells/abj-04-IT1aaqDBAISlHDUV.htm)|Étreinte d'Achaékek|`@Compendium[pf2e.spells-srd.IT1aaqDBAISlHDUV]`|
|[Acid Arrow](spells/evo-02-f8hRqLJaxBVhF1u0.htm)|Flèche acide|`@Compendium[pf2e.spells-srd.f8hRqLJaxBVhF1u0]`|
|[Acid Splash](spells/evo-00-gISYsBFby1TiXfBt.htm)|Aspersion d’acide|`@Compendium[pf2e.spells-srd.gISYsBFby1TiXfBt]`|
|[Acid Storm](spells/evo-05-ZW8ovbu1etdfMre3.htm)|Tempête d'acide|`@Compendium[pf2e.spells-srd.ZW8ovbu1etdfMre3]`|
|[Acidic Burst](spells/evo-01-rnNGALRtsjspFTws.htm)|Explosion d'acide|`@Compendium[pf2e.spells-srd.rnNGALRtsjspFTws]`|
|[Adapt Self](spells/trs-01-GUnw9YXaW3YyaCAU.htm)|Adaptation de soi|`@Compendium[pf2e.spells-srd.GUnw9YXaW3YyaCAU]`|
|[Adaptive Ablation](spells/abj-04-GoKkejPj5yWJPIPK.htm)|Ablation adaptative|`@Compendium[pf2e.spells-srd.GoKkejPj5yWJPIPK]`|
|[Admonishing Ray](spells/nec-01-uToa7ksKAzmEpkKC.htm)|Rayon d'admonestation|`@Compendium[pf2e.spells-srd.uToa7ksKAzmEpkKC]`|
|[Aerial Form](spells/trs-04-NzXpEzcZAjuDTZjK.htm)|Forme aérienne|`@Compendium[pf2e.spells-srd.NzXpEzcZAjuDTZjK]`|
|[Agile Feet](spells/trs-01-cOjlzWerBwbPWVkX.htm)|Pieds Agiles|`@Compendium[pf2e.spells-srd.cOjlzWerBwbPWVkX]`|
|[Agitate](spells/enc-01-r8g7oSumKOHDqJsd.htm)|Agitation|`@Compendium[pf2e.spells-srd.r8g7oSumKOHDqJsd]`|
|[Agonizing Despair](spells/enc-03-nplNt08TvokZUxtR.htm)|Désespoir agonisant|`@Compendium[pf2e.spells-srd.nplNt08TvokZUxtR]`|
|[Air Bubble](spells/con-01-W69zswpj0Trdy5rj.htm)|Bulle d'air|`@Compendium[pf2e.spells-srd.W69zswpj0Trdy5rj]`|
|[Air Walk](spells/trs-04-b5sGjGlBf58f8jn0.htm)|Marche dans les airs|`@Compendium[pf2e.spells-srd.b5sGjGlBf58f8jn0]`|
|[Alarm](spells/abj-01-4WAib3GichxLjp5p.htm)|Alarme|`@Compendium[pf2e.spells-srd.4WAib3GichxLjp5p]`|
|[All is One, One is All](spells/nec-08-gtWxTfMbIN5RHQw6.htm)|Un est tout, tout est un|`@Compendium[pf2e.spells-srd.gtWxTfMbIN5RHQw6]`|
|[Allegro](spells/enc-00-IQchIYUwbsVTa9Mc.htm)|Allégro|`@Compendium[pf2e.spells-srd.IQchIYUwbsVTa9Mc]`|
|[Alter Reality](spells/div-10-h8zxY9hTeHtWsBVW.htm)|Altérer la réalité|`@Compendium[pf2e.spells-srd.h8zxY9hTeHtWsBVW]`|
|[Anathematic Reprisal](spells/enc-04-7tR29sQt35NfIWqN.htm)|Anathème et représailles|`@Compendium[pf2e.spells-srd.7tR29sQt35NfIWqN]`|
|[Ancestral Defense](spells/abj-03-J5KrjQKCg2PrF1vz.htm)|Défense ancestrale|`@Compendium[pf2e.spells-srd.J5KrjQKCg2PrF1vz]`|
|[Ancestral Form](spells/trs-06-xEjGEBvTfDJECSki.htm)|Forme ancestrale|`@Compendium[pf2e.spells-srd.xEjGEBvTfDJECSki]`|
|[Ancestral Memories](spells/div-01-dtOUkMC57izf93z5.htm)|Souvenirs ancestraux|`@Compendium[pf2e.spells-srd.dtOUkMC57izf93z5]`|
|[Ancestral Touch](spells/enc-01-d2pi7laQkzlr3wrS.htm)|Toucher ancestral|`@Compendium[pf2e.spells-srd.d2pi7laQkzlr3wrS]`|
|[Angelic Halo](spells/abj-01-w3uGXDVEdbLFZVO0.htm)|Halo angélique|`@Compendium[pf2e.spells-srd.w3uGXDVEdbLFZVO0]`|
|[Angelic Messenger](spells/con-01-joEruBVz31Uxczzq.htm)|Messager angélique|`@Compendium[pf2e.spells-srd.joEruBVz31Uxczzq]`|
|[Angelic Wings](spells/evo-03-kRsmUlSWhi6PJvZ7.htm)|Ailes d'ange|`@Compendium[pf2e.spells-srd.kRsmUlSWhi6PJvZ7]`|
|[Animal Feature](spells/trs-02-vGEgI8e7AW6FQ3tP.htm)|Trait animal|`@Compendium[pf2e.spells-srd.vGEgI8e7AW6FQ3tP]`|
|[Animal Form](spells/trs-02-wp09USMB3GIW1qbp.htm)|Forme animale|`@Compendium[pf2e.spells-srd.wp09USMB3GIW1qbp]`|
|[Animal Messenger](spells/enc-02-yhz9fF69uwRhnHix.htm)|Messager animal|`@Compendium[pf2e.spells-srd.yhz9fF69uwRhnHix]`|
|[Animal Vision](spells/div-03-1HfusQ8NDWutGvMx.htm)|Vision animale|`@Compendium[pf2e.spells-srd.1HfusQ8NDWutGvMx]`|
|[Animate Dead](spells/nec-01-9WGeBwIIbbUuWKq0.htm)|Animation des morts|`@Compendium[pf2e.spells-srd.9WGeBwIIbbUuWKq0]`|
|[Animate Object](spells/trs-02-sLzPzk7DJnfuORJ0.htm)|Animation d'objet|`@Compendium[pf2e.spells-srd.sLzPzk7DJnfuORJ0]`|
|[Animate Rope](spells/trs-01-rVANhQgB8Uqi9PTl.htm)|Animation de corde|`@Compendium[pf2e.spells-srd.rVANhQgB8Uqi9PTl]`|
|[Animated Assault](spells/evo-02-WqPhJNzLa8vSjrH6.htm)|Assaut animé|`@Compendium[pf2e.spells-srd.WqPhJNzLa8vSjrH6]`|
|[Animus Mine](spells/abj-02-fWU7Qjp1JiX9g6eg.htm)|Mine psychique|`@Compendium[pf2e.spells-srd.fWU7Qjp1JiX9g6eg]`|
|[Ant Haul](spells/trs-01-X9dkmh23lFwMjrYd.htm)|Charge de fourmi|`@Compendium[pf2e.spells-srd.X9dkmh23lFwMjrYd]`|
|[Anticipate Peril](spells/div-01-EUMjrJJwSgsqNidi.htm)|Anticipation du danger|`@Compendium[pf2e.spells-srd.EUMjrJJwSgsqNidi]`|
|[Antimagic Field](spells/abj-08-My7FvAoLYgGDDBzy.htm)|Champ d'antimagie|`@Compendium[pf2e.spells-srd.My7FvAoLYgGDDBzy]`|
|[Apex Companion](spells/trs-10-qqQYrXaRJXr7uc4i.htm)|Compagnon alpha|`@Compendium[pf2e.spells-srd.qqQYrXaRJXr7uc4i]`|
|[Appearance of Wealth](spells/ill-01-ZeftDoh0nFAXBAWY.htm)|Richesse apparente|`@Compendium[pf2e.spells-srd.ZeftDoh0nFAXBAWY]`|
|[Aqueous Orb](spells/con-03-oUDNCArkQTdhllxD.htm)|Orbe aqueuse|`@Compendium[pf2e.spells-srd.oUDNCArkQTdhllxD]`|
|[Arcane Countermeasure](spells/abj-05-h47yv6j6x1pUtzlr.htm)|Contre-mesure arcanique|`@Compendium[pf2e.spells-srd.h47yv6j6x1pUtzlr]`|
|[Armor of Bones](spells/nec-03-pSNLufPPsReKQtJR.htm)|Armure d'os|`@Compendium[pf2e.spells-srd.pSNLufPPsReKQtJR]`|
|[Aromatic Lure](spells/enc-04-LX4pCagYLpc9hEji.htm)|Leurre aromatique|`@Compendium[pf2e.spells-srd.LX4pCagYLpc9hEji]`|
|[Artistic Flourish](spells/trs-04-eb4FXf62NYArTqek.htm)|Fioritures artistiques|`@Compendium[pf2e.spells-srd.eb4FXf62NYArTqek]`|
|[Asterism](spells/evo-04-KG7amdeXWc7MjGXe.htm)|Étoile|`@Compendium[pf2e.spells-srd.KG7amdeXWc7MjGXe]`|
|[Astral Projection](spells/div-05-4Cntq9odgW6xMpAs.htm)|Projection astrale|`@Compendium[pf2e.spells-srd.4Cntq9odgW6xMpAs]`|
|[Athletic Rush](spells/trs-01-UGJzJRJDoonfWqqI.htm)|Athlétisme poussé|`@Compendium[pf2e.spells-srd.UGJzJRJDoonfWqqI]`|
|[Atone](spells/abj-04-7Fd4lxozd11MQ55N.htm)|Pénitence|`@Compendium[pf2e.spells-srd.7Fd4lxozd11MQ55N]`|
|[Augment Summoning](spells/con-01-tWzxuJdbXqvskdIo.htm)|Convocation améliorée|`@Compendium[pf2e.spells-srd.tWzxuJdbXqvskdIo]`|
|[Augury](spells/div-02-41TZEjhO6D1nWw2X.htm)|Augure|`@Compendium[pf2e.spells-srd.41TZEjhO6D1nWw2X]`|
|[Avatar](spells/trs-10-ckUOoqOM7Kg7VqxB.htm)|Avatar|`@Compendium[pf2e.spells-srd.ckUOoqOM7Kg7VqxB]`|
|[Awaken Animal](spells/div-06-B4dDkYsHFo1H0CIF.htm)|Éveil d’animal|`@Compendium[pf2e.spells-srd.B4dDkYsHFo1H0CIF]`|
|[Awaken Portal](spells/con-03-AkF4yFcSCdhoULyZ.htm)|Éveiller un portail|`@Compendium[pf2e.spells-srd.AkF4yFcSCdhoULyZ]`|
|[Baleful Polymorph](spells/trs-06-dN8QBNuTiaBHCKUe.htm)|Métamorphose funeste|`@Compendium[pf2e.spells-srd.dN8QBNuTiaBHCKUe]`|
|[Bane](spells/enc-01-7ZinJNzxq0XF0oMx.htm)|Imprécation|`@Compendium[pf2e.spells-srd.7ZinJNzxq0XF0oMx]`|
|[Banishment](spells/abj-05-bay4AfSu2iIozNNW.htm)|Bannissement|`@Compendium[pf2e.spells-srd.bay4AfSu2iIozNNW]`|
|[Barkskin](spells/abj-02-YWrfKetOqDwVFut7.htm)|Peau d'écorce|`@Compendium[pf2e.spells-srd.YWrfKetOqDwVFut7]`|
|[Battlefield Persistence](spells/div-03-2jWVNdVlbJq84dfT.htm)|Persévérance sur le champ de bataille|`@Compendium[pf2e.spells-srd.2jWVNdVlbJq84dfT]`|
|[Beastmaster Trance](spells/div-03-5WM3WjshXgrkVCg6.htm)|Transe du Maître des bêtes|`@Compendium[pf2e.spells-srd.5WM3WjshXgrkVCg6]`|
|[Befuddle](spells/enc-01-pBevG6bSQOiyflev.htm)|Embrouiller|`@Compendium[pf2e.spells-srd.pBevG6bSQOiyflev]`|
|[Bestial Curse](spells/trs-04-VuPDHoVEPLbMfCJC.htm)|Malédiction bestiale|`@Compendium[pf2e.spells-srd.VuPDHoVEPLbMfCJC]`|
|[Bind Soul](spells/nec-09-GYmXvS9NJ7QwfWGg.htm)|Âme prisonnière|`@Compendium[pf2e.spells-srd.GYmXvS9NJ7QwfWGg]`|
|[Bind Undead](spells/nec-03-GUeRTriJkMlMlVrk.htm)|Lier un mort-vivant|`@Compendium[pf2e.spells-srd.GUeRTriJkMlMlVrk]`|
|[Bit of Luck](spells/div-01-Fr2CGvWcgSyLcUi7.htm)|Un peu de chance|`@Compendium[pf2e.spells-srd.Fr2CGvWcgSyLcUi7]`|
|[Black Tentacles](spells/con-05-2w4OpAGihn1JSHFD.htm)|Tentacules noirs|`@Compendium[pf2e.spells-srd.2w4OpAGihn1JSHFD]`|
|[Blade Barrier](spells/evo-06-peCF6VArm8urfwxZ.htm)|Barrière de lames|`@Compendium[pf2e.spells-srd.peCF6VArm8urfwxZ]`|
|[Blanket Of Stars](spells/ill-06-m34WOIGZCEg1h76G.htm)|Couverture d'étoiles|`@Compendium[pf2e.spells-srd.m34WOIGZCEg1h76G]`|
|[Bless](spells/enc-01-XSujb7EsSwKl19Uu.htm)|Bénédiction|`@Compendium[pf2e.spells-srd.XSujb7EsSwKl19Uu]`|
|[Blight](spells/nec-04-7yWXx3qC4eFNHhxD.htm)|Flétrissement végétal|`@Compendium[pf2e.spells-srd.7yWXx3qC4eFNHhxD]`|
|[Blightburn Blast](spells/evo-07-EgkypvUZIZkx1UlQ.htm)|Explosion de fléau brûlant|`@Compendium[pf2e.spells-srd.EgkypvUZIZkx1UlQ]`|
|[Blind Ambition](spells/enc-01-Dj44lViYKvOJ8a53.htm)|Ambition aveugle|`@Compendium[pf2e.spells-srd.Dj44lViYKvOJ8a53]`|
|[Blinding Beauty](spells/enc-03-Q56HLIHVKY6bC5W3.htm)|Beauté aveuglante|`@Compendium[pf2e.spells-srd.Q56HLIHVKY6bC5W3]`|
|[Blinding Fury](spells/enc-06-NhGXgmI3AjkkwnPk.htm)|Furie aveuglante|`@Compendium[pf2e.spells-srd.NhGXgmI3AjkkwnPk]`|
|[Blindness](spells/nec-03-VosLNn2M8S7JH67D.htm)|Cécité|`@Compendium[pf2e.spells-srd.VosLNn2M8S7JH67D]`|
|[Blink](spells/con-04-zR67Rt3UMHKC5evy.htm)|Clignotement|`@Compendium[pf2e.spells-srd.zR67Rt3UMHKC5evy]`|
|[Blister](spells/nec-05-59NR1hA2jPSgg2sW.htm)|Enveloppe|`@Compendium[pf2e.spells-srd.59NR1hA2jPSgg2sW]`|
|[Blistering Invective](spells/enc-02-hoR6w8BqX2F35Tdx.htm)|Invective torrides|`@Compendium[pf2e.spells-srd.hoR6w8BqX2F35Tdx]`|
|[Blood Feast](spells/nec-05-ES6FkwXXqYr4ujQH.htm)|Festin de sang|`@Compendium[pf2e.spells-srd.ES6FkwXXqYr4ujQH]`|
|[Blood Vendetta](spells/nec-02-TaaMEYdZXQXF0Sks.htm)|Vendetta du sang|`@Compendium[pf2e.spells-srd.TaaMEYdZXQXF0Sks]`|
|[Blood Ward](spells/abj-01-dFejDNEmVj3CwYLL.htm)|Protection du sang|`@Compendium[pf2e.spells-srd.dFejDNEmVj3CwYLL]`|
|[Blur](spells/ill-02-3JG1t3T4mWn6vTke.htm)|Flou|`@Compendium[pf2e.spells-srd.3JG1t3T4mWn6vTke]`|
|[Bottle the Storm](spells/evo-04-VUwpDY4Z91s9QCg0.htm)||`@Compendium[pf2e.spells-srd.VUwpDY4Z91s9QCg0]`|
|[Brain Drain](spells/enc-01-k2QrUk7jWMAWozMh.htm)|Drain du cerveau|`@Compendium[pf2e.spells-srd.k2QrUk7jWMAWozMh]`|
|[Brand the Impenitent](spells/abj-02-Y8cSjhU33oUqccxJ.htm)|Marquer l'impénitent|`@Compendium[pf2e.spells-srd.Y8cSjhU33oUqccxJ]`|
|[Breath of Drought](spells/evo-02-Wt94cw03L77sbud7.htm)||`@Compendium[pf2e.spells-srd.Wt94cw03L77sbud7]`|
|[Breath of Life](spells/nec-05-Hnc7eGi7vyZenAIm.htm)|Souffle de vie|`@Compendium[pf2e.spells-srd.Hnc7eGi7vyZenAIm]`|
|[Burning Hands](spells/evo-01-y6rAdMK6EFlV6U0t.htm)|Mains brûlantes|`@Compendium[pf2e.spells-srd.y6rAdMK6EFlV6U0t]`|
|[Cackle](spells/enc-01-YVK3JUkPVzHIeGXQ.htm)|Gloussement|`@Compendium[pf2e.spells-srd.YVK3JUkPVzHIeGXQ]`|
|[Call Spirit](spells/nec-05-gsYEuWv04XTDxe91.htm)|Appel d'un esprit|`@Compendium[pf2e.spells-srd.gsYEuWv04XTDxe91]`|
|[Call of the Grave](spells/nec-01-BH3sUerzMb2bWnv1.htm)|Appel de la tombe|`@Compendium[pf2e.spells-srd.BH3sUerzMb2bWnv1]`|
|[Call to Arms](spells/enc-01-dqaCLzINHBiKjh4J.htm)|Appel aux armes|`@Compendium[pf2e.spells-srd.dqaCLzINHBiKjh4J]`|
|[Calm Emotions](spells/enc-02-b515AZlB0sridKSq.htm)|Apaisement des émotions|`@Compendium[pf2e.spells-srd.b515AZlB0sridKSq]`|
|[Captivating Adoration](spells/enc-04-jJphHQlENHFlSElH.htm)|Adoration captivante|`@Compendium[pf2e.spells-srd.jJphHQlENHFlSElH]`|
|[Cataclysm](spells/evo-10-wLIvH0AT1u7oa64N.htm)|Cataclysme|`@Compendium[pf2e.spells-srd.wLIvH0AT1u7oa64N]`|
|[Celestial Brand](spells/nec-05-9WlTR9JlEcjRmGiD.htm)|Marque céleste|`@Compendium[pf2e.spells-srd.9WlTR9JlEcjRmGiD]`|
|[Chain Lightning](spells/evo-06-TDNlDWbYb58Y55Da.htm)|Eclairs multiples|`@Compendium[pf2e.spells-srd.TDNlDWbYb58Y55Da]`|
|[Chameleon Coat](spells/ill-05-xPrbxyOEwy9QaPVn.htm)||`@Compendium[pf2e.spells-srd.xPrbxyOEwy9QaPVn]`|
|[Champion's Sacrifice](spells/abj-06-USM530HlzZ1RMd99.htm)|Sacrifice du Champion|`@Compendium[pf2e.spells-srd.USM530HlzZ1RMd99]`|
|[Charged Javelin](spells/evo-01-g1eY1vN44mgluE33.htm)||`@Compendium[pf2e.spells-srd.g1eY1vN44mgluE33]`|
|[Charitable Urge](spells/enc-02-UirEIHILQgip87qv.htm)|Désir Charitable|`@Compendium[pf2e.spells-srd.UirEIHILQgip87qv]`|
|[Charm](spells/enc-01-vLA0q0WOK2YPuJs6.htm)|Charme|`@Compendium[pf2e.spells-srd.vLA0q0WOK2YPuJs6]`|
|[Charming Touch](spells/enc-01-ut9IhJ9jSZSHDUop.htm)|Contact charmeur|`@Compendium[pf2e.spells-srd.ut9IhJ9jSZSHDUop]`|
|[Charming Words](spells/enc-01-KMFRKzNCq7hVNH7H.htm)|Paroles charmeuses|`@Compendium[pf2e.spells-srd.KMFRKzNCq7hVNH7H]`|
|[Chill Touch](spells/nec-00-mAMEt4FFbdqoRnkN.htm)|Contact glacial|`@Compendium[pf2e.spells-srd.mAMEt4FFbdqoRnkN]`|
|[Chilling Darkness](spells/evo-03-sRfSBHWHdbIa0aGc.htm)|Ténèbres glaciales|`@Compendium[pf2e.spells-srd.sRfSBHWHdbIa0aGc]`|
|[Chilling Spray](spells/evo-01-8TQiFzGf4feoHeH0.htm)||`@Compendium[pf2e.spells-srd.8TQiFzGf4feoHeH0]`|
|[Chroma Leach](spells/nec-04-4y2DMq9DV5HvmnxC.htm)||`@Compendium[pf2e.spells-srd.4y2DMq9DV5HvmnxC]`|
|[Chromatic Wall](spells/abj-05-forsqeofEszBNtLq.htm)|Mur chromatique|`@Compendium[pf2e.spells-srd.forsqeofEszBNtLq]`|
|[Circle of Protection](spells/abj-03-mpGCMTldMVa0pWYs.htm)|Cercle de protection|`@Compendium[pf2e.spells-srd.mpGCMTldMVa0pWYs]`|
|[Claim Undead](spells/nec-06-GzN9bG6cKZ96YC6l.htm)||`@Compendium[pf2e.spells-srd.GzN9bG6cKZ96YC6l]`|
|[Clairaudience](spells/div-03-HXhWYJviWalN5tQ2.htm)|Clairaudience|`@Compendium[pf2e.spells-srd.HXhWYJviWalN5tQ2]`|
|[Clairvoyance](spells/div-04-zvKWclOZ7A53DObE.htm)|Clairvoyance|`@Compendium[pf2e.spells-srd.zvKWclOZ7A53DObE]`|
|[Clinging Ice](spells/con-00-MraZBLJ4Be3ogmWL.htm)|Glace tenace|`@Compendium[pf2e.spells-srd.MraZBLJ4Be3ogmWL]`|
|[Clinging Shadows Stance](spells/evo-04-uJXs4M6IeixfPBLc.htm)|Posture des ombres tenaces|`@Compendium[pf2e.spells-srd.uJXs4M6IeixfPBLc]`|
|[Cloak of Colors](spells/ill-05-TCk2MDwf5L5OYjFC.htm)|Cape colorée|`@Compendium[pf2e.spells-srd.TCk2MDwf5L5OYjFC]`|
|[Cloak of Shadow](spells/evo-01-SE0fbgBj7atuukdv.htm)|Manteau d'ombre|`@Compendium[pf2e.spells-srd.SE0fbgBj7atuukdv]`|
|[Clone](spells/nec-09-pZr1xrCpaSu6qrXU.htm)||`@Compendium[pf2e.spells-srd.pZr1xrCpaSu6qrXU]`|
|[Cloudkill](spells/nec-05-MlpbeZ61Euhl0d60.htm)|Nuage mortel|`@Compendium[pf2e.spells-srd.MlpbeZ61Euhl0d60]`|
|[Clownish Curse](spells/enc-04-S7ylpCJyq0CYkux9.htm)||`@Compendium[pf2e.spells-srd.S7ylpCJyq0CYkux9]`|
|[Collective Transposition](spells/con-06-c3XygMbzrZMgV1y3.htm)|Transposition collective|`@Compendium[pf2e.spells-srd.c3XygMbzrZMgV1y3]`|
|[Color Spray](spells/ill-01-UKsIOWmMx4hSpafl.htm)|Couleurs dansantes|`@Compendium[pf2e.spells-srd.UKsIOWmMx4hSpafl]`|
|[Command](spells/enc-01-aIHY2DArKFweIrpf.htm)|Injonction|`@Compendium[pf2e.spells-srd.aIHY2DArKFweIrpf]`|
|[Commanding Lash](spells/enc-04-4DaHIgtMBTyxebY3.htm)|Fouet du maître|`@Compendium[pf2e.spells-srd.4DaHIgtMBTyxebY3]`|
|[Commune](spells/div-06-7DN13ILADW2N9Z1t.htm)|Communion|`@Compendium[pf2e.spells-srd.7DN13ILADW2N9Z1t]`|
|[Commune with Nature](spells/div-06-l4LFwY7iuzX6sDXr.htm)|Communion avec la nature|`@Compendium[pf2e.spells-srd.l4LFwY7iuzX6sDXr]`|
|[Competitive Edge](spells/enc-04-NOB92Wpn7jXvtyVW.htm)|Avantage du compétiteur|`@Compendium[pf2e.spells-srd.NOB92Wpn7jXvtyVW]`|
|[Comprehend Language](spells/div-02-vTQvfYu2llKQedmY.htm)|Compréhension des langues|`@Compendium[pf2e.spells-srd.vTQvfYu2llKQedmY]`|
|[Cone of Cold](spells/evo-05-3puDanGfpEt6jK5k.htm)|Cône de froid|`@Compendium[pf2e.spells-srd.3puDanGfpEt6jK5k]`|
|[Confusion](spells/enc-04-LiGbewa9pO0yjbsY.htm)|Confusion|`@Compendium[pf2e.spells-srd.LiGbewa9pO0yjbsY]`|
|[Consecrate](spells/evo-02-Dbd5W6G8U2vzWolN.htm)|consécration|`@Compendium[pf2e.spells-srd.Dbd5W6G8U2vzWolN]`|
|[Consuming Darkness](spells/evo-05-HMTloW1hvRFJ5Z2D.htm)|Obscurité dévorante|`@Compendium[pf2e.spells-srd.HMTloW1hvRFJ5Z2D]`|
|[Contingency](spells/abj-07-WG91Z5TiR6oO5FOw.htm)|Contingence|`@Compendium[pf2e.spells-srd.WG91Z5TiR6oO5FOw]`|
|[Continual Flame](spells/evo-02-ynm8JIU3sc3qUMpa.htm)|Flamme éternelle|`@Compendium[pf2e.spells-srd.ynm8JIU3sc3qUMpa]`|
|[Control Sand](spells/evo-07-oahqARSgOGDRybBQ.htm)||`@Compendium[pf2e.spells-srd.oahqARSgOGDRybBQ]`|
|[Control Water](spells/evo-05-zfn5RqAdF63neqpP.htm)|Contrôle de l'eau|`@Compendium[pf2e.spells-srd.zfn5RqAdF63neqpP]`|
|[Control Weather](spells/evo-08-XkDCzMIyc0YOjw05.htm)|Contrôle du climat|`@Compendium[pf2e.spells-srd.XkDCzMIyc0YOjw05]`|
|[Counter Performance](spells/enc-01-WILXkjU5Yq3yw10r.htm)|Contre-représentation|`@Compendium[pf2e.spells-srd.WILXkjU5Yq3yw10r]`|
|[Countless Eyes](spells/div-04-ykyKclKTCMp2SFXa.htm)|Yeux innombrables|`@Compendium[pf2e.spells-srd.ykyKclKTCMp2SFXa]`|
|[Cozy Cabin](spells/con-03-mwPfoYfVGSMAaUec.htm)|Cabine douillette|`@Compendium[pf2e.spells-srd.mwPfoYfVGSMAaUec]`|
|[Crashing Wave](spells/evo-03-T4QKmtYPeCgYxVGe.htm)||`@Compendium[pf2e.spells-srd.T4QKmtYPeCgYxVGe]`|
|[Create Demiplane](spells/con-08-ZwwIUavMbEwcZz35.htm)|Création de demi-plan|`@Compendium[pf2e.spells-srd.ZwwIUavMbEwcZz35]`|
|[Create Food](spells/con-02-Mt6ZzkVX8Q4xigFq.htm)|Création de nourriture|`@Compendium[pf2e.spells-srd.Mt6ZzkVX8Q4xigFq]`|
|[Create Skinstitch](spells/trs-06-NCUXsaj2yFxOVhrK.htm)||`@Compendium[pf2e.spells-srd.NCUXsaj2yFxOVhrK]`|
|[Create Undead](spells/nec-02-c3b6LdLlQDPngNIb.htm)|Création de mort-vivant|`@Compendium[pf2e.spells-srd.c3b6LdLlQDPngNIb]`|
|[Create Water](spells/con-01-WzLKjSw6hsBhuklC.htm)|Création d'eau|`@Compendium[pf2e.spells-srd.WzLKjSw6hsBhuklC]`|
|[Creation](spells/con-04-TUj8eugNqAvB1vVR.htm)|Création|`@Compendium[pf2e.spells-srd.TUj8eugNqAvB1vVR]`|
|[Crisis of Faith](spells/enc-03-3x6eUCm17n6ROzUa.htm)|Manque de foi|`@Compendium[pf2e.spells-srd.3x6eUCm17n6ROzUa]`|
|[Crusade](spells/enc-09-xFY9RtDE4DQKlWNR.htm)|Croisade|`@Compendium[pf2e.spells-srd.xFY9RtDE4DQKlWNR]`|
|[Crushing Despair](spells/enc-05-GaRQlC9Yw1BGKHfN.htm)|Désespoir foudroyant|`@Compendium[pf2e.spells-srd.GaRQlC9Yw1BGKHfN]`|
|[Cry of Destruction](spells/evo-01-0H1ozccQGGFLUwFI.htm)|Cri de destruction|`@Compendium[pf2e.spells-srd.0H1ozccQGGFLUwFI]`|
|[Cup of Dust](spells/nec-03-10siFBMF4pIDhVmf.htm)||`@Compendium[pf2e.spells-srd.10siFBMF4pIDhVmf]`|
|[Curse Of Lost Time](spells/trs-03-B3eLlbaPxOYHcs1o.htm)||`@Compendium[pf2e.spells-srd.B3eLlbaPxOYHcs1o]`|
|[Curse of Death](spells/nec-05-nQS4vPm5zprqkzFZ.htm)||`@Compendium[pf2e.spells-srd.nQS4vPm5zprqkzFZ]`|
|[Daemonic Pact](spells/con-01-Vpohy4XH1DaH95hT.htm)|PActe daemoniaque|`@Compendium[pf2e.spells-srd.Vpohy4XH1DaH95hT]`|
|[Dance of Darkness](spells/evo-05-9BGEf9Sv5rgNBCk0.htm)|Danse de ténèbre|`@Compendium[pf2e.spells-srd.9BGEf9Sv5rgNBCk0]`|
|[Dancing Lights](spells/evo-00-kl2q6JvBZwed4B6v.htm)|Lumières dansantes|`@Compendium[pf2e.spells-srd.kl2q6JvBZwed4B6v]`|
|[Darkened Eyes](spells/trs-04-oiUhJbJ3YCKF62Fu.htm)|Assombrir le regard|`@Compendium[pf2e.spells-srd.oiUhJbJ3YCKF62Fu]`|
|[Darkness](spells/evo-02-4GE2ZdODgIQtg51c.htm)|Ténèbres|`@Compendium[pf2e.spells-srd.4GE2ZdODgIQtg51c]`|
|[Darkvision](spells/div-02-pZTqGY1MLRjgKasV.htm)|Vision dans le noir|`@Compendium[pf2e.spells-srd.pZTqGY1MLRjgKasV]`|
|[Daze](spells/enc-00-4gBIw4IDrSfFHik4.htm)|Hébétement|`@Compendium[pf2e.spells-srd.4gBIw4IDrSfFHik4]`|
|[Dazzling Flash](spells/evo-01-zul5cBTfr7NXHBZf.htm)|Éclair éblouissant|`@Compendium[pf2e.spells-srd.zul5cBTfr7NXHBZf]`|
|[Deafness](spells/nec-02-fZPCv2VHuM2yPbC8.htm)|Surdité|`@Compendium[pf2e.spells-srd.fZPCv2VHuM2yPbC8]`|
|[Death Knell](spells/nec-02-dLdRqT6UxTKlsPgp.htm)|Mise à mort|`@Compendium[pf2e.spells-srd.dLdRqT6UxTKlsPgp]`|
|[Death Ward](spells/abj-05-YvXKGlHOt7mdW2jZ.htm)|Protection contre la mort|`@Compendium[pf2e.spells-srd.YvXKGlHOt7mdW2jZ]`|
|[Death's Call](spells/nec-01-HG4afO9EOGEU9bZN.htm)|Appel de la mort|`@Compendium[pf2e.spells-srd.HG4afO9EOGEU9bZN]`|
|[Debilitating Dichotomy](spells/evo-04-VmqdVWCb8zAUCW8S.htm)|Dichotomie débilitante|`@Compendium[pf2e.spells-srd.VmqdVWCb8zAUCW8S]`|
|[Deceiver's Cloak](spells/ill-03-FrKPwgFxWIGMGgs4.htm)|Cape de tromperie|`@Compendium[pf2e.spells-srd.FrKPwgFxWIGMGgs4]`|
|[Delay Affliction](spells/nec-03-ziHDISWkFSwz3pmn.htm)|Retarder l'affliction|`@Compendium[pf2e.spells-srd.ziHDISWkFSwz3pmn]`|
|[Delay Consequence](spells/abj-01-3wmX7htzOXiHLdAn.htm)|Retarder les conséquences|`@Compendium[pf2e.spells-srd.3wmX7htzOXiHLdAn]`|
|[Delusional Pride](spells/enc-04-J8pL8yTshga8QOk8.htm)|Fierté illusoire|`@Compendium[pf2e.spells-srd.J8pL8yTshga8QOk8]`|
|[Destructive Aura](spells/evo-04-7d4DUTDIlzDa8OvX.htm)|Aura destructrice|`@Compendium[pf2e.spells-srd.7d4DUTDIlzDa8OvX]`|
|[Detect Alignment](spells/div-01-QnTtGCAvdWRU4spv.htm)|Détection de l’alignement|`@Compendium[pf2e.spells-srd.QnTtGCAvdWRU4spv]`|
|[Detect Magic](spells/div-00-gpzpAAAJ1Lza2JVl.htm)|Détection de la magie|`@Compendium[pf2e.spells-srd.gpzpAAAJ1Lza2JVl]`|
|[Detect Poison](spells/div-01-QqxwHeYEVylkYjsO.htm)|Détection du poison|`@Compendium[pf2e.spells-srd.QqxwHeYEVylkYjsO]`|
|[Detect Scrying](spells/div-04-AnWCohzPgK4L9GVl.htm)|Détection de la scrutation|`@Compendium[pf2e.spells-srd.AnWCohzPgK4L9GVl]`|
|[Diabolic Edict](spells/enc-01-Vctwx1ewa8HUOA94.htm)|Édit diabolique|`@Compendium[pf2e.spells-srd.Vctwx1ewa8HUOA94]`|
|[Diamond Dust](spells/evo-04-wjJW9hWY5CkkMvY5.htm)||`@Compendium[pf2e.spells-srd.wjJW9hWY5CkkMvY5]`|
|[Dim the Light](spells/evo-01-6UafOE1ZUbHamsZJ.htm)|Diminuer la lumière|`@Compendium[pf2e.spells-srd.6UafOE1ZUbHamsZJ]`|
|[Dimension Door](spells/con-04-VlNcjmYyu95vOUe8.htm)|Porte dimensionnelle|`@Compendium[pf2e.spells-srd.VlNcjmYyu95vOUe8]`|
|[Dimensional Anchor](spells/abj-04-ksLCg62cLOojw3gN.htm)|Ancre dimensionnelle|`@Compendium[pf2e.spells-srd.ksLCg62cLOojw3gN]`|
|[Dimensional Lock](spells/abj-07-XZE4BawIlTf88Yl9.htm)|Verrou dimensionnel|`@Compendium[pf2e.spells-srd.XZE4BawIlTf88Yl9]`|
|[Dimensional Steps](spells/con-04-zjG6NncHyAKqSF7m.htm)|Pas interdimensionnel|`@Compendium[pf2e.spells-srd.zjG6NncHyAKqSF7m]`|
|[Dinosaur Form](spells/trs-04-KhM8MhoUgoUjBMIz.htm)|Forme de dinosaure|`@Compendium[pf2e.spells-srd.KhM8MhoUgoUjBMIz]`|
|[Dirge of Doom](spells/enc-00-1xLVcA8Y1onw7toT.htm)|Chant funeste|`@Compendium[pf2e.spells-srd.1xLVcA8Y1onw7toT]`|
|[Disappearance](spells/ill-08-wfleiawxsfhpRRwf.htm)|Disparition|`@Compendium[pf2e.spells-srd.wfleiawxsfhpRRwf]`|
|[Discern Lies](spells/div-04-uNsliWpl8Q1JdFcM.htm)|Détection du mensonge|`@Compendium[pf2e.spells-srd.uNsliWpl8Q1JdFcM]`|
|[Discern Location](spells/div-08-CeSh8QcVnqP5OlLj.htm)|Localisation suprême|`@Compendium[pf2e.spells-srd.CeSh8QcVnqP5OlLj]`|
|[Discern Secrets](spells/div-00-UmXhuKrYZR3W16mQ.htm)|Discerner les secrets|`@Compendium[pf2e.spells-srd.UmXhuKrYZR3W16mQ]`|
|[Disintegrate](spells/evo-06-r7ihOgKv19eJQnik.htm)|Désintégration|`@Compendium[pf2e.spells-srd.r7ihOgKv19eJQnik]`|
|[Disjunction](spells/abj-09-ihbRf964JDXztcy3.htm)|Disjonction|`@Compendium[pf2e.spells-srd.ihbRf964JDXztcy3]`|
|[Dismantle](spells/trs-02-jW2asKFchuoxniSH.htm)||`@Compendium[pf2e.spells-srd.jW2asKFchuoxniSH]`|
|[Dispel Magic](spells/abj-02-9HpwDN4MYQJnW0LG.htm)|Dissipation de la magie|`@Compendium[pf2e.spells-srd.9HpwDN4MYQJnW0LG]`|
|[Disperse into Air](spells/trs-04-L8pzCOi7Jzx5ALs9.htm)|Disparition dans les airs|`@Compendium[pf2e.spells-srd.L8pzCOi7Jzx5ALs9]`|
|[Disrupt Undead](spells/nec-00-kcelf6IHl3L9VXXg.htm)|Destruction de mort-vivant|`@Compendium[pf2e.spells-srd.kcelf6IHl3L9VXXg]`|
|[Disrupting Weapons](spells/nec-01-2iQKhCQBijhj5Rf3.htm)|Armes perturbatrices|`@Compendium[pf2e.spells-srd.2iQKhCQBijhj5Rf3]`|
|[Divine Aura](spells/abj-08-nsQvjNyg4Whw2mek.htm)|Aura divine|`@Compendium[pf2e.spells-srd.nsQvjNyg4Whw2mek]`|
|[Divine Decree](spells/evo-07-sX2o0HH4RjJDAZ8C.htm)|Décret divin|`@Compendium[pf2e.spells-srd.sX2o0HH4RjJDAZ8C]`|
|[Divine Inspiration](spells/enc-08-KtTGLbLG9nqMbUYL.htm)|Inspiration divine|`@Compendium[pf2e.spells-srd.KtTGLbLG9nqMbUYL]`|
|[Divine Lance](spells/evo-00-qwZBXN6zBoB9BHXE.htm)|Lance divine|`@Compendium[pf2e.spells-srd.qwZBXN6zBoB9BHXE]`|
|[Divine Plagues](spells/nec-01-NkeLctXo9FLGnDhi.htm)||`@Compendium[pf2e.spells-srd.NkeLctXo9FLGnDhi]`|
|[Divine Vessel](spells/trs-07-hiVL8qsnTJtpouw0.htm)|Réceptacle divin|`@Compendium[pf2e.spells-srd.hiVL8qsnTJtpouw0]`|
|[Divine Wrath](spells/evo-04-hVU9msO9yGkxKZ3J.htm)|Colère divine|`@Compendium[pf2e.spells-srd.hVU9msO9yGkxKZ3J]`|
|[Diviner's Sight](spells/div-01-oFwmdb6LlRrh9AUT.htm)|Vision du devin|`@Compendium[pf2e.spells-srd.oFwmdb6LlRrh9AUT]`|
|[Dominate](spells/enc-06-OsOhx3TGIZ7AhD0P.htm)|Domination|`@Compendium[pf2e.spells-srd.OsOhx3TGIZ7AhD0P]`|
|[Door to Beyond](spells/con-04-iMmexY6ZosLS4I5R.htm)|Porte vers l'au-delà|`@Compendium[pf2e.spells-srd.iMmexY6ZosLS4I5R]`|
|[Downpour](spells/evo-04-K4LXpaBWrGy6jIER.htm)|Déluge|`@Compendium[pf2e.spells-srd.K4LXpaBWrGy6jIER]`|
|[Draconic Barrage](spells/evo-01-S6Kkk15MWGqzC00a.htm)||`@Compendium[pf2e.spells-srd.S6Kkk15MWGqzC00a]`|
|[Dragon Breath](spells/evo-03-JcobNl4iE9HmMYtE.htm)|Souffle de dragon|`@Compendium[pf2e.spells-srd.JcobNl4iE9HmMYtE]`|
|[Dragon Claws](spells/trs-01-K8vvrOgW4bGakXxm.htm)|Griffes de dragon|`@Compendium[pf2e.spells-srd.K8vvrOgW4bGakXxm]`|
|[Dragon Form](spells/trs-06-5c692cCcTDXjSEzk.htm)|Forme de dragon|`@Compendium[pf2e.spells-srd.5c692cCcTDXjSEzk]`|
|[Dragon Wings](spells/trs-05-HWJODX2zPg5cg34F.htm)|Ailes de dragon|`@Compendium[pf2e.spells-srd.HWJODX2zPg5cg34F]`|
|[Drain Life](spells/nec-03-cqdmSmQnM0q6wbWG.htm)|Drain de vie|`@Compendium[pf2e.spells-srd.cqdmSmQnM0q6wbWG]`|
|[Dread Aura](spells/enc-04-KPGGkyBFbKse7KpK.htm)|Aura effroyable|`@Compendium[pf2e.spells-srd.KPGGkyBFbKse7KpK]`|
|[Dread Secret](spells/div-06-XULNb8ItUsfupxqH.htm)|Secret effroyable|`@Compendium[pf2e.spells-srd.XULNb8ItUsfupxqH]`|
|[Dream Council](spells/ill-08-rwCh2qTYPA44KEoK.htm)|Conseil onirique|`@Compendium[pf2e.spells-srd.rwCh2qTYPA44KEoK]`|
|[Dream Message](spells/enc-03-yM3KTTSAIHhyuP14.htm)|Message onirique|`@Compendium[pf2e.spells-srd.yM3KTTSAIHhyuP14]`|
|[Dreamer's Call](spells/enc-04-PztLrElcZfLwRnEq.htm)|Appel du rêveur|`@Compendium[pf2e.spells-srd.PztLrElcZfLwRnEq]`|
|[Dreaming Potential](spells/enc-05-y0Vy7iNL3ET8K00C.htm)|Potentiel onirique|`@Compendium[pf2e.spells-srd.y0Vy7iNL3ET8K00C]`|
|[Drop Dead](spells/ill-05-scTRIrTfXquVYHGw.htm)|Raide mort|`@Compendium[pf2e.spells-srd.scTRIrTfXquVYHGw]`|
|[Dull Ambition](spells/enc-04-MLdMUOdwSKegOGlo.htm)||`@Compendium[pf2e.spells-srd.MLdMUOdwSKegOGlo]`|
|[Duplicate Foe](spells/con-07-73rToy0v5Ra9NvL6.htm)|Dupliquer l'ennemi|`@Compendium[pf2e.spells-srd.73rToy0v5Ra9NvL6]`|
|[Dust Storm](spells/con-04-K1wmI4qPmRhFczmy.htm)|Tempête de poussière|`@Compendium[pf2e.spells-srd.K1wmI4qPmRhFczmy]`|
|[Dutiful Challenge](spells/abj-04-caehfpQz7yp9yNzz.htm)|Défi respectueux|`@Compendium[pf2e.spells-srd.caehfpQz7yp9yNzz]`|
|[Déjà Vu](spells/enc-01-rerNA6YZsdxuJYt3.htm)|Déjà vu|`@Compendium[pf2e.spells-srd.rerNA6YZsdxuJYt3]`|
|[Earthbind](spells/trs-03-gPvtmKMRpg9I9D7H.htm)|Cloué à terre|`@Compendium[pf2e.spells-srd.gPvtmKMRpg9I9D7H]`|
|[Earthquake](spells/evo-08-x7SPrsRxGb2Vy2nu.htm)|Tremblement de terre|`@Compendium[pf2e.spells-srd.x7SPrsRxGb2Vy2nu]`|
|[Eclipse Burst](spells/nec-07-0jadeyQIItIuRgeH.htm)|Eclipse soudaine|`@Compendium[pf2e.spells-srd.0jadeyQIItIuRgeH]`|
|[Ectoplasmic Interstice](spells/nec-04-QE9f3OxvvBThymD4.htm)||`@Compendium[pf2e.spells-srd.QE9f3OxvvBThymD4]`|
|[Eject Soul](spells/nec-01-rVhHaWqUsVUO4GuY.htm)||`@Compendium[pf2e.spells-srd.rVhHaWqUsVUO4GuY]`|
|[Electric Arc](spells/evo-00-kBhaPuzLUSwS6vVf.htm)|Arc électrique|`@Compendium[pf2e.spells-srd.kBhaPuzLUSwS6vVf]`|
|[Electrified Crystal Ward](spells/evo-03-57ulIxg3Of2wCbEh.htm)|Glyphe de cristal électrique|`@Compendium[pf2e.spells-srd.57ulIxg3Of2wCbEh]`|
|[Elemental Betrayal](spells/evo-01-f9uqHnNBMU0774SF.htm)||`@Compendium[pf2e.spells-srd.f9uqHnNBMU0774SF]`|
|[Elemental Blast](spells/evo-05-Qlp8G3knwLGhAxQ0.htm)|Explosion élémentaire|`@Compendium[pf2e.spells-srd.Qlp8G3knwLGhAxQ0]`|
|[Elemental Form](spells/trs-05-1K6AYGisvo9gqdhs.htm)|Forme élémentaire|`@Compendium[pf2e.spells-srd.1K6AYGisvo9gqdhs]`|
|[Elemental Motion](spells/evo-03-B8aCUMCHCIMUCEVK.htm)|Mobilité élémentaire|`@Compendium[pf2e.spells-srd.B8aCUMCHCIMUCEVK]`|
|[Elemental Tempest](spells/evo-04-vfHr1N8Rf2bBpdgn.htm)|Tempête élémentaire|`@Compendium[pf2e.spells-srd.vfHr1N8Rf2bBpdgn]`|
|[Elemental Toss](spells/evo-01-0JUOgbbFCapp3HlW.htm)|Projectile élémentaire|`@Compendium[pf2e.spells-srd.0JUOgbbFCapp3HlW]`|
|[Embrace the Pit](spells/trs-03-ilGsyGLGjjIPHbyP.htm)|Étreinte de la fosse|`@Compendium[pf2e.spells-srd.ilGsyGLGjjIPHbyP]`|
|[Empty Body](spells/con-09-mau1Olq58ECF0ZPi.htm)|Désertion de l'âme|`@Compendium[pf2e.spells-srd.mau1Olq58ECF0ZPi]`|
|[Empty Inside](spells/abj-01-0cF9HvHzzWSbCFBP.htm)|Vide intérieur|`@Compendium[pf2e.spells-srd.0cF9HvHzzWSbCFBP]`|
|[Endure](spells/enc-01-4c1c6eNzU1PFGkAy.htm)|Endurer|`@Compendium[pf2e.spells-srd.4c1c6eNzU1PFGkAy]`|
|[Endure Elements](spells/abj-02-5esP2GVzvxWsMgaX.htm)|Endurance aux éléments|`@Compendium[pf2e.spells-srd.5esP2GVzvxWsMgaX]`|
|[Enduring Might](spells/abj-04-eG1fBodYwolaXK98.htm)|Puissance protectrice|`@Compendium[pf2e.spells-srd.eG1fBodYwolaXK98]`|
|[Energy Absorption](spells/abj-04-LoBjvguamA12iyW0.htm)|Absorption d'énergie|`@Compendium[pf2e.spells-srd.LoBjvguamA12iyW0]`|
|[Energy Aegis](spells/abj-07-m2xFMNyQiUKQDRaj.htm)|Égide énergétique|`@Compendium[pf2e.spells-srd.m2xFMNyQiUKQDRaj]`|
|[Enervation](spells/nec-04-eexkxcqnkXazsGfK.htm)||`@Compendium[pf2e.spells-srd.eexkxcqnkXazsGfK]`|
|[Enhance Victuals](spells/trs-02-rdTEF1hfAWbN58NE.htm)|Amélioration des victuailles|`@Compendium[pf2e.spells-srd.rdTEF1hfAWbN58NE]`|
|[Enlarge](spells/trs-02-wzctak6BxOW8xvFV.htm)|Agrandissement|`@Compendium[pf2e.spells-srd.wzctak6BxOW8xvFV]`|
|[Enlarge Companion](spells/trs-04-8rj45fKzCFcB0fxs.htm)|Compagnon aggrandi|`@Compendium[pf2e.spells-srd.8rj45fKzCFcB0fxs]`|
|[Entangle](spells/trs-02-J6vNvrUT3b1hx2iA.htm)|Enchevêtrement|`@Compendium[pf2e.spells-srd.J6vNvrUT3b1hx2iA]`|
|[Enthrall](spells/enc-03-IihxWhRfpsBgQ5jS.htm)|Discours captivant|`@Compendium[pf2e.spells-srd.IihxWhRfpsBgQ5jS]`|
|[Entrancing Eyes](spells/enc-07-Vw2CNwlRRKABsuZi.htm)||`@Compendium[pf2e.spells-srd.Vw2CNwlRRKABsuZi]`|
|[Ephemeral Hazards](spells/ill-04-HisaZTk67YAxLGBq.htm)||`@Compendium[pf2e.spells-srd.HisaZTk67YAxLGBq]`|
|[Ephemeral Tracking](spells/div-03-QpjHqxwTGdILLvjD.htm)|Pistage éphémère|`@Compendium[pf2e.spells-srd.QpjHqxwTGdILLvjD]`|
|[Eradicate Undeath](spells/nec-04-J5MNC4xq3CHH31qT.htm)|Éradication de la non-mort|`@Compendium[pf2e.spells-srd.J5MNC4xq3CHH31qT]`|
|[Establish Ward](spells/abj-05-XlQBVvlDWGrGlApl.htm)|Établir une protection|`@Compendium[pf2e.spells-srd.XlQBVvlDWGrGlApl]`|
|[Ethereal Jaunt](spells/con-07-D2nPKbIS67m9199U.htm)|Forme éthérée|`@Compendium[pf2e.spells-srd.D2nPKbIS67m9199U]`|
|[Evil Eye](spells/enc-00-f45JpY7Ph2cAJGW2.htm)|Mauvais œil|`@Compendium[pf2e.spells-srd.f45JpY7Ph2cAJGW2]`|
|[Exchange Image](spells/ill-01-znMvKqcRDilIVMwA.htm)||`@Compendium[pf2e.spells-srd.znMvKqcRDilIVMwA]`|
|[Expeditious Excavation](spells/trs-02-aXoh6OQAL57lgh0a.htm)||`@Compendium[pf2e.spells-srd.aXoh6OQAL57lgh0a]`|
|[Extend Spell](spells/div-03-GxxnhRIaoGKtu1iO.htm)|Extension de durée|`@Compendium[pf2e.spells-srd.GxxnhRIaoGKtu1iO]`|
|[Fabricated Truth](spells/enc-10-6s0UW4bujggma9TC.htm)|Vérité fabriquée|`@Compendium[pf2e.spells-srd.6s0UW4bujggma9TC]`|
|[Face in the Crowd](spells/ill-01-yH13KXUK2x093NUv.htm)|Fondu dans la foule|`@Compendium[pf2e.spells-srd.yH13KXUK2x093NUv]`|
|[Faerie Dust](spells/enc-01-IkS3lDGUpIOMug7v.htm)|Poussière féerique|`@Compendium[pf2e.spells-srd.IkS3lDGUpIOMug7v]`|
|[Faerie Fire](spells/evo-02-HRb2doyaLtaoCfi3.htm)|Lueurs féériques|`@Compendium[pf2e.spells-srd.HRb2doyaLtaoCfi3]`|
|[Fallow Field](spells/nec-04-2RhZkHNv8ajq0yLq.htm)||`@Compendium[pf2e.spells-srd.2RhZkHNv8ajq0yLq]`|
|[False Life](spells/nec-02-8ViwItUgwT4lOvvb.htm)|Simulacre de vie|`@Compendium[pf2e.spells-srd.8ViwItUgwT4lOvvb]`|
|[False Vision](spells/ill-05-RCbLd7dfquHnuvrZ.htm)|Vision faussée|`@Compendium[pf2e.spells-srd.RCbLd7dfquHnuvrZ]`|
|[Familiar Form](spells/trs-04-CHZQJg7O7991Vl4m.htm)||`@Compendium[pf2e.spells-srd.CHZQJg7O7991Vl4m]`|
|[Familiar's Face](spells/div-03-9GkOWDFDEMuV3hJr.htm)|Visage du familier|`@Compendium[pf2e.spells-srd.9GkOWDFDEMuV3hJr]`|
|[Fantastic Facade](spells/ill-09-nA0XlPsnMNrQMpio.htm)||`@Compendium[pf2e.spells-srd.nA0XlPsnMNrQMpio]`|
|[Fatal Aria](spells/enc-10-lyJDBD9OFW11vLyT.htm)|Aria fatale|`@Compendium[pf2e.spells-srd.lyJDBD9OFW11vLyT]`|
|[Favorable Review](spells/enc-04-i7u6gAdNcyIyyo3h.htm)|Critique favorable|`@Compendium[pf2e.spells-srd.i7u6gAdNcyIyyo3h]`|
|[Fear](spells/enc-01-4koZzrnMXhhosn0D.htm)|Terreur|`@Compendium[pf2e.spells-srd.4koZzrnMXhhosn0D]`|
|[Feast of Ashes](spells/nec-02-siU9xRlqWXeKT0mH.htm)||`@Compendium[pf2e.spells-srd.siU9xRlqWXeKT0mH]`|
|[Feather Fall](spells/abj-01-TTwOKGqmZeKSyNMH.htm)|Feuille morte|`@Compendium[pf2e.spells-srd.TTwOKGqmZeKSyNMH]`|
|[Feeblemind](spells/enc-06-5BbU1V6wGSGbrmRD.htm)|Débilité|`@Compendium[pf2e.spells-srd.5BbU1V6wGSGbrmRD]`|
|[Feet to Fins](spells/trs-03-RvBlSIJmxiqfCpR9.htm)|Pieds en nageoires|`@Compendium[pf2e.spells-srd.RvBlSIJmxiqfCpR9]`|
|[Fey Disappearance](spells/enc-03-mBojKJatf9PTYC38.htm)|Disparition féerique|`@Compendium[pf2e.spells-srd.mBojKJatf9PTYC38]`|
|[Fey Glamour](spells/ill-05-DdXKfIjDtORUtUvY.htm)|Enchetement féerique|`@Compendium[pf2e.spells-srd.DdXKfIjDtORUtUvY]`|
|[Field of Life](spells/nec-06-x5rGOmhDRDVQPrnW.htm)|Champ de vie|`@Compendium[pf2e.spells-srd.x5rGOmhDRDVQPrnW]`|
|[Fiery Body](spells/trs-07-XS7Wyh5YC0NWeWyB.htm)|Corps enflammé|`@Compendium[pf2e.spells-srd.XS7Wyh5YC0NWeWyB]`|
|[Final Sacrifice](spells/evo-02-x0rWq0wS06dns4G2.htm)||`@Compendium[pf2e.spells-srd.x0rWq0wS06dns4G2]`|
|[Finger of Death](spells/nec-07-Z9OrRXKgAPv6Hn5l.htm)|Doigt de mort|`@Compendium[pf2e.spells-srd.Z9OrRXKgAPv6Hn5l]`|
|[Fire Ray](spells/evo-01-oJKZi8OQgmVXHOc0.htm)|Rayon de feu|`@Compendium[pf2e.spells-srd.oJKZi8OQgmVXHOc0]`|
|[Fire Seeds](spells/evo-06-kuoYff1csM5eAcAP.htm)|Germe de feu|`@Compendium[pf2e.spells-srd.kuoYff1csM5eAcAP]`|
|[Fire Shield](spells/evo-04-YrzBLPLd3r9m6t1p.htm)|Bouclier de feu|`@Compendium[pf2e.spells-srd.YrzBLPLd3r9m6t1p]`|
|[Fireball](spells/evo-03-sxQZ6yqTn0czJxVd.htm)|Boule de feu|`@Compendium[pf2e.spells-srd.sxQZ6yqTn0czJxVd]`|
|[Flame Barrier](spells/abj-04-y7Tusv3CieZktkkV.htm)|Barrière de flammes|`@Compendium[pf2e.spells-srd.y7Tusv3CieZktkkV]`|
|[Flame Strike](spells/evo-05-E3X2RbzWHCdz7gsk.htm)|Colonne de feu|`@Compendium[pf2e.spells-srd.E3X2RbzWHCdz7gsk]`|
|[Flaming Fusillade](spells/evo-06-bynT1UKaDqr8dLNM.htm)||`@Compendium[pf2e.spells-srd.bynT1UKaDqr8dLNM]`|
|[Flaming Sphere](spells/evo-02-2ZdHjnpEQJuqOYSG.htm)|Sphère de feu|`@Compendium[pf2e.spells-srd.2ZdHjnpEQJuqOYSG]`|
|[Fleet Step](spells/trs-01-aEM2cttJ2eYcLssW.htm)|Pas rapide|`@Compendium[pf2e.spells-srd.aEM2cttJ2eYcLssW]`|
|[Flesh to Stone](spells/trs-06-znv4ECL7ZtuiagtA.htm)|Chair en pierre|`@Compendium[pf2e.spells-srd.znv4ECL7ZtuiagtA]`|
|[Floating Disk](spells/con-01-LvezN4a3kYf1OHMg.htm)|Disque flottant|`@Compendium[pf2e.spells-srd.LvezN4a3kYf1OHMg]`|
|[Fly](spells/trs-04-A2JfEKe6BZcTG1S8.htm)|Vol|`@Compendium[pf2e.spells-srd.A2JfEKe6BZcTG1S8]`|
|[Font of Serenity](spells/abj-04-yY1H5zhO5dHmD8lz.htm)|Source de sérénité|`@Compendium[pf2e.spells-srd.yY1H5zhO5dHmD8lz]`|
|[Forbidding Ward](spells/abj-00-RA7VKcen3p56rVyZ.htm)|Sceau d'interdiction|`@Compendium[pf2e.spells-srd.RA7VKcen3p56rVyZ]`|
|[Force Bolt](spells/evo-01-Hu38hoAUSYeFpkVa.htm)|Trait de force|`@Compendium[pf2e.spells-srd.Hu38hoAUSYeFpkVa]`|
|[Force Cage](spells/evo-07-HES5jvGiNZZnJycK.htm)||`@Compendium[pf2e.spells-srd.HES5jvGiNZZnJycK]`|
|[Forced Quiet](spells/abj-01-k43PIYwuQqjeJ3S3.htm)|Silence forcé|`@Compendium[pf2e.spells-srd.k43PIYwuQqjeJ3S3]`|
|[Foresight](spells/div-09-qsNeG9KZpODSACMq.htm)|Prémonition|`@Compendium[pf2e.spells-srd.qsNeG9KZpODSACMq]`|
|[Forgotten Lines](spells/div-04-u0AtDZs6BhBPtjEs.htm)|Lignes oubliées|`@Compendium[pf2e.spells-srd.u0AtDZs6BhBPtjEs]`|
|[Foul Miasma](spells/nec-04-6nTBr5XNuKOuPM5m.htm)||`@Compendium[pf2e.spells-srd.6nTBr5XNuKOuPM5m]`|
|[Freedom](spells/abj-08-4ddJSjC9Zz5DX0oG.htm)|Liberté|`@Compendium[pf2e.spells-srd.4ddJSjC9Zz5DX0oG]`|
|[Freedom of Movement](spells/abj-04-aqRYNoSvxsVfqglH.htm)|Liberté de mouvement|`@Compendium[pf2e.spells-srd.aqRYNoSvxsVfqglH]`|
|[Fungal Hyphae](spells/nec-02-oNUyCqbpGWHifS02.htm)||`@Compendium[pf2e.spells-srd.oNUyCqbpGWHifS02]`|
|[Fungal Infestation](spells/con-02-3VxVbZqIRvpKkg3O.htm)|Infestation fongique|`@Compendium[pf2e.spells-srd.3VxVbZqIRvpKkg3O]`|
|[Gaseous Form](spells/trs-04-V8wXOsoejQhe6CyG.htm)|Forme gazeuse|`@Compendium[pf2e.spells-srd.V8wXOsoejQhe6CyG]`|
|[Gate](spells/con-10-U13bC0tNgrlHoeTK.htm)|Portail|`@Compendium[pf2e.spells-srd.U13bC0tNgrlHoeTK]`|
|[Geas](spells/enc-03-Q690d3mw3TUrKX7E.htm)|Serment rituel|`@Compendium[pf2e.spells-srd.Q690d3mw3TUrKX7E]`|
|[Genie's Veil](spells/abj-01-OyiKIbWllLZC6sGz.htm)|Veille du génie|`@Compendium[pf2e.spells-srd.OyiKIbWllLZC6sGz]`|
|[Gentle Repose](spells/nec-02-xRgU9rrhmGAgG4Rc.htm)|Préservation des morts|`@Compendium[pf2e.spells-srd.xRgU9rrhmGAgG4Rc]`|
|[Ghost Sound](spells/ill-00-atlgGNI1E1Ox3O3a.htm)|Son imaginaire|`@Compendium[pf2e.spells-srd.atlgGNI1E1Ox3O3a]`|
|[Ghostly Tragedy](spells/div-04-WVc30DGbM7TRLLul.htm)|Tragédie fantômatique|`@Compendium[pf2e.spells-srd.WVc30DGbM7TRLLul]`|
|[Ghostly Transcription](spells/ill-04-vhe9DduqaivMs8FV.htm)||`@Compendium[pf2e.spells-srd.vhe9DduqaivMs8FV]`|
|[Ghostly Weapon](spells/trs-03-AMEu5zzLN7uCX645.htm)|Arme fantomatique|`@Compendium[pf2e.spells-srd.AMEu5zzLN7uCX645]`|
|[Ghoulish Cravings](spells/nec-02-JhRuR7Jj3ViShpq7.htm)|Désirs macabres|`@Compendium[pf2e.spells-srd.JhRuR7Jj3ViShpq7]`|
|[Glacial Heart](spells/evo-05-ZyREiMaul0VhDYh3.htm)|Cœur glacial|`@Compendium[pf2e.spells-srd.ZyREiMaul0VhDYh3]`|
|[Glibness](spells/enc-04-JyxTmqjYYn63V5LY.htm)|Bagou|`@Compendium[pf2e.spells-srd.JyxTmqjYYn63V5LY]`|
|[Glimpse the Truth](spells/div-04-ZjbVgIIqMstmdkqP.htm)|Entrevoir la vérité|`@Compendium[pf2e.spells-srd.ZjbVgIIqMstmdkqP]`|
|[Glitterdust](spells/evo-02-0qaqksrGGDj74HXE.htm)|Poussière scintillante|`@Compendium[pf2e.spells-srd.0qaqksrGGDj74HXE]`|
|[Globe of Invulnerability](spells/abj-04-JOdOpbPDl7nqvJUm.htm)|Globe d'invulnérabilité|`@Compendium[pf2e.spells-srd.JOdOpbPDl7nqvJUm]`|
|[Glutton's Jaw](spells/nec-01-SdXFiQ4Py8761sNO.htm)|Mâchoires gloutonnes|`@Compendium[pf2e.spells-srd.SdXFiQ4Py8761sNO]`|
|[Glyph of Warding](spells/abj-03-o0l57UfBm9ScEUMW.htm)|Glyphe de garde|`@Compendium[pf2e.spells-srd.o0l57UfBm9ScEUMW]`|
|[Goblin Pox](spells/nec-01-zJQnkKEKbJqGB3iB.htm)|Variole Gobeline|`@Compendium[pf2e.spells-srd.zJQnkKEKbJqGB3iB]`|
|[Goodberry](spells/nec-01-vQuwLqtFFYt0K15N.htm)|Baie nourricière|`@Compendium[pf2e.spells-srd.vQuwLqtFFYt0K15N]`|
|[Grasping Grave](spells/nec-05-2YIr0S2Gt14PMMQp.htm)|Étreinte de la tombe|`@Compendium[pf2e.spells-srd.2YIr0S2Gt14PMMQp]`|
|[Gravity Weapon](spells/evo-01-IWUe32Y5k2QFd7YQ.htm)|Arme pesante|`@Compendium[pf2e.spells-srd.IWUe32Y5k2QFd7YQ]`|
|[Gravity Well](spells/evo-03-06pzGkKTyPE3tHR8.htm)||`@Compendium[pf2e.spells-srd.06pzGkKTyPE3tHR8]`|
|[Grease](spells/con-01-Wu0xFpewMKRK3HG8.htm)|Graisse|`@Compendium[pf2e.spells-srd.Wu0xFpewMKRK3HG8]`|
|[Grim Tendrils](spells/nec-01-k34hDOfIIMAxNL4a.htm)|Sinistres volutes|`@Compendium[pf2e.spells-srd.k34hDOfIIMAxNL4a]`|
|[Grisly Growths](spells/trs-05-d9sBzPOXX3KT8uTu.htm)||`@Compendium[pf2e.spells-srd.d9sBzPOXX3KT8uTu]`|
|[Guidance](spells/div-00-izcxFQFwf3woCnFs.htm)|Assistance divine|`@Compendium[pf2e.spells-srd.izcxFQFwf3woCnFs]`|
|[Gust of Wind](spells/evo-01-g8QqHpv2CWDwmIm1.htm)|Bourrasque|`@Compendium[pf2e.spells-srd.g8QqHpv2CWDwmIm1]`|
|[Halcyon Infusion](spells/div-06-UJmKPm1FC6pf6txP.htm)|Infusion paisible|`@Compendium[pf2e.spells-srd.UJmKPm1FC6pf6txP]`|
|[Hallucination](spells/ill-05-U58aQWJ47VrI36yP.htm)|Hallucination|`@Compendium[pf2e.spells-srd.U58aQWJ47VrI36yP]`|
|[Hallucinatory Terrain](spells/ill-04-HBJPsonQnWcC3qdX.htm)|Terrain hallucinatoire|`@Compendium[pf2e.spells-srd.HBJPsonQnWcC3qdX]`|
|[Hand of the Apprentice](spells/evo-01-bSDTWUIvgXkBaEv8.htm)|Main de l'apprenti|`@Compendium[pf2e.spells-srd.bSDTWUIvgXkBaEv8]`|
|[Harm](spells/nec-01-wdA52JJnsuQWeyqz.htm)|Mise à mal|`@Compendium[pf2e.spells-srd.wdA52JJnsuQWeyqz]`|
|[Haste](spells/trs-03-o6YCGx4lycsYpww4.htm)|Rapidité|`@Compendium[pf2e.spells-srd.o6YCGx4lycsYpww4]`|
|[Heal](spells/nec-01-rfZpqmj0AIIdkVIs.htm)|Guérison|`@Compendium[pf2e.spells-srd.rfZpqmj0AIIdkVIs]`|
|[Heal Animal](spells/nec-01-Zmh4ynfnCtwKeAYl.htm)|Guérison des animaux|`@Compendium[pf2e.spells-srd.Zmh4ynfnCtwKeAYl]`|
|[Heal Companion](spells/nec-01-KIV2LqzS5KtqOItV.htm)|Guérison de compagnon|`@Compendium[pf2e.spells-srd.KIV2LqzS5KtqOItV]`|
|[Healer's Blessing](spells/nec-01-rhJyqB9g3ziImQgM.htm)|Bénédiction du guérisseur|`@Compendium[pf2e.spells-srd.rhJyqB9g3ziImQgM]`|
|[Heart's Desire](spells/ill-03-PVXqMko4yGgw90uo.htm)|Désir du cœur|`@Compendium[pf2e.spells-srd.PVXqMko4yGgw90uo]`|
|[Heartbond](spells/abj-02-dZV8nZUKRhGIr6g9.htm)|Lien du cœur|`@Compendium[pf2e.spells-srd.dZV8nZUKRhGIr6g9]`|
|[Heat Metal](spells/evo-02-v4KzRPol5XQOOmk0.htm)||`@Compendium[pf2e.spells-srd.v4KzRPol5XQOOmk0]`|
|[Hellfire Plume](spells/evo-05-9LHr9SuDLTicdbXs.htm)|Volute de feu infernal|`@Compendium[pf2e.spells-srd.9LHr9SuDLTicdbXs]`|
|[Hero's Defiance](spells/nec-10-uGXWkR2h8q9MRzEM.htm)|Défi du héros|`@Compendium[pf2e.spells-srd.uGXWkR2h8q9MRzEM]`|
|[Heroes' Feast](spells/con-05-yRf59eFtZ50cGlem.htm)|Festin des héros|`@Compendium[pf2e.spells-srd.yRf59eFtZ50cGlem]`|
|[Heroic Feat](spells/div-06-4j0FQ1mkidBAXuQV.htm)|Don héroïque|`@Compendium[pf2e.spells-srd.4j0FQ1mkidBAXuQV]`|
|[Heroism](spells/enc-03-KqvqNAfGIE5a9wSv.htm)|Héroïsme|`@Compendium[pf2e.spells-srd.KqvqNAfGIE5a9wSv]`|
|[Hideous Laughter](spells/enc-02-tlSE7Ly8vi1Dgddv.htm)|Fou rire|`@Compendium[pf2e.spells-srd.tlSE7Ly8vi1Dgddv]`|
|[Holy Cascade](spells/evo-04-DZ9bzXYqMjAK9TzC.htm)|Cascade bénite|`@Compendium[pf2e.spells-srd.DZ9bzXYqMjAK9TzC]`|
|[Horrid Wilting](spells/nec-08-M0jQlpQYUr0pp2Sv.htm)|Flétrissure|`@Compendium[pf2e.spells-srd.M0jQlpQYUr0pp2Sv]`|
|[Horrific Visage](spells/ill-03-aZg3amDcrXz3cLCz.htm)|Visage terrifiant|`@Compendium[pf2e.spells-srd.aZg3amDcrXz3cLCz]`|
|[House of Imaginary Walls](spells/ill-00-PEfSofHm73IT3Khc.htm)|Demeure aux murs imaginaires|`@Compendium[pf2e.spells-srd.PEfSofHm73IT3Khc]`|
|[Humanoid Form](spells/trs-02-2qGqa33E4GPUCbMV.htm)|Forme humanoïde|`@Compendium[pf2e.spells-srd.2qGqa33E4GPUCbMV]`|
|[Hunter's Luck](spells/div-02-GfrKNJ9pNeATiKCc.htm)|Chance du chasseur|`@Compendium[pf2e.spells-srd.GfrKNJ9pNeATiKCc]`|
|[Hunter's Vision](spells/div-05-tj86Rnq3QuQnDtG3.htm)|Vision du chasseur|`@Compendium[pf2e.spells-srd.tj86Rnq3QuQnDtG3]`|
|[Hurtling Stone](spells/evo-01-pRKaEXnjGJXbPHPC.htm)|Jet de pierre|`@Compendium[pf2e.spells-srd.pRKaEXnjGJXbPHPC]`|
|[Hydraulic Push](spells/evo-01-jfVCuOpzC6mUrf6f.htm)|Poussée hydraulique|`@Compendium[pf2e.spells-srd.jfVCuOpzC6mUrf6f]`|
|[Hydraulic Torrent](spells/evo-04-Y3G6Y6EDgCY0s3fq.htm)|Torrent|`@Compendium[pf2e.spells-srd.Y3G6Y6EDgCY0s3fq]`|
|[Hymn of Healing](spells/nec-01-gSUQlTDYoLDGAsCP.htm)|Hymne de guérison|`@Compendium[pf2e.spells-srd.gSUQlTDYoLDGAsCP]`|
|[Hypercognition](spells/div-03-ZYoC630tNGutgbE0.htm)|Hypercognition|`@Compendium[pf2e.spells-srd.ZYoC630tNGutgbE0]`|
|[Hyperfocus](spells/div-01-XhgMx9WC6NfXd9RP.htm)|Hyperfocalisation|`@Compendium[pf2e.spells-srd.XhgMx9WC6NfXd9RP]`|
|[Hypnotic Pattern](spells/ill-03-K2WpC3FFoHrqX9Q5.htm)|Lueurs hypnotiques|`@Compendium[pf2e.spells-srd.K2WpC3FFoHrqX9Q5]`|
|[Ice Storm](spells/evo-04-kHyjQbibRGPNCixx.htm)||`@Compendium[pf2e.spells-srd.kHyjQbibRGPNCixx]`|
|[Ill Omen](spells/div-01-6ZIKB0151LUR19Rw.htm)|Mauvais présage|`@Compendium[pf2e.spells-srd.6ZIKB0151LUR19Rw]`|
|[Illusory Creature](spells/ill-02-f8SBoXiXQjlCKqly.htm)|Créature illusoire|`@Compendium[pf2e.spells-srd.f8SBoXiXQjlCKqly]`|
|[Illusory Disguise](spells/ill-01-i35dpZFI7jZcRoBo.htm)|Déguisement illusoire|`@Compendium[pf2e.spells-srd.i35dpZFI7jZcRoBo]`|
|[Illusory Object](spells/ill-01-2oH5IufzdESuYxat.htm)|Objet illusoire|`@Compendium[pf2e.spells-srd.2oH5IufzdESuYxat]`|
|[Illusory Scene](spells/ill-05-Ucf8eynbZMfUucjE.htm)|Scène illusoire|`@Compendium[pf2e.spells-srd.Ucf8eynbZMfUucjE]`|
|[Imp Sting](spells/nec-02-oryfsRK27jAUnziw.htm)||`@Compendium[pf2e.spells-srd.oryfsRK27jAUnziw]`|
|[Impaling Briars](spells/con-08-oGV6YdpZLIG4G4gH.htm)|Ronces empaleuses|`@Compendium[pf2e.spells-srd.oGV6YdpZLIG4G4gH]`|
|[Impaling Spike](spells/con-05-oXeEbcUdgJGWHGEJ.htm)|Pique empaleuse|`@Compendium[pf2e.spells-srd.oXeEbcUdgJGWHGEJ]`|
|[Implosion](spells/evo-09-4WS7HrFjwNvTn8T2.htm)|Implosion|`@Compendium[pf2e.spells-srd.4WS7HrFjwNvTn8T2]`|
|[Imprint Message](spells/div-01-WJOQryAODgYmrL6g.htm)|Imprimer un message|`@Compendium[pf2e.spells-srd.WJOQryAODgYmrL6g]`|
|[Imprisonment](spells/evo-08-2CNqkt2s2IYkVnv6.htm)|Emprisonnement|`@Compendium[pf2e.spells-srd.2CNqkt2s2IYkVnv6]`|
|[Incendiary Aura](spells/evo-01-AspA30tzKCHFWRf0.htm)||`@Compendium[pf2e.spells-srd.AspA30tzKCHFWRf0]`|
|[Indestructibility](spells/abj-10-UG0SmRYSdbrx2rTA.htm)|Invulnérabilité|`@Compendium[pf2e.spells-srd.UG0SmRYSdbrx2rTA]`|
|[Infernal Pact](spells/con-01-30BBep9U4BDV0EgQ.htm)|Pacte infernal|`@Compendium[pf2e.spells-srd.30BBep9U4BDV0EgQ]`|
|[Insect Form](spells/trs-03-XI6Lzd2B5pernkPd.htm)|Forme d'insecte|`@Compendium[pf2e.spells-srd.XI6Lzd2B5pernkPd]`|
|[Inspire Competence](spells/enc-00-f0Z5mqGA6Yu79B8x.htm)|Inspiration talentueuse|`@Compendium[pf2e.spells-srd.f0Z5mqGA6Yu79B8x]`|
|[Inspire Courage](spells/enc-00-IAjvwqgiDr3qGYxY.htm)|Inspiration vaillante|`@Compendium[pf2e.spells-srd.IAjvwqgiDr3qGYxY]`|
|[Inspire Defense](spells/enc-00-bH0kPuf7UKxRvi2P.htm)|Inspiration défensive|`@Compendium[pf2e.spells-srd.bH0kPuf7UKxRvi2P]`|
|[Inspire Heroics](spells/enc-04-kF0rs9mCPvJGfAZE.htm)|Inspiration héroïque|`@Compendium[pf2e.spells-srd.kF0rs9mCPvJGfAZE]`|
|[Interstellar Void](spells/evo-03-L37RTc7K79OUpZ7X.htm)|Vide intersidéral|`@Compendium[pf2e.spells-srd.L37RTc7K79OUpZ7X]`|
|[Inveigle](spells/enc-02-5pwK2FZX6QwgtfqX.htm)|Envoûtement|`@Compendium[pf2e.spells-srd.5pwK2FZX6QwgtfqX]`|
|[Invisibility](spells/ill-02-XXqE1eY3w3z6xJCB.htm)|Invisibilité|`@Compendium[pf2e.spells-srd.XXqE1eY3w3z6xJCB]`|
|[Invisibility Cloak](spells/ill-04-Nun72GTmb31YqSKh.htm)|Cape d'invisibilité|`@Compendium[pf2e.spells-srd.Nun72GTmb31YqSKh]`|
|[Invisibility Sphere](spells/ill-03-I0j56TNRmGcTyoqJ.htm)|Sphère d'invisibilité|`@Compendium[pf2e.spells-srd.I0j56TNRmGcTyoqJ]`|
|[Invisible Item](spells/ill-01-nX85Brzax9f650aK.htm)||`@Compendium[pf2e.spells-srd.nX85Brzax9f650aK]`|
|[Invoke the Crimson Oath](spells/evo-02-AsKLseOo8hwv5Jha.htm)|Invoquer le Serment écarlate|`@Compendium[pf2e.spells-srd.AsKLseOo8hwv5Jha]`|
|[Iron Gut](spells/trs-02-IhwREVWG0OzzrbWA.htm)||`@Compendium[pf2e.spells-srd.IhwREVWG0OzzrbWA]`|
|[Item Facade](spells/ill-01-4ZGte0i9YbLh4dRi.htm)|Façade|`@Compendium[pf2e.spells-srd.4ZGte0i9YbLh4dRi]`|
|[Jealous Hex](spells/nec-01-kvm68hVtmADiIvN4.htm)|Maléfice de jalousie|`@Compendium[pf2e.spells-srd.kvm68hVtmADiIvN4]`|
|[Join Pasts](spells/div-00-8bdt1TvNKzsCu9Ct.htm)|Joindre les passés|`@Compendium[pf2e.spells-srd.8bdt1TvNKzsCu9Ct]`|
|[Jump](spells/trs-01-Q7QQ91vQtyi1Ux36.htm)|Saut|`@Compendium[pf2e.spells-srd.Q7QQ91vQtyi1Ux36]`|
|[Ki Blast](spells/evo-03-oo7YcRC2gcez81PV.htm)|Explosion Ki|`@Compendium[pf2e.spells-srd.oo7YcRC2gcez81PV]`|
|[Ki Form](spells/trs-09-YDMOqndvYFu3OjA6.htm)|Forme ki|`@Compendium[pf2e.spells-srd.YDMOqndvYFu3OjA6]`|
|[Ki Rush](spells/trs-01-ps0nmhclT6aIXgd8.htm)|Ruée Ki|`@Compendium[pf2e.spells-srd.ps0nmhclT6aIXgd8]`|
|[Ki Strike](spells/trs-01-ZL8NTvB22NeEWhVG.htm)|Frappe Ki|`@Compendium[pf2e.spells-srd.ZL8NTvB22NeEWhVG]`|
|[Knock](spells/trs-02-6Ot4N22t5tPD51BO.htm)|Déblocage|`@Compendium[pf2e.spells-srd.6Ot4N22t5tPD51BO]`|
|[Know Direction](spells/div-00-tXa5vOu5giBNCjdR.htm)|Repérage|`@Compendium[pf2e.spells-srd.tXa5vOu5giBNCjdR]`|
|[Know the Enemy](spells/div-04-l6zjNysNedpJcmDT.htm)|Connaître son ennemi|`@Compendium[pf2e.spells-srd.l6zjNysNedpJcmDT]`|
|[Lament](spells/enc-01-T90ij2uu6ZaBaSXV.htm)|Lamentation|`@Compendium[pf2e.spells-srd.T90ij2uu6ZaBaSXV]`|
|[Lay on Hands](spells/nec-01-zNN9212H2FGfM7VS.htm)|Imposition des mains|`@Compendium[pf2e.spells-srd.zNN9212H2FGfM7VS]`|
|[Legend Lore](spells/div-07-hp6Q64dl7xbdn4gQ.htm)|Mythes et légendes|`@Compendium[pf2e.spells-srd.hp6Q64dl7xbdn4gQ]`|
|[Leng Sting](spells/nec-07-QVMjPfXlpnmeuWKS.htm)||`@Compendium[pf2e.spells-srd.QVMjPfXlpnmeuWKS]`|
|[Levitate](spells/evo-03-fI20AVwOzJMHXRdo.htm)|Lévitation|`@Compendium[pf2e.spells-srd.fI20AVwOzJMHXRdo]`|
|[Liberating Command](spells/enc-01-0Dcd4iEXqCrkm4Jn.htm)|Ordre libérateur|`@Compendium[pf2e.spells-srd.0Dcd4iEXqCrkm4Jn]`|
|[Life Boost](spells/nec-01-fAlzXtQAASaJx0mY.htm)|Gain de vie|`@Compendium[pf2e.spells-srd.fAlzXtQAASaJx0mY]`|
|[Life Link](spells/nec-01-2gQYrCPwBmwau26O.htm)||`@Compendium[pf2e.spells-srd.2gQYrCPwBmwau26O]`|
|[Life Siphon](spells/nec-04-zvvHOQV78WKUB33l.htm)|Siphon de vie|`@Compendium[pf2e.spells-srd.zvvHOQV78WKUB33l]`|
|[Life-Giving Form](spells/nec-06-9kOI14Jep97TzGO7.htm)|Forme génératrice de vie|`@Compendium[pf2e.spells-srd.9kOI14Jep97TzGO7]`|
|[Light](spells/evo-00-WBmvzNDfpwka3qT4.htm)|Lumière|`@Compendium[pf2e.spells-srd.WBmvzNDfpwka3qT4]`|
|[Light of Revelation](spells/div-02-ou56ShiFH7GWF8hX.htm)|Lumière de révélation|`@Compendium[pf2e.spells-srd.ou56ShiFH7GWF8hX]`|
|[Lightning Bolt](spells/evo-03-9AAkVUCwF6WVNNY2.htm)|Éclair|`@Compendium[pf2e.spells-srd.9AAkVUCwF6WVNNY2]`|
|[Lightning Storm](spells/evo-05-JyT346VmGtRLsDnV.htm)||`@Compendium[pf2e.spells-srd.JyT346VmGtRLsDnV]`|
|[Lingering Composition](spells/enc-01-irTdhxTixU9u9YUm.htm)|Composition prolongée|`@Compendium[pf2e.spells-srd.irTdhxTixU9u9YUm]`|
|[Linnorm Sting](spells/nec-09-ZMY58Yk5hnyfeE3q.htm)||`@Compendium[pf2e.spells-srd.ZMY58Yk5hnyfeE3q]`|
|[Litany against Sloth](spells/evo-05-2mVW1KT3AjW2pvDO.htm)|Litanie contre la paresse|`@Compendium[pf2e.spells-srd.2mVW1KT3AjW2pvDO]`|
|[Litany against Wrath](spells/evo-03-X1b9ollVMSLXDN9o.htm)|Litanie contre la colère|`@Compendium[pf2e.spells-srd.X1b9ollVMSLXDN9o]`|
|[Litany of Depravity](spells/evo-07-tYP8unoR0a5Dq9EA.htm)|Litanie de dépravation|`@Compendium[pf2e.spells-srd.tYP8unoR0a5Dq9EA]`|
|[Litany of Righteousness](spells/evo-07-n3b3pDmA6L5YRTyq.htm)|Litanie de vertu|`@Compendium[pf2e.spells-srd.n3b3pDmA6L5YRTyq]`|
|[Litany of Self-Interest](spells/enc-05-mm3hZ6jgaJaKK16n.htm)|Litanie d'égoïsme|`@Compendium[pf2e.spells-srd.mm3hZ6jgaJaKK16n]`|
|[Localized Quake](spells/trs-04-cBUuG1yJHGeKffpg.htm)|Séisme localisé|`@Compendium[pf2e.spells-srd.cBUuG1yJHGeKffpg]`|
|[Locate](spells/div-03-LQzlKbYjZSMFQawP.htm)|Localisation|`@Compendium[pf2e.spells-srd.LQzlKbYjZSMFQawP]`|
|[Lock](spells/abj-01-Azoh0BSoCASrA1lr.htm)|Verrouillage|`@Compendium[pf2e.spells-srd.Azoh0BSoCASrA1lr]`|
|[Longstrider](spells/trs-01-KcLVELhCUcKXxiKE.htm)|Grande foulée|`@Compendium[pf2e.spells-srd.KcLVELhCUcKXxiKE]`|
|[Loremaster's Etude](spells/div-01-5Pc55FGGqVpIAJ62.htm)|Étude du maître savant|`@Compendium[pf2e.spells-srd.5Pc55FGGqVpIAJ62]`|
|[Lucky Break](spells/div-04-7OwZHalOdRCRnFmZ.htm)|Répit chanceux|`@Compendium[pf2e.spells-srd.7OwZHalOdRCRnFmZ]`|
|[Mad Monkeys](spells/con-03-tFKJCPvOQZxKq6ON.htm)|Singes fous|`@Compendium[pf2e.spells-srd.tFKJCPvOQZxKq6ON]`|
|[Mage Armor](spells/abj-01-aAbfKn8maGjJjk2W.htm)|Armure du mage|`@Compendium[pf2e.spells-srd.aAbfKn8maGjJjk2W]`|
|[Mage Hand](spells/evo-00-pwzdSlJgYqN7bs2w.htm)|Manipulation à distance|`@Compendium[pf2e.spells-srd.pwzdSlJgYqN7bs2w]`|
|[Magic Aura](spells/ill-01-PRrZ7anETWPm90YY.htm)|Aura magique|`@Compendium[pf2e.spells-srd.PRrZ7anETWPm90YY]`|
|[Magic Fang](spells/trs-01-EE7Q5BHIrfWNCPtT.htm)|Morsure magique|`@Compendium[pf2e.spells-srd.EE7Q5BHIrfWNCPtT]`|
|[Magic Hide](spells/abj-01-u2uSeH6YSbK1ajTy.htm)|Peau magique|`@Compendium[pf2e.spells-srd.u2uSeH6YSbK1ajTy]`|
|[Magic Missile](spells/evo-01-gKKqvLohtrSJj3BM.htm)|Projectile magique|`@Compendium[pf2e.spells-srd.gKKqvLohtrSJj3BM]`|
|[Magic Mouth](spells/ill-02-eIQ86FOXK34HiNLs.htm)|Bouche magique|`@Compendium[pf2e.spells-srd.eIQ86FOXK34HiNLs]`|
|[Magic Stone](spells/nec-01-9u6X9ykhzG11NK1n.htm)||`@Compendium[pf2e.spells-srd.9u6X9ykhzG11NK1n]`|
|[Magic Warrior Aspect](spells/trs-02-b6UnLNikoq2Std1f.htm)|Aspect du Guerrier Magique|`@Compendium[pf2e.spells-srd.b6UnLNikoq2Std1f]`|
|[Magic Warrior Transformation](spells/trs-02-tp4K7mYDL5MRHvJc.htm)|Transformation du Guerrier magique|`@Compendium[pf2e.spells-srd.tp4K7mYDL5MRHvJc]`|
|[Magic Weapon](spells/trs-01-TFitdEOpQC4SzKQQ.htm)|Arme magique|`@Compendium[pf2e.spells-srd.TFitdEOpQC4SzKQQ]`|
|[Magic's Vessel](spells/enc-01-u4FGIUQgruLjml7J.htm)|Réceptacle magique|`@Compendium[pf2e.spells-srd.u4FGIUQgruLjml7J]`|
|[Magical Fetters](spells/con-03-2ZPqcM9wNoVnpwkK.htm)|Entraves magiques|`@Compendium[pf2e.spells-srd.2ZPqcM9wNoVnpwkK]`|
|[Magnificent Mansion](spells/con-07-vPWMEyVTreMOoFnm.htm)|Manoir somptueux|`@Compendium[pf2e.spells-srd.vPWMEyVTreMOoFnm]`|
|[Malicious Shadow](spells/evo-03-vhMCd15ZwNJn0zen.htm)||`@Compendium[pf2e.spells-srd.vhMCd15ZwNJn0zen]`|
|[Malignant Sustenance](spells/nec-04-qzsQmpiQodHBBWYI.htm)|Alimentation maléfique|`@Compendium[pf2e.spells-srd.qzsQmpiQodHBBWYI]`|
|[Mantis Form](spells/trs-04-bKDsmKVosexwJ80i.htm)|Forme de la mante|`@Compendium[pf2e.spells-srd.bKDsmKVosexwJ80i]`|
|[Mariner's Curse](spells/nec-05-z2mfh3oPnfYqXflY.htm)|Malédicition du marin|`@Compendium[pf2e.spells-srd.z2mfh3oPnfYqXflY]`|
|[Mask of Terror](spells/ill-07-O6VQC1Bs4aSYDa6R.htm)|Masque terrifiant|`@Compendium[pf2e.spells-srd.O6VQC1Bs4aSYDa6R]`|
|[Massacre](spells/nec-09-10VcmSYNBrvBphu1.htm)|Massacre|`@Compendium[pf2e.spells-srd.10VcmSYNBrvBphu1]`|
|[Maze](spells/con-08-Oj1PJBMQD9vuwCv7.htm)|Dédale|`@Compendium[pf2e.spells-srd.Oj1PJBMQD9vuwCv7]`|
|[Maze of Locked Doors](spells/abj-07-qOeBQyC1z7OScHvP.htm)|Dédale de portes fermées|`@Compendium[pf2e.spells-srd.qOeBQyC1z7OScHvP]`|
|[Medusa's Wrath](spells/trs-08-wTYxxYJWN348oV15.htm)|Courroux de la méduse|`@Compendium[pf2e.spells-srd.wTYxxYJWN348oV15]`|
|[Meld into Stone](spells/trs-03-vh1RpbWfqdNC4L3P.htm)|Fusion dans la pierre|`@Compendium[pf2e.spells-srd.vh1RpbWfqdNC4L3P]`|
|[Mending](spells/trs-01-dINQzhqGmIsqGMUY.htm)|Réparation|`@Compendium[pf2e.spells-srd.dINQzhqGmIsqGMUY]`|
|[Message](spells/ill-00-vLzFcIaSXs7YTIqJ.htm)|Message|`@Compendium[pf2e.spells-srd.vLzFcIaSXs7YTIqJ]`|
|[Message Rune](spells/abj-01-Z7N5IxJCwrAdIgSg.htm)|Rune messagère|`@Compendium[pf2e.spells-srd.Z7N5IxJCwrAdIgSg]`|
|[Meteor Swarm](spells/evo-09-jrBa9deU2ULFWvSl.htm)|Nuée de météores|`@Compendium[pf2e.spells-srd.jrBa9deU2ULFWvSl]`|
|[Mind Blank](spells/abj-08-C2w3YfBKjIRS07DP.htm)|Esprit impénétrable|`@Compendium[pf2e.spells-srd.C2w3YfBKjIRS07DP]`|
|[Mind Probe](spells/div-05-9BnhadUO8FMLmeZ3.htm)|Sonde mentale|`@Compendium[pf2e.spells-srd.9BnhadUO8FMLmeZ3]`|
|[Mind Reading](spells/div-03-KHnhPHL4x1AQHfbC.htm)|Lecture des pensées|`@Compendium[pf2e.spells-srd.KHnhPHL4x1AQHfbC]`|
|[Mindlink](spells/div-01-D442XMADp01qJ7Cs.htm)|Lien mental|`@Compendium[pf2e.spells-srd.D442XMADp01qJ7Cs]`|
|[Miracle](spells/div-10-YfJTXyVGzLhM6V8U.htm)|Miracle|`@Compendium[pf2e.spells-srd.YfJTXyVGzLhM6V8U]`|
|[Mirror Image](spells/ill-02-j8vIoIEWElvpwkcI.htm)|Image miroir|`@Compendium[pf2e.spells-srd.j8vIoIEWElvpwkcI]`|
|[Misdirection](spells/ill-02-c2bTWBNO1BYX4Zfg.htm)|Détection faussée|`@Compendium[pf2e.spells-srd.c2bTWBNO1BYX4Zfg]`|
|[Mislead](spells/ill-06-WPXzPl7YbMEIGWfi.htm)|Double illusoire|`@Compendium[pf2e.spells-srd.WPXzPl7YbMEIGWfi]`|
|[Modify Memory](spells/div-04-FhOaQDTSnsY7tiam.htm)|Modification de mémoire|`@Compendium[pf2e.spells-srd.FhOaQDTSnsY7tiam]`|
|[Moment of Renewal](spells/nec-08-4MOew29Z1oCX8O28.htm)|Instant de renouveau|`@Compendium[pf2e.spells-srd.4MOew29Z1oCX8O28]`|
|[Monstrosity Form](spells/trs-08-8AMvNVOUEtxBCDvJ.htm)|Forme monstrueuse|`@Compendium[pf2e.spells-srd.8AMvNVOUEtxBCDvJ]`|
|[Moon Frenzy](spells/trs-05-YtesyvfAIwXOqISq.htm)|Frénésie lunaire|`@Compendium[pf2e.spells-srd.YtesyvfAIwXOqISq]`|
|[Moonbeam](spells/evo-01-SuBtUJiU6DbSJYIw.htm)|Rayon de lune|`@Compendium[pf2e.spells-srd.SuBtUJiU6DbSJYIw]`|
|[Moonlight Bridge](spells/con-06-In2A7GCyxxaqZdPI.htm)|Pont de Lumière lunaire|`@Compendium[pf2e.spells-srd.In2A7GCyxxaqZdPI]`|
|[Mystic Beacon](spells/evo-04-2SYq0ZTsOtJEigFx.htm)|Phare mystique|`@Compendium[pf2e.spells-srd.2SYq0ZTsOtJEigFx]`|
|[Nature Incarnate](spells/trs-10-ZXwxs5tRjEGrjAJT.htm)|Incarnation de la nature|`@Compendium[pf2e.spells-srd.ZXwxs5tRjEGrjAJT]`|
|[Nature's Bounty](spells/con-04-vSSKyUdrHu86E5Gk.htm)|Générosité naturelle|`@Compendium[pf2e.spells-srd.vSSKyUdrHu86E5Gk]`|
|[Nature's Enmity](spells/enc-09-Tc5NLaMu71vrGTJQ.htm)|Hostilité naturelle|`@Compendium[pf2e.spells-srd.Tc5NLaMu71vrGTJQ]`|
|[Necrotic Radiation](spells/nec-04-29p7NMY2OTpaINzt.htm)||`@Compendium[pf2e.spells-srd.29p7NMY2OTpaINzt]`|
|[Needle of Vengeance](spells/enc-01-aEitTTb9PnOyidRf.htm)|Aiguille de la vengeance|`@Compendium[pf2e.spells-srd.aEitTTb9PnOyidRf]`|
|[Negate Aroma](spells/abj-01-0fKHBh5goe2eiFYL.htm)|Supprimer l’arôme|`@Compendium[pf2e.spells-srd.0fKHBh5goe2eiFYL]`|
|[Neutralize Poison](spells/nec-03-SUKaxVZW2TlM8lu0.htm)|Neutralisation du poison|`@Compendium[pf2e.spells-srd.SUKaxVZW2TlM8lu0]`|
|[Nightmare](spells/ill-04-Uqj344bezBq3ESdq.htm)|Cauchemar|`@Compendium[pf2e.spells-srd.Uqj344bezBq3ESdq]`|
|[Nondetection](spells/abj-03-EoKBlgf6Smt8opaU.htm)|Antidétection|`@Compendium[pf2e.spells-srd.EoKBlgf6Smt8opaU]`|
|[Noxious Vapors](spells/evo-01-1meVElIu1CEVYWkv.htm)||`@Compendium[pf2e.spells-srd.1meVElIu1CEVYWkv]`|
|[Nudge Fate](spells/div-00-G0T1xv1FoZ23Jxvt.htm)|Coup de pouce au destin|`@Compendium[pf2e.spells-srd.G0T1xv1FoZ23Jxvt]`|
|[Nymph's Token](spells/enc-01-pHrVvoTKygXeczVG.htm)|Amulette de la nymphe|`@Compendium[pf2e.spells-srd.pHrVvoTKygXeczVG]`|
|[Oathkeeper's Insignia](spells/con-01-cokeXkDHUAo4zHsw.htm)|Insigne du gardien du serment|`@Compendium[pf2e.spells-srd.cokeXkDHUAo4zHsw]`|
|[Object Memory](spells/div-01-RztmhJrLLQWoGVdB.htm)|Mémoire de l'objet|`@Compendium[pf2e.spells-srd.RztmhJrLLQWoGVdB]`|
|[Object Reading](spells/div-01-5LYi9Efs6cko4GGL.htm)|Lecture d'objet|`@Compendium[pf2e.spells-srd.5LYi9Efs6cko4GGL]`|
|[Obscuring Mist](spells/con-02-9XHmC2JgTUIQ1CCm.htm)|Brume de dissimulation|`@Compendium[pf2e.spells-srd.9XHmC2JgTUIQ1CCm]`|
|[Ode to Ouroboros](spells/nec-05-DgCS456mXKw97vNy.htm)|Ode à Ouroboros|`@Compendium[pf2e.spells-srd.DgCS456mXKw97vNy]`|
|[Outcast's Curse](spells/enc-04-KSAEhNfZyXMO7Z7V.htm)|Malédiction du paria|`@Compendium[pf2e.spells-srd.KSAEhNfZyXMO7Z7V]`|
|[Overflowing Sorrow](spells/enc-04-eCniO6INHNfc9Svr.htm)|Chagrins débordantss|`@Compendium[pf2e.spells-srd.eCniO6INHNfc9Svr]`|
|[Overstuff](spells/trs-01-mFHQ2u4LWiejqKQG.htm)|Gaver|`@Compendium[pf2e.spells-srd.mFHQ2u4LWiejqKQG]`|
|[Overwhelming Presence](spells/enc-09-fkDeKktdmbeplYRY.htm)|Présence écrasante|`@Compendium[pf2e.spells-srd.fkDeKktdmbeplYRY]`|
|[Paralyze](spells/enc-03-DCQHaLrYXMI37dvW.htm)|Paralysie|`@Compendium[pf2e.spells-srd.DCQHaLrYXMI37dvW]`|
|[Paranoia](spells/ill-02-Mkbq9xlAUxHUHyR2.htm)|Paranoïa|`@Compendium[pf2e.spells-srd.Mkbq9xlAUxHUHyR2]`|
|[Parch](spells/evo-01-7CUgqHunmHfW2lC5.htm)||`@Compendium[pf2e.spells-srd.7CUgqHunmHfW2lC5]`|
|[Pass Without Trace](spells/abj-01-zlnXpME1T2uvn8Lr.htm)|Passage sans trace|`@Compendium[pf2e.spells-srd.zlnXpME1T2uvn8Lr]`|
|[Passwall](spells/con-05-115Xp9E38CJENhNS.htm)|Passe-muraille|`@Compendium[pf2e.spells-srd.115Xp9E38CJENhNS]`|
|[Penumbral Shroud](spells/evo-01-zdb8cjOIDVKYMWdr.htm)||`@Compendium[pf2e.spells-srd.zdb8cjOIDVKYMWdr]`|
|[Perfect Strike](spells/div-02-GQopUYTuhmtb7WMG.htm)|Coup parfait|`@Compendium[pf2e.spells-srd.GQopUYTuhmtb7WMG]`|
|[Perfected Form](spells/abj-04-8ifpNZkaxrbs3dBJ.htm)|Forme parfaite|`@Compendium[pf2e.spells-srd.8ifpNZkaxrbs3dBJ]`|
|[Perfected Mind](spells/abj-01-cDFAQN7Z3es07WSA.htm)|Esprit perfectionné|`@Compendium[pf2e.spells-srd.cDFAQN7Z3es07WSA]`|
|[Perseis's Precautions](spells/div-03-ovx7O2FHvkjXhMcA.htm)|Précautions de Perséis|`@Compendium[pf2e.spells-srd.ovx7O2FHvkjXhMcA]`|
|[Personal Blizzard](spells/con-03-g4MAIQodRDVfNp1B.htm)|Blizzard personnel|`@Compendium[pf2e.spells-srd.g4MAIQodRDVfNp1B]`|
|[Personal Rain Cloud](spells/evo-01-kJKSLfCgqxmN2FY8.htm)||`@Compendium[pf2e.spells-srd.kJKSLfCgqxmN2FY8]`|
|[Pest Form](spells/trs-01-gfPjmG6Fe6D3MFjl.htm)|Forme de nuisible|`@Compendium[pf2e.spells-srd.gfPjmG6Fe6D3MFjl]`|
|[Pet Cache](spells/con-01-F1nlmqOIucch3Cmt.htm)|Cachette de l'animal de compagnie|`@Compendium[pf2e.spells-srd.F1nlmqOIucch3Cmt]`|
|[Phantasmal Calamity](spells/ill-06-0XP2XOxT9VSiXFDr.htm)|Calamité imaginaire|`@Compendium[pf2e.spells-srd.0XP2XOxT9VSiXFDr]`|
|[Phantasmal Killer](spells/ill-04-tlcrVRqW1MSKJ5IC.htm)|Assassin imaginaire|`@Compendium[pf2e.spells-srd.tlcrVRqW1MSKJ5IC]`|
|[Phantasmal Treasure](spells/ill-02-L0GoJpHxSD0wRY5k.htm)||`@Compendium[pf2e.spells-srd.L0GoJpHxSD0wRY5k]`|
|[Phantom Pain](spells/ill-01-R8bqnYiThB6MYTxD.htm)|Douleur fantôme|`@Compendium[pf2e.spells-srd.R8bqnYiThB6MYTxD]`|
|[Phantom Steed](spells/con-02-WPKJOhEihhcIm2uQ.htm)|Coursier fantôme|`@Compendium[pf2e.spells-srd.WPKJOhEihhcIm2uQ]`|
|[Phase Familiar](spells/abj-01-rMOI8JFJ0nT2mrCF.htm)|Déphasage de familier|`@Compendium[pf2e.spells-srd.rMOI8JFJ0nT2mrCF]`|
|[Physical Boost](spells/trs-01-1lmzILdCFENln8Cy.htm)|Amélioration physique|`@Compendium[pf2e.spells-srd.1lmzILdCFENln8Cy]`|
|[Pied Piping](spells/enc-10-Um0aaJotqMKGmAlR.htm)|Flûte de Hamelin|`@Compendium[pf2e.spells-srd.Um0aaJotqMKGmAlR]`|
|[Pillar of Water](spells/evo-03-eIPIZp2FUbFcLNdj.htm)||`@Compendium[pf2e.spells-srd.eIPIZp2FUbFcLNdj]`|
|[Pillars of Sand](spells/con-05-qr0HOiiuqj5LKlDt.htm)|Piliers de sable|`@Compendium[pf2e.spells-srd.qr0HOiiuqj5LKlDt]`|
|[Planar Ally](spells/con-05-vgy00hnqxN9VoeoF.htm)|Allié planaire|`@Compendium[pf2e.spells-srd.vgy00hnqxN9VoeoF]`|
|[Planar Binding](spells/abj-06-2ykmAVKrsAWcazcC.htm)|Contrat planaire|`@Compendium[pf2e.spells-srd.2ykmAVKrsAWcazcC]`|
|[Plane Shift](spells/con-07-5bTt2CvYHPvaR7QQ.htm)|Changement de plan|`@Compendium[pf2e.spells-srd.5bTt2CvYHPvaR7QQ]`|
|[Plant Form](spells/trs-05-zCcfPS4y5SrZzU2x.htm)|Forme de plante|`@Compendium[pf2e.spells-srd.zCcfPS4y5SrZzU2x]`|
|[Plant Growth](spells/nec-04-ZhJ8d9Uk4lwIx86b.htm)|Croissance végétale|`@Compendium[pf2e.spells-srd.ZhJ8d9Uk4lwIx86b]`|
|[Polar Ray](spells/evo-08-BKIet436snMNcnez.htm)|Rayon polaire|`@Compendium[pf2e.spells-srd.BKIet436snMNcnez]`|
|[Positive Luminance](spells/nec-04-8XuNn0h0rHE24m3B.htm)|Luminosité positive|`@Compendium[pf2e.spells-srd.8XuNn0h0rHE24m3B]`|
|[Possession](spells/nec-07-wU6hNzK8Yfqdmc8m.htm)|Possession|`@Compendium[pf2e.spells-srd.wU6hNzK8Yfqdmc8m]`|
|[Power Word Blind](spells/enc-07-Yk3t4ekEiFIoEz9c.htm)|Mot de pouvoir aveuglant|`@Compendium[pf2e.spells-srd.Yk3t4ekEiFIoEz9c]`|
|[Power Word Kill](spells/enc-09-m3lcOFm400lQCUps.htm)|Mot de pouvoir mortel|`@Compendium[pf2e.spells-srd.m3lcOFm400lQCUps]`|
|[Power Word Stun](spells/enc-08-7PJSqUeKxTqOVrPk.htm)|Mot de pouvoir étourdissant|`@Compendium[pf2e.spells-srd.7PJSqUeKxTqOVrPk]`|
|[Practice Makes Perfect](spells/div-01-HOj2YsTpkoMpYJH9.htm)|En forgeant on devient forgeron|`@Compendium[pf2e.spells-srd.HOj2YsTpkoMpYJH9]`|
|[Precious Metals](spells/trs-04-APTMURAW1N0Wpk4w.htm)|Métaux précieux|`@Compendium[pf2e.spells-srd.APTMURAW1N0Wpk4w]`|
|[Prestidigitation](spells/evo-00-Qw3fnUlaUbnn7ipC.htm)|Prestidigitation|`@Compendium[pf2e.spells-srd.Qw3fnUlaUbnn7ipC]`|
|[Primal Call](spells/abj-06-pZc8ZwtsyWnxUUW0.htm)|Appel primordial|`@Compendium[pf2e.spells-srd.pZc8ZwtsyWnxUUW0]`|
|[Primal Herd](spells/trs-10-pmP8HhXvvEKP3LqU.htm)|Troupeau primitif|`@Compendium[pf2e.spells-srd.pmP8HhXvvEKP3LqU]`|
|[Primal Phenomenon](spells/div-10-MLNeD5sAunV0E23j.htm)|Phénomène primordial|`@Compendium[pf2e.spells-srd.MLNeD5sAunV0E23j]`|
|[Primal Summons](spells/con-06-kwlKUxEuT8T15YW6.htm)|Convocations primordiales|`@Compendium[pf2e.spells-srd.kwlKUxEuT8T15YW6]`|
|[Prismatic Sphere](spells/abj-09-PngDCmU0MXZkbu0v.htm)|Sphère prismatique|`@Compendium[pf2e.spells-srd.PngDCmU0MXZkbu0v]`|
|[Prismatic Spray](spells/evo-07-d6o52BnjViNz7Gub.htm)|Rayon prismatique|`@Compendium[pf2e.spells-srd.d6o52BnjViNz7Gub]`|
|[Prismatic Wall](spells/abj-08-iL6TujgTCtRRa0Y0.htm)|Mur prismatique|`@Compendium[pf2e.spells-srd.iL6TujgTCtRRa0Y0]`|
|[Private Sanctum](spells/abj-04-onjZCEHs3JJJRTD0.htm)|Sanctuaire secret|`@Compendium[pf2e.spells-srd.onjZCEHs3JJJRTD0]`|
|[Produce Flame](spells/evo-00-O9w7r4BKgPogYDDe.htm)|Flammes|`@Compendium[pf2e.spells-srd.O9w7r4BKgPogYDDe]`|
|[Project Image](spells/ill-07-0873MWM0qKDDv81O.htm)|Projection d'image|`@Compendium[pf2e.spells-srd.0873MWM0qKDDv81O]`|
|[Protection](spells/abj-01-gMODOGamz88rgHuf.htm)|Protection|`@Compendium[pf2e.spells-srd.gMODOGamz88rgHuf]`|
|[Protective Ward](spells/abj-01-lY9fOk1qBDDhBT8s.htm)|Champ protecteur|`@Compendium[pf2e.spells-srd.lY9fOk1qBDDhBT8s]`|
|[Protector's Sacrifice](spells/abj-01-rQYob0QMJ0I1U2sU.htm)|Sacrifice du protecteur|`@Compendium[pf2e.spells-srd.rQYob0QMJ0I1U2sU]`|
|[Protector's Sphere](spells/abj-04-DH9Y3RQGWO0GzXGU.htm)|Sphère du protecteur|`@Compendium[pf2e.spells-srd.DH9Y3RQGWO0GzXGU]`|
|[Prying Eye](spells/div-05-Ek5XI0aEdZhBgm21.htm)|Œil indiscret|`@Compendium[pf2e.spells-srd.Ek5XI0aEdZhBgm21]`|
|[Pulse of the City](spells/div-04-ivKnEtI1z4UqEKIA.htm)|Pouls de la ville|`@Compendium[pf2e.spells-srd.ivKnEtI1z4UqEKIA]`|
|[Pummeling Rubble](spells/evo-01-Rn2LkoSq1XhLsODV.htm)|Frappe de débris|`@Compendium[pf2e.spells-srd.Rn2LkoSq1XhLsODV]`|
|[Punishing Winds](spells/evo-08-wi405lBjPcbF1DeR.htm)|Vents punitifs|`@Compendium[pf2e.spells-srd.wi405lBjPcbF1DeR]`|
|[Purify Food and Drink](spells/nec-01-qXTB7Ec9yYh5JPPV.htm)|Purifier la nourriture et la boisson|`@Compendium[pf2e.spells-srd.qXTB7Ec9yYh5JPPV]`|
|[Purple Worm Sting](spells/nec-06-ayRXv0wQH00TTNZe.htm)|Dard du ver pourpre|`@Compendium[pf2e.spells-srd.ayRXv0wQH00TTNZe]`|
|[Pushing Gust](spells/con-01-myC2EIrsjmB8xosi.htm)|Bourrasque agressive|`@Compendium[pf2e.spells-srd.myC2EIrsjmB8xosi]`|
|[Putrefy Food and Drink](spells/nec-01-CxpFy4HJHf4ACbxF.htm)||`@Compendium[pf2e.spells-srd.CxpFy4HJHf4ACbxF]`|
|[Pyrotechnics](spells/trs-03-TUbXnR4RAuYzRx1u.htm)||`@Compendium[pf2e.spells-srd.TUbXnR4RAuYzRx1u]`|
|[Quench](spells/abj-02-02J0rDTk37KN2sjt.htm)|Éteindre|`@Compendium[pf2e.spells-srd.02J0rDTk37KN2sjt]`|
|[Quivering Palm](spells/nec-08-Ovvflf5aFbmBxqq8.htm)|Paume vibratoire|`@Compendium[pf2e.spells-srd.Ovvflf5aFbmBxqq8]`|
|[Raise Dead](spells/nec-06-IkGYwHRLhkuoGReG.htm)|Rappel à la vie|`@Compendium[pf2e.spells-srd.IkGYwHRLhkuoGReG]`|
|[Ranger's Bramble](spells/trs-03-KktHf7zIAWOr499h.htm)|Roncier du rôdeur|`@Compendium[pf2e.spells-srd.KktHf7zIAWOr499h]`|
|[Ravening Maw](spells/nec-05-VDWIZuLOJqwBthHc.htm)||`@Compendium[pf2e.spells-srd.VDWIZuLOJqwBthHc]`|
|[Ravenous Reanimation](spells/nec-07-u3pdkfmy0AWICdoM.htm)||`@Compendium[pf2e.spells-srd.u3pdkfmy0AWICdoM]`|
|[Ray of Enfeeblement](spells/nec-01-J7Y7tl0bbdz7TcCc.htm)|Rayon affaiblissant|`@Compendium[pf2e.spells-srd.J7Y7tl0bbdz7TcCc]`|
|[Ray of Frost](spells/evo-00-gYjPm7YwGtEa1oxh.htm)|Rayon de givre|`@Compendium[pf2e.spells-srd.gYjPm7YwGtEa1oxh]`|
|[Read Aura](spells/div-00-OhD2Z6rIGGD5ocZA.htm)|Lecture de l'aura|`@Compendium[pf2e.spells-srd.OhD2Z6rIGGD5ocZA]`|
|[Read Fate](spells/div-01-Vvxgn7saUPW2bJhb.htm)|Lire le destin|`@Compendium[pf2e.spells-srd.Vvxgn7saUPW2bJhb]`|
|[Read Omens](spells/div-04-pkcOby5prOausy1k.htm)|Lire les présages|`@Compendium[pf2e.spells-srd.pkcOby5prOausy1k]`|
|[Reaper's Lantern](spells/nec-02-dxOF7d5kAWusLKWF.htm)||`@Compendium[pf2e.spells-srd.dxOF7d5kAWusLKWF]`|
|[Rebounding Barrier](spells/abj-04-5p8naLYjFcG13OkU.htm)|Barrière rebondissante|`@Compendium[pf2e.spells-srd.5p8naLYjFcG13OkU]`|
|[Rebuke Death](spells/nec-04-4nHYSMHito1GUXlm.htm)|Réprimander la mort|`@Compendium[pf2e.spells-srd.4nHYSMHito1GUXlm]`|
|[Redact](spells/trs-01-ifXNOhtmU4fKL68v.htm)||`@Compendium[pf2e.spells-srd.ifXNOhtmU4fKL68v]`|
|[Reflective Scales](spells/abj-04-Mv5L4201uk8hnAtD.htm)|Écailles réfléchisantes|`@Compendium[pf2e.spells-srd.Mv5L4201uk8hnAtD]`|
|[Regenerate](spells/nec-07-2Vkd1IxylPceUAAF.htm)|Régénération|`@Compendium[pf2e.spells-srd.2Vkd1IxylPceUAAF]`|
|[Reincarnate](spells/nec-03-gIVaSCrLhhBzGHQY.htm)||`@Compendium[pf2e.spells-srd.gIVaSCrLhhBzGHQY]`|
|[Remake](spells/con-10-dMKP4fkWx8V2cqAy.htm)|Recréer|`@Compendium[pf2e.spells-srd.dMKP4fkWx8V2cqAy]`|
|[Remember the Lost](spells/div-04-nrW6lGV4xDMqLS3P.htm)|Souvenez vous des pertes|`@Compendium[pf2e.spells-srd.nrW6lGV4xDMqLS3P]`|
|[Remove Curse](spells/nec-04-OyFCwQuw8XRazsNr.htm)|Délivrance des malédictions|`@Compendium[pf2e.spells-srd.OyFCwQuw8XRazsNr]`|
|[Remove Disease](spells/nec-03-RneiyehRO6f7LP44.htm)|Délivrance des maladies|`@Compendium[pf2e.spells-srd.RneiyehRO6f7LP44]`|
|[Remove Fear](spells/enc-02-EfFMLVbmkBWmzoLF.htm)|Délivrance de la peur|`@Compendium[pf2e.spells-srd.EfFMLVbmkBWmzoLF]`|
|[Remove Paralysis](spells/nec-02-BCuHKrDeJ4eq53M6.htm)|Délivrance de la paralysie|`@Compendium[pf2e.spells-srd.BCuHKrDeJ4eq53M6]`|
|[Repulsion](spells/abj-06-yrZA4k2VAqEP8xx7.htm)|Répulsion|`@Compendium[pf2e.spells-srd.yrZA4k2VAqEP8xx7]`|
|[Resilient Sphere](spells/abj-04-qJQADktwD0x8kLAy.htm)|Sphère d'isolement|`@Compendium[pf2e.spells-srd.qJQADktwD0x8kLAy]`|
|[Resist Energy](spells/abj-02-Fr58LDSrbndgld9n.htm)|Résistance à l'énergie|`@Compendium[pf2e.spells-srd.Fr58LDSrbndgld9n]`|
|[Resplendent Mansion](spells/con-09-KPDHmmjJiw7PhTYF.htm)|Manoir resplendissant|`@Compendium[pf2e.spells-srd.KPDHmmjJiw7PhTYF]`|
|[Rest Eternal](spells/nec-04-MTNlvZ0A9xY5sOg1.htm)||`@Compendium[pf2e.spells-srd.MTNlvZ0A9xY5sOg1]`|
|[Restoration](spells/nec-02-SnaLVgxZ9ryUFmUr.htm)|Restauration|`@Compendium[pf2e.spells-srd.SnaLVgxZ9ryUFmUr]`|
|[Restorative Moment](spells/nec-05-pCvJ4yoZJxDtgUMI.htm)|Moment restauratif|`@Compendium[pf2e.spells-srd.pCvJ4yoZJxDtgUMI]`|
|[Restore Senses](spells/nec-02-Et8RSCLx8w7uOLvo.htm)|Restaurer les sens|`@Compendium[pf2e.spells-srd.Et8RSCLx8w7uOLvo]`|
|[Resurrect](spells/nec-05-kqhPt9344UkcGVYO.htm)|Résurrection|`@Compendium[pf2e.spells-srd.kqhPt9344UkcGVYO]`|
|[Retributive Pain](spells/abj-04-SSsUC7rZo0CwayPn.htm)|Souffrance vengeresse|`@Compendium[pf2e.spells-srd.SSsUC7rZo0CwayPn]`|
|[Retrocognition](spells/div-07-rsZ5c0AUyywe5yoK.htm)|Rétrocognition|`@Compendium[pf2e.spells-srd.rsZ5c0AUyywe5yoK]`|
|[Reverse Gravity](spells/evo-07-37ESlJzUvVbOudOT.htm)|Inversion de la gravité|`@Compendium[pf2e.spells-srd.37ESlJzUvVbOudOT]`|
|[Revival](spells/nec-10-HpIJTVqgXorH9X0L.htm)|Vivifier|`@Compendium[pf2e.spells-srd.HpIJTVqgXorH9X0L]`|
|[Righteous Might](spells/trs-06-MMCQsh12TPaDdPbV.htm)|Force du colosse|`@Compendium[pf2e.spells-srd.MMCQsh12TPaDdPbV]`|
|[Rime Slick](spells/evo-02-Popa5umI3H33levx.htm)||`@Compendium[pf2e.spells-srd.Popa5umI3H33levx]`|
|[Roar of the Wyrm](spells/enc-04-FSu6ZKxr3xdS75wq.htm)|Rugissement du Ver|`@Compendium[pf2e.spells-srd.FSu6ZKxr3xdS75wq]`|
|[Rope Trick](spells/con-04-GzdgM0m7wXKuFSho.htm)|Corde enchantée|`@Compendium[pf2e.spells-srd.GzdgM0m7wXKuFSho]`|
|[Rusting Grasp](spells/trs-04-0fYE64odlKqISzft.htm)||`@Compendium[pf2e.spells-srd.0fYE64odlKqISzft]`|
|[Safe Passage](spells/abj-03-IFuEzfmmWyNwVbhY.htm)|Passage sûr|`@Compendium[pf2e.spells-srd.IFuEzfmmWyNwVbhY]`|
|[Safeguard Secret](spells/abj-04-OsrtOeG0TvDNnEFH.htm)|Secret bien gardé|`@Compendium[pf2e.spells-srd.OsrtOeG0TvDNnEFH]`|
|[Sanctified Ground](spells/abj-03-y3jlOfgsQH1JjkJE.htm)|Zone sanctifiée|`@Compendium[pf2e.spells-srd.y3jlOfgsQH1JjkJE]`|
|[Sanctuary](spells/abj-01-8xRzLhwGL7Dgy3EZ.htm)|Sanctuaire|`@Compendium[pf2e.spells-srd.8xRzLhwGL7Dgy3EZ]`|
|[Savor the Sting](spells/enc-01-xn0V2HDrmDWNzPEt.htm)|Savourer la douleur|`@Compendium[pf2e.spells-srd.xn0V2HDrmDWNzPEt]`|
|[Scholarly Recollection](spells/div-01-dXIRotMLsABDQQSB.htm)|Souvenirs érudits|`@Compendium[pf2e.spells-srd.dXIRotMLsABDQQSB]`|
|[Scintillating Pattern](spells/ill-08-uEyfLoFQsRKBRIcB.htm)|Motif scintillant|`@Compendium[pf2e.spells-srd.uEyfLoFQsRKBRIcB]`|
|[Scintillating Safeguard](spells/abj-06-9W2Qv0wXLg6tQg3y.htm)|Précaution scintillante|`@Compendium[pf2e.spells-srd.9W2Qv0wXLg6tQg3y]`|
|[Scouring Sand](spells/evo-01-HGmBY8KjgLV97nUp.htm)||`@Compendium[pf2e.spells-srd.HGmBY8KjgLV97nUp]`|
|[Scrying](spells/div-06-r784cIz17eWujtQj.htm)|Scrutation|`@Compendium[pf2e.spells-srd.r784cIz17eWujtQj]`|
|[Sculpt Sound](spells/ill-03-HHGUBGle4OjoxvNR.htm)||`@Compendium[pf2e.spells-srd.HHGUBGle4OjoxvNR]`|
|[Sea Surge](spells/evo-02-cf7Jkm39uEjUFtHt.htm)||`@Compendium[pf2e.spells-srd.cf7Jkm39uEjUFtHt]`|
|[Seal Fate](spells/nec-04-gfKhtVsXF3HKSdmY.htm)||`@Compendium[pf2e.spells-srd.gfKhtVsXF3HKSdmY]`|
|[Searing Light](spells/evo-03-DyiD239dNS7RIxZE.htm)|Lumière brûlante|`@Compendium[pf2e.spells-srd.DyiD239dNS7RIxZE]`|
|[Secret Chest](spells/con-05-BoA00y45uDlmou07.htm)|Coffre secret|`@Compendium[pf2e.spells-srd.BoA00y45uDlmou07]`|
|[Secret Page](spells/ill-03-VVAZPCvd4d90qVA1.htm)|Page secrète|`@Compendium[pf2e.spells-srd.VVAZPCvd4d90qVA1]`|
|[See Invisibility](spells/div-02-jwK43yKsHTkJQvQ9.htm)|Détection de l'invisibilité|`@Compendium[pf2e.spells-srd.jwK43yKsHTkJQvQ9]`|
|[Sending](spells/div-05-R9xqCBblkS5KE4y7.htm)|Communication à distance|`@Compendium[pf2e.spells-srd.R9xqCBblkS5KE4y7]`|
|[Sepulchral Mask](spells/div-01-3mINzPzup2m9qzFU.htm)|Masque sépucral|`@Compendium[pf2e.spells-srd.3mINzPzup2m9qzFU]`|
|[Shadow Blast](spells/evo-05-IqJ9URobmJ9L9UBG.htm)|Explosion d'ombre|`@Compendium[pf2e.spells-srd.IqJ9URobmJ9L9UBG]`|
|[Shadow Illusion](spells/ill-06-EOWh6VVcSjB3WPjX.htm)|Illusion de l'ombre|`@Compendium[pf2e.spells-srd.EOWh6VVcSjB3WPjX]`|
|[Shadow Jump](spells/con-05-5ttAVJbWg2GVKmrN.htm)|Saut de l'ombre|`@Compendium[pf2e.spells-srd.5ttAVJbWg2GVKmrN]`|
|[Shadow Siphon](spells/ill-05-tcwT97RWKxsJiefG.htm)|Siphon d'ombre|`@Compendium[pf2e.spells-srd.tcwT97RWKxsJiefG]`|
|[Shadow Walk](spells/con-05-rxvS7EMJ7qmexAyA.htm)|Traversée des ombres|`@Compendium[pf2e.spells-srd.rxvS7EMJ7qmexAyA]`|
|[Shadow's Web](spells/evo-07-n8eEXXAtguoErW0y.htm)|Toile d'ombre|`@Compendium[pf2e.spells-srd.n8eEXXAtguoErW0y]`|
|[Shall not Falter, Shall not Rout](spells/nec-05-rnFAHvKpcsU4BJD4.htm)|Tu ne faibliras pas, tu ne prendras pas la fuite|`@Compendium[pf2e.spells-srd.rnFAHvKpcsU4BJD4]`|
|[Shape Stone](spells/trs-04-McnPlLFvKtQVXNcG.htm)|Façonnage de la pierre|`@Compendium[pf2e.spells-srd.McnPlLFvKtQVXNcG]`|
|[Shape Wood](spells/trs-02-CXICME10TkEJxz0P.htm)|Façonnage du bois|`@Compendium[pf2e.spells-srd.CXICME10TkEJxz0P]`|
|[Shapechange](spells/trs-09-pswdik31kuHEvdno.htm)|Changement de forme|`@Compendium[pf2e.spells-srd.pswdik31kuHEvdno]`|
|[Share Burden](spells/abj-01-cJq5NarY0eOZN74A.htm)|Partager le Fardeau|`@Compendium[pf2e.spells-srd.cJq5NarY0eOZN74A]`|
|[Share Lore](spells/div-01-nXmC2Xx9WmS5NsAo.htm)|Connaissance partagée|`@Compendium[pf2e.spells-srd.nXmC2Xx9WmS5NsAo]`|
|[Shared Nightmare](spells/enc-04-skvgOWNTitLehL0b.htm)|Cauchemar partagé|`@Compendium[pf2e.spells-srd.skvgOWNTitLehL0b]`|
|[Shatter](spells/evo-02-1xbFBQDRs0hT5xZ9.htm)|Fracassement|`@Compendium[pf2e.spells-srd.1xbFBQDRs0hT5xZ9]`|
|[Shattering Gem](spells/abj-01-0uRpypf1Hi7ahvTl.htm)|Gemme explosive|`@Compendium[pf2e.spells-srd.0uRpypf1Hi7ahvTl]`|
|[Shepherd of Souls](spells/nec-05-SzKkzq3Rr6vKIxbp.htm)|Berger des âmes|`@Compendium[pf2e.spells-srd.SzKkzq3Rr6vKIxbp]`|
|[Shield](spells/abj-00-TVKNbcgTee19PXZR.htm)|Bouclier|`@Compendium[pf2e.spells-srd.TVKNbcgTee19PXZR]`|
|[Shield Other](spells/nec-02-d7Lwx6KAs47MtF0q.htm)|Protection d'autrui|`@Compendium[pf2e.spells-srd.d7Lwx6KAs47MtF0q]`|
|[Shifting Form](spells/trs-04-SDkIFrrO1PsE02Kd.htm)|Forme changeante|`@Compendium[pf2e.spells-srd.SDkIFrrO1PsE02Kd]`|
|[Shifting Sand](spells/trs-03-HcIAQZjNXHemoXSU.htm)||`@Compendium[pf2e.spells-srd.HcIAQZjNXHemoXSU]`|
|[Shillelagh](spells/trs-01-s3abwDbTV43pGFFW.htm)|Gourdin magique|`@Compendium[pf2e.spells-srd.s3abwDbTV43pGFFW]`|
|[Shocking Grasp](spells/evo-01-r3NeUnsgt9mS03Sn.htm)|Décharge électrique|`@Compendium[pf2e.spells-srd.r3NeUnsgt9mS03Sn]`|
|[Shockwave](spells/evo-01-dgCH2E0gMLMUgyFl.htm)||`@Compendium[pf2e.spells-srd.dgCH2E0gMLMUgyFl]`|
|[Show the Way](spells/div-03-ThE5zPYKF4weiljj.htm)|Montrer le chemin|`@Compendium[pf2e.spells-srd.ThE5zPYKF4weiljj]`|
|[Shrink](spells/trs-02-PjhUmyKnq6K5uDby.htm)|Rétrécir|`@Compendium[pf2e.spells-srd.PjhUmyKnq6K5uDby]`|
|[Shrink Item](spells/trs-03-kWh8sJH7yawidMyW.htm)|Rétrécir un objet|`@Compendium[pf2e.spells-srd.kWh8sJH7yawidMyW]`|
|[Shroud of Night](spells/evo-00-cE7PRAX8Up7fmYef.htm)|Voile de nuit|`@Compendium[pf2e.spells-srd.cE7PRAX8Up7fmYef]`|
|[Sigil](spells/trs-00-AUctDF2fqPZN2w4W.htm)|Symbole|`@Compendium[pf2e.spells-srd.AUctDF2fqPZN2w4W]`|
|[Silence](spells/ill-02-gIdDLrbswTV3OBJy.htm)|Silence|`@Compendium[pf2e.spells-srd.gIdDLrbswTV3OBJy]`|
|[Silver's Refrain](spells/abj-00-xM35hJacTM1BSXUl.htm)|Refrain d'argent|`@Compendium[pf2e.spells-srd.xM35hJacTM1BSXUl]`|
|[Simulacrum](spells/ill-04-h5UqEdeqK8iTcU0J.htm)||`@Compendium[pf2e.spells-srd.h5UqEdeqK8iTcU0J]`|
|[Sleep](spells/enc-01-o4lRVTwSxnOOn5vl.htm)|Sommeil|`@Compendium[pf2e.spells-srd.o4lRVTwSxnOOn5vl]`|
|[Slough Skin](spells/nec-02-X3dYByf3YmkcdwG0.htm)||`@Compendium[pf2e.spells-srd.X3dYByf3YmkcdwG0]`|
|[Slow](spells/trs-03-WsUwpfmhKrKwoIe3.htm)|Lenteur|`@Compendium[pf2e.spells-srd.WsUwpfmhKrKwoIe3]`|
|[Snare Hopping](spells/con-01-dDt8VFuLuhznT19v.htm)|Piège artisanal bondissant|`@Compendium[pf2e.spells-srd.dDt8VFuLuhznT19v]`|
|[Snowball](spells/evo-01-W6QlRwQLPoBSw6PZ.htm)|Boule de neige|`@Compendium[pf2e.spells-srd.W6QlRwQLPoBSw6PZ]`|
|[Solid Fog](spells/con-04-piMJO6aYeDJbrhEo.htm)|Brouillard dense|`@Compendium[pf2e.spells-srd.piMJO6aYeDJbrhEo]`|
|[Song of Marching](spells/enc-00-LTUaK3smfm5eDiFK.htm)|Chanson de marche|`@Compendium[pf2e.spells-srd.LTUaK3smfm5eDiFK]`|
|[Song of Strength](spells/enc-00-Xxdwkt0EEDgP1LGc.htm)|Chanson de force|`@Compendium[pf2e.spells-srd.Xxdwkt0EEDgP1LGc]`|
|[Song of the Fallen](spells/nec-10-IGXGs9PlqUCvODcH.htm)|Chanson des morts au combat|`@Compendium[pf2e.spells-srd.IGXGs9PlqUCvODcH]`|
|[Soothe](spells/enc-01-szIyEsvihc5e1w8n.htm)|Apaiser|`@Compendium[pf2e.spells-srd.szIyEsvihc5e1w8n]`|
|[Soothing Ballad](spells/enc-07-0JigNJDRwevZOyjI.htm)|Ballade apaisante|`@Compendium[pf2e.spells-srd.0JigNJDRwevZOyjI]`|
|[Soothing Mist](spells/nec-02-RGBZrVRIEDb2G48h.htm)|Brume apaisante|`@Compendium[pf2e.spells-srd.RGBZrVRIEDb2G48h]`|
|[Soothing Words](spells/enc-01-YGRpHU5yxw73mls8.htm)|Paroles apaisantes|`@Compendium[pf2e.spells-srd.YGRpHU5yxw73mls8]`|
|[Soul Siphon](spells/nec-01-D6T17BdazhNy3KPm.htm)||`@Compendium[pf2e.spells-srd.D6T17BdazhNy3KPm]`|
|[Sound Burst](spells/evo-02-wzLkNU3AAqOSKFPR.htm)|Cacophonie|`@Compendium[pf2e.spells-srd.wzLkNU3AAqOSKFPR]`|
|[Speak with Animals](spells/div-02-BBvV7qoXGdw09q1C.htm)|Communication avec les animaux|`@Compendium[pf2e.spells-srd.BBvV7qoXGdw09q1C]`|
|[Speak with Plants](spells/div-04-qvwIwJ9QBihy8R0t.htm)|Communication avec les plantes|`@Compendium[pf2e.spells-srd.qvwIwJ9QBihy8R0t]`|
|[Speaking Sky](spells/div-06-Zvg0FWzClGbzucFd.htm)|Ciel parlant|`@Compendium[pf2e.spells-srd.Zvg0FWzClGbzucFd]`|
|[Spectral Hand](spells/nec-02-rthC6dGm3nNrt1xN.htm)|Main spectrale|`@Compendium[pf2e.spells-srd.rthC6dGm3nNrt1xN]`|
|[Spell Immunity](spells/abj-04-tgIhRUFtgDSELpJE.htm)|Immunité contre les sorts|`@Compendium[pf2e.spells-srd.tgIhRUFtgDSELpJE]`|
|[Spell Turning](spells/abj-07-OOELvfkTedBMlWtq.htm)|Renvoi de sorts|`@Compendium[pf2e.spells-srd.OOELvfkTedBMlWtq]`|
|[Spellmaster's Ward](spells/abj-05-VrhCpCIQB8PFax77.htm)|Protection du Maître des sorts|`@Compendium[pf2e.spells-srd.VrhCpCIQB8PFax77]`|
|[Spellwrack](spells/abj-06-8Umt1AzYfFbC4fui.htm)|Ravages magiques|`@Compendium[pf2e.spells-srd.8Umt1AzYfFbC4fui]`|
|[Spider Climb](spells/trs-02-5KobTMrZeZxuXMgl.htm)|Pattes d'araignée|`@Compendium[pf2e.spells-srd.5KobTMrZeZxuXMgl]`|
|[Spider Sting](spells/nec-01-DYdvMZ8G2LiSLVWw.htm)|Morsure d'araignée|`@Compendium[pf2e.spells-srd.DYdvMZ8G2LiSLVWw]`|
|[Spike Stones](spells/con-04-3xD8DYrr8YDVYGg7.htm)|Piques de pierre|`@Compendium[pf2e.spells-srd.3xD8DYrr8YDVYGg7]`|
|[Spirit Blast](spells/nec-06-PHVHBbdHeQRfjLmE.htm)|Coup spirituel|`@Compendium[pf2e.spells-srd.PHVHBbdHeQRfjLmE]`|
|[Spirit Link](spells/nec-01-yHujiDQPdtXW797e.htm)|Lien spirituel|`@Compendium[pf2e.spells-srd.yHujiDQPdtXW797e]`|
|[Spirit Object](spells/nec-00-yp4w9SG4RuqRM8KD.htm)|Animation d'objet|`@Compendium[pf2e.spells-srd.yp4w9SG4RuqRM8KD]`|
|[Spirit Sense](spells/div-02-3ehSrqTAm7IPqbIZ.htm)|Perception des esprits|`@Compendium[pf2e.spells-srd.3ehSrqTAm7IPqbIZ]`|
|[Spirit Song](spells/nec-08-Ht35SDf9PDStJfoC.htm)|Chant spirituel|`@Compendium[pf2e.spells-srd.Ht35SDf9PDStJfoC]`|
|[Spirit Veil](spells/ill-03-pQ3NIzZXeIIcU81C.htm)|Voile spirituel|`@Compendium[pf2e.spells-srd.pQ3NIzZXeIIcU81C]`|
|[Spiritual Anamnesis](spells/nec-04-Jli9WBjQZ2MmKJ8y.htm)||`@Compendium[pf2e.spells-srd.Jli9WBjQZ2MmKJ8y]`|
|[Spiritual Epidemic](spells/nec-08-MS60WhVifb45qORJ.htm)|Épidémie spirituelle|`@Compendium[pf2e.spells-srd.MS60WhVifb45qORJ]`|
|[Spiritual Guardian](spells/abj-05-jQdm301h6e8hIY4U.htm)|Gardien spirituel|`@Compendium[pf2e.spells-srd.jQdm301h6e8hIY4U]`|
|[Spiritual Weapon](spells/evo-02-Fq9yCbqI2RDt6Orw.htm)|Arme spirituelle|`@Compendium[pf2e.spells-srd.Fq9yCbqI2RDt6Orw]`|
|[Splash of Art](spells/ill-01-xTpp8dHZsNMDm75B.htm)|Avalanche d'art|`@Compendium[pf2e.spells-srd.xTpp8dHZsNMDm75B]`|
|[Spray of Stars](spells/evo-01-mlNYROcFrUF8nFgk.htm)|Aspersion d'étoiles|`@Compendium[pf2e.spells-srd.mlNYROcFrUF8nFgk]`|
|[Stabilize](spells/nec-00-SnjhtQYexDtNDdEg.htm)|Stabilisation|`@Compendium[pf2e.spells-srd.SnjhtQYexDtNDdEg]`|
|[Stasis](spells/trs-04-cOwSsSXRsBaXUvlr.htm)||`@Compendium[pf2e.spells-srd.cOwSsSXRsBaXUvlr]`|
|[Statuette](spells/trs-06-29AyhknPKiDBcy8s.htm)||`@Compendium[pf2e.spells-srd.29AyhknPKiDBcy8s]`|
|[Status](spells/div-02-HTou8cG05yuSkesj.htm)|Rapport|`@Compendium[pf2e.spells-srd.HTou8cG05yuSkesj]`|
|[Steal Shadow](spells/nec-03-0chL1b4OFIZxpN3v.htm)|Vol d'ombre|`@Compendium[pf2e.spells-srd.0chL1b4OFIZxpN3v]`|
|[Steal the Sky](spells/evo-05-zoY0fQYTF1NzezTg.htm)|Voler le ciel|`@Compendium[pf2e.spells-srd.zoY0fQYTF1NzezTg]`|
|[Stinking Cloud](spells/con-03-GKpEcy9WId6NJvtx.htm)|Nuage nauséabond|`@Compendium[pf2e.spells-srd.GKpEcy9WId6NJvtx]`|
|[Stoke the Heart](spells/enc-00-GeUbPvwdZ4B4l0up.htm)|Enflammer les cœurs|`@Compendium[pf2e.spells-srd.GeUbPvwdZ4B4l0up]`|
|[Stone Tell](spells/evo-06-YIMampGpij4Y30yE.htm)|Pierre commère|`@Compendium[pf2e.spells-srd.YIMampGpij4Y30yE]`|
|[Stone to Flesh](spells/trs-06-Pr1ruNTbzGn3H9w5.htm)|Pierre en chair|`@Compendium[pf2e.spells-srd.Pr1ruNTbzGn3H9w5]`|
|[Stoneskin](spells/abj-04-2BV2yYPfVJ5zirZt.htm)|Peau de pierre|`@Compendium[pf2e.spells-srd.2BV2yYPfVJ5zirZt]`|
|[Storm Lord](spells/evo-09-XYhU3Wi94n1RKxTa.htm)|Seigneur des tempêtes|`@Compendium[pf2e.spells-srd.XYhU3Wi94n1RKxTa]`|
|[Storm of Vengeance](spells/evo-09-r4HLQcYwB62bTayl.htm)|Tempête vengeresse|`@Compendium[pf2e.spells-srd.r4HLQcYwB62bTayl]`|
|[Stormwind Flight](spells/trs-04-gSFg9zKwgcNZLMEs.htm)|Vol de l'ouragan|`@Compendium[pf2e.spells-srd.gSFg9zKwgcNZLMEs]`|
|[Strange Geometry](spells/ill-05-m4Mc5XbdML92BKOE.htm)||`@Compendium[pf2e.spells-srd.m4Mc5XbdML92BKOE]`|
|[Subconscious Suggestion](spells/enc-05-GP3wewkQXEPrLxYj.htm)|Suggestion subconsciente|`@Compendium[pf2e.spells-srd.GP3wewkQXEPrLxYj]`|
|[Sudden Blight](spells/nec-02-XFtO4BBI22Uox2QP.htm)||`@Compendium[pf2e.spells-srd.XFtO4BBI22Uox2QP]`|
|[Sudden Bolt](spells/evo-02-zK0e9d9DSnxC4eAD.htm)||`@Compendium[pf2e.spells-srd.zK0e9d9DSnxC4eAD]`|
|[Sudden Shift](spells/abj-01-P9bqJsF3WkxGAJKJ.htm)|Déplacement soudain|`@Compendium[pf2e.spells-srd.P9bqJsF3WkxGAJKJ]`|
|[Suggestion](spells/enc-04-qwlh6aDgi86U3Q7H.htm)|Suggestion|`@Compendium[pf2e.spells-srd.qwlh6aDgi86U3Q7H]`|
|[Summon Animal](spells/con-01-4YnON9JHYqtLzccu.htm)|Convocation d'animal|`@Compendium[pf2e.spells-srd.4YnON9JHYqtLzccu]`|
|[Summon Celestial](spells/con-05-lTDixrrNKaCvLKwX.htm)|Convocation de céleste|`@Compendium[pf2e.spells-srd.lTDixrrNKaCvLKwX]`|
|[Summon Construct](spells/con-01-lKcsmeOrgHtK4xQa.htm)|Convocation de créature artificielle|`@Compendium[pf2e.spells-srd.lKcsmeOrgHtK4xQa]`|
|[Summon Dragon](spells/con-05-kghwmH3tQjMIhdH1.htm)|Convocation de dragon|`@Compendium[pf2e.spells-srd.kghwmH3tQjMIhdH1]`|
|[Summon Elemental](spells/con-02-lpT6LotUaQPfinjj.htm)|Convocation d'élémentaire|`@Compendium[pf2e.spells-srd.lpT6LotUaQPfinjj]`|
|[Summon Entity](spells/con-05-i1TvBID5QLyXrUCa.htm)|Convocation d'entité|`@Compendium[pf2e.spells-srd.i1TvBID5QLyXrUCa]`|
|[Summon Fey](spells/con-01-hs7h8f4Z1ZNdUt3s.htm)|Convocation de fée|`@Compendium[pf2e.spells-srd.hs7h8f4Z1ZNdUt3s]`|
|[Summon Fiend](spells/con-05-29ytKctjg7qSW2ff.htm)|Convocation de fiélon|`@Compendium[pf2e.spells-srd.29ytKctjg7qSW2ff]`|
|[Summon Giant](spells/con-05-e9UJoVYUd5kJWUpi.htm)|Convocation de géant|`@Compendium[pf2e.spells-srd.e9UJoVYUd5kJWUpi]`|
|[Summon Instrument](spells/con-00-yvs1zN5Pai5U4CJX.htm)|Convocation d'instrument|`@Compendium[pf2e.spells-srd.yvs1zN5Pai5U4CJX]`|
|[Summon Plant or Fungus](spells/con-01-jSRAyd57kd4WZ4yE.htm)|Convocation de plante ou de champignon|`@Compendium[pf2e.spells-srd.jSRAyd57kd4WZ4yE]`|
|[Sun Blade](spells/evo-02-pMTltbI3S3UIuFaR.htm)|Lame solaire|`@Compendium[pf2e.spells-srd.pMTltbI3S3UIuFaR]`|
|[Sun's Fury](spells/evo-03-LrFUj76CHDBV0vHW.htm)|Furie du soleil|`@Compendium[pf2e.spells-srd.LrFUj76CHDBV0vHW]`|
|[Sunburst](spells/evo-07-a3aQxCpoj1q1NQxC.htm)|Explosion de lumière|`@Compendium[pf2e.spells-srd.a3aQxCpoj1q1NQxC]`|
|[Swamp of Sloth](spells/con-03-iQD8OhhkwhvD8Blw.htm)|Marais de paresse|`@Compendium[pf2e.spells-srd.iQD8OhhkwhvD8Blw]`|
|[Swarm Form](spells/trs-04-Pd2M1XY8EXrSfWgJ.htm)||`@Compendium[pf2e.spells-srd.Pd2M1XY8EXrSfWgJ]`|
|[Swarming Wasp Stings](spells/nec-04-LVwmAH5NGvTuuQSU.htm)||`@Compendium[pf2e.spells-srd.LVwmAH5NGvTuuQSU]`|
|[Swarmsense](spells/div-01-5wjl0ZwEvvUh7sor.htm)|Sens de la nuée|`@Compendium[pf2e.spells-srd.5wjl0ZwEvvUh7sor]`|
|[Sweet Dream](spells/enc-01-BRtKFk0PKfWIlCAB.htm)|Doux rêve|`@Compendium[pf2e.spells-srd.BRtKFk0PKfWIlCAB]`|
|[Symphony of the Unfettered Heart](spells/abj-05-ZAX0OOcKtYMQlquR.htm)|Symphonie du cœur affranchi|`@Compendium[pf2e.spells-srd.ZAX0OOcKtYMQlquR]`|
|[Synaptic Pulse](spells/enc-05-BilnTGuXrof9Dt9D.htm)|Pulsation synaptique|`@Compendium[pf2e.spells-srd.BilnTGuXrof9Dt9D]`|
|[Synesthesia](spells/div-05-OAt2ZEns1gIOCgrn.htm)|Synesthésie|`@Compendium[pf2e.spells-srd.OAt2ZEns1gIOCgrn]`|
|[Take its Course](spells/nec-04-ZeHeNQ5BNq6m5F1j.htm)|Suivre son cours|`@Compendium[pf2e.spells-srd.ZeHeNQ5BNq6m5F1j]`|
|[Talking Corpse](spells/nec-04-FM3SmEW8N1FCRjqt.htm)|Cadavre loquace|`@Compendium[pf2e.spells-srd.FM3SmEW8N1FCRjqt]`|
|[Tanglefoot](spells/con-00-uZK2BYzPnxUBnDjr.htm)|Entrave|`@Compendium[pf2e.spells-srd.uZK2BYzPnxUBnDjr]`|
|[Tangling Creepers](spells/con-06-JbAcSLu62TU1OgNF.htm)|Entraves végétales|`@Compendium[pf2e.spells-srd.JbAcSLu62TU1OgNF]`|
|[Telekinetic Haul](spells/evo-05-tpLTLbJUrYcMWGld.htm)|Transport télékinésique|`@Compendium[pf2e.spells-srd.tpLTLbJUrYcMWGld]`|
|[Telekinetic Maneuver](spells/evo-02-mrDi3v933gsmnw25.htm)|Manoeuvre télékinésique|`@Compendium[pf2e.spells-srd.mrDi3v933gsmnw25]`|
|[Telekinetic Projectile](spells/evo-00-60sgbuMWN0268dB7.htm)|Projectile télékinésique|`@Compendium[pf2e.spells-srd.60sgbuMWN0268dB7]`|
|[Telepathic Bond](spells/div-05-gfVXAW95YWRz0pJC.htm)|Lien télépathique|`@Compendium[pf2e.spells-srd.gfVXAW95YWRz0pJC]`|
|[Telepathic Demand](spells/enc-09-ItZgGznUBhgWBwFG.htm)|Ordre télépathique|`@Compendium[pf2e.spells-srd.ItZgGznUBhgWBwFG]`|
|[Telepathy](spells/div-04-HqTI6wRrck1YXp3F.htm)|Télépathie|`@Compendium[pf2e.spells-srd.HqTI6wRrck1YXp3F]`|
|[Teleport](spells/con-06-69L70wKfGDY66Mk9.htm)|Téléportation|`@Compendium[pf2e.spells-srd.69L70wKfGDY66Mk9]`|
|[Teleportation Circle](spells/con-07-5ZW1w9f4gWlSIuWA.htm)|Cercle de téléportation|`@Compendium[pf2e.spells-srd.5ZW1w9f4gWlSIuWA]`|
|[Tempest Form](spells/trs-06-zhqnMOVPzVvWSUbC.htm)|Forme tempêtueuse|`@Compendium[pf2e.spells-srd.zhqnMOVPzVvWSUbC]`|
|[Tempest Surge](spells/evo-01-ho1jSoYKrHUNnM90.htm)|Onde de tempête|`@Compendium[pf2e.spells-srd.ho1jSoYKrHUNnM90]`|
|[Tempest Touch](spells/evo-01-EzB9i7R6aBRAtJCh.htm)|Toucher tempêtueux|`@Compendium[pf2e.spells-srd.EzB9i7R6aBRAtJCh]`|
|[Temporary Tool](spells/con-01-TPI9fRCAYsDqpAe4.htm)|Outil temporaire|`@Compendium[pf2e.spells-srd.TPI9fRCAYsDqpAe4]`|
|[Tempt Fate](spells/div-04-GqvKSxzN7A7kuFk4.htm)|Tenter le sort|`@Compendium[pf2e.spells-srd.GqvKSxzN7A7kuFk4]`|
|[Tentacular Limbs](spells/trs-01-i8PBZsnoCrK7IWph.htm)|Membres tentaculaires|`@Compendium[pf2e.spells-srd.i8PBZsnoCrK7IWph]`|
|[Terminate Bloodline](spells/nec-06-yUM5OYeMY8971b2S.htm)||`@Compendium[pf2e.spells-srd.yUM5OYeMY8971b2S]`|
|[Terrain Transposition](spells/con-05-rtA3HRGoy7PQTOhq.htm)|Transposition de terrain|`@Compendium[pf2e.spells-srd.rtA3HRGoy7PQTOhq]`|
|[Tether](spells/abj-01-MVrxZarUTnJxAUN8.htm)|Liens|`@Compendium[pf2e.spells-srd.MVrxZarUTnJxAUN8]`|
|[Thoughtful Gift](spells/con-01-qTr2oCgIXl703Whb.htm)|Cadeau attentionné|`@Compendium[pf2e.spells-srd.qTr2oCgIXl703Whb]`|
|[Threefold Aspect](spells/trs-03-MNiT0dHol5fEcKlz.htm)||`@Compendium[pf2e.spells-srd.MNiT0dHol5fEcKlz]`|
|[Thunderburst](spells/evo-03-LFSwMtQVP05EzlZe.htm)|Explosion tempêtueuse|`@Compendium[pf2e.spells-srd.LFSwMtQVP05EzlZe]`|
|[Tidal Surge](spells/evo-01-iAnpxrLaBU4V6Sej.htm)|Raz-de-marée|`@Compendium[pf2e.spells-srd.iAnpxrLaBU4V6Sej]`|
|[Time Beacon](spells/div-07-G56DJkxlUjFv0C4Z.htm)|Repère temporel|`@Compendium[pf2e.spells-srd.G56DJkxlUjFv0C4Z]`|
|[Time Stop](spells/trs-10-1dsahW4g1ggXtypx.htm)|Arrêt du temps|`@Compendium[pf2e.spells-srd.1dsahW4g1ggXtypx]`|
|[Tireless Worker](spells/nec-04-gzvRDpM6EvcfYHeu.htm)||`@Compendium[pf2e.spells-srd.gzvRDpM6EvcfYHeu]`|
|[Tongues](spells/div-05-SwUiVavHKMWG7t5K.htm)|Don des langues|`@Compendium[pf2e.spells-srd.SwUiVavHKMWG7t5K]`|
|[Touch of Corruption](spells/nec-01-jFmWSIpJGGebim6y.htm)|Toucher de corruption|`@Compendium[pf2e.spells-srd.jFmWSIpJGGebim6y]`|
|[Touch of Idiocy](spells/enc-02-CQb8HtQ1BPeZmu9h.htm)|Idiotie|`@Compendium[pf2e.spells-srd.CQb8HtQ1BPeZmu9h]`|
|[Touch of Obedience](spells/enc-01-MmQiEc7aM9PDLO2J.htm)|Obéissance|`@Compendium[pf2e.spells-srd.MmQiEc7aM9PDLO2J]`|
|[Touch of Undeath](spells/nec-01-GYI4xloAgkm6tTrT.htm)|Bénédiction de la non-mort|`@Compendium[pf2e.spells-srd.GYI4xloAgkm6tTrT]`|
|[Touch of the Moon](spells/enc-04-oXCwHBeDja4e0Mx0.htm)|Contact lunaire|`@Compendium[pf2e.spells-srd.oXCwHBeDja4e0Mx0]`|
|[Transcribe Moment](spells/evo-04-NxOYiKCqcuAHVRCj.htm)|Instant transcrit|`@Compendium[pf2e.spells-srd.NxOYiKCqcuAHVRCj]`|
|[Transmute Rock And Mud](spells/trs-05-LzfrBDxxPTiuN7uL.htm)||`@Compendium[pf2e.spells-srd.LzfrBDxxPTiuN7uL]`|
|[Traveler's Transit](spells/evo-04-LrhTFHUtSS9ahogL.htm)|Voyageur en transit|`@Compendium[pf2e.spells-srd.LrhTFHUtSS9ahogL]`|
|[Tree Shape](spells/trs-02-dileJ0Yxqg76LMvu.htm)|Morphologie d'arbre|`@Compendium[pf2e.spells-srd.dileJ0Yxqg76LMvu]`|
|[Tree Stride](spells/con-05-F1qxaqsEItmBura2.htm)|Voyage par les arbres|`@Compendium[pf2e.spells-srd.F1qxaqsEItmBura2]`|
|[Trickster's Twin](spells/ill-04-ZhLYJlOZzUB1OKoe.htm)|Jumeau du trompeur|`@Compendium[pf2e.spells-srd.ZhLYJlOZzUB1OKoe]`|
|[Triple Time](spells/enc-00-c8R2fpk88fBwJ1ie.htm)|À trois temps|`@Compendium[pf2e.spells-srd.c8R2fpk88fBwJ1ie]`|
|[True Seeing](spells/div-06-uqlxMQQeSGWEVjki.htm)|Vision lucide|`@Compendium[pf2e.spells-srd.uqlxMQQeSGWEVjki]`|
|[True Strike](spells/div-01-Gb7SeieEvd0pL2Eh.htm)|Coup au but|`@Compendium[pf2e.spells-srd.Gb7SeieEvd0pL2Eh]`|
|[True Target](spells/div-07-AlbpWWN87yGegoAF.htm)|Cible verrouillée|`@Compendium[pf2e.spells-srd.AlbpWWN87yGegoAF]`|
|[Unblinking Flame Revelation](spells/abj-03-VBNevVovTCpn04vL.htm)|Révélation de la flamme qui ne vacille pas|`@Compendium[pf2e.spells-srd.VBNevVovTCpn04vL]`|
|[Unbreaking Wave Advance](spells/evo-03-ytboJsyZEbE1MLeV.htm)|Avancée de la vague qui ne se brise pas|`@Compendium[pf2e.spells-srd.ytboJsyZEbE1MLeV]`|
|[Uncontrollable Dance](spells/enc-08-Jvyy6oVIQsD34MHB.htm)|Danse incontrôlable|`@Compendium[pf2e.spells-srd.Jvyy6oVIQsD34MHB]`|
|[Undeath's Blessing](spells/nec-01-FedTjedva2rYk33r.htm)||`@Compendium[pf2e.spells-srd.FedTjedva2rYk33r]`|
|[Undetectable Alignment](spells/abj-02-FzAtX8yXBjTaisJK.htm)|Alignement indétectable|`@Compendium[pf2e.spells-srd.FzAtX8yXBjTaisJK]`|
|[Unfathomable Song](spells/enc-09-FmNDwqMEjeTEGPrY.htm)|Chant énigmatique|`@Compendium[pf2e.spells-srd.FmNDwqMEjeTEGPrY]`|
|[Unfettered Pack](spells/abj-07-bVtkBJvGLP69qVGI.htm)|Meute sans entrave|`@Compendium[pf2e.spells-srd.bVtkBJvGLP69qVGI]`|
|[Unfolding Wind Rush](spells/evo-03-tvO6Kmc2pQve9DC5.htm)|Ruée du vent qui see déploie|`@Compendium[pf2e.spells-srd.tvO6Kmc2pQve9DC5]`|
|[Unimpeded Stride](spells/trs-01-gwOYh5zMVZB0HNcT.htm)|Déplacement sans réserve|`@Compendium[pf2e.spells-srd.gwOYh5zMVZB0HNcT]`|
|[Unity](spells/abj-04-tYLBjOTvBVn9JtRb.htm)|Unité|`@Compendium[pf2e.spells-srd.tYLBjOTvBVn9JtRb]`|
|[Unrelenting Observation](spells/div-08-y2cQYLr5mljDSu1G.htm)|Observation implacable|`@Compendium[pf2e.spells-srd.y2cQYLr5mljDSu1G]`|
|[Unseasonable Squall](spells/evo-03-TT9owkeMBXJxcERB.htm)||`@Compendium[pf2e.spells-srd.TT9owkeMBXJxcERB]`|
|[Unseen Custodians](spells/con-03-uuoPmbjEtqzWZs0v.htm)|Domestiques invisibles|`@Compendium[pf2e.spells-srd.uuoPmbjEtqzWZs0v]`|
|[Unseen Servant](spells/con-01-xqmHD8JIjak15lRk.htm)|Serviteur invisible|`@Compendium[pf2e.spells-srd.xqmHD8JIjak15lRk]`|
|[Untwisting Iron Buffer](spells/abj-03-FPnkOYyWIaOzkmqn.htm)|Protection du fer qui ne plie pas|`@Compendium[pf2e.spells-srd.FPnkOYyWIaOzkmqn]`|
|[Unusual Anatomy](spells/trs-05-ddKBoCjmSyPSHcws.htm)|Étrange anatomie|`@Compendium[pf2e.spells-srd.ddKBoCjmSyPSHcws]`|
|[Vampiric Exsanguination](spells/nec-06-fd31tAHSSGXyOxW6.htm)|Saignée du vampire|`@Compendium[pf2e.spells-srd.fd31tAHSSGXyOxW6]`|
|[Vampiric Maiden](spells/nec-04-07xYlmGX32XtHGEt.htm)||`@Compendium[pf2e.spells-srd.07xYlmGX32XtHGEt]`|
|[Vampiric Touch](spells/nec-03-N1Z1oLPdBxaSgrEE.htm)|Contact vampirique|`@Compendium[pf2e.spells-srd.N1Z1oLPdBxaSgrEE]`|
|[Veil](spells/ill-04-hkfH9Z53hPzcOwNB.htm)|Voile|`@Compendium[pf2e.spells-srd.hkfH9Z53hPzcOwNB]`|
|[Veil of Confidence](spells/enc-01-Q25JQAgnJSGgFDKZ.htm)|Voile de confiance|`@Compendium[pf2e.spells-srd.Q25JQAgnJSGgFDKZ]`|
|[Veil of Dreams](spells/enc-01-NNoKWiWKqJkdD2ln.htm)|Voile de rêves|`@Compendium[pf2e.spells-srd.NNoKWiWKqJkdD2ln]`|
|[Ventriloquism](spells/ill-01-yV7Ouzaoe7DHLESI.htm)|Ventriloquie|`@Compendium[pf2e.spells-srd.yV7Ouzaoe7DHLESI]`|
|[Verdant Sprout](spells/con-01-EDABphKEPUBiMmQC.htm)|Éclosion verdoyante|`@Compendium[pf2e.spells-srd.EDABphKEPUBiMmQC]`|
|[Vibrant Pattern](spells/ill-06-RQjSQVZRG497cJhX.htm)|Motif éclatant|`@Compendium[pf2e.spells-srd.RQjSQVZRG497cJhX]`|
|[Vibrant Thorns](spells/trs-01-nnSipUPNd3sm5vYL.htm)|Épines florissantes|`@Compendium[pf2e.spells-srd.nnSipUPNd3sm5vYL]`|
|[Vigilant Eye](spells/div-04-4LSf04FFvDgMyDk6.htm)|Œil vigilant|`@Compendium[pf2e.spells-srd.4LSf04FFvDgMyDk6]`|
|[Vision of Weakness](spells/div-02-NhNKzq1DvFxkvTEc.htm)|Vision de faiblesse|`@Compendium[pf2e.spells-srd.NhNKzq1DvFxkvTEc]`|
|[Visions of Danger](spells/ill-07-jBGAYmR0BkkbpJvG.htm)|Dangereuses visions|`@Compendium[pf2e.spells-srd.jBGAYmR0BkkbpJvG]`|
|[Vital Beacon](spells/nec-04-ikSb3LRGnrwXJBVX.htm)|Fanal de vie|`@Compendium[pf2e.spells-srd.ikSb3LRGnrwXJBVX]`|
|[Volcanic Eruption](spells/evo-07-O7ZEqWjwdKyo2CUv.htm)|Eruption volcanique|`@Compendium[pf2e.spells-srd.O7ZEqWjwdKyo2CUv]`|
|[Vomit Swarm](spells/evo-02-cB17yFc9456Pyfec.htm)||`@Compendium[pf2e.spells-srd.cB17yFc9456Pyfec]`|
|[Wail of the Banshee](spells/nec-09-FEsuyf203wTNE2et.htm)|Plainte d'outre-tombe|`@Compendium[pf2e.spells-srd.FEsuyf203wTNE2et]`|
|[Waking Nightmare](spells/enc-01-DU5daB09xwfE1y38.htm)|Cauchemar éveillé|`@Compendium[pf2e.spells-srd.DU5daB09xwfE1y38]`|
|[Wall Of Flesh](spells/nec-05-ZLLY6ThJXCCrO0rL.htm)||`@Compendium[pf2e.spells-srd.ZLLY6ThJXCCrO0rL]`|
|[Wall of Fire](spells/evo-04-IarZrgCeaiUqOuRu.htm)|Mur de feu|`@Compendium[pf2e.spells-srd.IarZrgCeaiUqOuRu]`|
|[Wall of Force](spells/evo-06-7Iela4GgVeO3LfAo.htm)|Mur de force|`@Compendium[pf2e.spells-srd.7Iela4GgVeO3LfAo]`|
|[Wall of Ice](spells/evo-05-R5FHRv7VqyRnxg2t.htm)|Mur de glace|`@Compendium[pf2e.spells-srd.R5FHRv7VqyRnxg2t]`|
|[Wall of Stone](spells/con-05-kOa055FIrO9Smnya.htm)|Mur de pierre|`@Compendium[pf2e.spells-srd.kOa055FIrO9Smnya]`|
|[Wall of Thorns](spells/con-03-KsWhliKfUs3IpW3c.htm)|Mur d'épines|`@Compendium[pf2e.spells-srd.KsWhliKfUs3IpW3c]`|
|[Wall of Wind](spells/evo-03-it4ZsAi6XgvGcodc.htm)|Mur de vent|`@Compendium[pf2e.spells-srd.it4ZsAi6XgvGcodc]`|
|[Wanderer's Guide](spells/div-03-ppA1StEigPLKEQqR.htm)|Guide du pélerin|`@Compendium[pf2e.spells-srd.ppA1StEigPLKEQqR]`|
|[Ward Domain](spells/abj-06-Er9XNUlL0wB0PM36.htm)|Protection de domaine|`@Compendium[pf2e.spells-srd.Er9XNUlL0wB0PM36]`|
|[Warp Mind](spells/enc-07-8kJbiBEjMWG4VUjs.htm)|Déformer l'esprit|`@Compendium[pf2e.spells-srd.8kJbiBEjMWG4VUjs]`|
|[Warped Terrain](spells/ill-01-fXdADBwxmBsU9xPk.htm)|Terrain déformé|`@Compendium[pf2e.spells-srd.fXdADBwxmBsU9xPk]`|
|[Water Breathing](spells/trs-02-MZGkMsPBztFN0pUO.htm)|Respiration aquatique|`@Compendium[pf2e.spells-srd.MZGkMsPBztFN0pUO]`|
|[Water Walk](spells/trs-02-Seaah9amXg70RKw2.htm)|Marche sur l'eau|`@Compendium[pf2e.spells-srd.Seaah9amXg70RKw2]`|
|[Weapon Storm](spells/evo-04-8M03UxGXjYyDFAoy.htm)|Tempête d'armes|`@Compendium[pf2e.spells-srd.8M03UxGXjYyDFAoy]`|
|[Weapon Surge](spells/evo-01-W37iBXLsY2trJ1rS.htm)|Arme améliorée|`@Compendium[pf2e.spells-srd.W37iBXLsY2trJ1rS]`|
|[Weapon of Judgement](spells/evo-09-ZqmP9gijBmK7y8Xy.htm)|Arme du jugement|`@Compendium[pf2e.spells-srd.ZqmP9gijBmK7y8Xy]`|
|[Web](spells/con-02-9s5tqqXNzcoKamWx.htm)|Toile d'araignée|`@Compendium[pf2e.spells-srd.9s5tqqXNzcoKamWx]`|
|[Weird](spells/ill-09-qDjeG6dxT4aEEC6J.htm)|Ennemi subconscient|`@Compendium[pf2e.spells-srd.qDjeG6dxT4aEEC6J]`|
|[Whirling Flames](spells/evo-03-Wi2HcreCfujKiCvW.htm)||`@Compendium[pf2e.spells-srd.Wi2HcreCfujKiCvW]`|
|[Whirling Scarves](spells/abj-03-k9x6bXXpIgAXMDsx.htm)|Écharpes tourbillonnantes|`@Compendium[pf2e.spells-srd.k9x6bXXpIgAXMDsx]`|
|[Wholeness of Body](spells/nec-02-qJZZdYBdNaWRJFER.htm)|Plénitude physique|`@Compendium[pf2e.spells-srd.qJZZdYBdNaWRJFER]`|
|[Wild Morph](spells/trs-01-0xR9vrt6uDFl0Umo.htm)|Métamorphose sauvage|`@Compendium[pf2e.spells-srd.0xR9vrt6uDFl0Umo]`|
|[Wild Shape](spells/trs-01-8RWfKConLYFZpQ9X.htm)|Morphologie sauvage|`@Compendium[pf2e.spells-srd.8RWfKConLYFZpQ9X]`|
|[Wild Winds Stance](spells/evo-04-SkarN4VlNxSJSJNw.htm)|Posture des vents violents|`@Compendium[pf2e.spells-srd.SkarN4VlNxSJSJNw]`|
|[Wilding Word](spells/enc-00-GdN5YQE47gd79k7X.htm)|Paroles sauvages|`@Compendium[pf2e.spells-srd.GdN5YQE47gd79k7X]`|
|[Wind Jump](spells/trs-05-kRxlkPPe6Gr7Du59.htm)|Saut du vent|`@Compendium[pf2e.spells-srd.kRxlkPPe6Gr7Du59]`|
|[Wind Walk](spells/trs-08-U0hL0LLaprcnAyzC.htm)|Voyage venteux|`@Compendium[pf2e.spells-srd.U0hL0LLaprcnAyzC]`|
|[Winter Bolt](spells/evo-01-DeNz6eAUlE0IE9U3.htm)||`@Compendium[pf2e.spells-srd.DeNz6eAUlE0IE9U3]`|
|[Wish](spells/div-10-6dDtGIUerazSHIOu.htm)|Souhait|`@Compendium[pf2e.spells-srd.6dDtGIUerazSHIOu]`|
|[Wish-Twisted Form](spells/trs-05-B3tbO85GBpzQ3u8l.htm)|Forme déformée par un souhait|`@Compendium[pf2e.spells-srd.B3tbO85GBpzQ3u8l]`|
|[Withering Grasp](spells/nec-01-7tp97g0UCJ9wOrd5.htm)||`@Compendium[pf2e.spells-srd.7tp97g0UCJ9wOrd5]`|
|[Word of Freedom](spells/abj-04-2jB9eyX7YekwoCvA.htm)|Mot de liberté|`@Compendium[pf2e.spells-srd.2jB9eyX7YekwoCvA]`|
|[Word of Recall](spells/con-07-nzbnTqHgNKiGZkrZ.htm)|Mot de Rappel|`@Compendium[pf2e.spells-srd.nzbnTqHgNKiGZkrZ]`|
|[Word of Truth](spells/div-01-aF7RiG7c8GzSQLYt.htm)|Mot de vérité|`@Compendium[pf2e.spells-srd.aF7RiG7c8GzSQLYt]`|
|[Worm's Repast](spells/nec-02-H4oF5szC7aogqtvw.htm)||`@Compendium[pf2e.spells-srd.H4oF5szC7aogqtvw]`|
|[Wyvern Sting](spells/nec-05-IoHxAkK0uGqrgtWl.htm)||`@Compendium[pf2e.spells-srd.IoHxAkK0uGqrgtWl]`|
|[You're Mine](spells/enc-05-x2LALaHXO7644GQA.htm)|Tu es à moi|`@Compendium[pf2e.spells-srd.x2LALaHXO7644GQA]`|
|[Zeal for Battle](spells/enc-04-I8CPe9Pp7GABqOyB.htm)|Zèle au combat|`@Compendium[pf2e.spells-srd.I8CPe9Pp7GABqOyB]`|
|[Zealous Conviction](spells/enc-06-GYD0XZ4t3tQq6shc.htm)|Conviction zélée|`@Compendium[pf2e.spells-srd.GYD0XZ4t3tQq6shc]`|
|[Zenith Star](spells/div-01-Yrek2Yd4k3DPC2zV.htm)|Étoile au zénith|`@Compendium[pf2e.spells-srd.Yrek2Yd4k3DPC2zV]`|
|[Zone of Truth](spells/enc-03-aewxsale5xWEPKLk.htm)|Zone de vérité|`@Compendium[pf2e.spells-srd.aewxsale5xWEPKLk]`|


## Équipement

| Nom (EN)   | Nom (FR)    | Lien compendium |
|------------|-------------|-----------------|
|[Abadar's Flawless Scale](equipment/equipment-03-R09AZzvyNA3Jginm.htm)|Balance sans défaut d'Abadar|`@Compendium[pf2e.equipment-srd.R09AZzvyNA3Jginm]`|
|[Acid Flask (Greater)](equipment/weapon-11-jC8GmH0Un6vDxdMj.htm)|Fiole d'acide (Supérieur)|`@Compendium[pf2e.equipment-srd.jC8GmH0Un6vDxdMj]`|
|[Acid Flask (Lesser)](equipment/weapon-01-M1k5QQc1qQLxzyCK.htm)|Fiole d’acide (Inférieur)|`@Compendium[pf2e.equipment-srd.M1k5QQc1qQLxzyCK]`|
|[Acid Flask (Major)](equipment/weapon-17-GttLYMT2welSpwCd.htm)|Fiole d'acide (Majeur)|`@Compendium[pf2e.equipment-srd.GttLYMT2welSpwCd]`|
|[Acid Flask (Moderate)](equipment/weapon-03-SgtqZxt26BdjUmEB.htm)|Fiole d'acide (Modéré)|`@Compendium[pf2e.equipment-srd.SgtqZxt26BdjUmEB]`|
|[Acrobat's Staff](equipment/weapon-06-17Oza88ts0ASngdw.htm)|Bâton de l'Acrobate|`@Compendium[pf2e.equipment-srd.17Oza88ts0ASngdw]`|
|[Adamantine Buckler (High-Grade)](equipment/armor-16-AbYyx61j1K4F27tC.htm)|Targe en adamantium (Haute qualité)|`@Compendium[pf2e.equipment-srd.AbYyx61j1K4F27tC]`|
|[Adamantine Buckler (Standard-Grade)](equipment/armor-08-4aVkppixLHPg940o.htm)|Targe en adamantium (qualité standard)|`@Compendium[pf2e.equipment-srd.4aVkppixLHPg940o]`|
|[Adamantine Chunk](equipment/equipment-00-Zo5MZWVBKssVPEcv.htm)|Morceau d'adamantium|`@Compendium[pf2e.equipment-srd.Zo5MZWVBKssVPEcv]`|
|[Adamantine Ingot](equipment/equipment-00-8WH6ub3FVFYtcXCT.htm)|Lingot d'adamantium|`@Compendium[pf2e.equipment-srd.8WH6ub3FVFYtcXCT]`|
|[Adamantine Shield (High-Grade)](equipment/armor-16-rA1hULhhqOP2u4Ze.htm)|Bouclier en adamantium (Haute qualité)|`@Compendium[pf2e.equipment-srd.rA1hULhhqOP2u4Ze]`|
|[Adamantine Shield (Standard-Grade)](equipment/armor-08-2gMlhHlkQp3DuWw5.htm)|Bouclier en adamantium (qualité standard)|`@Compendium[pf2e.equipment-srd.2gMlhHlkQp3DuWw5]`|
|[Addiction Suppressant (Greater)](equipment/consumable-10-ZOOrK4yEW6rFpPJ1.htm)||`@Compendium[pf2e.equipment-srd.ZOOrK4yEW6rFpPJ1]`|
|[Addiction Suppressant (Lesser)](equipment/consumable-01-SNMggNsvgPoAYjpU.htm)|Suppresseur de dépendance (inférieur)|`@Compendium[pf2e.equipment-srd.SNMggNsvgPoAYjpU]`|
|[Addiction Suppressant (Major)](equipment/consumable-14-esDSCEI1cf8Op93I.htm)||`@Compendium[pf2e.equipment-srd.esDSCEI1cf8Op93I]`|
|[Addiction Suppressant (Moderate)](equipment/consumable-06-cWYa0i1BqhbEruD6.htm)||`@Compendium[pf2e.equipment-srd.cWYa0i1BqhbEruD6]`|
|[Addlebrain](equipment/consumable-07-d5aCuFS1dhKXhsZ0.htm)|Brouillecervelle|`@Compendium[pf2e.equipment-srd.d5aCuFS1dhKXhsZ0]`|
|[Advanced Book of Translation (Tien)](equipment/equipment-05-gjjF0sQMpXf3CrTF.htm)||`@Compendium[pf2e.equipment-srd.gjjF0sQMpXf3CrTF]`|
|[Adventurer's Pack](equipment/kit-2req0jGaxz8hScdB.htm)|Pack d'aventurier|`@Compendium[pf2e.equipment-srd.2req0jGaxz8hScdB]`|
|[Aeon Stone (Agate Ellipsoid)](equipment/equipment-05-47FmnpSOE96SJ8H4.htm)||`@Compendium[pf2e.equipment-srd.47FmnpSOE96SJ8H4]`|
|[Aeon Stone (Azure Briolette)](equipment/equipment-04-f9vNVQ2v45mDI8Xr.htm)||`@Compendium[pf2e.equipment-srd.f9vNVQ2v45mDI8Xr]`|
|[Aeon Stone (Black Pearl)](equipment/equipment-12-J7vQowV531Sz73Gw.htm)|Pierre d'éternité (Perle noire)|`@Compendium[pf2e.equipment-srd.J7vQowV531Sz73Gw]`|
|[Aeon Stone (Clear Spindle)](equipment/equipment-07-M2CPAgSAoEL4oawq.htm)|Pierre d'éternité (fuseau translucide)|`@Compendium[pf2e.equipment-srd.M2CPAgSAoEL4oawq]`|
|[Aeon Stone (Dull Grey)](equipment/equipment-01-CVTbxCY85nLoHYuw.htm)|Pierre d'éternité (gris terne)|`@Compendium[pf2e.equipment-srd.CVTbxCY85nLoHYuw]`|
|[Aeon Stone (Dusty Rose Prism)](equipment/equipment-03-TKr8IL5F3cxsfHPH.htm)||`@Compendium[pf2e.equipment-srd.TKr8IL5F3cxsfHPH]`|
|[Aeon Stone (Gold Nodule)](equipment/equipment-06-odSvUKzphdwvDqgE.htm)|Pierre d'éternité (nodule doré)|`@Compendium[pf2e.equipment-srd.odSvUKzphdwvDqgE]`|
|[Aeon Stone (Lavender and Green Ellipsoid)](equipment/equipment-19-5sMAAIymln2yIl4q.htm)|Pierre d'éternité (ellipsoïde vert et lavande)|`@Compendium[pf2e.equipment-srd.5sMAAIymln2yIl4q]`|
|[Aeon Stone (Orange Prism)](equipment/equipment-16-MO8J2IcBhiTnm9D8.htm)|Pierre d'éternité (prisme orange)|`@Compendium[pf2e.equipment-srd.MO8J2IcBhiTnm9D8]`|
|[Aeon Stone (Pale Lavender Ellipsoid)](equipment/equipment-13-B4R0LK8bgw0Vjtf0.htm)|Pierre d'éternité (ellipsoïde lavande pâle)|`@Compendium[pf2e.equipment-srd.B4R0LK8bgw0Vjtf0]`|
|[Aeon Stone (Pale Orange Rhomboid)](equipment/equipment-19-U1GAAGhbzX9Lynq0.htm)||`@Compendium[pf2e.equipment-srd.U1GAAGhbzX9Lynq0]`|
|[Aeon Stone (Pearlescent Pyramid)](equipment/equipment-10-5SKGqxahAGsJgzRF.htm)||`@Compendium[pf2e.equipment-srd.5SKGqxahAGsJgzRF]`|
|[Aeon Stone (Pearly White Spindle)](equipment/equipment-35-4A8SFipG78SMWQEU.htm)||`@Compendium[pf2e.equipment-srd.4A8SFipG78SMWQEU]`|
|[Aeon Stone (Pink Rhomboid)](equipment/equipment-12-lJ3BzUZkGTOCWleL.htm)|Pierre d'éternité (Rhombe rose vif)|`@Compendium[pf2e.equipment-srd.lJ3BzUZkGTOCWleL]`|
|[Aeon Stone (Tourmaline Sphere)](equipment/equipment-07-kSaUlWgYMywIRV3C.htm)|Pierre d'éternité (sphère tourmaline)|`@Compendium[pf2e.equipment-srd.kSaUlWgYMywIRV3C]`|
|[Aeon Stone (Western Star)](equipment/equipment-06-V5kv7e1r2qV2k3lf.htm)||`@Compendium[pf2e.equipment-srd.V5kv7e1r2qV2k3lf]`|
|[Agate](equipment/treasure-7SPJO9xr89N8E23s.htm)|Agate|`@Compendium[pf2e.equipment-srd.7SPJO9xr89N8E23s]`|
|[Aklys](equipment/weapon-00-YnPYSKCQBLIOtm0J.htm)|Aklys|`@Compendium[pf2e.equipment-srd.YnPYSKCQBLIOtm0J]`|
|[Alabaster](equipment/treasure-nIlx1IQhYJfQtpVF.htm)|Albâtre|`@Compendium[pf2e.equipment-srd.nIlx1IQhYJfQtpVF]`|
|[Alabaster and obsidian game set](equipment/treasure-00-hnzNKdD5hjQc1NUx.htm)|pièces d'un jeu en albâtre et en obsidienne|`@Compendium[pf2e.equipment-srd.hnzNKdD5hjQc1NUx]`|
|[Alabaster idol](equipment/treasure-r7QGx0YFZCR0W5iu.htm)|idôle en albâtre|`@Compendium[pf2e.equipment-srd.r7QGx0YFZCR0W5iu]`|
|[Alarm Snare](equipment/consumable-01-AFX1V0Go9DqPWBlN.htm)|Piège artisanal d'alarme|`@Compendium[pf2e.equipment-srd.AFX1V0Go9DqPWBlN]`|
|[Alchemical Atomizer](equipment/equipment-10-whShgzN5YBzrXV3r.htm)||`@Compendium[pf2e.equipment-srd.whShgzN5YBzrXV3r]`|
|[Alchemical Crossbow](equipment/weapon-00-loueS11Tfa9WD320.htm)|Arbalète alchimique|`@Compendium[pf2e.equipment-srd.loueS11Tfa9WD320]`|
|[Alchemist Goggles](equipment/equipment-04-7JVgLiNTAs4clEW8.htm)|Lunettes d'alchimiste|`@Compendium[pf2e.equipment-srd.7JVgLiNTAs4clEW8]`|
|[Alchemist Goggles (Greater)](equipment/equipment-11-8XZqdeZStFAM4XnH.htm)|Lunettes d'alchimiste (supérieurs)|`@Compendium[pf2e.equipment-srd.8XZqdeZStFAM4XnH]`|
|[Alchemist Goggles (Major)](equipment/equipment-17-bnmfBLXOBd3ah6GK.htm)|Lunettes d'alchimistes (majeures)|`@Compendium[pf2e.equipment-srd.bnmfBLXOBd3ah6GK]`|
|[Alchemist's Fire (Greater)](equipment/weapon-11-n5yTlTs63o53n3k6.htm)|Feu grégeois (supérieur)|`@Compendium[pf2e.equipment-srd.n5yTlTs63o53n3k6]`|
|[Alchemist's Fire (Lesser)](equipment/weapon-01-yd3kEK21YknZLlcT.htm)|Feu Grégeois (Inférieur)|`@Compendium[pf2e.equipment-srd.yd3kEK21YknZLlcT]`|
|[Alchemist's Fire (Major)](equipment/weapon-17-cLZrOihPht5M7eTw.htm)|Feu grégeois (majeur)|`@Compendium[pf2e.equipment-srd.cLZrOihPht5M7eTw]`|
|[Alchemist's Fire (Moderate)](equipment/weapon-03-gWr4q4HiyGhETA8H.htm)|Feu grégeois (modéré)|`@Compendium[pf2e.equipment-srd.gWr4q4HiyGhETA8H]`|
|[Alchemist's Lab](equipment/equipment-00-k6xZkDUpF7E1Totd.htm)|Laboratoire d’alchimie|`@Compendium[pf2e.equipment-srd.k6xZkDUpF7E1Totd]`|
|[Alchemist's Lab (Expanded)](equipment/equipment-03-uhka9LHEP3wDKytG.htm)|Laboratoire d'alchimie (complet)|`@Compendium[pf2e.equipment-srd.uhka9LHEP3wDKytG]`|
|[Alchemist's Tools](equipment/equipment-00-4ftXXUCBHcf4b0MH.htm)|Outils d'alchimiste|`@Compendium[pf2e.equipment-srd.4ftXXUCBHcf4b0MH]`|
|[Alcohol](equipment/consumable-00-UMAXLDpI6YLSfYX1.htm)|Alcool|`@Compendium[pf2e.equipment-srd.UMAXLDpI6YLSfYX1]`|
|[Aldori Dueling Sword](equipment/weapon-01-iSceAVMkVv1uKK7t.htm)|Épée de duel aldorie|`@Compendium[pf2e.equipment-srd.iSceAVMkVv1uKK7t]`|
|[Aligned Oil](equipment/consumable-09-3uqUIZZuEyAMORUi.htm)|Huile alignée|`@Compendium[pf2e.equipment-srd.3uqUIZZuEyAMORUi]`|
|[Alignment Ampoule (Greater)](equipment/weapon-11-s3wY38FriyervrtC.htm)||`@Compendium[pf2e.equipment-srd.s3wY38FriyervrtC]`|
|[Alignment Ampoule (Lesser)](equipment/weapon-01-WNWnpAsmFoFUlQLK.htm)||`@Compendium[pf2e.equipment-srd.WNWnpAsmFoFUlQLK]`|
|[Alignment Ampoule (Major)](equipment/weapon-17-Aulo25tnSdOCOmrI.htm)||`@Compendium[pf2e.equipment-srd.Aulo25tnSdOCOmrI]`|
|[Alignment Ampoule (Moderate)](equipment/weapon-03-K7vNtVcHUKB9wks1.htm)||`@Compendium[pf2e.equipment-srd.K7vNtVcHUKB9wks1]`|
|[Aluum Charm](equipment/equipment-10-bU731I8njNmSAQhQ.htm)||`@Compendium[pf2e.equipment-srd.bU731I8njNmSAQhQ]`|
|[Amber](equipment/treasure-qFkhYAeCxYci8jgk.htm)|Ambre|`@Compendium[pf2e.equipment-srd.qFkhYAeCxYci8jgk]`|
|[Ambrosia of Undying Hope](equipment/consumable-19-gn5ITN5GILHKxGA4.htm)||`@Compendium[pf2e.equipment-srd.gn5ITN5GILHKxGA4]`|
|[Amethyst](equipment/treasure-00-epTuWsVDuPzUXMk9.htm)|Amethyste|`@Compendium[pf2e.equipment-srd.epTuWsVDuPzUXMk9]`|
|[Amphora with lavish scenes](equipment/treasure-JYxnuXg9MV6y1gpQ.htm)|Amphore portant des scènes somptueuses|`@Compendium[pf2e.equipment-srd.JYxnuXg9MV6y1gpQ]`|
|[Anarchic](equipment/equipment-11-65YL6nk1jIzCWutt.htm)|Anarchique|`@Compendium[pf2e.equipment-srd.65YL6nk1jIzCWutt]`|
|[Ancestral Echoing](equipment/equipment-15-oL8G6OqITPJ5Fd6A.htm)||`@Compendium[pf2e.equipment-srd.oL8G6OqITPJ5Fd6A]`|
|[Anchor Spear](equipment/weapon-13-kSLHMQjKL77CtvBx.htm)||`@Compendium[pf2e.equipment-srd.kSLHMQjKL77CtvBx]`|
|[Ancient dragon skull etched with mystic sigils](equipment/treasure-00-7Uk6LMmzsCxuhhA6.htm)|Crâne de dragon vénérable gravé de symboles mystiques|`@Compendium[pf2e.equipment-srd.7Uk6LMmzsCxuhhA6]`|
|[Animal Staff](equipment/weapon-04-WcuknnE3xYfSdbhm.htm)|Bâton animal|`@Compendium[pf2e.equipment-srd.WcuknnE3xYfSdbhm]`|
|[Animal Staff (Greater)](equipment/weapon-08-bT9azgpc96DNbitA.htm)|Bâton animal (supérieur)|`@Compendium[pf2e.equipment-srd.bT9azgpc96DNbitA]`|
|[Animal Staff (Major)](equipment/weapon-12-Oo7IpJQDSTmzUyzG.htm)|Bâton animal (majeur)|`@Compendium[pf2e.equipment-srd.Oo7IpJQDSTmzUyzG]`|
|[Anklets of Alacrity](equipment/equipment-17-9IwSktj0Xj7A2Ruh.htm)|Chaines de chevilles d'alacrité|`@Compendium[pf2e.equipment-srd.9IwSktj0Xj7A2Ruh]`|
|[Ankylostar](equipment/weapon-13-ZuyzSxmg4QdnZqLy.htm)||`@Compendium[pf2e.equipment-srd.ZuyzSxmg4QdnZqLy]`|
|[Antidote (Greater)](equipment/consumable-10-XWkeL34yJK6t5qUE.htm)|Antidote (supérieur)|`@Compendium[pf2e.equipment-srd.XWkeL34yJK6t5qUE]`|
|[Antidote (Lesser)](equipment/consumable-01-ktjFOp3U0wQD9t0Z.htm)|Antidote (Inférieur)|`@Compendium[pf2e.equipment-srd.ktjFOp3U0wQD9t0Z]`|
|[Antidote (Major)](equipment/consumable-14-ANzB9WzlGQXDn76G.htm)|Antidote (majeur)|`@Compendium[pf2e.equipment-srd.ANzB9WzlGQXDn76G]`|
|[Antidote (Moderate)](equipment/consumable-06-AJ1dC7EtTIfBey0M.htm)|Antidote (modéré)|`@Compendium[pf2e.equipment-srd.AJ1dC7EtTIfBey0M]`|
|[Antimagic](equipment/equipment-15-o02lg3k1RBoFXVFV.htm)|Antimagie (rune)|`@Compendium[pf2e.equipment-srd.o02lg3k1RBoFXVFV]`|
|[Antimagic Oil](equipment/consumable-20-737LQfIaLXBPEzc3.htm)|Huile antimagie|`@Compendium[pf2e.equipment-srd.737LQfIaLXBPEzc3]`|
|[Antiplague (Greater)](equipment/consumable-10-ORDaAzMZLrf6Hn8A.htm)|Antimaladie (supérieur)|`@Compendium[pf2e.equipment-srd.ORDaAzMZLrf6Hn8A]`|
|[Antiplague (Lesser)](equipment/consumable-01-UqinuuCWePTYGhVO.htm)|Antimaladie (Inférieur)|`@Compendium[pf2e.equipment-srd.UqinuuCWePTYGhVO]`|
|[Antiplague (Major)](equipment/consumable-14-kajpTXg3oHIJStQa.htm)|Antimaladie (majeur)|`@Compendium[pf2e.equipment-srd.kajpTXg3oHIJStQa]`|
|[Antiplague (Moderate)](equipment/consumable-06-biRbqKo2C97XLfQ0.htm)|Antimaladie (modéré)|`@Compendium[pf2e.equipment-srd.biRbqKo2C97XLfQ0]`|
|[Antivenom Potion](equipment/consumable-02-N3jcmW5XzEJZQVtJ.htm)||`@Compendium[pf2e.equipment-srd.N3jcmW5XzEJZQVtJ]`|
|[Antler Arrow](equipment/consumable-02-NSQOijKqomyotXkj.htm)||`@Compendium[pf2e.equipment-srd.NSQOijKqomyotXkj]`|
|[Applereed Mutagen (Greater)](equipment/consumable-18-vTEWlRBUD4VzVwxK.htm)||`@Compendium[pf2e.equipment-srd.vTEWlRBUD4VzVwxK]`|
|[Applereed Mutagen (Lesser)](equipment/consumable-04-0RTsyWutvO42O0zK.htm)||`@Compendium[pf2e.equipment-srd.0RTsyWutvO42O0zK]`|
|[Applereed Mutagen (Moderate)](equipment/consumable-12-8e37h6HBr4ZyBSGt.htm)||`@Compendium[pf2e.equipment-srd.8e37h6HBr4ZyBSGt]`|
|[Aquamarine](equipment/treasure-00-dBQQZUXaMhCAFkpY.htm)|Aigue-marine|`@Compendium[pf2e.equipment-srd.dBQQZUXaMhCAFkpY]`|
|[Arcane Scroll Case of Simplicity](equipment/equipment-04-vxBcsWCjWd6DZ0Jz.htm)|Étui à parchemin de simplicité arcanique|`@Compendium[pf2e.equipment-srd.vxBcsWCjWd6DZ0Jz]`|
|[Archaic Wayfinder](equipment/equipment-02-hyMbJb2MRF5beIaU.htm)|Guide archaïque|`@Compendium[pf2e.equipment-srd.hyMbJb2MRF5beIaU]`|
|[Armbands of Athleticism](equipment/equipment-09-wI4Tj8bNwpZHequC.htm)|Brassards d'athlète|`@Compendium[pf2e.equipment-srd.wI4Tj8bNwpZHequC]`|
|[Armbands of Athleticism (Greater)](equipment/equipment-17-FNccUmKsyXKmfe5c.htm)|Brassards d'athlète (supérieurs)|`@Compendium[pf2e.equipment-srd.FNccUmKsyXKmfe5c]`|
|[Armor Potency (+1)](equipment/equipment-05-Ro3g2JpJXrKXVyEr.htm)|Puissance d'armure (+1)|`@Compendium[pf2e.equipment-srd.Ro3g2JpJXrKXVyEr]`|
|[Armor Potency (+2)](equipment/equipment-11-4Hb8tvvWFtykbZhG.htm)|Puissance d'armure (+2)|`@Compendium[pf2e.equipment-srd.4Hb8tvvWFtykbZhG]`|
|[Armor Potency (+3)](equipment/equipment-18-6ELjUFY0sEJ7nZlZ.htm)|Puissance d'armure (+3)|`@Compendium[pf2e.equipment-srd.6ELjUFY0sEJ7nZlZ]`|
|[Armored Skirt](equipment/equipment-00-bkbL0YitEh46Ne0f.htm)|Jupe d'armure|`@Compendium[pf2e.equipment-srd.bkbL0YitEh46Ne0f]`|
|[Aroden's Hearthstone](equipment/equipment-25-1ZOTwnqA9ccfdrey.htm)|Cardioline d'Aroden|`@Compendium[pf2e.equipment-srd.1ZOTwnqA9ccfdrey]`|
|[Arrow-Catching Shield](equipment/armor-11-9ZGkQo739t9utj37.htm)|Bouclier intercepteur de projectiles|`@Compendium[pf2e.equipment-srd.9ZGkQo739t9utj37]`|
|[Arrows](equipment/consumable-00-w2ENw2VMPcsbif8g.htm)|Flèches|`@Compendium[pf2e.equipment-srd.w2ENw2VMPcsbif8g]`|
|[Arsenic](equipment/consumable-01-OIirLySQDLZgT15S.htm)|Arsenic|`@Compendium[pf2e.equipment-srd.OIirLySQDLZgT15S]`|
|[Artisan's Tools](equipment/equipment-00-y34yjumCFakrbtdw.htm)|Outils d'artisan|`@Compendium[pf2e.equipment-srd.y34yjumCFakrbtdw]`|
|[Artisan's Tools (Sterling)](equipment/equipment-03-0QgniSjpzksm5riV.htm)|Outils d'artisan (excellents)|`@Compendium[pf2e.equipment-srd.0QgniSjpzksm5riV]`|
|[Aurifying Salts](equipment/consumable-03-pGnoCqFy0ESPbuGv.htm)||`@Compendium[pf2e.equipment-srd.pGnoCqFy0ESPbuGv]`|
|[Axe of the Dwarven Lords](equipment/weapon-26-tPwYYwxNqdGQp2nW.htm)|Hache de seigneurs nains|`@Compendium[pf2e.equipment-srd.tPwYYwxNqdGQp2nW]`|
|[Axiomatic](equipment/equipment-11-6xu6dPIaUZ7edKEB.htm)|Axiomatique|`@Compendium[pf2e.equipment-srd.6xu6dPIaUZ7edKEB]`|
|[Azure Worm Repellent](equipment/consumable-15-R4UPjbSDwDEmDJBA.htm)||`@Compendium[pf2e.equipment-srd.R4UPjbSDwDEmDJBA]`|
|[Azurite](equipment/treasure-RNnVb83lRRUXg8hG.htm)|Azurite|`@Compendium[pf2e.equipment-srd.RNnVb83lRRUXg8hG]`|
|[Backpack](equipment/backpack-00-3lgwjrFEsQVKzhh7.htm)|Sac à dos|`@Compendium[pf2e.equipment-srd.3lgwjrFEsQVKzhh7]`|
|[Bag of Devouring Type I](equipment/backpack-07-MN8SF2sArQhJg6QG.htm)|Sac dévoreur (Type I)|`@Compendium[pf2e.equipment-srd.MN8SF2sArQhJg6QG]`|
|[Bag of Devouring Type II](equipment/backpack-11-EIBmADRICTyYzxik.htm)|Sac dévoreur (Type II)|`@Compendium[pf2e.equipment-srd.EIBmADRICTyYzxik]`|
|[Bag of Devouring Type III](equipment/backpack-13-n3kJYoTrzXYwlYaV.htm)|sac dévoreur (Type III)|`@Compendium[pf2e.equipment-srd.n3kJYoTrzXYwlYaV]`|
|[Bag of Holding (Type I)](equipment/backpack-04-jaEEvuQ32GjAa8jy.htm)|Sac sans fond (type1)|`@Compendium[pf2e.equipment-srd.jaEEvuQ32GjAa8jy]`|
|[Bag of Holding (Type II)](equipment/backpack-07-JBMBaN9dZLytfFLQ.htm)|Sac sans fond (Type 2)|`@Compendium[pf2e.equipment-srd.JBMBaN9dZLytfFLQ]`|
|[Bag of Holding (Type III)](equipment/backpack-11-jkb2WNby4mjcYqq9.htm)|Sac sans fond (Type 3)|`@Compendium[pf2e.equipment-srd.jkb2WNby4mjcYqq9]`|
|[Bag of Holding (Type IV)](equipment/backpack-13-34DA4rFy7bduRAld.htm)|Sac sans fond (Type 4)|`@Compendium[pf2e.equipment-srd.34DA4rFy7bduRAld]`|
|[Bag of Weasels](equipment/backpack-04-W5znRDeklmWEGzFY.htm)|Sac de belettes|`@Compendium[pf2e.equipment-srd.W5znRDeklmWEGzFY]`|
|[Balisse Feather](equipment/consumable-12-Do8vjuUBOslgPtyw.htm)||`@Compendium[pf2e.equipment-srd.Do8vjuUBOslgPtyw]`|
|[Bandolier](equipment/backpack-00-HamOU17sqb5ljiB5.htm)|Bandoulière|`@Compendium[pf2e.equipment-srd.HamOU17sqb5ljiB5]`|
|[Barbed Vest](equipment/equipment-10-qZUo5SmGxp5BlqFN.htm)||`@Compendium[pf2e.equipment-srd.qZUo5SmGxp5BlqFN]`|
|[Barding of the Zephyr](equipment/equipment-10-eoI3M6FXtcPWeg7i.htm)|Barde du zéphir|`@Compendium[pf2e.equipment-srd.eoI3M6FXtcPWeg7i]`|
|[Barkskin Potion](equipment/consumable-04-zC7LipQPHRYw2RXx.htm)|Potion de peau d'écorce|`@Compendium[pf2e.equipment-srd.zC7LipQPHRYw2RXx]`|
|[Basic Crafter's Book](equipment/equipment-00-w4Hd6nunVVqw3GWj.htm)|Livre d’artisan basique|`@Compendium[pf2e.equipment-srd.w4Hd6nunVVqw3GWj]`|
|[Basilisk Eye](equipment/consumable-09-vL6AtFbcxbipGvtf.htm)||`@Compendium[pf2e.equipment-srd.vL6AtFbcxbipGvtf]`|
|[Bastard Sword](equipment/weapon-00-6KWYmeRMxsQfWhhJ.htm)|Épée bâtarde|`@Compendium[pf2e.equipment-srd.6KWYmeRMxsQfWhhJ]`|
|[Battle Axe](equipment/weapon-00-War0uyLBx1jA0Ge7.htm)|Hache d'armes|`@Compendium[pf2e.equipment-srd.War0uyLBx1jA0Ge7]`|
|[Beacon Shot](equipment/consumable-03-taAjenWKjBJpQyrE.htm)|Signal lumineux|`@Compendium[pf2e.equipment-srd.taAjenWKjBJpQyrE]`|
|[Beckoning Cat Amulet](equipment/consumable-05-HU9eYAAfZMYnFMd9.htm)||`@Compendium[pf2e.equipment-srd.HU9eYAAfZMYnFMd9]`|
|[Bedroll](equipment/equipment-00-fagzYdmfYyMQ6J77.htm)|Sac de couchage|`@Compendium[pf2e.equipment-srd.fagzYdmfYyMQ6J77]`|
|[Belladonna](equipment/consumable-02-FL8QU8TcNauBMMhD.htm)|Belladone|`@Compendium[pf2e.equipment-srd.FL8QU8TcNauBMMhD]`|
|[Bellflower Toolbelt](equipment/equipment-06-ifsBwfIhd6UcoUCI.htm)|Ceinture à outil Campanule|`@Compendium[pf2e.equipment-srd.ifsBwfIhd6UcoUCI]`|
|[Belt Pouch](equipment/backpack-00-eFqKVKrf62XOGWUw.htm)|Bourse|`@Compendium[pf2e.equipment-srd.eFqKVKrf62XOGWUw]`|
|[Belt of Giant Strength](equipment/equipment-17-7utuH8VJjKEzKtNw.htm)|Ceinture de force de géant|`@Compendium[pf2e.equipment-srd.7utuH8VJjKEzKtNw]`|
|[Belt of Good Health](equipment/equipment-04-AhvjU4QbinWPM9t3.htm)||`@Compendium[pf2e.equipment-srd.AhvjU4QbinWPM9t3]`|
|[Belt of Regeneration](equipment/equipment-17-K2rMmiBlzcysNuj6.htm)|Ceinture de régénération|`@Compendium[pf2e.equipment-srd.K2rMmiBlzcysNuj6]`|
|[Belt of the Five Kings](equipment/equipment-09-YCjVrQnHfOtpmjYW.htm)|Ceinture des cinq rois|`@Compendium[pf2e.equipment-srd.YCjVrQnHfOtpmjYW]`|
|[Berserker's Cloak](equipment/equipment-12-DwLaGtbBdCh2NFbT.htm)|Cape du Berseker|`@Compendium[pf2e.equipment-srd.DwLaGtbBdCh2NFbT]`|
|[Berserker's Cloak (Greater)](equipment/equipment-19-OGKI8NS8Er3qumJS.htm)|Cape du berseker (supérieure)|`@Compendium[pf2e.equipment-srd.OGKI8NS8Er3qumJS]`|
|[Bestial Mutagen (Greater)](equipment/consumable-11-gDheph8YteBtnyKp.htm)|Mutagène bestial (supérieur)|`@Compendium[pf2e.equipment-srd.gDheph8YteBtnyKp]`|
|[Bestial Mutagen (Lesser)](equipment/consumable-01-IQK9N2mEOyAj3iWU.htm)|Mutagène bestial (inférieur)|`@Compendium[pf2e.equipment-srd.IQK9N2mEOyAj3iWU]`|
|[Bestial Mutagen (Major)](equipment/consumable-17-nS75vsM3x5jxlUqn.htm)|Mutagène bestial (Majeur)|`@Compendium[pf2e.equipment-srd.nS75vsM3x5jxlUqn]`|
|[Bestial Mutagen (Moderate)](equipment/consumable-03-VISk5uLPVIvNWovB.htm)|Mutagène bestiale (modéré)|`@Compendium[pf2e.equipment-srd.VISk5uLPVIvNWovB]`|
|[Bi-Resonant Wayfinder](equipment/equipment-06-LDVqBvMNNcRBucW1.htm)|Guide à double résonance|`@Compendium[pf2e.equipment-srd.LDVqBvMNNcRBucW1]`|
|[Biting Snare](equipment/consumable-04-9EZb1hmSKOGrU4Cf.htm)|Piège artisanal à mâchoires|`@Compendium[pf2e.equipment-srd.9EZb1hmSKOGrU4Cf]`|
|[Black Adder Venom](equipment/consumable-02-ScclzFrjyB0YJlrb.htm)|Venin de vipère noire|`@Compendium[pf2e.equipment-srd.ScclzFrjyB0YJlrb]`|
|[Black Dragon's Breath Potion (Adult)](equipment/consumable-12-x04huvOBBXmExTDl.htm)|Potion de souffle du dragon noir adulte|`@Compendium[pf2e.equipment-srd.x04huvOBBXmExTDl]`|
|[Black Dragon's Breath Potion (Wyrm)](equipment/consumable-17-LR7Ke2V8vHjQYBni.htm)|Potion de souffle de dracosire noir|`@Compendium[pf2e.equipment-srd.LR7Ke2V8vHjQYBni]`|
|[Black Dragon's Breath Potion (Young)](equipment/consumable-07-qNIv0owESzOzo1s0.htm)|Potion de souffle de jeune dragon noir|`@Compendium[pf2e.equipment-srd.qNIv0owESzOzo1s0]`|
|[Black Lotus Extract](equipment/consumable-19-xQS1MSqGQz44FWUh.htm)|Extrait de lotus noir|`@Compendium[pf2e.equipment-srd.xQS1MSqGQz44FWUh]`|
|[Black Smear Poison](equipment/consumable-02-cTBmQgWUyf50x3dY.htm)||`@Compendium[pf2e.equipment-srd.cTBmQgWUyf50x3dY]`|
|[Blackaxe](equipment/weapon-25-o9IErbpmItz9NZT3.htm)||`@Compendium[pf2e.equipment-srd.o9IErbpmItz9NZT3]`|
|[Blackfinger Blight](equipment/consumable-12-WvDbdESFJY6Be2u1.htm)||`@Compendium[pf2e.equipment-srd.WvDbdESFJY6Be2u1]`|
|[Blade of the Black Soverign](equipment/weapon-15-tD4ZNDutHoCGIFQn.htm)||`@Compendium[pf2e.equipment-srd.tD4ZNDutHoCGIFQn]`|
|[Blade of the Rabbit Prince](equipment/weapon-15-APZJC0A469gBozf1.htm)||`@Compendium[pf2e.equipment-srd.APZJC0A469gBozf1]`|
|[Bladed Diabolo](equipment/weapon-00-0lCXehyFlXdYxDfA.htm)|Diabolo à lames|`@Compendium[pf2e.equipment-srd.0lCXehyFlXdYxDfA]`|
|[Bladed Hoop](equipment/weapon-00-IEA7M6GGv4rnGLdW.htm)|Cerceau à lames|`@Compendium[pf2e.equipment-srd.IEA7M6GGv4rnGLdW]`|
|[Bladed Scarf](equipment/weapon-00-iCdvNfzvsTZ0s5vg.htm)|Écharpe à lame|`@Compendium[pf2e.equipment-srd.iCdvNfzvsTZ0s5vg]`|
|[Bleeding Spines Snare](equipment/consumable-12-9Kk6P7idLGRhZJ2q.htm)|Piège artisanal à pointes barbelées|`@Compendium[pf2e.equipment-srd.9Kk6P7idLGRhZJ2q]`|
|[Blessed Tattoo](equipment/equipment-04-y0ngvQ7ArcUuoEHT.htm)|Tatouage de bénédiction|`@Compendium[pf2e.equipment-srd.y0ngvQ7ArcUuoEHT]`|
|[Blight Bomb (Greater)](equipment/weapon-11-LhMXIGelSrFPYild.htm)|Bombe fléau (supérieure)|`@Compendium[pf2e.equipment-srd.LhMXIGelSrFPYild]`|
|[Blight Bomb (Lesser)](equipment/weapon-01-j8ajvNqyyQGBpBch.htm)|Bombe fléau (inférieure)|`@Compendium[pf2e.equipment-srd.j8ajvNqyyQGBpBch]`|
|[Blight Bomb (Major)](equipment/weapon-17-sJjuv1991SZ7DWWD.htm)|Bombe fléau (majeure)|`@Compendium[pf2e.equipment-srd.sJjuv1991SZ7DWWD]`|
|[Blight Bomb (Moderate)](equipment/weapon-03-158vwM1andv8DbRI.htm)|Bombe fléau (modérée)|`@Compendium[pf2e.equipment-srd.158vwM1andv8DbRI]`|
|[Blightburn Resin](equipment/consumable-11-Cc4SokzVBoBCkHId.htm)|Résine de champibrûle|`@Compendium[pf2e.equipment-srd.Cc4SokzVBoBCkHId]`|
|[Blightburn Ward](equipment/equipment-13-keFeNqSR7W35aCeT.htm)|Protège fléau brûlant|`@Compendium[pf2e.equipment-srd.keFeNqSR7W35aCeT]`|
|[Blindpepper Bomb](equipment/consumable-05-E7BcwZy8nTpTLYf1.htm)||`@Compendium[pf2e.equipment-srd.E7BcwZy8nTpTLYf1]`|
|[Blindpepper Tube](equipment/consumable-01-81XVuTsF1zD6EXmN.htm)||`@Compendium[pf2e.equipment-srd.81XVuTsF1zD6EXmN]`|
|[Blood Sap](equipment/consumable-01-PNwgGnN081l0cETR.htm)|Sève sanglante|`@Compendium[pf2e.equipment-srd.PNwgGnN081l0cETR]`|
|[Bloodbane](equipment/equipment-08-C9wOlvuVCjVbz1YQ.htm)||`@Compendium[pf2e.equipment-srd.C9wOlvuVCjVbz1YQ]`|
|[Bloodbane (Greater)](equipment/equipment-13-zEys8FeMMAwTqwgW.htm)||`@Compendium[pf2e.equipment-srd.zEys8FeMMAwTqwgW]`|
|[Bloodeye Coffee](equipment/consumable-00-2DoUHoueAyyP4lMN.htm)|Café œil-sanglant|`@Compendium[pf2e.equipment-srd.2DoUHoueAyyP4lMN]`|
|[Bloodhound Mask (Greater)](equipment/consumable-11-2x8F5s3PyKzYsRrZ.htm)||`@Compendium[pf2e.equipment-srd.2x8F5s3PyKzYsRrZ]`|
|[Bloodhound Mask (Lesser)](equipment/consumable-02-mDJSaarQsIMX7Opi.htm)|Masque du limier (Inférieur)|`@Compendium[pf2e.equipment-srd.mDJSaarQsIMX7Opi]`|
|[Bloodhound Mask (Moderate)](equipment/consumable-06-qasFfdiZKIczoZ9p.htm)||`@Compendium[pf2e.equipment-srd.qasFfdiZKIczoZ9p]`|
|[Bloodletting Kukri](equipment/weapon-06-tNue4PqJe85ZEE5v.htm)|Kukri de la saignée|`@Compendium[pf2e.equipment-srd.tNue4PqJe85ZEE5v]`|
|[Bloodseeker Beak](equipment/consumable-04-k6D64EAjcKMf8NZB.htm)|Trompe de cherchesang|`@Compendium[pf2e.equipment-srd.k6D64EAjcKMf8NZB]`|
|[Bloodstone](equipment/treasure-D8gzHTvP0uFxjwyA.htm)|Héliotrope|`@Compendium[pf2e.equipment-srd.D8gzHTvP0uFxjwyA]`|
|[Blowgun](equipment/weapon-00-FPwsiGqMCNPLHmjX.htm)|sarbacane|`@Compendium[pf2e.equipment-srd.FPwsiGqMCNPLHmjX]`|
|[Blowgun Darts](equipment/consumable-00-sqhr1crb184s3Vnd.htm)|Fléchettes de sabacane|`@Compendium[pf2e.equipment-srd.sqhr1crb184s3Vnd]`|
|[Blue Dragon's Breath Potion (Adult)](equipment/consumable-12-zQnidYCmLCBPfdii.htm)|Potion de souffle du dragon bleu adulte|`@Compendium[pf2e.equipment-srd.zQnidYCmLCBPfdii]`|
|[Blue Dragon's Breath Potion (Wyrm)](equipment/consumable-17-ltILKAEnrZ8vKwY0.htm)|Potion de souffle de dracosire bleu|`@Compendium[pf2e.equipment-srd.ltILKAEnrZ8vKwY0]`|
|[Blue Dragon's Breath Potion (Young)](equipment/consumable-07-xpVHMxLcwgXzL39e.htm)|Potion de souffle d'un jeune dragon bleu|`@Compendium[pf2e.equipment-srd.xpVHMxLcwgXzL39e]`|
|[Blue Dragonfly Poison](equipment/consumable-03-rfyWXgrVD2lm91CC.htm)||`@Compendium[pf2e.equipment-srd.rfyWXgrVD2lm91CC]`|
|[Blueblisters](equipment/consumable-03-3iRTGS2vYXz7SBrn.htm)||`@Compendium[pf2e.equipment-srd.3iRTGS2vYXz7SBrn]`|
|[Bo Staff](equipment/weapon-00-hMYdSFmMWzidzHih.htm)|Bô|`@Compendium[pf2e.equipment-srd.hMYdSFmMWzidzHih]`|
|[Boarding Pike](equipment/weapon-01-sF9uOLAjQiSmt9i3.htm)|Pique d'abordage|`@Compendium[pf2e.equipment-srd.sF9uOLAjQiSmt9i3]`|
|[Body Recovery Kit](equipment/consumable-09-rzRI8taFlNY8lbkl.htm)||`@Compendium[pf2e.equipment-srd.rzRI8taFlNY8lbkl]`|
|[Bola](equipment/weapon-00-YUzPv0i8d8p2J9yx.htm)|Bolas|`@Compendium[pf2e.equipment-srd.YUzPv0i8d8p2J9yx]`|
|[Bolts](equipment/consumable-00-AITVZmakiu3RgfKo.htm)|Carreau(x)|`@Compendium[pf2e.equipment-srd.AITVZmakiu3RgfKo]`|
|[Bomb Snare](equipment/consumable-08-axU0I9xIm4xm2VPH.htm)||`@Compendium[pf2e.equipment-srd.axU0I9xIm4xm2VPH]`|
|[Bomber's Eye Elixir (Greater)](equipment/consumable-14-lasqzrPVEHI5MkQd.htm)|Elixir du mitrailleur (supérieur)|`@Compendium[pf2e.equipment-srd.lasqzrPVEHI5MkQd]`|
|[Bomber's Eye Elixir (Lesser)](equipment/consumable-04-T4ouD4mVFHA3EHs6.htm)|Elixir d'oeil du mitrailleur (inférieure)|`@Compendium[pf2e.equipment-srd.T4ouD4mVFHA3EHs6]`|
|[Bomber's Saddle](equipment/backpack-13-k5y89wYu3NX1AhVI.htm)||`@Compendium[pf2e.equipment-srd.k5y89wYu3NX1AhVI]`|
|[Bookthief Brew](equipment/consumable-01-PLun5Enmp8ZbjogV.htm)||`@Compendium[pf2e.equipment-srd.PLun5Enmp8ZbjogV]`|
|[Boots of Bounding](equipment/equipment-07-ecqz1iUGtyQEkZwy.htm)|Bottes de saut|`@Compendium[pf2e.equipment-srd.ecqz1iUGtyQEkZwy]`|
|[Boots of Bounding (Greater)](equipment/equipment-14-31knfVD7lEd8BPrQ.htm)||`@Compendium[pf2e.equipment-srd.31knfVD7lEd8BPrQ]`|
|[Boots of Dancing](equipment/equipment-11-oxL8uExmIomunm5g.htm)|Bottes de dance éffrénée|`@Compendium[pf2e.equipment-srd.oxL8uExmIomunm5g]`|
|[Boots of Elvenkind](equipment/equipment-05-T00Xa9aDwHxd60Zh.htm)|Bottes elfiques|`@Compendium[pf2e.equipment-srd.T00Xa9aDwHxd60Zh]`|
|[Boots of Elvenkind (Greater)](equipment/equipment-11-8NgA4PFFXZPZ9SSg.htm)|Bottes elfiques (supérieures)|`@Compendium[pf2e.equipment-srd.8NgA4PFFXZPZ9SSg]`|
|[Boots of Speed](equipment/equipment-13-Ck5k13uTNqibLFJk.htm)|Bottes de rapidité|`@Compendium[pf2e.equipment-srd.Ck5k13uTNqibLFJk]`|
|[Bort's Blessing](equipment/equipment-05-iS7hAQMAaThHYE8g.htm)||`@Compendium[pf2e.equipment-srd.iS7hAQMAaThHYE8g]`|
|[Bottled Air](equipment/equipment-07-7TQw7V1zZKl0a0Xz.htm)|Air en bouteille|`@Compendium[pf2e.equipment-srd.7TQw7V1zZKl0a0Xz]`|
|[Bottled Lightning (Greater)](equipment/weapon-11-r2iTRbt1zpkAqHj2.htm)|Foudre en bouteille (supérieur)|`@Compendium[pf2e.equipment-srd.r2iTRbt1zpkAqHj2]`|
|[Bottled Lightning (Lesser)](equipment/weapon-01-AFR01HVd7DcZvkpP.htm)|Foudre en bouteille (inférieur)|`@Compendium[pf2e.equipment-srd.AFR01HVd7DcZvkpP]`|
|[Bottled Lightning (Major)](equipment/weapon-17-rpbbfkexLhtadBDV.htm)|Foudre en bouteille (majeur)|`@Compendium[pf2e.equipment-srd.rpbbfkexLhtadBDV]`|
|[Bottled Lightning (Moderate)](equipment/weapon-03-97QyNEOAyYLdGaYc.htm)|Foudre en bouteille (modéré)|`@Compendium[pf2e.equipment-srd.97QyNEOAyYLdGaYc]`|
|[Bottomless Stein](equipment/equipment-02-BJxnnY9ap2wMDnRN.htm)||`@Compendium[pf2e.equipment-srd.BJxnnY9ap2wMDnRN]`|
|[Boulderhead Bock](equipment/consumable-02-I0eUrgVGl8a2ZjFc.htm)|Bock têtederoc|`@Compendium[pf2e.equipment-srd.I0eUrgVGl8a2ZjFc]`|
|[Bountiful Cauldron](equipment/equipment-09-MBF9bZCwkJp2RZoN.htm)|Chaudron d'abondance|`@Compendium[pf2e.equipment-srd.MBF9bZCwkJp2RZoN]`|
|[Bracelets of Dashing](equipment/equipment-03-BKdzb8hu3kZtKH3Z.htm)|Bracelet de fougue|`@Compendium[pf2e.equipment-srd.BKdzb8hu3kZtKH3Z]`|
|[Bracers of Armor I](equipment/equipment-08-02q8s6sSicMkhs1l.htm)|Protège-bras d'armure I|`@Compendium[pf2e.equipment-srd.02q8s6sSicMkhs1l]`|
|[Bracers of Armor II](equipment/equipment-14-8mhSUxEvNuXDP8Ki.htm)|Protège-bras d'armure II|`@Compendium[pf2e.equipment-srd.8mhSUxEvNuXDP8Ki]`|
|[Bracers of Armor III](equipment/equipment-20-rqJzQawe3CbXiWnG.htm)|Protège-bras d'armure III|`@Compendium[pf2e.equipment-srd.rqJzQawe3CbXiWnG]`|
|[Bracers of Missile Deflection](equipment/equipment-03-eurAnvH8bK0ZctOR.htm)|Protège-bras de déviation de projectiles|`@Compendium[pf2e.equipment-srd.eurAnvH8bK0ZctOR]`|
|[Bracers of Missile Deflection (Greater)](equipment/equipment-09-P5nasaE0JgvkZyZp.htm)|Protège-bras de déviation de projectiles (supérieurs)|`@Compendium[pf2e.equipment-srd.P5nasaE0JgvkZyZp]`|
|[Brass Dragon's Breath Potion (Adult)](equipment/consumable-12-rOQlcAV4m8Zaebue.htm)|Potion de souffle du dragon d'airain adulte|`@Compendium[pf2e.equipment-srd.rOQlcAV4m8Zaebue]`|
|[Brass Dragon's Breath Potion (Wyrm)](equipment/consumable-17-idtIMGkFmIe958Et.htm)|Potion de souffle de dracosire d'airain|`@Compendium[pf2e.equipment-srd.idtIMGkFmIe958Et]`|
|[Brass Dragon's Breath Potion (Young)](equipment/consumable-07-TqX34x9CijryfrlM.htm)|Potion de souffle d'un jeune dragon d'airain|`@Compendium[pf2e.equipment-srd.TqX34x9CijryfrlM]`|
|[Brass Ear](equipment/equipment-00-M91ye9B7IdxDIDsS.htm)|Oreille en laiton|`@Compendium[pf2e.equipment-srd.M91ye9B7IdxDIDsS]`|
|[Brass anklet](equipment/treasure-00-uAQKNkgub7KNOHoR.htm)|Chaine de cheville en laiton|`@Compendium[pf2e.equipment-srd.uAQKNkgub7KNOHoR]`|
|[Brass scepter with amethyst head](equipment/treasure-00-l6Eifu4DScg0OBxb.htm)|Sceptre de laiton à pommeau d'améthyste|`@Compendium[pf2e.equipment-srd.l6Eifu4DScg0OBxb]`|
|[Brass statuette of a bull](equipment/treasure-00-rOMPvaCeLqNvT6rJ.htm)|Statuette de taureau en laiton|`@Compendium[pf2e.equipment-srd.rOMPvaCeLqNvT6rJ]`|
|[Bravo's Brew (Greater)](equipment/consumable-15-sWPkbSlWAEAStBqD.htm)|Breuvage de bravoure (supérieure)|`@Compendium[pf2e.equipment-srd.sWPkbSlWAEAStBqD]`|
|[Bravo's Brew (Lesser)](equipment/consumable-02-zM9VX3QwM81DzDUA.htm)|Breuvage de bravoure (Inférieur)|`@Compendium[pf2e.equipment-srd.zM9VX3QwM81DzDUA]`|
|[Bravo's Brew (Moderate)](equipment/consumable-10-oCuwJ9IUDAuzsUwa.htm)|Breuvage de bravoure (modéré)|`@Compendium[pf2e.equipment-srd.oCuwJ9IUDAuzsUwa]`|
|[Breastplate](equipment/armor-00-r0ifJfoz8aqf0mwk.htm)|Cuirasse|`@Compendium[pf2e.equipment-srd.r0ifJfoz8aqf0mwk]`|
|[Breastplate of Command](equipment/armor-10-RUEAV5LMUGFHcXcW.htm)|Cuirasse de commandement|`@Compendium[pf2e.equipment-srd.RUEAV5LMUGFHcXcW]`|
|[Breastplate of Command (Greater)](equipment/armor-18-XKON66YXYLXlGPPg.htm)|Cuirasse de commandement (supérieur)|`@Compendium[pf2e.equipment-srd.XKON66YXYLXlGPPg]`|
|[Brimstone Fumes](equipment/consumable-16-9nhvZ7VnDQHuyBdf.htm)|Fumées sulfureuses|`@Compendium[pf2e.equipment-srd.9nhvZ7VnDQHuyBdf]`|
|[Bronze Bull Pendant](equipment/consumable-02-nXStoLxPrrP2b6WB.htm)|Pendentif de taureau de bronze|`@Compendium[pf2e.equipment-srd.nXStoLxPrrP2b6WB]`|
|[Bronze Dragon's Breath Potion (Adult)](equipment/consumable-12-vXLZfi5iCSKb96tW.htm)|Potion de souffle du dragon de bronze adulte|`@Compendium[pf2e.equipment-srd.vXLZfi5iCSKb96tW]`|
|[Bronze Dragon's Breath Potion (Wyrm)](equipment/consumable-17-lJTl2SuUaluMzPBl.htm)|Potion de souffle de dracosire de bronze|`@Compendium[pf2e.equipment-srd.lJTl2SuUaluMzPBl]`|
|[Bronze Dragon's Breath Potion (Young)](equipment/consumable-07-TzXNMv7tnFy1FYgg.htm)|Potion de souffle du jeune dragon de bronze|`@Compendium[pf2e.equipment-srd.TzXNMv7tnFy1FYgg]`|
|[Bronze bowl with wave imagery](equipment/treasure-00-I3KlOUruQ2CZdcJ6.htm)|Bol en bronze avec des motifs de vagues|`@Compendium[pf2e.equipment-srd.I3KlOUruQ2CZdcJ6]`|
|[Bronze brazier with Asmodean artwork](equipment/treasure-00-lvrvCGrmnMmE4TxU.htm)|Braséro en bronze de style asmodéen|`@Compendium[pf2e.equipment-srd.lvrvCGrmnMmE4TxU]`|
|[Bronze chalice with bloodstones](equipment/treasure-ja5rYjVRwwUXWtOd.htm)|Calice en bronze avec des héliotropes|`@Compendium[pf2e.equipment-srd.ja5rYjVRwwUXWtOd]`|
|[Brooch of Shielding](equipment/equipment-02-TmQalYKNNRuEdoTh.htm)|Broche de défense|`@Compendium[pf2e.equipment-srd.TmQalYKNNRuEdoTh]`|
|[Broom of Flying](equipment/equipment-12-1erd18HS57aCyC6r.htm)|Balais volant|`@Compendium[pf2e.equipment-srd.1erd18HS57aCyC6r]`|
|[Buckler](equipment/armor-00-1k3AsSW7lpU0kEpY.htm)|Targe|`@Compendium[pf2e.equipment-srd.1k3AsSW7lpU0kEpY]`|
|[Bullhook](equipment/equipment-03-2EOeljZiUdNVf8s2.htm)||`@Compendium[pf2e.equipment-srd.2EOeljZiUdNVf8s2]`|
|[Bullhook (Greater)](equipment/equipment-12-Yguqipt5N29Bkz0d.htm)||`@Compendium[pf2e.equipment-srd.Yguqipt5N29Bkz0d]`|
|[Buoyancy Vest](equipment/equipment-00-2fVNJ23gb3dixVu8.htm)||`@Compendium[pf2e.equipment-srd.2fVNJ23gb3dixVu8]`|
|[Caltrop Snare](equipment/consumable-01-kF761P3ibBIFmLm9.htm)|Piège artisanal de chausse-trappes|`@Compendium[pf2e.equipment-srd.kF761P3ibBIFmLm9]`|
|[Caltrops](equipment/equipment-00-7fSnvJ2xoSfa6JXD.htm)|Chausse-trappes|`@Compendium[pf2e.equipment-srd.7fSnvJ2xoSfa6JXD]`|
|[Candle](equipment/consumable-00-Ti4gWILk69LPxKuU.htm)|Bougie|`@Compendium[pf2e.equipment-srd.Ti4gWILk69LPxKuU]`|
|[Candle of Invocation](equipment/consumable-16-WAUq4TB4Q9FDxWFz.htm)||`@Compendium[pf2e.equipment-srd.WAUq4TB4Q9FDxWFz]`|
|[Candle of Revealing](equipment/consumable-07-XpmPX3ScEOBgAoKd.htm)||`@Compendium[pf2e.equipment-srd.XpmPX3ScEOBgAoKd]`|
|[Candle of Truth](equipment/consumable-08-61mFRFnaCLHDtvdv.htm)|Cierge de vérité|`@Compendium[pf2e.equipment-srd.61mFRFnaCLHDtvdv]`|
|[Cape of the Mountebank](equipment/equipment-10-uiJAR3jQbQHhiP3Q.htm)|Cape de prestidigateur|`@Compendium[pf2e.equipment-srd.uiJAR3jQbQHhiP3Q]`|
|[Capsaicin Tonic](equipment/consumable-04-ShFbUrFrQg7Ung8D.htm)||`@Compendium[pf2e.equipment-srd.ShFbUrFrQg7Ung8D]`|
|[Carnelian](equipment/treasure-00-P2MIJxEQiIWJmV8n.htm)|Cornaline|`@Compendium[pf2e.equipment-srd.P2MIJxEQiIWJmV8n]`|
|[Cartographer's Kit](equipment/kit-YQLWR9cCXQY5xaaG.htm)||`@Compendium[pf2e.equipment-srd.YQLWR9cCXQY5xaaG]`|
|[Carved wooden game set](equipment/treasure-00-xzmYwmvj61br2bRp.htm)|Pièces de jeu en bois sculpté|`@Compendium[pf2e.equipment-srd.xzmYwmvj61br2bRp]`|
|[Cassock of Devotion](equipment/equipment-11-62vH66XLehPiRwwo.htm)|Soutane de dévotion|`@Compendium[pf2e.equipment-srd.62vH66XLehPiRwwo]`|
|[Cat's Eye Elixir](equipment/consumable-02-bQPRKEpnLakJBAAh.htm)|Elixir d'oeil de chat|`@Compendium[pf2e.equipment-srd.bQPRKEpnLakJBAAh]`|
|[Caterwaul Sling](equipment/weapon-05-2KNAip9W6IoBrfIU.htm)|Fronde qui feule|`@Compendium[pf2e.equipment-srd.2KNAip9W6IoBrfIU]`|
|[Caustic Deteriorating Dust](equipment/consumable-09-GXH1PSioEkxTDceK.htm)||`@Compendium[pf2e.equipment-srd.GXH1PSioEkxTDceK]`|
|[Celestial Armor](equipment/armor-13-2uHcTZ40oZ62R9gy.htm)|Armure céleste|`@Compendium[pf2e.equipment-srd.2uHcTZ40oZ62R9gy]`|
|[Celestial Hair](equipment/consumable-20-FB7qAHjiiHTGhRnn.htm)||`@Compendium[pf2e.equipment-srd.FB7qAHjiiHTGhRnn]`|
|[Celestial Peach (Life)](equipment/consumable-20-ktAjhlI9NxpFJMdp.htm)||`@Compendium[pf2e.equipment-srd.ktAjhlI9NxpFJMdp]`|
|[Celestial Peach (Rejuvination)](equipment/consumable-17-KsoofB7FCppaBTdK.htm)||`@Compendium[pf2e.equipment-srd.KsoofB7FCppaBTdK]`|
|[Ceremonial dagger with onyx hilt](equipment/treasure-00-VHJM3jYtpzMqZ1wc.htm)|Dague cérémonielle avec un pommeau en onyx|`@Compendium[pf2e.equipment-srd.VHJM3jYtpzMqZ1wc]`|
|[Ceremonial shortsword with spinels](equipment/treasure-F4YgvB74p8kwif1t.htm)|épée courte cérémonielle avec des spinelles|`@Compendium[pf2e.equipment-srd.F4YgvB74p8kwif1t]`|
|[Cerulean Scourge](equipment/consumable-16-9hdT05ywPVyh9vQX.htm)||`@Compendium[pf2e.equipment-srd.9hdT05ywPVyh9vQX]`|
|[Chain (10 feet)](equipment/equipment-00-V78itAtTHI4ugy0P.htm)|Chaine (3 m)|`@Compendium[pf2e.equipment-srd.V78itAtTHI4ugy0P]`|
|[Chain Mail](equipment/armor-00-Kf4eJEXnFPuAsseP.htm)|Cotte de mailles|`@Compendium[pf2e.equipment-srd.Kf4eJEXnFPuAsseP]`|
|[Chain Shirt](equipment/armor-00-MPcM4Wt6KmWE2kGL.htm)|Chemise de mailles|`@Compendium[pf2e.equipment-srd.MPcM4Wt6KmWE2kGL]`|
|[Chain of the Stilled Spirit](equipment/equipment-09-6MIBHKMJmD8uQVam.htm)||`@Compendium[pf2e.equipment-srd.6MIBHKMJmD8uQVam]`|
|[Chalk](equipment/equipment-00-xShIDyydOMkGvGNb.htm)|Craie|`@Compendium[pf2e.equipment-srd.xShIDyydOMkGvGNb]`|
|[Chandelier crafted from dreams](equipment/treasure-00-MK6nnMZNWHmChAXZ.htm)|Chandelier confectionné à partir de songes|`@Compendium[pf2e.equipment-srd.MK6nnMZNWHmChAXZ]`|
|[Channel Protection Amulet](equipment/equipment-03-3vxoffA4slKHXtj2.htm)|Amulette de protection canalisée|`@Compendium[pf2e.equipment-srd.3vxoffA4slKHXtj2]`|
|[Cheetah's Elixir (Greater)](equipment/consumable-09-t4X6GDybqLmt7UkN.htm)|Elixir du guépard (supérieur)|`@Compendium[pf2e.equipment-srd.t4X6GDybqLmt7UkN]`|
|[Cheetah's Elixir (Lesser)](equipment/consumable-01-tyt6rFtv32MZ4DT9.htm)|Elixir du guépard (Inférieur)|`@Compendium[pf2e.equipment-srd.tyt6rFtv32MZ4DT9]`|
|[Cheetah's Elixir (Moderate)](equipment/consumable-05-AqLxu3ir4UGzdOaz.htm)|Elixir du gupéard (modéré)|`@Compendium[pf2e.equipment-srd.AqLxu3ir4UGzdOaz]`|
|[Chest](equipment/backpack-00-OdVDPN9vIpu3Zud3.htm)|Coffre|`@Compendium[pf2e.equipment-srd.OdVDPN9vIpu3Zud3]`|
|[Chime of Opening](equipment/equipment-06-3sGpEBXsZwjGnoES.htm)|Carillon d'ouverture|`@Compendium[pf2e.equipment-srd.3sGpEBXsZwjGnoES]`|
|[Choker of Elocution](equipment/equipment-06-TacKaUs8cIddqiCU.htm)|Collier d'élocution|`@Compendium[pf2e.equipment-srd.TacKaUs8cIddqiCU]`|
|[Choker of Elocution (Greater)](equipment/equipment-10-br4eClrcGeLTL6Ba.htm)|Collier d'élocution (supérieur)|`@Compendium[pf2e.equipment-srd.br4eClrcGeLTL6Ba]`|
|[Chronicler Wayfinder](equipment/equipment-06-KlkPJgiCiHpuyNpv.htm)||`@Compendium[pf2e.equipment-srd.KlkPJgiCiHpuyNpv]`|
|[Chrysoberyl](equipment/treasure-00-uMxXncL2bQI04GZ9.htm)|Chrysoberyl|`@Compendium[pf2e.equipment-srd.uMxXncL2bQI04GZ9]`|
|[Chrysoberyl symbol of an evil eye](equipment/treasure-00-qgKOCdtdybxwmiEv.htm)|Symbole de mauvais œil en chrysobéryl|`@Compendium[pf2e.equipment-srd.qgKOCdtdybxwmiEv]`|
|[Chrysoprase](equipment/treasure-00-FW1AEh214U1mMWh7.htm)|Chrysoprase|`@Compendium[pf2e.equipment-srd.FW1AEh214U1mMWh7]`|
|[Cinderclaw Gauntlet](equipment/weapon-05-Dti9PZZoty6We8OV.htm)|Gantelet de griffe de braise|`@Compendium[pf2e.equipment-srd.Dti9PZZoty6We8OV]`|
|[Circlet of Persuasion](equipment/equipment-17-0yeM77XLNrB0a0LF.htm)|Serre-tête de persuasion|`@Compendium[pf2e.equipment-srd.0yeM77XLNrB0a0LF]`|
|[Citrine](equipment/treasure-00-WEnrPya5A8CKi91A.htm)|Citrine|`@Compendium[pf2e.equipment-srd.WEnrPya5A8CKi91A]`|
|[Clan Dagger](equipment/weapon-00-kJJvKm80KwWXPukV.htm)|Dague de clan|`@Compendium[pf2e.equipment-srd.kJJvKm80KwWXPukV]`|
|[Clandestine Cloak](equipment/equipment-06-hmmDa6LCS22dZT7P.htm)|Cape de clandestinité|`@Compendium[pf2e.equipment-srd.hmmDa6LCS22dZT7P]`|
|[Clandestine Cloak (Greater)](equipment/equipment-10-6jRXUXzYKIpm2uNp.htm)|Cape de clandestinité (supérieure)|`@Compendium[pf2e.equipment-srd.6jRXUXzYKIpm2uNp]`|
|[Class Kit (Alchemist)](equipment/kit-KVXcrw446rzYRdpy.htm)|Kit de classe (Alchimiste)|`@Compendium[pf2e.equipment-srd.KVXcrw446rzYRdpy]`|
|[Class Kit (Barbarian)](equipment/kit-adNgRqqmkDDKhMRT.htm)|Kit de classe (Barbare)|`@Compendium[pf2e.equipment-srd.adNgRqqmkDDKhMRT]`|
|[Class Kit (Bard)](equipment/kit-asQAvJXm3DSzPQtA.htm)|Kit de classe (Barde)|`@Compendium[pf2e.equipment-srd.asQAvJXm3DSzPQtA]`|
|[Class Kit (Champion)](equipment/kit-gB4kCTgR3b6SJw7n.htm)|Kit de classe (Champion)|`@Compendium[pf2e.equipment-srd.gB4kCTgR3b6SJw7n]`|
|[Class Kit (Cleric)](equipment/kit-iKnFPR1X8mOAeVCV.htm)|Kit de classe (Prêtre)|`@Compendium[pf2e.equipment-srd.iKnFPR1X8mOAeVCV]`|
|[Class Kit (Druid)](equipment/kit-gCvGa77UZjhsbuOJ.htm)|Kit de classe (Druide)|`@Compendium[pf2e.equipment-srd.gCvGa77UZjhsbuOJ]`|
|[Class Kit (Fighter)](equipment/kit-cozhl69heIF0vjUt.htm)|Kit de classe (Guerrier)|`@Compendium[pf2e.equipment-srd.cozhl69heIF0vjUt]`|
|[Class Kit (Investigator)](equipment/kit-zcNBG8cMnILqXMFd.htm)||`@Compendium[pf2e.equipment-srd.zcNBG8cMnILqXMFd]`|
|[Class Kit (Monk)](equipment/kit-drY7DJ9rMQEP0et8.htm)|Kit de classe (moine)|`@Compendium[pf2e.equipment-srd.drY7DJ9rMQEP0et8]`|
|[Class Kit (Oracle)](equipment/kit-Ac0BSiDJhtMmPnge.htm)|Kit de classe (oracle)|`@Compendium[pf2e.equipment-srd.Ac0BSiDJhtMmPnge]`|
|[Class Kit (Ranger)](equipment/kit-7E3sWotW56biNttA.htm)|Kit de classe (Rôdeur)|`@Compendium[pf2e.equipment-srd.7E3sWotW56biNttA]`|
|[Class Kit (Rogue)](equipment/kit-nPGrNslTdc6VBuCB.htm)|Kit de classe (Roublard)|`@Compendium[pf2e.equipment-srd.nPGrNslTdc6VBuCB]`|
|[Class Kit (Sorcerer)](equipment/kit-bJKgKrIQ5puuifcz.htm)|Kit de classe (Ensorceleur)|`@Compendium[pf2e.equipment-srd.bJKgKrIQ5puuifcz]`|
|[Class Kit (Swashbuckler)](equipment/kit-9RxYSGp86HKtK8R8.htm)||`@Compendium[pf2e.equipment-srd.9RxYSGp86HKtK8R8]`|
|[Class Kit (Witch)](equipment/kit-5WT7gdzHNb5BG1J0.htm)|Kit de classe (sorcière)|`@Compendium[pf2e.equipment-srd.5WT7gdzHNb5BG1J0]`|
|[Class Kit (Wizard)](equipment/kit-tHWnYpDZlscldOX0.htm)|Kit de classe (Magicien)|`@Compendium[pf2e.equipment-srd.tHWnYpDZlscldOX0]`|
|[Claw Blade](equipment/weapon-00-fvvfZxfGV9i3urkd.htm)|Lames griffues|`@Compendium[pf2e.equipment-srd.fvvfZxfGV9i3urkd]`|
|[Climbing Bolt](equipment/consumable-04-n5L7HE9H8jn8ftQy.htm)|Carreau d'escalade|`@Compendium[pf2e.equipment-srd.n5L7HE9H8jn8ftQy]`|
|[Climbing Kit](equipment/equipment-00-9UJbMaglf35GVzaZ.htm)|Kit d'escalade|`@Compendium[pf2e.equipment-srd.9UJbMaglf35GVzaZ]`|
|[Climbing Kit (Extreme)](equipment/equipment-03-TWv2EeDJaCjtmGUN.htm)||`@Compendium[pf2e.equipment-srd.TWv2EeDJaCjtmGUN]`|
|[Cloak of Elvenkind](equipment/equipment-07-MNBnZn0b80Q7yHJM.htm)|Cape elfique|`@Compendium[pf2e.equipment-srd.MNBnZn0b80Q7yHJM]`|
|[Cloak of Elvenkind (Greater)](equipment/equipment-12-ZelCRLDI6M5IfjAI.htm)|Cape elfique (supérieure)|`@Compendium[pf2e.equipment-srd.ZelCRLDI6M5IfjAI]`|
|[Cloak of Immolation](equipment/equipment-07-nOI2irf1OifSdp9P.htm)|Cape d'immolation|`@Compendium[pf2e.equipment-srd.nOI2irf1OifSdp9P]`|
|[Cloak of Repute](equipment/equipment-04-oC4ZMEdBJ3ia4ALm.htm)||`@Compendium[pf2e.equipment-srd.oC4ZMEdBJ3ia4ALm]`|
|[Cloak of Repute (Greater)](equipment/equipment-09-xzN8mFG2Z70SaXLa.htm)||`@Compendium[pf2e.equipment-srd.xzN8mFG2Z70SaXLa]`|
|[Cloak of Repute (Major)](equipment/equipment-17-NFFgDfLBkCjkj0dc.htm)||`@Compendium[pf2e.equipment-srd.NFFgDfLBkCjkj0dc]`|
|[Cloak of the Bat](equipment/equipment-10-dipcMOtBFxtmsjkS.htm)|Cape de la chauve-souris|`@Compendium[pf2e.equipment-srd.dipcMOtBFxtmsjkS]`|
|[Cloak of the Bat (Greater)](equipment/equipment-17-ifAp8wHKBZltgHG0.htm)|Cape de la chauve souris (supérieure)|`@Compendium[pf2e.equipment-srd.ifAp8wHKBZltgHG0]`|
|[Clockwork Dial](equipment/equipment-00-937vrPcl9rpzdtJw.htm)||`@Compendium[pf2e.equipment-srd.937vrPcl9rpzdtJw]`|
|[Clockwork Recorder](equipment/equipment-06-NLC3c1mudubNNekY.htm)||`@Compendium[pf2e.equipment-srd.NLC3c1mudubNNekY]`|
|[Clothing (Explorer's)](equipment/armor-00-dDIPA1WE9ESF67EB.htm)|Vêtements (d'explorateur)|`@Compendium[pf2e.equipment-srd.dDIPA1WE9ESF67EB]`|
|[Clothing (Fine)](equipment/equipment-00-jJZbdMHMdh8UL2j8.htm)|Vêtements (beaux)|`@Compendium[pf2e.equipment-srd.jJZbdMHMdh8UL2j8]`|
|[Clothing (High-Fashion Fine)](equipment/equipment-03-rXZxM7SbqEnvXyal.htm)|Vêtements (beaux)|`@Compendium[pf2e.equipment-srd.rXZxM7SbqEnvXyal]`|
|[Clothing (Ordinary)](equipment/equipment-00-ilbLQNy6TbBPW7sQ.htm)|Vêtements (ordinaire)|`@Compendium[pf2e.equipment-srd.ilbLQNy6TbBPW7sQ]`|
|[Clothing (Winter)](equipment/equipment-00-SLQgdLk5K9JV4ACF.htm)|Vêtement (hiver)|`@Compendium[pf2e.equipment-srd.SLQgdLk5K9JV4ACF]`|
|[Club](equipment/weapon-00-c58wczIzH2gzeXQL.htm)|Gourdin|`@Compendium[pf2e.equipment-srd.c58wczIzH2gzeXQL]`|
|[Cognitive Mutagen (Greater)](equipment/consumable-11-ZGojRKG1yYiVWemR.htm)|Mutagène cognitif (supérieur)|`@Compendium[pf2e.equipment-srd.ZGojRKG1yYiVWemR]`|
|[Cognitive Mutagen (Lesser)](equipment/consumable-01-wbr6rkyaVYnDhdgV.htm)|Mutagène cognitif (Inférieur)|`@Compendium[pf2e.equipment-srd.wbr6rkyaVYnDhdgV]`|
|[Cognitive Mutagen (Major)](equipment/consumable-17-VBK9i74dry8yf8f0.htm)||`@Compendium[pf2e.equipment-srd.VBK9i74dry8yf8f0]`|
|[Cognitive Mutagen (Moderate)](equipment/consumable-03-qpzL9UnTi4cDhy6J.htm)|Mutagènhe cognitif (modéré)|`@Compendium[pf2e.equipment-srd.qpzL9UnTi4cDhy6J]`|
|[Cold Iron Blanch (Greater)](equipment/consumable-16-WXnGIl4d62detRlf.htm)||`@Compendium[pf2e.equipment-srd.WXnGIl4d62detRlf]`|
|[Cold Iron Blanch (Lesser)](equipment/consumable-03-cImNgwDCqwa6Dil1.htm)||`@Compendium[pf2e.equipment-srd.cImNgwDCqwa6Dil1]`|
|[Cold Iron Blanch (Moderate)](equipment/consumable-09-jnCP2zYJamUeZsJC.htm)||`@Compendium[pf2e.equipment-srd.jnCP2zYJamUeZsJC]`|
|[Cold Iron Buckler (High-Grade)](equipment/armor-15-kCjvkkuI3JP4TQ0t.htm)|targe en fer froid de haute qualité|`@Compendium[pf2e.equipment-srd.kCjvkkuI3JP4TQ0t]`|
|[Cold Iron Buckler (Low-Grade)](equipment/armor-02-ckUxr51wJVGRNAD0.htm)|Targe en fer froid de (basse qualité)|`@Compendium[pf2e.equipment-srd.ckUxr51wJVGRNAD0]`|
|[Cold Iron Buckler (Standard-Grade)](equipment/armor-07-PsyfqGIzDbr1mX6d.htm)|Targe en fer froid (qualité standard)|`@Compendium[pf2e.equipment-srd.PsyfqGIzDbr1mX6d]`|
|[Cold Iron Chunk](equipment/equipment-00-rEz9WYhlx2Pm0gKk.htm)|Morceau de fer froid|`@Compendium[pf2e.equipment-srd.rEz9WYhlx2Pm0gKk]`|
|[Cold Iron Ingot](equipment/equipment-00-eQjibJl41aQpQ47d.htm)|Lingot de fer froid|`@Compendium[pf2e.equipment-srd.eQjibJl41aQpQ47d]`|
|[Cold Iron Shield (High-Grade)](equipment/armor-15-nRmGErgTLZYf6WMD.htm)|bouclier en fer froid de haute qualité|`@Compendium[pf2e.equipment-srd.nRmGErgTLZYf6WMD]`|
|[Cold Iron Shield (Low-Grade)](equipment/armor-02-5E6l3RheSyl99G3m.htm)|Bouclier en fer froid (basse qualité)|`@Compendium[pf2e.equipment-srd.5E6l3RheSyl99G3m]`|
|[Cold Iron Shield (Standard-Grade)](equipment/armor-07-p1uYtIUYXXoNdVKg.htm)|Bouclier en fer froid (qualité standard)|`@Compendium[pf2e.equipment-srd.p1uYtIUYXXoNdVKg]`|
|[Collar of Empathy](equipment/equipment-09-cO7ANYLkcmfCn9c9.htm)|Collier d'empathie|`@Compendium[pf2e.equipment-srd.cO7ANYLkcmfCn9c9]`|
|[Collar of Inconspicuousness](equipment/equipment-08-Rsoh0Y3RQD8x8ito.htm)|Collier de modération|`@Compendium[pf2e.equipment-srd.Rsoh0Y3RQD8x8ito]`|
|[Colorful pastoral tapestry](equipment/treasure-00-noLtrW7wFfRFxSUt.htm)|Tapisserie pastorale colorée|`@Compendium[pf2e.equipment-srd.noLtrW7wFfRFxSUt]`|
|[Colorful velvet half mask](equipment/treasure-00-Ab8yDhtYDJnFrjI4.htm)|Loup de velours coloré (loup = demi-masque)|`@Compendium[pf2e.equipment-srd.Ab8yDhtYDJnFrjI4]`|
|[Combat Grapnel](equipment/weapon-01-3T3ayvgw9HDn05Mz.htm)||`@Compendium[pf2e.equipment-srd.3T3ayvgw9HDn05Mz]`|
|[Comealong](equipment/equipment-00-4QufuSeCbjDD2hMa.htm)||`@Compendium[pf2e.equipment-srd.4QufuSeCbjDD2hMa]`|
|[Communication Bangle](equipment/equipment-01-thWEk8pHq1hqyhMn.htm)||`@Compendium[pf2e.equipment-srd.thWEk8pHq1hqyhMn]`|
|[Compass](equipment/equipment-00-wob6DB1FFdWs0mbp.htm)|Boussole|`@Compendium[pf2e.equipment-srd.wob6DB1FFdWs0mbp]`|
|[Compass (Lensatic)](equipment/equipment-03-xVhd8NF9KQ6VWfMu.htm)||`@Compendium[pf2e.equipment-srd.xVhd8NF9KQ6VWfMu]`|
|[Composite Longbow](equipment/weapon-00-dUC8Fsa6FZtVikS3.htm)|Arc long composite|`@Compendium[pf2e.equipment-srd.dUC8Fsa6FZtVikS3]`|
|[Composite Shortbow](equipment/weapon-00-e4NwsnPnpQKbDZ9F.htm)|Arc court composite|`@Compendium[pf2e.equipment-srd.e4NwsnPnpQKbDZ9F]`|
|[Comprehension Elixir (Greater)](equipment/consumable-07-OcBPjVplvy2GbQ8P.htm)|Elixir de compréhension (supérieur)|`@Compendium[pf2e.equipment-srd.OcBPjVplvy2GbQ8P]`|
|[Comprehension Elixir (Lesser)](equipment/consumable-02-3Klm2gPmzOw6ntVb.htm)|Elixir de comprehension (inférieur)|`@Compendium[pf2e.equipment-srd.3Klm2gPmzOw6ntVb]`|
|[Concealed Sheath](equipment/equipment-03-laU7xnX42wXch2Dv.htm)||`@Compendium[pf2e.equipment-srd.laU7xnX42wXch2Dv]`|
|[Cookware](equipment/equipment-00-Yf7maiRDmmHAyF82.htm)|Batterie de cuisine|`@Compendium[pf2e.equipment-srd.Yf7maiRDmmHAyF82]`|
|[Copper Dragon's Breath Potion (Adult)](equipment/consumable-12-2VC7FLItVrFWNe4L.htm)|Potion de souffle du dragon de cuivre adulte|`@Compendium[pf2e.equipment-srd.2VC7FLItVrFWNe4L]`|
|[Copper Dragon's Breath Potion (Wyrm)](equipment/consumable-17-2JXUER4xY8s36HUv.htm)|Potion de souffle de dracosire de cuivre|`@Compendium[pf2e.equipment-srd.2JXUER4xY8s36HUv]`|
|[Copper Dragon's Breath Potion (Young)](equipment/consumable-07-vPcDIowg3LAqZYAE.htm)|Potion de souffle du jeune dragon de cuivre|`@Compendium[pf2e.equipment-srd.vPcDIowg3LAqZYAE]`|
|[Copper Pieces](equipment/treasure-00-lzJ8AVhRcbFul5fh.htm)|Pièces de cuivre|`@Compendium[pf2e.equipment-srd.lzJ8AVhRcbFul5fh]`|
|[Copper and spinel puzzle box](equipment/treasure-00-PNnmcBhEwZmAKsq9.htm)|boîte de puzzle en cuivre ornée de spinelles|`@Compendium[pf2e.equipment-srd.PNnmcBhEwZmAKsq9]`|
|[Copper statuette of a salamander](equipment/treasure-00-xXo7KXnhp1TfvxRc.htm)|Statuette de salamandre en cuivre|`@Compendium[pf2e.equipment-srd.xXo7KXnhp1TfvxRc]`|
|[Coral](equipment/treasure-00-u7YrZzA2yem780cP.htm)|Corail|`@Compendium[pf2e.equipment-srd.u7YrZzA2yem780cP]`|
|[Coral idol of an elemental lord](equipment/treasure-00-EKgLX0TCyFik3gPS.htm)|Idole en corail d'un seigneur élémentaire|`@Compendium[pf2e.equipment-srd.EKgLX0TCyFik3gPS]`|
|[Cordelia's Construct Key](equipment/equipment-08-UBfAwXBU5iQ59Bz7.htm)||`@Compendium[pf2e.equipment-srd.UBfAwXBU5iQ59Bz7]`|
|[Cordelia's Greater Construct Key](equipment/equipment-12-rUxQi4pUbMeHrnSX.htm)||`@Compendium[pf2e.equipment-srd.rUxQi4pUbMeHrnSX]`|
|[Corpseward Pendant](equipment/equipment-05-8YnlhfZSQ89FjDZ2.htm)||`@Compendium[pf2e.equipment-srd.8YnlhfZSQ89FjDZ2]`|
|[Corrosive](equipment/equipment-08-Wm0X7Pfd1bfocPSv.htm)|Corrosive (rune)|`@Compendium[pf2e.equipment-srd.Wm0X7Pfd1bfocPSv]`|
|[Corrosive (Greater)](equipment/equipment-15-vQUIUAFOTOWj3ohh.htm)|Corrosive supérieure (rune)|`@Compendium[pf2e.equipment-srd.vQUIUAFOTOWj3ohh]`|
|[Corrosive Ammunition](equipment/consumable-07-903CuhvVUhE1lmoB.htm)||`@Compendium[pf2e.equipment-srd.903CuhvVUhE1lmoB]`|
|[Coyote Cloak](equipment/equipment-03-mvMeloQxSiEGIlhL.htm)|Cape de coyote|`@Compendium[pf2e.equipment-srd.mvMeloQxSiEGIlhL]`|
|[Coyote Cloak (Greater)](equipment/equipment-09-sls3pfkhgCbW723f.htm)|Cape de coyotte (supérieure)|`@Compendium[pf2e.equipment-srd.sls3pfkhgCbW723f]`|
|[Crafter's Eyepiece](equipment/equipment-03-RgNBGpBc9G2yw1C2.htm)|Oculaire d'artisan|`@Compendium[pf2e.equipment-srd.RgNBGpBc9G2yw1C2]`|
|[Crafter's Eyepiece (Greater)](equipment/equipment-11-JSYtBZ7XblyAEFoV.htm)|Oculaire d'artisan (supérieur)|`@Compendium[pf2e.equipment-srd.JSYtBZ7XblyAEFoV]`|
|[Crimson Brand](equipment/weapon-09-k5P9YZO4ARlE4By3.htm)||`@Compendium[pf2e.equipment-srd.k5P9YZO4ARlE4By3]`|
|[Crimson Worm Repellent](equipment/consumable-18-nctubaNAmzvXYDkc.htm)||`@Compendium[pf2e.equipment-srd.nctubaNAmzvXYDkc]`|
|[Crossbow](equipment/weapon-00-62nnVQvGhoVLLl2K.htm)|Arbalète|`@Compendium[pf2e.equipment-srd.62nnVQvGhoVLLl2K]`|
|[Crowbar](equipment/equipment-00-44F1mfJei4GY8f2X.htm)|Pied-de-biche|`@Compendium[pf2e.equipment-srd.44F1mfJei4GY8f2X]`|
|[Crowbar (Levered)](equipment/equipment-03-4kz3vhkKPUuXBpxk.htm)||`@Compendium[pf2e.equipment-srd.4kz3vhkKPUuXBpxk]`|
|[Crying Angel Pendant](equipment/consumable-02-HHELOoN5GVonUiIa.htm)|Pendentif de l'ange en pleurs|`@Compendium[pf2e.equipment-srd.HHELOoN5GVonUiIa]`|
|[Crystal Ball (Clear Quartz)](equipment/equipment-14-RM3SR1omNZfppZGl.htm)|Boule de cristal|`@Compendium[pf2e.equipment-srd.RM3SR1omNZfppZGl]`|
|[Crystal Ball (Moonstone)](equipment/equipment-16-S0IshWO7Vx29PKaq.htm)||`@Compendium[pf2e.equipment-srd.S0IshWO7Vx29PKaq]`|
|[Crystal Ball (Obsidian)](equipment/equipment-19-WKdI4LbwgcNHhMdp.htm)||`@Compendium[pf2e.equipment-srd.WKdI4LbwgcNHhMdp]`|
|[Crystal Ball (Peridot)](equipment/equipment-17-ByRyBRjNBcK5rwQ9.htm)||`@Compendium[pf2e.equipment-srd.ByRyBRjNBcK5rwQ9]`|
|[Crystal Ball (Selenite)](equipment/equipment-15-VDMYyVQUWJAjVyru.htm)||`@Compendium[pf2e.equipment-srd.VDMYyVQUWJAjVyru]`|
|[Crystal Shards (Greater)](equipment/weapon-12-ivaL0xt33k6QNwQK.htm)|Éclats de cristal (supérieur)|`@Compendium[pf2e.equipment-srd.ivaL0xt33k6QNwQK]`|
|[Crystal Shards (Major)](equipment/weapon-18-V2knI4GpdJYLupjg.htm)|Éclats de cristal (majeur)|`@Compendium[pf2e.equipment-srd.V2knI4GpdJYLupjg]`|
|[Crystal Shards (Moderate)](equipment/weapon-04-V4wgOWYmHlbSZsVG.htm)|Éclats de  cristal (modéré)|`@Compendium[pf2e.equipment-srd.V4wgOWYmHlbSZsVG]`|
|[Crystal dinner set, fine silverware](equipment/treasure-00-ugOCvoAZnVyEuIiN.htm)|Couverts d'argenterie fine et verres en cristal|`@Compendium[pf2e.equipment-srd.ugOCvoAZnVyEuIiN]`|
|[Crystallized dragon heart](equipment/treasure-00-svhZ61CaSgatn5by.htm)|Cœur de dragon cristallisé|`@Compendium[pf2e.equipment-srd.svhZ61CaSgatn5by]`|
|[Cunning](equipment/equipment-05-T4gTHDKJ0HI10p3y.htm)||`@Compendium[pf2e.equipment-srd.T4gTHDKJ0HI10p3y]`|
|[Cursed Dreamstone](equipment/equipment-09-idw9HdfR4QvseXsc.htm)|Pierre onirique maudite|`@Compendium[pf2e.equipment-srd.idw9HdfR4QvseXsc]`|
|[Cytillesh](equipment/consumable-05-rjUJEY424jyG9dGn.htm)|Cytillesh|`@Compendium[pf2e.equipment-srd.rjUJEY424jyG9dGn]`|
|[Cytillesh Oil](equipment/consumable-03-aZm1x9tpvBAT8YCd.htm)|Hule de Cytillesh|`@Compendium[pf2e.equipment-srd.aZm1x9tpvBAT8YCd]`|
|[Dagger](equipment/weapon-00-rQWaJhI5Bko5x14Z.htm)|Dague|`@Compendium[pf2e.equipment-srd.rQWaJhI5Bko5x14Z]`|
|[Dagger of Eternal Sleep](equipment/weapon-05-1S3RgLWa1DBitB9V.htm)||`@Compendium[pf2e.equipment-srd.1S3RgLWa1DBitB9V]`|
|[Dagger of Venom](equipment/weapon-05-ZvIEJCY60fHqzl6r.htm)|Dague venimeuse|`@Compendium[pf2e.equipment-srd.ZvIEJCY60fHqzl6r]`|
|[Daikyu](equipment/weapon-00-RN3m5Hb5LPbdZGoO.htm)|Daikyu|`@Compendium[pf2e.equipment-srd.RN3m5Hb5LPbdZGoO]`|
|[Dancing](equipment/equipment-13-DCPsilr8wbPXxTUv.htm)|Dansante (rune)|`@Compendium[pf2e.equipment-srd.DCPsilr8wbPXxTUv]`|
|[Dancing Scarf](equipment/equipment-03-Epc1e1Q9M9bcwOR0.htm)|Echarpe dansante|`@Compendium[pf2e.equipment-srd.Epc1e1Q9M9bcwOR0]`|
|[Dancing Scarf (Greater)](equipment/equipment-09-QJ1PhbtbLzwhRlY0.htm)|Écharpe dansante (supérieure)|`@Compendium[pf2e.equipment-srd.QJ1PhbtbLzwhRlY0]`|
|[Daredevil Boots](equipment/equipment-10-mmdnWrQsh7vDspLK.htm)|Bottes de casse-cou|`@Compendium[pf2e.equipment-srd.mmdnWrQsh7vDspLK]`|
|[Daredevil Boots (Greater)](equipment/equipment-17-kjFFmqci69k2zMXF.htm)|Bottes de casse-cou (supérieures)|`@Compendium[pf2e.equipment-srd.kjFFmqci69k2zMXF]`|
|[Darkvision Elixir (Greater)](equipment/consumable-08-bz8Y8bVUe7QfBk9g.htm)|Élixir de vision dans le noir (supérieur)|`@Compendium[pf2e.equipment-srd.bz8Y8bVUe7QfBk9g]`|
|[Darkvision Elixir (Lesser)](equipment/consumable-02-88pGCHV0uKMskTVO.htm)|Élixir de vision dans le noir (mineure)|`@Compendium[pf2e.equipment-srd.88pGCHV0uKMskTVO]`|
|[Darkvision Elixir (Moderate)](equipment/consumable-04-Y8115p3cmQJBqk5d.htm)|Élixir de vision dans le noir (modéré)|`@Compendium[pf2e.equipment-srd.Y8115p3cmQJBqk5d]`|
|[Darkwood Branch](equipment/equipment-00-5NHciGwLcVJwEjQu.htm)|Branche d'ébénite|`@Compendium[pf2e.equipment-srd.5NHciGwLcVJwEjQu]`|
|[Darkwood Buckler (High-Grade)](equipment/armor-16-sRHR4cEo8WnowjU3.htm)|targe en ébénite de haute qualité|`@Compendium[pf2e.equipment-srd.sRHR4cEo8WnowjU3]`|
|[Darkwood Buckler (Standard-Grade)](equipment/armor-08-BUHV5rev5HVgCrUB.htm)|Targe en ébénite (qualité standard)|`@Compendium[pf2e.equipment-srd.BUHV5rev5HVgCrUB]`|
|[Darkwood Lumber](equipment/equipment-00-r9lPNhrvG69Ae88d.htm)|Planche d'ébénite|`@Compendium[pf2e.equipment-srd.r9lPNhrvG69Ae88d]`|
|[Darkwood Shield (High-Grade)](equipment/armor-16-V4kbHwiODxwJOJga.htm)|bouclier en ébénite de haute qualité|`@Compendium[pf2e.equipment-srd.V4kbHwiODxwJOJga]`|
|[Darkwood Shield (Standard-Grade)](equipment/armor-08-UdvSJPCtBr2Y1e22.htm)|Bouclier en ébénite de qualité standard|`@Compendium[pf2e.equipment-srd.UdvSJPCtBr2Y1e22]`|
|[Darkwood Tower Shield (High-Grade)](equipment/armor-16-8gcuJ2Ev3euFyXnS.htm)|pavois en ébénite de haute qualité|`@Compendium[pf2e.equipment-srd.8gcuJ2Ev3euFyXnS]`|
|[Darkwood Tower Shield (Standard-Grade)](equipment/armor-08-Snu8yjMrUi3ockyA.htm)|Pavois en ébénite de qualité standard|`@Compendium[pf2e.equipment-srd.Snu8yjMrUi3ockyA]`|
|[Darkwood violin by a legend](equipment/treasure-00-PTkTPYtxrPwgCzeq.htm)|Violon en ébénite d'un facteur légendaire|`@Compendium[pf2e.equipment-srd.PTkTPYtxrPwgCzeq]`|
|[Dart](equipment/weapon-00-Tt4Qw64fwrxhr5gT.htm)|Fléchette|`@Compendium[pf2e.equipment-srd.Tt4Qw64fwrxhr5gT]`|
|[Daylight Vapor](equipment/consumable-13-6vyr3drpizIrd5PS.htm)||`@Compendium[pf2e.equipment-srd.6vyr3drpizIrd5PS]`|
|[Dazing Coil](equipment/consumable-14-ClKb4YQn8TfPIclE.htm)|Anneaux d'étourdissement|`@Compendium[pf2e.equipment-srd.ClKb4YQn8TfPIclE]`|
|[Deafening Music Box](equipment/equipment-08-k9BYXA4b7Z4Hy6lZ.htm)||`@Compendium[pf2e.equipment-srd.k9BYXA4b7Z4Hy6lZ]`|
|[Death Knell Powder](equipment/consumable-14-8qUja4YdvewN4es0.htm)||`@Compendium[pf2e.equipment-srd.8qUja4YdvewN4es0]`|
|[Deathcap Powder](equipment/consumable-13-87AzvRja9uQOLJCC.htm)|Poudre d'amanite phalloïde|`@Compendium[pf2e.equipment-srd.87AzvRja9uQOLJCC]`|
|[Decanter of Endless Water](equipment/equipment-07-aBVrNIPoPGOYxm80.htm)|Carafe intarissable|`@Compendium[pf2e.equipment-srd.aBVrNIPoPGOYxm80]`|
|[Deck of Many Things](equipment/equipment-22-Gyi4IVrAVJRPJF2s.htm)||`@Compendium[pf2e.equipment-srd.Gyi4IVrAVJRPJF2s]`|
|[Deepdread Claw](equipment/weapon-14-jE5H6jmDJEzxGvSP.htm)||`@Compendium[pf2e.equipment-srd.jE5H6jmDJEzxGvSP]`|
|[Demilich Eye Gem](equipment/equipment-15-Q4o8pEdxSY0bnJQZ.htm)||`@Compendium[pf2e.equipment-srd.Q4o8pEdxSY0bnJQZ]`|
|[Demon Armor](equipment/armor-13-hYBZK1kaGPeR85CH.htm)|Armure démoniaque|`@Compendium[pf2e.equipment-srd.hYBZK1kaGPeR85CH]`|
|[Demon Mask](equipment/equipment-04-2gYZiUw9yjtb0yJY.htm)|Masque du démon|`@Compendium[pf2e.equipment-srd.2gYZiUw9yjtb0yJY]`|
|[Demon Mask (Greater)](equipment/equipment-10-shKPtYN0YUOe07K2.htm)|Masque du démon (supérieur)|`@Compendium[pf2e.equipment-srd.shKPtYN0YUOe07K2]`|
|[Depth Gauge](equipment/equipment-00-7HHKalX0yYJSHROS.htm)||`@Compendium[pf2e.equipment-srd.7HHKalX0yYJSHROS]`|
|[Detective's Kit](equipment/equipment-03-ItLZL9Bd6xwgfeB8.htm)|Kit de l'investigateur|`@Compendium[pf2e.equipment-srd.ItLZL9Bd6xwgfeB8]`|
|[Deteriorating Dust](equipment/consumable-04-SGS7fA2hcElw1EaL.htm)||`@Compendium[pf2e.equipment-srd.SGS7fA2hcElw1EaL]`|
|[Diadem of Intellect](equipment/equipment-17-1FKDq4Gfev5GObDT.htm)|Diadème d'intellect|`@Compendium[pf2e.equipment-srd.1FKDq4Gfev5GObDT]`|
|[Diamond ring with platinum band](equipment/treasure-00-ZQXCEq8IyGf3WnEj.htm)|Anneau de platine avec un diamant|`@Compendium[pf2e.equipment-srd.ZQXCEq8IyGf3WnEj]`|
|[Diamond, large](equipment/treasure-00-7OuVXvNRbNoKzDSy.htm)|Diamant, gros|`@Compendium[pf2e.equipment-srd.7OuVXvNRbNoKzDSy]`|
|[Diamond, small](equipment/treasure-00-z3PT7OgX34TQKAwH.htm)|Diamant, petit|`@Compendium[pf2e.equipment-srd.z3PT7OgX34TQKAwH]`|
|[Diluted Hype](equipment/consumable-05-1CAPGpQczfq5exrs.htm)||`@Compendium[pf2e.equipment-srd.1CAPGpQczfq5exrs]`|
|[Diplomat's Badge](equipment/equipment-05-JJZgRx6naNJmDa81.htm)|Insigne du diplomate|`@Compendium[pf2e.equipment-srd.JJZgRx6naNJmDa81]`|
|[Disguise Kit](equipment/equipment-00-Jvp0x2Sc82WVpExT.htm)|Kit de déguisement|`@Compendium[pf2e.equipment-srd.Jvp0x2Sc82WVpExT]`|
|[Disguise Kit (Elite Cosmetics)](equipment/equipment-03-IRqbsE8MgGLTfHLz.htm)||`@Compendium[pf2e.equipment-srd.IRqbsE8MgGLTfHLz]`|
|[Disguise Kit (Elite)](equipment/equipment-03-YhkomhtKBK3i9C7Q.htm)|Kit de déguisement (Elite)|`@Compendium[pf2e.equipment-srd.YhkomhtKBK3i9C7Q]`|
|[Disguise Kit (Replacement Cosmetics)](equipment/equipment-00-FWMGkUotwUJuht7i.htm)|Kit de déguisement (Stock de cosmétiques)|`@Compendium[pf2e.equipment-srd.FWMGkUotwUJuht7i]`|
|[Disintegration Bolt](equipment/consumable-15-Gfew65lwkzZc3mUV.htm)|Carreau de désintégration|`@Compendium[pf2e.equipment-srd.Gfew65lwkzZc3mUV]`|
|[Dispelling Sliver](equipment/consumable-16-mGIJCGFkBQLkzhTg.htm)||`@Compendium[pf2e.equipment-srd.mGIJCGFkBQLkzhTg]`|
|[Disrupting](equipment/equipment-05-LwQb7ryTC8FlOXgX.htm)|Pertubatrice (rune)|`@Compendium[pf2e.equipment-srd.LwQb7ryTC8FlOXgX]`|
|[Disrupting (Greater)](equipment/equipment-14-oVrVzML63VFvVfKk.htm)|Pertubatrice supérieure (rune)|`@Compendium[pf2e.equipment-srd.oVrVzML63VFvVfKk]`|
|[Divine Scroll Case of Simplicity](equipment/equipment-04-9zdm3EyEQXgMox8b.htm)|Étui à parchemin de simplicité divine|`@Compendium[pf2e.equipment-srd.9zdm3EyEQXgMox8b]`|
|[Divine art piece created by Shelyn](equipment/treasure-00-SmN0vRjkPaSaAJZt.htm)|Oeuvre d'art divine (par Shélyn)|`@Compendium[pf2e.equipment-srd.SmN0vRjkPaSaAJZt]`|
|[Diviner's Nose Chain](equipment/equipment-09-O6deRhRldwTqMP97.htm)||`@Compendium[pf2e.equipment-srd.O6deRhRldwTqMP97]`|
|[Dogslicer](equipment/weapon-00-olwngGXM3hpgoLEP.htm)|Tranchechien|`@Compendium[pf2e.equipment-srd.olwngGXM3hpgoLEP]`|
|[Doubling Rings](equipment/equipment-03-DwMXEqy7Ws8NYQQh.htm)|Anneau de doublement|`@Compendium[pf2e.equipment-srd.DwMXEqy7Ws8NYQQh]`|
|[Doubling Rings (Greater)](equipment/equipment-11-gRQXLqUuP4GWfDWI.htm)||`@Compendium[pf2e.equipment-srd.gRQXLqUuP4GWfDWI]`|
|[Dragon Bile](equipment/consumable-15-6swrLimere2nZtz9.htm)|Bile de dragon|`@Compendium[pf2e.equipment-srd.6swrLimere2nZtz9]`|
|[Dragon Turtle Scale](equipment/consumable-04-GnXKCkgZQG0UmuHz.htm)|Écaille de tortue dragon|`@Compendium[pf2e.equipment-srd.GnXKCkgZQG0UmuHz]`|
|[Dragon's Eye Charm](equipment/equipment-07-A4BhFOb4iQtJYYKq.htm)|Charme d'oeil de dragon|`@Compendium[pf2e.equipment-srd.A4BhFOb4iQtJYYKq]`|
|[Dragonhide Buckler (High-Grade)](equipment/armor-16-R1I1MEKwDYtCfZr3.htm)|Targe en peau de dragon (Haute qualité)|`@Compendium[pf2e.equipment-srd.R1I1MEKwDYtCfZr3]`|
|[Dragonhide Buckler (Standard-Grade)](equipment/armor-08-vfzzrp0tKXznWnhY.htm)|Taarge en peau de dragon (qualité normale)|`@Compendium[pf2e.equipment-srd.vfzzrp0tKXznWnhY]`|
|[Dragonhide Shield (High-Grade)](equipment/armor-16-LVixPROPOwIfHgEK.htm)|Bouclier en Peau de Dragon de Haute Qualité|`@Compendium[pf2e.equipment-srd.LVixPROPOwIfHgEK]`|
|[Dragonhide Shield (Standard-Grade)](equipment/armor-08-daX1jX7XYo3mWZmP.htm)|Bouclier en Peau de Dragon de Qualité Standard|`@Compendium[pf2e.equipment-srd.daX1jX7XYo3mWZmP]`|
|[Dragonplate](equipment/armor-16-3xsQ1AA4dyMHLxpw.htm)|Harnois du dragon|`@Compendium[pf2e.equipment-srd.3xsQ1AA4dyMHLxpw]`|
|[Dragonscale Amulet](equipment/equipment-15-bTLmJMATrrtq8NuT.htm)|Amulette en écailes de dragon|`@Compendium[pf2e.equipment-srd.bTLmJMATrrtq8NuT]`|
|[Dragonslayer's Shield](equipment/armor-09-Q8BscHUFiM1a86PO.htm)|Bouclier du pourfendeur de dragons|`@Compendium[pf2e.equipment-srd.Q8BscHUFiM1a86PO]`|
|[Drakeheart Mutagen (Greater)](equipment/consumable-11-WuzLBK78DgIt8SsN.htm)||`@Compendium[pf2e.equipment-srd.WuzLBK78DgIt8SsN]`|
|[Drakeheart Mutagen (Lesser)](equipment/consumable-01-GS4YvQieBS11JNYR.htm)|Mutagène de cœur de drake (Inférieur)|`@Compendium[pf2e.equipment-srd.GS4YvQieBS11JNYR]`|
|[Drakeheart Mutagen (Major)](equipment/consumable-17-M4ZOHOlne43ArjOC.htm)||`@Compendium[pf2e.equipment-srd.M4ZOHOlne43ArjOC]`|
|[Drakeheart Mutagen (Moderate)](equipment/consumable-03-xY2MogTwH9Fd8UPG.htm)||`@Compendium[pf2e.equipment-srd.xY2MogTwH9Fd8UPG]`|
|[Dread Ampoule (Greater)](equipment/weapon-11-kpJmjm843vaMZjIu.htm)|Ampoule d'effroi (supérieure)|`@Compendium[pf2e.equipment-srd.kpJmjm843vaMZjIu]`|
|[Dread Ampoule (Lesser)](equipment/weapon-01-ds0OdA989ZZw9km1.htm)|Ampoule d'effroi (inférieure)|`@Compendium[pf2e.equipment-srd.ds0OdA989ZZw9km1]`|
|[Dread Ampoule (Major)](equipment/weapon-17-MCHYtxP8E7njLC3s.htm)|Ampoule d'effroi (majeure)|`@Compendium[pf2e.equipment-srd.MCHYtxP8E7njLC3s]`|
|[Dread Ampoule (Moderate)](equipment/weapon-03-IvFEJqp2MUew65nQ.htm)|Ampoule d'effroi (modérée)|`@Compendium[pf2e.equipment-srd.IvFEJqp2MUew65nQ]`|
|[Dread Blindfold](equipment/equipment-17-pDw2wi0znVb8Dysg.htm)|Bandreau d'effroi|`@Compendium[pf2e.equipment-srd.pDw2wi0znVb8Dysg]`|
|[Dreadsmoke Thurible](equipment/equipment-09-XWWosdDg34KZhrEi.htm)||`@Compendium[pf2e.equipment-srd.XWWosdDg34KZhrEi]`|
|[Dreamstone](equipment/equipment-09-lRdrk7Dh9eVlXFHi.htm)|Pierre onirique|`@Compendium[pf2e.equipment-srd.lRdrk7Dh9eVlXFHi]`|
|[Dreamtime Tea](equipment/consumable-04-dwaF9zHPK9823COa.htm)|Thé des songes|`@Compendium[pf2e.equipment-srd.dwaF9zHPK9823COa]`|
|[Druid's Vestments](equipment/equipment-10-hvlEFx25ogf1K1C2.htm)|Chasuble de druide|`@Compendium[pf2e.equipment-srd.hvlEFx25ogf1K1C2]`|
|[Dueling Cape](equipment/equipment-00-DA3HgyEBGEbtRNOo.htm)|Cape de duel|`@Compendium[pf2e.equipment-srd.DA3HgyEBGEbtRNOo]`|
|[Dust Pods](equipment/consumable-01-azFpL1NLBJB7xuli.htm)|Sachet de poussière|`@Compendium[pf2e.equipment-srd.azFpL1NLBJB7xuli]`|
|[Dust of Appearance](equipment/consumable-06-8iGmSwTTUdj6gqN5.htm)|Poudre d'apparition|`@Compendium[pf2e.equipment-srd.8iGmSwTTUdj6gqN5]`|
|[Dust of Corpse Animation](equipment/consumable-08-2koNKqbQV05myfuL.htm)||`@Compendium[pf2e.equipment-srd.2koNKqbQV05myfuL]`|
|[Dust of Corpse Animation (Greater)](equipment/consumable-16-yPFHTY1GH3rdWwds.htm)||`@Compendium[pf2e.equipment-srd.yPFHTY1GH3rdWwds]`|
|[Dust of Disappearance](equipment/consumable-09-9XSOAzq3xp9g6qkF.htm)|Poudre de disparition|`@Compendium[pf2e.equipment-srd.9XSOAzq3xp9g6qkF]`|
|[Dwarven Daisy (Lesser)](equipment/weapon-01-xZFT5JdEvDi7q467.htm)|Fleur de mort naine (inférieure)|`@Compendium[pf2e.equipment-srd.xZFT5JdEvDi7q467]`|
|[Dwarven Daisy (Moderate)](equipment/weapon-03-Do4rJuA1nhr2TTiF.htm)|Fleur de mort naine (moyenne)|`@Compendium[pf2e.equipment-srd.Do4rJuA1nhr2TTiF]`|
|[Dwarven Thrower](equipment/weapon-13-XK4DM8wOtcuOsji6.htm)|Marteau de lancer nain|`@Compendium[pf2e.equipment-srd.XK4DM8wOtcuOsji6]`|
|[Dwarven War Axe](equipment/weapon-00-MYnh7w7EL3AcQT41.htm)|Hache de guerre naine|`@Compendium[pf2e.equipment-srd.MYnh7w7EL3AcQT41]`|
|[Eagle Eye Elixir (Greater)](equipment/consumable-10-xWCdsGSwLLTeX6tQ.htm)|Elixir d'oeil de faucon (supérieur)|`@Compendium[pf2e.equipment-srd.xWCdsGSwLLTeX6tQ]`|
|[Eagle Eye Elixir (Lesser)](equipment/consumable-01-7Y2yOr4ltpP2tyuL.htm)|Élixir d'œil du faucon (Inférieur)|`@Compendium[pf2e.equipment-srd.7Y2yOr4ltpP2tyuL]`|
|[Eagle Eye Elixir (Major)](equipment/consumable-16-kicNrnZz1KjJYRVI.htm)|Élixir d'œil de faucon (majeur)|`@Compendium[pf2e.equipment-srd.kicNrnZz1KjJYRVI]`|
|[Eagle Eye Elixir (Moderate)](equipment/consumable-05-lPcnDlBGz5QwCMYw.htm)|Élixir d'œil de faucon (modéré)|`@Compendium[pf2e.equipment-srd.lPcnDlBGz5QwCMYw]`|
|[Earplugs](equipment/equipment-00-iYiA6CMR1N2E2Ny2.htm)|Bouchons d'oreilles|`@Compendium[pf2e.equipment-srd.iYiA6CMR1N2E2Ny2]`|
|[Earthglide Cloak](equipment/equipment-15-3pKIEBtZBVmSmVPl.htm)||`@Compendium[pf2e.equipment-srd.3pKIEBtZBVmSmVPl]`|
|[Earthsight Box](equipment/equipment-09-kZKK4Va3e7n3p3tv.htm)||`@Compendium[pf2e.equipment-srd.kZKK4Va3e7n3p3tv]`|
|[Eclipse](equipment/weapon-08-FCA7CE4mK85SVLz3.htm)|Éclipse|`@Compendium[pf2e.equipment-srd.FCA7CE4mK85SVLz3]`|
|[Effervescent Ampoule](equipment/consumable-02-VPvyyQXjn2HBjnTS.htm)|Ampoule effervescente|`@Compendium[pf2e.equipment-srd.VPvyyQXjn2HBjnTS]`|
|[Elder Sign](equipment/equipment-25-KEICt6Tusa3JdTE8.htm)|Sceau des anciens|`@Compendium[pf2e.equipment-srd.KEICt6Tusa3JdTE8]`|
|[Electric Eelskin](equipment/armor-10-8HxM35X8DDt2gw9d.htm)|Peau d'anguille électrique|`@Compendium[pf2e.equipment-srd.8HxM35X8DDt2gw9d]`|
|[Elegant cloth doll](equipment/treasure-00-L8IzHSz2jYGFpp4N.htm)|Poupée de chiffon élégante|`@Compendium[pf2e.equipment-srd.L8IzHSz2jYGFpp4N]`|
|[Elemental Gem](equipment/consumable-10-TgZdamADHuaKyriW.htm)|Gemme élémentaire|`@Compendium[pf2e.equipment-srd.TgZdamADHuaKyriW]`|
|[Elemental Wayfinder (Air)](equipment/equipment-09-SMBPvsZH7yAsGSQa.htm)||`@Compendium[pf2e.equipment-srd.SMBPvsZH7yAsGSQa]`|
|[Elemental Wayfinder (Earth)](equipment/equipment-09-PMmJ47MLUXAWmXFg.htm)||`@Compendium[pf2e.equipment-srd.PMmJ47MLUXAWmXFg]`|
|[Elemental Wayfinder (Fire)](equipment/equipment-09-JhDhFq2JGBGR2lwb.htm)||`@Compendium[pf2e.equipment-srd.JhDhFq2JGBGR2lwb]`|
|[Elemental Wayfinder (Water)](equipment/equipment-09-QZliTmapMRrEJ6q8.htm)||`@Compendium[pf2e.equipment-srd.QZliTmapMRrEJ6q8]`|
|[Elixir of Life (Greater)](equipment/consumable-13-mYozcVg9fQY6zO7C.htm)|Elixir de vie (supérieur)|`@Compendium[pf2e.equipment-srd.mYozcVg9fQY6zO7C]`|
|[Elixir of Life (Lesser)](equipment/consumable-05-TZUskLT7yvP7N2co.htm)|Élixir de vie (Inférieur)|`@Compendium[pf2e.equipment-srd.TZUskLT7yvP7N2co]`|
|[Elixir of Life (Major)](equipment/consumable-15-AmxSqEoFhRLMYd1W.htm)|Elixir de vie (majeur)|`@Compendium[pf2e.equipment-srd.AmxSqEoFhRLMYd1W]`|
|[Elixir of Life (Minor)](equipment/consumable-01-hDLbR56Id2OtU318.htm)|Elixir de vie (mineur)|`@Compendium[pf2e.equipment-srd.hDLbR56Id2OtU318]`|
|[Elixir of Life (Moderate)](equipment/consumable-09-c21stU5rhN4F2fZl.htm)|Elixir de vie (modéré)|`@Compendium[pf2e.equipment-srd.c21stU5rhN4F2fZl]`|
|[Elixir of Life (True)](equipment/consumable-19-qK964kZz1ALcysOa.htm)|Elixir de vie (ultime)|`@Compendium[pf2e.equipment-srd.qK964kZz1ALcysOa]`|
|[Elixir of Rejuvenation](equipment/consumable-20-UHrLqWCnFEUspSQj.htm)|Elixir de jouvence|`@Compendium[pf2e.equipment-srd.UHrLqWCnFEUspSQj]`|
|[Elven Absinthe](equipment/consumable-05-26bM70yn28MjDT9A.htm)|Absinthe elfique|`@Compendium[pf2e.equipment-srd.26bM70yn28MjDT9A]`|
|[Elven Chain (High-Grade)](equipment/armor-20-PuqdH7DmNsN79HyE.htm)|Maille elfique (haute qualité)|`@Compendium[pf2e.equipment-srd.PuqdH7DmNsN79HyE]`|
|[Elven Chain (Standard-Grade)](equipment/armor-13-peAvz7u35GEfTXxp.htm)|Maille Elfique (qualité standard)|`@Compendium[pf2e.equipment-srd.peAvz7u35GEfTXxp]`|
|[Elven Curve Blade](equipment/weapon-00-mgrabKkyLmvxgLBK.htm)|Lame courbe elfique|`@Compendium[pf2e.equipment-srd.mgrabKkyLmvxgLBK]`|
|[Emerald](equipment/treasure-00-hMIG86aqKcVYJGsg.htm)|Émeraude|`@Compendium[pf2e.equipment-srd.hMIG86aqKcVYJGsg]`|
|[Emerald Grasshopper](equipment/consumable-05-3uhaf2YL9hmix3pe.htm)|Sauterelle d'éméraude|`@Compendium[pf2e.equipment-srd.3uhaf2YL9hmix3pe]`|
|[Emerald, brilliant green](equipment/treasure-00-0zVMeeklYZ7hcgXw.htm)|Émeraude, vert brillant|`@Compendium[pf2e.equipment-srd.0zVMeeklYZ7hcgXw]`|
|[Emergency Disguise](equipment/consumable-01-BjCGt377LpCwbtyS.htm)||`@Compendium[pf2e.equipment-srd.BjCGt377LpCwbtyS]`|
|[Energy Mutagen (Greater)](equipment/consumable-11-xVtyaEnYZ1aJT4ki.htm)||`@Compendium[pf2e.equipment-srd.xVtyaEnYZ1aJT4ki]`|
|[Energy Mutagen (Lesser)](equipment/consumable-01-mbrwudO35tItsldq.htm)||`@Compendium[pf2e.equipment-srd.mbrwudO35tItsldq]`|
|[Energy Mutagen (Major)](equipment/consumable-17-34MbgwW1SGlowyk2.htm)||`@Compendium[pf2e.equipment-srd.34MbgwW1SGlowyk2]`|
|[Energy Mutagen (Moderate)](equipment/consumable-03-SLJTJwyIRjuWjfuK.htm)||`@Compendium[pf2e.equipment-srd.SLJTJwyIRjuWjfuK]`|
|[Energy-Resistant](equipment/equipment-08-Lw3B9DpnyrpXD9Di.htm)|Résistance à l'énergie (rune)|`@Compendium[pf2e.equipment-srd.Lw3B9DpnyrpXD9Di]`|
|[Energy-Resistant (Greater)](equipment/equipment-12-9CAWAKkZE7dr4FlJ.htm)|Résistance à l'énergie supérieure (rune)|`@Compendium[pf2e.equipment-srd.9CAWAKkZE7dr4FlJ]`|
|[Engraved copper ring](equipment/treasure-00-Mm3xgz27Q94wZhti.htm)|Anneau de cuivre gravé|`@Compendium[pf2e.equipment-srd.Mm3xgz27Q94wZhti]`|
|[Engulfing Snare](equipment/consumable-14-DIQg2Tml1wWjSC1q.htm)||`@Compendium[pf2e.equipment-srd.DIQg2Tml1wWjSC1q]`|
|[Enormous chryselephantine sculpture by a legend](equipment/treasure-00-rgPjAtKt6J1AZ1Pj.htm)|Sculpture chryséléphantine énorme par un sculpteur légendaire|`@Compendium[pf2e.equipment-srd.rgPjAtKt6J1AZ1Pj]`|
|[Enormous tapestry of a major battle](equipment/treasure-00-6wCA8h0jpMnqPbib.htm)|Tapisserie immense représentant une bataille majeure|`@Compendium[pf2e.equipment-srd.6wCA8h0jpMnqPbib]`|
|[Envisioning Mask](equipment/equipment-11-SGbEZzm5MLSv1DDX.htm)||`@Compendium[pf2e.equipment-srd.SGbEZzm5MLSv1DDX]`|
|[Essence Prism](equipment/equipment-28-kwo4VKC9Qkplusxs.htm)|Prisme des essences|`@Compendium[pf2e.equipment-srd.kwo4VKC9Qkplusxs]`|
|[Etched copper ewer](equipment/treasure-cSBupUqjsHJwivbp.htm)|aiguière de cuivre gravée|`@Compendium[pf2e.equipment-srd.cSBupUqjsHJwivbp]`|
|[Ethereal](equipment/equipment-17-q70WXJO1rswduHuT.htm)|Ethérée (rune)|`@Compendium[pf2e.equipment-srd.q70WXJO1rswduHuT]`|
|[Ethersight Ring](equipment/equipment-07-qzgi4PCcvpH0QPVj.htm)||`@Compendium[pf2e.equipment-srd.qzgi4PCcvpH0QPVj]`|
|[Everburning Torch](equipment/equipment-01-mRz8Jmk4Q06SsZpC.htm)|Torche éternelle|`@Compendium[pf2e.equipment-srd.mRz8Jmk4Q06SsZpC]`|
|[Everyneed Pack](equipment/equipment-03-P8xfjaEsrcKxl7k0.htm)||`@Compendium[pf2e.equipment-srd.P8xfjaEsrcKxl7k0]`|
|[Everyneed Pack (Greater)](equipment/equipment-07-31tSOy1iHCjNNd4N.htm)||`@Compendium[pf2e.equipment-srd.31tSOy1iHCjNNd4N]`|
|[Exploding Shield](equipment/armor-05-slQRh4FVDzP8h1wj.htm)||`@Compendium[pf2e.equipment-srd.slQRh4FVDzP8h1wj]`|
|[Explorer's Yurt](equipment/equipment-10-9Y86NM6nt2WtYBOy.htm)|Yourte d'explorateur|`@Compendium[pf2e.equipment-srd.9Y86NM6nt2WtYBOy]`|
|[Explosive Ammunition](equipment/consumable-09-rKknk8odXDBpON5l.htm)|Munition explosive|`@Compendium[pf2e.equipment-srd.rKknk8odXDBpON5l]`|
|[Explosive Ammunition (Greater)](equipment/consumable-13-g27kK39Nzqphl1Tb.htm)|Munition explosive (supérieure)|`@Compendium[pf2e.equipment-srd.g27kK39Nzqphl1Tb]`|
|[Exquisite Sword Cane](equipment/weapon-04-RN6rEc8eSkruNLPW.htm)|Canne épée raffinée|`@Compendium[pf2e.equipment-srd.RN6rEc8eSkruNLPW]`|
|[Exquisite Sword Cane Sheath](equipment/weapon-00-R2mDnYVqewmx3dV0.htm)|Fourreau de canne épée raffinée|`@Compendium[pf2e.equipment-srd.R2mDnYVqewmx3dV0]`|
|[Extended Deteriorating Dust](equipment/consumable-06-sAMzkU7kzUZfqTPV.htm)||`@Compendium[pf2e.equipment-srd.sAMzkU7kzUZfqTPV]`|
|[Eye of Apprehension](equipment/consumable-12-cxOf1V1kN9tnj1g9.htm)|Oeil d'appréhension|`@Compendium[pf2e.equipment-srd.cxOf1V1kN9tnj1g9]`|
|[Eye of Fortune](equipment/equipment-13-lKMSAlp0ZoFUK4FV.htm)|Oeil de chance|`@Compendium[pf2e.equipment-srd.lKMSAlp0ZoFUK4FV]`|
|[Eye of the Wise](equipment/equipment-11-eoQRjG2RjppnbYGL.htm)|Oeil du sage|`@Compendium[pf2e.equipment-srd.eoQRjG2RjppnbYGL]`|
|[Eyes of the Eagle](equipment/equipment-09-dLTN4FU3qBoDZ5CJ.htm)|Yeux d'aigle|`@Compendium[pf2e.equipment-srd.dLTN4FU3qBoDZ5CJ]`|
|[Fade Band](equipment/consumable-12-FrbvejTNfS5fep9f.htm)|Fil de disparition|`@Compendium[pf2e.equipment-srd.FrbvejTNfS5fep9f]`|
|[Falchion](equipment/weapon-00-XGtIUZ4ZNKuFx1uL.htm)|Cimeterre à deux mains|`@Compendium[pf2e.equipment-srd.XGtIUZ4ZNKuFx1uL]`|
|[False Death](equipment/consumable-05-iUOcC9vFboSFI8fU.htm)||`@Compendium[pf2e.equipment-srd.iUOcC9vFboSFI8fU]`|
|[False Manacles](equipment/equipment-00-DAqI0GwKuRwBagz5.htm)||`@Compendium[pf2e.equipment-srd.DAqI0GwKuRwBagz5]`|
|[Familiar Satchel](equipment/equipment-00-xE2CGurZp4eHc1Oc.htm)||`@Compendium[pf2e.equipment-srd.xE2CGurZp4eHc1Oc]`|
|[Famous portrait by a master](equipment/treasure-00-LEfLVcrbhZPmyUyI.htm)|Portrait fameux par un maître peintre|`@Compendium[pf2e.equipment-srd.LEfLVcrbhZPmyUyI]`|
|[Fang Snare](equipment/consumable-04-YYD82q2NfAbuDmgf.htm)||`@Compendium[pf2e.equipment-srd.YYD82q2NfAbuDmgf]`|
|[Fashionable Wayfinder](equipment/equipment-03-XLGJ1bLhMtP1jc2w.htm)||`@Compendium[pf2e.equipment-srd.XLGJ1bLhMtP1jc2w]`|
|[Fauchard](equipment/weapon-00-FuS6F91Rhd4m3T6d.htm)|Fauchard|`@Compendium[pf2e.equipment-srd.FuS6F91Rhd4m3T6d]`|
|[Fear Gem](equipment/consumable-04-ZAtwiAPkk1zwCf82.htm)|Gemme effrayante|`@Compendium[pf2e.equipment-srd.ZAtwiAPkk1zwCf82]`|
|[Fearsome](equipment/equipment-05-P6v2AtJw7AUwaDzf.htm)||`@Compendium[pf2e.equipment-srd.P6v2AtJw7AUwaDzf]`|
|[Fearsome (Greater)](equipment/equipment-12-uz3JCjRvkE44jxMd.htm)||`@Compendium[pf2e.equipment-srd.uz3JCjRvkE44jxMd]`|
|[Fearweed](equipment/consumable-10-RH1fNsslZ1wdeqy2.htm)||`@Compendium[pf2e.equipment-srd.RH1fNsslZ1wdeqy2]`|
|[Feather Step Stone](equipment/consumable-03-0CNSvLpeSM4aIfPJ.htm)|Pierre de pas de plume|`@Compendium[pf2e.equipment-srd.0CNSvLpeSM4aIfPJ]`|
|[Feather Token (Anchor)](equipment/consumable-07-VvljzRwthKMgqUR3.htm)|Plume magique (ancre)|`@Compendium[pf2e.equipment-srd.VvljzRwthKMgqUR3]`|
|[Feather Token (Bird)](equipment/consumable-03-fFq8nsGvSUgzVeND.htm)|Plume magique (oiseau)|`@Compendium[pf2e.equipment-srd.fFq8nsGvSUgzVeND]`|
|[Feather Token (Chest)](equipment/consumable-03-Dn2KQgIeWNijaUzL.htm)|Plume magique (coffre)|`@Compendium[pf2e.equipment-srd.Dn2KQgIeWNijaUzL]`|
|[Feather Token (Fan)](equipment/consumable-04-3Fmbw9wJkqZBV9De.htm)|Plume magique (éventail)|`@Compendium[pf2e.equipment-srd.3Fmbw9wJkqZBV9De]`|
|[Feather Token (Holly Bush)](equipment/consumable-02-cwurQLvQaqjK70UI.htm)|Plume magique (buisson de houx)|`@Compendium[pf2e.equipment-srd.cwurQLvQaqjK70UI]`|
|[Feather Token (Ladder)](equipment/consumable-01-3cWko20JPnjeoofV.htm)|Plume magique (échelle)|`@Compendium[pf2e.equipment-srd.3cWko20JPnjeoofV]`|
|[Feather Token (Swan Boat)](equipment/consumable-08-WzsKbMMewXbf1nws.htm)|Plume magique (bateau cygne)|`@Compendium[pf2e.equipment-srd.WzsKbMMewXbf1nws]`|
|[Feather Token (Tree)](equipment/consumable-06-4owFeOy4zxy8rv7W.htm)|Plume magique (arbre)|`@Compendium[pf2e.equipment-srd.4owFeOy4zxy8rv7W]`|
|[Feather Token (Whip)](equipment/consumable-09-XAuWVlZYGgCguzwz.htm)|Plume magique (fouet)|`@Compendium[pf2e.equipment-srd.XAuWVlZYGgCguzwz]`|
|[Fiendish Teleportation](equipment/equipment-11-2z9wagHoADDOnyvZ.htm)||`@Compendium[pf2e.equipment-srd.2z9wagHoADDOnyvZ]`|
|[Fighter's Fork](equipment/weapon-03-Kx6FQS5GyVB6jlrW.htm)|Fourche de guerre|`@Compendium[pf2e.equipment-srd.Kx6FQS5GyVB6jlrW]`|
|[Fighting Fan](equipment/weapon-00-76AqNZB5xuFWxgJI.htm)|Éventail de combat|`@Compendium[pf2e.equipment-srd.76AqNZB5xuFWxgJI]`|
|[Filcher's Fork](equipment/weapon-00-CVTgOpNuRE7hsnc1.htm)|Fourchette du chapardeur|`@Compendium[pf2e.equipment-srd.CVTgOpNuRE7hsnc1]`|
|[Final Blade](equipment/equipment-20-5Q5MfHX43Sjm8C6Z.htm)|Lame finale|`@Compendium[pf2e.equipment-srd.5Q5MfHX43Sjm8C6Z]`|
|[Fine gold spyglass](equipment/treasure-00-x46wlAbSDKFEumvg.htm)|Belle longue vue en or|`@Compendium[pf2e.equipment-srd.x46wlAbSDKFEumvg]`|
|[Fingerprint Kit](equipment/equipment-03-QlFJyxBTYFSN2EA7.htm)||`@Compendium[pf2e.equipment-srd.QlFJyxBTYFSN2EA7]`|
|[Fire Poi](equipment/weapon-00-rfP9e1fnwjnIQSJK.htm)|Poï enflammé|`@Compendium[pf2e.equipment-srd.rfP9e1fnwjnIQSJK]`|
|[Fire-Jump Ring](equipment/equipment-10-64mxKuxc9k98FkUi.htm)||`@Compendium[pf2e.equipment-srd.64mxKuxc9k98FkUi]`|
|[Fishing Tackle](equipment/equipment-00-3tOyV4VZEZhwnAMO.htm)|Articles de pêche|`@Compendium[pf2e.equipment-srd.3tOyV4VZEZhwnAMO]`|
|[Fishing Tackle (Professional)](equipment/equipment-03-ksorBjuXO0Bvdmhl.htm)||`@Compendium[pf2e.equipment-srd.ksorBjuXO0Bvdmhl]`|
|[Fist](equipment/weapon-00-UcLqR32mNaGdNkUk.htm)|Poing|`@Compendium[pf2e.equipment-srd.UcLqR32mNaGdNkUk]`|
|[Flail](equipment/weapon-00-t5FbyZtRL4qV0V7k.htm)|Fléau|`@Compendium[pf2e.equipment-srd.t5FbyZtRL4qV0V7k]`|
|[Flame Navette](equipment/consumable-16-4Pmo9gc81JAOzdke.htm)|Navette flamboyante|`@Compendium[pf2e.equipment-srd.4Pmo9gc81JAOzdke]`|
|[Flame Tongue](equipment/weapon-13-G3a60Ug3YNCMyMVR.htm)|Épée ardente|`@Compendium[pf2e.equipment-srd.G3a60Ug3YNCMyMVR]`|
|[Flame Tongue (Greater)](equipment/weapon-17-pvxRcuBexbFawjCg.htm)|Épée ardente (supérieure)|`@Compendium[pf2e.equipment-srd.pvxRcuBexbFawjCg]`|
|[Flaming](equipment/equipment-08-XszNvxnymWYRaoTp.htm)|Enflammée (rune)|`@Compendium[pf2e.equipment-srd.XszNvxnymWYRaoTp]`|
|[Flaming (Greater)](equipment/equipment-15-RSZwUlCzUX7Nb4UA.htm)|Enflamée supérieure (rune)|`@Compendium[pf2e.equipment-srd.RSZwUlCzUX7Nb4UA]`|
|[Flare Snare](equipment/consumable-02-riLNCaVS9zGvt4Nn.htm)||`@Compendium[pf2e.equipment-srd.riLNCaVS9zGvt4Nn]`|
|[Flawed Orb of Gold Dragonkind](equipment/equipment-25-9Ptn7yy2QeM8taU8.htm)||`@Compendium[pf2e.equipment-srd.9Ptn7yy2QeM8taU8]`|
|[Flaying Knife](equipment/weapon-08-dfheZ5lK0thQUTVT.htm)||`@Compendium[pf2e.equipment-srd.dfheZ5lK0thQUTVT]`|
|[Flayleaf](equipment/consumable-00-PTwm2NAcReBw2iUY.htm)|Éreintante|`@Compendium[pf2e.equipment-srd.PTwm2NAcReBw2iUY]`|
|[Flint and Steel](equipment/equipment-00-UlIxxLm71UdRgCFE.htm)|Silex et amorce|`@Compendium[pf2e.equipment-srd.UlIxxLm71UdRgCFE]`|
|[Floating Shield](equipment/armor-11-ydNocKN6afzIyVh6.htm)|Bouclier flottant|`@Compendium[pf2e.equipment-srd.ydNocKN6afzIyVh6]`|
|[Floating Shield (Greater)](equipment/armor-16-uAs0hUUgROrndceD.htm)|Bouclier flottant (supérieur)|`@Compendium[pf2e.equipment-srd.uAs0hUUgROrndceD]`|
|[Floorbell](equipment/equipment-05-98JIeYGEuE6pPV05.htm)||`@Compendium[pf2e.equipment-srd.98JIeYGEuE6pPV05]`|
|[Flying Blade Wheel Snare](equipment/consumable-20-qvfO0jFIvIM8hReG.htm)||`@Compendium[pf2e.equipment-srd.qvfO0jFIvIM8hReG]`|
|[Focus Cathartic (Greater)](equipment/consumable-12-vMs9n8oXlZttcJkX.htm)||`@Compendium[pf2e.equipment-srd.vMs9n8oXlZttcJkX]`|
|[Focus Cathartic (Lesser)](equipment/consumable-02-8KbayiwrUJtvif0a.htm)||`@Compendium[pf2e.equipment-srd.8KbayiwrUJtvif0a]`|
|[Focus Cathartic (Major)](equipment/consumable-18-Pnw4A7fhUhTS85Te.htm)||`@Compendium[pf2e.equipment-srd.Pnw4A7fhUhTS85Te]`|
|[Focus Cathartic (Moderate)](equipment/consumable-04-XfcSVHwMA60JxUXJ.htm)||`@Compendium[pf2e.equipment-srd.XfcSVHwMA60JxUXJ]`|
|[Force Shield](equipment/armor-09-DIzZr0K20eCbNzQo.htm)|Bouclier de force|`@Compendium[pf2e.equipment-srd.DIzZr0K20eCbNzQo]`|
|[Forensic Dye](equipment/consumable-01-8eTGOQ9P69405jIO.htm)|Teinture forensique|`@Compendium[pf2e.equipment-srd.8eTGOQ9P69405jIO]`|
|[Forge Warden](equipment/armor-10-NoTUhKkiTY5BQU5T.htm)|Gardien de la forge|`@Compendium[pf2e.equipment-srd.NoTUhKkiTY5BQU5T]`|
|[Forgefather's Seal](equipment/equipment-24-jpQcKMmP1I5674P7.htm)|Sceau du père des forges|`@Compendium[pf2e.equipment-srd.jpQcKMmP1I5674P7]`|
|[Formula Book (Blank)](equipment/equipment-00-qCEOZ6109Yo34tRx.htm)|Formulaire|`@Compendium[pf2e.equipment-srd.qCEOZ6109Yo34tRx]`|
|[Formulated Sunlight](equipment/consumable-00-AqslI71DReZOzgAW.htm)||`@Compendium[pf2e.equipment-srd.AqslI71DReZOzgAW]`|
|[Fortification](equipment/equipment-12-8buhFcGwuaJRrp0y.htm)|Fortification (rune)|`@Compendium[pf2e.equipment-srd.8buhFcGwuaJRrp0y]`|
|[Fortification (Greater)](equipment/equipment-18-ujWnpVMkbTljMGN9.htm)|Fortification (supérieure)|`@Compendium[pf2e.equipment-srd.ujWnpVMkbTljMGN9]`|
|[Fortifying Pebble](equipment/consumable-04-aQ4Q8Rq6ntWaHcIv.htm)|Caillou de fortification|`@Compendium[pf2e.equipment-srd.aQ4Q8Rq6ntWaHcIv]`|
|[Four-Ways Dogslicer](equipment/weapon-12-3oexArva2aEm69WV.htm)||`@Compendium[pf2e.equipment-srd.3oexArva2aEm69WV]`|
|[Freezing Ammunition](equipment/consumable-05-Xnmx6SJ6OkJOfYxF.htm)||`@Compendium[pf2e.equipment-srd.Xnmx6SJ6OkJOfYxF]`|
|[Frenzy Oil](equipment/consumable-16-7p3C9xf3XgUbvKHL.htm)||`@Compendium[pf2e.equipment-srd.7p3C9xf3XgUbvKHL]`|
|[Frost](equipment/equipment-08-M5M1WJ5KzbYfRGsY.htm)|Froid (rune)|`@Compendium[pf2e.equipment-srd.M5M1WJ5KzbYfRGsY]`|
|[Frost (Greater)](equipment/equipment-15-Sexud7FdxIrg50vU.htm)|Froid supérieure (rune)|`@Compendium[pf2e.equipment-srd.Sexud7FdxIrg50vU]`|
|[Frost Brand](equipment/weapon-16-87IvbaQnkUOBgdD0.htm)|Epée de givre|`@Compendium[pf2e.equipment-srd.87IvbaQnkUOBgdD0]`|
|[Frost Vial (Greater)](equipment/weapon-11-intVPcEzbE9o4NQd.htm)|Fiole givrée (supérieur)|`@Compendium[pf2e.equipment-srd.intVPcEzbE9o4NQd]`|
|[Frost Vial (Lesser)](equipment/weapon-01-EpxmUtLpCkE8R6KJ.htm)|Fiole givrée (inférieur)|`@Compendium[pf2e.equipment-srd.EpxmUtLpCkE8R6KJ]`|
|[Frost Vial (Major)](equipment/weapon-17-4DJQID8GIlxQ7b9C.htm)|Fiole givrée (majeur)|`@Compendium[pf2e.equipment-srd.4DJQID8GIlxQ7b9C]`|
|[Frost Vial (Moderate)](equipment/weapon-03-nVvH1ZcM7OwIVIs8.htm)|Fiole givrée (modéré)|`@Compendium[pf2e.equipment-srd.nVvH1ZcM7OwIVIs8]`|
|[Full Plate](equipment/armor-02-Gq1cZWSKOtJhKd2p.htm)|Harnois|`@Compendium[pf2e.equipment-srd.Gq1cZWSKOtJhKd2p]`|
|[Gaff](equipment/weapon-00-NApB2Fl7i8wzp7RX.htm)|Gaffe|`@Compendium[pf2e.equipment-srd.NApB2Fl7i8wzp7RX]`|
|[Gallows Tooth](equipment/consumable-08-u6g7AClRFEAj4lf4.htm)|Dent du gibet|`@Compendium[pf2e.equipment-srd.u6g7AClRFEAj4lf4]`|
|[Ganjay Book (1 bulk book)](equipment/equipment-01-2mt15LcfMArk7JqT.htm)||`@Compendium[pf2e.equipment-srd.2mt15LcfMArk7JqT]`|
|[Ganjay Book (Light bulk book)](equipment/equipment-01-yvS4UatIZAGH4tng.htm)||`@Compendium[pf2e.equipment-srd.yvS4UatIZAGH4tng]`|
|[Garnet](equipment/treasure-00-NuX85j3bYLYVcINr.htm)|Grenat|`@Compendium[pf2e.equipment-srd.NuX85j3bYLYVcINr]`|
|[Gauntlet](equipment/weapon-00-Ix2vicchE79d6Cl3.htm)|Gantelet|`@Compendium[pf2e.equipment-srd.Ix2vicchE79d6Cl3]`|
|[Gauntlight](equipment/equipment-20-Sd5tUgbqICK7MkTP.htm)||`@Compendium[pf2e.equipment-srd.Sd5tUgbqICK7MkTP]`|
|[Genius Diadem](equipment/equipment-18-QZaOQ8luuxWXpFqJ.htm)|Diadème du génie|`@Compendium[pf2e.equipment-srd.QZaOQ8luuxWXpFqJ]`|
|[Ghost Ammunition](equipment/consumable-14-Zjevw0WzUs1IIUAL.htm)|Munition fantôme|`@Compendium[pf2e.equipment-srd.Zjevw0WzUs1IIUAL]`|
|[Ghost Charge (Greater)](equipment/weapon-11-28osNZINXLWzqzUL.htm)|Charge fantômr (supérieure)|`@Compendium[pf2e.equipment-srd.28osNZINXLWzqzUL]`|
|[Ghost Charge (Lesser)](equipment/weapon-01-LNd5u19GqC51ngby.htm)|Charge fantôme (inférieure)|`@Compendium[pf2e.equipment-srd.LNd5u19GqC51ngby]`|
|[Ghost Charge (Major)](equipment/weapon-17-i7bJcib5TUJKOd4Z.htm)|Charge fantôme (majeure)|`@Compendium[pf2e.equipment-srd.i7bJcib5TUJKOd4Z]`|
|[Ghost Charge (Moderate)](equipment/weapon-03-Wf94e8YNhZdIvWc9.htm)|Charge fantôme (modérée)|`@Compendium[pf2e.equipment-srd.Wf94e8YNhZdIvWc9]`|
|[Ghost Dust](equipment/consumable-16-dmQAN56aMro0gecx.htm)|Poussière fantomatique|`@Compendium[pf2e.equipment-srd.dmQAN56aMro0gecx]`|
|[Ghost Ink](equipment/consumable-01-qnvq3PSTiejQTSi9.htm)|Encre sympathique|`@Compendium[pf2e.equipment-srd.qnvq3PSTiejQTSi9]`|
|[Ghost Touch](equipment/equipment-04-JQdwHECogcTzdd8R.htm)|Spectrale (rune)|`@Compendium[pf2e.equipment-srd.JQdwHECogcTzdd8R]`|
|[Ghoul Hide](equipment/armor-06-hRwnNTMj7wa8S4Ji.htm)|Peau de goule|`@Compendium[pf2e.equipment-srd.hRwnNTMj7wa8S4Ji]`|
|[Giant Centipede Venom](equipment/consumable-01-txmX5ghhPS72GKXy.htm)|Venin de mille-pattes géant|`@Compendium[pf2e.equipment-srd.txmX5ghhPS72GKXy]`|
|[Giant Scorpion Venom](equipment/consumable-06-QGXNqpP5KvSldoZz.htm)|Venin de scorpion géant|`@Compendium[pf2e.equipment-srd.QGXNqpP5KvSldoZz]`|
|[Giant Wasp Venom](equipment/consumable-07-aBOPYlfHAcXUmhF7.htm)|Venin de guèpe géante|`@Compendium[pf2e.equipment-srd.aBOPYlfHAcXUmhF7]`|
|[Gilded ceremonial armor](equipment/treasure-00-0b6BUtoFD4DotZHR.htm)|Armure cérémonielle dorée|`@Compendium[pf2e.equipment-srd.0b6BUtoFD4DotZHR]`|
|[Gilded scepter with sapphire](equipment/treasure-00-K44zDGRTSecRjEv0.htm)|Sceptre doré surmonté d'un saphir|`@Compendium[pf2e.equipment-srd.K44zDGRTSecRjEv0]`|
|[Glaive](equipment/weapon-00-vlnzTSsRmWeSkt9O.htm)|Coutille|`@Compendium[pf2e.equipment-srd.vlnzTSsRmWeSkt9O]`|
|[Glaive of the Artist](equipment/weapon-14-J5It6yq3ZkN059zP.htm)||`@Compendium[pf2e.equipment-srd.J5It6yq3ZkN059zP]`|
|[Glamered](equipment/equipment-05-iTxqImupNnm8gvoe.htm)|Mimétisme (rune)|`@Compendium[pf2e.equipment-srd.iTxqImupNnm8gvoe]`|
|[Glamorous Buckler](equipment/armor-02-IxuDS3POB6EH8TVN.htm)|Targe étincelante|`@Compendium[pf2e.equipment-srd.IxuDS3POB6EH8TVN]`|
|[Glass Cutter](equipment/equipment-00-wewWxypLaXcHjJ5P.htm)||`@Compendium[pf2e.equipment-srd.wewWxypLaXcHjJ5P]`|
|[Gloom Blade](equipment/weapon-09-w5ZX1R3dPvuLcuRx.htm)|Lame obscure|`@Compendium[pf2e.equipment-srd.w5ZX1R3dPvuLcuRx]`|
|[Glorious Plate](equipment/armor-14-tLNsmY90aN668PMG.htm)||`@Compendium[pf2e.equipment-srd.tLNsmY90aN668PMG]`|
|[Gloves of Carelessness](equipment/equipment-07-ftSaD4c5io4pP4OB.htm)|Gants de négligence|`@Compendium[pf2e.equipment-srd.ftSaD4c5io4pP4OB]`|
|[Gloves of Storing](equipment/equipment-07-ngz7dYysC1NkBBRK.htm)|Gants de rangement|`@Compendium[pf2e.equipment-srd.ngz7dYysC1NkBBRK]`|
|[Gluttonous Spear](equipment/weapon-06-2z4GSaKaVX22kzPU.htm)||`@Compendium[pf2e.equipment-srd.2z4GSaKaVX22kzPU]`|
|[Gnome Flickmace](equipment/weapon-00-oSQET5hKn9q4xlrl.htm)|Masse-yoyo gnome|`@Compendium[pf2e.equipment-srd.oSQET5hKn9q4xlrl]`|
|[Gnome Hooked Hammer](equipment/weapon-00-1U7Laa7Yt7i3G77L.htm)|Marteau-piolet gnome|`@Compendium[pf2e.equipment-srd.1U7Laa7Yt7i3G77L]`|
|[Goggles of Night](equipment/equipment-05-K7VHhUamFz3kTnm5.htm)|Lunettes de nyctalope|`@Compendium[pf2e.equipment-srd.K7VHhUamFz3kTnm5]`|
|[Goggles of Night (Greater)](equipment/equipment-11-Tej30MsQo8Ek9aA6.htm)|Lunettes de nyctalope (supérieures)|`@Compendium[pf2e.equipment-srd.Tej30MsQo8Ek9aA6]`|
|[Goggles of Night (Major)](equipment/equipment-18-rXXNw6dwVn96giDi.htm)|Lunettes de nyctalope (majeures)|`@Compendium[pf2e.equipment-srd.rXXNw6dwVn96giDi]`|
|[Gold Dragon's Breath Potion (Adult)](equipment/consumable-12-NqNwLeqto2isVWNL.htm)|Potion de souffle du dragon d'or adulte|`@Compendium[pf2e.equipment-srd.NqNwLeqto2isVWNL]`|
|[Gold Dragon's Breath Potion (Wyrm)](equipment/consumable-17-KZbAqbt7qxJPPkii.htm)|Potion de souffle de dracosire d'or|`@Compendium[pf2e.equipment-srd.KZbAqbt7qxJPPkii]`|
|[Gold Dragon's Breath Potion (Young)](equipment/consumable-07-3fSc2e5xRjJ7w4g3.htm)|Potion de souffle du jeune dragon d'or|`@Compendium[pf2e.equipment-srd.3fSc2e5xRjJ7w4g3]`|
|[Gold Pieces](equipment/treasure-00-B6B7tBWJSqOBz5zz.htm)|Pièces d'or|`@Compendium[pf2e.equipment-srd.B6B7tBWJSqOBz5zz]`|
|[Gold and aquamarine diadem](equipment/treasure-00-e6arpbDxMlmogfee.htm)|Diadème d'or serti d'aigue-marines|`@Compendium[pf2e.equipment-srd.e6arpbDxMlmogfee]`|
|[Gold and garnet ring](equipment/treasure-00-P7jMpOYmusKse1Yu.htm)|Anneau d'or avec un grenat|`@Compendium[pf2e.equipment-srd.P7jMpOYmusKse1Yu]`|
|[Gold and opal bracelet](equipment/treasure-00-x7rEpjFkUNTqmt1t.htm)|Bracelet en or avec des opales|`@Compendium[pf2e.equipment-srd.x7rEpjFkUNTqmt1t]`|
|[Gold chalice with black pearls](equipment/treasure-00-ony7u60cfBMtLG8F.htm)|Calice en or serti de perles noires|`@Compendium[pf2e.equipment-srd.ony7u60cfBMtLG8F]`|
|[Gold dragon statuette](equipment/treasure-00-2MPfYdRWfUNnt5JK.htm)|Statuette de dragon en or|`@Compendium[pf2e.equipment-srd.2MPfYdRWfUNnt5JK]`|
|[Gold mask of a high priest](equipment/treasure-00-tcP4ZKGZr5i4586v.htm)|Masque en or d'un grand prêtre|`@Compendium[pf2e.equipment-srd.tcP4ZKGZr5i4586v]`|
|[Gold necklace with peridots](equipment/treasure-5ix9inqdHLksqeoR.htm)|Collier en or serti de péridots|`@Compendium[pf2e.equipment-srd.5ix9inqdHLksqeoR]`|
|[Gold rapier with amethysts](equipment/treasure-00-9FsyNDq3VOsFumhz.htm)|rapière en or avec des améthystes|`@Compendium[pf2e.equipment-srd.9FsyNDq3VOsFumhz]`|
|[Gold urn with scenes of judgment](equipment/treasure-00-H8BXvZ1DUg3uJ53K.htm)|Urne doré portant des scènes de jugement|`@Compendium[pf2e.equipment-srd.H8BXvZ1DUg3uJ53K]`|
|[Golden Legion Epaulet](equipment/equipment-03-CDuN9kzyxBZ4cShq.htm)|Épaulette de la Légion dorée|`@Compendium[pf2e.equipment-srd.CDuN9kzyxBZ4cShq]`|
|[Golem Stylus](equipment/equipment-10-d8wTwHiA09HP45IM.htm)||`@Compendium[pf2e.equipment-srd.d8wTwHiA09HP45IM]`|
|[Gorget of the Primal Roar](equipment/equipment-11-WYg2dZ8UK1N3rpgs.htm)|Gorgerin de rugissement primal|`@Compendium[pf2e.equipment-srd.WYg2dZ8UK1N3rpgs]`|
|[Gorgon's Breath](equipment/consumable-13-QImdOdyolrMWwuxD.htm)||`@Compendium[pf2e.equipment-srd.QImdOdyolrMWwuxD]`|
|[Gourd Home](equipment/equipment-09-dJoYIbM6GF6wgX0b.htm)|Calebasse de camping|`@Compendium[pf2e.equipment-srd.dJoYIbM6GF6wgX0b]`|
|[Grail of Twisted Desires](equipment/equipment-10-3nv4JcajPAxDgGMb.htm)||`@Compendium[pf2e.equipment-srd.3nv4JcajPAxDgGMb]`|
|[Grappling Arrow](equipment/consumable-00-IAeGh5qvu7ob4yad.htm)|Flèche grappin|`@Compendium[pf2e.equipment-srd.IAeGh5qvu7ob4yad]`|
|[Grappling Bolt](equipment/consumable-00-sAI6yzB11LlxisD7.htm)|Flèche grappin|`@Compendium[pf2e.equipment-srd.sAI6yzB11LlxisD7]`|
|[Grappling Hook](equipment/equipment-00-6DCy7tEF1cxaIJMy.htm)|Grappin|`@Compendium[pf2e.equipment-srd.6DCy7tEF1cxaIJMy]`|
|[Grasping Snare](equipment/consumable-08-TFnr9Gq7VXPJu0GQ.htm)||`@Compendium[pf2e.equipment-srd.TFnr9Gq7VXPJu0GQ]`|
|[Grasping Tree](equipment/consumable-03-hxYxRz9nOECtLak5.htm)||`@Compendium[pf2e.equipment-srd.hxYxRz9nOECtLak5]`|
|[Graveroot](equipment/consumable-03-qQqAY59NgGNoy2xr.htm)|Racine des tombes|`@Compendium[pf2e.equipment-srd.qQqAY59NgGNoy2xr]`|
|[Greataxe](equipment/weapon-00-8COlYvHe6hKCXY8x.htm)|Grande hache|`@Compendium[pf2e.equipment-srd.8COlYvHe6hKCXY8x]`|
|[Greatclub](equipment/weapon-00-kdGqnqbrwPzQfTsm.htm)|Massue|`@Compendium[pf2e.equipment-srd.kdGqnqbrwPzQfTsm]`|
|[Greatpick](equipment/weapon-00-3Zv5hSXXtlaDatUv.htm)|Grand pic de guerre|`@Compendium[pf2e.equipment-srd.3Zv5hSXXtlaDatUv]`|
|[Greatsword](equipment/weapon-00-UX71GkWBL9g41VwM.htm)|Épée à deux mains|`@Compendium[pf2e.equipment-srd.UX71GkWBL9g41VwM]`|
|[Green Dragon's Breath Potion (Adult)](equipment/consumable-12-eV0owvv0lYQph0nn.htm)|Potion de souffle du dragon vert adulte|`@Compendium[pf2e.equipment-srd.eV0owvv0lYQph0nn]`|
|[Green Dragon's Breath Potion (Wyrm)](equipment/consumable-17-OJvCWeJPHEc7GpFG.htm)|Potion de souffle de dracosire vert|`@Compendium[pf2e.equipment-srd.OJvCWeJPHEc7GpFG]`|
|[Green Dragon's Breath Potion (Young)](equipment/consumable-07-vvgCQzWkScCzte7K.htm)|Potion de souffle du jeune dragon vert|`@Compendium[pf2e.equipment-srd.vvgCQzWkScCzte7K]`|
|[Green Wyrmling Breath Potion](equipment/consumable-05-GNy0YEusxjcGMCNn.htm)||`@Compendium[pf2e.equipment-srd.GNy0YEusxjcGMCNn]`|
|[Greengut](equipment/consumable-17-qvAtAv211cVqktgU.htm)||`@Compendium[pf2e.equipment-srd.qvAtAv211cVqktgU]`|
|[Grievous](equipment/equipment-09-qUnDHEXteUQGE8yp.htm)|Douloureuse (rune)|`@Compendium[pf2e.equipment-srd.qUnDHEXteUQGE8yp]`|
|[Grim Trophy](equipment/consumable-07-eEIWjTvZyKsKhYaz.htm)|Sinistre trophée|`@Compendium[pf2e.equipment-srd.eEIWjTvZyKsKhYaz]`|
|[Grinning Pugwampi](equipment/consumable-14-mJSCOIJllj2GtACC.htm)||`@Compendium[pf2e.equipment-srd.mJSCOIJllj2GtACC]`|
|[Guiding Chisel](equipment/equipment-16-TGCzwDIrAqo3HUhr.htm)|Ciseau d'orientation|`@Compendium[pf2e.equipment-srd.TGCzwDIrAqo3HUhr]`|
|[Guisarme](equipment/weapon-00-TT0pJz9odKApfq6D.htm)|Guisarme|`@Compendium[pf2e.equipment-srd.TT0pJz9odKApfq6D]`|
|[Hail of Arrows Snare](equipment/consumable-16-Odl2SyKw8Zg6ckKb.htm)||`@Compendium[pf2e.equipment-srd.Odl2SyKw8Zg6ckKb]`|
|[Halberd](equipment/weapon-00-dgWxsYm0DWHb27h6.htm)|Hallebarde|`@Compendium[pf2e.equipment-srd.dgWxsYm0DWHb27h6]`|
|[Half Plate](equipment/armor-01-pRoikbRo5HFW6YUB.htm)|Armure de plates|`@Compendium[pf2e.equipment-srd.pRoikbRo5HFW6YUB]`|
|[Halfling Sling Staff](equipment/weapon-00-DV4qelKHrviM0O5i.htm)|Fustibale de halfelin|`@Compendium[pf2e.equipment-srd.DV4qelKHrviM0O5i]`|
|[Hammer](equipment/equipment-00-ML0GR2cIEdMn2hoD.htm)|Marteau|`@Compendium[pf2e.equipment-srd.ML0GR2cIEdMn2hoD]`|
|[Hampering Snare](equipment/consumable-01-Km4lSOsyrip5q6iD.htm)|Piège artisanal ralentisseur|`@Compendium[pf2e.equipment-srd.Km4lSOsyrip5q6iD]`|
|[Hand Crossbow](equipment/weapon-00-XyA6PKV46aNlLXOd.htm)|Arbalète de poing|`@Compendium[pf2e.equipment-srd.XyA6PKV46aNlLXOd]`|
|[Hand mirror with decorated frame](equipment/treasure-00-JP7G2gOS3OuMNzeE.htm)|Miroir à main au cadre décoré|`@Compendium[pf2e.equipment-srd.JP7G2gOS3OuMNzeE]`|
|[Hand of the Mage](equipment/equipment-02-r1hgg2rweqGL1LBl.htm)|Main du mage|`@Compendium[pf2e.equipment-srd.r1hgg2rweqGL1LBl]`|
|[Handcuffs (Average)](equipment/equipment-03-wBeljBAAFsvgkRMy.htm)||`@Compendium[pf2e.equipment-srd.wBeljBAAFsvgkRMy]`|
|[Handcuffs (Good)](equipment/equipment-09-WR8GOQSx7B5AlzNu.htm)||`@Compendium[pf2e.equipment-srd.WR8GOQSx7B5AlzNu]`|
|[Handcuffs (Superior)](equipment/equipment-17-XXALNl2JjJket1vr.htm)||`@Compendium[pf2e.equipment-srd.XXALNl2JjJket1vr]`|
|[Handwraps of Mighty Blows](equipment/weapon-02-FNDq4NFSN0g2HKWO.htm)|Bandelettes de coups puissants (+1)|`@Compendium[pf2e.equipment-srd.FNDq4NFSN0g2HKWO]`|
|[Hardened Harrow Deck](equipment/weapon-14-qtlkftQXfO4wuCVu.htm)||`@Compendium[pf2e.equipment-srd.qtlkftQXfO4wuCVu]`|
|[Harrow Carrying Case](equipment/equipment-00-P4hCmpeuBochkxsJ.htm)||`@Compendium[pf2e.equipment-srd.P4hCmpeuBochkxsJ]`|
|[Harrow Deck (Common)](equipment/equipment-00-B3aJa0csNZOGXLXT.htm)||`@Compendium[pf2e.equipment-srd.B3aJa0csNZOGXLXT]`|
|[Harrow Deck (Fine)](equipment/equipment-00-OP1TkZ9ugn86W4Br.htm)||`@Compendium[pf2e.equipment-srd.OP1TkZ9ugn86W4Br]`|
|[Harrow Deck (Simple)](equipment/equipment-00-C1j0Zs26TPVjplbs.htm)||`@Compendium[pf2e.equipment-srd.C1j0Zs26TPVjplbs]`|
|[Harrow Mat](equipment/equipment-00-Ze64YjceQqnwRCsU.htm)||`@Compendium[pf2e.equipment-srd.Ze64YjceQqnwRCsU]`|
|[Hat of Disguise](equipment/equipment-02-fvpLYx1Lo42cdleQ.htm)|Couvre-chef de déguisement|`@Compendium[pf2e.equipment-srd.fvpLYx1Lo42cdleQ]`|
|[Hat of Disguise (Greater)](equipment/equipment-07-WPSp5MLb0VOfmUqH.htm)|Couvre-chef de déguisement (supérieur)|`@Compendium[pf2e.equipment-srd.WPSp5MLb0VOfmUqH]`|
|[Hat of the Magi](equipment/equipment-03-SswqJqeAWGtX3tTF.htm)|Couvre-chef de thaumaturge|`@Compendium[pf2e.equipment-srd.SswqJqeAWGtX3tTF]`|
|[Hat of the Magi (Greater)](equipment/equipment-09-Hd1AfC08ytBg67Ey.htm)|Couvre-chef de thaumaturge (supérieur)|`@Compendium[pf2e.equipment-srd.Hd1AfC08ytBg67Ey]`|
|[Hatchet](equipment/weapon-00-LGgvev6AV0So8tP9.htm)|Hachette|`@Compendium[pf2e.equipment-srd.LGgvev6AV0So8tP9]`|
|[Headband of Inspired Wisdom](equipment/equipment-17-hjfoSyfsGSTLpPMr.htm)|Bandreau d'inspiration|`@Compendium[pf2e.equipment-srd.hjfoSyfsGSTLpPMr]`|
|[Healer's Gloves](equipment/equipment-04-o1zKhvYUUc1hE2AE.htm)|Gants du guérisseur|`@Compendium[pf2e.equipment-srd.o1zKhvYUUc1hE2AE]`|
|[Healer's Gloves (Greater)](equipment/equipment-09-0GOHTXBxs6H6ARBz.htm)|Gants de guérisseur (supérieurs)|`@Compendium[pf2e.equipment-srd.0GOHTXBxs6H6ARBz]`|
|[Healer's Tools](equipment/equipment-00-s1vB3HdXjMigYAnY.htm)|Outils de guérisseur|`@Compendium[pf2e.equipment-srd.s1vB3HdXjMigYAnY]`|
|[Healer's Tools (Expanded)](equipment/equipment-03-SGkOHFyBbzWdBk8D.htm)||`@Compendium[pf2e.equipment-srd.SGkOHFyBbzWdBk8D]`|
|[Healing Potion (Greater)](equipment/consumable-12-TSrbwHW8zm0mhkwb.htm)|Potion de guérison (supérieur)|`@Compendium[pf2e.equipment-srd.TSrbwHW8zm0mhkwb]`|
|[Healing Potion (Lesser)](equipment/consumable-03-e0vSAQfxhHauiAoD.htm)|Potion de guérison (inférieur)|`@Compendium[pf2e.equipment-srd.e0vSAQfxhHauiAoD]`|
|[Healing Potion (Major)](equipment/consumable-18-p3ppzFSsZXFRe3H8.htm)|Potion de guérison (majeure)|`@Compendium[pf2e.equipment-srd.p3ppzFSsZXFRe3H8]`|
|[Healing Potion (Minor)](equipment/consumable-01-2RuepCemJhrpKKao.htm)|Potion de guérison (Mineure)|`@Compendium[pf2e.equipment-srd.2RuepCemJhrpKKao]`|
|[Healing Potion (Moderate)](equipment/consumable-06-G7haQ5gDt30ftJLC.htm)|Potion de guérison (modéré)|`@Compendium[pf2e.equipment-srd.G7haQ5gDt30ftJLC]`|
|[Heartstone](equipment/equipment-09-yhw1mr4oDhzMQc3D.htm)|Cardioline|`@Compendium[pf2e.equipment-srd.yhw1mr4oDhzMQc3D]`|
|[Heavy Crossbow](equipment/weapon-00-giO4LwIKGzJZoaxa.htm)|Arbalète lourde|`@Compendium[pf2e.equipment-srd.giO4LwIKGzJZoaxa]`|
|[Heedless Spurs](equipment/equipment-15-LfiGXU03Khb1o6fs.htm)||`@Compendium[pf2e.equipment-srd.LfiGXU03Khb1o6fs]`|
|[Hellfire Boots](equipment/equipment-13-vBgElXswf6xBx4k9.htm)||`@Compendium[pf2e.equipment-srd.vBgElXswf6xBx4k9]`|
|[Hellknight Plate](equipment/armor-02-lCgXeV52eYYl5cUX.htm)|Armure de Chevalier infernal|`@Compendium[pf2e.equipment-srd.lCgXeV52eYYl5cUX]`|
|[Hematite](equipment/treasure-00-02sF1QlxEx6H6zEP.htm)|Hématite|`@Compendium[pf2e.equipment-srd.02sF1QlxEx6H6zEP]`|
|[Hemlock](equipment/consumable-17-MP7updiiSct04vno.htm)|Cigüe|`@Compendium[pf2e.equipment-srd.MP7updiiSct04vno]`|
|[Hide](equipment/armor-00-AnwzlOs0njF9Jqnr.htm)|Armure de peau|`@Compendium[pf2e.equipment-srd.AnwzlOs0njF9Jqnr]`|
|[Hobbling Snare](equipment/consumable-04-0lhh2l4kh3QrwYH9.htm)||`@Compendium[pf2e.equipment-srd.0lhh2l4kh3QrwYH9]`|
|[Holly and Mistletoe](equipment/equipment-00-QbOlqr4lSkeOEfty.htm)|Houx et gui|`@Compendium[pf2e.equipment-srd.QbOlqr4lSkeOEfty]`|
|[Holy](equipment/equipment-11-DH0kB9Wbr5pDeunX.htm)||`@Compendium[pf2e.equipment-srd.DH0kB9Wbr5pDeunX]`|
|[Holy Avenger](equipment/weapon-14-OIs6WPuCRh2UJTOe.htm)|Épée de la justice|`@Compendium[pf2e.equipment-srd.OIs6WPuCRh2UJTOe]`|
|[Holy Prayer Beads](equipment/equipment-05-KrmSuQIyu6OEi5ew.htm)|Chapelet sacré|`@Compendium[pf2e.equipment-srd.KrmSuQIyu6OEi5ew]`|
|[Holy Prayer Beads (Greater)](equipment/equipment-11-OHs7L8CJMdp2UV0P.htm)|Chapelet sacré (supérieur)|`@Compendium[pf2e.equipment-srd.OHs7L8CJMdp2UV0P]`|
|[Holy Water](equipment/consumable-01-efFe4EK7ThUrH446.htm)|Eau bénite|`@Compendium[pf2e.equipment-srd.efFe4EK7ThUrH446]`|
|[Homeward Wayfinder](equipment/equipment-13-sRpA9sGZe1P0EERB.htm)||`@Compendium[pf2e.equipment-srd.sRpA9sGZe1P0EERB]`|
|[Honeyscent](equipment/consumable-10-7TT0XIb8ovLFOP2e.htm)||`@Compendium[pf2e.equipment-srd.7TT0XIb8ovLFOP2e]`|
|[Horn of Blasting](equipment/equipment-09-WAMDYE9tt3W8obzr.htm)|Cor de dévastation|`@Compendium[pf2e.equipment-srd.WAMDYE9tt3W8obzr]`|
|[Horn of Fog](equipment/equipment-06-hcGvN03ieNWlSQYa.htm)|Corne de brume|`@Compendium[pf2e.equipment-srd.hcGvN03ieNWlSQYa]`|
|[Horns of Naraga](equipment/equipment-26-SHzZSENlkz3Qy46j.htm)|Cornes de Nagara|`@Compendium[pf2e.equipment-srd.SHzZSENlkz3Qy46j]`|
|[Horsechopper](equipment/weapon-00-YnwEttFE1bJacLDh.htm)|Coupecheval|`@Compendium[pf2e.equipment-srd.YnwEttFE1bJacLDh]`|
|[Horseshoes of Speed](equipment/equipment-07-peoheZMxdPHUNo93.htm)|Fers à cheval de vitesse|`@Compendium[pf2e.equipment-srd.peoheZMxdPHUNo93]`|
|[Horseshoes of Speed (Greater)](equipment/equipment-14-t0pzdb54KD2JQcEa.htm)|Fers à cheval de vitesse (supérieurs)|`@Compendium[pf2e.equipment-srd.t0pzdb54KD2JQcEa]`|
|[Hourglass](equipment/equipment-00-WAsQ6WuMYUB77Uh0.htm)|Sablier|`@Compendium[pf2e.equipment-srd.WAsQ6WuMYUB77Uh0]`|
|[Hummingbird Wayfinder](equipment/equipment-12-o5jn0wvPaSh65p3J.htm)||`@Compendium[pf2e.equipment-srd.o5jn0wvPaSh65p3J]`|
|[Hunger Oil](equipment/consumable-11-8GEsRsQAdDljRT1s.htm)||`@Compendium[pf2e.equipment-srd.8GEsRsQAdDljRT1s]`|
|[Hunter's Arrowhead](equipment/equipment-04-JCGsj7iY88o9uTP5.htm)|Pointe de flèche du chasseur|`@Compendium[pf2e.equipment-srd.JCGsj7iY88o9uTP5]`|
|[Hunter's Bane](equipment/consumable-02-Yew9oddFsH0KeDLh.htm)|Fléau du chasseur|`@Compendium[pf2e.equipment-srd.Yew9oddFsH0KeDLh]`|
|[Hunter's Bow](equipment/weapon-03-RapaDb10oJZOjujo.htm)||`@Compendium[pf2e.equipment-srd.RapaDb10oJZOjujo]`|
|[Hunter's Dawn](equipment/weapon-23-2jtZPnHF1M8vKWry.htm)||`@Compendium[pf2e.equipment-srd.2jtZPnHF1M8vKWry]`|
|[Hunting Spider Venom](equipment/consumable-05-5RGNjhDxZ0yMhTds.htm)|Venin d'araignée chasseresse|`@Compendium[pf2e.equipment-srd.5RGNjhDxZ0yMhTds]`|
|[Hype](equipment/consumable-08-z3IG6t2o0zsfP2XQ.htm)||`@Compendium[pf2e.equipment-srd.z3IG6t2o0zsfP2XQ]`|
|[Illuminated manuscript](equipment/treasure-00-l8dFZIYYngDOIYHb.htm)|Manuscrit enluminé|`@Compendium[pf2e.equipment-srd.l8dFZIYYngDOIYHb]`|
|[Illustrated book](equipment/treasure-00-G5WuYX1ghrZcJ1J1.htm)|Livre illustré|`@Compendium[pf2e.equipment-srd.G5WuYX1ghrZcJ1J1]`|
|[Immovable Rod](equipment/equipment-09-14LrP7bx8Q1jimHO.htm)|Sceptre inamovible|`@Compendium[pf2e.equipment-srd.14LrP7bx8Q1jimHO]`|
|[Impenetrable Scale](equipment/armor-17-aLA7pikfeNIAAGLw.htm)|Armure impénétrable|`@Compendium[pf2e.equipment-srd.aLA7pikfeNIAAGLw]`|
|[Incense of Distilled Death](equipment/consumable-12-4RKfLoqVluZGWzLc.htm)||`@Compendium[pf2e.equipment-srd.4RKfLoqVluZGWzLc]`|
|[Indestructible Shield](equipment/armor-18-SUbYk6B1iPoGyyjh.htm)|Bouclier indestructible|`@Compendium[pf2e.equipment-srd.SUbYk6B1iPoGyyjh]`|
|[Inexplicable Apparatus](equipment/equipment-18-V9iVPhIk980GT6A2.htm)|Appareil inexplicable|`@Compendium[pf2e.equipment-srd.V9iVPhIk980GT6A2]`|
|[Infernal Health](equipment/equipment-15-nROnhUnxhbPW5tkG.htm)||`@Compendium[pf2e.equipment-srd.nROnhUnxhbPW5tkG]`|
|[Infiltrator's Accessory](equipment/weapon-05-AYdpUABAZeZnSA7s.htm)||`@Compendium[pf2e.equipment-srd.AYdpUABAZeZnSA7s]`|
|[Infiltrator's Elixir](equipment/consumable-02-cuomhpenkqGM5lLG.htm)|Elixir d'infiltration|`@Compendium[pf2e.equipment-srd.cuomhpenkqGM5lLG]`|
|[Inscribed crocodile skull](equipment/treasure-00-K7KzNh3YsXBrDlyj.htm)|Crâne de crcodile inscrit|`@Compendium[pf2e.equipment-srd.K7KzNh3YsXBrDlyj]`|
|[Insistent Door Knocker](equipment/equipment-06-I95zlGUDCply1Ydm.htm)||`@Compendium[pf2e.equipment-srd.I95zlGUDCply1Ydm]`|
|[Insistent Door Knocker (Greater)](equipment/equipment-11-a5dEzZPuxsmTvlWS.htm)||`@Compendium[pf2e.equipment-srd.a5dEzZPuxsmTvlWS]`|
|[Insistent Door Knocker (Major)](equipment/equipment-17-3BTIKYHck3JIAPiH.htm)||`@Compendium[pf2e.equipment-srd.3BTIKYHck3JIAPiH]`|
|[Inspiring Spotlight](equipment/equipment-09-M1fRjN7E3P55zvmc.htm)||`@Compendium[pf2e.equipment-srd.M1fRjN7E3P55zvmc]`|
|[Instant Evisceration Snare](equipment/consumable-20-xqCz2vStMNJujfpp.htm)|Piège artisanal d'éviscération instantanée|`@Compendium[pf2e.equipment-srd.xqCz2vStMNJujfpp]`|
|[Instant Fortress](equipment/equipment-16-EXXqJp8rU6HR5Ufg.htm)|Forteresse instantanée|`@Compendium[pf2e.equipment-srd.EXXqJp8rU6HR5Ufg]`|
|[Intricate silver and gold music box](equipment/treasure-00-NLcnptEjjiY9teq7.htm)|Boîte à musique chargée d'or et d'argent|`@Compendium[pf2e.equipment-srd.NLcnptEjjiY9teq7]`|
|[Invisibility](equipment/equipment-08-VDudQ4x2ozosAbTb.htm)|Invisibilité (rune)|`@Compendium[pf2e.equipment-srd.VDudQ4x2ozosAbTb]`|
|[Invisibility (Greater)](equipment/equipment-10-bxz885LMjLCkpDq3.htm)|Invisibilité supérieure (rune)|`@Compendium[pf2e.equipment-srd.bxz885LMjLCkpDq3]`|
|[Invisibility Potion](equipment/consumable-04-bikFUFRLwfdvX2x2.htm)|Potion d'invisibilité|`@Compendium[pf2e.equipment-srd.bikFUFRLwfdvX2x2]`|
|[Invisible Net](equipment/consumable-07-LBhzIWBH9TJ8A2K5.htm)||`@Compendium[pf2e.equipment-srd.LBhzIWBH9TJ8A2K5]`|
|[Iron Cube](equipment/consumable-06-StsE5POM7OSE36Ia.htm)|Cube de fer|`@Compendium[pf2e.equipment-srd.StsE5POM7OSE36Ia]`|
|[Iron Cudgel](equipment/consumable-14-XvmYNnQ4vg8GREH7.htm)|Gourdin de fer|`@Compendium[pf2e.equipment-srd.XvmYNnQ4vg8GREH7]`|
|[Iron Equalizer](equipment/consumable-12-KrtZmC3qfZIGXu76.htm)|Fer d'équilibrage|`@Compendium[pf2e.equipment-srd.KrtZmC3qfZIGXu76]`|
|[Iron Medallion](equipment/consumable-10-ewzJzyJ4Vo9lZKvp.htm)|Médaillon de fer|`@Compendium[pf2e.equipment-srd.ewzJzyJ4Vo9lZKvp]`|
|[Iron and rock crystal brazier](equipment/treasure-00-YZsZ6stG0i9GBCZw.htm)|Brasero en fer et en cristal de roche|`@Compendium[pf2e.equipment-srd.YZsZ6stG0i9GBCZw]`|
|[Iron cauldron with gargoyle faces](equipment/treasure-00-YeMi3otmwQ34W2we.htm)|Chaudron en fer avec des visages de gargouille|`@Compendium[pf2e.equipment-srd.YeMi3otmwQ34W2we]`|
|[Isolation Draught](equipment/consumable-07-VwzIiWAtbJCgGOEz.htm)||`@Compendium[pf2e.equipment-srd.VwzIiWAtbJCgGOEz]`|
|[Ivory](equipment/treasure-00-2PXwkWD3ymM7s0Ul.htm)|Ivoire|`@Compendium[pf2e.equipment-srd.2PXwkWD3ymM7s0Ul]`|
|[Jade](equipment/treasure-00-LyQQvO7COYAvFRNw.htm)|Jade|`@Compendium[pf2e.equipment-srd.LyQQvO7COYAvFRNw]`|
|[Jade Bauble](equipment/consumable-08-cKqZe5imzxqSnzwD.htm)|Babiole de jade|`@Compendium[pf2e.equipment-srd.cKqZe5imzxqSnzwD]`|
|[Jade Cat](equipment/consumable-02-2MgFoNXTccL8Own9.htm)|Chat de jade|`@Compendium[pf2e.equipment-srd.2MgFoNXTccL8Own9]`|
|[Jasper](equipment/treasure-00-j4DU62QY4lJ4WEPQ.htm)|Jaspe|`@Compendium[pf2e.equipment-srd.j4DU62QY4lJ4WEPQ]`|
|[Javelin](equipment/weapon-00-JNt7GmLCCVz5BiEI.htm)|Javeline|`@Compendium[pf2e.equipment-srd.JNt7GmLCCVz5BiEI]`|
|[Javelin of Lightning](equipment/consumable-09-wDhqRxuXPQfyD0eX.htm)|Javeline de foudre|`@Compendium[pf2e.equipment-srd.wDhqRxuXPQfyD0eX]`|
|[Jawbreaker Shield](equipment/armor-12-QJNyQlmPNzIrnuLf.htm)||`@Compendium[pf2e.equipment-srd.QJNyQlmPNzIrnuLf]`|
|[Jellyfish Lamp](equipment/equipment-00-3MGQ8KxH7HNA8THZ.htm)|Méduse lumineuse|`@Compendium[pf2e.equipment-srd.3MGQ8KxH7HNA8THZ]`|
|[Jerkin of Liberation](equipment/armor-17-COsToVFlA36UmkB7.htm)||`@Compendium[pf2e.equipment-srd.COsToVFlA36UmkB7]`|
|[Jet](equipment/treasure-00-CRTNNgrmssD6bQqQ.htm)|Jais|`@Compendium[pf2e.equipment-srd.CRTNNgrmssD6bQqQ]`|
|[Jet and white gold game set](equipment/treasure-00-DWzoqVeEamX118Qk.htm)|Pièces de jeu en or blanc et en jais|`@Compendium[pf2e.equipment-srd.DWzoqVeEamX118Qk]`|
|[Jeweled gold puzzle box](equipment/treasure-00-yrVJKmpBS0mqFuO1.htm)|Boîte de puzzle en or ornée de gemmes|`@Compendium[pf2e.equipment-srd.yrVJKmpBS0mqFuO1]`|
|[Jeweled mithral crown](equipment/treasure-00-BlqDXtRZcevCYRNz.htm)|Couronne de mithril avec des joyaux|`@Compendium[pf2e.equipment-srd.BlqDXtRZcevCYRNz]`|
|[Jeweled orrery of the planes](equipment/treasure-00-hEBNs4mYaVWz3UsX.htm)|Planétaire des plans composé de gemmes|`@Compendium[pf2e.equipment-srd.hEBNs4mYaVWz3UsX]`|
|[Jewel‑encrusted gold altar](equipment/treasure-zRsMLEqh2t2hqSw4.htm)|Autel en or incrusté de gemmes|`@Compendium[pf2e.equipment-srd.zRsMLEqh2t2hqSw4]`|
|[Juggernaut Mutagen (Greater)](equipment/consumable-11-TMtQnY6yvIRCpK9v.htm)||`@Compendium[pf2e.equipment-srd.TMtQnY6yvIRCpK9v]`|
|[Juggernaut Mutagen (Lesser)](equipment/consumable-01-xE0EdDrf734l2fQH.htm)|Mutagène de Juggernaut (inférieur)|`@Compendium[pf2e.equipment-srd.xE0EdDrf734l2fQH]`|
|[Juggernaut Mutagen (Major)](equipment/consumable-17-1kNp6yOS0aZPBPzZ.htm)||`@Compendium[pf2e.equipment-srd.1kNp6yOS0aZPBPzZ]`|
|[Juggernaut Mutagen (Moderate)](equipment/consumable-03-mj9i9GeQTADByNPZ.htm)|Mutagène de juggernaut|`@Compendium[pf2e.equipment-srd.mj9i9GeQTADByNPZ]`|
|[Juggling Club](equipment/weapon-00-RhfhVSfiH23v4U7k.htm)|Massue de jongleur|`@Compendium[pf2e.equipment-srd.RhfhVSfiH23v4U7k]`|
|[Kama](equipment/weapon-00-zSAoyzcf3nSeZCiF.htm)|Kama|`@Compendium[pf2e.equipment-srd.zSAoyzcf3nSeZCiF]`|
|[Katana](equipment/weapon-00-Y1dkRsRd1Z7Jf2y6.htm)|Katana|`@Compendium[pf2e.equipment-srd.Y1dkRsRd1Z7Jf2y6]`|
|[Katar](equipment/weapon-00-Fg3GkCDkszj5WtgQ.htm)|Katar|`@Compendium[pf2e.equipment-srd.Fg3GkCDkszj5WtgQ]`|
|[Keen](equipment/equipment-13-hg3IogR8ue2IWwgS.htm)|Acérée (rune)|`@Compendium[pf2e.equipment-srd.hg3IogR8ue2IWwgS]`|
|[Khakkara](equipment/weapon-00-KgKSX95uVSg8iBBA.htm)|Khakkara|`@Compendium[pf2e.equipment-srd.KgKSX95uVSg8iBBA]`|
|[Khopesh](equipment/weapon-00-YLb6XnT7OxAVGL5m.htm)|Khopesh|`@Compendium[pf2e.equipment-srd.YLb6XnT7OxAVGL5m]`|
|[Kin-Warding](equipment/equipment-03-7vwcuBIe4BNS5uuE.htm)||`@Compendium[pf2e.equipment-srd.7vwcuBIe4BNS5uuE]`|
|[King's Sleep](equipment/consumable-18-RkI3jKKrCozvEvfr.htm)|Sommeil du roi|`@Compendium[pf2e.equipment-srd.RkI3jKKrCozvEvfr]`|
|[Knapsack of Halflingkind](equipment/backpack-09-rdF2RKgFK0vOlaeV.htm)|Besace halfeline|`@Compendium[pf2e.equipment-srd.rdF2RKgFK0vOlaeV]`|
|[Knapsack of Halflingkind (Greater)](equipment/backpack-13-9cK0QVSjNd8tQtiQ.htm)|Besace Halfeline (supérieure)|`@Compendium[pf2e.equipment-srd.9cK0QVSjNd8tQtiQ]`|
|[Knockout Dram](equipment/consumable-07-0XSl2DU7JvKXOqTo.htm)||`@Compendium[pf2e.equipment-srd.0XSl2DU7JvKXOqTo]`|
|[Kortos Diamond](equipment/weapon-22-Vz7nITDWjrXmyoJn.htm)||`@Compendium[pf2e.equipment-srd.Vz7nITDWjrXmyoJn]`|
|[Kukri](equipment/weapon-00-TDrO7Xdyn7juFy3c.htm)|Kukri|`@Compendium[pf2e.equipment-srd.TDrO7Xdyn7juFy3c]`|
|[Ladder (10-foot)](equipment/equipment-00-5j5KyZsGOfbrJNUZ.htm)|Échelle (3 m)|`@Compendium[pf2e.equipment-srd.5j5KyZsGOfbrJNUZ]`|
|[Lance](equipment/weapon-00-OPMzQdM9rZznMGhW.htm)|Lance d'arçon|`@Compendium[pf2e.equipment-srd.OPMzQdM9rZznMGhW]`|
|[Lantern (Bull's Eye)](equipment/equipment-00-QrNvP9SgnK9DrerA.htm)|Lanterne sourde|`@Compendium[pf2e.equipment-srd.QrNvP9SgnK9DrerA]`|
|[Lantern (Hooded)](equipment/equipment-00-dIRZ0LL7G31fJNYz.htm)|Lanterne à capote|`@Compendium[pf2e.equipment-srd.dIRZ0LL7G31fJNYz]`|
|[Lantern of Empty Light](equipment/equipment-06-j7ITLMfZRhXFkpIh.htm)||`@Compendium[pf2e.equipment-srd.j7ITLMfZRhXFkpIh]`|
|[Lapis lazuli](equipment/treasure-DE4880DRtsRqax1N.htm)|Lapis lazuli|`@Compendium[pf2e.equipment-srd.DE4880DRtsRqax1N]`|
|[Lapis lazuli pendant](equipment/treasure-00-rjHqUOFqUeyTQyin.htm)|Pendentif en lapis lazuli|`@Compendium[pf2e.equipment-srd.rjHqUOFqUeyTQyin]`|
|[Lastwall Soup](equipment/consumable-01-VeCNWhvEr82ZNoSV.htm)|Soupe de Dernier-Rempart|`@Compendium[pf2e.equipment-srd.VeCNWhvEr82ZNoSV]`|
|[Leadenleg](equipment/consumable-04-QJ1fTrX42PoEWpK5.htm)||`@Compendium[pf2e.equipment-srd.QJ1fTrX42PoEWpK5]`|
|[Leaper's Elixir (Greater)](equipment/consumable-07-6eQvHNHf1IC2X5Rx.htm)|Elixir du sauteur (supérieur)|`@Compendium[pf2e.equipment-srd.6eQvHNHf1IC2X5Rx]`|
|[Leaper's Elixir (Lesser)](equipment/consumable-01-uG3xtNrs26scOVgW.htm)|Élixir du sauteur (inférieur)|`@Compendium[pf2e.equipment-srd.uG3xtNrs26scOVgW]`|
|[Leather Armor](equipment/armor-00-4tIVTg9wj56RrveA.htm)|Armure de cuir|`@Compendium[pf2e.equipment-srd.4tIVTg9wj56RrveA]`|
|[Leather flagon with Caydenite symbol](equipment/treasure-00-dloHmPtGDr8zSwJu.htm)|Gourde en cuir avec un symbole de Cayden|`@Compendium[pf2e.equipment-srd.dloHmPtGDr8zSwJu]`|
|[Lethargy Poison](equipment/consumable-02-zo0ophqfKunJFxZN.htm)|Poison de léthargie|`@Compendium[pf2e.equipment-srd.zo0ophqfKunJFxZN]`|
|[Liar's Demise](equipment/consumable-14-vAEQsYQbOYmqfIc0.htm)||`@Compendium[pf2e.equipment-srd.vAEQsYQbOYmqfIc0]`|
|[Lich Dust](equipment/consumable-09-ca2lzxfJxvuLDrKu.htm)|Poussière de liche|`@Compendium[pf2e.equipment-srd.ca2lzxfJxvuLDrKu]`|
|[Lich Phylactery](equipment/equipment-12-NEbr7nKuiluJgBHT.htm)||`@Compendium[pf2e.equipment-srd.NEbr7nKuiluJgBHT]`|
|[Life's Last Breath](equipment/weapon-18-1gi4mdZYNgPrzWHc.htm)||`@Compendium[pf2e.equipment-srd.1gi4mdZYNgPrzWHc]`|
|[Lifeblight Residue](equipment/consumable-15-cSrCTi2zE5OU5ylH.htm)||`@Compendium[pf2e.equipment-srd.cSrCTi2zE5OU5ylH]`|
|[Life‑size sculpture by an expert](equipment/treasure-zdiHlKtd4Fy1aIPC.htm)|Sculpture à l'échelle (par un expert)|`@Compendium[pf2e.equipment-srd.zdiHlKtd4Fy1aIPC]`|
|[Lifting Belt](equipment/equipment-04-g2oaOGSpttfH1q6W.htm)|Ceinture de levée|`@Compendium[pf2e.equipment-srd.g2oaOGSpttfH1q6W]`|
|[Light Hammer](equipment/weapon-00-FibwLZ12EIEwLGhw.htm)|Marteau léger|`@Compendium[pf2e.equipment-srd.FibwLZ12EIEwLGhw]`|
|[Light Mace](equipment/weapon-00-x1TOpwH755Ami5bC.htm)|Masse d'armes légère|`@Compendium[pf2e.equipment-srd.x1TOpwH755Ami5bC]`|
|[Light Pick](equipment/weapon-00-RbM4HvPyrZ4YJxRc.htm)|Pic de guerre léger|`@Compendium[pf2e.equipment-srd.RbM4HvPyrZ4YJxRc]`|
|[Lion's Shield](equipment/armor-06-eMDt5vCQztp7cC6B.htm)|Bouclier du lion|`@Compendium[pf2e.equipment-srd.eMDt5vCQztp7cC6B]`|
|[Living flame shaped into a phoenix](equipment/treasure-00-PzWETSKeVKisKCyl.htm)|Flamme vivante ayant la forme d'un phénix|`@Compendium[pf2e.equipment-srd.PzWETSKeVKisKCyl]`|
|[Lock (Average)](equipment/equipment-03-q2TYVAMaK6UfenbV.htm)|Serrure (Intermédiaire)|`@Compendium[pf2e.equipment-srd.q2TYVAMaK6UfenbV]`|
|[Lock (Good)](equipment/equipment-09-byKAxLyN7AXySmw7.htm)|Serrure (Bonne)|`@Compendium[pf2e.equipment-srd.byKAxLyN7AXySmw7]`|
|[Lock (Poor)](equipment/equipment-00-PBlxTrEyr3qXFBqq.htm)|Serrure (médiocre)|`@Compendium[pf2e.equipment-srd.PBlxTrEyr3qXFBqq]`|
|[Lock (Simple)](equipment/equipment-01-JZXCTwoIWoGKjMX6.htm)|Serrure (simple)|`@Compendium[pf2e.equipment-srd.JZXCTwoIWoGKjMX6]`|
|[Lock (Superior)](equipment/equipment-17-8xWA0demi2yQsl3C.htm)|Serrure (Supérieure)|`@Compendium[pf2e.equipment-srd.8xWA0demi2yQsl3C]`|
|[Locket of Sealed Nightmares](equipment/equipment-19-0NZuvpCVr21WcQtH.htm)||`@Compendium[pf2e.equipment-srd.0NZuvpCVr21WcQtH]`|
|[Longbow](equipment/weapon-00-MVAWttmT0QDa7LsV.htm)|Arc long|`@Compendium[pf2e.equipment-srd.MVAWttmT0QDa7LsV]`|
|[Longspear](equipment/weapon-00-aXuJh4i8HqSu6NYV.htm)|Pique|`@Compendium[pf2e.equipment-srd.aXuJh4i8HqSu6NYV]`|
|[Longsword](equipment/weapon-00-LJdbVTOZog39EEbi.htm)|Épée longue|`@Compendium[pf2e.equipment-srd.LJdbVTOZog39EEbi]`|
|[Looter's Lethargy](equipment/consumable-02-p3RduziOxQxtREg9.htm)||`@Compendium[pf2e.equipment-srd.p3RduziOxQxtREg9]`|
|[Lover's Gloves](equipment/equipment-08-nZE16PtiyY7DWlzd.htm)||`@Compendium[pf2e.equipment-srd.nZE16PtiyY7DWlzd]`|
|[Lover's Ink](equipment/consumable-02-AALcDOpwxCBN50lA.htm)||`@Compendium[pf2e.equipment-srd.AALcDOpwxCBN50lA]`|
|[Luck Blade](equipment/weapon-17-pIYlenaADKnxdp11.htm)|Lame chanceuse|`@Compendium[pf2e.equipment-srd.pIYlenaADKnxdp11]`|
|[Luck Blade (Wishing)](equipment/weapon-19-C3vAFRWoeGbMQTAH.htm)|Lame chanceuse de souhait|`@Compendium[pf2e.equipment-srd.C3vAFRWoeGbMQTAH]`|
|[Lucky Rabbit's Foot](equipment/consumable-10-3IknuOpIV1OSNl98.htm)||`@Compendium[pf2e.equipment-srd.3IknuOpIV1OSNl98]`|
|[Mace](equipment/weapon-00-9iDqOLNFKxiTcFKE.htm)|Masse d'armes|`@Compendium[pf2e.equipment-srd.9iDqOLNFKxiTcFKE]`|
|[Machete](equipment/weapon-00-O7IZBvVoe7W2XnBa.htm)|Machette|`@Compendium[pf2e.equipment-srd.O7IZBvVoe7W2XnBa]`|
|[Maestro's Instrument (Greater)](equipment/equipment-18-rlDIbl6EQYXQpWVs.htm)|Instrument de maestro (supérieur)|`@Compendium[pf2e.equipment-srd.rlDIbl6EQYXQpWVs]`|
|[Maestro's Instrument (Lesser)](equipment/equipment-03-bAfyWCvgsYDyw3ff.htm)|Instrument du maestro (inférieur)|`@Compendium[pf2e.equipment-srd.bAfyWCvgsYDyw3ff]`|
|[Maestro's Instrument (Moderate)](equipment/equipment-10-3mprh9aZ670HfNhT.htm)|Instrument de maestro (modéré)|`@Compendium[pf2e.equipment-srd.3mprh9aZ670HfNhT]`|
|[Mage Bane](equipment/consumable-12-8uyRlAkWdEyfOziq.htm)||`@Compendium[pf2e.equipment-srd.8uyRlAkWdEyfOziq]`|
|[Magic Wand (1st-Level Spell)](equipment/consumable-03-UJWiN0K3jqVjxvKk.htm)|Baguette magique (sort de 1er niveau)|`@Compendium[pf2e.equipment-srd.UJWiN0K3jqVjxvKk]`|
|[Magic Wand (2nd-Level Spell)](equipment/consumable-05-vJZ49cgi8szuQXAD.htm)|Baguette magique (sort de niveau 2)|`@Compendium[pf2e.equipment-srd.vJZ49cgi8szuQXAD]`|
|[Magic Wand (3rd-Level Spell)](equipment/consumable-07-wrDmWkGxmwzYtfiA.htm)|Baguette magique (sort de niveau 3)|`@Compendium[pf2e.equipment-srd.wrDmWkGxmwzYtfiA]`|
|[Magic Wand (4th-Level Spell)](equipment/consumable-09-Sn7v9SsbEDMUIwrO.htm)|Baguette magique (sort de niveau 4)|`@Compendium[pf2e.equipment-srd.Sn7v9SsbEDMUIwrO]`|
|[Magic Wand (5th-Level Spell)](equipment/consumable-11-5BF7zMnrPYzyigCs.htm)|Baguette magique (sort de niveau 5)|`@Compendium[pf2e.equipment-srd.5BF7zMnrPYzyigCs]`|
|[Magic Wand (6th-Level Spell)](equipment/consumable-13-kiXh4SUWKr166ZeM.htm)|Baguette magique (sort de niveau 6)|`@Compendium[pf2e.equipment-srd.kiXh4SUWKr166ZeM]`|
|[Magic Wand (7th-Level Spell)](equipment/consumable-15-nmXPj9zuMRQBNT60.htm)|Baguette magique (sort de niveau 7)|`@Compendium[pf2e.equipment-srd.nmXPj9zuMRQBNT60]`|
|[Magic Wand (8th-Level Spell)](equipment/consumable-17-Qs8RgNH6thRPv2jt.htm)|Baguette magique (sort de niveau 8)|`@Compendium[pf2e.equipment-srd.Qs8RgNH6thRPv2jt]`|
|[Magic Wand (9th-Level Spell)](equipment/consumable-19-Fgv722039TVM5JTc.htm)|Baguette magique (sort de niveau 9)|`@Compendium[pf2e.equipment-srd.Fgv722039TVM5JTc]`|
|[Magnifying Glass](equipment/equipment-00-5GbC7RTgyAeaOcAI.htm)|Loupe|`@Compendium[pf2e.equipment-srd.5GbC7RTgyAeaOcAI]`|
|[Mail of Luck](equipment/armor-13-AdJgj7vVvNGvPIEj.htm)|Maille chanceuse|`@Compendium[pf2e.equipment-srd.AdJgj7vVvNGvPIEj]`|
|[Main-Gauche](equipment/weapon-00-iWcw1sFLAzByVLBP.htm)|Main-gauche|`@Compendium[pf2e.equipment-srd.iWcw1sFLAzByVLBP]`|
|[Major painting by a legend](equipment/treasure-00-OzxNhgRFEgdTybFl.htm)|Peinture majeure de grande renommée|`@Compendium[pf2e.equipment-srd.OzxNhgRFEgdTybFl]`|
|[Malachite](equipment/treasure-00-Ds8GJEQyRYamRIiB.htm)|Malachite|`@Compendium[pf2e.equipment-srd.Ds8GJEQyRYamRIiB]`|
|[Malyass Root Paste](equipment/consumable-07-76T49dJYfxIrPvQe.htm)|Pâte de racine de malyasse|`@Compendium[pf2e.equipment-srd.76T49dJYfxIrPvQe]`|
|[Mambele](equipment/weapon-00-MeA0g9xvWCbGxf2g.htm)|Mambele (Hunga munga)|`@Compendium[pf2e.equipment-srd.MeA0g9xvWCbGxf2g]`|
|[Manacles (Average)](equipment/equipment-03-lWeADBqMFwFlVIuV.htm)|Menottes (Moyennes)|`@Compendium[pf2e.equipment-srd.lWeADBqMFwFlVIuV]`|
|[Manacles (Good)](equipment/equipment-09-5g0rE5yMsTH3E2LJ.htm)|Menottes (bonnes)|`@Compendium[pf2e.equipment-srd.5g0rE5yMsTH3E2LJ]`|
|[Manacles (Poor)](equipment/equipment-00-ZPY2dOaMYciIJv1Q.htm)|Menottes (médiocre)|`@Compendium[pf2e.equipment-srd.ZPY2dOaMYciIJv1Q]`|
|[Manacles (Simple)](equipment/equipment-01-ckGYDocGEaelHfXF.htm)|Menottes (simple)|`@Compendium[pf2e.equipment-srd.ckGYDocGEaelHfXF]`|
|[Manacles (Superior)](equipment/equipment-17-5zkoqnp6X5yPCXVy.htm)|Menottes (Supérieures)|`@Compendium[pf2e.equipment-srd.5zkoqnp6X5yPCXVy]`|
|[Manacles of Persuasion](equipment/equipment-03-Fq61XZxfgsn4ZDXf.htm)||`@Compendium[pf2e.equipment-srd.Fq61XZxfgsn4ZDXf]`|
|[Marble altar](equipment/treasure-00-TTuRgoLxVR2c6NEx.htm)|Autel en marbre|`@Compendium[pf2e.equipment-srd.TTuRgoLxVR2c6NEx]`|
|[Mariner's Astrolabe](equipment/equipment-03-O1W40RBvlZW069Mw.htm)||`@Compendium[pf2e.equipment-srd.O1W40RBvlZW069Mw]`|
|[Marked Playing Cards](equipment/equipment-00-Q4KkKGGXq4bNGHh2.htm)||`@Compendium[pf2e.equipment-srd.Q4KkKGGXq4bNGHh2]`|
|[Marking Snare](equipment/consumable-01-oplQpQSTyTvHDDtq.htm)|Piège artisanal marqueur|`@Compendium[pf2e.equipment-srd.oplQpQSTyTvHDDtq]`|
|[Martyr's Shield](equipment/armor-07-HWQEGY2X0QXj2zfN.htm)|Bouclier du martyr|`@Compendium[pf2e.equipment-srd.HWQEGY2X0QXj2zfN]`|
|[Marvelous Calliope](equipment/equipment-04-WKdmAhoji9Y9RC7D.htm)||`@Compendium[pf2e.equipment-srd.WKdmAhoji9Y9RC7D]`|
|[Marvelous Medicines](equipment/equipment-12-aZ8HzjZCOJCV7ZWh.htm)|Remèdes merveilleux|`@Compendium[pf2e.equipment-srd.aZ8HzjZCOJCV7ZWh]`|
|[Marvelous Medicines (Greater)](equipment/equipment-18-wYpWQCD2IYqDtqpc.htm)|Remèdes merveilleux (supérieur)|`@Compendium[pf2e.equipment-srd.wYpWQCD2IYqDtqpc]`|
|[Mask of the Banshee](equipment/equipment-09-tr2eOlJMmBKBZtI9.htm)||`@Compendium[pf2e.equipment-srd.tr2eOlJMmBKBZtI9]`|
|[Mask of the Banshee (Greater)](equipment/equipment-09-b2shhButUcNimET9.htm)||`@Compendium[pf2e.equipment-srd.b2shhButUcNimET9]`|
|[Material Component Pouch](equipment/equipment-00-VHxXMvBeBTq2FSdf.htm)|Bourse de composants matériels|`@Compendium[pf2e.equipment-srd.VHxXMvBeBTq2FSdf]`|
|[Mattock of the Titans](equipment/weapon-19-WCWdJmR5tYO7Aulb.htm)|Pioche des titans|`@Compendium[pf2e.equipment-srd.WCWdJmR5tYO7Aulb]`|
|[Maul](equipment/weapon-00-mlrmkpOlwpnGkw4I.htm)|Maillet|`@Compendium[pf2e.equipment-srd.mlrmkpOlwpnGkw4I]`|
|[Medusa Armor](equipment/armor-14-Iic9QpNemaumfkHc.htm)|Armure de la méduse|`@Compendium[pf2e.equipment-srd.Iic9QpNemaumfkHc]`|
|[Medusa's Scream](equipment/armor-13-nzQKwdUxzmmwWtzT.htm)||`@Compendium[pf2e.equipment-srd.nzQKwdUxzmmwWtzT]`|
|[Medusa's Scream (Greater)](equipment/armor-13-TyW1XOWYMM2xHGaI.htm)||`@Compendium[pf2e.equipment-srd.TyW1XOWYMM2xHGaI]`|
|[Mending Lattice](equipment/consumable-13-JSmdTDUEGwHVbV7g.htm)|Maillage réparateur|`@Compendium[pf2e.equipment-srd.JSmdTDUEGwHVbV7g]`|
|[Mentalist's Staff](equipment/weapon-04-xwiZBOjispKVZzGA.htm)|Bâton du mentaliste|`@Compendium[pf2e.equipment-srd.xwiZBOjispKVZzGA]`|
|[Mentalist's Staff (Greater)](equipment/weapon-08-0RCC0fOg1Lp7f79I.htm)|Bâton du mentaliste (supérieur)|`@Compendium[pf2e.equipment-srd.0RCC0fOg1Lp7f79I]`|
|[Mentalist's Staff (Major)](equipment/weapon-12-DTIBj6Yhy73G5P6j.htm)|Bâton du mentaliste (majeur)|`@Compendium[pf2e.equipment-srd.DTIBj6Yhy73G5P6j]`|
|[Merchant's Guile](equipment/equipment-05-AHkmoVr1NSFytJwp.htm)||`@Compendium[pf2e.equipment-srd.AHkmoVr1NSFytJwp]`|
|[Merchant's Scale](equipment/equipment-00-o0ccn5HQAudGgNdU.htm)|Balance de marchand|`@Compendium[pf2e.equipment-srd.o0ccn5HQAudGgNdU]`|
|[Mesmerizing Opal](equipment/consumable-02-aKbrBW1SnFDxya5J.htm)|Opale hypnotisante|`@Compendium[pf2e.equipment-srd.aKbrBW1SnFDxya5J]`|
|[Messenger's Ring](equipment/equipment-09-HAtj6AGCIZHpD7Nl.htm)|Anneau du messager|`@Compendium[pf2e.equipment-srd.HAtj6AGCIZHpD7Nl]`|
|[Messenger's Ring (Greater)](equipment/equipment-17-b9YHyjOL4sg7tjI4.htm)|Anneau du messager (supérieur)|`@Compendium[pf2e.equipment-srd.b9YHyjOL4sg7tjI4]`|
|[Metalmist Sphere (Greater)](equipment/consumable-13-dyvwp4EHgJDzVjJh.htm)||`@Compendium[pf2e.equipment-srd.dyvwp4EHgJDzVjJh]`|
|[Metalmist Sphere (Lesser)](equipment/consumable-03-llqztgP14IJGSxU4.htm)||`@Compendium[pf2e.equipment-srd.llqztgP14IJGSxU4]`|
|[Metalmist Sphere (Moderate)](equipment/consumable-08-KgF3nPT2tEskXqSS.htm)||`@Compendium[pf2e.equipment-srd.KgF3nPT2tEskXqSS]`|
|[Meteor Hammer](equipment/weapon-00-xu3azdMCIa53Oe1f.htm)|Marteau météore|`@Compendium[pf2e.equipment-srd.xu3azdMCIa53Oe1f]`|
|[Mindfog Mist](equipment/consumable-15-GMi5tw0cbMx3ZQPg.htm)|Brumentale|`@Compendium[pf2e.equipment-srd.GMi5tw0cbMx3ZQPg]`|
|[Mirror](equipment/equipment-00-6cyw0yK91cNsbvSK.htm)|Miroir|`@Compendium[pf2e.equipment-srd.6cyw0yK91cNsbvSK]`|
|[Mirror of Sorshen](equipment/equipment-25-Ywt7p5Fyx18lK8km.htm)|Mirroir de Sorshen|`@Compendium[pf2e.equipment-srd.Ywt7p5Fyx18lK8km]`|
|[Mistform Elixir (Greater)](equipment/consumable-10-04giYigfDL5geu5f.htm)|Elixir de brumeforme (supérieur)|`@Compendium[pf2e.equipment-srd.04giYigfDL5geu5f]`|
|[Mistform Elixir (Lesser)](equipment/consumable-04-GyO89RBVjAKFxsFm.htm)|Elixir de brumeforme (inférieur)|`@Compendium[pf2e.equipment-srd.GyO89RBVjAKFxsFm]`|
|[Mistform Elixir (Moderate)](equipment/consumable-06-Ax8XcBd0rRd1Z7hN.htm)|Elixir de brumeforme (modéré)|`@Compendium[pf2e.equipment-srd.Ax8XcBd0rRd1Z7hN]`|
|[Miter of Communion](equipment/equipment-10-8E7qXnNOSm51UwsG.htm)||`@Compendium[pf2e.equipment-srd.8E7qXnNOSm51UwsG]`|
|[Mithral Buckler (High-Grade)](equipment/armor-16-ZZrCiVcoKinA5wyu.htm)|targe en mithral de haute qualité|`@Compendium[pf2e.equipment-srd.ZZrCiVcoKinA5wyu]`|
|[Mithral Buckler (Standard-Grade)](equipment/armor-08-CF9BCVE0ppxMmULb.htm)|Targe en mithral (qualité standard)|`@Compendium[pf2e.equipment-srd.CF9BCVE0ppxMmULb]`|
|[Mithral Chunk](equipment/equipment-00-B0gbs7xKQc2J7AiV.htm)|Morceau de mithral|`@Compendium[pf2e.equipment-srd.B0gbs7xKQc2J7AiV]`|
|[Mithral Ingot](equipment/equipment-00-QpP7Mo7ad5lMOTpv.htm)|Lingot de mithral|`@Compendium[pf2e.equipment-srd.QpP7Mo7ad5lMOTpv]`|
|[Mithral Shield (High-Grade)](equipment/armor-16-5dBSn9brsT9NISoR.htm)|bouclier en mithral de haute qualité|`@Compendium[pf2e.equipment-srd.5dBSn9brsT9NISoR]`|
|[Mithral Shield (Standard-Grade)](equipment/armor-08-ETHbpwjh8aLGrXi0.htm)|Bouclier en mithral (qualité standard)|`@Compendium[pf2e.equipment-srd.ETHbpwjh8aLGrXi0]`|
|[Mnemonic Acid](equipment/consumable-08-YnhcVcTbXrVfiH83.htm)||`@Compendium[pf2e.equipment-srd.YnhcVcTbXrVfiH83]`|
|[Monkey Pin](equipment/consumable-02-jdDDqv9LbEYX2wAE.htm)|Broche de singe|`@Compendium[pf2e.equipment-srd.jdDDqv9LbEYX2wAE]`|
|[Monkey's Fist](equipment/weapon-00-cKbiv1dUMViikKOS.htm)||`@Compendium[pf2e.equipment-srd.cKbiv1dUMViikKOS]`|
|[Monkey's Paw](equipment/equipment-20-QWiXrqhSCkvdHbsi.htm)|Main du singe|`@Compendium[pf2e.equipment-srd.QWiXrqhSCkvdHbsi]`|
|[Moonlit Chain](equipment/armor-07-vHZSx5f093Wrivzn.htm)|Maille lunaire|`@Compendium[pf2e.equipment-srd.vHZSx5f093Wrivzn]`|
|[Moonstone](equipment/treasure-00-vKdjLDfL75SfFGuL.htm)|Pierre de lune|`@Compendium[pf2e.equipment-srd.vKdjLDfL75SfFGuL]`|
|[Moonstone Diadem](equipment/equipment-08-8x8zqH4pJKJRlyAj.htm)||`@Compendium[pf2e.equipment-srd.8x8zqH4pJKJRlyAj]`|
|[Moonstone and onyx game set](equipment/treasure-00-Mp6StcWjlKCpD8Jx.htm)|Pièces de jeu en onyx et orichalque|`@Compendium[pf2e.equipment-srd.Mp6StcWjlKCpD8Jx]`|
|[Morningstar](equipment/weapon-00-5fu6dCtqhdBnHNqh.htm)|Morgenstern|`@Compendium[pf2e.equipment-srd.5fu6dCtqhdBnHNqh]`|
|[Mortar of Hidden Meaning](equipment/equipment-04-LmIpYZ1lS2UDGXvU.htm)||`@Compendium[pf2e.equipment-srd.LmIpYZ1lS2UDGXvU]`|
|[Mountain to the Sky](equipment/equipment-16-ElHAsHIzsVQZp8WW.htm)||`@Compendium[pf2e.equipment-srd.ElHAsHIzsVQZp8WW]`|
|[Mug](equipment/equipment-00-wRSS5vP8ltrThsoC.htm)|Tasse|`@Compendium[pf2e.equipment-srd.wRSS5vP8ltrThsoC]`|
|[Mummified Bat](equipment/consumable-10-UnDjPxFOs6bldlcM.htm)|Chauve-souris mommifiée|`@Compendium[pf2e.equipment-srd.UnDjPxFOs6bldlcM]`|
|[Murderer's Knot](equipment/consumable-07-YGaO4HyH6jn3P731.htm)|Noeud du meurtier|`@Compendium[pf2e.equipment-srd.YGaO4HyH6jn3P731]`|
|[Musical Instrument (Handheld)](equipment/equipment-00-MPv5Yx4w7scZGj2Y.htm)|Instruments de musique (portatifs)|`@Compendium[pf2e.equipment-srd.MPv5Yx4w7scZGj2Y]`|
|[Musical Instrument (Heavy)](equipment/equipment-00-csfJtggwGCF28U2j.htm)||`@Compendium[pf2e.equipment-srd.csfJtggwGCF28U2j]`|
|[Musical Instrument (Virtuoso handheld)](equipment/equipment-03-3ld14dsn2RLu9owg.htm)|Instruments de musique (Virtuose)|`@Compendium[pf2e.equipment-srd.3ld14dsn2RLu9owg]`|
|[Musical Instrument (Virtuoso heavy)](equipment/equipment-03-yw1kPxsdCoDUzOaE.htm)||`@Compendium[pf2e.equipment-srd.yw1kPxsdCoDUzOaE]`|
|[Naginata](equipment/weapon-00-NufKswitJjxDCX8f.htm)|Naginata|`@Compendium[pf2e.equipment-srd.NufKswitJjxDCX8f]`|
|[Nauseating Snare](equipment/consumable-06-MbPboT76BBKVGepB.htm)||`@Compendium[pf2e.equipment-srd.MbPboT76BBKVGepB]`|
|[Necklace of Fireballs I](equipment/equipment-05-6cV9Kpwc7aiuhqbH.htm)|Collier à boules de feu I|`@Compendium[pf2e.equipment-srd.6cV9Kpwc7aiuhqbH]`|
|[Necklace of Fireballs II](equipment/equipment-07-Zun8aKbODnBFeut6.htm)|Collier à boules de feu II|`@Compendium[pf2e.equipment-srd.Zun8aKbODnBFeut6]`|
|[Necklace of Fireballs III](equipment/equipment-09-IufO3dNPhC1c2ZcL.htm)|Collier à boules de feu III|`@Compendium[pf2e.equipment-srd.IufO3dNPhC1c2ZcL]`|
|[Necklace of Fireballs IV](equipment/equipment-11-Dr9b08JhDThUenF9.htm)|Collier à boules de feu IV|`@Compendium[pf2e.equipment-srd.Dr9b08JhDThUenF9]`|
|[Necklace of Fireballs V](equipment/equipment-13-hDO0vRS8r9K2Zw87.htm)|Collier à boules de feu V|`@Compendium[pf2e.equipment-srd.hDO0vRS8r9K2Zw87]`|
|[Necklace of Fireballs VI](equipment/equipment-15-ZZcIhgiKuptXRGyK.htm)|Collier à boules de feu VI|`@Compendium[pf2e.equipment-srd.ZZcIhgiKuptXRGyK]`|
|[Necklace of Fireballs VII](equipment/equipment-17-mOY0STwY5hx4UPCN.htm)|Collier à boules de feu VII|`@Compendium[pf2e.equipment-srd.mOY0STwY5hx4UPCN]`|
|[Necklace of Knives](equipment/equipment-02-9uhJ7OB65ZTdUc8b.htm)||`@Compendium[pf2e.equipment-srd.9uhJ7OB65ZTdUc8b]`|
|[Necklace of Strangulation](equipment/equipment-15-NLF4Z7jn44Sf3RGS.htm)|Collier étrangleur|`@Compendium[pf2e.equipment-srd.NLF4Z7jn44Sf3RGS]`|
|[Nectar of Purification](equipment/consumable-01-ukTlC4G83aVQEg7u.htm)|Nectar de purification|`@Compendium[pf2e.equipment-srd.ukTlC4G83aVQEg7u]`|
|[Net](equipment/equipment-00-T8EopYZLT137CsdW.htm)||`@Compendium[pf2e.equipment-srd.T8EopYZLT137CsdW]`|
|[Nethysian Bulwark](equipment/armor-16-EKXRigmEZgqmFA62.htm)||`@Compendium[pf2e.equipment-srd.EKXRigmEZgqmFA62]`|
|[Nettleweed Residue](equipment/consumable-08-KdeeRCrtsDCJLfgc.htm)|Résidu de lierrortie|`@Compendium[pf2e.equipment-srd.KdeeRCrtsDCJLfgc]`|
|[Nightmare Salt](equipment/consumable-20-dRi775Uhzqgn7aBs.htm)||`@Compendium[pf2e.equipment-srd.dRi775Uhzqgn7aBs]`|
|[Nightmare Vapor](equipment/consumable-16-X6vNtRjyHIuN7vqj.htm)|Vapeur de chauchemar|`@Compendium[pf2e.equipment-srd.X6vNtRjyHIuN7vqj]`|
|[Nightstick](equipment/weapon-00-80G0z7iFUCjHeYGf.htm)||`@Compendium[pf2e.equipment-srd.80G0z7iFUCjHeYGf]`|
|[Nine-Ring Sword](equipment/weapon-00-znIsDYwM5rLK2aVD.htm)|Épée aux neufs anneaux|`@Compendium[pf2e.equipment-srd.znIsDYwM5rLK2aVD]`|
|[Noxious Jerkin](equipment/armor-13-GAu6v14pCSgLJh2D.htm)||`@Compendium[pf2e.equipment-srd.GAu6v14pCSgLJh2D]`|
|[Numerian Steel Breastplate](equipment/armor-10-GDSclGF1yk0sw86V.htm)||`@Compendium[pf2e.equipment-srd.GDSclGF1yk0sw86V]`|
|[Nunchaku](equipment/weapon-00-LA8wwpQBi6tylE6z.htm)|Nunchaku|`@Compendium[pf2e.equipment-srd.LA8wwpQBi6tylE6z]`|
|[Oathbow](equipment/weapon-11-bheWnrBqMphBRUmn.htm)|Arc du serment|`@Compendium[pf2e.equipment-srd.bheWnrBqMphBRUmn]`|
|[Obfuscation Oil](equipment/consumable-15-QD5rDuk0OHadPGuh.htm)|Huile d'obscurcissement|`@Compendium[pf2e.equipment-srd.QD5rDuk0OHadPGuh]`|
|[Oblivion Essence](equipment/consumable-19-5hGJ6CZi0A9OjMd4.htm)||`@Compendium[pf2e.equipment-srd.5hGJ6CZi0A9OjMd4]`|
|[Obsidian](equipment/treasure-00-oH7dJGkgAB0Y2Mad.htm)|Obsidienne|`@Compendium[pf2e.equipment-srd.oH7dJGkgAB0Y2Mad]`|
|[Occult Scroll Case of Simplicity](equipment/equipment-04-EGYcFO9eYfajGKEf.htm)|Étui à parchemin de cimplicité occulte|`@Compendium[pf2e.equipment-srd.EGYcFO9eYfajGKEf]`|
|[Ogre Hook](equipment/weapon-00-Oiq3QgLrM4i3W5Hg.htm)|Crochet ogre|`@Compendium[pf2e.equipment-srd.Oiq3QgLrM4i3W5Hg]`|
|[Oil (1 pint)](equipment/consumable-00-Du05UxCyCM0ZWH8z.htm)|Huile|`@Compendium[pf2e.equipment-srd.Du05UxCyCM0ZWH8z]`|
|[Oil of Animation](equipment/consumable-12-KVrmsNbro6mJ9wuT.htm)|Huile d'animation|`@Compendium[pf2e.equipment-srd.KVrmsNbro6mJ9wuT]`|
|[Oil of Keen Edges](equipment/consumable-11-1DHjXJEdJ7GlGSzg.htm)|Huile d'affûtage|`@Compendium[pf2e.equipment-srd.1DHjXJEdJ7GlGSzg]`|
|[Oil of Mending](equipment/consumable-03-QN8UIz0nMcnLUWHu.htm)|Huile de réparation|`@Compendium[pf2e.equipment-srd.QN8UIz0nMcnLUWHu]`|
|[Oil of Object Animation](equipment/consumable-08-C0kSU5KWMzLngvTa.htm)||`@Compendium[pf2e.equipment-srd.C0kSU5KWMzLngvTa]`|
|[Oil of Object Animation (Greater)](equipment/consumable-16-EP8f2NL28vPlmX7k.htm)||`@Compendium[pf2e.equipment-srd.EP8f2NL28vPlmX7k]`|
|[Oil of Potency](equipment/consumable-02-j2CHumvbjmlLQX2i.htm)|Huile de puissance|`@Compendium[pf2e.equipment-srd.j2CHumvbjmlLQX2i]`|
|[Oil of Repulsion](equipment/consumable-11-IZRfgOYlZ3HRBkYX.htm)|Huile de répulsion|`@Compendium[pf2e.equipment-srd.IZRfgOYlZ3HRBkYX]`|
|[Oil of Revelation](equipment/consumable-05-o9k5L682AlZfhpRu.htm)||`@Compendium[pf2e.equipment-srd.o9k5L682AlZfhpRu]`|
|[Oil of Unlife (Greater)](equipment/consumable-12-RhOx5wenvlljzOku.htm)||`@Compendium[pf2e.equipment-srd.RhOx5wenvlljzOku]`|
|[Oil of Unlife (Lesser)](equipment/consumable-03-BSInwFNVBVkfFK0B.htm)||`@Compendium[pf2e.equipment-srd.BSInwFNVBVkfFK0B]`|
|[Oil of Unlife (Major)](equipment/consumable-18-obER4cKi8UbGhSg7.htm)||`@Compendium[pf2e.equipment-srd.obER4cKi8UbGhSg7]`|
|[Oil of Unlife (Minor)](equipment/consumable-01-fTQ5e4utVfgtXV1e.htm)||`@Compendium[pf2e.equipment-srd.fTQ5e4utVfgtXV1e]`|
|[Oil of Unlife (Moderate)](equipment/consumable-06-YHev1WJ2tOiTBg9o.htm)||`@Compendium[pf2e.equipment-srd.YHev1WJ2tOiTBg9o]`|
|[Oil of Weightlessness](equipment/consumable-02-6DmHDtIsGzH1s5JO.htm)|Huile d'apesanteur|`@Compendium[pf2e.equipment-srd.6DmHDtIsGzH1s5JO]`|
|[Oil of Weightlessness (Greater)](equipment/consumable-06-Ru4xaA4kjdZ4IFS5.htm)|Huile d'apesanteur (supérieure)|`@Compendium[pf2e.equipment-srd.Ru4xaA4kjdZ4IFS5]`|
|[Oily Button](equipment/consumable-05-xAEbIQTNonVjCg38.htm)||`@Compendium[pf2e.equipment-srd.xAEbIQTNonVjCg38]`|
|[Olfactory Obfuscator (Greater)](equipment/consumable-10-MBL8rOfvokYmUvwc.htm)||`@Compendium[pf2e.equipment-srd.MBL8rOfvokYmUvwc]`|
|[Olfactory Obfuscator (Lesser)](equipment/consumable-03-R0gsYr0jD1giTzpx.htm)||`@Compendium[pf2e.equipment-srd.R0gsYr0jD1giTzpx]`|
|[Omnidirectional Spear Snare](equipment/consumable-16-tnCKwIsRsKj3FtG6.htm)||`@Compendium[pf2e.equipment-srd.tnCKwIsRsKj3FtG6]`|
|[Onyx](equipment/treasure-00-3l30rcsNpDM7moKu.htm)|Onyx|`@Compendium[pf2e.equipment-srd.3l30rcsNpDM7moKu]`|
|[Onyx Panther](equipment/consumable-02-0aSdDSjJ5sMzBz1U.htm)|Panthère d'onyx|`@Compendium[pf2e.equipment-srd.0aSdDSjJ5sMzBz1U]`|
|[Opal](equipment/treasure-00-ilnc82SKDYHcoEMH.htm)|Opale|`@Compendium[pf2e.equipment-srd.ilnc82SKDYHcoEMH]`|
|[Orb Shard](equipment/weapon-20-YjaXxg9uQ02IbwLi.htm)||`@Compendium[pf2e.equipment-srd.YjaXxg9uQ02IbwLi]`|
|[Orb of Dragonkind](equipment/equipment-25-L8OButuVM3PFxgrZ.htm)|Orbe des dragons|`@Compendium[pf2e.equipment-srd.L8OButuVM3PFxgrZ]`|
|[Orc Knuckle Dagger](equipment/weapon-00-sAjzLXgA2LnIwpBM.htm)|Dague de poing orque|`@Compendium[pf2e.equipment-srd.sAjzLXgA2LnIwpBM]`|
|[Orc Necksplitter](equipment/weapon-00-J6H5UbLVsSXShoTs.htm)|Coupecou orc|`@Compendium[pf2e.equipment-srd.J6H5UbLVsSXShoTs]`|
|[Orichalcum Buckler (High-Grade)](equipment/armor-17-OKo8ub6D11ztZc2V.htm)|targe en orichalque de haute qualité|`@Compendium[pf2e.equipment-srd.OKo8ub6D11ztZc2V]`|
|[Orichalcum Chunk](equipment/equipment-00-YOgUBjv2YHzPFKUb.htm)|Morceau d'orichalque|`@Compendium[pf2e.equipment-srd.YOgUBjv2YHzPFKUb]`|
|[Orichalcum Ingot](equipment/equipment-00-4AnNkIA5Ux6ePy7r.htm)|Lingot d'orichalque|`@Compendium[pf2e.equipment-srd.4AnNkIA5Ux6ePy7r]`|
|[Orichalcum Shield (High-Grade)](equipment/armor-17-PeS3J9r4ss7gNytK.htm)|bouclier en orichalque de haute qualité|`@Compendium[pf2e.equipment-srd.PeS3J9r4ss7gNytK]`|
|[Origin Unguent](equipment/consumable-02-npSWkkSsQfiKixPO.htm)||`@Compendium[pf2e.equipment-srd.npSWkkSsQfiKixPO]`|
|[Original manuscript from a world‑famous author](equipment/treasure-00-Oru7cHMGULLXRLV3.htm)|Manuscrit original (auteur mondialement connu)|`@Compendium[pf2e.equipment-srd.Oru7cHMGULLXRLV3]`|
|[Owlbear Claw](equipment/consumable-01-qoM7Va5GqcLLBzgu.htm)|Griffe d'hibours|`@Compendium[pf2e.equipment-srd.qoM7Va5GqcLLBzgu]`|
|[Pacifying](equipment/equipment-05-R8I13CDRzvpVXOVe.htm)||`@Compendium[pf2e.equipment-srd.R8I13CDRzvpVXOVe]`|
|[Pactmaster's Grace](equipment/equipment-14-IHybXd0JGwhMrOlr.htm)||`@Compendium[pf2e.equipment-srd.IHybXd0JGwhMrOlr]`|
|[Padded Armor](equipment/armor-00-zBYEU9E7034ENCmh.htm)|Armure matelassée|`@Compendium[pf2e.equipment-srd.zBYEU9E7034ENCmh]`|
|[Palm Crossbow](equipment/equipment-03-gDfBzU6umebo5RXP.htm)||`@Compendium[pf2e.equipment-srd.gDfBzU6umebo5RXP]`|
|[Panacea](equipment/consumable-13-LRSiYsgEz3e0PEwX.htm)|Panacée|`@Compendium[pf2e.equipment-srd.LRSiYsgEz3e0PEwX]`|
|[Panacea Fruit](equipment/consumable-19-9WenEIQrBV8POQU1.htm)||`@Compendium[pf2e.equipment-srd.9WenEIQrBV8POQU1]`|
|[Parade armor with flourishes](equipment/treasure-00-GgwlIaNPJVsyRwzz.htm)|Armure de parade avec des fioritures|`@Compendium[pf2e.equipment-srd.GgwlIaNPJVsyRwzz]`|
|[Parrying Scabbard](equipment/equipment-00-o29fgVteF3yj8NKn.htm)|Fourreau de parade|`@Compendium[pf2e.equipment-srd.o29fgVteF3yj8NKn]`|
|[Pathfinder Chronicle](equipment/equipment-03-bbSc1VU1LiQqReKd.htm)|Chronique de l'éclaireur|`@Compendium[pf2e.equipment-srd.bbSc1VU1LiQqReKd]`|
|[Pathfinder's Coin](equipment/equipment-02-sZxPKnLtspXPRDNb.htm)|Pièce de l'éclaireur|`@Compendium[pf2e.equipment-srd.sZxPKnLtspXPRDNb]`|
|[Pathfinder's Pouch](equipment/backpack-06-0c7zLY9c88K2n0GC.htm)|Bourse d'Éclaireur|`@Compendium[pf2e.equipment-srd.0c7zLY9c88K2n0GC]`|
|[Pearl, black](equipment/treasure-hhJfZgLhPaPh5V7e.htm)|Perle noire|`@Compendium[pf2e.equipment-srd.hhJfZgLhPaPh5V7e]`|
|[Pearl, irregular freshwater](equipment/treasure-00-RLFxTy0TjMkN6rM2.htm)|Perle d'eau douce irrégulière|`@Compendium[pf2e.equipment-srd.RLFxTy0TjMkN6rM2]`|
|[Pearl, saltwater](equipment/treasure-00-7iVFlnr2ZbBdtOx8.htm)|Perle d'eau salée|`@Compendium[pf2e.equipment-srd.7iVFlnr2ZbBdtOx8]`|
|[Pendant of the Occult](equipment/equipment-03-zPhqmCvWyHO8i9ws.htm)|Pendentif des sciences occultes|`@Compendium[pf2e.equipment-srd.zPhqmCvWyHO8i9ws]`|
|[Pendant of the Occult (Greater)](equipment/equipment-09-1bvH8zFQvDYky9tr.htm)|Pendentif des sciences occultes (supérieur)|`@Compendium[pf2e.equipment-srd.1bvH8zFQvDYky9tr]`|
|[Penetrating Ammunition](equipment/consumable-12-tOg1nt08dBTZbkwR.htm)|Munition pénétrante|`@Compendium[pf2e.equipment-srd.tOg1nt08dBTZbkwR]`|
|[Peridot](equipment/treasure-00-oZYu6GMIIoOYW2Nh.htm)|Péridot|`@Compendium[pf2e.equipment-srd.oZYu6GMIIoOYW2Nh]`|
|[Periscope](equipment/equipment-02-ZZnh7BkwApPyNyqc.htm)|Périscope|`@Compendium[pf2e.equipment-srd.ZZnh7BkwApPyNyqc]`|
|[Persona Mask](equipment/equipment-03-MKupH1T018JubYJW.htm)|Masque de représentation|`@Compendium[pf2e.equipment-srd.MKupH1T018JubYJW]`|
|[Persona Mask (Greater)](equipment/equipment-09-0PPQl3TEr1yNhhN6.htm)|Masque de représentation (supérieur)|`@Compendium[pf2e.equipment-srd.0PPQl3TEr1yNhhN6]`|
|[Peshpine Grenade (Greater)](equipment/weapon-11-ZVhSxudgfnSO4AMd.htm)|Grenades d'épines de pesh (supérieure)|`@Compendium[pf2e.equipment-srd.ZVhSxudgfnSO4AMd]`|
|[Peshpine Grenade (Lesser)](equipment/weapon-01-c124j3cpv8rl5MLp.htm)|Grenade d'épines de pesh (Inférieure)|`@Compendium[pf2e.equipment-srd.c124j3cpv8rl5MLp]`|
|[Peshpine Grenade (Major)](equipment/weapon-17-b1gx7ZhuFJAyJOo8.htm)|Grenade d'épines de pesh (majeure)|`@Compendium[pf2e.equipment-srd.b1gx7ZhuFJAyJOo8]`|
|[Peshpine Grenade (Moderate)](equipment/weapon-03-AvGZqDfalwwbYFvA.htm)|Grenade d'épines de pesh (Modérée)|`@Compendium[pf2e.equipment-srd.AvGZqDfalwwbYFvA]`|
|[Phasing ether silk tapestry](equipment/treasure-00-q72jL8PRcjw3DVoF.htm)|Tapisserie de soie d'araignée de phase|`@Compendium[pf2e.equipment-srd.q72jL8PRcjw3DVoF]`|
|[Philosopher's Extractor](equipment/equipment-28-mH3LImCgJAkfKAA3.htm)|Distillateur philosophal|`@Compendium[pf2e.equipment-srd.mH3LImCgJAkfKAA3]`|
|[Philosopher's Stone](equipment/consumable-20-Fv97oB3iEIFAyzTu.htm)|Pierre philosophale|`@Compendium[pf2e.equipment-srd.Fv97oB3iEIFAyzTu]`|
|[Phylactery of Faithfulness](equipment/equipment-09-pPl2b7fqfB6dyQwf.htm)|Phylactère du croyant|`@Compendium[pf2e.equipment-srd.pPl2b7fqfB6dyQwf]`|
|[Phylactery of Faithfulness (Greater)](equipment/equipment-17-hrG2w4IfF1QZhSzw.htm)|Phylactère du croyant (Supérieur)|`@Compendium[pf2e.equipment-srd.hrG2w4IfF1QZhSzw]`|
|[Pick](equipment/weapon-00-6I4YJAQUbTAqbpsI.htm)|Pic de guerre|`@Compendium[pf2e.equipment-srd.6I4YJAQUbTAqbpsI]`|
|[Piereta](equipment/weapon-20-O6he0J7l1uQgsama.htm)||`@Compendium[pf2e.equipment-srd.O6he0J7l1uQgsama]`|
|[Piton](equipment/equipment-00-oX39xqMLVB8kNrY0.htm)|Piton|`@Compendium[pf2e.equipment-srd.oX39xqMLVB8kNrY0]`|
|[Plain brass censer](equipment/treasure-00-dzsJfE1JIWUMCiK4.htm)|Encensoir en laiton massif|`@Compendium[pf2e.equipment-srd.dzsJfE1JIWUMCiK4]`|
|[Planar Ribbon](equipment/equipment-20-h2aVryWbNP24gC05.htm)||`@Compendium[pf2e.equipment-srd.h2aVryWbNP24gC05]`|
|[Plasma Hype](equipment/consumable-12-bRqczrlQDaeUvg6b.htm)||`@Compendium[pf2e.equipment-srd.bRqczrlQDaeUvg6b]`|
|[Plate Armor of the Deep](equipment/armor-15-gp5kgCySEOuntQPF.htm)|Harnois des profondeurs|`@Compendium[pf2e.equipment-srd.gp5kgCySEOuntQPF]`|
|[Platinum Pieces](equipment/treasure-00-JuNPeK5Qm1w6wpb4.htm)|Pièce de platine|`@Compendium[pf2e.equipment-srd.JuNPeK5Qm1w6wpb4]`|
|[Platinum dragon statuette](equipment/treasure-00-HMdfNM7ibp1XtcVT.htm)|Statuette de dragon en platine|`@Compendium[pf2e.equipment-srd.HMdfNM7ibp1XtcVT]`|
|[Platinum image of a fey noble with a bit of orichalcum](equipment/treasure-00-LzHxy9YPEaCueLB9.htm)|Image en platine d'une noble fée avec un morceau d'orichalque|`@Compendium[pf2e.equipment-srd.LzHxy9YPEaCueLB9]`|
|[Platinum‑framed monocle](equipment/treasure-nBY7ueX18tcSiica.htm)|Monocle à la monture de platine|`@Compendium[pf2e.equipment-srd.nBY7ueX18tcSiica]`|
|[Playing Cards](equipment/equipment-00-EsqFTJXcmzfngRgL.htm)||`@Compendium[pf2e.equipment-srd.EsqFTJXcmzfngRgL]`|
|[Pocket Stage](equipment/equipment-05-BNelZMBHKlPAWl9Z.htm)|Scène de poche|`@Compendium[pf2e.equipment-srd.BNelZMBHKlPAWl9Z]`|
|[Poi](equipment/weapon-00-PnFfW5u24xZV6mOH.htm)|Poï|`@Compendium[pf2e.equipment-srd.PnFfW5u24xZV6mOH]`|
|[Poisoner's Staff](equipment/equipment-04-Rwx6I3CDdOeOl5xW.htm)||`@Compendium[pf2e.equipment-srd.Rwx6I3CDdOeOl5xW]`|
|[Poisoner's Staff (Greater)](equipment/equipment-08-10CiChAgeSExN7Ie.htm)||`@Compendium[pf2e.equipment-srd.10CiChAgeSExN7Ie]`|
|[Poisoner's Staff (Major)](equipment/equipment-08-lL50lfx5tBosb1s3.htm)||`@Compendium[pf2e.equipment-srd.lL50lfx5tBosb1s3]`|
|[Poisonous Cloak Type I](equipment/equipment-06-65ZhBT2S8bCeEIgz.htm)|Cape empoisonnée (Type I)|`@Compendium[pf2e.equipment-srd.65ZhBT2S8bCeEIgz]`|
|[Poisonous Cloak Type II](equipment/equipment-10-BNShQdbxiPMisV60.htm)|Cape empoisonnée (Type II)|`@Compendium[pf2e.equipment-srd.BNShQdbxiPMisV60]`|
|[Poisonous Cloak Type III](equipment/equipment-13-f8AI23xzAHyuU35m.htm)|Cape empoisonnée (Type III)|`@Compendium[pf2e.equipment-srd.f8AI23xzAHyuU35m]`|
|[Poisonous Cloak Type IV](equipment/equipment-17-XbQrf5aYEWweje30.htm)|Cape empoisonnée (type IV)|`@Compendium[pf2e.equipment-srd.XbQrf5aYEWweje30]`|
|[Poisonous Dagger](equipment/weapon-05-hK5xPjOFd2hb81mi.htm)||`@Compendium[pf2e.equipment-srd.hK5xPjOFd2hb81mi]`|
|[Polytool](equipment/weapon-01-vRCH0cQJMllWvpfU.htm)|Outil universel|`@Compendium[pf2e.equipment-srd.vRCH0cQJMllWvpfU]`|
|[Porcelain doll with amber eyes](equipment/treasure-00-z84EQt8kcPf0RXwx.htm)|Poupée de porcelaine avec des yeux d'ambre|`@Compendium[pf2e.equipment-srd.z84EQt8kcPf0RXwx]`|
|[Porcelain vase inlaid with gold](equipment/treasure-00-JECXvZ0bKTDr79mo.htm)|Vase de porcelaine incrusté d'or|`@Compendium[pf2e.equipment-srd.JECXvZ0bKTDr79mo]`|
|[Possibility Tome](equipment/equipment-18-vZaXDOrp5Faxw5fS.htm)|Livre des possibilités|`@Compendium[pf2e.equipment-srd.vZaXDOrp5Faxw5fS]`|
|[Potency Crystal](equipment/consumable-01-ZCsksGf6NPUKz2Uw.htm)|Cristal de puissance|`@Compendium[pf2e.equipment-srd.ZCsksGf6NPUKz2Uw]`|
|[Potion of Acid Resistance (Greater)](equipment/consumable-14-vDgDrbSoGuiUIz2R.htm)|Potion de résistance à l'acide (supérieure)|`@Compendium[pf2e.equipment-srd.vDgDrbSoGuiUIz2R]`|
|[Potion of Acid Resistance (Lesser)](equipment/consumable-06-u1gZaIZHnEfZDc1o.htm)|Potion de résistance à l'acide (inférieur)|`@Compendium[pf2e.equipment-srd.u1gZaIZHnEfZDc1o]`|
|[Potion of Acid Resistance (Moderate)](equipment/consumable-10-yG6Za8FaG3hpXUGh.htm)||`@Compendium[pf2e.equipment-srd.yG6Za8FaG3hpXUGh]`|
|[Potion of Acid Retaliation (Greater)](equipment/consumable-12-jVmTtUeNxDZMP5dU.htm)||`@Compendium[pf2e.equipment-srd.jVmTtUeNxDZMP5dU]`|
|[Potion of Acid Retaliation (Lesser)](equipment/consumable-03-omRmCbkQoK5YmCGv.htm)||`@Compendium[pf2e.equipment-srd.omRmCbkQoK5YmCGv]`|
|[Potion of Acid Retaliation (Major)](equipment/consumable-18-fabw5fDuaTMUF0tb.htm)||`@Compendium[pf2e.equipment-srd.fabw5fDuaTMUF0tb]`|
|[Potion of Acid Retaliation (Minor)](equipment/consumable-01-apGQcVMSRprFEeKt.htm)||`@Compendium[pf2e.equipment-srd.apGQcVMSRprFEeKt]`|
|[Potion of Acid Retaliation (Moderate)](equipment/consumable-06-OWPOTwMsrYma9d0v.htm)||`@Compendium[pf2e.equipment-srd.OWPOTwMsrYma9d0v]`|
|[Potion of Cold Resistance (Greater)](equipment/consumable-14-VZmUrseCZs9xMaxt.htm)|Potion de résistance au froid (supérieure)|`@Compendium[pf2e.equipment-srd.VZmUrseCZs9xMaxt]`|
|[Potion of Cold Resistance (Lesser)](equipment/consumable-06-mRs24OwmGPjoIDvO.htm)|Potion de résistance au froid (inférieur)|`@Compendium[pf2e.equipment-srd.mRs24OwmGPjoIDvO]`|
|[Potion of Cold Resistance (Moderate)](equipment/consumable-10-evdDpgFpiFZt9UyA.htm)||`@Compendium[pf2e.equipment-srd.evdDpgFpiFZt9UyA]`|
|[Potion of Cold Retaliation (Greater)](equipment/consumable-12-tW2rmGSanKQlUkiU.htm)||`@Compendium[pf2e.equipment-srd.tW2rmGSanKQlUkiU]`|
|[Potion of Cold Retaliation (Lesser)](equipment/consumable-03-Uq3rnsAHbLKyW49E.htm)||`@Compendium[pf2e.equipment-srd.Uq3rnsAHbLKyW49E]`|
|[Potion of Cold Retaliation (Major)](equipment/consumable-18-bCPAiqiWmH7pVxNE.htm)||`@Compendium[pf2e.equipment-srd.bCPAiqiWmH7pVxNE]`|
|[Potion of Cold Retaliation (Minor)](equipment/consumable-01-zExWH2EsY9STTORq.htm)||`@Compendium[pf2e.equipment-srd.zExWH2EsY9STTORq]`|
|[Potion of Cold Retaliation (Moderate)](equipment/consumable-06-M6pES1Nck1S6SWX9.htm)||`@Compendium[pf2e.equipment-srd.M6pES1Nck1S6SWX9]`|
|[Potion of Disguise (Greater)](equipment/consumable-11-ThX0ntpTqonGqguT.htm)||`@Compendium[pf2e.equipment-srd.ThX0ntpTqonGqguT]`|
|[Potion of Disguise (Lesser)](equipment/consumable-05-QNub2kTE7LpdMPII.htm)||`@Compendium[pf2e.equipment-srd.QNub2kTE7LpdMPII]`|
|[Potion of Disguise (Moderate)](equipment/consumable-08-25Rr05SIfTj0GA31.htm)||`@Compendium[pf2e.equipment-srd.25Rr05SIfTj0GA31]`|
|[Potion of Electricity Resistance (Greater)](equipment/consumable-14-F8vrGctUqYIEkKOz.htm)|Potion de résistance à l'électricité (supérieur)|`@Compendium[pf2e.equipment-srd.F8vrGctUqYIEkKOz]`|
|[Potion of Electricity Resistance (Lesser)](equipment/consumable-06-cAyxcRSSnfscGyMa.htm)|Potion de résistance à l'électricité (inférieur)|`@Compendium[pf2e.equipment-srd.cAyxcRSSnfscGyMa]`|
|[Potion of Electricity Resistance (Moderate)](equipment/consumable-10-MHoghQhxbvZRyyaB.htm)||`@Compendium[pf2e.equipment-srd.MHoghQhxbvZRyyaB]`|
|[Potion of Electricity Retaliation (Greater)](equipment/consumable-12-QsYfjRMBz48H6UAp.htm)||`@Compendium[pf2e.equipment-srd.QsYfjRMBz48H6UAp]`|
|[Potion of Electricity Retaliation (Lesser)](equipment/consumable-03-TG4SnQCGOsgQfrE1.htm)||`@Compendium[pf2e.equipment-srd.TG4SnQCGOsgQfrE1]`|
|[Potion of Electricity Retaliation (Major)](equipment/consumable-18-bEaiID2KLQ8lTCai.htm)||`@Compendium[pf2e.equipment-srd.bEaiID2KLQ8lTCai]`|
|[Potion of Electricity Retaliation (Minor)](equipment/consumable-01-w0S1SIRsHgGdRh1d.htm)||`@Compendium[pf2e.equipment-srd.w0S1SIRsHgGdRh1d]`|
|[Potion of Electricity Retaliation (Moderate)](equipment/consumable-06-oDfucsKeWpJmmWN1.htm)||`@Compendium[pf2e.equipment-srd.oDfucsKeWpJmmWN1]`|
|[Potion of Expeditious Retreat](equipment/consumable-01-jTfacZ4SRuQd7Avh.htm)||`@Compendium[pf2e.equipment-srd.jTfacZ4SRuQd7Avh]`|
|[Potion of Fire Resistance (Greater)](equipment/consumable-14-1OJxRnPs0viJVrZq.htm)|Potion de résistance au feu (supérieure)|`@Compendium[pf2e.equipment-srd.1OJxRnPs0viJVrZq]`|
|[Potion of Fire Resistance (Lesser)](equipment/consumable-06-bDnk4TSzvD5BQmE5.htm)|Potion de résistance au feu (inférieure)|`@Compendium[pf2e.equipment-srd.bDnk4TSzvD5BQmE5]`|
|[Potion of Fire Resistance (Moderate)](equipment/consumable-10-m99u0zXVbyPdK8Mf.htm)||`@Compendium[pf2e.equipment-srd.m99u0zXVbyPdK8Mf]`|
|[Potion of Fire Retaliation (Greater)](equipment/consumable-12-hMUYvp0neF0LAFqc.htm)||`@Compendium[pf2e.equipment-srd.hMUYvp0neF0LAFqc]`|
|[Potion of Fire Retaliation (Lesser)](equipment/consumable-03-67KiGZtoTMbl7nv2.htm)||`@Compendium[pf2e.equipment-srd.67KiGZtoTMbl7nv2]`|
|[Potion of Fire Retaliation (Major)](equipment/consumable-18-TalLb1YlvXzutuPc.htm)||`@Compendium[pf2e.equipment-srd.TalLb1YlvXzutuPc]`|
|[Potion of Fire Retaliation (Minor)](equipment/consumable-01-ugIxgGIkJh2aXE2N.htm)||`@Compendium[pf2e.equipment-srd.ugIxgGIkJh2aXE2N]`|
|[Potion of Fire Retaliation (Moderate)](equipment/consumable-06-AgBIZtwciSsCZeNN.htm)||`@Compendium[pf2e.equipment-srd.AgBIZtwciSsCZeNN]`|
|[Potion of Flying (Greater)](equipment/consumable-15-E999hL7XlAlfZhjK.htm)|Potion de vol (Supérieur)|`@Compendium[pf2e.equipment-srd.E999hL7XlAlfZhjK]`|
|[Potion of Flying (Standard)](equipment/consumable-08-KlJSw919hpN6V9oK.htm)|Potion de vol (standard)|`@Compendium[pf2e.equipment-srd.KlJSw919hpN6V9oK]`|
|[Potion of Leaping](equipment/consumable-05-rzEQvcWfhR3T4FNd.htm)|Potion de saut|`@Compendium[pf2e.equipment-srd.rzEQvcWfhR3T4FNd]`|
|[Potion of Quickness](equipment/consumable-08-kORpovlPYicysr2g.htm)|Potion de rapidité|`@Compendium[pf2e.equipment-srd.kORpovlPYicysr2g]`|
|[Potion of Resistance (Greater)](equipment/consumable-14-ywYt2SyZaV95KcZH.htm)||`@Compendium[pf2e.equipment-srd.ywYt2SyZaV95KcZH]`|
|[Potion of Resistance (Lesser)](equipment/consumable-06-V2TUEoiDwJ125qzN.htm)|Potion de résistance (inférieur)|`@Compendium[pf2e.equipment-srd.V2TUEoiDwJ125qzN]`|
|[Potion of Resistance (Moderate)](equipment/consumable-10-dzfmP3WsA15puenS.htm)||`@Compendium[pf2e.equipment-srd.dzfmP3WsA15puenS]`|
|[Potion of Sonic Resistance (Greater)](equipment/consumable-14-GcllrP56XWEsYQ4j.htm)|Potion de résistance au son (supérieure)|`@Compendium[pf2e.equipment-srd.GcllrP56XWEsYQ4j]`|
|[Potion of Sonic Resistance (Lesser)](equipment/consumable-06-eSHX07MqMT4SM1zY.htm)|Potion de résistance au son (inférieur)|`@Compendium[pf2e.equipment-srd.eSHX07MqMT4SM1zY]`|
|[Potion of Sonic Resistance (Moderate)](equipment/consumable-10-kbjspHV7kwKKULmd.htm)||`@Compendium[pf2e.equipment-srd.kbjspHV7kwKKULmd]`|
|[Potion of Swimming (Greater)](equipment/consumable-11-OMJgFmy3jut79Iaj.htm)|Potion de nage (Supérieure)|`@Compendium[pf2e.equipment-srd.OMJgFmy3jut79Iaj]`|
|[Potion of Swimming (Moderate)](equipment/consumable-06-Eb4dEuV22QVlMumS.htm)|Potion de nage (modérée)|`@Compendium[pf2e.equipment-srd.Eb4dEuV22QVlMumS]`|
|[Potion of Tongues](equipment/consumable-12-drxEWZl8mqMOZ23E.htm)|Potion de don des langues|`@Compendium[pf2e.equipment-srd.drxEWZl8mqMOZ23E]`|
|[Potion of Undetectability](equipment/consumable-18-UblsH5cGyUdXypek.htm)|Potion de détection impossible|`@Compendium[pf2e.equipment-srd.UblsH5cGyUdXypek]`|
|[Potion of Water Breathing](equipment/consumable-03-WQhnfj1LbrEzvh8z.htm)|Potion de respiration aquatique|`@Compendium[pf2e.equipment-srd.WQhnfj1LbrEzvh8z]`|
|[Predictable Silver Piece](equipment/equipment-01-ZIQAzOavTXJCcCMD.htm)||`@Compendium[pf2e.equipment-srd.ZIQAzOavTXJCcCMD]`|
|[Previously lost volume from a legendary author](equipment/treasure-00-laXnb43oznxeSLT9.htm)|Ouvrage perdu (auteur légandaire)|`@Compendium[pf2e.equipment-srd.laXnb43oznxeSLT9]`|
|[Primal Scroll Case of Simplicity](equipment/equipment-04-mqOjTuUHv4cRdt2N.htm)|Étui à parchemin de simplicité primordiale|`@Compendium[pf2e.equipment-srd.mqOjTuUHv4cRdt2N]`|
|[Primeval Mistletoe](equipment/equipment-06-J5MqY1P3JWrezcQX.htm)|Gui primitif|`@Compendium[pf2e.equipment-srd.J5MqY1P3JWrezcQX]`|
|[Primeval Mistletoe (Greater)](equipment/equipment-14-HZDkF6MlNQ6yYobD.htm)|Gui primitif (supérieure)|`@Compendium[pf2e.equipment-srd.HZDkF6MlNQ6yYobD]`|
|[Psychic Warding Bracelet](equipment/consumable-03-hoX2sJiYtJrSp6BH.htm)||`@Compendium[pf2e.equipment-srd.hoX2sJiYtJrSp6BH]`|
|[Purple Worm Repellent](equipment/consumable-13-BxzwmwQ5O4fewa4w.htm)||`@Compendium[pf2e.equipment-srd.BxzwmwQ5O4fewa4w]`|
|[Purple Worm Venom](equipment/consumable-13-iRDCa4OVSTGUD3vi.htm)|Venin de ver pourpre|`@Compendium[pf2e.equipment-srd.iRDCa4OVSTGUD3vi]`|
|[Putrescent Glob](equipment/consumable-07-TSbuKm91qmqdqQW3.htm)||`@Compendium[pf2e.equipment-srd.TSbuKm91qmqdqQW3]`|
|[Pyrite](equipment/treasure-00-YFokzLQ0HK74Vf6n.htm)|Pyrite|`@Compendium[pf2e.equipment-srd.YFokzLQ0HK74Vf6n]`|
|[Pyrite Rat](equipment/equipment-02-YmTGzsenhogSNDXK.htm)||`@Compendium[pf2e.equipment-srd.YmTGzsenhogSNDXK]`|
|[Quality painting by an unknown](equipment/treasure-00-iz7Mv2xGwqyicNiA.htm)|Peinture de qualité (auteur inconnu)|`@Compendium[pf2e.equipment-srd.iz7Mv2xGwqyicNiA]`|
|[Quality sculpture by an unknown](equipment/treasure-00-aGJJwRpFRMgneROG.htm)|Sculpture de qualité (artiste inconnu)|`@Compendium[pf2e.equipment-srd.aGJJwRpFRMgneROG]`|
|[Quartz, milky, rose, or smoky](equipment/treasure-00-PGlNIOpo8yedgTFR.htm)|Quartz rose, laiteux ou fumé|`@Compendium[pf2e.equipment-srd.PGlNIOpo8yedgTFR]`|
|[Quartz, rock crystal](equipment/treasure-00-qnK4cIHZ2irCV4BL.htm)|Quartz, cristal de roche|`@Compendium[pf2e.equipment-srd.qnK4cIHZ2irCV4BL]`|
|[Quickpatch Glue](equipment/consumable-03-iNR6t5dDiYGQYTeA.htm)||`@Compendium[pf2e.equipment-srd.iNR6t5dDiYGQYTeA]`|
|[Quicksilver Mutagen (Greater)](equipment/consumable-11-eQDTsetadI8u8Kc0.htm)|Mutagène vif-argent (supérieur)|`@Compendium[pf2e.equipment-srd.eQDTsetadI8u8Kc0]`|
|[Quicksilver Mutagen (Lesser)](equipment/consumable-01-5MKBwpE401uz4kNN.htm)|Mutagène de Vif-argent (inférieur)|`@Compendium[pf2e.equipment-srd.5MKBwpE401uz4kNN]`|
|[Quicksilver Mutagen (Major)](equipment/consumable-17-4GXzTN6iSDGfYEAi.htm)|Mutagène vif-argent (majeur)|`@Compendium[pf2e.equipment-srd.4GXzTN6iSDGfYEAi]`|
|[Quicksilver Mutagen (Moderate)](equipment/consumable-03-n52BSbZsnx4Vmt2p.htm)|Mutagène de vif-argent (modéré)|`@Compendium[pf2e.equipment-srd.n52BSbZsnx4Vmt2p]`|
|[Quill of Passage](equipment/equipment-11-idAyVZxTkPSNLbnt.htm)||`@Compendium[pf2e.equipment-srd.idAyVZxTkPSNLbnt]`|
|[Radiant Lance](equipment/weapon-15-fLqRH3XpvDZEMxOO.htm)||`@Compendium[pf2e.equipment-srd.fLqRH3XpvDZEMxOO]`|
|[Ranseur](equipment/weapon-00-UfurZQK6H6SgOjqe.htm)|Corsèque|`@Compendium[pf2e.equipment-srd.UfurZQK6H6SgOjqe]`|
|[Rapier](equipment/weapon-00-tH5GirEy7YB3ZgCk.htm)|Rapière|`@Compendium[pf2e.equipment-srd.tH5GirEy7YB3ZgCk]`|
|[Rat-Catcher Trident](equipment/weapon-11-rvxphQJrycwqejMW.htm)||`@Compendium[pf2e.equipment-srd.rvxphQJrycwqejMW]`|
|[Ration Tonic](equipment/consumable-01-3Wb0N7iqmTn6e2Xc.htm)|Ration énergisante|`@Compendium[pf2e.equipment-srd.3Wb0N7iqmTn6e2Xc]`|
|[Ration Tonic (Greater)](equipment/consumable-07-EkC2W5A5fohoIKSd.htm)||`@Compendium[pf2e.equipment-srd.EkC2W5A5fohoIKSd]`|
|[Rations](equipment/equipment-00-L9ZV076913otGtiB.htm)|Rations|`@Compendium[pf2e.equipment-srd.L9ZV076913otGtiB]`|
|[Razmiri Wayfinder](equipment/equipment-10-fuDYKpDN9RmCX7do.htm)||`@Compendium[pf2e.equipment-srd.fuDYKpDN9RmCX7do]`|
|[Ready](equipment/equipment-06-QNPwzwKervKpk6YO.htm)||`@Compendium[pf2e.equipment-srd.QNPwzwKervKpk6YO]`|
|[Ready (Greater)](equipment/equipment-11-fumxKes1z2hLN2U7.htm)||`@Compendium[pf2e.equipment-srd.fumxKes1z2hLN2U7]`|
|[Reaper's Lancet](equipment/weapon-05-6HOQwVUQNplJsrhq.htm)||`@Compendium[pf2e.equipment-srd.6HOQwVUQNplJsrhq]`|
|[Recording Rod (Basic)](equipment/consumable-03-1Nez8K5C4fwgFrTz.htm)||`@Compendium[pf2e.equipment-srd.1Nez8K5C4fwgFrTz]`|
|[Recording Rod (Reusable)](equipment/consumable-05-jrLgEJxPvUKtSMMO.htm)||`@Compendium[pf2e.equipment-srd.jrLgEJxPvUKtSMMO]`|
|[Red Dragon's Breath Potion (Adult)](equipment/consumable-12-Bn1o5foPq1kqZKf0.htm)|Potion de souffle du dragon rouge adulte|`@Compendium[pf2e.equipment-srd.Bn1o5foPq1kqZKf0]`|
|[Red Dragon's Breath Potion (Wyrm)](equipment/consumable-17-d2sU6nOtBdIVJMVI.htm)|Potion de souffle de dracosire rouge|`@Compendium[pf2e.equipment-srd.d2sU6nOtBdIVJMVI]`|
|[Red Dragon's Breath Potion (Young)](equipment/consumable-07-fkGHYeGVK6O2VW1s.htm)|Potion de souffle de jeune dragon rouge|`@Compendium[pf2e.equipment-srd.fkGHYeGVK6O2VW1s]`|
|[Refined Pesh](equipment/consumable-01-Vy3z2cIQ8uJRUMYw.htm)|Pesh raffiné|`@Compendium[pf2e.equipment-srd.Vy3z2cIQ8uJRUMYw]`|
|[Reflecting Shield](equipment/armor-18-La9qYc5NHsg423Jb.htm)|Bouclier réfléchissant|`@Compendium[pf2e.equipment-srd.La9qYc5NHsg423Jb]`|
|[Reforging Shield](equipment/armor-15-aPD0z9dBsHqgiCW0.htm)|Bouclier réparateur|`@Compendium[pf2e.equipment-srd.aPD0z9dBsHqgiCW0]`|
|[Religious Symbol (Silver)](equipment/equipment-00-upzjwQ96cZG0Xlmx.htm)|Symbole religieux (argent)|`@Compendium[pf2e.equipment-srd.upzjwQ96cZG0Xlmx]`|
|[Religious Symbol (Wooden)](equipment/equipment-00-plplsXJsqrdqNQVI.htm)|Symbole religieux (bois)|`@Compendium[pf2e.equipment-srd.plplsXJsqrdqNQVI]`|
|[Religious Text](equipment/equipment-00-z1okOYtNVnpBNj9F.htm)|Texte religieux|`@Compendium[pf2e.equipment-srd.z1okOYtNVnpBNj9F]`|
|[Repair Kit](equipment/equipment-00-vLGDUFrg4yGzpTQX.htm)|Kit de réparation|`@Compendium[pf2e.equipment-srd.vLGDUFrg4yGzpTQX]`|
|[Repair Kit (Superb)](equipment/equipment-03-aDsdYMPpVc8hOnM5.htm)|Kit de réparation (Superbe)|`@Compendium[pf2e.equipment-srd.aDsdYMPpVc8hOnM5]`|
|[Replacement Filter (Level 1)](equipment/equipment-01-khU38JBZBOAxWzhY.htm)||`@Compendium[pf2e.equipment-srd.khU38JBZBOAxWzhY]`|
|[Replacement Filter (Level 10)](equipment/equipment-10-vuvRsWXrzHbs0tF5.htm)||`@Compendium[pf2e.equipment-srd.vuvRsWXrzHbs0tF5]`|
|[Replacement Filter (Level 5)](equipment/equipment-05-CtAg4DpSssKBIw8R.htm)||`@Compendium[pf2e.equipment-srd.CtAg4DpSssKBIw8R]`|
|[Repulsion Resin](equipment/consumable-16-ws3OXRgAawwYIKK6.htm)||`@Compendium[pf2e.equipment-srd.ws3OXRgAawwYIKK6]`|
|[Resilient](equipment/equipment-08-eSjQgsl3pYkirOwk.htm)|Résilience (rune)|`@Compendium[pf2e.equipment-srd.eSjQgsl3pYkirOwk]`|
|[Resilient (Greater)](equipment/equipment-14-fCdcyCkGlmp0c34A.htm)|Résilience supérieure (rune)|`@Compendium[pf2e.equipment-srd.fCdcyCkGlmp0c34A]`|
|[Resilient (Major)](equipment/equipment-20-WKcvvaZ0LxwYreb7.htm)|Résilience majeure (rune)|`@Compendium[pf2e.equipment-srd.WKcvvaZ0LxwYreb7]`|
|[Retribution Axe](equipment/weapon-03-q6ZvspNDkzJSP6dg.htm)|Hâche de représailles|`@Compendium[pf2e.equipment-srd.q6ZvspNDkzJSP6dg]`|
|[Returning](equipment/equipment-03-qlunQzfnzPQpMG6U.htm)|Boomerang (rune)|`@Compendium[pf2e.equipment-srd.qlunQzfnzPQpMG6U]`|
|[Rhino Hide](equipment/armor-09-C4XKMcHZoGzrAZBl.htm)|Peau de rhinocéros|`@Compendium[pf2e.equipment-srd.C4XKMcHZoGzrAZBl]`|
|[Rhodochrosite](equipment/treasure-Ujaoyu3WE6V7Y8Vg.htm)|Rhodochrosite|`@Compendium[pf2e.equipment-srd.Ujaoyu3WE6V7Y8Vg]`|
|[Rhoka Sword](equipment/weapon-00-k8V3wTG1gMU5ksUr.htm)|Épée rhoka|`@Compendium[pf2e.equipment-srd.k8V3wTG1gMU5ksUr]`|
|[Ring of Climbing](equipment/equipment-12-GpuzMoZYehYvZ50E.htm)|Anneau d'escalade|`@Compendium[pf2e.equipment-srd.GpuzMoZYehYvZ50E]`|
|[Ring of Counterspells](equipment/equipment-10-cBKrXbxM02MlycpV.htm)|Anneau de contresort|`@Compendium[pf2e.equipment-srd.cBKrXbxM02MlycpV]`|
|[Ring of Discretion](equipment/equipment-01-FABVnBeYtGXshlwm.htm)||`@Compendium[pf2e.equipment-srd.FABVnBeYtGXshlwm]`|
|[Ring of Energy Resistance](equipment/equipment-06-EkZVXMdtqTTgahiJ.htm)|Anneau de résistance aux énergies|`@Compendium[pf2e.equipment-srd.EkZVXMdtqTTgahiJ]`|
|[Ring of Energy Resistance (Greater)](equipment/equipment-10-X3Nfa7bByYFrg1lU.htm)|Anneau de résistance aux énergies (supérieure)|`@Compendium[pf2e.equipment-srd.X3Nfa7bByYFrg1lU]`|
|[Ring of Energy Resistance (Major)](equipment/equipment-14-zyAzx6fLsPurRFQO.htm)|Anneau de résistance aux énergies (majeur)|`@Compendium[pf2e.equipment-srd.zyAzx6fLsPurRFQO]`|
|[Ring of Lies](equipment/equipment-10-OTeONq4r10Xm6gSy.htm)|Anneau de mensonges|`@Compendium[pf2e.equipment-srd.OTeONq4r10Xm6gSy]`|
|[Ring of Maniacal Devices](equipment/equipment-11-nLvP7U50hLqz26Uy.htm)|Anneau d'objets déments|`@Compendium[pf2e.equipment-srd.nLvP7U50hLqz26Uy]`|
|[Ring of Manical Devices (Greater)](equipment/equipment-18-Wkm2VYPsfGjWBtQe.htm)||`@Compendium[pf2e.equipment-srd.Wkm2VYPsfGjWBtQe]`|
|[Ring of Minor Arcana](equipment/equipment-05-3RPYWoUv1VKGjJ7i.htm)||`@Compendium[pf2e.equipment-srd.3RPYWoUv1VKGjJ7i]`|
|[Ring of Spell Turning](equipment/equipment-20-Ivqd84dkWA8DW8YJ.htm)|Anneau de renvoi de sorts|`@Compendium[pf2e.equipment-srd.Ivqd84dkWA8DW8YJ]`|
|[Ring of Stoneshifting](equipment/equipment-12-FSo4D0bnDeRAURip.htm)||`@Compendium[pf2e.equipment-srd.FSo4D0bnDeRAURip]`|
|[Ring of Sustenance](equipment/equipment-07-14rbefsoClgClRQ8.htm)|Anneau de subsistance|`@Compendium[pf2e.equipment-srd.14rbefsoClgClRQ8]`|
|[Ring of Swimming](equipment/equipment-12-fFx6SEyZlHZtcLGX.htm)|Anneau de nage|`@Compendium[pf2e.equipment-srd.fFx6SEyZlHZtcLGX]`|
|[Ring of Truth](equipment/equipment-10-BKFuMxi0dXu7yFxe.htm)|Anneau de vérité|`@Compendium[pf2e.equipment-srd.BKFuMxi0dXu7yFxe]`|
|[Ring of Wizardry (Type I)](equipment/equipment-07-faKyy6ETkDgrUnvf.htm)|Anneau des arcanes (Type I)|`@Compendium[pf2e.equipment-srd.faKyy6ETkDgrUnvf]`|
|[Ring of Wizardry (Type II)](equipment/equipment-10-2tSDgHfSkkaX4CA4.htm)|Anneau des arcanes (Type II)|`@Compendium[pf2e.equipment-srd.2tSDgHfSkkaX4CA4]`|
|[Ring of Wizardry (Type III)](equipment/equipment-12-otCgznS0L14Z46rf.htm)|Anneau des arcanes (Type III)|`@Compendium[pf2e.equipment-srd.otCgznS0L14Z46rf]`|
|[Ring of Wizardry (Type IV)](equipment/equipment-14-8jPsriZqWY1hAgob.htm)|Anneau des arcanes (Type IV)|`@Compendium[pf2e.equipment-srd.8jPsriZqWY1hAgob]`|
|[Ring of the Ram](equipment/equipment-06-s97FDCHi2UcfzKGn.htm)|Anneau du bélier|`@Compendium[pf2e.equipment-srd.s97FDCHi2UcfzKGn]`|
|[Ring of the Ram (Greater)](equipment/equipment-13-oSltacqbeotmzLNJ.htm)||`@Compendium[pf2e.equipment-srd.oSltacqbeotmzLNJ]`|
|[Ring of the Weary Traveler](equipment/equipment-05-Xv6NlFohsFtIHp6K.htm)||`@Compendium[pf2e.equipment-srd.Xv6NlFohsFtIHp6K]`|
|[Ringmaster's Staff](equipment/weapon-06-tpKD2TZEzAToow1O.htm)||`@Compendium[pf2e.equipment-srd.tpKD2TZEzAToow1O]`|
|[Ringmaster's Staff (Greater)](equipment/weapon-12-wWBi79jkzYGWD6uC.htm)||`@Compendium[pf2e.equipment-srd.wWBi79jkzYGWD6uC]`|
|[Robe of Eyes](equipment/equipment-17-KFfM3Y8SbhdxpbQI.htm)|Robe de vision totale|`@Compendium[pf2e.equipment-srd.KFfM3Y8SbhdxpbQI]`|
|[Robe of the Archmagi](equipment/armor-15-Wkh5rQx3IqInEKdT.htm)|Robe d'Archimage|`@Compendium[pf2e.equipment-srd.Wkh5rQx3IqInEKdT]`|
|[Robe of the Archmagi (Greater)](equipment/armor-19-7ynjS9llFg7tPMoj.htm)|Robe de l'archimage (supérieure)|`@Compendium[pf2e.equipment-srd.7ynjS9llFg7tPMoj]`|
|[Rock-Braced](equipment/equipment-13-1n22FbWdDNC7tLT6.htm)||`@Compendium[pf2e.equipment-srd.1n22FbWdDNC7tLT6]`|
|[Rod of Cancellation](equipment/equipment-20-HuGZspUvJqR09Y8u.htm)||`@Compendium[pf2e.equipment-srd.HuGZspUvJqR09Y8u]`|
|[Rod of Negation](equipment/equipment-14-gEenEoxLjL5z28rG.htm)|Sceptre de négation|`@Compendium[pf2e.equipment-srd.gEenEoxLjL5z28rG]`|
|[Rod of Wonder](equipment/equipment-08-6wVWwpL9pYr3yQtt.htm)|Sceptre merveilleux|`@Compendium[pf2e.equipment-srd.6wVWwpL9pYr3yQtt]`|
|[Rope](equipment/equipment-00-fyYnQf1NAx9fWFaS.htm)|Corde|`@Compendium[pf2e.equipment-srd.fyYnQf1NAx9fWFaS]`|
|[Rope of Climbing (Greater)](equipment/equipment-07-gfBAxuitJje6NL7G.htm)||`@Compendium[pf2e.equipment-srd.gfBAxuitJje6NL7G]`|
|[Rope of Climbing (Lesser)](equipment/equipment-03-G4cmHHhAChAlypFN.htm)||`@Compendium[pf2e.equipment-srd.G4cmHHhAChAlypFN]`|
|[Rope of Climbing (Moderate)](equipment/equipment-05-9XrsaIc80eXhKaoM.htm)||`@Compendium[pf2e.equipment-srd.9XrsaIc80eXhKaoM]`|
|[Rubbing Set](equipment/equipment-00-QFAHoE7FJ9TPCGWL.htm)||`@Compendium[pf2e.equipment-srd.QFAHoE7FJ9TPCGWL]`|
|[Ruby String](equipment/consumable-14-j77uu6eFlsYoZApx.htm)||`@Compendium[pf2e.equipment-srd.j77uu6eFlsYoZApx]`|
|[Ruby, large](equipment/treasure-00-3UOS4sien8IQbPNV.htm)|rubis, gros|`@Compendium[pf2e.equipment-srd.3UOS4sien8IQbPNV]`|
|[Ruby, small](equipment/treasure-00-WuW89GD0TG7RYTLF.htm)|Rubis, petit|`@Compendium[pf2e.equipment-srd.WuW89GD0TG7RYTLF]`|
|[Ruler](equipment/equipment-00-fti8z0JYDxH63n0J.htm)||`@Compendium[pf2e.equipment-srd.fti8z0JYDxH63n0J]`|
|[Runestone](equipment/consumable-01-ev3F9qlMNlNdCOAI.htm)|Pierre runique|`@Compendium[pf2e.equipment-srd.ev3F9qlMNlNdCOAI]`|
|[Sack](equipment/backpack-00-DujblC14ytJEZMaz.htm)|Sac|`@Compendium[pf2e.equipment-srd.DujblC14ytJEZMaz]`|
|[Saddlebags](equipment/backpack-00-mBziC8v2eOtTs0f5.htm)|Fontes|`@Compendium[pf2e.equipment-srd.mBziC8v2eOtTs0f5]`|
|[Sai](equipment/weapon-00-u2u6dkr01AB34tyA.htm)|Saï|`@Compendium[pf2e.equipment-srd.u2u6dkr01AB34tyA]`|
|[Saint's bone with lost scriptures](equipment/treasure-00-aqhEATP0vu4kx15I.htm)|Os de saint avec des écritures perdues|`@Compendium[pf2e.equipment-srd.aqhEATP0vu4kx15I]`|
|[Saints' Balm](equipment/consumable-07-ktRGlgegUBZcr0aJ.htm)||`@Compendium[pf2e.equipment-srd.ktRGlgegUBZcr0aJ]`|
|[Salamander Elixir (Greater)](equipment/consumable-16-XMuLrJYL6fxv4YNA.htm)|Elixir de la salamandre (supérieur)|`@Compendium[pf2e.equipment-srd.XMuLrJYL6fxv4YNA]`|
|[Salamander Elixir (Lesser)](equipment/consumable-04-Ekk7o1gPu8RotixD.htm)|Elixir de la salamandre (inférieur)|`@Compendium[pf2e.equipment-srd.Ekk7o1gPu8RotixD]`|
|[Salamander Elixir (Moderate)](equipment/consumable-12-EEJiqzU89Ofk7dr6.htm)|Elixir de la salamandre (modéré)|`@Compendium[pf2e.equipment-srd.EEJiqzU89Ofk7dr6]`|
|[Salve of Antiparalysis](equipment/consumable-06-ZEKmCg8K2hUHbmnT.htm)|Baume antiparalysie|`@Compendium[pf2e.equipment-srd.ZEKmCg8K2hUHbmnT]`|
|[Salve of Antiparalysis (Greater)](equipment/consumable-12-ybu2BMu2oWj74wl8.htm)|Baume antiparalysie (supérieur)|`@Compendium[pf2e.equipment-srd.ybu2BMu2oWj74wl8]`|
|[Salve of Slipperiness](equipment/consumable-05-MobYbxEL4KgxVi63.htm)|Onguent d'insaisissabilité|`@Compendium[pf2e.equipment-srd.MobYbxEL4KgxVi63]`|
|[Sap](equipment/weapon-00-TLQErnOpM9Luy7rL.htm)|Matraque|`@Compendium[pf2e.equipment-srd.TLQErnOpM9Luy7rL]`|
|[Sapphire](equipment/treasure-00-yTYPPB7WrhanBPbu.htm)|Saphir|`@Compendium[pf2e.equipment-srd.yTYPPB7WrhanBPbu]`|
|[Sard](equipment/treasure-00-mixyMNYUClKfqURh.htm)|Sardoine|`@Compendium[pf2e.equipment-srd.mixyMNYUClKfqURh]`|
|[Sardonyx](equipment/treasure-00-AgsZScsqv3puTmZx.htm)|Sardonyx|`@Compendium[pf2e.equipment-srd.AgsZScsqv3puTmZx]`|
|[Satchel](equipment/backpack-00-tYBW8TBF8WOAp3x0.htm)|Sacoche|`@Compendium[pf2e.equipment-srd.tYBW8TBF8WOAp3x0]`|
|[Savior Spike](equipment/consumable-02-qeTWg0TWw9CwMKCO.htm)|Pointe salvatrice|`@Compendium[pf2e.equipment-srd.qeTWg0TWw9CwMKCO]`|
|[Sawtooth Saber](equipment/weapon-00-wkzxLpSe7LN6c5Ld.htm)|Sabre dentelé|`@Compendium[pf2e.equipment-srd.wkzxLpSe7LN6c5Ld]`|
|[Scale Mail](equipment/armor-00-YMQr577asquZIP65.htm)|Armure d'écailles|`@Compendium[pf2e.equipment-srd.YMQr577asquZIP65]`|
|[Scholarly Journal](equipment/equipment-03-hywANJCzT7hMgWna.htm)||`@Compendium[pf2e.equipment-srd.hywANJCzT7hMgWna]`|
|[Scholarly Journal Compendium](equipment/equipment-03-5rkGXSbaqBbY4MiR.htm)||`@Compendium[pf2e.equipment-srd.5rkGXSbaqBbY4MiR]`|
|[Scimitar](equipment/weapon-00-grmaV4GdoGD7sKbn.htm)|Cimeterre|`@Compendium[pf2e.equipment-srd.grmaV4GdoGD7sKbn]`|
|[Scorpion Whip](equipment/weapon-00-UXjKXqsfWYiayeMD.htm)|Fouet scorpion|`@Compendium[pf2e.equipment-srd.UXjKXqsfWYiayeMD]`|
|[Scour](equipment/consumable-08-8BDBsf55gP0UW07Y.htm)|Érode|`@Compendium[pf2e.equipment-srd.8BDBsf55gP0UW07Y]`|
|[Scourge](equipment/weapon-00-I5DJUbAFGMwa6qCz.htm)|Fléau|`@Compendium[pf2e.equipment-srd.I5DJUbAFGMwa6qCz]`|
|[Scrimshaw whale bone](equipment/treasure-00-8P87jwOAC3zMuzoF.htm)|Gravure en os de baleine|`@Compendium[pf2e.equipment-srd.8P87jwOAC3zMuzoF]`|
|[Scroll Case](equipment/equipment-00-yxDOmx3VYSWoAEMa.htm)|Étui à parchemin|`@Compendium[pf2e.equipment-srd.yxDOmx3VYSWoAEMa]`|
|[Scroll of 10th-level Spell](equipment/consumable-19-o1XIHJ4MJyroAHfF.htm)|Parchemin de sort de niveau 10|`@Compendium[pf2e.equipment-srd.o1XIHJ4MJyroAHfF]`|
|[Scroll of 1st-level Spell](equipment/consumable-01-RjuupS9xyXDLgyIr.htm)|Parchemin de sort de niveau 1|`@Compendium[pf2e.equipment-srd.RjuupS9xyXDLgyIr]`|
|[Scroll of 2nd-level Spell](equipment/consumable-03-Y7UD64foDbDMV9sx.htm)|Parchemin de sort de niveau 2|`@Compendium[pf2e.equipment-srd.Y7UD64foDbDMV9sx]`|
|[Scroll of 3rd-level Spell](equipment/consumable-05-ZmefGBXGJF3CFDbn.htm)|Parchemin de sort de niveau 3|`@Compendium[pf2e.equipment-srd.ZmefGBXGJF3CFDbn]`|
|[Scroll of 4th-level Spell](equipment/consumable-07-QSQZJ5BC3DeHv153.htm)|Parchemin de sort de niveau 4|`@Compendium[pf2e.equipment-srd.QSQZJ5BC3DeHv153]`|
|[Scroll of 5th-level Spell](equipment/consumable-09-tjLvRWklAylFhBHQ.htm)|Parchemin de sort de niveau 5|`@Compendium[pf2e.equipment-srd.tjLvRWklAylFhBHQ]`|
|[Scroll of 6th-level Spell](equipment/consumable-11-4sGIy77COooxhQuC.htm)|Parchemin de sort de niveau 6|`@Compendium[pf2e.equipment-srd.4sGIy77COooxhQuC]`|
|[Scroll of 7th-level Spell](equipment/consumable-13-fomEZZ4MxVVK3uVu.htm)|Parchemin de sort de niveau 7|`@Compendium[pf2e.equipment-srd.fomEZZ4MxVVK3uVu]`|
|[Scroll of 8th-level Spell](equipment/consumable-15-iPki3yuoucnj7bIt.htm)|Parchemin de sort de niveau 8|`@Compendium[pf2e.equipment-srd.iPki3yuoucnj7bIt]`|
|[Scroll of 9th-level Spell](equipment/consumable-17-cFHomF3tty8Wi1e5.htm)|Parchemin de sort de niveau 9|`@Compendium[pf2e.equipment-srd.cFHomF3tty8Wi1e5]`|
|[Scrollstaff](equipment/weapon-01-WS78LUHzlpeONMRo.htm)|Bâton-parchemin|`@Compendium[pf2e.equipment-srd.WS78LUHzlpeONMRo]`|
|[Scythe](equipment/weapon-00-H74vYwHJ8XT4qOPI.htm)|Faux|`@Compendium[pf2e.equipment-srd.H74vYwHJ8XT4qOPI]`|
|[Scything Blade Snare](equipment/consumable-12-mWXaROc9ytjdGVVP.htm)||`@Compendium[pf2e.equipment-srd.mWXaROc9ytjdGVVP]`|
|[Sea Touch Elixir (Greater)](equipment/consumable-15-i4D4B7tpYv9vMvQY.htm)|Elixir de caresse marine (supérieur)|`@Compendium[pf2e.equipment-srd.i4D4B7tpYv9vMvQY]`|
|[Sea Touch Elixir (Lesser)](equipment/consumable-05-zcJgT5RS8p2MEbOB.htm)|Elixir de caresse marine (inférieur)|`@Compendium[pf2e.equipment-srd.zcJgT5RS8p2MEbOB]`|
|[Sea Touch Elixir (Moderate)](equipment/consumable-12-2hc1EEcb3pfr7Hac.htm)|Elixir de caresse marine (modéré)|`@Compendium[pf2e.equipment-srd.2hc1EEcb3pfr7Hac]`|
|[Sealing Chest (Greater)](equipment/equipment-11-JPVQ0qvTaKkTF2z4.htm)||`@Compendium[pf2e.equipment-srd.JPVQ0qvTaKkTF2z4]`|
|[Sealing Chest (Lesser)](equipment/equipment-04-g9pOZsJykSihdyrL.htm)||`@Compendium[pf2e.equipment-srd.g9pOZsJykSihdyrL]`|
|[Sealing Chest (Major)](equipment/equipment-14-iwC6V10pTd6Sk0ML.htm)||`@Compendium[pf2e.equipment-srd.iwC6V10pTd6Sk0ML]`|
|[Sealing Chest (Moderate)](equipment/equipment-07-nI5x32JWjcEqYCq6.htm)||`@Compendium[pf2e.equipment-srd.nI5x32JWjcEqYCq6]`|
|[Serene Mutagen (Greater)](equipment/consumable-11-Yej7lnnDYDZybGqo.htm)||`@Compendium[pf2e.equipment-srd.Yej7lnnDYDZybGqo]`|
|[Serene Mutagen (Lesser)](equipment/consumable-01-bOPQDM54W8ZDoULY.htm)|Mutagène de sérénité (inférieur)|`@Compendium[pf2e.equipment-srd.bOPQDM54W8ZDoULY]`|
|[Serene Mutagen (Major)](equipment/consumable-17-ccrdVliTNBh2mNZf.htm)||`@Compendium[pf2e.equipment-srd.ccrdVliTNBh2mNZf]`|
|[Serene Mutagen (Moderate)](equipment/consumable-03-XEYveTvLH1lJ4jeI.htm)|Mutagène de sérénité (modéré)|`@Compendium[pf2e.equipment-srd.XEYveTvLH1lJ4jeI]`|
|[Serithtial](equipment/weapon-23-emGyagWNmjvtjiGK.htm)|Sérithtial|`@Compendium[pf2e.equipment-srd.emGyagWNmjvtjiGK]`|
|[Serrating](equipment/consumable-10-SV7W0lC2d8mfYuhy.htm)||`@Compendium[pf2e.equipment-srd.SV7W0lC2d8mfYuhy]`|
|[Serum of Sex Shift](equipment/consumable-07-9ignmYCACjfzkxDQ.htm)|Sérum de changement de sexe|`@Compendium[pf2e.equipment-srd.9ignmYCACjfzkxDQ]`|
|[Set of decorated ceramic plates](equipment/treasure-00-RKX4vLWyczPwfsKS.htm)|Collection d'assiettes en céramique décorées|`@Compendium[pf2e.equipment-srd.RKX4vLWyczPwfsKS]`|
|[Set of decorated porcelain plates](equipment/treasure-00-rVfFUBb0r423IPg8.htm)|Collection d'assiettes en porcelaine décorées|`@Compendium[pf2e.equipment-srd.rVfFUBb0r423IPg8]`|
|[Set of six ivory dice](equipment/treasure-00-69IWSryF5BWkwWXY.htm)|Jeu de dés à six faces en ivoire|`@Compendium[pf2e.equipment-srd.69IWSryF5BWkwWXY]`|
|[Shadow](equipment/equipment-03-kEy7Uc1VisizGgtf.htm)|Ombre (rune)|`@Compendium[pf2e.equipment-srd.kEy7Uc1VisizGgtf]`|
|[Shadow (Greater)](equipment/equipment-09-bSm0Hki8N2L50OZw.htm)|Ombre supérieure (rune)|`@Compendium[pf2e.equipment-srd.bSm0Hki8N2L50OZw]`|
|[Shadow (Major)](equipment/equipment-17-2FjdEflsVldnuebM.htm)|Ombre majeure (rune)|`@Compendium[pf2e.equipment-srd.2FjdEflsVldnuebM]`|
|[Shadow Essence](equipment/consumable-10-Wjkw0lEUOhypYvzo.htm)||`@Compendium[pf2e.equipment-srd.Wjkw0lEUOhypYvzo]`|
|[Shark Tooth Charm](equipment/consumable-05-Tj4uaNw2lgevxGl7.htm)|Charme en dent de requin|`@Compendium[pf2e.equipment-srd.Tj4uaNw2lgevxGl7]`|
|[Shears](equipment/weapon-00-IF6qUrR3i030v0dH.htm)|Cisailles|`@Compendium[pf2e.equipment-srd.IF6qUrR3i030v0dH]`|
|[Sheath](equipment/equipment-00-Zycu6zaGvDsqLH5g.htm)|Gaine|`@Compendium[pf2e.equipment-srd.Zycu6zaGvDsqLH5g]`|
|[Shell](equipment/treasure-00-6vb56WClb06JrpuJ.htm)|Coquillage|`@Compendium[pf2e.equipment-srd.6vb56WClb06JrpuJ]`|
|[Shield Bash](equipment/weapon-00-ZhxxqYpVdVx0jSMm.htm)|Coup de bouclier|`@Compendium[pf2e.equipment-srd.ZhxxqYpVdVx0jSMm]`|
|[Shield Boss](equipment/weapon-00-dfum7DpOEkwxwTsT.htm)|Ombon|`@Compendium[pf2e.equipment-srd.dfum7DpOEkwxwTsT]`|
|[Shield Sconce](equipment/equipment-00-XV2EHt1RWHqzPeUT.htm)||`@Compendium[pf2e.equipment-srd.XV2EHt1RWHqzPeUT]`|
|[Shield Spikes](equipment/weapon-00-nSO0Z662LkkLfa2u.htm)|Pointes de bouclier|`@Compendium[pf2e.equipment-srd.nSO0Z662LkkLfa2u]`|
|[Shield of the Unified Legion](equipment/armor-20-PlYr5AhYwHIztMw2.htm)||`@Compendium[pf2e.equipment-srd.PlYr5AhYwHIztMw2]`|
|[Shielding Salve](equipment/consumable-01-EWujUs3YmlBu2jhm.htm)||`@Compendium[pf2e.equipment-srd.EWujUs3YmlBu2jhm]`|
|[Shifting](equipment/equipment-06-roeYtwlIe65BPMJ1.htm)|Changeante (rune)|`@Compendium[pf2e.equipment-srd.roeYtwlIe65BPMJ1]`|
|[Shining Ammunition](equipment/consumable-01-NI7twU2G6UCDmvCO.htm)|Munition luisante|`@Compendium[pf2e.equipment-srd.NI7twU2G6UCDmvCO]`|
|[Shining Wayfinder](equipment/equipment-04-z6eQdPTgSxTLR1Qr.htm)||`@Compendium[pf2e.equipment-srd.z6eQdPTgSxTLR1Qr]`|
|[Shiver](equipment/consumable-03-UfoaNXFA7AWZiXxI.htm)|Grelotte|`@Compendium[pf2e.equipment-srd.UfoaNXFA7AWZiXxI]`|
|[Shock](equipment/equipment-08-NVst7e69agGG9Qwd.htm)|Foudre (rune)|`@Compendium[pf2e.equipment-srd.NVst7e69agGG9Qwd]`|
|[Shock (Greater)](equipment/equipment-15-TEa1oKZbwsOvC6TZ.htm)|Foudre supérieure (rune)|`@Compendium[pf2e.equipment-srd.TEa1oKZbwsOvC6TZ]`|
|[Shoony Shovel](equipment/equipment-09-aR0tsZ3xi1iwr53u.htm)||`@Compendium[pf2e.equipment-srd.aR0tsZ3xi1iwr53u]`|
|[Shortbow](equipment/weapon-00-hIgqLgH3YcLZBeoT.htm)|Arc court|`@Compendium[pf2e.equipment-srd.hIgqLgH3YcLZBeoT]`|
|[Shortsword](equipment/weapon-00-7tKkkF8eZ4iCLJtp.htm)|Épée courte|`@Compendium[pf2e.equipment-srd.7tKkkF8eZ4iCLJtp]`|
|[Shot of the First Vault](equipment/equipment-23-6o6zvitwlNHNaNJM.htm)|Projectile du premier coffre|`@Compendium[pf2e.equipment-srd.6o6zvitwlNHNaNJM]`|
|[Shrinking Potion](equipment/consumable-04-FqbDpztscJfM4XMe.htm)|Potion de rétrécissement (standard)|`@Compendium[pf2e.equipment-srd.FqbDpztscJfM4XMe]`|
|[Shrinking Potion (Greater)](equipment/consumable-08-5xsfj30uXMKINxnk.htm)|Potion de rétrécissement|`@Compendium[pf2e.equipment-srd.5xsfj30uXMKINxnk]`|
|[Shuriken](equipment/weapon-00-fjkwYZ0hRmBztwBG.htm)|Shuriken|`@Compendium[pf2e.equipment-srd.fjkwYZ0hRmBztwBG]`|
|[Sickle](equipment/weapon-00-ynnBwzkzsR6B73iO.htm)|Serpe|`@Compendium[pf2e.equipment-srd.ynnBwzkzsR6B73iO]`|
|[Sight-Theft Grit](equipment/consumable-09-xvJ2jzqipBNrwZ2w.htm)||`@Compendium[pf2e.equipment-srd.xvJ2jzqipBNrwZ2w]`|
|[Signal Whistle](equipment/equipment-00-USHK6XQRwmq17xKh.htm)|Sifflet de signalisation|`@Compendium[pf2e.equipment-srd.USHK6XQRwmq17xKh]`|
|[Signaling Snare](equipment/consumable-01-XcD8p1o71tPohZWT.htm)|Piège artisanal indicateur|`@Compendium[pf2e.equipment-srd.XcD8p1o71tPohZWT]`|
|[Silk ceremonial armor](equipment/treasure-fUENXLX3tzi33CcB.htm)|Armure de soie cérémonielle|`@Compendium[pf2e.equipment-srd.fUENXLX3tzi33CcB]`|
|[Silk fan decorated with turquoise](equipment/treasure-00-vJLb8wwipdwFQDH3.htm)|Éventail en soie rehaussé de turquoises|`@Compendium[pf2e.equipment-srd.vJLb8wwipdwFQDH3]`|
|[Silk mask decorated with citrines](equipment/treasure-00-8RH0e9UqXIxAPJJG.htm)|Masque en soie décoré de citrines|`@Compendium[pf2e.equipment-srd.8RH0e9UqXIxAPJJG]`|
|[Silver Buckler (High-Grade)](equipment/armor-15-LboYDYZ0IbDWuWMl.htm)|targe en argent de haute qualité|`@Compendium[pf2e.equipment-srd.LboYDYZ0IbDWuWMl]`|
|[Silver Buckler (Low-Grade)](equipment/armor-02-aA9clnIP3deHNNjo.htm)|Targe en argent (basse qualité)|`@Compendium[pf2e.equipment-srd.aA9clnIP3deHNNjo]`|
|[Silver Buckler (Standard-Grade)](equipment/armor-07-8DNpWWeL7X9MDG0i.htm)|Targe en argent (qualité standard)|`@Compendium[pf2e.equipment-srd.8DNpWWeL7X9MDG0i]`|
|[Silver Chunk](equipment/equipment-00-Ld8duhvwTQ5lCQmF.htm)|Morceau d'argent|`@Compendium[pf2e.equipment-srd.Ld8duhvwTQ5lCQmF]`|
|[Silver Dragon's Breath Potion (Adult)](equipment/consumable-12-t9wFCiFkCRcSLtaG.htm)|Potion de souffle du dragon d'argent adulte|`@Compendium[pf2e.equipment-srd.t9wFCiFkCRcSLtaG]`|
|[Silver Dragon's Breath Potion (Wyrm)](equipment/consumable-17-PW3H8TnouiLaTC8G.htm)|Potion de souffle de dracosire d'argent|`@Compendium[pf2e.equipment-srd.PW3H8TnouiLaTC8G]`|
|[Silver Dragon's Breath Potion (Young)](equipment/consumable-07-LKwI47eeJC0Y4hGu.htm)|Potion de souffle du jeune dragon d'argent|`@Compendium[pf2e.equipment-srd.LKwI47eeJC0Y4hGu]`|
|[Silver Ingot](equipment/equipment-00-jerA2fFiK5wb32r5.htm)|Lingot d'argent|`@Compendium[pf2e.equipment-srd.jerA2fFiK5wb32r5]`|
|[Silver Pieces](equipment/treasure-00-5Ew82vBF9YfaiY9f.htm)|Pièces d'argent|`@Compendium[pf2e.equipment-srd.5Ew82vBF9YfaiY9f]`|
|[Silver Shield (High-Grade)](equipment/armor-15-pRzafUKQviDSMRJp.htm)|bouclier en argent de haute qualité|`@Compendium[pf2e.equipment-srd.pRzafUKQviDSMRJp]`|
|[Silver Shield (Low-Grade)](equipment/armor-02-1xUIdz23mIlYWGPL.htm)|Bouclier en argent (basse qualité)|`@Compendium[pf2e.equipment-srd.1xUIdz23mIlYWGPL]`|
|[Silver Shield (Standard-Grade)](equipment/armor-07-KVj9RP2qvpsHHGqE.htm)|Bouclier en argent (qualité standard)|`@Compendium[pf2e.equipment-srd.KVj9RP2qvpsHHGqE]`|
|[Silver and jade censer](equipment/treasure-00-tMMYXdHps9QakCVn.htm)|Encensoir d'argent et de jade|`@Compendium[pf2e.equipment-srd.tMMYXdHps9QakCVn]`|
|[Silver coronet with peridots](equipment/treasure-00-Gi4vx84LhppOQAxr.htm)|Diadème d'argent avec des péridots|`@Compendium[pf2e.equipment-srd.Gi4vx84LhppOQAxr]`|
|[Silver flagon inscribed with fields](equipment/treasure-00-FI8PLxBFmHF6o0pH.htm)|Chope en argent gravée de champs|`@Compendium[pf2e.equipment-srd.FI8PLxBFmHF6o0pH]`|
|[Silver mirror with gilded frame](equipment/treasure-00-5dibOpDYOhJY6Ssn.htm)|Miroir en argent avec un cadre doré|`@Compendium[pf2e.equipment-srd.5dibOpDYOhJY6Ssn]`|
|[Silver statuette of a raven](equipment/treasure-00-5EYlhm8ZbR6dLUHF.htm)|Statuette de corbeau en argent|`@Compendium[pf2e.equipment-srd.5EYlhm8ZbR6dLUHF]`|
|[Silversheen](equipment/consumable-02-8fbsKEEbZ0CfBMIr.htm)|Lustrargent|`@Compendium[pf2e.equipment-srd.8fbsKEEbZ0CfBMIr]`|
|[Silvertongue Mutagen (Greater)](equipment/consumable-11-uggpIk6vguWFXVli.htm)|Mutagène de langue dorée (supérieur)|`@Compendium[pf2e.equipment-srd.uggpIk6vguWFXVli]`|
|[Silvertongue Mutagen (Lesser)](equipment/consumable-01-YwRAHWW8yUI07sy9.htm)|Mutagène de langue dorée (inférieur)|`@Compendium[pf2e.equipment-srd.YwRAHWW8yUI07sy9]`|
|[Silvertongue Mutagen (Major)](equipment/consumable-17-uD5vRYVOXNJ53sEE.htm)||`@Compendium[pf2e.equipment-srd.uD5vRYVOXNJ53sEE]`|
|[Silvertongue Mutagen (Moderate)](equipment/consumable-03-lIExlUFBKvBue8hb.htm)|Mutagène de langue dorée (modéré)|`@Compendium[pf2e.equipment-srd.lIExlUFBKvBue8hb]`|
|[Simple painting](equipment/treasure-00-bgswgiXV12HW0aEz.htm)|Peinture simple|`@Compendium[pf2e.equipment-srd.bgswgiXV12HW0aEz]`|
|[Simple sculpture](equipment/treasure-00-2T33bWTWXj1v2J03.htm)|Sculpture simple|`@Compendium[pf2e.equipment-srd.2T33bWTWXj1v2J03]`|
|[Simple silver circlet](equipment/treasure-00-CNLt74pUxM0GGjyl.htm)|Bandeau simple en argent|`@Compendium[pf2e.equipment-srd.CNLt74pUxM0GGjyl]`|
|[Sinew-Shock Serum (Greater)](equipment/consumable-12-62HxCEDwhlZaeR0Q.htm)||`@Compendium[pf2e.equipment-srd.62HxCEDwhlZaeR0Q]`|
|[Sinew-Shock Serum (Lesser)](equipment/consumable-02-1TWHN8RbimPVXM0U.htm)||`@Compendium[pf2e.equipment-srd.1TWHN8RbimPVXM0U]`|
|[Sinew-Shock Serum (Major)](equipment/consumable-18-YIgIF1845n5UBFnE.htm)||`@Compendium[pf2e.equipment-srd.YIgIF1845n5UBFnE]`|
|[Sinew-Shock Serum (Moderate)](equipment/consumable-04-tslVf3qtQE7V1YvG.htm)||`@Compendium[pf2e.equipment-srd.tslVf3qtQE7V1YvG]`|
|[Singing Bowl of the Versatile Stance](equipment/equipment-11-A7TCZXIwyxdqYSas.htm)||`@Compendium[pf2e.equipment-srd.A7TCZXIwyxdqYSas]`|
|[Singing Sword](equipment/weapon-05-OKAR7GIyJac8dmsi.htm)|Épée chantante|`@Compendium[pf2e.equipment-srd.OKAR7GIyJac8dmsi]`|
|[Sinister Knight](equipment/equipment-08-QDYPr19De3TBIysx.htm)||`@Compendium[pf2e.equipment-srd.QDYPr19De3TBIysx]`|
|[Sixfingers Elixir (Greater)](equipment/consumable-17-6odVabL0WL2H89Ic.htm)||`@Compendium[pf2e.equipment-srd.6odVabL0WL2H89Ic]`|
|[Sixfingers Elixir (Lesser)](equipment/consumable-05-JAOWw2GhupaYoYg9.htm)||`@Compendium[pf2e.equipment-srd.JAOWw2GhupaYoYg9]`|
|[Sixfingers Elixir (Moderate)](equipment/consumable-11-ieVRS2BjiWqauly6.htm)||`@Compendium[pf2e.equipment-srd.ieVRS2BjiWqauly6]`|
|[Skarja's Heartstone](equipment/equipment-13-Jp9K5Q9t9ZiDGSaI.htm)||`@Compendium[pf2e.equipment-srd.Jp9K5Q9t9ZiDGSaI]`|
|[Skeleton Key](equipment/equipment-05-fprUZviW8khm2BLo.htm)|Passe-partout|`@Compendium[pf2e.equipment-srd.fprUZviW8khm2BLo]`|
|[Skeleton Key (Greater)](equipment/equipment-11-AFE073UYI0mkWuUs.htm)|Passe_partout (supérieur)|`@Compendium[pf2e.equipment-srd.AFE073UYI0mkWuUs]`|
|[Skeptic's Elixir (Greater)](equipment/consumable-11-aOySqBbrrXfII1LS.htm)||`@Compendium[pf2e.equipment-srd.aOySqBbrrXfII1LS]`|
|[Skeptic's Elixir (Lesser)](equipment/consumable-01-rGyv7i41pSs2KXSj.htm)||`@Compendium[pf2e.equipment-srd.rGyv7i41pSs2KXSj]`|
|[Skeptic's Elixir (Moderate)](equipment/consumable-06-lZZiULs7ZmCbzewq.htm)||`@Compendium[pf2e.equipment-srd.lZZiULs7ZmCbzewq]`|
|[Skinsaw Mask](equipment/equipment-03-sgH9U2fKqWCedKNO.htm)||`@Compendium[pf2e.equipment-srd.sgH9U2fKqWCedKNO]`|
|[Skinstitch Salve](equipment/consumable-07-FgAPV0iLE6R1QMJ5.htm)||`@Compendium[pf2e.equipment-srd.FgAPV0iLE6R1QMJ5]`|
|[Sky Hammer](equipment/weapon-20-ANvbi1zKF1So8bON.htm)|Marteau céleste|`@Compendium[pf2e.equipment-srd.ANvbi1zKF1So8bON]`|
|[Slates of Distant Letters](equipment/equipment-13-1IeT2jIpBxxUo1uD.htm)||`@Compendium[pf2e.equipment-srd.1IeT2jIpBxxUo1uD]`|
|[Sleep Arrow](equipment/consumable-03-RcQ4ZIzRK2xLf4G5.htm)|Flèche soporifique|`@Compendium[pf2e.equipment-srd.RcQ4ZIzRK2xLf4G5]`|
|[Sleeves of Storage](equipment/equipment-04-1pglC9PQx6yOgcKL.htm)||`@Compendium[pf2e.equipment-srd.1pglC9PQx6yOgcKL]`|
|[Sleeves of Storage (Greater)](equipment/equipment-09-3hv6NVC2rVu4QCNt.htm)||`@Compendium[pf2e.equipment-srd.3hv6NVC2rVu4QCNt]`|
|[Slick](equipment/equipment-03-uQOaRpfkUFVYD0Gx.htm)|Glissante (rune)|`@Compendium[pf2e.equipment-srd.uQOaRpfkUFVYD0Gx]`|
|[Slick (Greater)](equipment/equipment-08-LiJMupjynmkM5Mld.htm)||`@Compendium[pf2e.equipment-srd.LiJMupjynmkM5Mld]`|
|[Slick (Major)](equipment/equipment-16-9imz3VgBXCg13RfT.htm)||`@Compendium[pf2e.equipment-srd.9imz3VgBXCg13RfT]`|
|[Slime Whip](equipment/weapon-07-USzeQdvsXz6P5L2k.htm)||`@Compendium[pf2e.equipment-srd.USzeQdvsXz6P5L2k]`|
|[Sling](equipment/weapon-00-UCH4myuFnokGv0vF.htm)|Fronde|`@Compendium[pf2e.equipment-srd.UCH4myuFnokGv0vF]`|
|[Sling Bullets](equipment/consumable-00-MKSeXwUm56c15MZa.htm)|Billes de fronde|`@Compendium[pf2e.equipment-srd.MKSeXwUm56c15MZa]`|
|[Slippers of Spider Climbing](equipment/equipment-07-skBa6D1uxb0b2USn.htm)|Chaussons de l'araignée|`@Compendium[pf2e.equipment-srd.skBa6D1uxb0b2USn]`|
|[Slippery Ribbon](equipment/consumable-01-H2BqcfujumZVWF69.htm)||`@Compendium[pf2e.equipment-srd.H2BqcfujumZVWF69]`|
|[Slumber Wine](equipment/consumable-12-z7eOUqVwyht6tj11.htm)|Vin soporifique|`@Compendium[pf2e.equipment-srd.z7eOUqVwyht6tj11]`|
|[Small cold iron cauldron with onyx](equipment/treasure-00-yv7OfuEaXTm6YCAc.htm)|petit chaudron en fer froid avec des onyx|`@Compendium[pf2e.equipment-srd.yv7OfuEaXTm6YCAc]`|
|[Smokestick (Greater)](equipment/consumable-07-ehss8yPTXxiUdVlJ.htm)|Bâton fumigène (supérieur)|`@Compendium[pf2e.equipment-srd.ehss8yPTXxiUdVlJ]`|
|[Smokestick (Lesser)](equipment/consumable-01-MoBlVd36uD9xVvZC.htm)|Bâton fumigène (inférieur)|`@Compendium[pf2e.equipment-srd.MoBlVd36uD9xVvZC]`|
|[Smoking Sword](equipment/weapon-03-onFxlajKyWpHZcXt.htm)||`@Compendium[pf2e.equipment-srd.onFxlajKyWpHZcXt]`|
|[Snagging Hook Snare](equipment/consumable-10-j9G3tnrKQ1N1dLzN.htm)||`@Compendium[pf2e.equipment-srd.j9G3tnrKQ1N1dLzN]`|
|[Snake Oil](equipment/consumable-01-xBZCVHAa1SnR8Xul.htm)|Huile de serpent|`@Compendium[pf2e.equipment-srd.xBZCVHAa1SnR8Xul]`|
|[Snapleaf](equipment/consumable-03-5KYn9J1Hj4IG3Z0X.htm)||`@Compendium[pf2e.equipment-srd.5KYn9J1Hj4IG3Z0X]`|
|[Snare Kit](equipment/equipment-00-h7gsRKv6rORYgsv0.htm)|Nécessaire de fabrication de pièges artisanaux :|`@Compendium[pf2e.equipment-srd.h7gsRKv6rORYgsv0]`|
|[Snare Kit (Specialist)](equipment/equipment-03-P2DjThbSOWwra49r.htm)|Nécessaire de fabrication de pièges artisanaux spécialisé|`@Compendium[pf2e.equipment-srd.P2DjThbSOWwra49r]`|
|[Sneaky Key](equipment/consumable-05-1v1OK06JxdXn6MP4.htm)|Clé sournoise|`@Compendium[pf2e.equipment-srd.1v1OK06JxdXn6MP4]`|
|[Snowshoes](equipment/equipment-00-AnJiuDSgJ1uBYRaZ.htm)||`@Compendium[pf2e.equipment-srd.AnJiuDSgJ1uBYRaZ]`|
|[Snowshoes of the Long Trek](equipment/equipment-05-aneZJVb3sbpG2fN7.htm)||`@Compendium[pf2e.equipment-srd.aneZJVb3sbpG2fN7]`|
|[Soap](equipment/equipment-00-81aHsD27HFGnq1Nt.htm)|Savon|`@Compendium[pf2e.equipment-srd.81aHsD27HFGnq1Nt]`|
|[Solidified moment of time](equipment/treasure-00-rIhvmrRVr51VgfKa.htm)|Moment de temps solidifié|`@Compendium[pf2e.equipment-srd.rIhvmrRVr51VgfKa]`|
|[Sovereign Glue](equipment/consumable-07-7IrQPyMm76nLVoXx.htm)||`@Compendium[pf2e.equipment-srd.7IrQPyMm76nLVoXx]`|
|[Sovereign Steel Chunk](equipment/equipment-00-f0lZf1oZYDEEeyYl.htm)||`@Compendium[pf2e.equipment-srd.f0lZf1oZYDEEeyYl]`|
|[Sovereign Steel Ingot](equipment/equipment-00-xuTO0Lbu999HnBhK.htm)||`@Compendium[pf2e.equipment-srd.xuTO0Lbu999HnBhK]`|
|[Spangled Rider's Suit](equipment/armor-09-Blp9Ha1YeIJOeFls.htm)||`@Compendium[pf2e.equipment-srd.Blp9Ha1YeIJOeFls]`|
|[Sparkblade](equipment/weapon-03-5BU4hJ1oXMLtSkuA.htm)||`@Compendium[pf2e.equipment-srd.5BU4hJ1oXMLtSkuA]`|
|[Spear](equipment/weapon-00-tOhoGvmCMw4JpWcS.htm)|Lance|`@Compendium[pf2e.equipment-srd.tOhoGvmCMw4JpWcS]`|
|[Spear Frog Poison](equipment/consumable-01-CeKPffF0FaEyhLMp.htm)||`@Compendium[pf2e.equipment-srd.CeKPffF0FaEyhLMp]`|
|[Spectacles of Understanding](equipment/equipment-05-x4l3JjePzROLDSK6.htm)||`@Compendium[pf2e.equipment-srd.x4l3JjePzROLDSK6]`|
|[Spectacles of Understanding (Greater)](equipment/equipment-11-ZKFmyG9fIoQWIrql.htm)||`@Compendium[pf2e.equipment-srd.ZKFmyG9fIoQWIrql]`|
|[Spectral Nightshade](equipment/consumable-13-C0Zhu7Vwy9Aipwoh.htm)||`@Compendium[pf2e.equipment-srd.C0Zhu7Vwy9Aipwoh]`|
|[Speed](equipment/equipment-16-KnZL0xPWDzQx9vWQ.htm)|Rapide (rune)|`@Compendium[pf2e.equipment-srd.KnZL0xPWDzQx9vWQ]`|
|[Spell-Eating Pitch](equipment/consumable-12-YScPBPwB4t9sydp0.htm)||`@Compendium[pf2e.equipment-srd.YScPBPwB4t9sydp0]`|
|[Spell-Storing](equipment/equipment-13-payq4TwkN2BRF6fs.htm)|Stockage de sort (rune)|`@Compendium[pf2e.equipment-srd.payq4TwkN2BRF6fs]`|
|[Spellbook (Blank)](equipment/equipment-00-FOWF5f0tCaApv9RE.htm)|Grimoire|`@Compendium[pf2e.equipment-srd.FOWF5f0tCaApv9RE]`|
|[Spellguard Blade](equipment/weapon-07-v6LpzpIA0BmKvEtK.htm)||`@Compendium[pf2e.equipment-srd.v6LpzpIA0BmKvEtK]`|
|[Spellguard Shield](equipment/armor-06-OqDAx4HJ39ojVtvg.htm)|Bouclier Gardesort|`@Compendium[pf2e.equipment-srd.OqDAx4HJ39ojVtvg]`|
|[Spellstrike Ammunition (Type I)](equipment/consumable-03-NSo0bFX7DGGjqKKl.htm)|Munition de frappe magique (Type I)|`@Compendium[pf2e.equipment-srd.NSo0bFX7DGGjqKKl]`|
|[Spellstrike Ammunition (Type II)](equipment/consumable-05-mmsuA7qPxFLLghtx.htm)|Munition de frappe magique (Type II)|`@Compendium[pf2e.equipment-srd.mmsuA7qPxFLLghtx]`|
|[Spellstrike Ammunition (Type III)](equipment/consumable-07-mz0GbCF2WT1DSYac.htm)|Munition de frappe magique (Type III)|`@Compendium[pf2e.equipment-srd.mz0GbCF2WT1DSYac]`|
|[Spellstrike Ammunition (Type IV)](equipment/consumable-09-WSZl9S0Nui7pTMa8.htm)|Munition de frappe magique (Type IV)|`@Compendium[pf2e.equipment-srd.WSZl9S0Nui7pTMa8]`|
|[Spellstrike Ammunition (Type IX)](equipment/consumable-19-aAGjV3wkopQ44VX3.htm)|Munition de frappe magique (Type IX)|`@Compendium[pf2e.equipment-srd.aAGjV3wkopQ44VX3]`|
|[Spellstrike Ammunition (Type V)](equipment/consumable-11-XyrFcBF5ygIFFGes.htm)|Munition de frappe magique (Type V)|`@Compendium[pf2e.equipment-srd.XyrFcBF5ygIFFGes]`|
|[Spellstrike Ammunition (Type VI)](equipment/consumable-13-n4FF7K3SLb7q1v6v.htm)|Munition de frappe magique (Type VI)|`@Compendium[pf2e.equipment-srd.n4FF7K3SLb7q1v6v]`|
|[Spellstrike Ammunition (Type VII)](equipment/consumable-15-WearPqN56FQofpF6.htm)|Munition de frappe magique (Type VII)|`@Compendium[pf2e.equipment-srd.WearPqN56FQofpF6]`|
|[Spellstrike Ammunition (Type VIII)](equipment/consumable-17-oqiC4Mc5qKNCl88e.htm)|Munition de frappe magique (Type VIII)|`@Compendium[pf2e.equipment-srd.oqiC4Mc5qKNCl88e]`|
|[Sphere of Annihilation](equipment/equipment-27-B4DnQNcGl6nFVKHl.htm)|Sphère d'annihilation|`@Compendium[pf2e.equipment-srd.B4DnQNcGl6nFVKHl]`|
|[Spider Root](equipment/consumable-09-oOXvk18K4izaJzG7.htm)|Racine arachnide|`@Compendium[pf2e.equipment-srd.oOXvk18K4izaJzG7]`|
|[Spike Snare](equipment/consumable-01-3V2U720YhW2nyGVx.htm)|Piège artisanal à pointes|`@Compendium[pf2e.equipment-srd.3V2U720YhW2nyGVx]`|
|[Spiked Chain](equipment/weapon-00-zi9ovfoRp2fMhfpO.htm)|Chaîne cloutée|`@Compendium[pf2e.equipment-srd.zi9ovfoRp2fMhfpO]`|
|[Spiked Gauntlet](equipment/weapon-00-YFfG2fMyaIAXkmtr.htm)|Gantelet clouté|`@Compendium[pf2e.equipment-srd.YFfG2fMyaIAXkmtr]`|
|[Spined Shield](equipment/armor-07-WDh4fb9N86mNLfDV.htm)|Bouclier de la manticore|`@Compendium[pf2e.equipment-srd.WDh4fb9N86mNLfDV]`|
|[Spinel, deep blue](equipment/treasure-00-hJc0dOcHJ2nkJcik.htm)|Spinelle, bleu profond|`@Compendium[pf2e.equipment-srd.hJc0dOcHJ2nkJcik]`|
|[Spinel, red or green](equipment/treasure-00-wBoGy4DfKHAvqAko.htm)|Spinelle, rouge ou verte|`@Compendium[pf2e.equipment-srd.wBoGy4DfKHAvqAko]`|
|[Spiritsight Crossbow](equipment/weapon-08-2UX3tlC5vKeQjG7t.htm)||`@Compendium[pf2e.equipment-srd.2UX3tlC5vKeQjG7t]`|
|[Splendid lyre of world‑famous lyrist](equipment/treasure-00-LJxNB3Yy2sTJaUbd.htm)|Lyre splendide (luthier mondialement connu)|`@Compendium[pf2e.equipment-srd.LJxNB3Yy2sTJaUbd]`|
|[Splint Mail](equipment/armor-01-6AhDKX1dwRwFpQsU.htm)|Clibanion|`@Compendium[pf2e.equipment-srd.6AhDKX1dwRwFpQsU]`|
|[Spore Sap](equipment/weapon-11-tE2IarA29mYsgrxj.htm)||`@Compendium[pf2e.equipment-srd.tE2IarA29mYsgrxj]`|
|[Spyglass](equipment/equipment-00-UtRH6jt3iNFCgLLG.htm)|Longue-vue|`@Compendium[pf2e.equipment-srd.UtRH6jt3iNFCgLLG]`|
|[Spyglass (Fine)](equipment/equipment-04-rV7MTDCseZmEZKDw.htm)|Longue-vue (belle)|`@Compendium[pf2e.equipment-srd.rV7MTDCseZmEZKDw]`|
|[Staff](equipment/weapon-00-FVjTuBCIefAgloUU.htm)|Bâton|`@Compendium[pf2e.equipment-srd.FVjTuBCIefAgloUU]`|
|[Staff of Abjuration](equipment/weapon-06-EYRrABqjUYPrhrZr.htm)|Bâton d'abjuration|`@Compendium[pf2e.equipment-srd.EYRrABqjUYPrhrZr]`|
|[Staff of Abjuration (Greater)](equipment/weapon-10-HRNQ1j2OM54Qakhp.htm)|Bâton d'abjuration (supérieur)|`@Compendium[pf2e.equipment-srd.HRNQ1j2OM54Qakhp]`|
|[Staff of Abjuration (Major)](equipment/weapon-14-ZSt2PxZVik7UHGG3.htm)|Bâton d'abjuration (majeur)|`@Compendium[pf2e.equipment-srd.ZSt2PxZVik7UHGG3]`|
|[Staff of Conjuration](equipment/weapon-06-6IJZamE4JkERQumf.htm)||`@Compendium[pf2e.equipment-srd.6IJZamE4JkERQumf]`|
|[Staff of Conjuration (Greater)](equipment/weapon-10-xQf98bdGgE7UiBe4.htm)||`@Compendium[pf2e.equipment-srd.xQf98bdGgE7UiBe4]`|
|[Staff of Conjuration (Major)](equipment/weapon-14-7aLpTxRVmoNQTqvJ.htm)||`@Compendium[pf2e.equipment-srd.7aLpTxRVmoNQTqvJ]`|
|[Staff of Divination](equipment/weapon-06-ObmYN6I64Pjj7yEA.htm)|Bâton de divination|`@Compendium[pf2e.equipment-srd.ObmYN6I64Pjj7yEA]`|
|[Staff of Divination (Greater)](equipment/weapon-10-E63MaprijBuUK9Om.htm)|Bâton de divination (supérieur)|`@Compendium[pf2e.equipment-srd.E63MaprijBuUK9Om]`|
|[Staff of Divination (Major)](equipment/weapon-14-lOeShVkqV98KlvTI.htm)|Bâton de divination (majeur)|`@Compendium[pf2e.equipment-srd.lOeShVkqV98KlvTI]`|
|[Staff of Enchantment](equipment/weapon-06-eaYNVLnTX9VejnaA.htm)|Bâton d'enchantement|`@Compendium[pf2e.equipment-srd.eaYNVLnTX9VejnaA]`|
|[Staff of Enchantment (Greater)](equipment/weapon-10-ExExv8dJVcCj4rL0.htm)|Bâton d'enchantement (supérieur)|`@Compendium[pf2e.equipment-srd.ExExv8dJVcCj4rL0]`|
|[Staff of Enchantment (Major)](equipment/weapon-14-MeUgw1hsSEcWP97G.htm)|Bâton d'enchantement (majeure)|`@Compendium[pf2e.equipment-srd.MeUgw1hsSEcWP97G]`|
|[Staff of Evocation](equipment/weapon-06-96VBd7CV8NQyv3lP.htm)|Bâton d'évocation|`@Compendium[pf2e.equipment-srd.96VBd7CV8NQyv3lP]`|
|[Staff of Evocation (Greater)](equipment/weapon-10-RCQ4DzDh34ZBiTxI.htm)|Bâton d'évocation (supérieur)|`@Compendium[pf2e.equipment-srd.RCQ4DzDh34ZBiTxI]`|
|[Staff of Evocation (Major)](equipment/weapon-14-kNvo4FNaiQRnZfO7.htm)|Bâton d'évocation (majeur)|`@Compendium[pf2e.equipment-srd.kNvo4FNaiQRnZfO7]`|
|[Staff of Fire](equipment/weapon-03-opfpl1JmKgrfds9P.htm)|Bâton de feu|`@Compendium[pf2e.equipment-srd.opfpl1JmKgrfds9P]`|
|[Staff of Fire (Greater)](equipment/weapon-08-KcjaeMgrsBGgwUWL.htm)|Bâton de feu (supérieur)|`@Compendium[pf2e.equipment-srd.KcjaeMgrsBGgwUWL]`|
|[Staff of Fire (Major)](equipment/weapon-12-qx4Cq99vng6GhzEh.htm)|Bâton de feu (majeur)|`@Compendium[pf2e.equipment-srd.qx4Cq99vng6GhzEh]`|
|[Staff of Healing](equipment/weapon-04-3OkOKxCee9WruGU5.htm)|Bâton de guérison|`@Compendium[pf2e.equipment-srd.3OkOKxCee9WruGU5]`|
|[Staff of Healing (Greater)](equipment/weapon-08-XSwEE8wjHr6UXzpw.htm)|Bâton de guérison (supérieur)|`@Compendium[pf2e.equipment-srd.XSwEE8wjHr6UXzpw]`|
|[Staff of Healing (Major)](equipment/weapon-12-FTVap8IjoKgCexH7.htm)|Bâton de guérison (majeur)|`@Compendium[pf2e.equipment-srd.FTVap8IjoKgCexH7]`|
|[Staff of Healing (True)](equipment/weapon-16-WbcQqXrUytXkCMK3.htm)|Bâton de guérison (ultime)|`@Compendium[pf2e.equipment-srd.WbcQqXrUytXkCMK3]`|
|[Staff of Illumination](equipment/weapon-08-8No84rsBOCVCkXJK.htm)||`@Compendium[pf2e.equipment-srd.8No84rsBOCVCkXJK]`|
|[Staff of Illusion](equipment/weapon-06-Q2QHZdBaoOLkE1lX.htm)||`@Compendium[pf2e.equipment-srd.Q2QHZdBaoOLkE1lX]`|
|[Staff of Illusion (Greater)](equipment/weapon-10-6u14xOkKhnB9lVy0.htm)||`@Compendium[pf2e.equipment-srd.6u14xOkKhnB9lVy0]`|
|[Staff of Illusion (Major)](equipment/weapon-14-gG4IUEzqD253s7nx.htm)||`@Compendium[pf2e.equipment-srd.gG4IUEzqD253s7nx]`|
|[Staff of Impossible Visions](equipment/weapon-06-WA5eoFFuAsyx7A2t.htm)||`@Compendium[pf2e.equipment-srd.WA5eoFFuAsyx7A2t]`|
|[Staff of Impossible Visions (Greater)](equipment/weapon-12-KxSyomQx7rpwqemP.htm)||`@Compendium[pf2e.equipment-srd.KxSyomQx7rpwqemP]`|
|[Staff of Impossible Visions (Major)](equipment/weapon-16-oT4pyqLKpJVXDb46.htm)||`@Compendium[pf2e.equipment-srd.oT4pyqLKpJVXDb46]`|
|[Staff of Impossible Visions (True)](equipment/weapon-20-1glQQ63AeQOfQNvc.htm)||`@Compendium[pf2e.equipment-srd.1glQQ63AeQOfQNvc]`|
|[Staff of Nature's Vengeance](equipment/weapon-10-aPbDAb6cJKPvCHk2.htm)||`@Compendium[pf2e.equipment-srd.aPbDAb6cJKPvCHk2]`|
|[Staff of Nature's Vengeance (Greater)](equipment/weapon-14-gca5Pgt9Pg8G23VA.htm)||`@Compendium[pf2e.equipment-srd.gca5Pgt9Pg8G23VA]`|
|[Staff of Nature's Vengeance (Major)](equipment/weapon-18-hnx3dOQrYLBtsu3V.htm)||`@Compendium[pf2e.equipment-srd.hnx3dOQrYLBtsu3V]`|
|[Staff of Necromancy](equipment/weapon-06-c40Zn2TCRr3inIBA.htm)|Bâton de nécromancie|`@Compendium[pf2e.equipment-srd.c40Zn2TCRr3inIBA]`|
|[Staff of Necromancy (Greater)](equipment/weapon-10-Ab5EjJyoRSec5YrW.htm)|Bâton de nécromancie (supérieur)|`@Compendium[pf2e.equipment-srd.Ab5EjJyoRSec5YrW]`|
|[Staff of Necromancy (Major)](equipment/weapon-14-IIV5L4y8zLH17mAK.htm)|Bâton de nécromancie (majeur)|`@Compendium[pf2e.equipment-srd.IIV5L4y8zLH17mAK]`|
|[Staff of Power](equipment/weapon-16-6m9niDjhA6tBfp5x.htm)|Bâton de surpuissance|`@Compendium[pf2e.equipment-srd.6m9niDjhA6tBfp5x]`|
|[Staff of Providence](equipment/weapon-06-XeTmuhuNnhGf7c4t.htm)||`@Compendium[pf2e.equipment-srd.XeTmuhuNnhGf7c4t]`|
|[Staff of Providence (Greater)](equipment/weapon-10-T1L6XbgMqJLDv2Pi.htm)||`@Compendium[pf2e.equipment-srd.T1L6XbgMqJLDv2Pi]`|
|[Staff of Providence (Major)](equipment/weapon-14-U8vVCE2ePjyca666.htm)||`@Compendium[pf2e.equipment-srd.U8vVCE2ePjyca666]`|
|[Staff of Providence (True)](equipment/weapon-20-pIagOwW8EFBaKW3k.htm)||`@Compendium[pf2e.equipment-srd.pIagOwW8EFBaKW3k]`|
|[Staff of Providence (Ture)](equipment/weapon-20-ZPnQMNOyHjTkkGx4.htm)||`@Compendium[pf2e.equipment-srd.ZPnQMNOyHjTkkGx4]`|
|[Staff of Sieges](equipment/weapon-20-dh4FEXlA0FxTfnpY.htm)||`@Compendium[pf2e.equipment-srd.dh4FEXlA0FxTfnpY]`|
|[Staff of Transmutation](equipment/weapon-06-p1pysEaGypaNxTEL.htm)|Bâton de transmutation|`@Compendium[pf2e.equipment-srd.p1pysEaGypaNxTEL]`|
|[Staff of Transmutation (Greater)](equipment/weapon-10-YxLC2Np7cEBnZzGX.htm)|Bâton de transmutation (supérieur)|`@Compendium[pf2e.equipment-srd.YxLC2Np7cEBnZzGX]`|
|[Staff of Transmutation (Major)](equipment/weapon-14-GlyiVrIokFpFFRu2.htm)|Bâton de transmutation (majeur)|`@Compendium[pf2e.equipment-srd.GlyiVrIokFpFFRu2]`|
|[Staff of the Black Desert](equipment/weapon-12-ED3By5mICaJeiQYo.htm)||`@Compendium[pf2e.equipment-srd.ED3By5mICaJeiQYo]`|
|[Staff of the Black Desert (Greater)](equipment/weapon-16-Ri09KBg9DG0aapLw.htm)||`@Compendium[pf2e.equipment-srd.Ri09KBg9DG0aapLw]`|
|[Staff of the Magi](equipment/weapon-20-nI9shR1EG3P09I8r.htm)||`@Compendium[pf2e.equipment-srd.nI9shR1EG3P09I8r]`|
|[Stalker Bane Snare](equipment/consumable-04-s9dtRS2SRTqzGdOF.htm)||`@Compendium[pf2e.equipment-srd.s9dtRS2SRTqzGdOF]`|
|[Stalwart's Ring](equipment/equipment-05-bhpYiOmBysl8ZEYn.htm)||`@Compendium[pf2e.equipment-srd.bhpYiOmBysl8ZEYn]`|
|[Standard Astrolabe](equipment/equipment-02-n8nmqJdfHrDfuG1J.htm)||`@Compendium[pf2e.equipment-srd.n8nmqJdfHrDfuG1J]`|
|[Standard Book of Translation (Tien)](equipment/equipment-02-c3dFpmXsIxH2i8ug.htm)||`@Compendium[pf2e.equipment-srd.c3dFpmXsIxH2i8ug]`|
|[Star sapphire](equipment/treasure-00-2Rb9MOLENbZ7yyRL.htm)|Saphir étoilé|`@Compendium[pf2e.equipment-srd.2Rb9MOLENbZ7yyRL]`|
|[Star sapphire necklace](equipment/treasure-00-FwYqacDEAmXk5jB5.htm)|Collier de saphirs étoilés|`@Compendium[pf2e.equipment-srd.FwYqacDEAmXk5jB5]`|
|[Starknife](equipment/weapon-00-Mv2I6M70bagbaBPn.htm)|Lamétoile|`@Compendium[pf2e.equipment-srd.Mv2I6M70bagbaBPn]`|
|[Starshot Arrow (Greater)](equipment/consumable-14-4dwBcqeuXePd89qW.htm)||`@Compendium[pf2e.equipment-srd.4dwBcqeuXePd89qW]`|
|[Starshot Arrow (Lesser)](equipment/consumable-07-HUPCdlFkJQC7Krim.htm)||`@Compendium[pf2e.equipment-srd.HUPCdlFkJQC7Krim]`|
|[Steel Shield](equipment/armor-00-Yr9yCuJiAlFh3QEB.htm)|Bouclier en acier|`@Compendium[pf2e.equipment-srd.Yr9yCuJiAlFh3QEB]`|
|[Stiletto Pen](equipment/weapon-01-Hh3F4DEP6aVulwQN.htm)||`@Compendium[pf2e.equipment-srd.Hh3F4DEP6aVulwQN]`|
|[Stole of Civility](equipment/equipment-09-dRVDcmkaB2p8zPcs.htm)||`@Compendium[pf2e.equipment-srd.dRVDcmkaB2p8zPcs]`|
|[Stone Body Mutagen (Greater)](equipment/consumable-15-cqTCg6C0lvYUKQmx.htm)||`@Compendium[pf2e.equipment-srd.cqTCg6C0lvYUKQmx]`|
|[Stone Body Mutagen (Lesser)](equipment/consumable-05-5ftFTiKY0BMvhzL9.htm)||`@Compendium[pf2e.equipment-srd.5ftFTiKY0BMvhzL9]`|
|[Stone Body Mutagen (Moderate)](equipment/consumable-12-FZ6SMwiei6NnEp3e.htm)||`@Compendium[pf2e.equipment-srd.FZ6SMwiei6NnEp3e]`|
|[Stone Bullet](equipment/consumable-15-wTVBD8XHPG810rlH.htm)|Bille de pierre|`@Compendium[pf2e.equipment-srd.wTVBD8XHPG810rlH]`|
|[Stone Fist Elixir](equipment/consumable-04-YcvSw7Zn3oyqlJaw.htm)|Elixir de poing de pierre|`@Compendium[pf2e.equipment-srd.YcvSw7Zn3oyqlJaw]`|
|[Stone of Weight](equipment/equipment-02-EuEH4f3oDUQ4YFZS.htm)|Pierre de lest|`@Compendium[pf2e.equipment-srd.EuEH4f3oDUQ4YFZS]`|
|[Stoneraiser Javelin](equipment/weapon-11-ud3pqCquYA5UecaS.htm)||`@Compendium[pf2e.equipment-srd.ud3pqCquYA5UecaS]`|
|[Storm Arrow](equipment/consumable-09-RpvH9EDquO0jS3Jz.htm)|Flèche d'orage|`@Compendium[pf2e.equipment-srd.RpvH9EDquO0jS3Jz]`|
|[Storm Flash](equipment/weapon-14-Za5Mnae009cDCwuN.htm)|Eclair d'orage|`@Compendium[pf2e.equipment-srd.Za5Mnae009cDCwuN]`|
|[Storm Flash (Greater)](equipment/weapon-18-i9mxfSIBTTOwsSlx.htm)|Éclair d'Orage (supérieur)|`@Compendium[pf2e.equipment-srd.i9mxfSIBTTOwsSlx]`|
|[Storm Hammer](equipment/weapon-03-EuTZxxwdVeN6Xg3A.htm)||`@Compendium[pf2e.equipment-srd.EuTZxxwdVeN6Xg3A]`|
|[Striking](equipment/equipment-04-DxCuJKynlnMQZHgp.htm)|Frappe (rune)|`@Compendium[pf2e.equipment-srd.DxCuJKynlnMQZHgp]`|
|[Striking (Greater)](equipment/equipment-12-KCQRFvUgbyclvOGi.htm)|Frappe supérieure (rune)|`@Compendium[pf2e.equipment-srd.KCQRFvUgbyclvOGi]`|
|[Striking (Major)](equipment/equipment-19-woxl2FrrgAcJDu0t.htm)|Frappe majeure (rune)|`@Compendium[pf2e.equipment-srd.woxl2FrrgAcJDu0t]`|
|[Striking Snare](equipment/consumable-08-6KdYdFovFBivwI8M.htm)||`@Compendium[pf2e.equipment-srd.6KdYdFovFBivwI8M]`|
|[Studded Leather Armor](equipment/armor-00-ewQZ0VeL38v3qFnN.htm)|Armure de cuir clouté|`@Compendium[pf2e.equipment-srd.ewQZ0VeL38v3qFnN]`|
|[Stunning Snare](equipment/consumable-12-a2XaerM1KkPyLIPM.htm)||`@Compendium[pf2e.equipment-srd.a2XaerM1KkPyLIPM]`|
|[Sturdy Satchel](equipment/equipment-02-KxUJu1IHY9FxxAGV.htm)||`@Compendium[pf2e.equipment-srd.KxUJu1IHY9FxxAGV]`|
|[Sturdy Shield (Greater)](equipment/armor-13-rrnWORxT2Ch4pUFb.htm)|Bouclier renforcé (supérieur)|`@Compendium[pf2e.equipment-srd.rrnWORxT2Ch4pUFb]`|
|[Sturdy Shield (Lesser)](equipment/armor-07-nDZX25OwoN0Imrq6.htm)|Bouclier solide (inférieur)|`@Compendium[pf2e.equipment-srd.nDZX25OwoN0Imrq6]`|
|[Sturdy Shield (Major)](equipment/armor-16-BWQzaHbGVqlBuMww.htm)|Bouclier renforcé (majeure)|`@Compendium[pf2e.equipment-srd.BWQzaHbGVqlBuMww]`|
|[Sturdy Shield (Minor)](equipment/armor-04-f9ygr5Cjrmop8LWV.htm)|Bouclier renforcé (mineure)|`@Compendium[pf2e.equipment-srd.f9ygr5Cjrmop8LWV]`|
|[Sturdy Shield (Moderate)](equipment/armor-10-pNQJ9PTOEHxEZCgp.htm)|Bouclier renforcé (modéré)|`@Compendium[pf2e.equipment-srd.pNQJ9PTOEHxEZCgp]`|
|[Sturdy Shield (Supreme)](equipment/armor-19-7Z8XXGiUiyyisKOD.htm)|Bouclier renforcé (ultime)|`@Compendium[pf2e.equipment-srd.7Z8XXGiUiyyisKOD]`|
|[Sun Orchid Elixir](equipment/consumable-20-P9dhnMcr3lbHBn2E.htm)|Éllixir d'orchidée solaire|`@Compendium[pf2e.equipment-srd.P9dhnMcr3lbHBn2E]`|
|[Sun Orchid Poultice](equipment/consumable-18-1LKJvK9ofzIIkQX3.htm)||`@Compendium[pf2e.equipment-srd.1LKJvK9ofzIIkQX3]`|
|[Sun Wheel](equipment/consumable-10-WW8uv3Nhg56UTZ35.htm)||`@Compendium[pf2e.equipment-srd.WW8uv3Nhg56UTZ35]`|
|[Sunrod](equipment/consumable-01-y4WJY8rCbY6d1MET.htm)|Bâton éclairant|`@Compendium[pf2e.equipment-srd.y4WJY8rCbY6d1MET]`|
|[Survey Map](equipment/equipment-03-UhcRWtnjU2WLSClx.htm)|Carte d'état-major|`@Compendium[pf2e.equipment-srd.UhcRWtnjU2WLSClx]`|
|[Survey Map (Atlas)](equipment/equipment-03-1M4SSDuTv2SwBltC.htm)||`@Compendium[pf2e.equipment-srd.1M4SSDuTv2SwBltC]`|
|[Swamp Lily Quilt](equipment/consumable-03-6P6lDnIxK7plyCEI.htm)||`@Compendium[pf2e.equipment-srd.6P6lDnIxK7plyCEI]`|
|[Swarmeater's Clasp](equipment/equipment-08-dPQhe0tAa5LkPFoW.htm)||`@Compendium[pf2e.equipment-srd.dPQhe0tAa5LkPFoW]`|
|[Swift Block Cabochon](equipment/consumable-07-4tnPWyApPZP1P1yO.htm)||`@Compendium[pf2e.equipment-srd.4tnPWyApPZP1P1yO]`|
|[Swim Fins](equipment/equipment-00-KIpkxwuPWt0rOGT1.htm)|Nageoires de natation|`@Compendium[pf2e.equipment-srd.KIpkxwuPWt0rOGT1]`|
|[Sword Cane](equipment/weapon-00-VitLIpdIAmKlGb7i.htm)|Canne épée|`@Compendium[pf2e.equipment-srd.VitLIpdIAmKlGb7i]`|
|[Tack](equipment/equipment-00-Bu2xT8NfB6xaeTJa.htm)|Sellerie|`@Compendium[pf2e.equipment-srd.Bu2xT8NfB6xaeTJa]`|
|[Talisman Cord](equipment/equipment-10-HvmIoRCaQJmkwluG.htm)|Corde de talisman|`@Compendium[pf2e.equipment-srd.HvmIoRCaQJmkwluG]`|
|[Talisman Cord (Greater)](equipment/equipment-16-62ZOnGqCMlXCHngg.htm)||`@Compendium[pf2e.equipment-srd.62ZOnGqCMlXCHngg]`|
|[Talisman Cord (Lesser)](equipment/equipment-04-tnPteWjbm97bdTj5.htm)|Corde de talisman (inférieur)|`@Compendium[pf2e.equipment-srd.tnPteWjbm97bdTj5]`|
|[Talisman of the Sphere](equipment/equipment-20-imBDGoQJnvfWYuLC.htm)|Talisman de la sphère|`@Compendium[pf2e.equipment-srd.imBDGoQJnvfWYuLC]`|
|[Tanglefoot Bag (Greater)](equipment/weapon-11-5Qkz4RVJr2Kx3RL6.htm)|Sacoche immobilisante (supérieur)|`@Compendium[pf2e.equipment-srd.5Qkz4RVJr2Kx3RL6]`|
|[Tanglefoot Bag (Lesser)](equipment/weapon-01-T6Appwwl6nUl56Xj.htm)|Sacoche immobilisante (inférieur)|`@Compendium[pf2e.equipment-srd.T6Appwwl6nUl56Xj]`|
|[Tanglefoot Bag (Major)](equipment/weapon-17-BxJNWWvpxacDIsdt.htm)|Sacoche immobilisante (majeur)|`@Compendium[pf2e.equipment-srd.BxJNWWvpxacDIsdt]`|
|[Tanglefoot Bag (Moderate)](equipment/weapon-03-evBPzM1VsuYcoenn.htm)|Sacoche immobilisante (modéré)|`@Compendium[pf2e.equipment-srd.evBPzM1VsuYcoenn]`|
|[Tankard owned by Cayden Cailean](equipment/treasure-00-HIxtCuH5YXNqit0W.htm)|Chope ayant appartenu à Cayden Cailean|`@Compendium[pf2e.equipment-srd.HIxtCuH5YXNqit0W]`|
|[Tears of Death](equipment/consumable-20-gDGPvobJV1QGYBPy.htm)|Larmes de la mort|`@Compendium[pf2e.equipment-srd.gDGPvobJV1QGYBPy]`|
|[Tears of the Last Azlanti](equipment/equipment-25-2VpuNz8EyXpvUf7P.htm)||`@Compendium[pf2e.equipment-srd.2VpuNz8EyXpvUf7P]`|
|[Tekko-kagi](equipment/weapon-00-vsJ0zvYZzduP7rtD.htm)|Tekko-kagi|`@Compendium[pf2e.equipment-srd.vsJ0zvYZzduP7rtD]`|
|[Temple Sword](equipment/weapon-00-NIsxR5zXtVa3PuyU.htm)|Épée du temple|`@Compendium[pf2e.equipment-srd.NIsxR5zXtVa3PuyU]`|
|[Ten-Foot Pole](equipment/equipment-00-ccsgob2TZ7WqTrp7.htm)|Perche de 3 m|`@Compendium[pf2e.equipment-srd.ccsgob2TZ7WqTrp7]`|
|[Tengu Gale Blade](equipment/weapon-00-6qJkA6fnHHiF4CEV.htm)|Lame tempête Tengu|`@Compendium[pf2e.equipment-srd.6qJkA6fnHHiF4CEV]`|
|[Tent (Four-Person)](equipment/equipment-00-JlClrm1WfJ45oWcS.htm)|Tente (4 personnes)|`@Compendium[pf2e.equipment-srd.JlClrm1WfJ45oWcS]`|
|[Tent (Pavilion)](equipment/equipment-02-xyzmQa3nhU8HxfUL.htm)|Tente pour 4 personnes|`@Compendium[pf2e.equipment-srd.xyzmQa3nhU8HxfUL]`|
|[Tent (Pup)](equipment/equipment-00-Wc9og4mTKZDH3xmz.htm)|Tente (petite)|`@Compendium[pf2e.equipment-srd.Wc9og4mTKZDH3xmz]`|
|[Terrifying Ammunition](equipment/consumable-06-ROdjFw7wby982qf5.htm)||`@Compendium[pf2e.equipment-srd.ROdjFw7wby982qf5]`|
|[The Whispering Reeds](equipment/equipment-10-eZcXKlXMbQGzDm3M.htm)||`@Compendium[pf2e.equipment-srd.eZcXKlXMbQGzDm3M]`|
|[Thieves' Tools](equipment/equipment-00-zvLyCVD8g2PdHJAc.htm)|Outils de voleur|`@Compendium[pf2e.equipment-srd.zvLyCVD8g2PdHJAc]`|
|[Thieves' Tools (Concealable Picks)](equipment/equipment-04-TxMFNfkoccirCitP.htm)||`@Compendium[pf2e.equipment-srd.TxMFNfkoccirCitP]`|
|[Thieves' Tools (Concealable)](equipment/equipment-04-Ejmv9IHGp9Ad9dgu.htm)||`@Compendium[pf2e.equipment-srd.Ejmv9IHGp9Ad9dgu]`|
|[Thieves' Tools (Infiltrator Picks)](equipment/equipment-03-BznmPaRjI4Orb0IH.htm)|Crochets d'infiltration|`@Compendium[pf2e.equipment-srd.BznmPaRjI4Orb0IH]`|
|[Thieves' Tools (Infiltrator)](equipment/equipment-03-6nrCxNQFycUVFOV2.htm)|Outils de voleur d'infiltration|`@Compendium[pf2e.equipment-srd.6nrCxNQFycUVFOV2]`|
|[Thieves' Tools (Replacement Picks)](equipment/equipment-00-Sw7MBLASN3xK4Y44.htm)|Crochets de remplacement|`@Compendium[pf2e.equipment-srd.Sw7MBLASN3xK4Y44]`|
|[Third Eye](equipment/equipment-19-7MTjAlCVVLsNFo7w.htm)|Troisième oeil|`@Compendium[pf2e.equipment-srd.7MTjAlCVVLsNFo7w]`|
|[Thought lens of astral essence](equipment/treasure-00-5CqtgygdvqecMoRU.htm)|Lentille de pensée d'essence astrale|`@Compendium[pf2e.equipment-srd.5CqtgygdvqecMoRU]`|
|[Throwing Knife](equipment/weapon-00-uM0OlCC1Sh6OLdNn.htm)|Couteau de lancer|`@Compendium[pf2e.equipment-srd.uM0OlCC1Sh6OLdNn]`|
|[Thunder Snare](equipment/consumable-02-yUYr8j65fC7EN0NY.htm)||`@Compendium[pf2e.equipment-srd.yUYr8j65fC7EN0NY]`|
|[Thundering](equipment/equipment-08-dTxbaa7HSiJbIuNN.htm)|Tonnerre (rune)|`@Compendium[pf2e.equipment-srd.dTxbaa7HSiJbIuNN]`|
|[Thundering (Greater)](equipment/equipment-15-Lb7F2BR9X9TF1vjX.htm)|Tonnerre supérieure (rune)|`@Compendium[pf2e.equipment-srd.Lb7F2BR9X9TF1vjX]`|
|[Thunderstone (Greater)](equipment/weapon-11-dsMkvuLgpOOGLWDy.htm)|Pierre à tonnerre (supérieur)|`@Compendium[pf2e.equipment-srd.dsMkvuLgpOOGLWDy]`|
|[Thunderstone (Lesser)](equipment/weapon-01-Xnqglykl3Cif8rN9.htm)|Pierre à tonnerre (inférieur)|`@Compendium[pf2e.equipment-srd.Xnqglykl3Cif8rN9]`|
|[Thunderstone (Major)](equipment/weapon-17-IuGydh0En8LbfnWo.htm)|Pierre à tonnerre (majeur)|`@Compendium[pf2e.equipment-srd.IuGydh0En8LbfnWo]`|
|[Thunderstone (Moderate)](equipment/weapon-03-JOaELkzLWTywhn5Z.htm)|Pierre à tonnerre (modéré)|`@Compendium[pf2e.equipment-srd.JOaELkzLWTywhn5Z]`|
|[Thurible of Revelation (Greater)](equipment/equipment-18-VBCk7JXGsuG0cug1.htm)|Encensoir de révélation (supérieure)|`@Compendium[pf2e.equipment-srd.VBCk7JXGsuG0cug1]`|
|[Thurible of Revelation (Lesser)](equipment/equipment-03-xaWuuQoBJiMLzggR.htm)|Encensoir de révélation (inférieur)|`@Compendium[pf2e.equipment-srd.xaWuuQoBJiMLzggR]`|
|[Thurible of Revelation (Moderate)](equipment/equipment-10-QEPx1fCf74xdpXBH.htm)|Encensoir de révélation (modéré)|`@Compendium[pf2e.equipment-srd.QEPx1fCf74xdpXBH]`|
|[Tiger Menuki](equipment/consumable-05-zrZ1FaaqW6VIajj7.htm)|Menuki du tigre|`@Compendium[pf2e.equipment-srd.zrZ1FaaqW6VIajj7]`|
|[Tiger's‑eye](equipment/treasure-00-DFNmzduKX6NSuPrT.htm)|Oeil de tigre|`@Compendium[pf2e.equipment-srd.DFNmzduKX6NSuPrT]`|
|[Time Shield Potion](equipment/consumable-13-1YROvQsCdq8WFWRj.htm)||`@Compendium[pf2e.equipment-srd.1YROvQsCdq8WFWRj]`|
|[Timeless Salts](equipment/consumable-04-Ha6n30Tj3TNru9Dj.htm)||`@Compendium[pf2e.equipment-srd.Ha6n30Tj3TNru9Dj]`|
|[Tindertwig](equipment/consumable-01-ylUdMTsfOQGJ3MN3.htm)|Brindille d'amadou|`@Compendium[pf2e.equipment-srd.ylUdMTsfOQGJ3MN3]`|
|[Toad Tears](equipment/consumable-02-mDNDs5P8ZAJXYbDL.htm)||`@Compendium[pf2e.equipment-srd.mDNDs5P8ZAJXYbDL]`|
|[Tool (Long)](equipment/equipment-00-wrpI5z7iWB8XvflQ.htm)|Outil|`@Compendium[pf2e.equipment-srd.wrpI5z7iWB8XvflQ]`|
|[Tool (Short)](equipment/equipment-00-Zr4PwTEuBLE30kkn.htm)|Outil|`@Compendium[pf2e.equipment-srd.Zr4PwTEuBLE30kkn]`|
|[Topaz](equipment/treasure-00-oyGXxUmS6QcthfRc.htm)|Topaze|`@Compendium[pf2e.equipment-srd.oyGXxUmS6QcthfRc]`|
|[Torag's Silver Anvil](equipment/weapon-18-Kmrkm1sHKkGXiJlr.htm)||`@Compendium[pf2e.equipment-srd.Kmrkm1sHKkGXiJlr]`|
|[Torch](equipment/equipment-00-8Jdw4yAzWYylGePS.htm)|Torche|`@Compendium[pf2e.equipment-srd.8Jdw4yAzWYylGePS]`|
|[Torrent Snare](equipment/consumable-03-nPWDuoe2PcgE0z2S.htm)||`@Compendium[pf2e.equipment-srd.nPWDuoe2PcgE0z2S]`|
|[Tourmaline](equipment/treasure-00-m26FikfAdZouHzhq.htm)|Tourmaline|`@Compendium[pf2e.equipment-srd.m26FikfAdZouHzhq]`|
|[Tower Shield](equipment/armor-00-ltundBNFAnP7bgPr.htm)|Pavois|`@Compendium[pf2e.equipment-srd.ltundBNFAnP7bgPr]`|
|[Towering sculpture by a master](equipment/treasure-00-eUXQTjezsuJ6EB4u.htm)|Sculpture monumentale (par un maître)|`@Compendium[pf2e.equipment-srd.eUXQTjezsuJ6EB4u]`|
|[Tracker's Goggles](equipment/equipment-03-ZEqAx8jEc6zhX3V1.htm)|Lunettes de pisteur|`@Compendium[pf2e.equipment-srd.ZEqAx8jEc6zhX3V1]`|
|[Tracker's Goggles (Greater)](equipment/equipment-09-OYeYYJ4i66VtGY3O.htm)|Lunettes de pisteur (supérieures)|`@Compendium[pf2e.equipment-srd.OYeYYJ4i66VtGY3O]`|
|[Traveler's Any-Tool](equipment/equipment-06-nNtakXnSrcXWndBV.htm)|Outil multifonction de baroudeur|`@Compendium[pf2e.equipment-srd.nNtakXnSrcXWndBV]`|
|[Trident](equipment/weapon-00-FJrsDoaIXksVjld9.htm)|Trident|`@Compendium[pf2e.equipment-srd.FJrsDoaIXksVjld9]`|
|[Trip Snare](equipment/consumable-04-SWqzv0hYCIczICeR.htm)||`@Compendium[pf2e.equipment-srd.SWqzv0hYCIczICeR]`|
|[Triton's Conch](equipment/equipment-09-KLWjINVkWwJlOZEX.htm)|Conque de triton|`@Compendium[pf2e.equipment-srd.KLWjINVkWwJlOZEX]`|
|[Truesight Potion](equipment/consumable-16-CoMwPsQ8mPj5Evti.htm)|Potion de vision lucide|`@Compendium[pf2e.equipment-srd.CoMwPsQ8mPj5Evti]`|
|[Truth Potion](equipment/consumable-06-HAEz4sSa6OH6C7Cs.htm)|Potion de vérité|`@Compendium[pf2e.equipment-srd.HAEz4sSa6OH6C7Cs]`|
|[Turquoise](equipment/treasure-00-8aPckmJTsyUfBndP.htm)|Turquoise|`@Compendium[pf2e.equipment-srd.8aPckmJTsyUfBndP]`|
|[Twining Staff](equipment/weapon-06-pjUACbrAXNIy9O7S.htm)||`@Compendium[pf2e.equipment-srd.pjUACbrAXNIy9O7S]`|
|[Unending Youth](equipment/equipment-20-9Itj7SLzmWRzDilY.htm)||`@Compendium[pf2e.equipment-srd.9Itj7SLzmWRzDilY]`|
|[Unholy](equipment/equipment-11-gmMrJREf4JSHd2dZ.htm)|Impie (rune)|`@Compendium[pf2e.equipment-srd.gmMrJREf4JSHd2dZ]`|
|[Unholy Water](equipment/consumable-01-uplCUQwMwBOBHz0E.htm)|Eau impie|`@Compendium[pf2e.equipment-srd.uplCUQwMwBOBHz0E]`|
|[Universal Solvent (Greater)](equipment/consumable-12-mR9RD9S08jIX1IPm.htm)||`@Compendium[pf2e.equipment-srd.mR9RD9S08jIX1IPm]`|
|[Universal Solvent (Major)](equipment/consumable-18-jdkMRl7zxOVGUuGI.htm)||`@Compendium[pf2e.equipment-srd.jdkMRl7zxOVGUuGI]`|
|[Universal Solvent (Moderate)](equipment/consumable-05-QSVIv5obLhKkmy67.htm)||`@Compendium[pf2e.equipment-srd.QSVIv5obLhKkmy67]`|
|[Unmemorable Mantle](equipment/equipment-07-Oudcra1CGDmUq0BH.htm)||`@Compendium[pf2e.equipment-srd.Oudcra1CGDmUq0BH]`|
|[Unmemorable Mantle (Greater)](equipment/equipment-09-e4lHxftAOoTmGXuG.htm)||`@Compendium[pf2e.equipment-srd.e4lHxftAOoTmGXuG]`|
|[Unmemorable Mantle (Major)](equipment/equipment-17-HcjEb07UjWchysx5.htm)||`@Compendium[pf2e.equipment-srd.HcjEb07UjWchysx5]`|
|[Urn of Ashes](equipment/equipment-09-3G2No6QaU1wSPTh6.htm)||`@Compendium[pf2e.equipment-srd.3G2No6QaU1wSPTh6]`|
|[Urumi](equipment/weapon-00-Sarxo8kSrJs2qJEw.htm)|Urumi|`@Compendium[pf2e.equipment-srd.Sarxo8kSrJs2qJEw]`|
|[Vaccine (Greater)](equipment/consumable-12-9mXjEGJaESesndWh.htm)||`@Compendium[pf2e.equipment-srd.9mXjEGJaESesndWh]`|
|[Vaccine (Lesser)](equipment/consumable-03-nzeTcOyQmZNrurVF.htm)||`@Compendium[pf2e.equipment-srd.nzeTcOyQmZNrurVF]`|
|[Vaccine (Major)](equipment/consumable-18-JZZmDY1IJmKcdR2D.htm)||`@Compendium[pf2e.equipment-srd.JZZmDY1IJmKcdR2D]`|
|[Vaccine (Minor)](equipment/consumable-01-yi1iL9dbLDSr4NZd.htm)||`@Compendium[pf2e.equipment-srd.yi1iL9dbLDSr4NZd]`|
|[Vaccine (Moderate)](equipment/consumable-06-HilBL7oeSSXqDor7.htm)||`@Compendium[pf2e.equipment-srd.HilBL7oeSSXqDor7]`|
|[Vanishing Coin](equipment/consumable-10-dBevXop3G2P3PGjp.htm)|Pièce d'invisibilité|`@Compendium[pf2e.equipment-srd.dBevXop3G2P3PGjp]`|
|[Vanishing Wayfinder](equipment/equipment-05-SCkQb4QdFWkATiby.htm)||`@Compendium[pf2e.equipment-srd.SCkQb4QdFWkATiby]`|
|[Vaultbreaker's Harness](equipment/equipment-06-LmveO9fSuEnkMKAH.htm)||`@Compendium[pf2e.equipment-srd.LmveO9fSuEnkMKAH]`|
|[Ventriloquist's Ring](equipment/equipment-03-1r6StS0irdvi5JHY.htm)|Anneau de ventriloquie|`@Compendium[pf2e.equipment-srd.1r6StS0irdvi5JHY]`|
|[Ventriloquist's Ring (Greater)](equipment/equipment-09-dolBtAdB5lpQpQpp.htm)|Anneau de ventriloquie (supérieure)|`@Compendium[pf2e.equipment-srd.dolBtAdB5lpQpQpp]`|
|[Verdant Staff](equipment/weapon-06-sQFQlglhORQsxBKS.htm)|Bâton verdoyant|`@Compendium[pf2e.equipment-srd.sQFQlglhORQsxBKS]`|
|[Verdant Staff (Greater)](equipment/weapon-12-ylRk8NvpK2kA8bjw.htm)|Bâton verdoyant (supérieur)|`@Compendium[pf2e.equipment-srd.ylRk8NvpK2kA8bjw]`|
|[Vermin Repellent Agent (Greater)](equipment/consumable-12-CReWZjAd3szYQ6Mx.htm)||`@Compendium[pf2e.equipment-srd.CReWZjAd3szYQ6Mx]`|
|[Vermin Repellent Agent (Lesser)](equipment/consumable-03-lgqyRmQ3GZbbxQsD.htm)||`@Compendium[pf2e.equipment-srd.lgqyRmQ3GZbbxQsD]`|
|[Vermin Repellent Agent (Major)](equipment/consumable-16-C6v5p0ZtIUroYlU2.htm)||`@Compendium[pf2e.equipment-srd.C6v5p0ZtIUroYlU2]`|
|[Vermin Repellent Agent (Moderate)](equipment/consumable-08-WGbE7qAGw8O5yH07.htm)||`@Compendium[pf2e.equipment-srd.WGbE7qAGw8O5yH07]`|
|[Vestige Lenses](equipment/equipment-03-PCQtpTKiGfSSRwNV.htm)||`@Compendium[pf2e.equipment-srd.PCQtpTKiGfSSRwNV]`|
|[Vial](equipment/equipment-00-wvOYIeJjU3UDVgMG.htm)|Fiole|`@Compendium[pf2e.equipment-srd.wvOYIeJjU3UDVgMG]`|
|[Victory Plate](equipment/armor-09-nVlKU2et4EeReayr.htm)||`@Compendium[pf2e.equipment-srd.nVlKU2et4EeReayr]`|
|[Victory Plate (Greater)](equipment/armor-15-ed2F0L3R6UNSFUmS.htm)||`@Compendium[pf2e.equipment-srd.ed2F0L3R6UNSFUmS]`|
|[Vine Arrow](equipment/consumable-03-eEnzHpPEbdGgRETM.htm)|Flèche de lierre|`@Compendium[pf2e.equipment-srd.eEnzHpPEbdGgRETM]`|
|[Violet Venom](equipment/consumable-03-Oip645RjC5y57wFa.htm)||`@Compendium[pf2e.equipment-srd.Oip645RjC5y57wFa]`|
|[Viper Arrow](equipment/consumable-04-ilB279mxqXnlaSFj.htm)|Flèche vipère|`@Compendium[pf2e.equipment-srd.ilB279mxqXnlaSFj]`|
|[Viper's Fang](equipment/consumable-14-sjxhPujyv6rJJRWO.htm)|Croc de vipère|`@Compendium[pf2e.equipment-srd.sjxhPujyv6rJJRWO]`|
|[Virtuoso silver flute](equipment/treasure-00-nzT4kWzAdLMcESWl.htm)|Flute en argent de virtuose|`@Compendium[pf2e.equipment-srd.nzT4kWzAdLMcESWl]`|
|[Vorpal](equipment/equipment-17-6xaxxKfvXED6LfIY.htm)|Vorpale (rune)|`@Compendium[pf2e.equipment-srd.6xaxxKfvXED6LfIY]`|
|[Voyager's Pack](equipment/backpack-17-LRSIRUERqBAJ1HGT.htm)|Sac à dos du voyageur|`@Compendium[pf2e.equipment-srd.LRSIRUERqBAJ1HGT]`|
|[Wakizashi](equipment/weapon-00-YOpopjoWgU7bkmwh.htm)|Wakizashi|`@Compendium[pf2e.equipment-srd.YOpopjoWgU7bkmwh]`|
|[Walking Cauldron](equipment/equipment-01-Zmz2M2u7mGOCbUf4.htm)||`@Compendium[pf2e.equipment-srd.Zmz2M2u7mGOCbUf4]`|
|[Wand of Continuation (1st-Level Spell)](equipment/equipment-05-a60NH7OztaEaGlU8.htm)|Baguette de prolongation (sort de niveau 1)|`@Compendium[pf2e.equipment-srd.a60NH7OztaEaGlU8]`|
|[Wand of Continuation (2nd-Level Spell)](equipment/equipment-07-5V9bgqgQY1CHLd40.htm)|Baguette de prolongation (sort de niveau 2)|`@Compendium[pf2e.equipment-srd.5V9bgqgQY1CHLd40]`|
|[Wand of Continuation (3rd-Level Spell)](equipment/equipment-09-R88HWv9rw1VNMRer.htm)|Baguette de prolongation (sort de niveau 3)|`@Compendium[pf2e.equipment-srd.R88HWv9rw1VNMRer]`|
|[Wand of Continuation (4th-Level Spell)](equipment/equipment-11-bCsdAkffuk29ssUg.htm)|Baguette de prolongation (sort de niveau 4)|`@Compendium[pf2e.equipment-srd.bCsdAkffuk29ssUg]`|
|[Wand of Continuation (5th-Level Spell)](equipment/equipment-13-tDEi3zLVpxwA74qz.htm)|Baguette de prolongation (sort de niveau 5)|`@Compendium[pf2e.equipment-srd.tDEi3zLVpxwA74qz]`|
|[Wand of Continuation (6th-Level Spell)](equipment/equipment-15-35rLqxDWgiDIkL8e.htm)|Baguette de prolongation (sort de niveau 6)|`@Compendium[pf2e.equipment-srd.35rLqxDWgiDIkL8e]`|
|[Wand of Continuation (7th-Level Spell)](equipment/equipment-17-H1XGrl6Z0bzXN2oi.htm)|Baguette de prolongation (sort de niveau 7)|`@Compendium[pf2e.equipment-srd.H1XGrl6Z0bzXN2oi]`|
|[Wand of Continuation (8th-Level Spell)](equipment/equipment-19-KMqHzKfpPq5H8GOo.htm)|Baguette de prolongation (sort de niveau 8)|`@Compendium[pf2e.equipment-srd.KMqHzKfpPq5H8GOo]`|
|[Wand of Crackling Lightning (3rd-Level Spell)](equipment/equipment-08-NmhKGflbT5NnKduz.htm)||`@Compendium[pf2e.equipment-srd.NmhKGflbT5NnKduz]`|
|[Wand of Crackling Lightning (4th-Level Spell)](equipment/equipment-10-oyjDCHscjcLhzall.htm)||`@Compendium[pf2e.equipment-srd.oyjDCHscjcLhzall]`|
|[Wand of Crackling Lightning (6th-Level Spell)](equipment/equipment-14-1SdtzFpjKIwuQ7Nh.htm)||`@Compendium[pf2e.equipment-srd.1SdtzFpjKIwuQ7Nh]`|
|[Wand of Crackling Lightning (8th-Level Spell)](equipment/equipment-18-U28jkj5ZDl2drtEH.htm)||`@Compendium[pf2e.equipment-srd.U28jkj5ZDl2drtEH]`|
|[Wand of Hopeless Night (2nd-Level Spell)](equipment/equipment-06-ZJ3ahspZOXL4CK4J.htm)||`@Compendium[pf2e.equipment-srd.ZJ3ahspZOXL4CK4J]`|
|[Wand of Hopeless Night (4th-Level Spell)](equipment/equipment-10-9ftc8XfloerPcJnI.htm)||`@Compendium[pf2e.equipment-srd.9ftc8XfloerPcJnI]`|
|[Wand of Manifold Missiles (1st-Level Spell)](equipment/equipment-05-KPWN5tGGkvZR7K3K.htm)|Baguette de projectiles multiples (sort de niveau 1)|`@Compendium[pf2e.equipment-srd.KPWN5tGGkvZR7K3K]`|
|[Wand of Manifold Missiles (3rd-Level Spell)](equipment/equipment-09-3Lexs7KnhbV0HgFh.htm)|Baguette de projectiles multiples (sort de niveau 3)|`@Compendium[pf2e.equipment-srd.3Lexs7KnhbV0HgFh]`|
|[Wand of Manifold Missiles (5th-Level Spell)](equipment/equipment-13-y6if8e8OB3RUywF8.htm)|Baguette de projectiles multiples (sort de niveau 5)|`@Compendium[pf2e.equipment-srd.y6if8e8OB3RUywF8]`|
|[Wand of Manifold Missiles (7th-Level Spell)](equipment/equipment-17-WeX7rAO2kAyP0QnG.htm)|Baguette de projectiles multiples (sort de niveau 7)|`@Compendium[pf2e.equipment-srd.WeX7rAO2kAyP0QnG]`|
|[Wand of Noisome Acid (2nd-Level Spell)](equipment/equipment-06-JN2wkB9JG4nklVKP.htm)||`@Compendium[pf2e.equipment-srd.JN2wkB9JG4nklVKP]`|
|[Wand of Noisome Acid (4nd-Level Spell)](equipment/equipment-10-Nv4DjyBqIMXQ4KM4.htm)||`@Compendium[pf2e.equipment-srd.Nv4DjyBqIMXQ4KM4]`|
|[Wand of Noisome Acid (6th-Level Spell)](equipment/equipment-14-ljZwHU5BMnFafVa3.htm)||`@Compendium[pf2e.equipment-srd.ljZwHU5BMnFafVa3]`|
|[Wand of Noisome Acid (8th-Level Spell)](equipment/equipment-18-T1XSnU0bwrn3m520.htm)||`@Compendium[pf2e.equipment-srd.T1XSnU0bwrn3m520]`|
|[Wand of Overflowing Life (3rd-Level Spell)](equipment/equipment-09-qQpcBE7ngv7TIAW3.htm)||`@Compendium[pf2e.equipment-srd.qQpcBE7ngv7TIAW3]`|
|[Wand of Overflowing Life (4th-Level Spell)](equipment/equipment-11-85GqeSGXjSQwLy07.htm)||`@Compendium[pf2e.equipment-srd.85GqeSGXjSQwLy07]`|
|[Wand of Overflowing Life (5th-Level Spell)](equipment/equipment-13-aj2eTtEAgLu4f14b.htm)||`@Compendium[pf2e.equipment-srd.aj2eTtEAgLu4f14b]`|
|[Wand of Overflowing Life (6th-Level Spell)](equipment/equipment-15-VtzChvlCG2TQRrgu.htm)||`@Compendium[pf2e.equipment-srd.VtzChvlCG2TQRrgu]`|
|[Wand of Overflowing Life (7th-Level Spell)](equipment/equipment-17-grRxpX1iE3zOJA1q.htm)||`@Compendium[pf2e.equipment-srd.grRxpX1iE3zOJA1q]`|
|[Wand of Overflowing Life (8th-Level Spell)](equipment/equipment-19-xZFbFeJckiQS7smT.htm)||`@Compendium[pf2e.equipment-srd.xZFbFeJckiQS7smT]`|
|[Wand of Slaying (7th-Level Spell)](equipment/equipment-16-CjfBdn0fOIarWzBc.htm)|Baguette tueuse (sort de niveau 7)|`@Compendium[pf2e.equipment-srd.CjfBdn0fOIarWzBc]`|
|[Wand of Slaying (8th-Level Spell)](equipment/equipment-18-LnXYSaLxWMyAT3Ef.htm)|Baguette tueuse (sort de niveau 8)|`@Compendium[pf2e.equipment-srd.LnXYSaLxWMyAT3Ef]`|
|[Wand of Slaying (9th-Level Spell)](equipment/equipment-20-mMZWdoHvP2yYJzrR.htm)|Baguette tueuse (sort de niveau 9)|`@Compendium[pf2e.equipment-srd.mMZWdoHvP2yYJzrR]`|
|[Wand of Smoldering Fireballs (3rd-Level Spell)](equipment/equipment-08-c2Oa9UbhjwAsZaPp.htm)|Baguette de boules de feu incendiaires (Sort de niveau 3)|`@Compendium[pf2e.equipment-srd.c2Oa9UbhjwAsZaPp]`|
|[Wand of Smoldering Fireballs (5th-Level Spell)](equipment/equipment-12-zToq18jKonWIp48U.htm)|Baguette de boules de feu incendiaires (Sort de niveau 5)|`@Compendium[pf2e.equipment-srd.zToq18jKonWIp48U]`|
|[Wand of Smoldering Fireballs (7th-Level Spell)](equipment/equipment-16-YR8IAV94fPo0kfBz.htm)|Baguette de boules de feu incendiaires (Sort de niveau 7)|`@Compendium[pf2e.equipment-srd.YR8IAV94fPo0kfBz]`|
|[Wand of Smoldering Fireballs (9th-Level Spell)](equipment/equipment-20-tBwMPimZ6A93XpHf.htm)|Baguette de boules de feu incendiaires (Sort de niveau 9)|`@Compendium[pf2e.equipment-srd.tBwMPimZ6A93XpHf]`|
|[Wand of Teeming Ghosts (2nd-Level Spell)](equipment/equipment-06-yWlQQOs0A3ApEc1J.htm)||`@Compendium[pf2e.equipment-srd.yWlQQOs0A3ApEc1J]`|
|[Wand of Teeming Ghosts (3rd-Level Spell)](equipment/equipment-08-SVEPGvNFj95HjlAE.htm)||`@Compendium[pf2e.equipment-srd.SVEPGvNFj95HjlAE]`|
|[Wand of Teeming Ghosts (4th-Level Spell)](equipment/equipment-10-ofhqc85Z3Rt7dcrQ.htm)||`@Compendium[pf2e.equipment-srd.ofhqc85Z3Rt7dcrQ]`|
|[Wand of Teeming Ghosts (5th-Level Spell)](equipment/equipment-12-JWacChecDZbdo6sT.htm)||`@Compendium[pf2e.equipment-srd.JWacChecDZbdo6sT]`|
|[Wand of Teeming Ghosts (6th-Level Spell)](equipment/equipment-14-4Lhy6pAOIZdOufu3.htm)||`@Compendium[pf2e.equipment-srd.4Lhy6pAOIZdOufu3]`|
|[Wand of Teeming Ghosts (7th-Level Spell)](equipment/equipment-16-sLnkcAGiRQkwQrla.htm)||`@Compendium[pf2e.equipment-srd.sLnkcAGiRQkwQrla]`|
|[Wand of Teeming Ghosts (8th-Level Spell)](equipment/equipment-18-ASroFtAWLwn6HJJH.htm)||`@Compendium[pf2e.equipment-srd.ASroFtAWLwn6HJJH]`|
|[Wand of Teeming Ghosts (9th-Level Spell)](equipment/equipment-20-Y7xHJEyX5Mm3gpq3.htm)||`@Compendium[pf2e.equipment-srd.Y7xHJEyX5Mm3gpq3]`|
|[Wand of Widening (1st-Level Spell)](equipment/equipment-04-Zw3BKaJYxxxzNZ0f.htm)|Baguette d'élargissement (sort de niveau 1)|`@Compendium[pf2e.equipment-srd.Zw3BKaJYxxxzNZ0f]`|
|[Wand of Widening (2nd-Level Spell)](equipment/equipment-06-qmWlvoIlJRJ6pAeG.htm)|Baguette d'élargissement (sort de niveau 2)|`@Compendium[pf2e.equipment-srd.qmWlvoIlJRJ6pAeG]`|
|[Wand of Widening (3rd-Level Spell)](equipment/equipment-08-TJaumkbZy11sIAgR.htm)|Baguette d'élargissement (sort de niveau 3)|`@Compendium[pf2e.equipment-srd.TJaumkbZy11sIAgR]`|
|[Wand of Widening (4th-Level Spell)](equipment/equipment-10-zYRzgETeR1Hs1ti1.htm)|Baguette d'élargissement (sort de niveau 4)|`@Compendium[pf2e.equipment-srd.zYRzgETeR1Hs1ti1]`|
|[Wand of Widening (5th-Level Spell)](equipment/equipment-12-TGxZ3acyWjjTvfU9.htm)|Baguette d'élargissement (sort de niveau 5)|`@Compendium[pf2e.equipment-srd.TGxZ3acyWjjTvfU9]`|
|[Wand of Widening (6th-Level Spell)](equipment/equipment-14-JDQ4jqp6O8SurQGe.htm)|Baguette d'élargissement (sort de niveau 6)|`@Compendium[pf2e.equipment-srd.JDQ4jqp6O8SurQGe]`|
|[Wand of Widening (7th-Level Spell)](equipment/equipment-16-kNfdGNIGzF0fW7aq.htm)|Baguette d'élargissement (sort de niveau 7)|`@Compendium[pf2e.equipment-srd.kNfdGNIGzF0fW7aq]`|
|[Wand of Widening (8th-Level Spell)](equipment/equipment-18-20nQTcGzpUv8jJ6R.htm)|Baguette d'élargissement (sort de niveau 8)|`@Compendium[pf2e.equipment-srd.20nQTcGzpUv8jJ6R]`|
|[Wand of Widening (9th-Level Spell)](equipment/equipment-20-t5978mZ6CqfUDCP6.htm)|Baguette d'élargissement (sort de niveau 9)|`@Compendium[pf2e.equipment-srd.t5978mZ6CqfUDCP6]`|
|[Wand of the Snowfields (5th-Level Spell)](equipment/equipment-14-ut83Grf73Z8ZTaV1.htm)||`@Compendium[pf2e.equipment-srd.ut83Grf73Z8ZTaV1]`|
|[Wand of the Snowfields (7th-Level Spell)](equipment/equipment-18-q2kE0mEUAEL3gQv0.htm)||`@Compendium[pf2e.equipment-srd.q2kE0mEUAEL3gQv0]`|
|[Wand of the Spider (2nd-Level Spell)](equipment/equipment-07-tf69NMnUoUAYrWtj.htm)||`@Compendium[pf2e.equipment-srd.tf69NMnUoUAYrWtj]`|
|[Wand of the Spider (4th-Level Spell)](equipment/equipment-11-9YtYO8u5UOg4q64Y.htm)||`@Compendium[pf2e.equipment-srd.9YtYO8u5UOg4q64Y]`|
|[War Flail](equipment/weapon-00-mVxA9E2ernBL6fM6.htm)|Fléau de guerre|`@Compendium[pf2e.equipment-srd.mVxA9E2ernBL6fM6]`|
|[War Razor](equipment/weapon-00-nOFcCidD5AwVZWTv.htm)|Rasoir de combat|`@Compendium[pf2e.equipment-srd.nOFcCidD5AwVZWTv]`|
|[Warhammer](equipment/weapon-00-rXt4629QSg7KDTgJ.htm)|Marteau de guerre|`@Compendium[pf2e.equipment-srd.rXt4629QSg7KDTgJ]`|
|[Warning Snare](equipment/consumable-04-XU8u9F3uoesGDjgM.htm)|Piège artisanal avertisseur|`@Compendium[pf2e.equipment-srd.XU8u9F3uoesGDjgM]`|
|[Warrior's Training Ring](equipment/equipment-05-c0gjWASSeNIRNmEw.htm)||`@Compendium[pf2e.equipment-srd.c0gjWASSeNIRNmEw]`|
|[Water Purifier](equipment/equipment-00-kDrlBnkDMVxPrahI.htm)||`@Compendium[pf2e.equipment-srd.kDrlBnkDMVxPrahI]`|
|[Waterproof Journal](equipment/equipment-01-i2JwyZJpVZhlOObL.htm)||`@Compendium[pf2e.equipment-srd.i2JwyZJpVZhlOObL]`|
|[Waterskin](equipment/equipment-00-VnPh324pKwd2ZB66.htm)|Outre|`@Compendium[pf2e.equipment-srd.VnPh324pKwd2ZB66]`|
|[Wax Key Blank](equipment/equipment-00-w5pzAwbKGLOOomrK.htm)||`@Compendium[pf2e.equipment-srd.w5pzAwbKGLOOomrK]`|
|[Wayfinder](equipment/equipment-02-gbwr57aT9ou8yKWT.htm)|Guide|`@Compendium[pf2e.equipment-srd.gbwr57aT9ou8yKWT]`|
|[Wayfinder of Rescue](equipment/equipment-04-PhjutBgR1egCuHvY.htm)||`@Compendium[pf2e.equipment-srd.PhjutBgR1egCuHvY]`|
|[Weapon Potency (+1)](equipment/equipment-02-DKWuJb2rSgiotOG7.htm)|Puissance d'arme +1 (rune)|`@Compendium[pf2e.equipment-srd.DKWuJb2rSgiotOG7]`|
|[Weapon Potency (+2)](equipment/equipment-10-pOoEiuEuITm4I2Il.htm)|Puissance d'arme +2 (rune)|`@Compendium[pf2e.equipment-srd.pOoEiuEuITm4I2Il]`|
|[Weapon Potency (+3)](equipment/equipment-16-rvItjGQaYT7Luwtm.htm)|Puissance d'arme +3 (rune)|`@Compendium[pf2e.equipment-srd.rvItjGQaYT7Luwtm]`|
|[Weeping Midnight](equipment/consumable-16-mamIdMfPguGM8QV7.htm)||`@Compendium[pf2e.equipment-srd.mamIdMfPguGM8QV7]`|
|[Whip](equipment/weapon-00-f1gwoTkf3Nn0v3PN.htm)|Fouet|`@Compendium[pf2e.equipment-srd.f1gwoTkf3Nn0v3PN]`|
|[Whip of Compliance](equipment/weapon-09-DXidUrzzkwWIMNMD.htm)||`@Compendium[pf2e.equipment-srd.DXidUrzzkwWIMNMD]`|
|[Whisper Briolette](equipment/consumable-07-kzfRjSrq4JNZlc32.htm)||`@Compendium[pf2e.equipment-srd.kzfRjSrq4JNZlc32]`|
|[Whisper of the First Lie](equipment/equipment-20-sKyJDfHdKacfbNOG.htm)|Murmure du mensonge originel|`@Compendium[pf2e.equipment-srd.sKyJDfHdKacfbNOG]`|
|[White Dragon's Breath Potion (Adult)](equipment/consumable-12-WwWssHoEhyh7Yrrw.htm)|Potion de souffle du dragon blanc adulte|`@Compendium[pf2e.equipment-srd.WwWssHoEhyh7Yrrw]`|
|[White Dragon's Breath Potion (Wyrm)](equipment/consumable-17-OiQDcUMrlrYXDgEf.htm)|Potion de souffle de dracosire blanc|`@Compendium[pf2e.equipment-srd.OiQDcUMrlrYXDgEf]`|
|[White Dragon's Breath Potion (Young)](equipment/consumable-07-xRrMmFzpa0WU9uV4.htm)|Potion de souffle du jeune dragon blanc|`@Compendium[pf2e.equipment-srd.xRrMmFzpa0WU9uV4]`|
|[Wide landscape by an expert](equipment/treasure-00-s6wRFNG5lVEkGGdJ.htm)|Large paysage (par un expert)|`@Compendium[pf2e.equipment-srd.s6wRFNG5lVEkGGdJ]`|
|[Winged](equipment/equipment-13-ds7j3D8IIyxWd2XI.htm)||`@Compendium[pf2e.equipment-srd.ds7j3D8IIyxWd2XI]`|
|[Winged (Greater)](equipment/equipment-19-Ztb4xv4UGZbF32TE.htm)||`@Compendium[pf2e.equipment-srd.Ztb4xv4UGZbF32TE]`|
|[Winged Boots](equipment/equipment-10-aKoMqPDmVzPI7Q20.htm)|Bottes ailées|`@Compendium[pf2e.equipment-srd.aKoMqPDmVzPI7Q20]`|
|[Winter Wolf Elixir (Greater)](equipment/consumable-16-aDn5blt2iiYJpzbe.htm)|Elixir de loup arctique (supérieur)|`@Compendium[pf2e.equipment-srd.aDn5blt2iiYJpzbe]`|
|[Winter Wolf Elixir (Lesser)](equipment/consumable-04-n9VrmK9Us0viv20P.htm)|Elixir de loup arctique (inférieur)|`@Compendium[pf2e.equipment-srd.n9VrmK9Us0viv20P]`|
|[Winter Wolf Elixir (Moderate)](equipment/consumable-12-2prxM8Q0F4sdSwPx.htm)|Elixir du loup arctique (modéré)|`@Compendium[pf2e.equipment-srd.2prxM8Q0F4sdSwPx]`|
|[Wolf Fang](equipment/consumable-01-clyXfh0aVXgij2Hb.htm)|Croc de loup|`@Compendium[pf2e.equipment-srd.clyXfh0aVXgij2Hb]`|
|[Wolfsbane](equipment/consumable-10-NRoA1HpA3ElPGBEQ.htm)|Aconit|`@Compendium[pf2e.equipment-srd.NRoA1HpA3ElPGBEQ]`|
|[Wondrous Figurine (Candy Constrictor)](equipment/equipment-05-6rhind8MDhtJHlwq.htm)||`@Compendium[pf2e.equipment-srd.6rhind8MDhtJHlwq]`|
|[Wondrous Figurine (Golden Lions)](equipment/equipment-10-RTxne78VqPqTz2VN.htm)|Figurine merveilleuse (Lions dorés)|`@Compendium[pf2e.equipment-srd.RTxne78VqPqTz2VN]`|
|[Wondrous Figurine (Jade Serpent)](equipment/equipment-07-RjJw7iHantxqeJu1.htm)|Figurine merveilleuse (Serpent de jade)|`@Compendium[pf2e.equipment-srd.RjJw7iHantxqeJu1]`|
|[Wondrous Figurine (Marble Elephant)](equipment/equipment-13-TXPQfQriNJqMYcfp.htm)|Figurine merveilleuse (Elephant de marbre)|`@Compendium[pf2e.equipment-srd.TXPQfQriNJqMYcfp]`|
|[Wondrous Figurine (Obsidian Steed)](equipment/equipment-15-RnpzfRWsOW6zmAgN.htm)|Figurine merveilleuse (Destrier d'obsidienne)|`@Compendium[pf2e.equipment-srd.RnpzfRWsOW6zmAgN]`|
|[Wondrous Figurine (Onyx Dog)](equipment/equipment-02-BANLXq8FhwqsDu0v.htm)|Figurine merveilleuse (Chien d'onyx)|`@Compendium[pf2e.equipment-srd.BANLXq8FhwqsDu0v]`|
|[Wondrous Figurine (Rubber Bear)](equipment/equipment-07-YO2X0EE3txascuuP.htm)||`@Compendium[pf2e.equipment-srd.YO2X0EE3txascuuP]`|
|[Wooden Shield](equipment/armor-00-ezVp13Uw8cWW08Da.htm)|Bouclier en bois|`@Compendium[pf2e.equipment-srd.ezVp13Uw8cWW08Da]`|
|[Wounding](equipment/equipment-07-fo6Yhq5mbQXsnZs0.htm)|Sanglante (rune)|`@Compendium[pf2e.equipment-srd.fo6Yhq5mbQXsnZs0]`|
|[Writ of Authenticity](equipment/equipment-02-YZxq8rGPiLPIKWIQ.htm)||`@Compendium[pf2e.equipment-srd.YZxq8rGPiLPIKWIQ]`|
|[Writing Set](equipment/equipment-00-QJb8S927Yj81EgHH.htm)|Nécessaire d'écriture|`@Compendium[pf2e.equipment-srd.QJb8S927Yj81EgHH]`|
|[Writing Set (Extra Ink and Paper)](equipment/equipment-00-3yLu12TCwN8utY0u.htm)|Encre eet papier supplémentaires|`@Compendium[pf2e.equipment-srd.3yLu12TCwN8utY0u]`|
|[Wyvern Poison](equipment/consumable-08-WL4O32qFifxnMj0H.htm)|Poison de vouivre|`@Compendium[pf2e.equipment-srd.WL4O32qFifxnMj0H]`|
|[Yellow Musk Poison](equipment/consumable-02-bJVk50mhaODgsOUe.htm)||`@Compendium[pf2e.equipment-srd.bJVk50mhaODgsOUe]`|
|[Zerk](equipment/consumable-04-eKpL2j1JA92wndO6.htm)|Zerk|`@Compendium[pf2e.equipment-srd.eKpL2j1JA92wndO6]`|
|[Zircon](equipment/treasure-00-p3mg6gK5KhpS7plo.htm)|Zircon|`@Compendium[pf2e.equipment-srd.p3mg6gK5KhpS7plo]`|
