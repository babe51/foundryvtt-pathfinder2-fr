# État de la traduction (familiar-abilities)

 * **officielle**: 8
 * **libre**: 23


Dernière mise à jour: 2021-02-07 20:50 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[01-0Xrkk46IM43iI1Fv.htm](familiar-abilities/01-0Xrkk46IM43iI1Fv.htm)|Darkvision|Vision dans le noir|officielle|
|[01-2fiQzoEKu6YUnrU9.htm](familiar-abilities/01-2fiQzoEKu6YUnrU9.htm)|Independent|Indépendant|libre|
|[01-57b8u8s3fV0UCrgJ.htm](familiar-abilities/01-57b8u8s3fV0UCrgJ.htm)|Plant Form|Forme de plante|libre|
|[01-5gwqSpkRqWzrbDDU.htm](familiar-abilities/01-5gwqSpkRqWzrbDDU.htm)|Damage Avoidance: Will|Évitement des dégâts : Volonté|libre|
|[01-7QosmRHlyLLhU1hX.htm](familiar-abilities/01-7QosmRHlyLLhU1hX.htm)|Lab Assistant|Assistant de laboratoire|officielle|
|[01-8Z1UkLEWkFWIjOF8.htm](familiar-abilities/01-8Z1UkLEWkFWIjOF8.htm)|Poison Reservoir|Réservoir de poison|libre|
|[01-92lgSEPFIDLvKOCF.htm](familiar-abilities/01-92lgSEPFIDLvKOCF.htm)|Accompanist|Accompagnateur|libre|
|[01-9PsptrEoCC4QdM23.htm](familiar-abilities/01-9PsptrEoCC4QdM23.htm)|Valet|Valet|libre|
|[01-A0C86V3MUECpX5jd.htm](familiar-abilities/01-A0C86V3MUECpX5jd.htm)|Amphibious|Amphibie|libre|
|[01-asOhEdyF8CWFbR96.htm](familiar-abilities/01-asOhEdyF8CWFbR96.htm)|Spellcasting|Incantateur|libre|
|[01-BXssJhTJjKrfojwG.htm](familiar-abilities/01-BXssJhTJjKrfojwG.htm)|Fast Movement: Land|Déplacement rapide : Au sol|libre|
|[01-cT5octWchU4gjrhP.htm](familiar-abilities/01-cT5octWchU4gjrhP.htm)|Manual Dexterity|Dextérité manuelle|officielle|
|[01-dWTfO5WbLkD5mw2H.htm](familiar-abilities/01-dWTfO5WbLkD5mw2H.htm)|Climber|Grimpeur|officielle|
|[01-FcQQLMAJMgOLjnSv.htm](familiar-abilities/01-FcQQLMAJMgOLjnSv.htm)|Resistance|Résistance|libre|
|[01-FlRUb8U13Crj3NaA.htm](familiar-abilities/01-FlRUb8U13Crj3NaA.htm)|Scent|Odorat|officielle|
|[01-jevzf9JbJJibpqaI.htm](familiar-abilities/01-jevzf9JbJJibpqaI.htm)|Skilled|Compétent|libre|
|[01-JRP2bdkdCdj2JDrq.htm](familiar-abilities/01-JRP2bdkdCdj2JDrq.htm)|Master's Form|Forme du maître|libre|
|[01-K5OLRDsGCfPZ6mO6.htm](familiar-abilities/01-K5OLRDsGCfPZ6mO6.htm)|Damage Avoidance: Reflex|Évitement des dégâts : Réflexes|libre|
|[01-Le8UWr5BU8rV3iBf.htm](familiar-abilities/01-Le8UWr5BU8rV3iBf.htm)|Tough|Robuste|libre|
|[01-mK3mAUWiRLZZYNdz.htm](familiar-abilities/01-mK3mAUWiRLZZYNdz.htm)|Damage Avoidance: Fortitude|Évitement des dégâts : Vigueur|libre|
|[01-O5TIjqXAuta8iVSz.htm](familiar-abilities/01-O5TIjqXAuta8iVSz.htm)|Focused Rejuvenation|Récupération concentrée|libre|
|[01-qTxH8mSOvc4PMzrP.htm](familiar-abilities/01-qTxH8mSOvc4PMzrP.htm)|Kinspeech|Parole des semblables|officielle|
|[01-SxWYVgqNMsq0OijU.htm](familiar-abilities/01-SxWYVgqNMsq0OijU.htm)|Fast Movement: Climb|Déplacement rapide : Escalade|libre|
|[01-uUrsZ4WvhjKjFjnt.htm](familiar-abilities/01-uUrsZ4WvhjKjFjnt.htm)|Toolbearer|Porte outils|libre|
|[01-uy15sDBuYNK48N3v.htm](familiar-abilities/01-uy15sDBuYNK48N3v.htm)|Burrower|Fouisseur|officielle|
|[01-v7zE3tKQb9G6PaYU.htm](familiar-abilities/01-v7zE3tKQb9G6PaYU.htm)|Partner in Crime|Associés dans le crime|libre|
|[01-VHQUZcjUxfC3GcJ9.htm](familiar-abilities/01-VHQUZcjUxfC3GcJ9.htm)|Fast Movement: Fly|Déplacement rapide : Vol|libre|
|[01-vpw2ReYdcyQBpdqn.htm](familiar-abilities/01-vpw2ReYdcyQBpdqn.htm)|Fast Movement: Swim|Déplacement rapide : Nage|libre|
|[01-XCqYnlVbLGqEGPeX.htm](familiar-abilities/01-XCqYnlVbLGqEGPeX.htm)|Touch Telepathy|Télépathie de contact|libre|
|[01-ZHSzNt3NxkXbj1mI.htm](familiar-abilities/01-ZHSzNt3NxkXbj1mI.htm)|Flier|Volant|libre|
|[01-zyMRLQnFCQVpltiR.htm](familiar-abilities/01-zyMRLQnFCQVpltiR.htm)|Speech|Parole|officielle|
